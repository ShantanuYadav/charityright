$(document).ready(function () {
    
  // function incrementValue(e) {
  //     e.preventDefault();
  //     var fieldName = $(e.target).data('field');
  //     var parent = $(e.target).closest('div');
  //     var base_value = parseFloat($("input.base_value").val())||0;
  //     var incremented_qty = (parseInt($("input.quantity-field").val()) || 0) + 1;
  //     $("input.quantity-field").val(incremented_qty);
  //     updateBaseValueAndLabels();
  //     $("#donationammount").val((incremented_qty*base_value)*100);
  //   }
    
  //   function decrementValue(e) {
  //     e.preventDefault();
  //     var fieldName = $(e.target).data('field');
  //     var parent = $(e.target).closest('div');
  //     var base_value = parseFloat($("input.base_value").val())||0;
  //     var incremented_qty = (parseInt($("input.quantity-field").val()) || 0) - 1;
  //     $("input.quantity-field").val(incremented_qty);
  //     updateBaseValueAndLabels();
  //     $("#donationammount").val((incremented_qty*base_value)*100);
  //   }
    
  //   $('.input-group').on('click', '.button-plus', function(e) {
  //     incrementValue(e);
  //   });
    
  //   $('.input-group').on('click', '.button-minus', function(e) {
  //     decrementValue(e);
  //   });

    $("[data-autofill-amount]").click(function(){
      var amount = parseFloat($(this).attr('data-autofill-amount'))||0;
      $('html, body').animate({
          scrollTop: $("#donation-widget-box").offset().top - 40
      }, 1000);
      updateBaseValueAndLabels(amount);
    });
    $(".set-feed-family").click(function(){
      $("#fordonation9").val('Feed a family');
    });
    $("input#quantityprice").on('change',function(){
      var value = parseFloat($(this).val())||0;
      updateBaseValueAndLabels(value);
    });
});

function updateBaseValueAndLabels(total_value){
var base_value = parseFloat($("input.base_value").val())||0;
// var quantity = parseFloat($("input.quantity-field").val())||0;
// console.log(quantity);
// $(".base_value_label").text('£'+ (base_value).toFixed(2) +' ×');
// var total_value = base_value*quantity;
// $(".total_value_label").text('= £'+ (total_value).toFixed(2) +' ');
$("#donationammount,#quantityprice").val((total_value).toFixed(2));
StripeCheckout.__app.configurations.button0.amount = total_value * 100; // convert to cents
}