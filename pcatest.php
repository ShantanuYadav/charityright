<?php
$return = array();
$return['IsCorrect'] = 'True';
$return['IsDirectDebitCapable'] = 'True';
$return['StatusInformation'] = 'CautiousOK';
$return['CorrectedSortCode'] = '000099';
$return['CorrectedAccountNumber'] = '12345678';
$return['IBAN'] = 'GB27NWBK00009912345678';
$return['Bank'] = 'TEST BANK PLC PLC';
$return['BankBIC'] = 'NWBKGB21';
$return['Branch'] = 'Worcester';
$return['BranchBIC'] = '18R';
$return['ContactAddressLine1'] = '2 High Street';
$return['ContactAddressLine2'] = 'Smallville';
$return['ContactPostTown'] = 'Worcester';
$return['ContactPostcode'] = 'WR2 6NJ';
$return['ContactPhone'] = '01234 456789';
$return['ContactFax'] = '';
$return['FasterPaymentsSupported'] = 'False';
$return['CHAPSSupported'] = 'True';
//print_r($return);
//echo 'fred';
echo json_encode(array($return));
?>