<?php
require_once('init.php');

class EventsAjax extends BasePage
{
	public function __construct()
	{	parent::__construct();

		if (method_exists($this, $method = 'Action_' . $_GET['action']))
		{	$this->$method();
		}

	} // end of fn __construct

	private function Action_ticketget($ticket = false)
	{	if ((is_a($ticket, 'TicketType') || ($ticket = new TicketType($_GET['ttid']))) && $ticket->id)
		{	$in_cart = (int)$_SESSION['events']['event_tickets'][$ticket->id];
			if ($ticket->details['stockcontrol'])
			{	if (!$to_book = $ticket->TicketsLeft($in_cart))
				{	echo '<div class="failmessage">No more of these tickets available to book</div>';
					if ($in_cart)
					{	echo '<p>You already have ', $in_cart, ' of these tickets in your cart</p>';
					}
					return;
				}
			}
			$eventdate = new EventDate($ticket->eventdate);
			echo '<div class="tpopInfo">"', $this->InputSafeString($ticket->event['eventname'] . '" at ' . $ticket->venue['venuename'] . ', ' . $this->GetCountry($ticket->venue['country'])), '</div><div class="tpopDates">', $eventdate->DatesString(), '</div>';
			if (!$_SESSION['events']['currency'] || ($ticket->details['price'] == 0) || ($_SESSION['events']['currency'] == $eventdate->details['currency']))
			{   if($ticket->details['price'] > 0){
					$cursymbol = $this->GetCurrency($eventdate->details['currency'], 'cursymbol');
					$ticket_price = $cursymbol.''.number_format($ticket->details['price'], 2);
				}else{
					$ticket_price = 'Free';
				}
				echo '<div class="tpopBookForm"><form onsubmit="SaveTicketBook(', $ticket->id, ');return false;"><label for="tpopFormNumber">', $this->InputSafeString($ticket->details['ttypename']), ' (',$ticket_price,') &times;<br class="visible-xs"/><label><input type="number" value="1" id="tpopFormNumber" min="1" step="1"/><input type="submit" value="Book" /></form></div>';
			} else
			{	echo '<div class="failmessage">You cannot book tickets with a different currency at the same time</div>';
			}
			echo '<div class="ttopFooter">';
			if ($to_book)
			{	echo '<p>', $to_book, ' left to book</p>';
				if ($in_cart)
				{	echo '<p>You already have ', $in_cart, ' of these tickets in your cart</p>';
				}
			}
			echo '</div>';
			//$this->VarDump($ticket->details);
		}
	} // end of fn Action_ticketget

	private function Action_ticketbook()
	{	if (($ticket = new TicketType($_GET['ttid'])) && $ticket->id)
		{	$saved = $ticket->AddToCart($_GET['number']);
			if ($saved['successmessage'])
			{	$eventdate = new EventDate($ticket->eventdate);
				echo '<div class="tpopBooked">', (int)$_GET['number'], ' ', $this->InputSafeString($ticket->details['ttypename']), ' tickets booked for "', $this->InputSafeString($ticket->event['eventname'] . '" at ' . $ticket->venue['venuename'] . ', ' . $this->GetCountry($ticket->venue['country'])), ' on ', $eventdate->DatesString(), '</div><div class="tpopCartLink"><a href="', SITE_SUB, '/bookcart.php">Go to cart</a></div>';
			} else
			{	echo '<div class="failmessage">', $this->InputSafeString($saved['failmessage']), '</div>';
				$this->Action_ticketget($ticket);
			}
		}
	} // end of fn Action_ticketbook

	private function Action_ticketadjust()
	{	if (($ticket = new TicketType($_GET['ttid'])) && $ticket->id)
		{	$_SESSION['events']['event_tickets'][$ticket->id] += (int)$_GET['adjust'];
			if (!$_SESSION['events']['event_tickets'][$ticket->id])
			{	unset($_SESSION['events']['event_tickets'][$ticket->id]);
				unset($_SESSION['events']['currency']);
				// check for any paid ticket types and put back currency
				if ($_SESSION['events']['event_tickets'])
				{	foreach ($_SESSION['events']['event_tickets'] as $ttid=>$qty)
					{	$check_ticket = new TicketType($ttid);
						if ($check_ticket->details['price'] > 0)
						{	$_SESSION['events']['currency'] = $check_ticket->eventdate['currency'];
							break;
						}
					}
				}
			}
			$cart = new BookingCart($_SESSION['events']);
			echo $cart->CartHeader();
		}
	} // end of fn Action_ticketadjust

	private function Action_cartheader()
	{	$cart = new BookingCart($_SESSION['events']);
		echo $bookHeader = $cart->BookCartHeader();
	} // end of fn Action_ticketbook

} // end of class EventsAjax

$ajax = new EventsAjax();
?>