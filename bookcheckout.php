<?php
require_once('init.php');

class BookingCheckoutPage extends EventsBasePage
{	private $cart;

	public function __construct()
	{	parent::__construct('events');
		$this->css[] = 'cart.css';
		$this->css[] = 'page.css';
		$this->js[] = 'events.js';
		$this->cart = new BookingCart($_SESSION['events']);
		$this->canonical_link = SITE_SUB . '/bookcheckout.php';
		if ($this->cart->CanCheckout() && isset($_POST['firstname']))
		{	$saved = $this->cart->SaveAddress($_POST, $_SESSION['events']['event_customer']);
			$this->cart->GetFromSession($_SESSION['events']);
			if ($saved['successmessage'])
			{	header('location: ' . SITE_SUB . '/bookbuy.php');
			}
			$this->failmessage = $saved['failmessage'];
		}
	} // end of fn __construct

	function MainBodyContent()
	{	echo '<div class="container page-header"><div class="container_inner"><h1 class="page_heading">Events - Checkout</h1></div></div>';
		echo '<div class="container page-section page-section-white"><div class="container_inner">', $this->cart->CartHeader();
		if ($this->cart->CanCheckout())
		{	echo '<div class="bookingAddressForm">', $this->cart->AddressForm(), '</div>';
		}
		echo '</div></div>';
	} // end of fn MemberBody

} // end of class BookingCheckoutPage

$page = new BookingCheckoutPage();
$page->Page();
?>