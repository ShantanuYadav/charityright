<?php
/**
 * Stripe - Payment Gateway integration example (Stripe Checkout)
 */
// Stripe library
require '../landing-pages-commons/stripe/Stripe.php';
$params = array(
    "testmode"   => "off",
    "private_live_key" => "sk_live_tpgBJlRErfe4Tm0iVYjwWykp",
    "public_live_key"  => "pk_live_9J7JiUJNtubpxNklNaLfTNVd",
    "private_test_key" => "sk_test_4UdqBVJQbrV1KXObfvs5yMUD00yIDQjeui",
    "public_test_key"  => "pk_test_0qLdOirAp6ECXSBdDJQ7MSNh00zqhKysUB"
);
$receipt_email = "digital@biggorillaapps.com";
$track_email_id = '';
$track_email_encoded = md5(time() . rand(0, 89739));
if (isset($_GET['teston'])) {
    $params['testmode'] = 'on';
    $receipt_email = 'zeeshan@biggorillaapps.com';
}
if ($params['testmode'] == "on") {
    Stripe::setApiKey($params['private_test_key']);
    $pubkey = $params['public_test_key'];
    $prikey = $params['private_test_key'];
} else {
    Stripe::setApiKey($params['private_live_key']);
    $pubkey = $params['public_live_key'];
    $prikey = $params['private_live_key'];
}
if (isset($_POST['stripeToken'])) {
    $invoiceid = microtime();                      // Invoice ID
    $description = "Invoice #" . $invoiceid ;
    try {
        $charge = Stripe_Charge::create(
            array(
                "amount" => intval(substr($_POST['donationammount'], 0, -2) * 100),
                "currency" => "usd",
                "source" => $_POST['stripeToken'],
                "description" => $description
            )
        );
        // Payment has succeeded, no exceptions were thrown or otherwise caught				
        $result = "success";
    } catch (Stripe_CardError $e) {
        $error = $e->getMessage();
        $result = "declined";
    } catch (Stripe_InvalidRequestError $e) {
        $result = "declined";
    } catch (Stripe_AuthenticationError $e) {
        $result = "declined";
    } catch (Stripe_ApiConnectionError $e) {
        $result = "declined";
    } catch (Stripe_Error $e) {
        $result = "declined";
    } catch (Exception $e) {
        if ($e->getMessage() == "zip_check_invalid") {
            $result = "declined";
        } else if ($e->getMessage() == "address_check_invalid") {
            $result = "declined";
        } else if ($e->getMessage() == "cvc_check_invalid") {
            $result = "declined";
        } else {
            $result = "declined";
        }
    }
    if ($result == "success") { ?>
        <link href="css/payment.css" rel="stylesheet" type="text/css" media="all">
        <div class="alert_g">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Success!</strong> Thanks for your contribution!
        </div>
        <?php
        $to = $receipt_email;
        $subject = "Donation email";
        $message = '<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<title>Charity Right</title>
<style type="text/css">
    @media only screen and (max-width: 479px) {	
	.offerBox {float:none !important; width:100% !important; padding-top:20px !important;}
	.offerHd {padding-bottom:0 !important;}
	.contentBox {padding:15px !important;}
	.offerCon {padding:0 15px !important;}
	.botContent {padding:15px !important;}
	.footer {padding:0 15px !important;}
	.footerLinks {font-size:13px !important; padding-bottom:5px !important;}
    }
</style>
</head>
<body style="background:#f6f2f2; margin:0;">
<table cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="#ffffff" style="max-width:700px;">
	<tr>
    	<td style="padding:20px;background: #000;" align="center"><img src="https://charityright.org.uk/sudan2020/img/logo.png" alt="" style="vertical-align:top;"></td>
    </tr>
    <tr>
    	<td><img src="https://charityright.org.uk/sudan2020/images/emailimages/banner.jpg" alt="" style="vertical-align:top; width:100%;"></td>
    </tr>
    <tr>
    	<td>
        	<table cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td class="contentBox" style="padding:30px; font-size:15px; font-family:Arial, Helvetica, sans-serif; color:#787676; line-height:22px;">
                    Hi Admin,<br><br>
                    <strong style="font-size:18px;">Hope you are doing well.</strong><br><br>
                    Name: ' . $_POST['username99'] . '<br>
                    Email: ' . $_POST['stripeEmail'] . '<br>
                    Donation For: ' . $_POST['fordonation99'] . '<br>
                    Amount: $' . intval($charge->amount / 100) . '<br>
                    </td>
                </tr>
            </table>                
        </td>
    </tr>
    <tr>
    	<td><img src="https://charityright.org.uk/sudan2020/images/emailimages/footer_bg.jpg" alt="" style="vertical-align:top; width:100%;"></td>
    </tr>
</table>
</body>
</html>';
        // Set Unique id for tracking using email ro time
        $track_email_id = @empty($_POST['stripeEmail']) ? time() . $_SERVER['REMOTE_ADDR'] : $_POST['stripeEmail'];
        $track_email_encoded = md5($track_email_id);
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: <info@charityright.org.uk>' . "\r\n";
        /*$headers .= 'Cc: myboss@example.com' . "\r\n";*/
        mail($to, $subject, $message, $headers);
        // submit to donation record api
        $page_name = $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        $page_name = $uriSegments['1'];
        $data['donor_name'] = $_POST['username99'];
        $data['donor_email'] = $_POST['stripeEmail'];
        $data['currency'] = 'usd';
        $data['donation_amount'] = intval($charge->amount / 100);
        $data['donation_for'] = $_POST['fordonation99'];
        $data['donation_page'] = $page_name;
        $data['zakat']        = @$_POST['zakat'];
        $data['gift_aid']     = @$_POST['gift_aid'];
        $data['home_address'] = @$_POST['home_address'];
        $data['postcode']     = @$_POST['postcode'];
        $data['country']      = @$_POST['country'];
        $data['test_mode'] = $params['testmode'];
        // api key 
        $data['api_key'] = '3408878710';
        $curl_target = "https://" . $_SERVER['HTTP_HOST'] . "/donations-record/index.php";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curl_target);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        if ($data['test_mode'] == 'on') {
            echo '<code> <pre> ' . print_r($server_output) . ' </pre> </code>';
        }
        ?>
    <?php } else { ?>
        <div class="alert_r">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Declined!</strong> <?php echo $result; ?>
        </div>
<?php }
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sponsor A Child In Sudan | Charity Right</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link href="../landing-pages-commons/css/ga-modal-style.css" rel="stylesheet" type="text/css" media="all">
    <!-- jQuery Modal -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <?php include('../landing-pages-commons/tracking-codes.php') ?>
</head>
<body class="page2">
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=391990840985330&ev=PageView&noscript=1" /></noscript>
    <noscript><img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/v3/?event=init&tid=2612558765069&pd[em]=<?php echo $track_email_encoded; ?>&noscript=1" /></noscript>
    <section class="nav_head">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-5">
                    <img class="img-fluid logo1" alt="Charity Right" src="img/logo.png" />
                </div>
                <div class="col-md-8 col-7 align-self-center">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="tel:01274 400389"><img class="img-fluid" alt="phone" src="img/mobile.png" />01274 400389</a></li>
                        <li class="list-inline-item"><a href="mailto:info@charityright.org.uk" target="_blank"><img class="img-fluid" alt="email" src="img/email.png" />info@charityright.org.uk</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="header_sec">
        <div class="container">
            <div class="clearfix">
                <div class="banner-left">
                    <h1>Save the life of refugee families in Sudan,
                        <br />
                        <span>This Ramadan</span>
                    </h1>
                    <p>$540 WILL FEED A FAMILY IN SUDAN FOR AN ENTIRE YEAR.<br> $45 WILL FEED THEM FOR A MONTH.</p>
                </div>
                <div class="banner-right">
                    <div class="form-start">
                        <form method="POST">
                            <div class="input-row">
                                <label>Name</label>
                                <input type="text" class="input" placeholder="Name" id="username9" autocomplete="off" required>
                            </div>
                            <div class="input-row">
                                <input type="text" class="input" name="quantityprice" id="quantityprice" value="40.00" placeholder="Amount" autocomplete="off">
                            </div>
                            <div class="input-row">
                                <label>Donation For?</label>
                                <select id="fordonation9">
                                    <option value="Feed a family in sudan">Feed a family in sudan</option>
                                    <option value="Feed a student in sudan">Feed a student in sudan</option>
                                    
                                    <option value="Feed a hifz student in sudan">Feed a hifz student in sudan</option>
                                </select>
                            </div>
                            <div class="input-row">
                                <label>Is your donation Zakat?</label>
                                <select id="donzakat" onChange="$('#zakat').val(this.value)">
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </div>
                            <button type="button" class="btn1 gift-aid-open"> Make Donation</button>
                        </form>
                    </div>
                </div>
            </div>
            <form action="" method="POST">
                <script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="<?php echo $pubkey; ?>" data-amount="4000" data-name="Charity Right" data-description="Donation" data-image="https://charityright.org.uk/sudan2020/img/logo.png" data-locale="auto" data-currency="usd" data-zip-code="true">
                </script>
                <input name="username99" value="" id="username99" type="hidden">
                <input name="donationammount" value="4000" id="donationammount" type="hidden">
                <input name="fordonation99" value="Feed a family in sudan" id="fordonation99" type="hidden">
                <input name="zakat" value="no" id="zakat" type="hidden">
                <input type="hidden" class="input" name="gift_aid">
                <input type="hidden" class="input" name="home_address">
                <input type="hidden" class="input" name="postcode">
                <input type="hidden" class="input" name="country">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script>
                    document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                    $('#username9').on('change', function() {
                        var newname = $(this).val();
                        $("#username99").val(newname);
                    });
                    $('#fordonation9').on('change', function() {
                        var fordonate = $(this).val();
                        $("#fordonation99").val(fordonate);
                    });
                </script>
                <button type="submit" class="submit-stripe-form" style="width: 1px;height: 1px;"></button>
            </form>
        </div>
    </section>
    <div class="zkatcalc-ribon" id="show-zakat-form">Donate Zakat at Charity Rights - Calculate Zakat Now!</div>
    <section class="donate_sec">
        <div class="container donate_bg">
            <div class="row">
                <div class="col-md-12">
                    <div class="donate_text">
                        <img class="img-fluid mx-auto d-block logo_donate" alt="Donate" src="img/logo1.png" />
                        <p>This Ramadan helps us save the lives of refugees in Sudan who are less fortunate than us. Support us by donating and we will ensure that your donation is turned into food to support the most vulnerable.</p>
                        <h4 class="help_text">HELP THEM</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="feed_sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="lato heading_text">We distributed 5.2 million meals in 2019. With your support we can feed even more.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a class="feed_box feed_left" href="#">
                        <img class="img-fluid" alt="" src="img/family1.png" />
                        <div class="text_feed">
                            <h3 class="roboto">FEED A FAMILY</h3>
                            <p>$540 Feeds a family for an entire year</p>
                            <p>$210 Feeds a family for 6 months</p>
                            <h4>$45 Feeds a family for 1 month</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="feed_box feed_center" href="#">
                        <img class="img-fluid" alt="" src="img/school1.png" />
                        <div class="text_feed">
                            <h3 class="roboto">FEED A school child</h3>
                            <p>$156 provides a school child with a meal every school day for an entire year</p>
                            <p>$78 provides a school child with a meal every school day for 6 months</p>
                            <h4>$13 provides a school child with a meal every school day for 1 month</h4>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="feed_box feed_right" href="#">
                        <img class="img-fluid" alt="" src="img/student1x.jpg" />
                        <div class="text_feed">
                            <h3 class="roboto">FEED A hifz student</h3>
                            <p>$300 provides a Hifz student with 3 meals every day for an entire year.</p>
                            <p>$150 provides a Hifz student with 3 meals every day for 6 months.</p>
                            <h4>$25 provides a Hifz student with 3 meals every day for 1 month.</h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img class="img-fluid logo" alt="" src="img/logo.png" />
                </div>
                <div class="col-md-4">
                    <ul class="list-unstyled">
                        <li><span class="img_class"><img class="img-fluid" alt="location" src="img/location.png" /></span> <span class="text_class">Oakwood Court, City Road, Bradford<br>
                                West Yorkshire, United Kingdom BD8 8JY</span></li>
                        <li><a href="tel:01274 400389"><span class="img_class"><img class="img-fluid" alt="phone" src="img/phone.png" /></span><span class="text_class">01274 400389</span></a></li>
                        <li><a href="mailto:info@charityright.org.uk" target="_blank"><span class="img_class"><img class="img-fluid" alt="email" src="img/mail.png" /></span><span class="">info@charityright.org.uk</span></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <p>Charity Right - Fighting Hunger, One Child at a Time.
                        Registered Charity Number 1163944</p>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal gftaid-modal" style="text-align:left;">
        <img src="https://charityright.org.uk/img/s960_Giftaid.jpg" alt="" style="width: 100%;">
        <div class="boxes checkclaimGift">
            <input type="checkbox" class="claimGiftAid" id="claimGiftAid">
            <label for="claimGiftAid">Please tick if you’d like to claim Gift Aid</label>
        </div>
        <br>
        <hr>
        <p class="ga-text">By ticking the above box, I confirm I am a UK taxpayer and I would like Charity Right to treat this donation as a Gift Aid donation. I understand that if I pay less income tax and/or capital gains tax than the amount of Gift Aid claimed on all my donations in that year, it is my responsibility to pay any difference</p>
        <div class="ga-form">
            <div class="input-row">
                <label>Your home address</label>
                <input type="text" class="input" placeholder="Address" id="home_address" autocomplete="off">
            </div>
            <div class="input-row">
                <label>Post code</label>
                <input type="text" class="input" placeholder="" id="postcode" autocomplete="off">
            </div>
        </div>
        <button type="button" class="btn make-donation-btn"> Skip Gift Aid </button>
    </div>
    <!-- javascript libraries -->
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAHImaffLZPznceJFmDYimIN9Rd9tuJGtc&libraries=places'></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5/jquery.googlemap.min.js'></script>

    <script>
        $(function() {
            $("#quantityprice").change(function(){
                var amount = (parseFloat($(this).val())||0) * 100;
                StripeCheckout.__app.configurations.button0.amount = amount; // convert to cents
                $("#donationammount").val(amount);
            });
            $(".gift-aid-open").click(function() {
                $(".gftaid-modal").modal();
            });
            $(".make-donation-btn").click(function() {
                // $("#stripe-donation-form").get(0).submit();
                $.modal.close();
                $(".submit-stripe-form").click();
            });
            $("#claimGiftAid").change(function() {
                if ($(this).is(":checked")) {
                    $(".make-donation-btn").text('CLAIM GIFT AID');
                    $("input[name='gift_aid']").val('yes');
                } else {
                    $(".make-donation-btn").text('Skip');
                    $("input[name='gift_aid']").val('no');
                }
            });
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('home_address'), {
                types: ['geocode']
            });
            // Avoid paying for data that you don't need by restricting the set of
            // place fields that are returned to just the address components.
            autocomplete.setFields(['address_component']);
            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        });
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            var address = [];
            var postcode = country = '';
            // // Get each component of the address from the place details,
            // // and then fill-in the corresponding field on the form.
            if (place != undefined) {
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (addressType == 'street_number' || addressType == 'route' || addressType == 'locality' || addressType == 'postal_town') {
                        if (addressType == 'street_number') {
                            var val = place.address_components[i]['short_name'];
                            address.push(val);
                        }
                        if (addressType == 'route' || addressType == 'locality' || addressType == 'postal_town') {
                            var val = place.address_components[i]['long_name'];
                            address.push(val);
                        }
                    }
                    if (addressType == 'country') {
                        var country = place.address_components[i]['short_name'];
                    }
                    if (addressType == 'postal_code') {
                        var postcode = place.address_components[i]['short_name'];
                    }
                }
                if (country) {
                    $('#country').val(country);
                    $('input[name=country]').val(country);
                }
                $('#postcode').val('');
                if (postcode) {
                    $('#postcode').val(postcode);
                    $('input[name=postcode]').val(postcode);
                }
                $("input[name=home_address]").val($("input#home_address").val());
            }
        }
    </script>
    <?php include('../landing-pages-commons/zakat-calculation-form.php'); ?>
    <script>
        $(function(){
            jQuery('#currency').val('US');
            jQuery('#currency').trigger('change');
        });
    </script>
</body>
</html>