<?php
include_once('init.php');

class CartViewPage extends CartPage
{	
	public function __construct() // constructor
	{	parent::__construct();
		if (isset($_GET['remove']))
		{	unset($_SESSION['cart_plates'][$_GET['remove']]);
		}
		if (isset($_GET['prod_remove']))
		{	unset($_SESSION['cart_products'][$_GET['prod_remove']]);
		}
		if (isset($_GET['prod_plus']))
		{	if ($_SESSION['cart_products'][$_GET['prod_plus']]['qty'])
			{	$_SESSION['cart_products'][$_GET['prod_plus']]['qty']++;
			}
		}
		if (isset($_GET['prod_minus']))
		{	if ($_SESSION['cart_products'][$_GET['prod_minus']]['qty'])
			{	$_SESSION['cart_products'][$_GET['prod_minus']]['qty']--;
				if (!$_SESSION['cart_products'][$_GET['prod_minus']]['qty'])
				{	unset($_SESSION['cart_products'][$_GET['prod_minus']]);
				}
			}
		}
		if (!isset($_POST['disccode']) && !isset($_SESSION['cart_disccode']) && $_COOKIE['promo'])
		{	$_POST['disccode'] = $_COOKIE['promo'];
			setcookie('promo', '', strtotime('-1 day'), '/');
		}
		if (isset($_POST['disccode']))
		{	if ($_POST['disccode'])
			{	$discount = new DiscountCode();
				if ($discount->GetFromCode($_POST['disccode']) && $discount->CurrentlyValid())
				{	$_SESSION['cart_disccode'] = $discount->details['disccode'];
					$this->successmessage = 'Discount code applied';
				} else
				{	$this->failmessage = 'Discount code not found';
				}
			} else
			{	unset($_SESSION['cart_disccode']);
			}
		}
	
	} // end of fn __construct, constructor
	
	protected function CartBody()
	{	ob_start();
		if (($cart_plates = $this->GetCartPlates()) || $_SESSION['cart_products'])
		{	$total_cost = 0;
			if ($_SESSION['cart_disccode'])
			{	$discount = new DiscountCode();
				if (!$discount->GetFromCode($_SESSION['cart_disccode']) || !$discount->CurrentlyValid())
				{	unset($discount);
				}
			}
			
			if ($cart_plates)
			{	foreach ($cart_plates as $plate_key=>$plate_vars)
				{	echo '<div class="cart_plate clearfix"><div class="cart_plate_images clearfix">';
					if (is_array($plate_vars['plate_images']))
					{	foreach($plate_vars['plate_images'] as $plate_image)
						{	echo '<div><img width="100%" src="resources/temp/', $plate_image, '?', time(), '" /></div>';
						}
					}
					echo '</div><div class="cart_plate_details">', $this->CartPlateDetails($plate_vars), '<span class="cartBlockPrice"><span><span>&pound;', number_format($plate_vars['cart_price'], 2), '</span></span></span></div><div class="cart_plate_options"><a class="cartButton" href="', SITE_URL, 'cart.php?remove=', $plate_key, '">Remove plate from cart</a><a class="cartButton" href="', SITE_URL, 'plate_edit.php?plate_editid=', $plate_key, '">edit this plate</a></div></div>';
					$total_cost += $plate_vars['cart_price'];
				}
			}
			echo '<div class="cartSummaryProductList"><h2 class="heading">Shop Products</h2>';
			if ($_SESSION['cart_products'])
			{	$attributes = array();
				echo '<div class="cspl_row cspl_rowheader clearfix"><div class="csplProduct">Product</div><div class="csplQty">Quantity</div><div class="csplPrice">Price</div></div>';
				foreach ($_SESSION['cart_products'] as $cart_item_id=>$product_details)
				{	if (($product = new Product($product_details['prodid'])) && $product->id && ($qty = (int)$product_details['qty']))
					{	$product_prices = array();
						for ($i = 1; $i <= $qty; $i++)
						{	if ($product->details['autoplateadd'])
							{	++$autoplateadd_count;
							}
							if ($product->details['autoplateadd'] && $discount && $discount->details['freefixingkit'])
							{	if ($autoplateadd_count <= count($cart_plates))
								{	$product_prices[0]++;
								} else
								{	$product_prices[$product->details['price']]++;
								}
							} else
							{	$product_prices[$product->details['price']]++;
							}
						}
						echo '<div class="cspl_row clearfix"><div class="csplPhoto">', $product->DefaultImage('small'), '</div><div class="csplName"><h4>', $this->InputSafeString($product->details['prodname']), '</h4>';
						if ($product_details['avid'])
						{	foreach ($product_details['avid'] as $avid)
							{	$att_value = new ProductAttributeValue($avid);
								if (!$attributes[$att_value->details['attid']])
								{	$attributes[$att_value->details['attid']] = new ProductAttribute($att_value->details['attid']);
								}
								echo '<p><label>', $this->InputSafeString($attributes[$att_value->details['attid']]->details['attname']), '</label>: ', $this->InputSafeString($att_value->details['avname']), '</p>';
							}
						}
						if ($product->details['addinfo'] && $product_details['addinfo'])
						{	echo '<p><label>', $this->InputSafeString($product->details['addinfo']), '</label>: ', $this->InputSafeString($product_details['addinfo']), '</p>';
						}
						$price_display = array();
						$prod_cost = 0;
						foreach ($product_prices as $p=>$q)
						{	$price_display[] = (int)$q . ' @ &pound;' . number_format($p, 2);
							$prod_cost += $p * $q;
						}
						echo '</div><div class="csplQty">', implode('<br />', $price_display), '</div><div class="csplPrice">&pound;', number_format($prod_cost, 2), '</div><div class="csplActionsContainer"><div class="csplActions"><a href="', SITE_URL, 'cart.php?prod_plus=', $cart_item_id, '"><img alt="plus 1" src="', SITE_URL, 'img/template/fflist_plus.png"></a><a href="', SITE_URL, 'cart.php?prod_minus=', $cart_item_id, '"><img alt="plus 1" src="', SITE_URL, 'img/template/fflist_minus.png"></a></div><div class="csplRemove"><a class="cartButton" href="', SITE_URL, 'cart.php?prod_remove=', $cart_item_id, '">remove</a></div></div></div>';
						$total_cost += $prod_cost;
					}
				}
			}
			$original_total = $total_cost;
			if ($discount)
			{	$total_cost = $discount->ApplyDiscount($total_cost);
			}
			echo '<div class="clear"></div><p class="cstAddLink"><a class="cartButton" href="', SITE_URL, 'products.php">', $_SESSION['cart_products'] ? 'Continue shopping' : 'Go to shop', '</a><div class="clear"></div></p></div>';
			echo '<div class="cart_discount"><form action="', SITE_URL, 'cart.php" method="post"><label for="cpDiscCode">Promo code</label><input type="text" name="disccode" id="cpDiscCode" value="', $this->InputSafeString($_SESSION['cart_disccode']), '" /><input class="cartButton" type="submit" value="apply promo code" /></form></div>';
			echo '<div class="cart_totals"><h3>Total Price</h3><span class="cartBlockPrice"><span><span>&pound;', PlateBuilderUtilities::formatCost($total_cost), '</span></span></span>';
			if ($discount_amount = $original_total - $total_cost)
			{	echo '<div class="cart_discount_total">with &pound;', number_format($discount_amount, 2), ' discount</div>';
			}
			echo '<div class="cart_buttons"><a class="cartButton" href="', SITE_URL, $cart_plates ? 'cart_docs' : 'cart_addr', '.php">Proceed to checkout</a></div><div class="clear"></div></div>';
		} else
		{	echo '<p class="cartempty">There is nothing in your cart</p>';
		}
	//	$this->VarDump($_SESSION);
		return ob_get_clean();
	} // end of fn CartBody

} // end of defn CartViewPage

$page = new CartViewPage();
$page->Page();
?>