<?php
require_once('init.php');

class CartPageAjax extends CartPage
{

	public function __construct()
	{	parent::__construct($dummy, 1);

		switch ($_GET['action'])
		{	case 'removedonation':
				$output = array('test'=>'');
				switch ($_GET['dontype'])
				{	case 'main':
						$this->cart->RemoveMainDonationFromCart($_GET['donkey']);
						break;
					case 'crstars':
						$this->cart->RemoveCRDonationFromCart($_GET['donkey']);
						break;
				}
				$output['cartlist'] = $this->CartStageContentInner();
				$this->AdjustStagesList();
				$output['breadcrumbs'] = $this->BreadcrumbsInner();
				$basket = $this->cart->PopUpBasket();
				$output['basket_desktop'] = $basket['desktop'];
				$output['basket_mobile'] = $basket['mobile'];
				echo json_encode($output);
				break;
			case 'alterdonation':
				$output = array('test'=>'');
				
				$this->cart->AlterQuantityMainDonation($_GET['donkey'], $_GET['quantity']);
				
				$output['cartlist'] = $this->CartStageContentInner();
				$this->AdjustStagesList();
				$output['breadcrumbs'] = $this->BreadcrumbsInner();
				$basket = $this->cart->PopUpBasket();
				$output['basket_desktop'] = $basket['desktop'];
				$output['basket_mobile'] = $basket['mobile'];
				echo json_encode($output);
				break;
		}

	} //  end of fn __construct

	protected function PageAssign(&$page){}

} // end of class CartPageAjax

$page = new CartPageAjax();
?>