<?php 
require_once('init.php');

class InfusionsoftFormAjax extends Base
{
	public function __construct()
	{	parent::__construct();
		$inf_form = new InfusionSoftContactForm();
		$saved = $inf_form->Save($_POST);
		if (SITE_TEST)
		{	$saved['successmessage'] = 'Test success';
		}
		if ($saved['successmessage'])
		{	echo '<div class="infFormSaved">', $saved['successmessage'], '</div>';
		} else
		{	if ($saved['failmessage'])
			{	echo '<div class="failmessage">', $saved['failmessage'], '</div>';
			}
			switch ($_POST['formtype'])
			{	case 'full':
					echo $inf_form->Form($_POST);
					break;
				case 'minimal':
					echo $inf_form->MinimalSectionForm($_POST);
					break;
			}
		}
		//echo $inf_form->Form($_POST);
		//$this->VarDump($_POST);
		
	} // end of fn __construct
	
} // end of class InfusionsoftFormAjax

$page = new InfusionsoftFormAjax();
?>