<?php
require_once('init.php');

class CRStarsPasswordPage extends MyCRStarsPage
{
	public function __construct()
	{	parent::__construct('password');
		if (isset($_POST['pword']))
		{	$saved = $this->camp_customer->ChangePassword($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}

	} // end of fn __construct

	function MemberBodyContent()
	{	ob_start();
		echo '<div class="container crstarsRegister"><div class="container_inner"><h1 class="crstarsMyHeader">Change My Password</h1><form action="" method="post"><p><label>Existing password</label><input type="password" name="oldpword" value="" /></p><p><label>Your new password</label><input type="password" name="pword" value="" /></p>
			<p><label>Please retype your new password</label><input type="password" name="rtpword" value="" /></p>
			<p class="regformButtons"><button type="submit">Save My New Password</button></p></form></div></div>';
		return ob_get_clean();
	} // end of fn MemberBodyContent

} // end of class CRStarsPasswordPage

$page = new CRStarsPasswordPage();
$page->Page();
?>