$(document).ready(function () {
      $(".scroll-to-widget").click(function(){
        $('html, body').animate({
          scrollTop: $("#donation-widget-box").offset().top - 40
        }, 1000,function(){
          $("#username9").focus();
        });
      });

      $("[data-autofill-amount]").click(function(){
        var val = parseFloat($(this).attr('data-autofill-amount'))||0;
        $("#quantityprice").val((val).toFixed(2));
        var cents = val * 100; // convert to cents
        StripeCheckout.__app.configurations.button0.amount = cents;
        $("#donationammount").val(cents);
      });

      $(window).scroll(function() {    
          var scroll = $(window).scrollTop();
          if (scroll >= 500) {
              //clearHeader, not clearheader - caps H
              $("header.header").addClass("solid-header");
          }else{
              $("header.header").removeClass("solid-header");
          }
      }); //missing );
});