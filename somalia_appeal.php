<?php 
require_once('init.php');

class SomaliaAppealPage extends IndexPage
{
	public function __construct($pageName = 'somalia-appeal')
	{	parent::__construct($pageName);
		$this->css['homepage_somalia.css'] = 'homepage_somalia.css';
	} // end of fn __construct

	function MainBodyContent()
	{	$this->SomaliaMemberBody();
	} // end of fn MemberBody

} // end of class SomaliaAppealPage

$page = new SomaliaAppealPage();
$page->Page();
?>