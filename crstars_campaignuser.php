<?php
require_once('init.php');

class CRStarsCampaignUserPage extends CRStarsPage
{	private $campaignuser;

	public function __construct()
	{	parent::__construct();
	} // end of fn __construct

	protected function BaseConstructFunctions()
	{	$this->campaignuser = new CampaignUser($_GET['cuid']);
		$this->canonical_link = $this->campaignuser->Link();
	} // end of fn BaseConstructFunctions

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		$this->campaignuser->SetFBMeta($this->fb_meta);
	} // end of fn SetFBMeta

	function MainBodyContent()
	{	$fullname = $this->campaignuser->FullName();
		$total_raised = array();
		$total_raised_gbp = 0;
		$campaigns = array();
		$membercampaigns = array();
		if ($campaign_rows = $this->campaignuser->GetCampaigns())
		{	$campaign_count = count($campaign_rows);
			foreach ($campaign_rows as $campaign_row)
			{	$campaigns[$campaign_row['cid']] = new Campaign($campaign_row);
				$campaigns[$campaign_row['cid']]->raised = $campaigns[$campaign_row['cid']]->GetDonationTotal();
				if ($campaigns[$campaign_row['cid']]->team)
				{	$membercampaigns[$campaigns[$campaign_row['cid']]->team['cid']] = $campaigns[$campaign_row['cid']]->team;
				}
				$total_raised[$campaign_row['currency']] += $campaigns[$campaign_row['cid']]->raised;
				if ($campaign_row['currency'] == 'GBP')
				{	$total_raised_gbp += $campaigns[$campaign_row['cid']]->raised;
				} else
				{	$total_raised_gbp += round($campaigns[$campaign_row['cid']]->raised / $this->GetCurrency($campaign_row['currency'], 'convertrate'), 2);
				}
			}
		}
		echo '<div class="container cr-stars-dashboard-header"><div class="container_inner">';
			echo '<div class="cr-stars-dashboard-header-inner cr-stars-row">';
				echo '<div class="cr-stars-row-half">';
					echo '<div class="cr-stars-dashboard-header-left-container">';
						echo '<div class="cr-stars-dashboard-header-left-left">';
							echo '<div class="cr-stars-dashboard-header-left-image">';
								if(!$src = $this->campaignuser->GetImageSRC('large')){
									$src = SITE_URL .'img/template/avatar.png';
								}

								echo '<img src="'.$src.'" alt="'.$fullname.'" title="'.$fullname.'" />';
							echo '</div>';
						echo '</div>';
						echo '<div class="cr-stars-dashboard-header-left-right">';
							echo '<div class="cr-stars-dashboard-header-left-name">';
								echo '<h1>'.$fullname.'</h1>';
							echo '</div>';
							echo '<div class="cr-stars-dashboard-header-left-raised">';
								echo '<div class="cr-stars-dashboard-header-raised-bar">';
									echo '<span class="raised-title">raised</span><span class="raised-amount"> ';
									if (count($total_raised) > 1)
									{	echo '&pound;', number_format($total_raised_gbp);
									} else
									{	foreach ($total_raised as $currency=>$raised)
										echo $this->GetCurrency($currency, 'cursymbol'), number_format($raised);
									}
									echo '</span>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '<div class="clear"></div></div>';
				echo '</div>';
				echo '<div class="cr-stars-row-half">';
					echo '<div class="cr-stars-dashboard-header-right-container">';
						echo '<div class="cr-stars-dashboard-header-search">';
							echo '<form id="crSearchDashboardHeader" method="get" action="'.SITE_URL.'cr-stars/search/"><input type="text" placeholder="Search CR Stars Campaigns..." name="crs_search" value="" required><button type="submit" class="icon-search">&nbsp;</button></form>';
						echo '</div>';
						echo '<div class="clear"></div>';
						echo '<div class="cr-stars-dashboard-header-right-bottom-container">';
							echo '<div class="cr-stars-dashboard-header-right-full">';
								echo '<div class="cr-stars-dashboard-header-campaign-count"><div class="cr-stars-dashboard-header-campaign-count-title">campaigns</div><div class="cr-stars-dashboard-header-campaign-count-count"><span>'.$campaign_count.'</span></div></div>';
							echo '</div>';

						echo '<div class="clear"></div></div>';
					echo '<div class="clear"></div></div>';
				echo '</div>';
			echo '<div class="clear"></div></div>';
		echo '</div></div>';
		/*
		echo '<div class="container campuserTop"><div class="container_inner"><div class="campuserTopLeft"><div class="campuserTopLeftImage"><div>';
		if ($src = $this->campaignuser->GetImageSRC('large'))
		{	echo '<img src="', $src, '" alt="', $fullname, '" title="', $fullname, '" />';
		}
		echo '</div></div><div class="campuserTopLeftText"><h1>', $fullname, '</h1><div class="campuserAmountBar"><span class="campuserAmountBarLabel">Raised</span><span class="campuserAmountBarAmount">';
		if (count($total_raised) > 1)
		{	echo '&pound;', number_format($total_raised_gbp);
		} else
		{	foreach ($total_raised as $currency=>$raised)
			echo $this->GetCurrency($currency, 'cursymbol'), number_format($raised);
		}
		echo '</span></div></div></div><div class="campuserTopRight hidden-xs"></div><div class="clear"></div>
			</div></div>';

		*/
		echo '<div class="container campuserMain"><div class="container_inner"><div class="campuserMainLeft">';
		if ($campaigns)
		{	echo '<div class="campuserMainHeader">', $this->InputSafeString($this->campaignuser->details['firstname']), '\'s Campaign', ($ccount = count($campaigns)) == 1 ? '' : 's', ' (', $ccount, ')</div>', $this->SideListCampaignsList($campaigns, false, true);
		}
		if ($membercampaigns)
		{
			echo '<div class="campuserMainHeader">', $this->InputSafeString($this->campaignuser->details['firstname']), ' is a member of ', $mccount = count($membercampaigns), ' Campaign', $mccount == 1 ? '' : 's', '</div>', $this->SideListCampaignsList($membercampaigns);
		}
		echo '</div>
				<div class="campuserMainRight">
					<div class="campuserMainHeader">', $this->InputSafeString($this->campaignuser->details['firstname']), ' is following ', $fccount = count($followed = $this->campaignuser->GetFollowedCampaigns()), ' Campaign', $fccount == 1 ? '' : 's', '</div>
					';
		if ($followed)
		{	echo $this->SideListCampaignsList($followed);
		} else
		{	echo '<div class="campuserMainContent campuserMainHolding">No campaigns followed</div>';
		}
		echo /*'<div class="campuserMainHeader">Right bar header</div><div class="campuserMainContent">';
		//$this->VarDump($followed);
		//$this->VarDump($this->campaignuser->GetCampaigns());
		//$this->VarDump($this->campaignuser);
		echo '</div>',*/
				'</div><div class="clear"></div></div></div>';
	} // end of fn MemberBody

} // end of class CRStarsCampaignUserPage

$page = new CRStarsCampaignUserPage();
$page->Page();
?>