<?php
class Donation extends BlankItem
{	public $payments = array();
	public $extrafields = array();

	public function __construct($id = 0)
	{	parent::__construct($id, 'donations', 'did');
	} // fn __construct

	public function ID($adddate = true)
	{	ob_start();
		switch($this->details['donationtype'])
		{	case 'oneoff':
				echo 'CR_ONLINE_', str_pad($this->id, 7, '0', STR_PAD_LEFT);
				break;
			case 'monthly':
				echo 'CR_WEB_', str_pad($this->id, 7, '0', STR_PAD_LEFT), $adddate ? date(' m/y ', strtotime($this->details['created'])) : '', $this->details['giftaid'] ? ' GA' : '', $this->details['zakat'] ? ' Z' : '';
				break;
			case 'manual':
				echo 'CR_MANUAL_', str_pad($this->id, 7, '0', STR_PAD_LEFT);
				break;
		}
		return ob_get_clean();
	} // fn ID

	public function Status()
	{	if ($this->details['donationref'])
		{	if ($this->GetRefund())
			{	return 'REFUNDED';
			} else
			{	return 'COMPLETED';
			}
		} else
		{	return 'PENDING';
		}
	} // fn Status

	public function ResetExtra()
	{	$this->payments = array();
		$this->extrafields = array();
	} // fn ResetExtra

	public function GetExtra()
	{	if ($this->id)
		{	$sql = 'SELECT donationpayments.* FROM donationpayments WHERE donationref="' . $this->details['donationref'] . '" ORDER BY paydate ASC';
			$this->payments = $this->db->ResultsArrayFromSQL($sql, 'dpid', true);
			$sql = 'SELECT donation_extras.* FROM donation_extras WHERE did=' . $this->id;
			$this->extrafields = array();
			if ($result = $this->db->Query($sql))
			{	while ($row = $this->db->FetchArray($result))
				{	$this->extrafields[$row['extrafield']] = $row['extravalue'];
				}
			}
		}
	} // fn GetExtra

	function AddDetailsForLang($lang = ''){}

	public function SumPaid()
	{	$paid = array('amount'=>0, 'gbpamount'=>0, 'adminamount'=>0, 'gbpadminamount'=>0);
		foreach ($this->payments as $payment)
		{	//$paid['amount'] += $payment['amount'];
			//$paid['gbpamount'] += $payment['gbpamount'];
			//$paid['adminamount'] += $payment['adminamount'];
			//$paid['gbpadminamount'] += $payment['gbpadminamount'];
			
			$paid['amount'] += $this->details['amount'];
			$paid['adminamount'] += $this->details['adminamount'];
			$part_pmt = $this->details['amount'] / $payment['amount'];
			$paid['gbpamount'] += round($payment['gbpamount'] * $part_pmt, 2);
			$paid['gbpadminamount'] += round($payment['gbpadminamount'] * $part_pmt, 2);
			
			break;
		}
		return $paid;
	} // fn SumPaid

	public function SumPaidGBP()
	{	$paid = 0;
		foreach ($this->payments as $payment)
		{	$paid += $payment['amountgbp'];
		}
		return $paid;
	} // fn SumPaidGBP

	public function GetDonationTypes()
	{	$donation_types = array();
		$donation_types['oneoff'] = array('label'=>'One-off', 'allowadmin'=>true);
		if (true || isset($_SESSION[SITE_NAME]['auserid']) || ($_SERVER['REMOTE_ADDR'] == '82.219.14.145'))
		{	$donation_types['monthly'] = array('label'=>'Monthly (UK only)', 'allowadmin'=>true);
		} else
		{	/*ob_start();
			print_r($_SESSION);
			echo SITE_NAME, ' - ', $_SERVER['REMOTE_ADDR'];
			mail('tim@websquare.co.uk', 'cr diag', ob_get_clean());*/
		}
		return $donation_types;
	} // end of fn GetDonationTypes

	public function GetDonationType($type = '')
	{	$donationtypes = $this->GetDonationTypes();
		return $donationtypes[$type];
	} // end of fn GetDonationType

	public function ThankYouLink()
	{	$thankyoupage = new PageContent('donate-thank-you');
		return $thankyoupage->FullLink();
	} // end of fn ThankYouLink

	public function CanBePaid()
	{	return $this->id && !$this->details['donationref'];
	} // end of fn CanBePaid

	public function CanBeCollectedMonthly()
	{	if ($this->id && $this->details['donationref'] && ($this->details['donationtype'] == 'monthly') && !$this->details['cancelled'] && $this->extrafields['StartDate'])
		{	return true;
		}
		return false;
	} // end of fn CanBePaid

	public function TotalPaid($include_admin = true)
	{	$total = 0;
		if ($this->payments)
		{	foreach ($this->payments as $payment)
			{	$total += $payment['amount'];
				if ($include_admin)
				{	$total += $payment['adminamount'];
				}
			}
		}
		return $total;
	} // end of fn TotalPaid

	public function TotalPaidGBP($include_admin = true)
	{	$total = 0;
		if ($this->payments)
		{	foreach ($this->payments as $payment)
			{	$total += $payment['gbpamount'];
				if ($include_admin)
				{	$total += $payment['gbpadminamount'];
				}
			}
		}
		return $total;
	} // end of fn TotalPaidGBP

	public function NextMonthlyPaymentDate()
	{	if ($this->CanBeCollectedMonthly())
		{	// check existing payments
			if (($payments = $this->payments) && ($lastpayment = array_pop($payments)))
			{	$next = $this->datefn->SQLDate($nextstamp = strtotime(substr($lastpayment['paydate'], 0, 10) . ' +1 month'));
			} else
			{	$next = $this->extrafields['StartDate'];
			}
			if (!in_array($dayofmonth = (int)date('j', $nextstamp), array(1, 15)))
			{	if ($dayofmonth > 15)
				{	$next = $this->datefn->SQLDate(strtotime($next . ' -' . ($dayofmonth - 15) . ' days'));
				} else
				{	if ($dayofmonth > 1)
					{	$next = $this->datefn->SQLDate(strtotime($next . ' -' . ($dayofmonth - 15) . ' days'));
					}
				}
			}
			return $next;
		}
		return '';
	} // end of fn NextMonthlyPaymentDate

	public function RequestDDPayment($date = '')
	{	$fail = array();
		$success = array();

		if ($date = $this->datefn->SQLDate(strtotime($date)))
		{	// then take payment
			$ez = new EazyCollectAPI();
			$result = $ez->AddPaymentRequest($this, $date);
			if ($result['return']['PaymentID'])
			{	// create payment record
				$fields = array();
				$fields['requestid'] = 'requestid="' . $this->SQLSafe($result['return']['PaymentID']) . '"';
				$fields['requestdate'] = 'requestdate="' . $this->datefn->SQLDateTime() . '"';
				$fields['responseexpected'] = 'responseexpected="' . $date . '"';
				$fields['did'] = 'did=' . $this->id;
				$fields['amount'] = 'amount=' . $this->details['amount'];

				$sql = 'INSERT INTO donation_ddrequests SET ' . implode(', ', $fields);
				if ($result = $this->db->Query($sql))
				{	$success[] = 'Payment requested';
				} else
				{	$fail[] = $this->db->Error();
				}
			} else
			{	if ($result['failmessage'])
				{	$fail[] = $result['failmessage'];
				}
			}
		} else
		{	$fail[] = 'no payment date';
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn RequestDDPayment

	public function RecordDDPayment()
	{	$fail = array();
		$success = array();

		if ($nextdate = $this->NextMonthlyPaymentDate())
		{	if ($nextdate < $this->datefn->SQLDate())
			{	// then take payment
				$ez = new EazyCollectAPI();
				$result = $ez->AddPaymentRequest($this);
				if ($result['return']['PaymentID'])
				{	// create payment record
					$fields = array();
					$fields['requestid'] = 'requestid="' . $this->SQLSafe($result['return']['PaymentID']) . '"';
					$fields['did'] = 'did=' . $this->id;
					$fields['amount'] = 'amount=' . $this->details['amount'];
					if ($this->details['currency'] == 'GBP')
					{	$fields['gbpamount'] = 'gbpamount=' . $this->details['amount'];
					} else
					{	$fields['gbpamount'] = 'gbpamount=' . round($this->details['amount'] / $this->GetCurrency($this->details['currency'], 'convertrate'), 2);
					}


					if ($result['successmessage'])
					{	$success[] = $result['successmessage'];
					}
				} else
				{	if ($result['failmessage'])
					{	$fail[] = $result['failmessage'];
					}
				}
			} else
			{	$fail[] = 'payment not due until ' . date('j M Y', strtotime($nextdate));
			}
		} else
		{	$fail[] = 'no payment due';
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn RecordDDPayment

	public function PaymentButton()
	{	ob_start();
		switch ($this->details['donationtype'])
		{	case 'monthly':
				echo 'not ready for DD yet';
				break;
			case 'oneoff':
				$wp = new WorldPayRedirect();
				echo $wp->OneOffDonationButton($this);
				break;
		}
		return ob_get_clean();
	} // end of fn PaymentButton

	public function GatewayTitle()
	{	ob_start();
		if ($this->details['adminamount'] > 0)
		{	echo ' (including ', $cursymbol = $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['adminamount']), ' for our administration costs)';
		}
		if ($inner = $this->GatewayTitleInner())
		{	echo ' for ', $inner;
		}
		switch ($this->details['donationtype'])
		{	case 'monthly':
				echo ' every month';
				break;
		}
		return trim(ob_get_clean());
	} // end of fn GatewayTitle

	public function GatewayTitleInner()
	{	ob_start();
		if (($countries = $this->GetDonationCountries()) && ($country = $countries[$this->details['donationcountry']]))
		{	echo $this->InputSafeString($country['shortname']);
			if ($project = $country['projects'][$this->details['donationproject']])
			{	echo ', ', $this->InputSafeString($project['projectname']);
				if ($project2 = $project['projects'][$this->details['donationproject2']])
				{	echo ', ', $this->InputSafeString($project2['projectname']);
					if ($this->details['quantity'])
					{	echo ' x ', $this->details['quantity'];
					}
				} else
				{	if ($this->details['quantity'])
					{	echo ' x ', $this->details['quantity'];
					}
				}
			} else
			{	if ($this->details['quantity'])
				{	echo ' x ', $this->details['quantity'];
				}
			}
		}
		return trim(ob_get_clean());
	} // end of fn GatewayTitle

	public function NextWorkingDay($starttime = '', $days_on = 10)
	{	if ($starttime)
		{	if ((int)$starttime != $starttime)
			{	$starttime = strtotime($starttime);
			}
		} else
		{	$starttime = time();
		}

		$endtime = $starttime + ($this->datefn->secInHour * 6);
		if ($days_on = (int)$days_on)
		{	$workingdays = 0;
			$bankhols = $this->GetBankHols();
			while ($workingdays < $days_on)
			{	$endtime += $this->datefn->secInDay;
				if ((date('N', $endtime) < 6) && !$bankhols[$this->datefn->SQLDate($endtime)])
				{	$workingdays++;
				}
			}
		}
		return $endtime;
	} // end of fn NextWorkingDay

	public function ConfirmDirectDebit(&$session, $send_confirmation = true)
	{	$fail = array();
		$success = array();
		$fields = array();
		$extras = array();

		if ($session['ezc_ContractID'])
		{	$fields['donationconfirmed'] = 'donationconfirmed="' . $this->datefn->SQLDateTime() . '"';
			$extras['ContractID'] = $session['ezc_ContractID'];
		} else
		{	$fail[] = 'Contract ID missing';
		}

		if ($session['ezc_CustomerID'])
		{	$extras['CustomerID'] = $session['ezc_CustomerID'];
		} else
		{	$fail[] = 'Customer ID missing';
		}

		if ($session['ezc_DirectDebitRef'])
		{	$fields['donationref'] = 'donationref="' . $this->SQLSafe($this->EZCRefFromContactID($session['ezc_DirectDebitRef'])) . '"';
			$extras['DirectDebitRef'] = $session['ezc_DirectDebitRef'];
		} else
		{	$fail[] = 'DirectDebitRef missing';
		}

		if ($session['ezc_StartDate'])
		{	$extras['StartDate'] = $this->datefn->SQLDate($session['ezc_StartDate']);
			$extras['StartDay'] = date('j', $session['ezc_StartDate']);
		} else
		{	$fail[] = 'Start date missing';
		}

		$extras['AccountHolder'] = $session['dd_accountname'];
		$extras['AccountNumber'] = $session['dd_accountnumber'];
		$extras['AccountSortCode'] = $session['dd_accountsortcode'];
		
		if (!$fail)
		{	$sql = 'UPDATE donations SET ' . implode(', ', $fields) . ' WHERE did=' . $this->id;
			if (($result = $this->db->Query($sql)) && $this->db->AffectedRows())
			{	foreach ($extras as $name=>$value)
				{	$extras_sql = 'INSERT INTO donation_extras SET did=' . $this->id  . ', extrafield="' . $this->SQLSafe($name) . '", extravalue="' . $this->SQLSafe($value) . '"';
					$this->db->Query($extras_sql);
				}
				$this->Get($this->id);
				$success[] = 'Direct debit created';
				if ($send_confirmation)
				{	$this->SendDirectDebitEmailConfirmation();
					$this->SendDirectDebitEmailConfirmationToAdmin();
				}
				$if = new InfusionSoftDonation();
				$if->ConfirmDirectDebit($this);
			} else
			{	$fail[] = 'db update failed';
			}
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn ConfirmDirectDebit

	public function IsMonthlyUnconfirmed()
	{	return !$this->details['donationref'] && ($this->details['donationtype'] == 'monthly');
	} // end of fn IsMonthlyUnconfirmed

	public function RecordUnpaidDirectDebit(&$session)
	{	$fail = array();
		$success = array();
		$extras = array();

		$extras['AccountHolder'] = $session['dd_accountname'];
		$extras['AccountNumber'] = $session['dd_accountnumber'];
		$extras['AccountSortCode'] = $session['dd_accountsortcode'];
	
		$startdate = $this->NextWorkingDay();
		$paymentDayInMonth = date('j', $startdate);
		if ($paymentDayInMonth <= 15)
		{	$startdate = mktime(0,0,0,date('n', $startdate), $paymentDayInMonth = 15, date('Y', $startdate));
		} else
		{	$startdate = mktime(0,0,0,date('n', $startdate) + 1, $paymentDayInMonth = 1, date('Y', $startdate));
		}
		
		$extras['StartDate'] = $this->datefn->SQLDate($startdate);
		$extras['StartDay'] = $paymentDayInMonth;

		foreach ($extras as $name=>$value)
		{	$extras_sql = 'INSERT INTO donation_extras SET did=' . $this->id  . ', extrafield="' . $this->SQLSafe($name) . '", extravalue="' . $this->SQLSafe($value) . '"';
			$this->db->Query($extras_sql);
		}

		$this->Get($this->id);
		$this->SendDirectDebitForm();
		$success[] = 'Direct debit record created';

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn RecordUnpaidDirectDebit

	public function SendDirectDebitForm()
	{	ob_start();
		echo '<p>In order to set up the direct debit for your monthly donation to Charity Right please print and complete the following form. This should then be sent to the address on the form.</p>';
		$body = ob_get_clean();
		$mail = new HTMLMail();
		$mail->SetSubject('The Next Step in Your Direct Debit Setup');
		$mail->SendFromTemplate($this->details['donoremail'], array('main_content'=>$body), 'default', array(array('text'=>$this->DirectDebitFormPDFContents(), 'name'=>'charity_right_direct_debit.pdf')));
	} // end of fn SendDirectDebitForm

	public function BuildPDFAttachment()
	{	ob_start();
		echo 'Content-Type: application/pdf; name="charity_right_directdebit.pdf"
Content-Transfer-Encoding: base64
Content-Disposition: inline;  filename="charity_right_directdebit.pdf"

';
		echo chunk_split(base64_encode($this->DirectDebitFormPDFContents()));
		return ob_get_clean();
	} // end of fn BuildPDFAttachment

	public function DirectDebitFormPDFContents()
	{	include_once(CITDOC_ROOT . '/html2pdf/html2pdf.class.php');
		//return 'dfdsfdsf';
		$html2pdf = new HTML2PDF('P', 'A4', 'en');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($this->DirectDebitFormHTMLContents());
		return $html2pdf->Output('', 'S');
	} // end of fn DirectDebitFormPDFContents

	public function DirectDebitFormPDFToFile()
	{	include_once(CITDOC_ROOT . '/html2pdf/html2pdf.class.php');
		$html2pdf = new HTML2PDF('P', 'A4', 'en');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($this->DirectDebitFormHTMLContents());
		return $html2pdf->Output(CITDOC_ROOT . '/mail_log/' . time() . '.pdf', 'F');
	} // end of fn DirectDebitFormPDFContents

	public function DirectDebitFormHTMLContents()
	{	ob_start();
		if (SITE_TEST)
		{	$inc_root = 'https://charityright.org.uk/';
		} else
		{	$inc_root = SITE_URL;
		}
		$inc_root_img = $inc_root . 'img/ddpdf/';
		$inc_root_img = 'https://purematrimony.com/img/cright/';
		echo '
			<style type="text/css">
			*
			{	font-size: 11px;
				-webkit-box-sizing: border-box;
				-moz-box-sizing: border-box;
				box-sizing: border-box;
			}
			.clear
			{	clear: both;
			}
			.number
			{	text-align: right;
			}
			.centre
			{	text-align: center;
			}
			ul
			{	list-style: circle; width: 170mm;
			}
			ul li
			{	padding: 5px 0px 5px 15px;
			}
			table.topTable
			{	border: 2px solid #000;
			}
			table.topTable td.topTableDiv
			{	width: 90mm; border: 1px solid #666; vertical-align: top; padding: 5px;
			}
			table.topTable td.topTableDiv.topTableCustomer
			{	height: 70mm;
			}
			table.topTable td.topTableDiv.topTableDD
			{	height: 40mm;
			}
			td.customerInfoBox
			{	padding: 5px 10px;
				border: 1px solid #666;
				background: #eee;
				color: #000;
			}
			td.customerInfoBox span
			{	color: #666;
			}
			.customerInfoHeader
			{	font-weight: bold;
			}
			td.customerInfoBox.customerInfoBoxCheck
			{	width: 35px;
				height: 20px;
				padding: 2px;
			}
			.addressBox
			{	margin: 10px 30px; background: #EEE; padding: 10px 30px;
			}
			.addressBox p
			{	padding: 5px 0px;
			}
			.bankDetailsTable
			{	width: 100%;
				border: 2px solid #000;
			}
			.bankDetailsTable .bankDetailsTableDiv
			{	width: 50%;
				padding: 10px;
			}
			.bankDetailsTable .bankDetailsTableDiv .bankDetailsTableDivLabel
			{	padding-top: 10px;
			}
			.bankDetailsTable .bankDetailsTableDiv tr:first-child .bankDetailsTableDivLabel
			{	padding-top: 0px;
			}
			</style>
			<page>
			<table class="topTable">
				<tr>
					<td class="topTableDiv topTableCustomer">
						<table style="width: 100%;">
							<tr>
								<td class="customerInfoHeader">Customer Ref:</td><td class="customerInfoBox">', $this->ID(), '</td>
							</tr>
							<tr>
								<td colspan="2" class="customerInfoBox"><span>Customer name: </span>', $this->InputSafeString($this->details['donorfirstname'] . ' ' . $this->details['donorsurname']), '</td>
							</tr>
							<tr>
								<td colspan="2" class="customerInfoBox">
									<table style="border-collapse: collapse;"><tr><td><span>Customer address: </span></td><td style="width: 55mm;">', $this->AddressOutput(), '</td></tr></table>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="customerInfoBox"><span>Email: </span>', $this->InputSafeString($this->details['donoremail']), '</td>
							</tr>
							<tr>
								<td colspan="2" class="customerInfoBox" style="width: 100%;"><span>Phone: </span>', $this->InputSafeString($this->details['donorphone']), '</td>
							</tr>
						</table>
					</td>
					<td class="topTableDiv" rowspan="2" style="padding-top: 20px;"><div style="text-align: center;"><img src=\'', $inc_root_img, 'logo_pink240.png\' /></div><div class="addressBox"><p>', nl2br($this->InputSafeString($this->GetParameter('compaddress'))), '</p><p>tel: ', $this->InputSafeString($this->GetParameter('compphone')), '</p></div><div style="padding: 5px 30px;">Please complete your details, sign the Direct Debit Instruction and return to Charity Right at the address above</div></td>
				</tr>
				<tr><td class="topTableDiv topTableDD"><table style="width: 100%;">
				<tr><td style="width: 55%;">1st direct debit amount</td><td style="width: 45%;" class="customerInfoBox number">', $currency = $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['amount'] + $this->details['adminamount'], 2, '.', ''), '</td></tr>',
					'<tr><td>Subsequent direct debit amount</td><td class="customerInfoBox number">', $currency, number_format($this->details['amount'] + $this->details['adminamount'], 2, '.', ''), '</td></tr>
					<tr><td>Payment date</td><td><table><tr><td>1st</td><td class="customerInfoBox customerInfoBoxCheck customerInfoBoxCheck"><img src=\'', $inc_root_img, ($this->extrafields['StartDay'] == 1) ? 'check_checked' : 'check_blank', '.jpg\' /></td><td style="width:20px;"></td><td>15th</td><td class="customerInfoBox customerInfoBoxCheck"><img src=\'', $inc_root_img, ($this->extrafields['StartDay'] == 15) ? 'check_checked' : 'check_blank', '.jpg\' /></td></tr></table></td></tr>',
					'<tr><td>DD start date</td><td class="customerInfoBox centre">', date('d-M-Y', strtotime($this->extrafields['StartDate'])), '</td></tr>
					<tr><td>Frequency</td><td class="customerInfoBox centre">Monthly</td></tr>
					<tr><td>Total number of payments</td><td class="customerInfoBox centre">Continuous</td></tr>
				</table></td></tr>
			</table>';
		//return ob_get_clean();
		echo '<table style="width: 190mm;">
				<tr><td style="width: 20%;">&nbsp;</td><td style="width: 60%; vertical-align: center; text-align: center; padding: 10px 0px; font-weight: bold;">Instruction to your Bank or Building Society to pay by Direct Debit</td><td style="width: 20%; text-align: right; padding: 10px 10px 10px 0px;"><img src="', $inc_root_img, 'DirectDebitLogo.jpg" height="30" /></td></tr>
				<tr><td colspan="3">
					<table class="bankDetailsTable">
						<tr>
							<td class="bankDetailsTableDiv"><table style="width: 100%;">
								<tr><td class="bankDetailsTableDivLabel" style="width: 100%;">Name(s) of account holder</td></tr>
								<tr><td class="customerInfoBox">', $this->InputSafeString($this->extrafields['AccountHolder']), '</td></tr>
								<tr><td class="bankDetailsTableDivLabel">Bank / Building Society Account Number</td></tr>
								<tr><td><table style="width: 100%; border-collapse: collapse;"><tr>';
		foreach (str_split($this->extrafields['AccountNumber']) as $acnumber)
		{	echo '<td class="customerInfoBox" style="width: 12.5%; text-align: center;">', $acnumber, '</td>';
		}
		echo '</tr></table></td></tr>
								<tr><td class="bankDetailsTableDivLabel">Bank Sort Code</td></tr>
								<tr><td><table style="width: 100%; border-collapse: collapse;"><tr>';
		foreach (str_split($this->extrafields['AccountSortCode']) as $acnumber)
		{	echo '<td class="customerInfoBox" style="width: 16.6%; text-align: center;">', $acnumber, '</td>';
		}
		echo '</tr></table></td></tr>
								<tr><td class="bankDetailsTableDivLabel">Name of your Bank or Building Society</td></tr>
								<tr><td class="customerInfoBox">&nbsp;</td></tr>
								<tr><td class="bankDetailsTableDivLabel">Address of your Bank or Building Society</td></tr>
								<tr><td class="customerInfoBox">&nbsp;</td></tr>
							</table></td>
							<td class="bankDetailsTableDiv"><table style="width: 100%;">
								<tr><td colspan="2" style="width: 100%;">
									<table style="width: 100%; border-collapse: collapse;"><tr><td style="width: 39%;">Service User Number</td>';
		foreach (str_split(EZC_SUN) as $sun_bit)
		{	echo '<td class="customerInfoBox" style="width: 10%; text-align: center;">', $sun_bit, '</td>';
		}
		echo '</tr></table>
								</td></tr>
								<tr><td colspan="2" style="width: 100%;">Instruction to your Bank or Building Society<br />Please pay Eazy Collect Re Charity Right from the account detailed in this instruction subject to the safeguards assured by the Direct Debit Guarantee. I understand that this instruction may remain with Eazy Collect Re Charity Right and, if so, details will be passed electronically to my Bank or Building Society.</td></tr>
								<tr><td colspan="2" class="bankDetailsTableDivLabel">Account Holder (s) Signature (s)</td></tr>
								<tr><td colspan="2" class="customerInfoBox" style="height: 50px;">&nbsp;</td></tr>
								<tr><td style="width: 50%; padding-right: 10px; text-align: right">Date</td><td style="width: 50%;" class="customerInfoBox">&nbsp;</td></tr>
							</table></td>
						</tr>
					</table>
				</td></tr>
				<tr><td colspan="3" style="padding-top: 3px; border-bottom: 1px dotted #333;">&nbsp;</td></tr>
				<tr><td colspan="3" style="padding-top: 3px;"><div style="text-align: center; padding: 5px 0px;">This Guarantee should be detached and retained by the payer</div></td></tr>
				<tr><td style="width: 20%;">&nbsp;</td><td style="width: 60%; vertical-align: bottom; text-align: center; padding: 10px 0px; font-weight: bold;">THE DIRECT DEBIT GUARANTEE</td><td style="width: 20%; text-align: right; padding: 10px 10px 10px 0px;"><img src="', $inc_root_img, 'DirectDebitLogo.jpg" height="30" /></td></tr>
				<tr><td colspan="3" style="padding: 10px 20px 5px 20px;><ul>
					<li>This Guarantee is offered by all Banks and Building Societies that accept instructions to pay Direct Debits.</li>
					<li>If there are any changes to the amount, date or frequency of your Direct Debit, Eazy Collect Re Charity Right will notify you 10 days in advance of your account being debited or as otherwise agreed. If you request Eazy Collect Re Charity Right to collect a payment, confirmation of the amount and date will be given to you at the time of the request.</li>
					<li>If an error is made in the payment of your Direct Debit by either Eazy Collect Re Charity Right or your Bank or Building Society, you are entitled to a full and immediate refund of the amount paid from your Bank or Building Society. - If you receive a refund you are not entitled to, you must pay it back when Eazy Collect Re Charity Right asks you to.</li>
					<li>You can cancel your Direct Debit at any time by simply contacting your Bank or Building Society. Written confirmation may be required. Please also notify us.</li></ul>
				</td></tr>
			</table></page>';
		return ob_get_clean();
	} // end of fn DirectDebitFormHTMLContents

	public function SendDirectDebitEmailConfirmationToAdmin()
	{	if ($email = $this->GetParameter('email_dd'))
		{	ob_start();
			echo '<h3>Confirmation of monthly donation set up</h3>
<p>Donation set up for ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['amount'] + $this->details['adminamount'], 2), ' per month.</p>';
			if ($this->details['zakat'])
			{	echo '<pThis is a Zakat donation</p>';
			}
			echo '<p>Details are as follows</p><ul>
<li>Date: ', date('j M Y', strtotime($this->details['donationconfirmed'])), '</li>
<li>Reference: ', $this->InputSafeString($this->extrafields['DirectDebitRef']), '</li>
<li>Account holder name: ', $this->InputSafeString($this->extrafields['AccountHolder']), '</li>
<li>Account number: ', $this->InputSafeString($this->extrafields['AccountNumber']), '</li>
<li>Account sort code: ', $this->InputSafeString($this->extrafields['AccountSortCode']), '</li>
<li>First payment: On or after ', date('j M Y', strtotime($this->extrafields['StartDate'])), '</li>
<p><a href="', SITE_URL, ADMIN_URL, '/donation.php?id=', $this->id, '">View in admin panel</a></p>';
			$body = ob_get_clean();
			$mail = new HTMLMail();
			$mail->SetSubject('Online Direct Debit Setup');
			$mail->Send($email, $body, strip_tags($body));
		//	$mail->Send('tim@websquare.co.uk', $body, strip_tags($body));
		}
	} // end of fn SendDirectDebitEmailConfirmationToAdmin

	public function DirectDebitEmailText()
	{	$countries = $this->GetDonationCountries();
		return (($project = $countries[$this->details['donationcountry']]['projects'][$this->details['donationproject']]) && $project['emailtext']) ? $this->InputSafeString($project['emailtext']): '';
	} // end of fn DirectDebitEmailText

	public function SendDirectDebitEmailConfirmation()
	{	ob_start();
		echo '<h3>Confirmation of your monthly donation</h3>
<p>Thank you for setting up a Direct Debit for Charity Right for ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['amount'] + $this->details['adminamount'], 2), 'per month ', $this->DirectDebitEmailText(), '.</p>';
		if ($this->details['zakat'])
		{	echo '<p>We appreciate that this is a Zakat donation. We\'d like to reassure you that this donation will be distributed in line with the Islamic principles of Zakat.</p>';
		}
		echo '<p>We can confirm that your Direct Debit details have now been successfully received and will be securely submitted to our appointed Direct Debit Bureau, Eazy Collect Services, for final setup with your bank or building society. You will receive an invoice accordingly for each billing period confirming the amount of Direct Debit to be taken. Please note "Eazy Collect DD" will appear on your bank statement for this direct debit payment.</p>
<p>Your details are as follows</p><ul>
<li>Date: ', date('j M Y', strtotime($this->details['donationconfirmed'])), '</li>
<li>Your reference: ', $this->InputSafeString($this->extrafields['DirectDebitRef']), '</li>
<li>Account holder name: ', $this->InputSafeString($this->extrafields['AccountHolder']), '</li>
<li>Account number: ****', $this->InputSafeString(substr($this->extrafields['AccountNumber'], -4)), '</li>
<li>Account sort code: ', $this->InputSafeString($this->extrafields['AccountSortCode']), '</li>
<li>First payment: On or after ', date('j M Y', strtotime($this->extrafields['StartDate'])), '</li>
<p>Your Direct Debit payments have been set up as follows:</p>
<table style="border-collapse: collapse;"><tr><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">First Amount</th><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">First Collection Date</th><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Subsequent Amounts</th><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Frequency</th><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Number of Payments</th></tr>
<tr><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['amount'], 2), '</td><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">', date('j M Y', strtotime($this->extrafields['StartDate'])), '</td><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['amount'], 2), '</td><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Monthly</td><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Continuous</td></tr></table>
<p>If your payment falls due on a weekend or Bank Holiday, it will be collected on the next working day. If any of these details are incorrect or if you have any questions regarding your Direct Debit, please do not hesitate to contact us on ', $this->InputSafeString($this->GetParameter('compphone')), ' or email ', $this->InputSafeString($this->GetParameter('compemail')), '. However, if your details are correct you need do nothing and your Direct Debit will be processed as normal. You have the right to cancel your Direct Debit at any time.</p>';
		$body = ob_get_clean();
		$mail = new HTMLMail();
		$mail->SetSubject('Confirmation of Direct Debit Setup');
		$mail->SendFromTemplate($this->details['donoremail'], array('main_content'=>$body), 'default');

	} // end of fn SendDirectDebitEmailConfirmation

	public function AddressOutput($sep = '<br />')
	{	$address = array();
		if ($this->details['donoradd1'])
		{	$address[] = $this->InputSafeString($this->details['donoradd1']);
		}
		if ($this->details['donoradd2'])
		{	$address[] = $this->InputSafeString($this->details['donoradd2']);
		}
		if ($this->details['donorcity'])
		{	$address[] = $this->InputSafeString($this->details['donorcity']);
		}
		if ($this->details['donorpostcode'])
		{	$address[] = $this->InputSafeString($this->details['donorpostcode']);
		}
		if ($country = $this->GetCountry($this->details['donorcountry']))
		{	$address[] = $this->InputSafeString($country);
		}
		return implode($sep, $address);
	} // end of fn AddressOutput

	public function EZCRefFromContactID($contractid = '')
	{	return 'EZC~' . $contractid;
	} // end of fn EZCRefFromContactID

	public function CreateFromSession($data = array())
	{
		$fail = array();
		$success = array();
		$extras = array();
		$fields = array('created="' . $this->datefn->SQLDateTime() . '"');

		if ($donationtype = $this->GetDonationType($data['donation_type']))
		{	$fields['donationtype'] = 'donationtype="' . $data['donation_type'] . '"';
			if ($donationtype['allowadmin'])
			{	$fields['adminamount'] = 'adminamount=' . (int)$data['donation_admin'];
			}
		} else
		{	$fail[] = 'type missing';
		}

		if (($amount = round($data['donation_amount'], 2)) > 0)
		{	$fields['amount'] = 'amount=' . $amount;
		} else
		{	$fail[] = 'amount missing';
		}

		if ($this->GetCurrency($data['donation_currency']))
		{	$fields['currency'] = 'currency="' . $data['donation_currency'] . '"';
		} else
		{	$fail[] = 'currency missing';
		}

		if ($donationcountry = $this->SQLSafe($data['donation_country']))
		{	$fields['donationcountry'] = 'donationcountry="' . $donationcountry . '"';
		} else
		{	$fail[] = 'donation country missing';
		}
		if ($donationproject = $this->SQLSafe($data['donation_project']))
		{	$fields['donationproject'] = 'donationproject="' . $donationproject . '"';
		} else
		{	$fail[] = 'donation project missing';
		}
		if ($donationproject2 = $this->SQLSafe($data['donation_project2']))
		{	$fields['donationproject2'] = 'donationproject2="' . $donationproject2 . '"';
		}

		if ($donortitle = $this->SQLSafe($data['donor_title']))
		{	$fields['donortitle'] = 'donortitle="' . $donortitle . '"';
		} else
		{	$fail[] = 'your title is missing';
		}
		if ($donorfirstname = $this->SQLSafe($data['donor_firstname']))
		{	$fields['donorfirstname'] = 'donorfirstname="' . $donorfirstname . '"';
		} else
		{	$fail[] = 'first name missing';
		}
		if ($donorsurname = $this->SQLSafe($data['donor_lastname']))
		{	$fields['donorsurname'] = 'donorsurname="' . $donorsurname . '"';
		} else
		{	$fail[] = 'first name missing';
		}

		if ($data['donor_country'] && $this->GetCountry($data['donor_country']))
		{	$fields['donorcountry'] = 'donorcountry="' . $data['donor_country'] . '"';
			if ($data['donor_country'] == 'GB')
			{	if ($data['donor_giftaid'])
				{	$fields['giftaid'] = 'giftaid=1';
				}
				$add_prefix = 'pca_';
				//$fail[] = $data[$add_prefix . 'donor_address1'];
			}

			if ($donoradd3 = $this->SQLSafe($data[$add_prefix . 'donor_address3']))
			{	$donoradd1lines = array();
				if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
				{	$donoradd1lines[] = $donoradd1;
				}
				if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
				{	$donoradd1lines[] = $donoradd2;
				}
				if ($donoradd1lines)
				{	$fields['donoradd1'] = 'donoradd1="' . implode(', ', $donoradd1lines) . '"';
				}
				$fields['donoradd2'] = 'donoradd2="' . $donoradd3 . '"';
				$address_done++;
			} else
			{	if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
				{	$fields['donoradd1'] = 'donoradd1="' . $donoradd1 . '"';
					$address_done++;
				}
				if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
				{	$fields['donoradd2'] = 'donoradd2="' . $donoradd2 . '"';
					$address_done++;
				}
			}
			if (!$address_done)
			{	$fail[] = 'address missing';
			}

			if ($donorpostcode = $this->SQLSafe($data[$add_prefix . 'donor_postcode']))
			{	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
			} else
			{	$fail[] = 'post code missing';
			}
			if ($donorcity = $this->SQLSafe($data[$add_prefix . 'donor_city']))
			{	$fields['donorcity'] = 'donorcity="' . $donorcity . '"';
			} else
			{	$fail[] = 'city/town missing';
			}
		} else
		{	$fail[] = 'country missing';
		}

		$fields['donation_zakat'] = 'zakat=' . ($data['donation_zakat'] ? 1 : 0);
		$fields['ddsolesig'] = 'ddsolesig=' . ($data['dd_confirm'] ? 0 : 1);
		$fields['quantity'] = 'quantity=' . (int)$data['donation_quantity'];

		if ($donoremail = $this->SQLSafe($data['donor_email']))
		{	$fields['donoremail'] = 'donoremail="' . $donoremail . '"';
		} else
		{	$fail[] = 'email missing';
		}
		$fields['donorphone'] = 'donorphone="' . $this->SQLSafe($data['donor_phone']) . '"';
		$fields['donoranon'] = 'donoranon=' . ($data['donor_anon'] ? 1 : 0);
		
		$fields['contactpref'] = 'contactpref=' . (int)$data['contactpref'];
		$fields['orderid'] = 'orderid=' . (int)$data['orderid'];

		//$fail[] = 'test';

		if (!$fail && ($set = implode(', ', $fields)))
		{	$sql = 'INSERT INTO donations SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
				{	$this->Get($id);
					$this->RecordUTMData();
					$success[] = 'New donation created';
					$if = new InfusionSoftDonation();
					$if->AddDonation($this);
				}
			} else $fail[] = $sql . ': ' . $this->db->Error();
			//$success[] = $sql;
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn CreateFromSession

	public function CreateFromSessionNew($data = array())
	{
		$fail = array();
		$success = array();
		$extras = array();
		$fields = array('created="' . $this->datefn->SQLDateTime() . '"');

		if ($donationtype = $this->GetDonationType($data['donation_type']))
		{	$fields['donationtype'] = 'donationtype="' . $data['donation_type'] . '"';
			if ($donationtype['allowadmin'])
			{	$fields['adminamount'] = 'adminamount=' . (int)$data['donation_admin'];
			}
		} else
		{	$fail[] = 'type missing';
		}

		if (($amount = round($data['donation_amount'], 2)) > 0)
		{	$fields['amount'] = 'amount=' . $amount;
		} else
		{	$fail[] = 'amount missing';
		}

		if ($this->GetCurrency($data['donation_currency']))
		{	$fields['currency'] = 'currency="' . $data['donation_currency'] . '"';
		} else
		{	$fail[] = 'currency missing';
		}

		if ($donationcountry = $this->SQLSafe($data['donation_country']))
		{	$fields['donationcountry'] = 'donationcountry="' . $donationcountry . '"';
		} else
		{	$fail[] = 'donation country missing';
		}
		if ($donationproject = $this->SQLSafe($data['donation_project']))
		{	$fields['donationproject'] = 'donationproject="' . $donationproject . '"';
		}
		//  else
		// {	$fail[] = 'donation project missing';
		// }
		if ($donationproject2 = $this->SQLSafe($data['donation_project2']))
		{	$fields['donationproject2'] = 'donationproject2="' . $donationproject2 . '"';
		}

		if ($donortitle = $this->SQLSafe($data['donor_title']))
		{	$fields['donortitle'] = 'donortitle="' . $donortitle . '"';
		} else
		{	$fail[] = 'your title is missing';
		}
		if ($donorfirstname = $this->SQLSafe($data['donor_firstname']))
		{	$fields['donorfirstname'] = 'donorfirstname="' . $donorfirstname . '"';
		} else
		{	$fail[] = 'first name missing';
		}
		if ($donorsurname = $this->SQLSafe($data['donor_lastname']))
		{	$fields['donorsurname'] = 'donorsurname="' . $donorsurname . '"';
		} else
		{	$fail[] = 'first name missing';
		}

		if ($data['donor_country'] && $this->GetCountry($data['donor_country']))
		{	$fields['donorcountry'] = 'donorcountry="' . $data['donor_country'] . '"';
			if ($data['donor_country'] == 'GB')
			{	if ($data['donor_giftaid'])
				{	$fields['giftaid'] = 'giftaid=1';
				}
				$add_prefix = 'pca_';
				//$fail[] = $data[$add_prefix . 'donor_address1'];
			}

			if ($donor_address1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
			{	$fields['donoradd1'] = 'donoradd1="' . $donor_address1 . '"';
			} else
			{	$fail[] = 'address missing';
			}

			if ($donorpostcode = $this->SQLSafe($data[$add_prefix . 'donor_postcode']))
			{	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
			} else
			{	$fail[] = 'post code missing';
			}

			// if ($donoradd3 = $this->SQLSafe($data[$add_prefix . 'donor_address3']))
			// {	$donoradd1lines = array();
			// 	if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
			// 	{	$donoradd1lines[] = $donoradd1;
			// 	}
			// 	if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
			// 	{	$donoradd1lines[] = $donoradd2;
			// 	}
			// 	if ($donoradd1lines)
			// 	{	$fields['donoradd1'] = 'donoradd1="' . implode(', ', $donoradd1lines) . '"';
			// 	}
			// 	$fields['donoradd2'] = 'donoradd2="' . $donoradd3 . '"';
			// 	$address_done++;
			// } else
			// {	if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
			// 	{	$fields['donoradd1'] = 'donoradd1="' . $donoradd1 . '"';
			// 		$address_done++;
			// 	}
			// 	if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
			// 	{	$fields['donoradd2'] = 'donoradd2="' . $donoradd2 . '"';
			// 		$address_done++;
			// 	}
			// }
			// if (!$address_done)
			// {	$fail[] = 'address missing';
			// }

			// if ($donorpostcode = $this->SQLSafe($data[$add_prefix . 'donor_postcode']))
			// {	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
			// } else
			// {	$fail[] = 'post code missing';
			// }
			// if ($donorcity = $this->SQLSafe($data[$add_prefix . 'donor_city']))
			// {	$fields['donorcity'] = 'donorcity="' . $donorcity . '"';
			// } else
			// {	$fail[] = 'city/town missing';
			// }
		} else
		{	$fail[] = 'country missing';
		}

		$fields['donation_zakat'] = 'zakat=' . ($data['donation_zakat'] ? 1 : 0);
		$fields['ddsolesig'] = 'ddsolesig=' . ($data['dd_confirm'] ? 0 : 1);
		$fields['quantity'] = 'quantity=' . (int)$data['donation_quantity'];

		if ($donoremail = $this->SQLSafe($data['donor_email']))
		{	$fields['donoremail'] = 'donoremail="' . $donoremail . '"';
		} else
		{	$fail[] = 'email missing';
		}
		// $fields['donorphone'] = 'donorphone="' . $this->SQLSafe($data['donor_phone']) . '"';
		$fields['donoranon'] = 'donoranon=' . ($data['donor_anon'] ? 1 : 0);
		
		// $fields['contactpref'] = 'contactpref=' . (int)$data['contactpref'];
		$fields['orderid'] = 'orderid=' . (int)$data['orderid'];

		//$fail[] = 'test';

		if (!$fail && ($set = implode(', ', $fields)))
		{	$sql = 'INSERT INTO donations SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
				{	$this->Get($id);
					$this->RecordUTMData();
					$success[] = 'New donation created';
					$if = new InfusionSoftDonation();
					$if->AddDonation($this);
				}
			} else $fail[] = $sql . ': ' . $this->db->Error();
			//$success[] = $sql;
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn CreateFromSessionNew

	protected function RecordUTMData()
	{	if (is_array($_SESSION['utm']) && $_SESSION['utm'])
		{	foreach ($_SESSION['utm'] as $name=>$value)
			{	$extras_sql = 'INSERT INTO donation_extras SET did=' . $this->id  . ', extrafield="' . $this->SQLSafe($name) . '", extravalue="' . $this->SQLSafe($value) . '"';
				$this->db->Query($extras_sql);
			}
		}
	} // end of fn RecordUTMData

	public function UpdateOneOffFromWorldPayPost($post = array())
	{	$fail = array();
		$success = array();

		$subfields = array('gateway="worldpay"');
		$pmtfields = array('gateway="worldpay"');

		if ($this->id)
		{	if ($this->details['donationref'])
			{	$fail[] = 'donation already paid';
			} else
			{	$ppsubid = 'WP~' . $post['transId'];
				$subfields[] = 'donationref="' . $ppsubid . '"';
				$pmtfields[] = 'donationref="' . $ppsubid . '"';
				$pmtfields[] = 'paymentref="' . $ppsubid . '"';

				$pmtfields[] = 'currency="' . $this->details['currency'] . '"';

				$subfields[] = 'donationconfirmed="' . $this->datefn->SQLDateTime() . '"';
				$pmtfields[] = 'paydate="' . $this->datefn->SQLDateTime() . '"';

				$fullamount = round($post['amount'], 2);
				if ($fullamount == round($this->details['amount'] + $this->details['adminamount'], 2))
				{	$pmtfields[] = 'amount=' . $this->details['amount'];

					if (($fullamount > 0) && ($convertrate = $this->GetCurrency($this->details['currency'], 'convertrate')))
					{	$pmtfields[] = 'gbpamount=' . round($this->details['amount'] / $convertrate, 2);
					}
					if ($this->details['adminamount'])
					{	$pmtfields[] = 'adminamount=' . $this->details['adminamount'];
						$pmtfields[] = 'gbpadminamount=' . round($this->details['adminamount'] / $convertrate, 2);
					}
				} else
				{	$fail[] = 'amount/mc_gross doesn\'t match (' . round($this->details['amount'] + $this->details['adminamount'], 2) . ' != ' . $fullamount . ')';
				}

				$name = explode(' ', $post['name'], 2);
				if ($payerfirstname = $this->SQLSafe($name[0]))
				{	$pmtfields[] = 'payerfirstname="' . $payerfirstname . '"';
				}
				if ($payerlastname = $this->SQLSafe($name[1]))
				{	$pmtfields[] = 'payerlastname="' . $payerlastname . '"';
				}
				if ($payercountry = $this->SQLSafe($post['country']))
				{	$pmtfields[] = 'payercountry="' . $payercountry . '"';
				}
				if ($payeremail = $this->SQLSafe($post['email']))
				{	$pmtfields[] = 'payeremail="' . $payeremail . '"';
				}

				if (!$fail)
				{	$subset = implode(', ', $subfields);
					$subsql = 'UPDATE donations SET ' . $subset . ' WHERE did=' . $this->id;
					$pmtset = implode(', ', $pmtfields);
					$pmtsql = 'INSERT INTO donationpayments SET ' . $pmtset;
					if ($result = $this->db->Query($subsql))
					{	$success[] = 'one off donation ' . $this->id . ' updated';
						if ($pmtresult = $this->db->Query($pmtsql))
						{	$this->Get($this->id);
							$this->SendOneOffDonationEmail();
							$if = new InfusionSoftDonation();
							$if->PayDonation($this);
						} else $fail[] = $pmtsql . '---' . $this->db->Error();
					} else $fail[] = $subsql . '---' . $this->db->Error();
				}
			}
		} else
		{	$fail[] = 'donation not found';
		}
		//return array();
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn UpdateOneOffFromWorldPayPost

	public function SendOneOffDonationEmail($email = '')
	{	$mail = new HTMLMail();
		print_r($this); exit;
		$countries = $this->GetDonationCountries();
		$emailtext = '';
		if ($project = $countries[$this->details['donationcountry']]['projects'][$this->details['donationproject']])
		{	if ($this->details['donationproject2'] && ($project2 = $countries[$this->details['donationcountry']]['projects'][$this->details['donationproject']]['projects'][$this->details['donationproject2']]))
			{	if ($project2['emailtext'])
				{	$emailtext = $this->InputSafeString($project2['emailtext']);
				}
			}
			if (!$emailtext && $project['emailtext'])
			{	$emailtext = $this->InputSafeString($project['emailtext']);
			}
		}
		if ($emailtext)
		{	$emailtext = ' ' . $emailtext;
		}
		ob_start();
		echo '<p>Dear ', $this->InputSafeString($this->details['donorfirstname']), '</p><p>Thank you for your donation of ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['amount'] + $this->details['adminamount'], 2), $emailtext, '.</p>';
		if ($this->details['zakat'])
		{	echo '<p>We appreciate that this is a Zakat donation. We\'d like to reassure you that this donation will be distributed in line with the Islamic principles of Zakat.</p>';
		}
		$body = ob_get_clean();
		$mail->SetSubject('Thank you for your donation to Charity Right');
		$mail->SendFromTemplate($email ? $email : $this->details['donoremail'], array('main_content'=>$body), 'default');
	} // end of fn SendOneOffDonationEmail

	public function extraSendOneOffDonationEmail($email = '',$data = '')
	{	$mail = new HTMLMail();
		$this->details = $data; 
		$countries = $this->GetDonationCountries();
		$emailtext = '';
		if ($project = $countries[$this->details['donationcountry']]['projects'][$this->details['donationproject']])
		{	if ($this->details['donationproject2'] && ($project2 = $countries[$this->details['donationcountry']]['projects'][$this->details['donationproject']]['projects'][$this->details['donationproject2']]))
			{	if ($project2['emailtext'])
				{	$emailtext = $this->InputSafeString($project2['emailtext']);
				}
			}
			if (!$emailtext && $project['emailtext'])
			{	$emailtext = $this->InputSafeString($project['emailtext']);
			}
		}
		if ($emailtext)
		{	$emailtext = ' ' . $emailtext;
		}
		ob_start();
		echo '<p>Dear ', $this->InputSafeString($this->details['donorfirstname']), '</p><p>Thank you for your donation of ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['amount'] + $this->details['adminamount'], 2), $emailtext, '.</p>';
		if ($this->details['zakat'])
		{	echo '<p>We appreciate that this is a Zakat donation. We\'d like to reassure you that this donation will be distributed in line with the Islamic principles of Zakat.</p>';
		}
		$body = ob_get_clean();
		$mail->SetSubject('Thank you for your donation to Charity Right');
		$mail->SendFromTemplate($email ? $email : $this->details['donoremail'], array('main_content'=>$body), 'default');
	} // end of fn SendOneOffDonationEmail


	public function GetDonationCountries($type = '', $liveonly = false)
	{	$countries = array();
		$c_sql = 'SELECT * FROM donation_countries ORDER BY listorder, dcid';
		if ($c_result = $this->db->Query($c_sql))
		{	while ($c_row = $this->db->FetchArray($c_result))
			{	$country = new DonationOption($c_row);
				$c_row['projects'] = array();
				foreach ($country->projects as $project_row)
				{	$project = new DonationProject($project_row);
					$project_row['projects'] = array();
					if ($project->prices && is_array($project->prices))
					{	$project_row['prices'] = array();
						foreach ($project->prices as $price)
						{	$project_row['prices'][$price['currency']] = $price['amount'];
						}
					}
					foreach ($project->projects as $project2_row)
					{	$project2 = new DonationProject2($project2_row);
						$project_row['projects'][$project2_row['dpcode']] = $project2_row;
						if ($project2->prices && is_array($project2->prices))
						{	$project_row['projects'][$project2_row['dpcode']]['prices'] = array();
							foreach ($project2->prices as $price)
							{	$project_row['projects'][$project2_row['dpcode']]['prices'][$price['currency']] = $price['amount'];
							}
						}
					}
					$c_row['projects'][$project_row['dpcode']] = $project_row;
				}
				$countries[$c_row['dccode']] = $c_row;
			}
		}
		
		foreach ($countries as $key=>$details)
		{	if ($type && !$details[$type])
			{	unset($countries[$key]);
			} else
			{	if ($liveonly && $details['projects'] && is_array($details['projects']))
				{	foreach ($details['projects'] as $projectid=>$projectdetails)
					{	if (!$projectdetails[$type])
						{	unset($countries[$key]['projects'][$projectid]);
						} else
						{	foreach ($projectdetails['projects'] as $projectid2=>$projectdetails2)
							{	if (!$projectdetails2[$type])
								{	unset($countries[$key]['projects'][$projectid]['projects'][$projectid2]);
								}
							}
						}
					}
					if (!$details['projects'])
					{	unset($countries[$key]);
					}
				}
			}
		}
		return $countries;
	} // end of fn GetDonationCountries
	
	public function WPTransactionId()
	{	if (substr($this->details['donationref'], 0, 3) == 'WP~')
		{	return substr($this->details['donationref'], 3);
		}
	} // end of fn WPTransactionId
	
	public function WPRefund()
	{	$wp = new WorldPayRedirect();
		$refund = $wp->RefundDonation($this);
		if (($refund['result'] == 'Y') || ($refund['result'] == 'A'))
		{	$this->RecordRefundWP();
		}
		return $refund;
	} // end of fn WPRefund
	
	public function RecordRefundWP()
	{	$post = array();
		$post['txn_id'] = $this->details['donationref'] . '_ref';
		$post['payment_date'] = $this->datefn->SQLDateTime();
		$post['mc_gross'] = -1 * $this->details['amount'];
		$post['mc_currency'] = $this->details['currency'];
		//$this->VarDump($post);
		$record = $this->RecordRefund($post);
		if ($record['failmessage'])
		{	print_r($record);
		}
	} // end of fn RecordRefundWP
	
	public function CanRefund()
	{	return $this->id && $this->details['donationref'] && ($this->details['gateway'] == 'worldpay') && !$this->GetRefund();
	} // end of fn CanRefund
	
	public function GetRefund()
	{	$refunds = array();
		$sql = 'SELECT * FROM donationrefunds WHERE dontable="donations" AND donid=' . $this->id  . ' ORDER BY refunded';
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return $row;
			}
		}
	} // end of fn GetRefund
	
	public function RecordRefund($post = array())
	{	if ($this->id) // only if exists
		{	$fail = array();
			$fields = array('donid=' . $this->id, 'dontable="donations"');
			
			if ($pptransid = $this->SQLSafe($post['txn_id']))
			{	$fields[] = 'transid="' . $pptransid . '"';
			} else
			{	$fail[] = 'no txn_id';
			}
			
			$refdate = $this->datefn->SQLDateTime(strtotime($post['payment_date']));
			if ((int)$refdate)
			{	$fields[] = 'refunded="' . $refdate . '"';
			} else
			{	$fail[] = 'no payment_date';
			}
			if ($refamount =  -1 * round($post['mc_gross'], 2))
			{	$fields[] = 'amount=' . $refamount;
			} else
			{	$fail[] = 'no amount';
			}
			$fields[] = 'currency="' . $this->SQLSafe($post['mc_currency']) . '"';
			
			if (($convertrate = $this->GetCurrency($post['mc_currency'], 'convertrate')) && ($refamount != 0))
			{	$fields[] = 'gbpamount=' . round($refamount / $convertrate, 2);
			}
			
			if (!$fail)
			{	$set = implode(', ', $fields);
				$sql = 'INSERT INTO donationrefunds SET ' . $set;
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows())
					{	if ($refundid = $this->db->InsertID())
						{	$success[] = 'refund record recorded';
						}
					} else
					{	$fail[] = 'refund record not inserted';
					}
				} else
				{	$fail[] = 'refund insert query failed: ' . $sql . ': ' . $this->db->Error();
				}
			}
			
		} else
		{	$fail[] = 'original transaction not found for refund';
		}
		
		return $created = array('failmessage'=>implode('; ', $fail), 'successmessage'=>$successmessage);
		
	} // end of fn RecordRefund

	public function GATransactionTrackingCode()
	{	if (false || !SITE_TEST)
		{	ob_start();
			echo '
<script type="text/javascript">
ga("require", "ecommerce");
ga("ecommerce:addTransaction", {
"id": "', $id = $this->ID(), '",
"affiliation": "Charity Right",
"revenue": "', $totalamount = round($this->details['amount'] + $this->details['adminamount'], 2), '",
"currency": "', $this->details['currency'], '"
});
ga("ecommerce:addItem", {
"id": "', $id, '",
"name": "', $this->details['donationtype'], ' donation",
"price": "', $totalamount, '",
"quantity": "', $this->details['quantity'] ? $this->details['quantity'] : '1', '"
});
ga("ecommerce:send");
</script>
';
			return ob_get_clean();
		}
	} // fn GATransactionTrackingCode

} // end of defn Campaign
?>