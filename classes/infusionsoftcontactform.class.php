<?php
class InfusionSoftContactForm extends Base
{	
	
	public function __construct()
	{	parent::__construct();
	} // fn __construct
	
	public function Save($data = array())
	{	$fail = array();
		$success = array();
		
	//	$fail[] = 'test';
		
		$inf = new InfusionSoft();
		if ($infid = $inf->AddContact($_POST))
		{	$inf->AddTagToContact($infid, 'newsletter');
			$success[] = 'Thank you for signing up';
		} else
		{	$fail[] = 'We could not sign you up at this time';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // fn Save
	
	public function FormInfusionsoftDirect()
	{	ob_start();
		echo '<form accept-charset="UTF-8" action="https://tn172.infusionsoft.com/app/form/process/e69c9f7b966c2771d0e29142b98afe0d" class="infusion-form" method="POST">
						<input name="inf_form_xid" type="hidden" value="e69c9f7b966c2771d0e29142b98afe0d" />
						<input name="inf_form_name" type="hidden" value="Newsletter sign up" />
						<input name="infusionsoft_version" type="hidden" value="1.60.0.55" />
						<div class="infusion-field infusion-field-text">
							<h3>Sign up to get the latest Charity Right news</h3>
						</div>
						<div class="infusion-field infusion-field-input">
							<input class="infusion-field-input-container" id="inf_field_FirstName" name="inf_field_FirstName" type="text" placeholder="First Name" required/>
						</div>
						<div class="infusion-field infusion-field-input">
							<input class="infusion-field-input-container" id="inf_field_LastName" name="inf_field_LastName" type="text" placeholder="Second Name" required/>
						</div>
						<div class="infusion-field infusion-field-select">
							<i class="select-arrow icon icon-angle-down"></i>
							<select class="infusion-field-input-container" id="inf_custom_country0" name="inf_custom_country0" required>
								<option value="">Choose Location</option>';
								$sql = 'SELECT countries.shortname FROM countries ORDER BY countries.shortname';
								if ($result = $this->db->Query($sql))
								{	while ($ctry = $this->db->FetchArray($result))
									{	$ctryname = $this->InputSafeString($ctry['shortname']);
										$selected = "";
										if($ctryname == "United Kingdom"){
											$selected = " selected";
										}
										echo '<option value="', $ctryname, '"',$selected,'>', $ctryname, '</option>';
									}
								}
		echo '				</select>
						</div>
						<div class="infusion-field infusion-field-input">
							<input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" type="email" placeholder="Email" required/>
						</div>
						<div class="infusion-submit infusion-field-submit">
							<input type="submit" value="Join" />
						</div>
						<div class="clear"></div>
					</form>
					<script type="text/javascript" src="https://tn172.infusionsoft.com/app/webTracking/getTrackingCode"></script>';
		return ob_get_clean();
	} // fn FormInfusionsoftDirect
	
	public function Form($data = array())
	{	ob_start();
		echo '<form action="https://tn172.infusionsoft.com/app/form/process/e69c9f7b966c2771d0e29142b98afe0d" onsubmit="InfusionsoftContactSubmit(\'full\');return false;" class="infusion-form">
						<div class="infusion-field infusion-field-text">
							<h3>Sign up to get the latest Charity Right news</h3>
						</div>
						<div class="infusion-field infusion-field-input">
							<input class="infusion-field-input-container" id="inf_field_FirstName" name="inf_field_FirstName" value="', $this->InputSafeString($data['firstname']), '" type="text" placeholder="First Name" required/>
						</div>
						<div class="infusion-field infusion-field-input">
							<input class="infusion-field-input-container" id="inf_field_LastName" name="inf_field_LastName" value="', $this->InputSafeString($data['lastname']), '" type="text" placeholder="Second Name" required/>
						</div>
						<div class="infusion-field infusion-field-select">
							<i class="select-arrow icon icon-angle-down"></i>
							<select class="infusion-field-input-container" id="inf_custom_country0" name="inf_custom_country0" required>
								<option value="">Choose Location</option>';
								$sql = 'SELECT countries.shortname FROM countries ORDER BY countries.shortname';
								if ($result = $this->db->Query($sql))
								{	$selected = isset($data['country']) ? $data['country'] : 'United Kingdom';
									while ($ctry = $this->db->FetchArray($result))
									{	$ctryname = $this->InputSafeString($ctry['shortname']);
										echo '<option value="', $ctryname, '"', $selected == $ctryname ? ' selected="selected"' : '','>', $ctryname, '</option>';
									}
								}
		echo '				</select>
						</div>
						<div class="infusion-field infusion-field-input">
							<input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" value="', $this->InputSafeString($data['email']), '" type="email" placeholder="Email" required/>
						</div>
						<div class="infusion-submit infusion-field-submit">
							<input type="submit" value="Join" />
						</div>
						<div class="clear"></div>
					</form>';
		return ob_get_clean();
	} // end of fn Form
	
	public function MinimalSectionForm($data = array())
	{	ob_start();
		echo '<form action="https://tn172.infusionsoft.com/app/form/process/e69c9f7b966c2771d0e29142b98afe0d" onsubmit="InfusionsoftContactSubmit(\'minimal\');return false;" class="infusion-form">
			<div class="infusion-field infusion-field-input">
				<input class="infusion-field-input-container" id="inf_field_FullName" name="inf_field_FullName" value="', $this->InputSafeString($data['fullname']), '" type="text" placeholder="Full Name" required/>
			</div>
			<div class="infusion-field infusion-field-input">
				<input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" value="', $this->InputSafeString($data['email']), '" type="email" placeholder="Email" required/>
			</div>
			<div class="infusion-submit infusion-field-submit">
				<input type="submit" value="Get our emails" />
			</div>
			<div class="clear"></div>
		</form>';
		return ob_get_clean();
	} // end of fn MinimalSectionForm
	
	public function Q19MinimalSectionForm($data = array())
	{	ob_start();
		echo '<!--<form action="https://tn172.infusionsoft.com/app/form/process/e69c9f7b966c2771d0e29142b98afe0d" onsubmit="InfusionsoftContactSubmit(\'q19lp\');return false;" class="infusion-form">-->
		<form onsubmit="InfusionsoftContactSubmit(\'q19lp\');return false;" class="xxinfusion-form">
			<div class="row">
				<div class="col-lg-3 col-md-12">
					<h4 class="sign_txt">Sign up to our emails</h4>
				</div>
				<div class="col-lg-3 col-md-4 infusion-submit infusion-field-submit">
					<input class="infusion-field-input-container form-control" id="inf_field_FullName" name="inf_field_FullName" value="', $this->InputSafeString($data['fullname']), '" type="text" placeholder="Your name" required/>
				</div>
				<div class="col-lg-3 col-md-4 infusion-submit infusion-field-submit">
					<input class="infusion-field-input-container form-control" id="inf_field_Email" name="inf_field_Email" value="', $this->InputSafeString($data['email']), '" type="email" placeholder="Your email address" required/>
				</div>
				<div class="col-lg-3 col-md-4 infusion-submit infusion-field-submit">
					<input type="submit" class="btn btn_donate" value="Get our emails" />
				</div>
			</div>
		</form>';
		return ob_get_clean();
	} // end of fn Q19MinimalSectionForm

} // end of defn InfusionSoftContactForm
?>