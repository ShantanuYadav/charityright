<?php
class AdminDonationOption extends DonationOption
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct

	public function SlugExists($slug = '')
	{	$sql = 'SELECT dcid FROM donation_countries WHERE slug="' . $this->SQLSafe($slug) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT dcid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	public function Save($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if (!$this->id)
		{	if ($donationname = $this->SQLSafe($data['donationname']))
			{	//$fields['donationname'] = 'donationname="' . $donationname . '"';
				// set dccode
				$dccode = strtolower(str_replace(array(' ','-'), '_', $donationname));
				$dccode = preg_replace('|[^0-9a-z_]|', '', $dccode);
				if ($dccode)
				{	$fields['dccode'] = 'dccode="' . $dccode . '"';
				} else
				{	$fail[] = 'invlid donation name (no letters or numbers)';
				}
			} else
			{	$fail[] = 'donation code cannot be empty';
			}
		}
		
		if ($shortname = $this->SQLSafe($data['shortname']))
		{	$fields['shortname'] = 'shortname="' . $shortname . '"';
		} else
		{	$fail[] = 'display name cannot be empty';
		}
		
		if ($formlabel = $this->SQLSafe($data['formlabel']))
		{	$fields['formlabel'] = 'formlabel="' . $formlabel . '"';
		} else
		{	$fail[] = 'donation form label cannot be empty';
		}
		
		if ($formlabelcrstars = $this->SQLSafe($data['formlabelcrstars']))
		{	$fields['formlabelcrstars'] = 'formlabelcrstars="' . $formlabelcrstars . '"';
		} else
		{	$fail[] = 'CR Stars form label cannot be empty';
		}

		$fields['listorder'] = 'listorder=' . (int)$data['listorder'];
		$fields['oneoff'] = 'oneoff=' . ($data['oneoff'] ? '1' : '0');
		$fields['monthly'] = 'monthly=' . ($data['monthly'] ? '1' : '0');
		$fields['crstars'] = 'crstars=' . ($data['crstars'] ? '1' : '0');
		$fields['nozakat'] = 'nozakat=' . ($data['nozakat'] ? '1' : '0');
		
		if ($this->id)
		{	$slug = $this->TextToSlug($data['slug']);
		} else
		{	if ($donationname)
			{	$slug = $this->TextToSlug(str_replace('_', '-', $donationname));
			}
		}
		
		if ($slug)
		{	$suffix = '';
			while ($this->SlugExists($slug . $suffix))
			{	$suffix++;
			}
			if ($suffix)
			{	$slug .= $suffix;
			}
			
			$fields['slug'] = 'slug="' . $slug . '"';
		} else
		{	if ($this->id || $donationname)
			{	$fail[] = 'slug missing';
			}
		}
		
	//	$fail[] = 'test';
	//	$this->VarDump($fields);
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE donation_countries SET ' . $set . ' WHERE dcid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO donation_countries SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new donation country added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function CanDelete()
	{	return $this->id && !$this->projects && !$this->GetDonations() && !$this->GetCampaigns();
	} // end of fn CanDelete
	
	public function InputForm()
	{	ob_start();
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddTextInput('Display name', 'shortname', $this->InputSafeString($data['shortname']), 'long', 255, 1);
		if ($this->id)
		{	$form->AddTextInput('Slug', 'slug', $this->InputSafeString($data['slug']), '', 255, 1);
		} else
		{	$form->AddTextInput('Donation code', 'donationname', $this->InputSafeString($data['donationname']), '', 255, 1);
		}
		$form->AddTextInput('Donations form label for sub-options', 'formlabel', $this->InputSafeString($data['formlabel']), 'verylong', 255, 1);
		$form->AddTextInput('CR Stars form label for sub-options', 'formlabelcrstars', $this->InputSafeString($data['formlabelcrstars']), 'verylong', 255, 1);
		$form->AddCheckBox('Used for one-off donations', 'oneoff', 1, $data['oneoff']);
		$form->AddCheckBox('Used for monthly donations', 'monthly', 1, $data['monthly']);
		$form->AddCheckBox('Used for CR Stars campaigns', 'crstars', 1, $data['crstars']);
		$form->AddCheckBox('Don\'t allow Zakat for this', 'nozakat', 1, $data['nozakat']);
		$form->AddTextInput('List order', 'listorder', (int)$data['listorder'], '', 5);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Donation Country', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this option</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
		
} // end of defn AdminDonationOption
?>