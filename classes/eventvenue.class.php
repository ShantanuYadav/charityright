<?php
class EventVenue extends BlankItem
{	
	public function __construct($id = 0)
	{	parent::__construct($id, 'eventvenues', 'evid');
	} // fn __construct
	
	public function GetDates($filter = array())
	{	$tables = array('eventdates'=>'eventdates');
		$fields = array('eventdates.*');
		$where = array('event'=>'eventdates.venue=' . (int)$this->id);
		$orderby = array();
		switch ($filter['orderby'])
		{	case 'latest':
				$orderby[] = 'eventdates.starttime DESC';
			case 'next':
			default:
				$orderby[] = 'eventdates.starttime ASC';
		}
		
		if ($filter['live'])
		{	$where['live'] = 'eventdates.endtime>="' . $this->datefn->SQLDateTime() . '"';
		}
		if ($filter['event'])
		{	$where['venue'] = 'eventdates.eid=' . (int)$filter['event'];
		}
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'edid');
		
	} // fn GetDates
	
	public function Link()
	{	if ($this->id) return SITE_URL . 'venue/' . $this->id . '/' . $this->details['slug'] . '/';
	} // fn Link
	
	public function LocationString()
	{	ob_start();
		echo $this->InputSafeString($this->details['venuename'] . ', ' . $this->GetCountry($this->details['country']));
		return ob_get_clean();
	} // fn LocationString
	
} // end of defn EventVenue
?>