<?php
class NewsCats extends Base
{	var $cats = array();

	function __construct($liveonly = 0)
	{	parent::__construct();
		$this->Get($liveonly);
	} // fn __construct
	
	function Get($liveonly = 0)
	{	$this->cats = array();
		
		if ($liveonly)
		{	$sql = 'SELECT newscats.* FROM newscats, news WHERE newscats.catid=news.category GROUP BY newscats.catid ORDER BY listorder ASC';
		} else
		{	$sql = 'SELECT * FROM newscats ORDER BY listorder ASC';
		}
		
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->cats[$row["catid"]] = $this->AssignCat($row);
			}
		}
		
	} // end of fn Get
	
	function AssignCat($catid)
	{	return new NewsCat($catid);
	} // end of fn AssignCat
	
} // end if class defn NewsCats
?>