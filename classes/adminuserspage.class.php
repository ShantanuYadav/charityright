<?php
class AdminUsersPage extends AdminPage
{	protected $edituser;
	protected $menuarea;

	function __construct()
	{	parent::__construct('ADMIN');
	} //  end of fn __construct

	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('administration'))
		{	$this->AdminUsersLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AdminUsersLoggedInConstruct()
	{	$this->css[] = 'adminusers.css';
		$this->breadcrumbs->AddCrumb('userlist.php', 'Admin Users');
		$this->AssignEditUser();
		$this->AdminUserConstructFunctions();
		if ($this->edituser->userid)
		{	$this->breadcrumbs->AddCrumb('useredit.php?userid=' . $this->edituser->userid, $this->edituser->details['ausername']);
		}
	} // end of fn AdminUsersLoggedInConstruct
	
	protected function AdminUserConstructFunctions()
	{	
	} // end of fn AdminUserConstructFunctions
	
	protected function AssignEditUser()
	{	$this->edituser = new AdminUser($_GET['userid']);
	} // end of fn AssignEvent
	
	protected function AdminUsersBody()
	{	if ($this->edituser->userid)
		{	if ($menu = $this->GetUsersMenu())
			{	echo '<div class="adminItemSubMenu"><ul>';
				foreach ($menu as $menuarea=>$menuitem)
				{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
				}
				echo '</ul><div class="clear"></div></div>';
			}
		}
	} // end of fn AdminUsersBody
	
	protected function GetUsersMenu()
	{	$menu = array();
		$menu['edit'] = array('text'=>'Edit', 'link'=>'useredit.php?userid=' . $this->edituser->userid);
		$menu['access'] = array('text'=>'Access areas', 'link'=>'useraccess.php?userid=' . $this->edituser->userid);
		return $menu;
	} // end of fn GetUsersMenu
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('administration'))
		{	$this->AdminUsersBody();
		}
	} // end of fn AdminBodyMain
	
} // end of defn AdminUsersPage
?>