<?php
class DonationLinkBuilder extends Base
{	

	function __construct() // constructor
	{	parent::__construct();
	} // end of fn __construct
	
	function BuildLink($data = array())
	{	ob_start();
		echo $data['full'] ? CIT_FULLLINK : '/', 'donate/';
		if (($country = new DonationOption($data['dcid'])) && $country->id)
		{	echo $country->details['slug'];
		} else
		{	echo 'COUNTRY_ERROR';
		}
		echo '/';
		if (($project = new DonationProject($data['dpid'])) && $project->id && ($project->details['dcid'] == $country->id))
		{	echo $project->details['slug'];
		} else
		{	echo 'PROJECT_ERROR';
		}
		echo '/', $data['type'] ? $data['type'] : 'TYPE_ERROR', '/';
		$currencies = $this->GetCurrencies($data['type']);
		if ($currencies[$data['currency']])
		{	echo $data['currency'];
		} else
		{	echo 'CURRENCY_ERROR';
		}
		echo '/', (($amount = (int)$data['amount']) || !$data['quick']) ? $amount : 'AMOUNT_ERROR', '/';
		$extras = array();
		if ($data['quick'])
		{	$extras[] = 'quick';
		}
		if ($data['zakat'])
		{	$extras[] = 'zakat';
		}
		if ($extras)
		{	echo implode('_', $extras), '/';
		}
		return ob_get_clean();
	} // end of fn BuildLink

	private function GetCurrencies($type = 'oneoff')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies
	
} // end of defn DonationLinkBuilder
?>
