<?php
class DonatePage extends Base
{	private $pagecontent = false;
	private $page = false;
	private $session_name = 'crmain_donation';
	private $stage = '';
	private $stages = array(
			1=>array('heading'=>'Make a Donation', 'content_method'=>'DonationStageContent', 'submit_method'=>'DonationStageSubmit'),
			2=>array('heading'=>'Personal Details', 'content_method'=>'PersonalStageContent', 'submit_method'=>'PersonalStageSubmit'),
			4=>array('heading'=>'Confirmation', 'content_method'=>'ConfirmationStageContent', 'submit_method'=>'ConfirmationStageSubmit'),
			5=>array('heading'=>'Payment', 'content_method'=>'PaymentStageContent', 'submit_method'=>'PaymentStageSubmit')
		);
	public $failmessage = '';
	public $successmessage = '';
	protected $donation;
	private $giftaid_unticked = false;
	public $quick_params = array();

	public function __construct(&$page, $stage = '')
	{	parent::__construct();
		$this->pagecontent = new PageContent('donate');
		$this->PageAssign($page);
		if (SITE_TEST && ($ddtest = false))
		{	$this->donation = new Donation(73);
		} else
		{	$this->donation = new Donation();
		}

		if (!$_SESSION[$this->session_name] || $_GET['reset_test'])
		{	$_SESSION[$this->session_name] = array();
		}

		if ($_GET['reset_test_cart'])
		{	unset($_SESSION[$this->cart_session_name]);
		}

		if ($this->stage = (int)$_POST['donate_submit_stage'])
		{	$this->SubmitDonateForm($_POST);
		}
		if ($ddtest)
		{	$this->stage = 5;
			$_SESSION[$this->session_name]['donation_type'] = 'monthly';
			$_SESSION[$this->session_name]['donation_amount'] = $this->donation->details['amount'];
		}

		if (!$this->stage && isset($this->stages[$stage]))
		{	$this->stage = $stage;
		}
		if (!$_SESSION[$this->session_name]['donation_type'])
		{	foreach ($this->donation->GetDonationTypes() as $dtype=>$ddetails)
			{	$_SESSION[$this->session_name]['donation_type'] = $dtype;
				break;
			}
		} else
		if ($_SESSION[$this->cart_session_name]['currency'])
		{	$_SESSION[$this->session_name]['donation_currency'] = $_SESSION[$this->cart_session_name]['currency'];
		} else
		{	if (!$_SESSION[$this->session_name]['donation_currency'])
			{	$_SESSION[$this->session_name]['donation_currency'] = 'GBP';
			}
		}
		//$this->VarDump($_SESSION[$this->session_name]);
		//print_r($_GET);
		if ($_GET['param1'])
		{	$donatecountry = new DonationOption();
			$donatecountry->GetFromSlug($_GET['param1']);
			if ($donatecountry->id)
			{	$_SESSION[$this->session_name]['donation_country'] = $donatecountry->details['dccode'];
				$_SESSION[$this->session_name]['donation_nozakat'] = $donatecountry->details['nozakat'];
			}// else die('country not found ' . $_GET['param1']);
		}
		if ($_GET['param3'])
		{	$_SESSION[$this->session_name]['donation_type'] = $_GET['param3'];
		}
		if ($_GET['param4'])
		{	$_SESSION[$this->session_name]['donation_currency'] = $_GET['param4'];
		}
		if ($_GET['param5'])
		{	$_SESSION[$this->session_name]['donation_amount'] = (int)$_GET['param5'];
		}
		if ($_GET['param2'])
		{	$donateproject = new DonationProject();
			$donateproject->GetFromSlug($_GET['param2']);
			if ($donateproject->id)
			{	$_SESSION[$this->session_name]['donation_project'] = $donateproject->details['dpcode'];
				if ($projects2 = $donateproject->projects)
				{	foreach ($projects2 as $project=>$details)
					{	$_SESSION[$this->session_name]['donation_project2'] = $details['dp2code'];
						$donateproject2 = new DonationProject2($details);
						if ($donateproject2->prices && isset($donateproject2->prices[$_SESSION[$this->session_name]['donation_currency']]))
						{	$_SESSION[$this->session_name]['donation_amount'] = $donateproject2->prices[$_SESSION[$this->session_name]['donation_currency']]['amount'];
						}
						break;
					}
				}
			}
		}
		if ($_GET['param6'] && ($this->quick_params = explode('_', $_GET['param6'])))
		{	if (in_array('zakat', $this->quick_params) && !$_SESSION[$this->session_name]['donation_nozakat'])
			{	$_SESSION[$this->session_name]['donation_zakat'] = 1;
			}
			if (in_array('quick', $this->quick_params))
			{	//$this->stage = 2;
				$this->page->bodyOnLoadJS[] = 'DonationAddToCart()';
				//$this->page->bodyOnLoadJS[] = 'window.location = "/cart/"';
			}
		}
		
		// assign default options
		if ($countries = $this->donation->GetDonationCountries($_SESSION[$this->session_name]['donation_type'], true))
		{	if (!$_SESSION[$this->session_name]['donation_country'])
			{	foreach ($countries as $country=>$details)
				{	$_SESSION[$this->session_name]['donation_country'] = $country;
					$_SESSION[$this->session_name]['donation_nozakat'] = $details['nozakat'];
					break;
				}
			}
			if (!$_SESSION[$this->session_name]['donation_project'])
			{	if ($projects = $countries[$_SESSION[$this->session_name]['donation_country']]['projects'])
				{	foreach ($projects as $project=>$details)
					{	$_SESSION[$this->session_name]['donation_project'] = $project;
						if ($details['prices'] && isset($details['prices'][$_SESSION[$this->session_name]['donation_currency']]))
						{	$_SESSION[$this->session_name]['donation_amount'] = $details['prices'][$_SESSION[$this->session_name]['donation_currency']];
						}
						break;
					}
				}
			}
			if (!$_SESSION[$this->session_name]['donation_project2'])
			{	if ($projects = $countries[$_SESSION[$this->session_name]['donation_country']]['projects'][$_SESSION[$this->session_name]['donation_project']]['projects'])
				{	foreach ($projects as $project=>$details)
					{	$_SESSION[$this->session_name]['donation_project2'] = $project;
						//$this->VarDump($details);
						if ($details['prices'] && isset($details['prices'][$_SESSION[$this->session_name]['donation_currency']]))
						{	$_SESSION[$this->session_name]['donation_amount'] = $details['prices'][$_SESSION[$this->session_name]['donation_currency']];
						}
						break;
					}
				}
			}
			//$this->VarDump($_SESSION[$this->session_name]);
		}
		
	} //  end of fn __construct

	protected function IsFixedPrice($countries = array(), $donation_type = '', $donation_country = '', $donation_project = '', $donation_project2 = '', $currency = 'GBP')
	{	//echo $donation_project2, $currency;
		if (!$countries)
		{	$countries = $this->donation->GetDonationCountries($donation_type, true);
		}
		if ($country = $countries[$donation_country])
		{	if ($country['prices'])
			{	return isset($country['prices'][$currency]);
			} else
			{	if ($project = $country['projects'][$donation_project])
				{	if ($project['prices'])
					{	return isset($project['prices'][$currency]);
					} else
					{	if ($project2 = $project['projects'][$donation_project2])
						{	if ($project2['prices'])
							{	return isset($project2['prices'][$currency]);
							}
						}
					}
				}
			}
		}
	} //  end of fn IsFixedPrice

	public function SessionName()
	{	return $this->session_name;
	} //  end of fn SessionName

	protected function PageAssign(&$page)
	{	$this->page = &$page;
		$this->page->css['style.css'] = 'style.css';
		$this->page->css['style-donate.css'] = 'style-donate.css';
		$this->page->js['donations.js'] = 'donations.js';
	} //  end of fn PageAssign

	protected function GetPresets($donation_type = '')
	{	$presets = array();
		$presets[1] = array('amount'=>10, 'heading'=>'', 'text'=>'Can feed a child for a month.', 'oneoff'=>true);
		$presets[2] = array('amount'=>120, 'heading'=>'', 'text'=>'Can feed a child for one year.', 'oneoff'=>true);
		$presets[3] = array('amount'=>450, 'heading'=>'', 'text'=>'Feed 10 families in Somalia for one month.', 'oneoff'=>true);
		$presets[4] = array('amount'=>10, 'heading'=>'', 'text'=>'Can feed a child a meal at school every day of every month.', 'monthly'=>true);
		$presets[5] = array('amount'=>20, 'heading'=>'', 'text'=>'Can feed a Hifz student or a child in an orphanage.', 'monthly'=>true);
		$presets[6] = array('amount'=>35, 'heading'=>'', 'text'=>'Can feed a whole family every month.', 'monthly'=>true);
		if ($donation_type)
		{	foreach ($presets as $id=>$preset)
			{	if (!$preset[$donation_type])
				{	unset($presets[$id]);
				}
			}
		}
		return $presets;
	} // end of fn GetPresets

	private function SubmitDonateForm($data = array())
	{	if (($method = $this->stages[$this->stage]['submit_method']) && method_exists($this, $method))
		{	$saved = $this->$method($data);
			$this->page->failmessage = $saved['failmessage'];
			//$this->page->successmessage = $saved['successmessage'];
		}
	} // end of fn SubmitDonateForm

	private function RedirectToStage($stage = 0)
	{	if (($stage = (int)$stage) && $this->stages[$stage])
		{	header('location:' . $this->pagecontent->Link() . $stage . '/');
			exit;
		}
	} // end of fn RedirectToStage

	private function DonationStageSubmit($data = array())
	{	echo 'here';die; //$this->VarDump($data);
		$fail = array();
		$success = array();
		if ($this->donation->GetDonationType($data['donation_type']))
		{	$_SESSION[$this->session_name]['donation_type'] = $data['donation_type'];
			$currencies = $this->GetCurrencies($data['donation_type']);
			if ($currencies[$data['donation_currency']])
			{	$_SESSION[$this->session_name]['donation_currency'] = $data['donation_currency'];
			} else
			{	$fail[] = 'Donation currency missing';
			}
			if ($_SESSION[$this->session_name]['donation_type'] == 'monthly')
			{	$_SESSION[$this->session_name]['donor_country'] = 'GB';
				unset($_SESSION[$this->session_name]['donor_address1'], $_SESSION[$this->session_name]['donor_address2'], $_SESSION[$this->session_name]['donor_city'], $_SESSION[$this->session_name]['donor_postcode']);
			}
		} else
		{	$fail[] = 'Donation type missing';
		}

		if (($_SESSION[$this->session_name]['donation_amount'] = round($data['donation_amount'], 2)) <= 0)
		{	$fail[] = 'Donation amount missing';
		}
		$_SESSION[$this->session_name]['donation_admin'] = round($data['donation_admin'], 2);

		$countries = $this->donation->GetDonationCountries($data['donation_type']);
		if ($countries[$data['donation_country']])
		{	$_SESSION[$this->session_name]['donation_country'] = $data['donation_country'];
			if ($countries[$data['donation_country']]['projects'])
			{	if ($countries[$data['donation_country']]['projects'][$data['donation_project']])
				{	$_SESSION[$this->session_name]['donation_project'] = $data['donation_project'];
					if ($countries[$data['donation_country']]['projects'][$data['donation_project']]['projects'])
					{	if ($countries[$data['donation_country']]['projects'][$data['donation_project']]['projects'][$data['donation_project2']])
						{	$_SESSION[$this->session_name]['donation_project2'] = $data['donation_project2'];
						} else
						{	$fail[] = 'Project incomplete';
						}
					} else
					{	$_SESSION[$this->session_name]['donation_project2'] = '';
					}
				} else
				{	$fail[] = 'Project missing';
				}
			} else
			{	$_SESSION[$this->session_name]['donation_project'] = '';
			}
		} else
		{	$fail[] = 'Country missing';
		}

		$_SESSION[$this->session_name]['donation_zakat'] = ($data['donation_zakat'] ? 1 : 0);
		$_SESSION[$this->session_name]['donation_quantity'] = (int)$data['donation_quantity'];

		if (!$fail)
		{	//$this->RedirectToStage($this->stage = 2);
			$this->stage = 2;
		}
		return array('failmessage'=>implode('<br />', $fail), 'successmessage'=>implode('<br />', $success));
	} // end of fn DonationStageSubmit

	private function PersonalStageSubmit($data = array())
	{	//$this->VarDump($data);
		$fail = array();
		$success = array();

		if (!$_SESSION[$this->session_name]['donor_title'] = $data['donor_title'])
		{	$fail[] = 'You must give your title (Mr, Mrs, Miss, etc.)';
		}
		if (!$_SESSION[$this->session_name]['donor_firstname'] = $data['donor_firstname'])
		{	$fail[] = 'You must give your first name';
		}
		if (!$_SESSION[$this->session_name]['donor_lastname'] = $data['donor_lastname'])
		{	$fail[] = 'You must give your last name';
		}

		$_SESSION[$this->session_name]['donor_giftaid'] = 0;
		if ($data['donor_country'] && $this->GetCountry($data['donor_country']))
		{	$_SESSION[$this->session_name]['donor_country'] = $data['donor_country'];
			if ($gb = ($data['donor_country'] == 'GB'))
			{	if ($data['donor_giftaid'])
				{	$_SESSION[$this->session_name]['donor_giftaid'] = 1;
				} else
				{	$this->giftaid_unticked = true;
				}
			}
		} else
		{	$fail[] = 'You must give your country of residence';
		}

		if ($_SESSION[$this->session_name]['pca_donor_address1'] = $data['pca_donor_address1'])
		{	$pca_address_done++;
		}
		if ($_SESSION[$this->session_name]['pca_donor_address2'] = $data['pca_donor_address2'])
		{	$pca_address_done++;
		}
		if ($_SESSION[$this->session_name]['pca_donor_address3'] = $data['pca_donor_address3'])
		{	$pca_address_done++;
		}
		if (!$pca_address_done && $gb)
		{	$fail[] = 'You must give your address';
		}

		if (!$_SESSION[$this->session_name]['pca_donor_city'] = $data['pca_donor_city'])
		{	if ($gb)
			{	$fail[] = 'You must give your town or city';
			}
		}

		if (!$_SESSION[$this->session_name]['pca_donor_postcode'] = $data['pca_donor_postcode'])
		{	if ($gb)
			{	$fail[] = 'You must give your postcode';
			}
		}

		if ($_SESSION[$this->session_name]['donor_address1'] = $data['donor_address1'])
		{	$address_done++;
		}
		if ($_SESSION[$this->session_name]['donor_address2'] = $data['donor_address2'])
		{	$address_done++;
		}
		if (!$address_done)
		{	if (!$gb)
			{	$fail[] = 'You must give your address';
			}
		}

		if (!$_SESSION[$this->session_name]['donor_city'] = $data['donor_city'])
		{	if (!$gb)
			{	$fail[] = 'You must give your town or city';
			}
		}

		if (!$_SESSION[$this->session_name]['donor_postcode'] = $data['donor_postcode'])
		{	if (!$gb)
			{	$fail[] = 'You must give your postcode';
			}
		}

		if ($data['donor_email'])
		{	if ($this->ValidEMail($data['donor_email']))
			{	$_SESSION[$this->session_name]['donor_email'] = $data['donor_email'];
			} else
			{	$fail[] = 'You must give a valid email address';
				if (!$_SESSION[$this->session_name]['donor_email'])
				{	$_SESSION[$this->session_name]['donor_email'] = $data['donor_email'];
				}
			}
		} else
		{	$fail[] = 'You must give your email address';
		}

		if ($this->ValidPhoneNumber($data['donor_phone'], $gb))
		{	$_SESSION[$this->session_name]['donor_phone'] = $data['donor_phone'];
		} else
		{	$fail[] = 'Your phone number is missing or invalid';
			if (!$_SESSION[$this->session_name]['donor_phone'])
			{	$_SESSION[$this->session_name]['donor_phone'] = $data['donor_phone'];
			}
		}
		
		if (is_array($data['contactpref']) && $data['contactpref'])
		{	$pref = new ContactPreferences();
			$_SESSION[$this->session_name]['contactpref'] = $pref->PreferencesValueFromArray($data['contactpref']);
		} else
		{	$_SESSION[$this->session_name]['contactpref'] = (int)$data['contactpref'];
		}

		if ($monthly = $_SESSION[$this->session_name]['donation_type'] == 'monthly')
		{
			if ($data['dd_accountname'] && $this->AccountNameSafe($data['dd_accountname']))
			{	if ($_SESSION[$this->session_name]['dd_accountname'] != $data['dd_accountname'])
				{	$dd_changed = true;
				}
				$_SESSION[$this->session_name]['dd_accountname'] = $data['dd_accountname'];
			} else
			{	$fail[] = 'Account name missing';
				$_SESSION[$this->session_name]['dd_accountname'] = $data['dd_accountname'];
			}

			if (!$data['dd_accountnumber'])
			{	$fail[] = 'Account number missing';
				$_SESSION[$this->session_name]['dd_accountnumber'] = '';
			} else
			{	if ($this->ValidBankAccountNumber($data['dd_accountnumber']))
				{	if ($_SESSION[$this->session_name]['dd_accountnumber'] != $data['dd_accountnumber'])
					{	$dd_changed = true;
					}
				} else
				{	$fail[] = 'Invalid account number (must be 8 numbers)';
				}
				$_SESSION[$this->session_name]['dd_accountnumber'] = $data['dd_accountnumber'];
			}

			if ($data['dd_accountsortcode'])
			{	if ($this->ValidSortCode($data['dd_accountsortcode']))
				{	if ($_SESSION[$this->session_name]['dd_accountsortcode'] != str_replace('-', '', $data['dd_accountsortcode']))
					{	$dd_changed = true;
						$_SESSION[$this->session_name]['dd_accountsortcode'] = $data['dd_accountsortcode'];
					}
				} else
				{	$fail[] = 'Sort code invalid';
					$_SESSION[$this->session_name]['dd_accountsortcode'] = $data['dd_accountsortcode'];
				}
			} else
			{	$fail[] = 'Sort code missing';
				$_SESSION[$this->session_name]['dd_accountsortcode'] = '';
			}

			$_SESSION[$this->session_name]['dd_confirm'] = $data['dd_confirm'] ? '1' : '0';
			$_SESSION[$this->session_name]['tc_confirm'] = $data['tc_confirm'] ? '1' : '0';
			if (!$_SESSION[$this->session_name]['tc_confirm'])
			{	$fail[] = 'You must agree to our terms and conditions';
			}
		}

		if ($fail)
		{	$this->giftaid_unticked = false;
		} else
		{	if (!$this->giftaid_unticked || $_POST['donor_giftaidchecked'])
			{	if ($monthly && ($dd_changed || !$_SESSION[$this->session_name]['dd_checked']))
				{	$pca = new PCAPredictAPI();
					$pca_result = $pca->CheckBankAccount($_SESSION[$this->session_name]);
					if ($pca_result['successmessage'])
					{	$_SESSION[$this->session_name]['dd_checked'] = true;
					} else
					{	$fail[] = $pca_result['failmessage'];
						$_SESSION[$this->session_name]['dd_checked'] = false;
					}
				}
				if (!$fail)
				{	$this->stage = 4;
					//$this->RedirectToStage($this->stage = 4);
				}
			}
		}
		return array('failmessage'=>implode('<br />', $fail), 'successmessage'=>implode('<br />', $success));
	} // end of fn PersonalStageSubmit

	private function ConfirmationStageSubmit($data = array())
	{	$donatecheck = $this->DonationStageSubmit($_SESSION[$this->session_name]);
		if ($donatecheck['failmessage'])
		{	$this->stage = 1;
			return array('failmessage'=>'You must tell us what you want to donate', 'successmessage'=>'');
		} else
		{	$personalcheck = $this->PersonalStageSubmit($_SESSION[$this->session_name]);
			if ($personalcheck['failmessage'])
			{	$this->stage = 2;
				return array('failmessage'=>'You must complete your details', 'successmessage'=>'');
			} else
			{	$this->stage = 5;
				switch ($_SESSION[$this->session_name]['donation_type'])
				{	case 'oneoff':
						$saved = $this->donation->CreateFromSession($_SESSION[$this->session_name]);
						if ($this->donation->CanBePaid())
						{	if (false || !SITE_TEST)
							{	$this->page->bodyOnLoadJS[] = '$("form#wp_button").submit()';
							}
						}
						return array('failmessage'=>$saved['failmessage']);
						break;
					case 'monthly':
						if ($_SESSION[$this->session_name]['dd_checked'])
						{	return $this->PaymentStageDDProcess($data);
						}
						//check for submission of direct debit details
						break;
				}
			}
		}
		return array('failmessage'=>'', 'successmessage'=>'');
	} // end of fn ConfirmationStageSubmit

	private function PaymentStageSubmit($data = array())
	{	$donatecheck = $this->DonationStageSubmit($_SESSION[$this->session_name]);
		//echo $this->stage;
		//print_r($_POST);
		//exit;
		if ($donatecheck['failmessage'])
		{	$this->stage = 1;
			return array('failmessage'=>'You must tell us what you want to donate', 'successmessage'=>'');
		} else
		{	$personalcheck = $this->PersonalStageSubmit($_SESSION[$this->session_name]);
			if ($personalcheck['failmessage'])
			{	$this->stage = 2;
				return array('failmessage'=>'You must complete your details', 'successmessage'=>'');
			} else
			{	$this->stage = 5;
				//echo $this->stage;
				//exit;
				switch ($_SESSION[$this->session_name]['donation_type'])
				{	case 'oneoff':
						$this->stage = 4;
						break;
					case 'monthly':
						//return $this->PaymentStageDDProcess($data);
						break;
					default:
					//	echo 'not found';
				}
			}
		}
		return array('failmessage'=>'', 'successmessage'=>'');
	} // end of fn PaymentStageSubmit

	public function PaymentStageDDProcess($data = array())
	{	$fail = array();
		$success = array();

		if ($_SESSION[$this->session_name]['dd_checked'])
		{
			$saved = $this->donation->CreateFromSession($_SESSION[$this->session_name]);
			if ($this->donation->CanBePaid())
			{
				if ($this->donation->details['ddsolesig'])
				{	// save account details for the donation
					$this->donation->RecordUnpaidDirectDebit($_SESSION[$this->session_name]);
					$success[] = 'Your Direct Debit form has been sent';
				} else
				{	$ez = new EazyCollectAPI();
					$dd_customercreate = $ez->AddCustomer($this->donation, $_SESSION[$this->session_name]);
					if ($dd_customercreate['return']['CustomerID'])
					{	$_SESSION[$this->session_name]['ezc_CustomerID'] = $dd_customercreate['return']['CustomerID'];
						$dd_contractcreate = $ez->AddContract($this->donation, $dd_customercreate['return']['CustomerID']);
						if ($dd_contractcreate['return']['ContractID'])
						{	$_SESSION[$this->session_name]['ezc_ContractID'] = $dd_contractcreate['return']['ContractID'];
							$_SESSION[$this->session_name]['ezc_DirectDebitRef'] = $dd_contractcreate['return']['DirectDebitRef'];
							$_SESSION[$this->session_name]['ezc_StartDate'] = $dd_contractcreate['return']['StartDate'];
							$donation_confirm = $this->donation->ConfirmDirectDebit($_SESSION[$this->session_name]);
							if ($donation_confirm['successmessage'])
							{	$success[] = 'Your Direct Debit has been created';
							} else
							{	$fail[] = $donation_confirm['failmessage'];
							}
						} else
						{	$fail[] = $dd_contractcreate['failmessage'];
						}
					} else
					{	$fail[] = $dd_customercreate['failmessage'];
					}
				}
			} else
			{	$fail[] = $saved['failmessage'];
			}
		} else
		{	$fail[] = 'bank account details have not been checked';
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn PaymentStageDDProcess

	public function Breadcrumbs()
	{	if (!$this->do_cart)
		{	ob_start();
			echo '<div class="container donation_breadcrumbs"><div class="container_inner"><ul>';
			$width = floor(100 /count( $this->stages));
			foreach ($this->stages as $id=>$stage)
			{	echo '<li', $id == $this->stage ? ' class="donation_breadcrumbs_current"' : '', ' style="z-index: ', 1000 - $id, '; width: ', $width, '%;">';
				if ($id < $this->stage)
				{	echo '<a href="', $this->pagecontent->Link(), $id, '/" style="z-index: ', 1000 - $id, ';">';
				} else
				{	echo '<div style="z-index: ', 1000 - $id, ';">';
				}
				echo $this->InputSafeString($stage['heading']), '<span></span>', $id < $this->stage ? '</a>' : '</div>', '</li>';
			}
			echo '</ul><div class="clear" /></div></div></div>';
			return ob_get_clean();
		}
	} // end of fn Breadcrumbs

	public function MainDonateContent()
	{	ob_start();
		// echo '<div class="container donate_main_content"><div class="container_inner">';
		if (($method = $this->stages[$this->stage]['content_method']) && method_exists($this, $method))
		{	echo $this->$method();
		}
		// if (SITE_TEST)
		// {	
		// 	if ($this->do_cart)
		// 	{	echo '<p><a href="/donate/?reset_test_cart=1">reset whole cart</a></p>';
		// 		$this->VarDump($_SESSION[$this->cart_session_name]);
		// 	} else
		// 	{	echo '<p><a href="/donate/?reset_test=1">reset donation</a></p>';
		// 		$this->VarDump($_SESSION[$this->session_name]);
		// 	}
		// }
		// echo '</div></div>';
		return ob_get_clean();
	} // end of fn MainDonateContent

	public function DonationTypeContainer($donation_type = '', $currency = '')
	{	ob_start();
		if (!$currency)
		{	$currency = $_SESSION[$this->cart_session_name]['currency'];
		}
		echo '<div class="donation_types_chooser">';
		if ($dtypes = $this->donation->GetDonationTypes())
		{	if ($currency && ($cart_cur = $this->GetCurrency($currency)))
			{	//$this->VarDump($cart_cur);
				foreach ($dtypes as $dtype=>$dtype_details)
				{	if (!$cart_cur['use_' . $dtype])
					{	unset($dtypes[$dtype]);
					}
				}
			}
			if($dtypes)
			{	if (count($dtypes) > 1)
				{
					echo 'I would like to make a<span class="hidden-xs">&nbsp;&nbsp;&nbsp;</span><br class="visible-xs"/><div class="custom-select-box"><select id="donation-type" name="donation_type" onchange="DonateTypeChangeNew();">';
					foreach ($dtypes as $dtype=>$ddetails)
					{	if (!$donation_type)
						{	$donation_type = $dtype;
						}
						echo '<option value="', $dtype, '" data-donation-type="', $dtype, '" ', ($dtype == $donation_type) ? 'selected="selected"' : '', '>', $this->InputSafeString($ddetails['label']), '</option>';
					}
					echo '</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div><br class="visible-xs"/><span class="hidden-xs">&nbsp;&nbsp;&nbsp;</span>donation';
				} else
				{	foreach ($dtypes as $dtype=>$ddetails)
					{
						echo 'I would like to make a<input type="hidden" id="donation-type" name="donation_type" value="', $donation_type = $dtype, '" /><span>&nbsp;', $this->InputSafeString($ddetails['label']), '&nbsp;</span>donation';
					//	echo '<input type="hidden" id="donation-type" name="donation_type" value="', $donation_type = $dtype, '" /><div class="donation-dd-temp-header"><p>To make a one-off donation, please complete the form below.</p><p>To make a monthly donation, please <a href="https://goo.gl/forms/2XIfwTYekKoXxJ7X2">click here</a>.</p></div>';
					}
				}
			}
		}
		echo '</div>';
		//echo 'donation';
		return ob_get_clean();
	} // end of fn DonationTypeContainer

	public function DonationCurrencyChooserContainer($donation_type = '', $currency = '')
	{	ob_start();
		if ($_SESSION[$this->cart_session_name]['currency'])
		{	echo '<input type="hidden" id="donation_currency" name="donation_currency" value="', $this->InputSafeString($_SESSION[$this->cart_session_name]['currency']), '" />';
		} else
		{
			echo '<div class="form-row">
				<div class="form-col form-col-2">
					<div class="form-col-inner">
						<label for="donation_currency" class="title-label">Which currency would you like to donate?</label>
					</div>
				</div>
				<div class="form-col form-col-2">
					<div class="form-col-inner donation-currency-list">', $this->CurrencyChooserNew($donation_type, $currency), '</div>
				</div>
			</div>';
		}
		return ob_get_clean();
	} // end of fn DonationCurrencyChooserContainer

	private function DonationStageContent_old()
	{	ob_start();
		echo '<form action="', $this->pagecontent->Link(), '" method="post" class="donation-form donation-stage-1 form-rows"', $this->do_cart ? ' onsubmit="return false;"' : '', '>
			<input type="hidden" name="donate_submit_stage" value="1" />';
		if (false)
		{	echo '<div class="form-row donation-dd-temp-header">
				<div class="form-col form-col-1">
					<p>To make a one-off donation, please complete the form below.</p><p>To make a monthly donation, please <a href="">click here</a>.</p>
				</div>
			</div>';
		}
		echo '<div class="form-row">
				<div class="form-col form-col-1">
					<div class="form-col-inner donation_type_container">', $this->DonationTypeContainer($_SESSION[$this->session_name]['donation_type'], $_SESSION[$this->session_name]['donation_currency']), '</div>
				</div>
			</div>
			<div class="form-row donation-preset-list">', $this->PresetsListNew(), '</div>
			<div class="form-row">
				<div class="form-col form-col-1">
					<div class="form-col-inner">
						<div class="form-row-separator clearfix"></div>
					</div>
				</div>
			</div>';

			$donation_country_selected = '';
			$donation_country_selected = !empty($_GET["region"])?$_GET["region"]:$_SESSION[$this->session_name]['donation_country'];

			echo '<div class="donation-country-list clearfix">', $this->DonationCountriesListNew($_SESSION[$this->session_name]['donation_type'], $donation_country_selected), '</div>
			<div class="donation-project-list clearfix">', $this->DonationProjectsListNew($_SESSION[$this->session_name]['donation_type'], $_SESSION[$this->session_name]['donation_country'], $_SESSION[$this->session_name]['donation_project']), '</div>
			<div class="donation-project2-list clearfix">', $this->DonationProjects2ListNew($_SESSION[$this->session_name]['donation_type'], $_SESSION[$this->session_name]['donation_country'], $_SESSION[$this->session_name]['donation_project'], $_SESSION[$this->session_name]['donation_project2']), '</div>
			<input type="hidden" name="donation_oldcurrency" value="', $_SESSION[$this->session_name]['donation_currency'], '" /><div id="donationCurrencyChooserContainer">', $this->DonationCurrencyChooserContainer($_SESSION[$this->session_name]['donation_type'], $_SESSION[$this->session_name]['donation_currency']), '</div>
			<div class="form-row">
				<div class="form-col form-col-2">
					<div class="form-col-inner">
						<label for="donation-amount" class="title-label">Donation amount (<span class="donation-currency-symbol">', $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol'), '</span>)</label>
					</div>
				</div>
				<div class="form-col form-col-2">
					<div class="form-col-inner">
						<div class="donation_amount_container">', $this->DonationAmountContainer($_SESSION[$this->session_name]['donation_type'], $_SESSION[$this->session_name]['donation_amount'], $_SESSION[$this->session_name]['donation_country'], $_SESSION[$this->session_name]['donation_project'], $_SESSION[$this->session_name]['donation_project2'], $_SESSION[$this->session_name]['donation_quantity'], false, $_SESSION[$this->session_name]['donation_currency']), '</div>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col-1">
					<div class="form-col-inner">
						<div class="form-row-separator clearfix"></div>
					</div>
				</div>
			</div>
			<div class="form-row dpZakatContainer', $_SESSION[$this->session_name]['donation_nozakat'] ? ' hidden' : '', '">
				<div class="form-col form-col-2">
					<div class="form-col-inner"><label for="donation-zakat" class="title-label">Is your donation Zakat?</label></div>
				</div>
				<div class="form-col form-col-2">
					<div class="form-col-inner">
						<div class="custom-select-box">
							<select id="donation_zakat" name="donation_zakat">
								<option value="0" ', !($_SESSION[$this->session_name]['donation_zakat']) ? 'selected="selected"' : '', '>No</option>
								<option value="1" ', ($_SESSION[$this->session_name]['donation_zakat']) ? 'selected="selected"' : '', '>Yes</option>
							</select>
							<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="donation-admin clearfix">
				<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation-admin" class="title-label">Would you like to donate towards the administration cost of running our projects? (<span class="donation-currency-symbol">', $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol'), '</span>)</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner">
							<input type="number" id="donation-admin" name="donation_admin" class="donation-admin number inputSelectOnFocus" value="', number_format($_SESSION[$this->session_name]['donation_admin'], 2, '.', ''), '" step=".01" min="0" />
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<div class="form-row-separator clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col form-col-1">
					<div class="form-col-inner">', $this->do_cart ? '<input type="submit" value="Add to Cart" onclick="DonationAddToCart();" class="button" />' : '<input type="submit" value="Next" class="button" />', '</div>
				</div>
			</div></div>
		<!-- websquare testing --></form>';
		if (SITE_TEST)
		{	echo '<p><a onclick="DonationFomReset();">reset form</a></p>';
		}
		if ($this->do_cart)
		{	// modal pop-up
			echo '<script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$(".cartModalPopup").jqm();});</script>',
	'<!-- START banners list modal popup --><div class="jqmWindow cartModalPopup"><a href="#" class="jqmClose cartCloseTop">&times;</a><div class="cart_modal_popup_inner donpage"></div></div>';
		}
		return ob_get_clean();
	} // end of fn DonationStageContent

	private function DonationStageContent()
	{	ob_start();
		// echo 'User IP Address - '.$_SERVER['REMOTE_ADDR']; die;
		$amount = '25';
		$type = 'oneoff';
		$custom = '';
		if (isset($_GET['amount']) && $_GET['amount'] != '') {
			$amount = $_GET['amount'];
		}
		if (isset($_GET['type']) && $_GET['type'] != '') {
			$type = $_GET['type'];
		}
		if (isset($_GET['custom']) && $_GET['custom'] != '') {
			$custom = $_GET['custom'];
		}
		echo '<script src="https://js.stripe.com/v2/"></script><input type="hidden" id="stripe_publishable_key" value="',STRIPE_PUBLISHABLE_KEY,'"><div class="hero-banner donate-hero-banner">
			<div class="container">
				<div id="loaderclass" class=""></div>
				<div class="donation-tabs">
					<div class="stv-radio-tabs-wrapper">
						<div class="main-label">
							<input type="radio" class="stv-radio-tab" name="donation_type" value="oneoff" id="tab1" onclick="DonationTypeClick(this);" ';
							if($type == 'oneoff'){ echo 'checked'; }
							echo '/>
							<label for="tab1">Give once</label>
						</div>	
						<div class="main-label">	
							<input type="radio" class="stv-radio-tab" name="donation_type" value="monthly" id="tab2" onclick="DonationTypeClick(this);"';
							if($type == 'monthly'){ echo 'checked'; }
							echo '/>
							<label for="tab2">Monthly</label>
						</div>	
					</div>';
					$donation_country_selected = '';
					$donation_country_selected = !empty($_GET["region"])?$_GET["region"]:$_SESSION[$this->session_name]['donation_country'];
					echo '<div class="step1">
						<div class="choose-area">
				    		<select id="donation-country" class="main-select2" name="donation_country">
								<option value="" disabled selected hidden>Choose an area of our work you’d like to support</option>
								<option ',($donation_country_selected == 'most_needed'?"selected":"" ),' value="most_needed">Help where it’s most needed</option>
								<option ',($donation_country_selected == 'schools'?"selected":"" ),' value="schools">School meals</option>
								<option ',($donation_country_selected == 'food_packs'?"selected":"" ),' value="food_packs">Family food packs</option>
								<option ',($donation_country_selected == 'girls'?"selected":"" ),' value="girls">Educate a girl</option>
								<option ',($donation_country_selected == 'hifzmeals'?"selected":"" ),' value="hifzmeals">Hifz</option>
								<option ',($donation_country_selected == 'fidya'?"selected":"" ),' value="fidya"> Fidya </option>
								<option ',($donation_country_selected == 'yemen_food_pack'?"selected":"" ),' value="yemen_food_pack"> Yemen Food Pack </option>
								
							</select>
							<input type="hidden" value="GBP" name="donation_currency" id="donation_currency">
						</div>
						<script>
							$(function(){
								$("#donation-country").change(function(){
									if($(this).val()== "kaffarah"){
										window.location.href = "/fitrana";
									}
								});
							});
						</script>
					    <div class="choose-money">
							<p class="choose-money-p">Choose an amount you’d like to give <span class="month_content" ';
							if($type == 'oneoff'){ echo 'style="display:none;"'; }
							echo '>every month</span></p>
						</div>
						<div class="choose-money-option">
							<div class="radio-toolbar radio-first-row">
								<div class="radio-parent">
							    	<input type="radio" id="first-price" class="donation-amount" name="donation_amount" value="25" ';
							    	if($amount == '25'){ echo 'checked'; }
							    	echo '>
								    <label for="first-price">&#163;25</label>
								</div>    
								<div class="radio-parent">
							    	<input type="radio" id="second-price" class="donation-amount" name="donation_amount" value="50" ';
							    	if($amount == '50'){ echo 'checked'; }
							    	echo '>
								    <label for="second-price">&#163;50</label>
								</div>
								<div class="radio-parent">
							    	<input type="radio" id="third-price" class="donation-amount" name="donation_amount" value="75" ';
							    	if($amount == '75'){ echo 'checked'; }
							    	echo '>
								    <label for="third-price">&#163;75</label> 
								</div>    
							</div>
							<div class="radio-toolbar radio-second-row">
								<div class="radio-parent hundred-pound">
							    	<input type="radio" id="fourth-price" class="donation-amount" name="donation_amount" value="100" ';
							    	if($amount == '100'){ echo 'checked'; }
							    	echo '>
								    <label for="fourth-price">&#163;100</label>
								</div>    
								<div class="radio-parent other-amount">
							    	<input type="radio" id="fifth-price" class="donation-amount" name="donation_amount" value="other" ';
	 if((isset($_GET["other_amout_inner"]) && trim($_GET["other_amout_inner"]) != '')){echo 'checked';}else{
							    	if($amount == 'other'){ echo 'checked'; }
	 }
							    	echo '>
								    <label for="fifth-price" id="other-amount-main">Other amount</label>
								</div>
							</div>
							<div id="other_amount" ';
								
	 							if((isset($_GET["other_amout_inner"]) && trim($_GET["other_amout_inner"]) != '')){echo 'style="display: block;"'; }else{
									if($amount != 'other'){ echo 'style="display: none;"'; }
								}
							echo '>
								<div class="inner-other-amount">
	                            	<label class="answer-yes">Please enter amount</label>
	                            	<input type="number" name="other_donation_amount" id="other_amout_inner" class="other-amount-input" ';
	 if((isset($_GET["other_amout_inner"]) && trim($_GET["other_amout_inner"]) != '')){echo 'value="',$_GET["other_amout_inner"],'"';}else{
	                            	if($amount == 'other' && $custom != ''){ echo 'value="',$custom,'"'; }
	 }
	                            	echo '>
	                            </div>	
	                    	</div>
						</div>';
					echo '<div class="tick-switch">
							<span class="same-address first-added">
								<div class="checkclaimGift">
									<input type="checkbox" class="donation_zakat" id="donation_zakat" >
									<label for="donation_zakat" style="font-weight: unset;">Is your donation Zakat?</label>
								</div>
							</span>
						</div>
						<div class="donate-btn">
							<input type="button" value="donate" onclick="DonationAddToCartNew();" class="text-uppercase text-center" name="">
						</div>
					</div>
					<div class="step2" style="display: none;">
						<div class="donation-support">
							<span class="donation-support-title">My donation will support : <span class="donation-support-country"></span></span>
							<span class="donation-support-title">I’m giving <span class="donation-support-amount">£<span class="donation-amount"></span> <span class="month_content" ';
							if($type == 'oneoff'){ echo 'style="display:none;"'; }
							echo '>every month</span> <a href="javascript:;" onclick="backToStep1();" class="edit-icon"><img src="../img/pen.png" class="edit-main-image"></a></span></span>
						</div>
						<div class="name-part">
							<div class="name-part-inner">
								<div class="gender-selection">
									<select class="main-select2" id="donor-title" name="donor_title">
										<option value="">Title</option>
										<option value="mr">Mr</option>
										<option value="miss">Miss</option>
										<option value="ms">Ms</option>
										<option value="mrs">Mrs</option>
										<option value="dr">Dr.</option>
										<option value="prof">Prof.</option>
									</select>
								</div>
								<div class="name-part-first">
									<input type="text" name="donor_firstname" class="form-control" id="donor-firstname" name="" placeholder="First Name" autocomplete="off">
								</div>
								<div class="name-part-first">
									<input type="text" name="donor-lastname" class="form-control" id="donor-lastname" name="" placeholder="Last Name" autocomplete="off">
								</div>
							</div>
						</div>
						<div class="email-field">
							<input type="email" class="form-control" name="donor_email" id="donor-email" placeholder="Email" autocomplete="off">
						</div>
						<div class="credit-card-part card-details">
							<div class="credit-card-inner">
								<div class="credit-card-first">
									<input type="number" class="form-control" id="cardNumber1" name="cardNumber" placeholder="Card number">
									<input type="hidden" name="stripeToken" id="stripeToken" />
								</div>
								<div class="credit-card-first">
									<span class="expiration">
									    <input type="text" name="expiry_month" id="expiry_month" placeholder="MM" maxlength="2" size="2" required="true" />
									    <span class="slash">/</span>
									    <input type="text" name="expiry_year" id="expiry_year" placeholder="YYYY" maxlength="4" size="2" required="true" />
									</span>
								</div>
								<div class="credit-card-first">
									<input type="password" class="form-control" id="cvv" name="cvv" placeholder="cvc" maxlength="4">
								</div>
							</div>
						</div>
						<div class="debit-card-details bank-details">
							<div class="bank-aacount-field">
								<input type="text" class="form-control" maxlength="8" id="dd-accountnumber" name="dd_accountnumber" pattern="[0-9]{8}" placeholder="Bank account number">
							</div>
							<div class="bank-aacount-field">
								<input type="text" class="form-control" maxlength="6" id="dd-accountsortcode" name="dd_accountsortcode" pattern="[0-9]{6}" placeholder="Sort code Code">
							</div>
							<div class="bank-aacount-modal">
								<!-- Button trigger modal -->
								<button type="button" class="btn btn-primary bank-aacount-modal-btn main-pop-up-btn" data-toggle="modal" data-target="#bank-aacount-modal">
								  Read your direct debit guarantee
								</button>

							</div>
						</div>
						<div class="donate-btn">
							<input type="button" value="one last step" onclick="DonationAddToCartNewStep2();" class="text-uppercase text-center" name="">
						</div>
					</div>
					<div class="step3" style="display: none;">
						<div class="donation-support">
							<span class="donation-support-title">My donation will support : <span class="donation-support-country"></span></span>
							<span class="donation-support-title">I’m giving <span class="donation-support-amount">£<span class="donation-amount"></span> <span class="month_content" ';
							if($type == 'oneoff'){ echo 'style="display:none;"'; }
							echo '>every month</span> <a href="javascript:;" onclick="backToStep1();" class="edit-icon"><img src="../img/pen.png" class="edit-main-image"></a></span></span>
						</div>
						<div class="edit-card-details">
							<a href="javascript:;" onclick="backToStep2();" class="text-uppercase"><span class="back-icon"><img src="../img/back.png" class="back-main-image"></span>edit-card-details</a>
						</div>
						<link rel="stylesheet" type="text/css" href="https://services.postcodeanywhere.co.uk/css/captureplus-2.30.min.css?key=pj67-xk39-kp45-jg35" />
						<script type="text/javascript" src="https://services.postcodeanywhere.co.uk/js/captureplus-2.30.min.js?key=pj67-xk39-kp45-jg35"></script>
						<div class="billing-address">
							<label>Your billing address : </label>
							<input type="textarea" id="billing-address" name="billing-address" placeholder="Enter billing address">
						</div>
						<div class="email-field">
							<input type="text" class="form-control" id="billing-post-code" placeholder="Post code field" name="billing_post_code">
						</div>
						
						<div class="donor-address-hidden-fields hidden">
							<input type="text" id="pca_donor_address1" name="pca_donor_address1" value="" autocomplete="off">
							<input type="text" id="pca_donor_address2" name="pca_donor_address2" value="" autocomplete="off">
							<input type="text" id="pca_donor_address3" name="pca_donor_address3" value="" autocomplete="off">
							<input type="text" id="pca_donor_city" name="pca_donor_city" value="" autocomplete="off">
							<input type="text" id="pca_donor_postcode" name="pca_donor_postcode" value="" autocomplete="off">
							<input type="text" id="pca_donor_country" name="pca_donor_country" value="" autocomplete="off">
						</div>
						<div class="select-country" style="margin-bottom: 10px;">
							<select class="main-select2 country-selection" id="donor-country" name="donor_country" autocomplete="off"><option>Select Your Country</option>';
								foreach ($this->CountryList() as $code=>$countryname)
								{	echo '<option value="', $code, '">', $this->InputSafeString($countryname), '</option>';
								};
						echo '</select>
						</div>
						<div class="gift-aid-action" style="">
							<div class="gift-aid gift-aid-once">
								<!-- Button trigger modal -->
								<button type="button" class="btn btn-primary click-btn main-pop-up-btn" data-toggle="modal" data-target="#oneoffGiftAidModal">Claim Gift Aid to boost your donation</button>
							</div>
							<div class="gift-aid gift-aid-monthly">
								<!-- Button trigger modal -->
								<button type="button" class="btn btn-primary click-btn main-pop-up-btn" data-toggle="modal" data-target="#gift-aid-monthly">Claim Gift Aid to boost your donation</button>
								
							</div>
						</div>
						<input type="hidden" name="" id="siteurl"  value="',SITE_URL,'donate-thank-you/">
						<div class="donate-btn">
							<input type="button" value="donate" onclick="DonationAddToCartFinal();" class="text-uppercase text-center donationadd_final" name="">
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div class="description-part">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12">
						<div class="provide-text text-center">
							<h1>Your Donations HELPS TO PROVIDE...</h1>
							<h2>...regular school meals to children who may go days without food and have no education.</h2>
							<p>By providing meals in a school setting, you not only help to improve the health of these children, you also give them the opportunity to learn and start to fulfil their potential.   </p>
							<button class="find-out-btn" '; ?> onclick="window.location.href = 'https://charityright.org.uk/why-food/';" <?php echo '>Find out more</button>
						</div>		
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<input type="hidden" name="donor_giftaid" id="donor_giftaid" value="0">
		<div class="modal fade donation-modal give-once-third-step-popup" id="oneoffGiftAidModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h5 class="modal-title" id="exampleModalLabel">Boost your donation with Gift Aid</h5>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true"><img src="../img/close.png"></span>
		                </button>
		            </div>
		            <div class="modal-body">
		            	<!-- <div class="gift-increased">
		            		<p>Your gift of £X will be increased to : <span class="increased-amount">£X.XX</span></p>
		            	</div> -->
		            	<div class="tick-switch">
		            		<span class="same-address">
			            		<div class="boxes checkclaimGift">
			            			<input type="checkbox" class="claimGiftAid" id="claimGiftAid" onChange="claimGiftAidBox(this);">
									<label for="claimGiftAid">Please tick if you’d like to claim Gift Aid</label>
								</div>
							</span
		            	</div>
		            	<div class="switch-permission">
						<p class="mx-auto text-center"><img src="../img/s960_Giftaid.jpg" style="width:200px"></p>
		            		<p>By ticking the above box, I confirm I am a UK taxpayer and I would like Charity Right to treat this donation as a Gift Aid donation. I understand that if I pay less income tax and/or capital gains tax than the amount of Gift Aid claimed on all my donations in that year, it is my responsibility to pay any difference </p>
		            	</div>
		            	<div class="pop-up-name">
		            		<div class="name-part-inner">
								<div class="gender-selection">
									<select class="main-select3 donor_title_pop" >
										<option>Title</option>
										<option value="mr">Mr</option>
										<option value="miss">Miss</option>
										<option value="ms">Ms</option>
										<option value="mrs">Mrs</option>
										<option value="dr">Dr.</option>
										<option value="prof">Prof.</option>
									</select>
								</div>
								<div class="name-part-first">
									<input type="text" class="form-control donor_firstname_pop" placeholder="First Name" readonly="readonly">
								</div>
								<div class="name-part-first">
									<input type="text" class="form-control donor_lastname_pop" placeholder="Last Name" readonly="readonly">
								</div>
							</div>
		            	</div>
		            	<div class="pop-up-billing-address">
			            	<div class="billing-address">
								<!-- <label>Your billing address : </label> -->
								<textarea name="home_address" id="home-address" placeholder="Your home address" form="usrform"></textarea>
							</div>
						</div>
						<div class="post-code-field">
							<input type="text" id="post-code-field1" placeholder="Post code field" name="">
						</div>

						<div class="same-address">
							<div class="boxes">
								<input type="checkbox" id="copy_billing_add" onChange="copy_billing_add(this);">
								<label for="copy_billing_add">Home address the same as your billing address?</label>
							</div>
						</div>	
						<div class="pop-up-btn">
							<div class="inner-pop-up-btn">
								<button class="text-uppercase text-center" data-dismiss="modal">cancel</button>
								<button class="text-uppercase text-center" id="claimGiftAidBtn" onclick="claimGiftAid();">CLAIM GIFT AID</button>
							</div>
						</div>
		            </div>
		        </div>
		    </div>
		</div></div>
		<!-- GiftAidReminderModal -->
		<div class="modal fade donation-modal give-once-third-step-popup" id="giftAidReminderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
					<div class="modal-body">
						<div class="donGiftaidCheckernew"><h4>Remember as a UK tax payer, you can top up your donation by 25% at no extra cost.</h4><p><a href="#" onclick="DonorGiftaidChecker(true);" data-dismiss="modal" class="form-check-label">Yes, please add giftaid to my donation</a></p><p><a onclick="DonationAddToCartFinal();" data-dismiss="modal">No, continue without giftaid</a></p></div>
		            </div>
		        </div>
		    </div>
		</div>
		<!-- Modal -->
		<div class="modal fade donation-modal" id="bank-aacount-modal" tabindex="-1" role="dialog" aria-labelledby="bank-aacount-modalLabel" aria-hidden="true">
		  	<div class="modal-dialog" role="document">
			    <div class="modal-content">
			    	<div class="modal-header">
			    		<h5 class="modal-title" id="bank-aacount-modalLabel">Your direct debit guarantee</h5>
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          		<span aria-hidden="true"><img src="../img/close.png"></span>
		                </button>
			      	</div>
			      	<div class="modal-body">
			      		<div class="debit-card-guarantee">
			        		<div class="debit-card-guarantee-des">
							
			        			<p>This guarantee is offered by all banks and building societies that accept instructions to pay direct debits.</p><p>If there are any changes to the amount, date or frequency of your direct debit, Eazy Collect (on behalf of Charity Right) will notify you 10 working days in advance of your account being debited or as otherwise agreed. If you request Eazy Collect (on behalf of Charity Right) to collect a payment, Eazy Collect will confirm the amount and date at the time of the request.</p><p>If an error is made in the payment of your direct debit, either by Eazy Collect (on behalf of Charity Right), or by your bank or building society, you are entitled to a full and immediate refund of the amount paid from your bank or building society.</p>
								<p>If you receive a refund you are not entitled to, you must pay it back when Eazy Collect (on behalf of Charity Right) asks you to.</p><p>You can cancel a direct debit at any time by simply contacting your bank or building society. Written confirmation may be required. Please also notify <a href="',SITE_SUB,'/who-we-are/#contact-us">Charity Right.</a></p>
			        		</div>
			        	</div>
			      	</div>
			    </div>
		 	</div>
		</div>

		<!-- Modal -->
		<div class="modal fade donation-modal step-third-monthly" id="gift-aid-monthly" tabindex="-1" role="dialog" aria-labelledby="gift-aid-monthlyLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h5 class="modal-title" id="gift-aid-monthlyLabel">Boost your donation with Gift Aid</h5>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true"><img src="../img/close.png"></span>
		                </button>
		            </div>
		            <div class="modal-body">
		            	<!-- <div class="gift-increased">
		            		<p>Every month,  your gift of £X will be increased to : <span class="increased-amount">£X.XX</span></p>
		            	</div> -->
		            	<div class="tick-switch">
		            		<span class="same-address">
		            			<div class="boxes checkclaimGift">
		            				<input type="checkbox" class="claimGiftAid" id="claimGiftAid2" onChange="claimGiftAidBox(this);">
									<label for="claimGiftAid2">Please tick if you’d like to claim Gift Aid</label>
								</div>
							</span>
		            	</div>
		            	<div class="switch-permission">
						
		            		<p>Yes, I am a UK taxpayer and I would like Charity Right to treat this donation and future donations associated with this subscription as Gift Aid donations. I understand that if I pay less income tax and/or capital gains tax than the amount of Gift Aid claimed on all my donations in that year, it is my responsibility to pay any difference .
							</p>
		            	</div>
		            	<div class="pop-up-name">
		            		<div class="name-part-inner">
								<div class="gender-selection">
									<select class="main-select3 donor_title_pop" >
										<option>Title</option>
										<option value="mr">Mr</option>
										<option value="miss">Miss</option>
										<option value="ms">Ms</option>
										<option value="mrs">Mrs</option>
										<option value="dr">Dr.</option>
										<option value="prof">Prof.</option>
									</select>
								</div>
								<div class="name-part-first">
									<input type="text" class="form-control donor_firstname_pop" placeholder="First Name" readonly="readonly">
								</div>
								<div class="name-part-first">
									<input type="text" class="form-control donor_lastname_pop" placeholder="Last Name" readonly="readonly">
								</div>
							</div>
		            	</div>
		            	<div class="pop-up-billing-address">
			            	<div class="billing-address">
								<!-- <label>Your billing address : </label> -->
								<textarea name="home_address_monthly" id="home-address-monthly" placeholder="Your home address" form="usrform"></textarea>
							</div>
						</div>
						<div class="post-code-field">
							<input type="text" id="post-code-field2" placeholder="Post code field" name="">
						</div>
						<div class="same-address">
							<div class="boxes">
								<input type="checkbox" id="copy_billing_add2" onChange="copy_billing_add(this);">
								<label for="copy_billing_add2">Home address the same as your billing address?</label>
							</div>
						</div>	
						<div class="pop-up-btn">
							<div class="inner-pop-up-btn">
								<button class="text-uppercase text-center" data-dismiss="modal">cancel</button>
								<button class="text-uppercase text-center" id="claimGiftAidBtn" onclick="claimGiftAid();">CLAIM GIFT AID</button>
							</div>
						</div>
		            </div>
		        </div>
		    </div>
		</div>
		<script type="text/javascript">$().ready(function(){ initAutocomplete();	});</script>
		';
		if ($this->do_cart)
		{	// modal pop-up
			echo '<script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$(".cartModalPopup").jqm();});</script>',
			'<!-- START banners list modal popup --><div class="jqmWindow cartModalPopup"><a href="#" class="jqmClose cartCloseTop">&times;</a><div class="cart_modal_popup_inner donpage"></div></div>';
		}
		return ob_get_clean();
	}

	public function DonationAmountContainer($type = '', $amount = 0, $country = '', $project = '', $project2 = '', $quantity = 0, $was_fixed = false, $currency = 'GBP')
	{	ob_start();
		if ($countries = $this->donation->GetDonationCountries($type, true))
		{	if ($this->IsFixedPrice($countries, $type, $country, $project, $project2, $currency))
			{	if (!$quantity = (int)$quantity)
				{	$quantity = 1;
				}
				$cursymbol = $this->GetCurrency($currency, 'cursymbol');
				echo '<input type="hidden" id="donation-amount-fixed" name="donation_amountfixed" value="', number_format($amount / $quantity, 2, '.', ''), '" /><input type="hidden" id="donation-amount" name="donation_amount" value="', number_format($amount, 2, '.', ''), '" />
						<input type="hidden" id="donation-quantity" class="donation-quantity" name="donation_quantity" value="', $quantity, '" />
						<div class="visible-lg">
							<span class="donation_amount_display">', number_format($amount, 2), '</span>
							<span class="donation_amount_container_break">= ', $cursymbol, '</span>
							<div class="donation_quantity_changer">
								<a class="donation_quantity_changer_minus', ($quantity > 1) ? '' : ' donation_quantity_changer_hidden', '" onclick="DonateQuantityIncrement(-1);">-</a><div class="dqcNumber">', $quantity, '</div><a class="donation_quantity_changer_plus" onclick="DonateQuantityIncrement(1);">+</a>';
							
				echo 		'</div>
							<span class="dqcAmountPer">', $cursymbol, number_format($amount / $quantity, 2), ' &times; </span>	
						</div>';
				echo '<div class="hidden-lg">
						<div class="donation_amount_container_qty_and_per">
						<span class="dqcAmountPer">', $cursymbol, number_format($amount / $quantity, 2), ' &times; </span>	
						<div class="donation_quantity_changer">
							<a class="donation_quantity_changer_minus', ($quantity > 1) ? '' : ' donation_quantity_changer_hidden', '" onclick="DonateQuantityIncrement(-1);">-</a><div class="dqcNumber">', $quantity, '</div><a class="donation_quantity_changer_plus" onclick="DonateQuantityIncrement(1);">+</a><div class="clear"></div>';
						
				echo '	</div>
						</div>
						<div class="clear"></div><div class="donation_amount_container_total"><span class="donation_amount_container_break">= ', $cursymbol, '</span>
						<span class="donation_amount_display">', number_format($amount, 2), '</span></div>
					</div>';
			} else
			{	if ($was_fixed)
				{	$amount = 0;
				}
				echo '<input type="number" id="donation-amount" name="donation_amount" class="donation-amount number inputSelectOnFocus" value="', number_format($amount, 2, '.', ''), '" onchange="DonateAmountChangeNew();" step=".01" min=".01" required />';
			}
		}
		return ob_get_clean();
	} // end of fn DonationAmountContainer

	public function DonationCountriesListNew($type = '', &$country_chosen = '')
	{	ob_start();
		if ($countries = $this->donation->GetDonationCountries($type, true))
		{	echo '<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation-country" class="title-label">Which causes would you like to support?</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner">
							<div class="custom-select-box">
								<select id="donation-country" class="donation-country" name="donation_country" onchange="DonateCountryChangeNew();">';
								foreach ($countries as $country=>$cdetails) {
									if (!$country_chosen) {
										$country_chosen = $country;
									}
									echo '<option value="'.$this->InputSafeString($country).'"'.(($country == $country_chosen) ? ' selected="selected"' : "").'>'.$this->InputSafeString($cdetails['shortname']).'</option>';
								}
							echo '</select>
								<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<div class="form-row-separator clearfix"></div>
						</div>
					</div>
				</div>';
		}
		return ob_get_clean();
	} // end of fn DonationCountriesListNew

	public function DonationProjectsListNew($type = '', $country = '', $donation_project = '')
	{	ob_start();
		$countries = $this->donation->GetDonationCountries($type, true);
		if ($projects = $countries[$country]['projects'])
		{	if (false && (count($projects) == 1))
			{	foreach ($projects as $project=>$details)
				{	echo '<input type="hidden" name="donation_project" value="', $project, '" />';
				}
			} else
			{	echo '<div class="form-row">
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<label for="donation-project" class="title-label">', $this->InputSafeString($countries[$country]['formlabel']), '</label>
							</div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box">
									<select id="donation-project" class="donation-project" name="donation_project" onchange="DonateProjectChangeNew();">';
				foreach ($projects as $project=>$details)
				{
					echo '<option value="', $project, '"', ($project == $donation_project) ? ' selected="selected"' : '', '>', $this->InputSafeString($details['projectname']), '</option>';
				}
				echo '				</select>
									<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner">
								<div class="form-row-separator clearfix"></div>
							</div>
						</div>
					</div>';
			}
		}
		return ob_get_clean();
	} // end of fn DonationProjectsListNew

	public function DonationProjects2ListNew($type = '', $country = '', $donation_project = '', $donation_project2 = '')
	{	ob_start();
		$countries = $this->donation->GetDonationCountries($type, true);
	//	$this->VarDump($countries[$country]['projects'][$donation_project]);
	//	echo $donation_project;
		if ($projects = $countries[$country]['projects'][$donation_project]['projects'])
		{
			if (false && (count($projects) == 1))
			{	foreach ($projects as $project=>$details)
				{	echo '<input type="hidden" name="donation_project2" value="', $project, '" />';
				}
			} else
			{	echo '<div class="form-row">
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<label for="donation-project2" class="title-label">', $this->InputSafeString($countries[$country]['projects'][$donation_project]['formlabel']), '</label>
							</div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box">
									<select id="donation-project2" class="donation-project2" name="donation_project2" onchange="DonateProject2Change();">';
				foreach ($projects as $project=>$details)
				{
					echo '<option value="', $project, '"', ($project == $donation_project2) ? ' selected="selected"' : '', '>', $this->InputSafeString($details['projectname']), '</option>';
				}
				echo '				</select>
									<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner">
								<div class="form-row-separator clearfix"></div>
							</div>
						</div>
					</div>';
			}
		} //else echo 'no projects found';
		return ob_get_clean();
	} // end of fn DonationProjects2ListNew

	protected function CurrencyChooserNew($type = '', $currency_selected = '')
	{	ob_start();
		if ($currencies = $this->GetCurrencies($type))
		{	if (!$currencies[$currency_selected])
			{	$currency_selected = false;
			}
			echo '<div class="custom-select-box"><select id="donation_currency" name="donation_currency" onchange="DonationCurrencyChangeNew();">';
			foreach ($currencies as $curcode=>$currency) {
				if (!$currency_selected)
				{	$currency_selected = $curcode;
				}
				echo '<option value="', $curcode, '"', $curcode == $currency_selected ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
			}
			echo '</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>';
		}
		return ob_get_clean();
	} // end of fn CurrencyChooserNew

	protected function GetCurrencies($type = '')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies

	protected function PresetsList($currency = '', $amount = false, $donation_type = '')
	{	ob_start();
		if (!$donation_type)
		{	$donation_type = $_SESSION[$this->session_name]['donation_type'];
		}
		if (!$currency)
		{	$currency = $_SESSION[$this->session_name]['donation_currency'];
		}
		if ($amount === false)
		{	$amount = $_SESSION[$this->session_name]['donation_amount'];
		}
		$cursymbol = $this->GetCurrency($currency, 'cursymbol');
		if ($presets = $this->GetPresets($donation_type))
		{	echo '<ul>';
			foreach ($presets as $id=>$preset)
			{	if ($currency != 'GBP')
				{	$preset['amount'] = round($preset['amount'] * $this->GetCurrency($currency, 'convertrate'));
				}
				echo '<li onclick="DonatePresetClick(', number_format($preset['amount'], 2, '.', ''), ',', ++$preset_count, ', \'', $this->InputSafeString($donation_type), '\');"', $preset['amount'] == $amount ? ' class="presets_selected"' : '', '><div class="presets_list_top"><span>', $cursymbol, $preset['amount'], '</span></div><div class="presets_list_bottom">';
				if ($preset['heading'])
				{	echo '<h3>', $this->InputSafeString($preset['heading']), '</h3>';
				}
				echo '<p>', $this->InputSafeString($preset['text']), '</p></div></li>';
			}
			echo '</ul><div class="clear"></div>';
		}
		return ob_get_clean();
	} // end of fn PresetsList

	protected function PresetsListNew($currency = '', $amount = false, $donation_type = '')
	{	return '';
		ob_start();
		if (!$donation_type)
		{	$donation_type = $_SESSION[$this->session_name]['donation_type'];
		}
		if (!$currency)
		{	$currency = $_SESSION[$this->session_name]['donation_currency'];
		}
		if ($amount === false)
		{	$amount = $_SESSION[$this->session_name]['donation_amount'];
		}
		$cursymbol = $this->GetCurrency($currency, 'cursymbol');
		if ($presets = $this->GetPresets($donation_type))
		{
			foreach ($presets as $id=>$preset) {
			if ($currency != 'GBP')
			{	$preset['amount'] = round($preset['amount'] * $this->GetCurrency($currency, 'convertrate'));
			}
			echo '<div class="form-col form-col-3">';
				echo '<div class="form-col-inner">';
					$preset_amount = number_format($preset['amount'], 2, '.', '');
					$donation_type = $this->InputSafeString($donation_type);
					++$preset_count;

					echo '<a href="#donation-preset-'.$preset_count.'" id="donation-preset-'.$preset_count.'" class="donation-preset '.(($preset['amount'] == $amount) ?'donation-presets-selected':"").'" onclick="DonatePresetClickNew('.$preset['amount'].','.$preset_count.',\''.$donation_type.'\'); return false;" >';
						echo '<span class="donation-preset-inner">';
							echo '<span class="donation-preset-top">';
								echo '<img src="'.SITE_URL.'img/template/donate-'.$preset_count.'.jpg" alt="'.$this->InputSafeString($preset['text']).'">';
							echo '</span>';
							echo '<span class="donation-preset-bottom">';
								if ($preset['heading']) {
								echo '<span class="donation-preset-title">';
									echo $this->InputSafeString($preset['heading']);
								echo '</span>';
								}
								echo '<span class="donation-preset-text">';
									echo $this->InputSafeString($preset['text']);
								echo '</span>';
								echo '<span class="donation-preset-button">';
									echo '<span class="button">Select</span>';
								echo '</span>';
							echo '</span>';
							echo '<span class="donation-preset-amount">', $cursymbol, $preset['amount'], '</span>';
						echo '</span>';
					echo '</a>';
				echo '</div>';
			echo '</div>';
			}
		}
		return ob_get_clean();
	} // end of fn PresetsList

	public function PersonalStageContent()
	{	ob_start();
		//$this->VarDump($this->CountryList());
		$pref = new ContactPreferences();
		$pp_page = new PageContent('use-of-cookies');
		$add_lines = array();
		if ($_SESSION[$this->session_name]['pca_donor_address1'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_address2'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_address3'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_city'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_postcode'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_country'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_country']);
		}
		echo '<form action="', $this->pagecontent->Link(), '" method="post" id="donatePersonalStageForm" class="donation-form donation-stage-2 form-rows"><input type="hidden" name="donate_submit_stage" value="2" />';
		if ($_SESSION[$this->session_name]['donation_type'] == 'monthly')
		{	echo '<img src="', SITE_URL, 'img/template/direct-debit-logo.png" alt="Direct Debit" class="direct-debit-logo">';
		}
		echo '	<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-title" class="title-label">Title <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-title" name="donor_title" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_title']), '" required /></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-firstname" class="title-label">First name <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-firstname" name="donor_firstname" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_firstname']), '" required /></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-lastname" class="title-label">Last name <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-lastname" name="donor_lastname" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_lastname']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-country" class="title-label">Country <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box"><select id="donor-country" name="donor_country" onchange="DonorCountryChangeNew();" required>';
		if ($_SESSION[$this->session_name]['donation_type'] == 'monthly')
		{	echo '<option value="GB">', $this->GetCountry($_SESSION[$this->session_name]['donor_country'] = 'GB', 'shortname'), '</option>';
		} else
		{	echo '<option value=""></option>';
			foreach ($this->CountryList() as $code=>$countryname)
			{	echo '<option value="', $code, '"', ($code == $_SESSION[$this->session_name]['donor_country']) ? ' selected="selected"' : "", '>', $this->InputSafeString($countryname), '</option>';
			}
		}
		echo '				</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>
							</div>
						</div>
					</div>
					<div class="form-row donorAddressPCA ', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? '' : 'hidden', '">
						<link rel="stylesheet" type="text/css" href="https://services.postcodeanywhere.co.uk/css/captureplus-2.30.min.css?key=pj67-xk39-kp45-jg35" />
						<script type="text/javascript" src="https://services.postcodeanywhere.co.uk/js/captureplus-2.30.min.js?key=pj67-xk39-kp45-jg35"></script>
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="pcaAddress" class="title-label">Address<span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="pcaAddressContainer clearfix ', ($_SESSION[$this->session_name]['pca_donor_address1'] && $_SESSION[$this->session_name]['pca_donor_city'] && $_SESSION[$this->session_name]['pca_donor_postcode']) ? 'hidden' : '', '">
									<input type="text" id="pcaAddress" name="pcaAddress" value="" placeholder="Start typing a postcode, street or address" />
								</div>
								<div class="donor-address-hidden-fields hidden">
									<input type="text" id="pca_donor_address1" name="pca_donor_address1" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']), '" autocomplete="off" />
									<input type="text" id="pca_donor_address2" name="pca_donor_address2" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']), '" autocomplete="off" />
									<input type="text" id="pca_donor_address3" name="pca_donor_address3" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']), '" autocomplete="off" />
									<input type="text" id="pca_donor_city" name="pca_donor_city" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']), '" autocomplete="off" />
									<input type="text" id="pca_donor_postcode" name="pca_donor_postcode" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']), '" autocomplete="off" />
									<input type="text" id="pca_donor_country" name="pca_donor_country" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_country']), '" autocomplete="off" />
								</div>
								<div class="donor-address-box clearfix ', ($_SESSION[$this->session_name]['pca_donor_address1'] && $_SESSION[$this->session_name]['pca_donor_city'] && $_SESSION[$this->session_name]['pca_donor_postcode']) ? '' : 'hidden', '">
									<div class="donor-address-box-content" name="pca_donor_all_fields">', implode('<br />', $add_lines), '</div>
									<div class="address-search-again"><a href="#pcaAddress" class="button">Search Address Again</a></div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row donorAddressManual', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? ' hidden' : '', '">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-address1" class="title-label">Address<span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<input type="text" id="donor-address1" name="donor_address1" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_address1']), '" placeholder="Address Line 1" />
								<input type="text" name="donor_address2" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_address2']), '" placeholder="Address Line 2" />
								<input type="text" name="donor_city" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_city']), '" placeholder="Town / City" />
								<input type="text" name="donor_postcode" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_postcode']), '" placeholder="Postcode" />
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-email" class="title-label">Email address <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="email" id="donor-email" name="donor_email" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_email']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-phone" class="title-label">Phone number <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-phone" name="donor_phone" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_phone']), '" required /></div>
						</div>
					</div>';
			if (true || SITE_TEST)
			{	echo '<div class="form-row crRegFormContactPref">
						<p class="cprefTitle">How would you like to receive updates?<br /><span>(Optional)</span></p>
						<p class="cprefHelp">We\'d love to tell you about how your donation helps and keep you in the loop with what Charity Right is up to. Your details will be kept safe and never shared with other organisations. See our <a href="', $pp_page->Link(), '">privacy policy</a>.</p>
						<p>', $pref->FormElementsList($_SESSION[$this->session_name]['contactpref']), '</p>
						<p class="cprefHelp">Don\'t forget, you can change your preferences at any time by emailing us at info@charityright.org.uk.</p>
					</div>
					';
			}
			if ($_SESSION[$this->session_name]['donation_type'] == 'monthly')
			{	echo '
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
						</div>
					</div>
					<div class="direct-debit-container clearfix"><img src="', SITE_URL, 'img/template/direct-debit-logo.png" alt="Direct Debit" class="direct-debit-logo">
						<h3>Direct Debit details</h3>
						<p>You have chosen to set up a Variable Direct Debit. Subject to your rights under the Direct Debit Guarantee this will automatically debit payments due from your bank or building society account. If you are a joint signatory on the account, a direct debit instruction will be sent to you by email.</p>
						<div class="form-row ">
							<div class="form-col form-col-4">
								<div class="form-col-inner"><label for="dd-accountname" class="title-label">Account name <span class="required">*</span></label></div>
							</div>
							<div class="form-col form-col-2">
								<div class="form-col-inner"><input type="text" id="dd-accountname" name="dd_accountname" value="', $this->InputSafeString($_SESSION[$this->session_name]['dd_accountname']), '" required /></div>
							</div>
						</div>
						<div class="form-row ">
							<div class="form-col form-col-4">
								<div class="form-col-inner"><label for="dd-accountnumber" class="title-label">Account number <span class="required">*</span></label></div>
							</div>
							<div class="form-col form-col-2">
								<div class="form-col-inner"><input type="text" maxlength="8" id="dd-accountnumber" name="dd_accountnumber" value="', $this->InputSafeString($_SESSION[$this->session_name]['dd_accountnumber']), '" pattern="[0-9]{8}" title="Invalid account number (must be 8 numbers)" required /></div>
							</div>
						</div>
						<div class="form-row ">
							<div class="form-col form-col-4">
								<div class="form-col-inner"><label for="dd-accountsortcode" class="title-label">Account sort code <span class="required">*</span></label></div>
							</div>
							<div class="form-col form-col-2">
								<div class="form-col-inner"><input type="text" maxlength="6" id="dd-accountsortcode" name="dd_accountsortcode" value="', $this->InputSafeString($_SESSION[$this->session_name]['dd_accountsortcode']), '" pattern="[0-9]{6}" title="Invalid sort code number (must be 6 numbers)"  required /></div>
							</div>
						</div>
						<div class="form-row ">
							<div class="form-col form-col-4">
								<div class="form-col-inner"><label for="dd-confirm" class="title-label">&nbsp;</label></div>
							</div>
							<div class="form-col form-col-2">
								<div class="form-col-inner">
									<div class="custom-checkbox clearfix"><input type="checkbox" name="dd_confirm" id="dd-confirm" value="1" ', $_SESSION[$this->session_name]['dd_confirm'] ? 'checked="checked"' : "", ' /><label for="dd-confirm">&nbsp;Please tick to confirm you are the account holder</label></div>
									<div class="form-help-text clearfix">Checking the box indicates that I am the Bank/Building Society account holder and no authority other than my own is required. If more than one person is required to authorise Debits from the account, leave this box unchecked and we will send you a paper Direct Debit Instruction for completion.</div>
									<div class="custom-checkbox clearfix">';
		$tc_page = new PageContent('terms-conditions');
		echo 							'<input type="checkbox" name="tc_confirm" id="tc-confirm" value="1" ', ($_SESSION[$this->session_name]['tc_confirm']) ? 'checked="checked"' : '', ' /><label for="tc-confirm">&nbsp;I understand and agree to the <a href="', $tc_page->Link(), '">Terms and Conditions</a></label>
									</div>
								</div>
							</div>
						</div>
					</div>';
			}
			echo '<div class="form-row donor-giftaid-box ', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? '' : ' hidden', '">
						<div class="form-col form-col-1">
							<div class="form-col-inner">
								<div class="alert-box alert-box-green">
									<h3>Reclaim Gift Aid - Add an extra <strong>', $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol'), number_format(($_SESSION[$this->session_name]['donation_amount'] + $_SESSION[$this->session_name]['donation_admin']) * 0.25, 2), '</strong>', $_SESSION[$this->session_name]['donation_type'] == 'monthly' ? ' per month' : '', ' to your donation at no extra cost to you.</h3>
									<p>&nbsp;</p><input type="hidden" name="donor_giftaidchecked" value="" />
									<div class="custom-checkbox clearfix"><input type="checkbox" name="donor_giftaid" id="donor-giftaid" value="1" ', $_SESSION[$this->session_name]['donor_giftaid'] ? 'checked="checked"' : '', '><label for="donor-giftaid">&nbsp;I want to Gift Aid my donation and any donations I make in the future or have made in the past 4 years to Charity Right.</label></div>
									<div class="form-help-text clearfix"><strong>By ticking this box you are confirming that:</strong><br />"I am a UK taxpayer and understand that if I pay less Income Tax and/or Capital Gains Tax than the amount of Gift Aid claimed on all my donations in that tax year it is my responsibility to pay any difference. I understand that other taxes such as VAT and Council Tax do not qualify. I understand the charity will reclaim 28p of tax on every &pound;1 that I gave up to 5 April 2008 and will reclaim 25p of tax on every &pound;1 that I give on or after 6 April 2008."</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner"><input type="submit" value="Next" class="button" /></div>
						</div>
					</div>';

			if ($this->giftaid_unticked)
			{	echo '<div class="donGiftaidChecker"><h4>Remember as a UK tax payer, you can top up your donation by 25% at no extra cost.</h4><p><a onclick="DonorGiftaidChecker(true);">Yes, please add giftaid to my donation</a></p><p><a onclick="DonorGiftaidChecker(false);">No, continue without giftaid</a></p></div>';
			}

		echo '</form>';
		return ob_get_clean();
	} // end of fn PersonalStageContent

	public function ConfirmationStageContent()
	{	ob_start();
		$dtypes = $this->donation->GetDonationTypes();
		$donation_countries = $this->donation->GetDonationCountries($_SESSION[$this->session_name]['donation_type']);
		$cursymbol = $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol');

		$address = array();
		if (isset($_SESSION[$this->session_name]['donor_address1']) && ($_SESSION[$this->session_name]['donor_country'] != 'GB'))
		{	if ($_SESSION[$this->session_name]['donor_address1'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_address1']);
			}
			if ($_SESSION[$this->session_name]['donor_address2'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_address2']);
			}
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_city']);
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_postcode']);
		} else
		{	if ($_SESSION[$this->session_name]['pca_donor_address1'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']);
			}
			if ($_SESSION[$this->session_name]['pca_donor_address2'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']);
			}
			if ($_SESSION[$this->session_name]['pca_donor_address3'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']);
			}
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']);
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']);
		}
		$address[] = $this->InputSafeString($this->GetCountry($_SESSION[$this->session_name]['donor_country']));
		
		echo '<form action="', $this->pagecontent->Link(), '" method="post" class="donation-form donation-stage-4 form-rows"><input type="hidden" name="donate_submit_stage" value="4" />
				<div class="form-row donation-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<h3>About Your Donation</h3>
							<p>You will make ', $this->InputSafeString($dtypes[$_SESSION[$this->session_name]['donation_type']]['label']), $_SESSION[$this->session_name]['donation_zakat'] ? ' Zakat' : '', ' donation of ', $cursymbol, number_format($_SESSION[$this->session_name]['donation_amount'], 2);
		if ($_SESSION[$this->session_name]['donation_admin'])
		{	echo ' plus ', $cursymbol, number_format($_SESSION[$this->session_name]['donation_admin'], 2), ' for our administration costs, making a total of ', $cursymbol, number_format($_SESSION[$this->session_name]['donation_admin'] + $_SESSION[$this->session_name]['donation_amount'], 2);
		}
		echo '.</p>', 
							$_SESSION[$this->session_name]['donor_giftaid'] ? '<p>We will claim Gift Aid on your donation.</p>' : '';
		if ($_SESSION[$this->session_name]['donation_country'])
		{	echo '		<p>Cause selected: ', $this->InputSafeString($donation_countries[$_SESSION[$this->session_name]['donation_country']]['shortname']);
			if (!$_SESSION[$this->session_name]['donation_project'] && $_SESSION[$this->session_name]['donation_quantity'])
			{	echo ' &times; ', (int)$_SESSION[$this->session_name]['donation_quantity'];
			}
			echo '.</p>';
		}
		if ($_SESSION[$this->session_name]['donation_project'])
		{	echo '		<p>', $this->InputSafeString($donation_countries[$_SESSION[$this->session_name]['donation_country']]['formlabel']), ': ', $this->InputSafeString($donation_countries[$_SESSION[$this->session_name]['donation_country']]['projects'][$_SESSION[$this->session_name]['donation_project']]['projectname']);
			if (!$_SESSION[$this->session_name]['donation_project2'] && $_SESSION[$this->session_name]['donation_quantity'])
			{	echo ' &times; ', (int)$_SESSION[$this->session_name]['donation_quantity'];
			}
			echo '.</p>';
		}
		if ($_SESSION[$this->session_name]['donation_project2'])
		{	echo '		<p>', $this->InputSafeString($donation_countries[$_SESSION[$this->session_name]['donation_country']]['projects'][$_SESSION[$this->session_name]['donation_project']]['formlabel']), ': ', $this->InputSafeString($donation_countries[$_SESSION[$this->session_name]['donation_country']]['projects'][$_SESSION[$this->session_name]['donation_project']]['projects'][$_SESSION[$this->session_name]['donation_project2']]['projectname']);
			if ($_SESSION[$this->session_name]['donation_quantity'])
			{	echo ' &times; ', (int)$_SESSION[$this->session_name]['donation_quantity'];
			}
			echo '.</p>';
		}
		echo '			<p><a href="', $this->pagecontent->Link(), '1/" class="button button-gray-outline">amend donation details</a></p>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row donation-your-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<h3>Your Details</h3>
							<p>', $this->InputSafeString($_SESSION[$this->session_name]['donor_title']), ' ', $this->InputSafeString($_SESSION[$this->session_name]['donor_firstname']), ' ', $this->InputSafeString($_SESSION[$this->session_name]['donor_lastname']), '</p>
							<p>', implode('<br />', $address), '</p>
							<p>Email: ', $this->InputSafeString($_SESSION[$this->session_name]['donor_email']), '</p>';
		if ($_SESSION[$this->session_name]['donor_phone'])
		{	echo '		<p>Phone number: ', $this->InputSafeString($_SESSION[$this->session_name]['donor_phone']), '</p>';
		}
		echo '			<p><a href="', $this->pagecontent->Link(), '2/" class="button button-gray-outline">amend your details</a></p>
						</div>
					</div>
				</div>';
		if ($_SESSION[$this->session_name]['donation_type'] == 'monthly')
		{	echo '
				<div class="form-row">
					<div class="form-col form-col-1"><div class="form-col-inner"><div class="form-row-separator clearfix"></div></div></div>
				</div>
				<div class="form-row direct-debit-account-details">
					<div class="form-col form-col-1"><img src="', SITE_URL, 'img/template/direct-debit-logo.png" alt="Direct Debit" class="direct-debit-logo">
						<div class="form-col-inner">
							<h3>Direct Debit - Account Details</h3>
							<p>Service user number: ', $this->InputSafeString(EZC_SUN), '</p>
							<p>Account name: ', $this->InputSafeString($_SESSION[$this->session_name]['dd_accountname']), '</p>
							<p>Account number: ', $this->InputSafeString($_SESSION[$this->session_name]['dd_accountnumber']), '</p>
							<p>Account sort code: ', $this->SortCodeDisplayString($_SESSION[$this->session_name]['dd_accountsortcode']), '</p>
							<p>Are you the only person to sign for this account: ', $_SESSION[$this->session_name]['dd_confirm'] ? 'Yes' : 'No', '</p>
							<p><a href="', $this->pagecontent->Link(), '2/" class="button button-gray-outline">amend your direct debit details</a></p>
						</div>
					</div>
				</div>
				<div class="form-row direct-debit-guarantee">
					<div class="form-col form-col-1"><img src="', SITE_URL, 'img/template/direct-debit-logo.png" alt="Direct Debit" class="direct-debit-logo">
						<div class="form-col-inner">
							<h3>The Direct Debit Guarantee</h3>
							<p>This Guarantee is offered by all banks and building societies that accept instructions to pay Direct Debits.</p>
							<p>If there are any changes to the amount, date or frequency of your Direct Debit ', EZC_SUNNAME, ' will notify you 10 working days in advance of your account being debited or as otherwise agreed. If you request ', EZC_SUNNAME, ' to collect a payment, confirmation of the amount and date will be given to you at the time of the request.</p>
							<p>If an error is made in the payment of your Direct Debit, by ', EZC_SUNNAME, ' or your bank or building society, you are entitled to a full and immediate refund of the amount paid from your bank or building society.<ul><li>If you receive a refund you are not entitled to, you must pay it back when ', EZC_SUNNAME, ' asks you to.</p>
							<p>You can cancel a Direct Debit at any time by simply contacting your bank or building society. Written confirmation may be required. Please also notify us.</p>
						</div>
					</div>
				</div>';
		}
		$tc_page = new PageContent('terms-conditions');
		echo '<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row donation-terms-and-conditions">
					<div class="form-col form-col-1">
						<div class="form-col-inner" style="text-align: center;"><p>By making your donation you are confirming that you accept our <a href="', $tc_page->Link(), '" target="_blank">terms and conditions</a>.</p></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner" style="text-align: center;"><input type="submit" value="Make Your Donation" class="button" /></div>
					</div>
				</div>
			</form>';
		return ob_get_clean();
	} // end of fn ConfirmationStageContent

	public function PaymentStageContent()
	{	ob_start();
		if ($this->donation->CanBePaid())
		{	switch ($this->donation->details['donationtype'])
			{	case 'oneoff':
					echo $this->donation->PaymentButton(), $this->donation->GATransactionTrackingCode(), '<div class="gatewayRedirect">please wait ... redirecting to Worldpay</div>';
					break;
				case 'monthly':
					if ($this->donation->details['ddsolesig'])
					{	echo '<h3>Based on the answers you have provided, we are unable to set your Direct Debit up online. We will email you a form which you can print out to complete and sign. Please return this form to us as soon as possible.</h3>';
					}
					break;
			}
		} else
		{	switch ($_SESSION[$this->session_name]['donation_type'])
			{	case 'oneoff':
					break;
				case 'monthly':
					if ($this->donation->id)
					{	if ($this->donation->details['donationref'])
						{	echo '<div class="donateDDCompleted"><h2>Direct Debit - Complete</h2>
								<p>You have successfully set up a direct debit for your account.</p>
								<p><span class="ddCompleteLabel">Date</span><span class="ddCompleteContent">', date('j M Y', strtotime($this->donation->details['donationconfirmed'])), '</span></p>
								<p><span class="ddCompleteLabel">Your reference</span><span class="ddCompleteContent">', $this->InputSafeString($this->donation->extrafields['DirectDebitRef']), '</span></p>
								<p><span class="ddCompleteLabel">Service user number</span><span class="ddCompleteContent">', $this->InputSafeString(EZC_SUN), '</span></p>
								<p><span class="ddCompleteLabel">Account holder name</span><span class="ddCompleteContent">', $this->InputSafeString($this->donation->extrafields['AccountHolder']), '</span></p>
								<p><span class="ddCompleteLabel">Account number</span><span class="ddCompleteContent">', $this->InputSafeString($this->donation->extrafields['AccountNumber']), '</span></p>
								<p><span class="ddCompleteLabel">Account sort code</span><span class="ddCompleteContent">', $this->InputSafeString($this->donation->extrafields['AccountSortCode']), '</span></p>
								<p><span class="ddCompleteLabel">First payment</span><span class="ddCompleteContent">On or after ', date('j M Y', strtotime($this->donation->extrafields['StartDate'])), '</span></p>
								<p>The company name that will appear on your bank statement against the direct debits will be "', EZC_SHORTNAME, '".</p>
								<p>Your Direct Debit Instruction will be confirmed to you by email or post within 5 working days. We will also notify you of the amount being collected at least 10 working days prior to the first collection. Any changes to the frequency or amount of your collections will be advised to you 10 working days in advance.</p>
								<h3>Instruction to your Bank or Building Society.</h3>
								<p>Please pay ', EZC_SUNNAME, ' from the account detailed in this instruction subject to the safeguards assured by the Direct Debit Guarantee. I understand that this instruction may remain with ', EZC_SUNNAME, ' and, if so, details will be passed electronically to my Bank / Building Society.</p>
								</div>';
						} else
						{	if ($this->donation->details['ddsolesig'])
							{	echo '<p>Based on the answers you have provided, we are unable to set your Direct Debit up online. We will email you a form which you can print out to complete and sign. Please return this form to us as soon as possible.</p>';
							}
						}
					//	$this->VarDump($this->donation->details);
					//	$this->VarDump($this->donation->extrafields);
						if (!SITE_TEST)
						{	$_SESSION[$this->session_name] = array();
						}
					}
				//	$this->VarDump($this->donation);
					break;
			}
		}
		return ob_get_clean();
	} // end of fn PaymentStageContent

	public function SortCodeDisplayString($sortcode = '')
	{	if (preg_match('|^[0-9]{6}$|', $sortcode))
		{	return implode('-', str_split($sortcode, 2));
		} else
		{	return $this->InputSafeString($sortcode);
		}
	} // end of fn SortCodeDisplayString

} // end of defn DonatePage
?>