<?php
class ContactPreferences extends Base
{	private $options = array();
	
	public function __construct($to_use = false)
	{	parent::__construct();
		$this->GetPreferenceList($to_use);
	} //  end of fn __construct

	private function GetPreferenceList($to_use = false)
	{	$this->options = array();
		$this->options[1] = array(
				'cpid'=>1,
				'cpvalue'=>0,
				'cpcode'=>'email',
				'cplabel'=>'Email',
				'listorder'=>10
			);
		$this->options[2] = array(
				'cpid'=>2,
				'cpvalue'=>1,
				'cpcode'=>'phone',
				'cplabel'=>'Phone',
				'listorder'=>20
			);
		$this->options[3] = array(
				'cpid'=>3,
				'cpvalue'=>2,
				'cpcode'=>'text',
				'cplabel'=>'Text',
				'listorder'=>30
			);
		$this->options[4] = array(
				'cpid'=>4,
				'cpvalue'=>3,
				'cpcode'=>'post',
				'cplabel'=>'Post',
				'listorder'=>40
			);
		if (is_array($to_use))
		{	foreach ($this->options as $key=>$options)
			{	if (!in_array($options['cpcode'], $to_use))
				{	unset($this->options[$key]);
				}
			}
		}
	} // end of fn GetPreferenceList
	
	public function FormElementsList($value = 0, $add_all = true)
	{	ob_start();
		echo '<ul class="contactPrefForm">';
		foreach ($this->options as $option)
		{	if (!$checked = ($value & pow(2, $option['cpvalue'])))
			{	$unchecked = true;
			}
			echo '<li><div class="custom-checkbox clearfix contactPrefFormElements"><input type="checkbox" name="contactpref[', $option['cpcode'], ']" id="contactpref_', $option['cpcode'], '" value="', $option['cpid'], '" ', $checked ? 'checked="checked" ' : '', 'onchange="ContactOptionsFormOnChange();" /><label for="contactpref_', $option['cpcode'], '">', $this->InputSafeString($option['cplabel']), '</label></div></li>';
		}
		if ($this->options && $add_all)
		{	echo '<li><div class="custom-checkbox clearfix"><input type="checkbox" id="contactpref_all" ', $unchecked ? '' : 'checked="checked" ', 'onchange="ContactOptionsFormSetAll();" /><label for="contactpref_all">Select all</label></div></li>';
		}
		echo '</ul>';
		return ob_get_clean();
	} //  end of fn FormElementsList
	
	public function AdminFormListElements($rowkey = 0, $value = 0)
	{	if (is_array($value))
		{	$value = $this->PreferencesValueFromArray($value);
		} else
		{	$value = (int)$value;
		}
		$rowkey = (int)$rowkey;
		foreach ($this->options as $option)
		{	echo '<label for="contactpref_', $rowkey, '_', $option['cpcode'], '">', $this->InputSafeString($option['cplabel']), '</label><input type="checkbox" name="contactpref[', $rowkey, '][', $option['cpcode'], ']" id="contactpref_', $rowkey, '_', $option['cpcode'], '" value="1"', ($value & pow(2, $option['cpvalue'])) ? ' checked="checked"' : '', ' /><br />';
		}
	} //  end of fn AdminFormListElements
	
	public function AdminFormBlankListElements()
	{	foreach ($this->options as $option)
		{	echo '<label for="contactpref_', $option['cpcode'], '">', $this->InputSafeString($option['cplabel']), '</label><input type="checkbox" name="contactpref_all[', $option['cpcode'], ']" id="contactpref_', $option['cpcode'], '" value="1" /><br />';
		}
	} //  end of fn AdminFormBlankListElements
	
	public function AdminFormElements(&$form, $value = 0)
	{	if (is_array($value))
		{	$value = $this->PreferencesValueFromArray($value);
		} else
		{	$value = (int)$value;
		}
		$form->AddRawText('<p class="formCprefHeader" style="padding-left: 250px;">Contact preferences</p>');
		foreach ($this->options as $option)
		{	$form->AddCheckBox($option['cplabel'], 'contactpref[' . $option['cpcode'] . ']', 1, $value & pow(2, $option['cpvalue']));
		}
	} //  end of fn AdminFormElements
	
	public function DisplayChosenPreferences($value = 0, $sep = ', ', $field = '')
	{	$list = array();
		if ($value = (int)$value)
		{	foreach ($this->options as $key=>$option)
			{	if ($value & pow(2, $option['cpvalue']))
				{	$list[] = $this->InputSafeString($option[isset($option[$field]) ? $field : 'cplabel']);
				}
			}
		}
		return implode($sep, $list);
	} //  end of fn DisplayChosenPreferences
	
	public function PreferencesValueFromArray($set = array())
	{	$value = 0;
		if (is_array($set))
		{	foreach ($this->options as $key=>$option)
			{	if ($set[$option['cpcode']])
				{	$value += pow(2, $option['cpvalue']);
				}
			}
		}
		return $value;
	} //  end of fn PreferencesValueFromArray
	
	public function OptionsSetFromValue($value = 0)
	{	$options_set = array();
		if ($value = (int)$value)
		{	foreach ($this->options as $key=>$option)
			{	if ($value & pow(2, $option['cpvalue']))
				{	$options_set[$key] = $option;
				}
			}
		}
		return $options_set;
	} //  end of fn OptionsSetFromValue
	
	public function IsOptionsSetFromValue($value = 0, $optioncode = '')
	{	if ($value && $optioncode && ($options_set = $this->OptionsSetFromValue($value)))
		{	foreach ($options_set as $key=>$option)
			{	if ($option['cpcode'] == $optioncode)
				{	//echo $option['cpcode'], '=', $optioncode, '<br />';
					return true;
				} //else echo $option['cpcode'], '!=', $optioncode, '<br />';
			}
		}
		return false;
	} //  end of fn OptionsSetFromValue
	
	public function RawOptions()
	{	return $this->options;
	} //  end of fn RawOptions
	
} // end of defn ContactPreferences
?>