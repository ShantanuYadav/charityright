<?php
class EventsBasePage extends BasePage
{
	public function __construct($pagename = 'events')
	{	parent::__construct($pagename);
		if ($this->page->details['redirectlinkforce'] && $this->page->details['redirectlink'])
		{	header('location: ' . $this->page->details['redirectlink']);
			exit;
		}
	} // end of fn __construct

	protected function HeaderLinks()
	{	ob_start();
		echo $this->EventsHeaderLink();
		return ob_get_clean();
	} // end of fn HeaderLinks

} // end of class EventsBasePage
?>