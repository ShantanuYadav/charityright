<?php
class AdminTicketType extends TicketType
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct

	public function SlugExists($slug = '')
	{	$sql = 'SELECT evid FROM eventvenues WHERE slug="' . $this->SQLSafe($slug) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT evid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	public function Save($data = array(), $edid = 0)
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if (!$this->id)
		{	if (($edid = (int)$edid) && ($eventdate = new EventDate($edid)) && $eventdate->id)
			{	$fields[] = 'edid=' . $edid;
			} else
			{	$fail[] = 'the event date is missing';
			}
		}
		
		if ($ttypename = $this->SQLSafe($data['ttypename']))
		{	$fields[] = 'ttypename="' . $ttypename . '"';
		} else
		{	$fail[] = 'ticket type name cannot be empty';
		}
	
		$fields[] = 'booklimit=' . (int)$data['booklimit'];
		$fields[] = 'price=' . round($data['price'], 2);
		$fields[] = 'stockcontrol=' . ($data['stockcontrol'] ? '1' : '0');
		$fields[] = 'live=' . ($data['live'] ? '1' : '0');
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE tickettypes SET ' . $set . ' WHERE ttid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO tickettypes SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new ticket type added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function CanDelete()
	{	return $this->id && !$this->GetBookings();
	} // end of fn CanDelete
	
	public function InputForm($edid = 0)
	{	ob_start();
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}
		
		if (!$eventdate = $this->eventdate)
		{	$eventdate_obj = new EventDate($edid);
			$eventdate = $eventdate_obj->details;
		}

		if ($this->id)
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		} else
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?edid=' . $edid);
		}
		$form->AddTextInput('Ticket name', 'ttypename', $this->InputSafeString($data['ttypename']), 'long');
		$form->AddTextInput('Price (' . $eventdate['currency'] . ')', 'price', number_format($data['price'], 2, '.', ''), 'number');
		$form->AddCheckBox('Use stock control', 'stockcontrol', 1, $data['stockcontrol']);
		$form->AddTextInput('Booking limit', 'booklimit', (int)$data['booklimit'], 'number');
		$form->AddCheckBox('Live (visible in front end)', 'live', 1, $data['live']);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Ticket Type', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this ticket type</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
	public function BookingsTable()
	{	ob_start();
		$perpage = 30;
		if ($bookings = $this->GetBookings())
		{	
			echo '<table><tr><th class="num">Booking number</th><th class="num">Order number</th><th>Booked by</th><th>Booked on</th><th>Actions</th></tr>';
			
			if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
			
			foreach ($bookings as $booking)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					if (!$orders[$booking['orderitem']])
					{	$orders[$booking['orderitem']] = $this->OrderFromBooking($booking);
					}
					echo '<tr class="stripe', $i++ % 2,  '"><td class="num">', (int)$booking['bid'], '</td><td class="num">', $orders[$booking['orderitem']]->id, '</td><td>', $this->InputSafeString($orders[$booking['orderitem']]->details['firstname'] . ' ' . $orders[$booking['orderitem']]->details['lastname']), ' (' . $orders[$booking['orderitem']]->details['email'] . ')</td><td>', date('d/m/y @H:i', strtotime($orders[$booking['orderitem']]->details['orderdate'])), '</td><td><a href="bookorder.php?id=', $orders[$booking['orderitem']]->id, '">view order</a></td></tr>';
				}
			}
			echo '</table>';
			if (count($bookings) > $perpage)
			{	$pagelink = $_SERVER['SCRIPT_NAME'];
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
					if ($get)
					{	$pagelink .= '?' . implode('&', $get);
					}
				}
				$pag = new Pagination($_GET['page'], count($bookings), $perpage, $pagelink, array(), 'page');
				echo '<div class="pagination">', $pag->Display(), '</div>';
			}
			echo '<p class="adminCSVDownload"><a href="tickettypebookings_csv.php?id=', $this->id, '">download csv of all bookings for this ticket type</a></p>';
		}
		return ob_get_clean();
	} // end of fn BookingsTable
	
	protected function AssignBookOrder($orderrow = array())
	{	return new AdminBookOrder($orderrow);
	} // fn OrderFromBooking
	
	public function BookingsCSVOuput()
	{	$csv = new CSVReport();
		$csv->AddToHeaders(array('event', 'venue', 'start date', 'booking number', 'ticket type', 'customer name', 'customer email', 'booked', 'order number'));
		if ($bookings = $this->GetBookings())
		{	$orders = array();
			foreach ($bookings as $booking)
			{	if (!$orders[$booking['orderitem']])
				{	$orders[$booking['orderitem']] = $this->OrderFromBooking($booking);
				}
				$row = array('"' . $this->InputSafeString($this->event['eventname']) . '"', '"' . $this->InputSafeString($this->venue['venuename']) . '"', '"' . substr($this->eventdate['starttime'], 0, 10) . '"', (int)$booking['bid'], '"' . $this->InputSafeString($this->details['ttypename']) . '"' , '"' . $this->InputSafeString($orders[$booking['orderitem']]->details['firstname'] . ' ' . $orders[$booking['orderitem']]->details['lastname']) . '"', '"' . $orders[$booking['orderitem']]->details['email'] . '"', '"' . $orders[$booking['orderitem']]->details['orderdate'] . '"', $orders[$booking['orderitem']]->id);
				$csv->AddToRows($row);
			}
		}
		$csv->Output($this->BookingsCSVName());
	} // end of fn BookingsCSVOuput
	
	public function BookingsCSVName()
	{	return $this->event['slug'] . '_' . $this->venue['slug'] . '_' . date('Ymd', strtotime($this->eventdate['starttime'])) . '_' . preg_replace('[^a-zA-Z0-9]', '-', $this->details['ttypename']) . '.csv';
	} // end of fn BookingsCSVName
	
} // end of defn AdminTicketType
?>