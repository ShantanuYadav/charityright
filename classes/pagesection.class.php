<?php
class PageSection extends BlankItem
{	protected $imagelocation = '';
	protected $imagedir = '';
	
	public function __construct($id = 0)
	{	parent::__construct($id, 'pagesections', 'psid');
		$this->imagelocation = SITE_URL . 'img/pagesections/';
		$this->imagedir = CITDOC_ROOT . '/img/pagesections/';
	} // fn __construct
	
	public function SubSections($liveonly = true)
	{	$subsections = array();
		$sql = 'SELECT * FROM pagesections WHERE pageid=' . (int)$this->details['pageid'] . ' AND parentsection=' . $this->id;
		if ($liveonly)
		{	$sql .= ' AND live=1';
		}
		$sql .= ' ORDER BY listorder, psid';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$subsections[$row['psid']] = $row;
			}
		}
		return $subsections;
	} // end of fn SubSections
	
	public function HasImage()
	{	return $this->id && file_exists($this->GetImageFile());
	} // end of fn HasImage
	
	public function GetImageFile()
	{	return $this->imagedir . $this->id .'.png';
	} // end of fn GetImageFile
	
	public function GetImageSRC()
	{	if ($this->HasImage())
		{	return $this->imagelocation . $this->id . '.png';
		}
	} // end of fn GetImageSRC

} // end of defn Event
?>