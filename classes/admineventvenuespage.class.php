<?php
class AdminEventVenuesPage extends AdminEventsPage
{	protected $venue;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('eventvenues.php', 'Venues');
		if ($this->venue->id)
		{	$this->breadcrumbs->AddCrumb('eventvenue.php?id=' . $this->venue->id, $this->InputSafeString($this->venue->details['venuename'] . ' [' . $this->GetCountry($this->venue->details['country']) . ']'));
		}
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function AssignEvent()
	{	$this->venue = new AdminEventVenue($_GET['id']);
	} // end of fn AssignEvent
	
	protected function AdminEventsBody()
	{	if ($this->venue->id)
		{	if ($menu = $this->GetEventMenu())
			{	echo '<div class="adminItemSubMenu"><ul>';
				foreach ($menu as $menuarea=>$menuitem)
				{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
				}
				echo '</ul><div class="clear"></div></div>';
			}
		}
	} // end of fn AdminEventsBody
	
	protected function GetEventMenu()
	{	$menu = array();
		$menu['edit'] = array('text'=>'Edit Venue', 'link'=>'eventvenue.php?id=' . $this->venue->id);
	//	$menu['dates'] = array('text'=>'Dates', 'link'=>'eventvenuedates.php?id=' . $this->venue->id);
		return $menu;
	} // end of fn GetEventMenu
	
} // end of defn AdminEventsPage
?>