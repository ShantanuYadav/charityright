<?php
class AdminNewsStory extends NewsStory
{	var $admintitle = "";
	var $langused = array();

	function __construct($id = 0)
	{	parent::__construct($id);
		$this->GetLangUsed();
		$this->GetAdminTitle();
	} //  end of fn __construct
	
	function GetAdminTitle()
	{	if ($this->id)
		{	if (!$this->admintitle = $this->details["headline"])
			{	if ($details = $this->GetDefaultDetails())
				{	$this->admintitle = $details["headline"] . " [{$this->language}]";
				}
			}
		}
	} // end of fn GetAdminTitle
	
	function GetDefaultDetails()
	{	$sql = "SELECT * FROM news_lang WHERE newsid=$this->id AND lang='{$this->def_lang}'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return $row;
			}
		}
		return array();
	} // end of fn GetDefaultDetails
	
	function AssignNewsLanguage()
	{	if (!$this->language = $_GET["lang"])
		{	$this->language = $this->def_lang;
		}
	} // end of fn AssignNewsLanguage
	
	function AddDetailsForDefaultLang(){}
	
	function GetLangUsed()
	{	$this->langused = array();
		if ($this->id)
		{	if ($result = $this->db->Query("SELECT lang FROM news_lang WHERE newsid=$this->id"))
			{	while ($row = $this->db->FetchArray($result))
				{	$this->langused[$row["lang"]] = true;
				}
			}
		}
	} // end of fn GetLangUsed

	public function SlugExists($slug = '')
	{	$sql = 'SELECT newsid FROM news_lang WHERE slug="' . $this->SQLSafe($slug) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT newsid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	function Save($data = array(), $imagefile = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		$l_fields = array();
		$admin_actions = array();
		
		if ($headline = $this->SQLSafe($data['headline']))
		{	$l_fields[] = 'headline="' . $headline . '"';
			if ($this->id && ($data['headline'] != $this->details['headline']))
			{	$admin_actions[] = array('action'=>'Headline (' . $this->language . ')', 'actionfrom'=>$this->details['headline'], 'actionto'=>$data['headline']);
			}
		} else
		{	$fail[] = 'headline cannot be empty';
		}
		
		if ($newstext = $this->SQLSafe($data['newstext']))
		{	$l_fields[] = 'newstext="' . $newstext . '"';
			if ($this->id && ($data['newstext'] != $this->details['newstext']))
			{	$admin_actions[] = array('action'=>'News text (' . $this->language . ')', 'actionfrom'=>$this->details['newstext'], 'actionto'=>$data['newstext'], 'actiontype'=>'html');
			}
		} else
		{	$fail[] = 'story cannot be empty';
		}
	
		$metadesc = $this->SQLSafe($data['metadesc']);
		$l_fields[] = 'metadesc="' . $metadesc . '"';
		if ($this->id && ($data['metadesc'] != $this->details['metadesc']))
		{	$admin_actions[] = array('action'=>'Metadesc (' . $this->language . ')', 'actionfrom'=>$this->details['metadesc'], 'actionto'=>$data['metadesc']);
		}
	
		$snippet = $this->SQLSafe($data['snippet']);
		$l_fields[] = 'snippet="' . $snippet . '"';
		if ($this->id && ($data['snippet'] != $this->details['snippet']))
		{	$admin_actions[] = array('action'=>'Snippet (' . $this->language . ')', 'actionfrom'=>$this->details['snippet'], 'actionto'=>$data['snippet']);
		}
		
		if ($this->id)
		{	$slug = $this->TextToSlug($data['slug']);
		} else
		{	if ($headline)
			{	$slug = $this->TextToSlug($headline);
			}
		}
		
		if ($slug)
		{	$suffix = '';
			while ($this->SlugExists($slug . $suffix))
			{	$suffix++;
			}
			$slug .= $suffix;
			
			$l_fields[] = 'slug="' . $slug . '"';
			if ($this->id && ($slug != $this->details['slug']))
			{	$admin_actions[] = array('action'=>'Slug', 'actionfrom'=>$this->details['slug'], 'actionto'=>$slug);
			}
		} else
		{	if ($this->id || $headline)
			{	$fail[] = 'slug missing';
			}
		}
		
		if ($this->id)
		{	// then allow update of timestamp
			if (($y = (int)$data["ydate"]) && ($m = (int)$data["mdate"]) && ($d = (int)$data["ddate"]))
			{	
				$submitted = $this->datefn->SQLDateTime(mktime(substr($data["time"], 0, 2), substr($data["time"], 3, 2), 0, $m, $d, $y));
				$fields[] = "submitted='$submitted'";
				if ($this->id && ($submitted != $this->details["submitted"]))
				{	$admin_actions[] = array("action"=>"Story posted", "actionfrom"=>$this->details["submitted"], "actionto"=>$submitted, "actiontype"=>"datetime");
				}
			}
			
			// if editing allow to put live
			$live = ($data["live"] ? "1" : "0");
			$fields[] = "live=" . $live;
			if ($this->id && ($live != (int)$this->details["live"]))
			{	$admin_actions[] = array("action"=>"Live?", "actionfrom"=>$this->details["live"], "actionto"=>$live, "actiontype"=>"boolean");
			}
			
		} else
		{	// new story so create timestamp
			$submitted = date("Y-m-d H:i:00");
			$fields[] = "submitted='$submitted'";
		}
		
		if ($this->id || !$fail)
		{	
			$set = implode(", ", $fields);
			if ($this->id)
			{	$sql = "UPDATE news SET $set WHERE newsid=" . $this->id;
			} else
			{	$sql = "INSERT INTO news SET $set";
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$record_changes = true;
						$success[] = "changes saved";
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = "new story added";
							$this->RecordAdminAction(array("tablename"=>"news", "tableid"=>$this->id, "area"=>"news", "action"=>"created"));
						} else
						{	$fail[] = "insert failed";
						}
					}
				}
				
				if ($this->id)
				{	
					if ($set = implode(", ", $l_fields))
					{	if ($this->langused[$this->language])
						{	$sql = "UPDATE news_lang SET $set WHERE newsid=$this->id AND lang='$this->language'";
						} else
						{	$sql = "INSERT INTO news_lang SET $set, newsid=$this->id, lang='$this->language'";
						}
						if ($result = $this->db->Query($sql))
						{	if ($this->db->AffectedRows())
							{	$success[] = "text changes saved";
								$record_changes = true;
							}
						}
					}

					$this->Get($this->id);
					$this->GetLangUsed();
					
					if ($imagefile["size"])
					{	$uploaded = $this->UploadPhoto($imagefile);
						if ($uploaded["successmessage"])
						{	$success[] = $uploaded["successmessage"];
							$this->RecordAdminAction(array("tablename"=>"news", "tableid"=>$this->id, "area"=>"news", "action"=>"New image uploaded"));
						}
						if ($uploaded["failmessage"])
						{	$fail[] = $uploaded["failmessage"];
						}
					} else
					{	if ($data["delposter"])
						{	@unlink($this->ImageFile());
							$success[] = "image deleted";
							$this->RecordAdminAction(array("tablename"=>"news", "tableid"=>$this->id, "area"=>"news", "action"=>"Image deleted"));
						}
					}
					
				}
				
				if ($record_changes)
				{	$base_parameters = array("tablename"=>"news", "tableid"=>$this->id, "area"=>"news");
					if ($admin_actions)
					{	foreach ($admin_actions as $admin_action)
						{	$this->RecordAdminAction(array_merge($base_parameters, $admin_action));
						}
					}
				}
			}
			
		}
		
		return array("failmessage"=>implode(", ", $fail), "successmessage"=>implode(", ", $success));
		
	} // end of fn Save
	
	function UploadPhoto($file)
	{	$fail = array();
		$successmessage = "";

		if ($file["size"])
		{	if ((!stristr($file["type"], "jpeg") && !stristr($file["type"], "jpg") && !stristr($file["type"], "png")) || $file["error"])
			{	$fail[] = "File type invalid (jpeg or png only)";
			} else
			{	
				if ($this->ReSizePhoto($file, $this->ImageFile(), $this->image_w))
				{	$successmessage = "New image uploaded";
				}
				@unlink($file["tmp_name"]);
				
			}
		} else
		{	$fail[] = "image not uploaded";
		}
		return array("failmessage"=>implode(", ", $fail), "successmessage"=>$successmessage);

	} // end of fn UploadPhoto
	
	function ReSizePhoto($uploadfile_array, $file, $width)
	{	
		$uploadfile = $uploadfile_array['tmp_name'];
		$isize = getimagesize($uploadfile);
		$new_ratio = $height / $width;
		$iratio = $isize[1] / $isize[0];
		
		$w_fromold = $isize[0];
		$h_fromold = $isize[1];
		
		if (stristr($uploadfile_array["type"], "jpeg") || stristr($uploadfile_array["type"], "jpg"))
		{	$oldimage = imagecreatefromjpeg($uploadfile);
		} else
		{	if (stristr($uploadfile_array["type"], "png"))
			{	$oldimage = imagecreatefrompng($uploadfile);
			} else
			{	return false;
			}
		}
		
		if ($w_fromold > $width)
		{	$height = ceil(($width / $w_fromold) * $h_fromold);
		} else
		{	$height = $h_fromold;
			$width = $w_fromold;
		}
		$newimage = imagecreatetruecolor($width, $height);		
		
		imagecopyresampled($newimage,$oldimage,0,0,0,0,$width, $height, $w_fromold, $h_fromold);
		
		imagedestroy($oldimage);
		
		ob_start();
		imagejpeg($newimage, NULL, 100);
		$final_image = ob_get_contents();
		ob_end_clean();
		
		imagedestroy($newimage);
		
		file_put_contents($file, $final_image);
		chmod($file, 0777);
		
		return true;
		
	} // end of fn ReSizePhoto
	
	function Delete()
	{	if ($this->CanDelete())
		{	$sql = "DELETE FROM news WHERE newsid=" . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	@unlink($this->ImageFile());
					$this->db->Query("DELETE FROM news_lang WHERE newsid=$this->id");
					$this->RecordAdminAction(array("tablename"=>"news", "tableid"=>$this->id, "area"=>"news", "action"=>"deleted"));
					$this->Reset();
					return true;
				}
			}
		}
	} // end of fn Delete
	
	function CanDelete()
	{	return $this->id && $this->CanAdminUserDelete();
	} // end of fn CanDelete
	
	function InputForm()
	{	 
		$data = $this->details;
		if ($this->id)
		{	
			if (!$this->langused[$this->language])
			{	if ($_POST)
				{	// initialise details from this
					foreach ($_POST as $field=>$value)
					{	$data[$field ] = $value;
					}
					if (($y = (int)$data['ydate']) && ($m = (int)$data['mdate']) && ($d = (int)$data['ddate']))
					{	
						$data['submitted'] = $this->datefn->SQLDateTime(mktime(substr($data['time'], 0, 2), substr($data['time'], 3, 2), 0, $m, $d, $y));
					}
				}
			}
		} else
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id . '&lang=' . $this->language, 'newsform');
		$form->AddTextInput('Headline', 'headline', $this->InputSafeString($data['headline']), 'long');
		if ($this->id)
		{	$form->AddTextInput('Slug', 'slug', $this->InputSafeString($data['slug']), 'long', 255, 1);
		}
		$form->AddTextArea('Snippet (try to keep below 300 characters)', 'snippet', $this->InputSafeString($data['snippet']), '', 0, 0, 5, 60);
		$form->AddTextArea('Story', 'newstext', stripslashes($data['newstext']), 'tinymce', 0, 0, 30, 60);
		$form->AddRawText("<p><label></label><a href='#' onclick='javascript:window.open(\"newsimagelist.php\", \"newsimages\", \"toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=550\"); return false;'>view available images</a></p>");
		if ($this->id)
		{	$years = array($y = date('Y'));
			$startyear = date('Y', strtotime($data['submitted']));
			while ($y > $startyear)
			{	$years[] = --$y;
			}
			$form->AddDateInput('Story Date', 'date', $data['submitted'], $years);
			$form->AddTextInput('Time', 'time', substr($data['submitted'], 11, 5));
			$form->AddCheckBox('Live?', 'live', 1, $data['live']);
		}
		$form->AddTextArea('Meta description', 'metadesc', $this->InputSafeString($data['metadesc']), '', 0, 0, 10, 60);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Story', 'submit');
		if ($histlink = $this->DisplayHistoryLink('news', $this->id))
		{	echo '<p>', $histlink, '</p>';
		}
		if ($this->id)
		{	echo '<h3>Editing ... ', $this->InputSafeString($this->admintitle), '</h3>';
			if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this story</a></p>';
			}
			$this->AdminEditLangList();
		}
		$form->Output();
	} // end of fn InputForm
	
} // end of defn NewsStory
?>