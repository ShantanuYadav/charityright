<?php
class CRMSearch extends Base
{

	public function FullName()
	{	
	} // end of fn FullName
	
	public function EmailSearch($filter = array())
	{	$emails = array();
		
		// get main donations
		$tables = array('donations'=>'donations');
		$fields = array('donations.*');
		$where = array('email'=>'donoremail LIKE "%' . $this->SQLSafe($filter['email']) . '%"');
		$orderby = array('created'=>'donations.created DESC');
		if ($filter['startdate'])
		{	$where['startdate'] = 'donations.created>="' . $this->SQLSafe($filter['startdate']) . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'donations.created<="' . $this->SQLSafe($filter['enddate']) . ' 23:59:59"';
		}
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $order)))
		{	while ($row = $this->db->FetchArray($result))
			{	$row['donoremail'] = strtolower($row['donoremail']);
				if ($emails[$row['donoremail']])
				{	if ($row['created'] < $emails[$row['donoremail']]['first'])
					{	$emails[$row['donoremail']]['first'] = $row['created'];
					}
					if ($row['created'] > $emails[$row['donoremail']]['last'])
					{	$emails[$row['donoremail']]['last'] = $row['created'];
					}
				} else
				{	$emails[$row['donoremail']] = array('email'=>$row['donoremail'], 'first'=>$row['created'], 'last'=>$row['created'], 'donations'=>0, 'donations_paid'=>0, 'crdonations'=>0, 'crdonations_paid'=>0, 'crcampaignsuser'=>0);
				}
				$emails[$row['donoremail']]['donations']++;
				if ($row['donationref'])
				{	$emails[$row['donoremail']]['donations_paid']++;
				}
			}
		}
		
		// crstars donations
		$tables = array('campaigndonations'=>'campaigndonations');
		$fields = array('campaigndonations.*');
		$where = array('email'=>'campaigndonations.donoremail LIKE "%' . $this->SQLSafe($filter['email']) . '%"');
		$orderby = array('donated'=>'campaigndonations.donated DESC');
		if ($filter['startdate'])
		{	$where['startdate'] = 'campaigndonations.donated>="' . $this->SQLSafe($filter['startdate']) . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'campaigndonations.donated<="' . $this->SQLSafe($filter['enddate']) . ' 23:59:59"';
		}
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $order)))
		{	while ($row = $this->db->FetchArray($result))
			{	$row['donoremail'] = strtolower($row['donoremail']);
				if ($emails[$row['donoremail']])
				{	if ($row['donated'] < $emails[$row['donoremail']]['first'])
					{	$emails[$row['donoremail']]['first'] = $row['donated'];
					}
					if ($row['donated'] > $emails[$row['donoremail']]['last'])
					{	$emails[$row['donoremail']]['last'] = $row['donated'];
					}
				} else
				{	$emails[$row['donoremail']] = array('email'=>$row['donoremail'], 'first'=>$row['donated'], 'last'=>$row['donated'], 'donations'=>0, 'donations_paid'=>0, 'crdonations'=>0, 'crdonations_paid'=>0, 'crcampaignsuser'=>0);
				}
				$emails[$row['donoremail']]['crdonations']++;
				if ($row['donationref'])
				{	$emails[$row['donoremail']]['crdonations_paid']++;
				}
			}
		}
		
		// crstars campaign users
		$tables = array('campaignusers'=>'campaignusers');
		$fields = array('campaignusers.*');
		$where = array('email'=>'campaignusers.email LIKE "%' . $this->SQLSafe($filter['email']) . '%"');
		$orderby = array('lastupdated'=>'campaignusers.lastupdated DESC');
		if ($filter['startdate'])
		{	$where['startdate'] = 'campaigndonations.lastupdated>="' . $this->SQLSafe($filter['startdate']) . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'campaigndonations.lastupdated<="' . $this->SQLSafe($filter['enddate']) . ' 23:59:59"';
		}
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $order)))
		{	while ($row = $this->db->FetchArray($result))
			{	$row['email'] = strtolower($row['email']);
				if ($emails[$row['email']])
				{	if ($row['lastupdated'] < $emails[$row['email']]['first'])
					{	$emails[$row['email']]['first'] = $row['lastupdated'];
					}
					if ($row['lastupdated'] > $emails[$row['email']]['last'])
					{	$emails[$row['email']]['last'] = $row['lastupdated'];
					}
				} else
				{	$emails[$row['email']] = array('email'=>$row['email'], 'first'=>$row['lastupdated'], 'last'=>$row['lastupdated'], 'donations'=>0, 'donations_paid'=>0, 'crdonations'=>0, 'crdonations_paid'=>0, 'crcampaignsuser'=>0);
				}
				$emails[$row['email']]['crcampaignsuser'] = $row['cuid'];
			}
		}
		
		if ($emails)
		{	uasort($emails, array($this, 'UAOrderResultsByLast'));
		}
		
		return $emails;
	} // end of fn EmailSearch

	public function UAOrderResultsByLast($a, $b)
	{	if ($a['last'] == $b['last'])
		{	return $a['first'] < $b['first'];
		} else
		{	return $a['last'] < $b['last'];
		}
	} // end of fn UAOrderResultsByLast
	
} // end if class defn CRMSearch
?>