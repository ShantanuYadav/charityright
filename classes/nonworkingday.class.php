<?php
class NonWorkingDay extends BlankItem
{	

	function __construct($id = 0)
	{	parent::__construct($id, 'nonworkingdays', 'nwid');
	} //  end of fn __construct
	
	function Save($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if (($d = (int)$data['dnwdate']) && ($m = (int)$data['mnwdate']) && ($y = (int)$data['ynwdate']))
		{	$nwdate = $this->datefn->SQLDate(mktime(0,0,0,$m, $d, $y));
			$fields['nwdate'] = 'nwdate="' . $nwdate . '"';
		} else
		{	$fail[] = 'date missing';
		}
		
		$fields[] = 'description="' . $this->SQLSafe($data['description']) . '"';
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE ' . $this->tablename . ' SET ' . $set . ' WHERE ' . $this->idfield . '=' . $this->id;
			} else
			{	$sql = 'INSERT INTO ' . $this->tablename . ' SET ' . $set;
			}

			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new non-working day added';
						} else
						{	$fail[] = 'inset failed';
						}
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	function CanDelete()
	{	return $this->id;
	} // end of fn CanDelete
	
	function InputForm()
	{	 
		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		
		if (!$data = $this->details)
		{	$data = $_POST;
		}
		
		$startyear = date('Y');
		$endyear = $startyear + 10;
		if ($this->id && (($existing_year = date('Y', strtotime($this->details['nwdate'])) < $startyear)))
		{	$startyear = $existing_year;
		}
		
		$years = array();
		for ($y = $startyear; $y < $endyear; $y++)
		{	$years[] = $y;
		}
		$form->AddDateInput('Date', 'nwdate', $data['nwdate'], $years);
		$form->AddTextInput('Description (optional)', 'description', $this->InputSafeString($data['description']));
		
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Non-Working Day', 'submit');
		
		if ($this->CanDelete())
		{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this non-working day</a></p>';
		}
		$form->Output();
	//	echo '<br />';
	} // end of fn InputForm
	
} // end of defn NonWorkingDay
?>