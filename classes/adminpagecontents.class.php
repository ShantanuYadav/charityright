<?php
class AdminPageContents extends Base
{	var $pages = array();
	var $adminuser = false;

	function __construct($adminuser = false)
	{	parent::__construct();
		$this->adminuser = ($adminuser ? true : false);
		$this->Get();
	} //  end of fn __construct
	
	function Reset()
	{	$this->pages = array();
	} // end of fn Reset
	
	function Get()
	{	$this->Reset();
		$sql = 'SELECT * FROM pages ORDER BY pagename';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->pages[] = new AdminPageContent($row, $this->adminuser);
			}
		}
	} // end of fn Get
	
	function PageList()
	{	
		echo '<table id="pagelist"><tr class="newlink"><th colspan="6"><a href="pageedit.php">new page</a></th></tr><tr><th>Title</th><th>Name</th><th>Links to</th><th>Live</th><th></th><th>Actions</th></tr>';
		foreach ($this->pages as $mainpage)
		{	$this->PageListLine($mainpage);
		}
		echo '</table>';
	} // end of fn PageList
	
	function PageListLine($page, $depth = 0)
	{	$extras = array();
		if ($page->details['headeronly'])
		{	$extras[] = 'menu header';
		}
		if ($page->details['redirectlink'])
		{	$extras[] = 'redirects';
		}
		if ($page->GetQuickDonateOptions(true))
		{	$extras[] = 'donate form';
		}
		echo '<tr', $depth ? (' class="child child' . $depth . '"') : '', '><td class="pagetitle"><a href="pageedit.php?id=', $page->id, '">', $this->InputSafeString($page->details['pagetitle']), '</a></td><td>', $this->InputSafeString($page->details['pagename']), '</td><td>';
		if ($link = $page->Link())
		{	echo $link, ' (<a href="', $link, '" target="_blank">preview</a>)';
		}
		echo '</td><td>', $page->details['pagelive'] ? 'Yes' : '', '</td>',
				'<td>',  implode('<br />', $extras), '</td>',
				'<td><a href="pageedit.php?id=', $page->id, '">edit</a>';
		if ($page->CanDelete())
		{	echo '&nbsp;|&nbsp;<a href="pageedit.php?id=', $page->id, '&delete=1">delete</a>';
		}
		echo '</td></tr>';
	} // end of fn PageListLine
	
} // end of defn AdminPageContent
?>
