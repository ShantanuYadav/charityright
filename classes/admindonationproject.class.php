<?php
class AdminDonationProject extends DonationProject
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct

	public function SlugExists($slug = '', $dcid = 0)
	{	$sql = 'SELECT dpid FROM donation_projects WHERE slug="' . $this->SQLSafe($slug) . '" AND dcid=' . (int)$dcid;
		if ($this->id)
		{	$sql .= ' AND NOT dpid=' . $this->id . '';
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	public function Save($data = array(), $donation_country = false)
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if (!$this->id)
		{	if ($donationname = $this->SQLSafe($data['donationname']))
			{	// set dccode
				$dpcode = strtolower(str_replace(array(' ','-'), '_', $donationname));
				$dpcode = preg_replace('|[^0-9a-z_]|', '', $dpcode);
				if ($dpcode)
				{	$fields['dpcode'] = 'dpcode="' . $dpcode . '"';
				} else
				{	$fail[] = 'invlid project name (no letters or numbers)';
				}
			} else
			{	$fail[] = 'donation code cannot be empty';
			}
			if ($donation_country->id)
			{	$fields['dcid'] = 'dcid=' . $donation_country->id;
			} else
			{	$fail[] = 'donation country missing';
			}
		}
		
		if ($projectname = $this->SQLSafe($data['projectname']))
		{	$fields['projectname'] = 'projectname="' . $projectname . '"';
		} else
		{	$fail[] = 'display name cannot be empty';
		}
		
		if ($formlabel = $this->SQLSafe($data['formlabel']))
		{	$fields['formlabel'] = 'formlabel="' . $formlabel . '"';
		} else
		{	$fail[] = 'donation form label cannot be empty';
		}
		
	//	if ($formlabelcrstars = $this->SQLSafe($data['formlabelcrstars']))
	//	{	$fields['formlabelcrstars'] = 'formlabelcrstars="' . $formlabelcrstars . '"';
	//	} else
	//	{	$fail[] = 'CR Stars form label cannot be empty';
	//	}
		
		$fields['emailtext'] = 'emailtext="' . $this->SQLSafe($data['emailtext']) . '"';
		$fields['listorder'] = 'listorder=' . (int)$data['listorder'];
		$fields['oneoff'] = 'oneoff=' . ($data['oneoff'] ? '1' : '0');
		$fields['monthly'] = 'monthly=' . ($data['monthly'] ? '1' : '0');
		$fields['crstars'] = 'crstars=' . ($data['crstars'] ? '1' : '0');
		
		if ($this->id)
		{	$slug = $this->TextToSlug($data['slug']);
		} else
		{	if ($donationname)
			{	$slug = $this->TextToSlug(str_replace('_', '-', $donationname));
			}
		}
		
		if ($slug)
		{	$suffix = '';
			while ($this->SlugExists($slug . $suffix, $this->id ? $this->details['dcid'] : $dcid))
			{	$suffix++;
			}
			if ($suffix)
			{	$slug .= $suffix;
			}
			
			$fields['slug'] = 'slug="' . $slug . '"';
		} else
		{	if ($this->id || $donationname)
			{	$fail[] = 'slug missing';
			}
		}
		
	//	$fail[] = 'test';
	//	$this->VarDump($fields);
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE donation_projects SET ' . $set . ' WHERE dpid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO donation_projects SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new donation project added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}
		
		if ($this->id)
		{	if ($this->projects)
			{	$this->db->Query('DELETE FROM donation_prices WHERE projectlevel=1 AND projectid=' . $this->id);
			} else
			{	$added = 0;
				$deleted = 0;
				$changed = 0;
				if (is_array($data['price']) && $data['price'])
				{	foreach ($data['price'] as $currency=>$amount)
					{	$amount = round($amount, 2);
						if ($amount > 0)
						{	if ($this->prices[$currency])
							{	$changesql = 'UPDATE donation_prices SET amount=' . $amount . ' WHERE projectlevel=1 AND projectid=' . $this->id . ' AND currency="' . $this->SQLSafe($currency) . '"';
								if ($changeresult = $this->db->Query($changesql))
								{	if ($this->db->AffectedRows())
									{	$changed++;
									}
								}
							} else
							{	$addsql = 'INSERT INTO donation_prices SET projectlevel=1, projectid=' . $this->id . ', currency="' . $this->SQLSafe($currency) . '", amount=' . $amount;
								if ($addresult = $this->db->Query($addsql))
								{	if ($this->db->AffectedRows())
									{	$added++;
									}
								}
							}
						} else
						{	if ($this->prices[$currency])
							{	$delsql = 'DELETE FROM donation_prices WHERE projectlevel=1 AND projectid=' . $this->id . ' AND currency="' . $this->SQLSafe($currency) . '"';
								if ($delresult = $this->db->Query($delsql))
								{	if ($this->db->AffectedRows())
									{	$deleted++;
									}
								}
							}
						}
					}
					if ($changed)
					{	$success[] = $changed . ' fixed price' . ($changed > 1 ? 's' : '') . ' changed';
					}
					if ($added)
					{	$success[] = $added . ' fixed price' . ($added > 1 ? 's' : '') . ' added';
					}
					if ($deleted)
					{	$success[] = $deleted . ' fixed price' . ($deleted > 1 ? 's' : '') . ' removed';
					}
					if ($changed || $added || $deleted)
					{	$this->GetPrices();
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function CanDelete()
	{	return $this->id && !$this->projects && !$this->GetDonations() && !$this->GetCampaigns();
	} // end of fn CanDelete
	
	public function DeleteExtra()
	{	$delpricessql = 'DELETE FROM donation_prices WHERE projectlevel=1 AND projectid=' . $this->id;
		$this->db->Query($delpricessql);
	} // end of fn DeleteExtra
	
	public function InputForm($donation_country = false)
	{	ob_start();
		if ($data = $this->details)
		{	$data['price'] = array();
			if ($this->prices)
			{	foreach ($this->prices as $currency=>$price)
				{	$data['price'][$currency] = $price['amount'];
				}
			}
		} else
		{	$data = $_POST;
			//$this->VarDump($data);
		}

		if ($this->id)
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		} else
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?dcid=' . $donation_country->id);
		}
		$form->AddTextInput('Display name', 'projectname', $this->InputSafeString($data['projectname']), 'long', 255, 1);
		if ($this->id)
		{	$form->AddTextInput('Slug', 'slug', $this->InputSafeString($data['slug']), '', 255, 1);
		} else
		{	$form->AddTextInput('Project code', 'donationname', $this->InputSafeString($data['donationname']), '', 255, 1);
		}
		$form->AddTextInput('Donations form label for sub-options', 'formlabel', $this->InputSafeString($data['formlabel']), 'verylong', 255, 1);
		//$form->AddTextInput('CR Stars form label for sub-options', 'formlabelcrstars', $this->InputSafeString($data['formlabelcrstars']), 'verylong', 255, 1);
		$form->AddCheckBox('Used for one-off donations', 'oneoff', 1, $data['oneoff']);
		$form->AddCheckBox('Used for monthly donations', 'monthly', 1, $data['monthly']);
		$form->AddCheckBox('Used for CR Stars campaigns', 'crstars', 1, $data['crstars']);
		$form->AddTextInput('List order', 'listorder', (int)$data['listorder'], '', 5);
		$form->AddTextArea('Email text', 'emailtext', stripslashes($data['emailtext']), '', 0, 0, 4, 60);
		if (true && !$this->projects && $currencies = $this->PossibelPriceCurrencies())
		{	ob_start();
			echo '<div class="donPricesOpener', $data['price'] ? ' donPricesHidden' : '', '"><label><a onclick="$(\'.donPricesOpener\').addClass(\'donPricesHidden\');$(\'.donPricesContainer\').removeClass(\'donPricesHidden\');">Show fixed prices</a></label><br /></div>
				<div class="donPricesContainer', $data['price'] ? '' : ' donPricesHidden', '"><label><a onclick="$(\'.donPricesContainer\').addClass(\'donPricesHidden\');$(\'.donPricesOpener\').removeClass(\'donPricesHidden\');">Hide fixed prices</a></label><br />';
			foreach ($currencies as $currency)
			{	echo '<label>', $this->InputSafeString($currency['curname']), ' (', $currency['cursymbol'], ')</label><input type="text" name="price[', $currency['curcode'], ']" value="', number_format($data['price'][$currency['curcode']], 2, '.', ''), '" class="number" /><br />';
			}
			echo '</div>';
			$form->AddRawText(ob_get_clean());
		}
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Project', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this project</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
	private function PossibelPriceCurrencies()
	{	$currencies = array();
		$sql = 'SELECT * FROM currencies WHERE use_monthly OR use_oneoff ORDER BY curorder, curname';
		if ($result = $this->db->Query($sql))
		{	while($row = $this->db->FetchArray($result))
			{	$currencies[$row['curcode']] = $row;
			}
		}
		return $currencies;
	} // end of fn PossibelPriceCurrencies
		
} // end of defn AdminDonationProject
?>