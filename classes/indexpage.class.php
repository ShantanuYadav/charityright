<?php
class IndexPage extends BasePage
{	
	public function __construct($pageName = 'home')
	{	parent::__construct($pageName);
		$this->css['homepage.css'] = 'homepage.css';
		if (true || SITE_TEST)
		{	$this->do_infform_footer = false;
		}
		if ($this->quickdonate = $this->page->GetQuickDonateOptions())
		{	$this->js['quickdonate'] = 'quickdonate.js';
			$this->css['quickdonate'] = 'quickdonate.css';
		}
	} // end of fn __construct

	public function MainBodyContent()
	{	if (!$this->page->details['noheader'] && (($hasbanner = $this->page->HasBanner()) || $this->page->details['headertext']))
		{	
          echo '<section class="hero-banner">
		<div class="container-fluid px-0 mx-0">
			<div class="hero-tabs">
				<div class="hero-banner-des">
					<div class="inner-des text-center">
						<h1 class="cr-intro-h1">Filling bellies.</h1>
						<h1 class="cr-intro-h1">Educating minds.</h1>
						<h1 class="cr-intro-h1">Fulfilling potential.</h1>
						<div class="main-hero-btn">
							<a href="/why-food/" class="hero-banner-btn cr-intro-btn">Find out more</a>
						</div>	
					</div>
				</div>
			</div>	
		</div>
	</section>

	<section class="middle-part">
		<div class="container">
			<div class="middle-part-inner-2">
				<form class="main-form" method="get" action="/donate/">
					<div class="middle-part-buttons text-center">
						<div class="middle-part-inner">
							<div class="middle-dropdown">
							<div class="main-dropdown">
								Your amount
								<select class="main-select4" name="amount">
									<option value="">Hmm, let me see…</option>
									<option value="25">&#163;25</option>
									<option value="50">&#163;50</option>
									<option value="75">&#163;75</option>
									<option value="100">&#163;100</option>
									<option value="other">Other</option>
								</select>
								<input type="hidden" class="custom_amount" name="custom" value="">
							</div>	
							</div>
							<div class="middle-dropdown">
							<div class="main-dropdown">
								Your amount
								<select class="main-select4" name="type">
									<option value="">Monthly or one-off?</option>
									<option value="oneoff">Give Once</option>
									<option value="monthly">Monthly</option>
								</select>
							</div>	
							</div>
							<div class="middle-dropdown">
								<button class="donate-btn" type="submit">You&#39;re amazing!
									<span class="red-text">Quick donate</span>
								</button>
							</div>	
						</div>
					</div>
				</form>	
			</div>
			<div class="middle-part-des">
				<h2>Charity Right is an international food programme that provides regular meals in school settings to help children get an education and fulfil their potential.</h2>
			</div>	
		</div>
	</section>


	<section class="description-part">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12">
					<div class="description-part-title text-center">
						<h1>SO what makes Charity Right so special?</h1>
						<div class="description-points">
							<div class="description-points-inner">
								<div class="description-points-each">
									<h3 class="description-points-title">Regular and reliable support</h3>
									<p class="description-points-des">We provide regular school meals. Great for getting children into school. </p>
								</div>
								<div class="description-points-each">
									<h3 class="description-points-title">Not your average meal</h3>
									<p class="description-points-des">Our meals are packed with nutrients. Food that boosts health and growth.</p>
								</div>
								<div class="description-points-each">
									<h3 class="description-points-title">Total respect, at all times</h3>
									<p class="description-points-des">We listen to the people we support. And we help them help themselves.</p>
								</div>
								<div class="description-points-each">
									<h3 class="description-points-title">Lifelong community benefits</h3>
									<p class="description-points-des">We’re all about long-term solutions. Work that changes lives forever.</p>
								</div>
								<div class="description-points-each">
									<h3 class="description-points-title">We go the extra mile</h3>
									<p class="description-points-des">Our work is rarely straightforward. So we adapt to get the best results. </p>
								</div>
							</div>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</section>


	<section class="turkey-part support-right-now">
		<div class="turkey-part-slider">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-12">
						<div class="turkey-part-title">
							<h1>Who needs your support right now?</h1>
							<h3>Uyghur Muslims in Turkey</h3>
							<p>Millions of Uyghur Muslims in China are being arrested, tortured and abused. Many have left their lives behind and fled for safety in Istanbul. However without homes, income or food, life is extremely challenging.</p>
							<p>By providing regular food, we want to help these families develop stable, happy and safe futures.</p>
							<div class="turkey-part-btn">
								<a href="https://charityright.org.uk/cr-star-campaign/1777/food-for-uyghurs/?utm_source=Hmpg" class="" tabindex="0">Find out more</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-sm-12">
						<div class="turkey-part-image">
							<img src="../img/homeimages/turkey-image.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="turkey-part-slider">
			<div class="container">
				<div class="row">
				<div class="col-lg-6 col-sm-12">
				<div class="turkey-part-title">
					<h1>Who needs your support right now?</h1>
					<h3>Slum children in Dhaka</h3>
					<p>In Bangladesh, 33% of all children under five suffer from malnutrition. That means there are 7 million children
					who can&#39;t develop to their full mental and physical potential because they don’t have enough food.</p>
					<p>We guarantee a hot meal every school day, so they no longer have to risk their lives to work as window washers
					and street vendors to pay for meals.</p>
					<div class="turkey-part-btn">
						<a href="https://charityright.org.uk/cr-star-campaign/1012/school-meals-in-dhakas-slums/?utm_source=Hmpg" class="" tabindex="0">Find out more</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-sm-12">
				<div class="turkey-part-image">
					<img src="../img/homeimages/slum-children-in-dhaka.jpg">
				</div>
			</div>
				</div>
			</div>
		</div>
		<div class="turkey-part-slider">
			<div class="container">
				<div class="row">
				<div class="col-lg-6 col-sm-12">
				<div class="turkey-part-title">
					<h1>Who needs your support right now?</h1>
					<h3>Eritrean refugees in Sudan</h3>
					<p>Harsh conditions of the refugee camps, along with drought and high costs of living, have plunged many families
					into poverty. Eritrean refugees in Sudan lack adequate food and water, and it&#39;s not unusual for some to go days
					without a meal.</p>
					<p>Providing families with monthly food packs and regular meals to school children allows them to focus on
					developing other areas of their lives, free from the worry of their next meal.</p>
					<div class="turkey-part-btn">
						<a href="https://charityright.org.uk/cr-star-campaign/2032/fighting-hunger-in-sudan/?utm_source=Hmpg" class="" tabindex="0">Find out more</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-sm-12">
				<div class="turkey-part-image">
					<img src="../img/homeimages/eritrean-refugees-sudan.jpg">
				</div>
			</div>
				</div>
			</div>
		</div>
		<div class="turkey-part-slider">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-12">
						<div class="turkey-part-title">
							<h1>Who needs your support right now?</h1>
							<h3>Malnourished children in Thar, Pakistan</h3>
							<p>In the last three years, over 1,500 children died in the Thar Desert, in Pakistan due to malnutrition. Sadly, this is nothing new for the district. Frequent drought kills not just people, but also the livestock that residents depend on for their food, milk, and livelihood.</p>
							<p>Without help, people will continue to die of starvation.</p>
							<div class="turkey-part-btn">
								<a href="https://charityright.org.uk/cr-star-campaign/1009/pakistan-school-meals/?utm_source=Hmpg" class="" tabindex="0">Find out more</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-sm-12">
						<div class="turkey-part-image">
							<img src="../img/homeimages/children-thar-pakistan.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="turkey-part-slider">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-12">
						<div class="turkey-part-title">
							<h1>Who needs your support right now?</h1>
							<h3>Rohingya refugees in Bangladesh</h3>
							<p>Due to violence and persecution almost a million Rohingya refugees fled to Bangladesh seeking shelter and safety for their families. </p>
							<p>Approximately half of them are children. Every month, we deliver food packs to vulnerable families and provide regular meals to school children. For many of these children, school provides a familiar routine that can help them recover from trauma.</p>
							<div class="turkey-part-btn">
								<a href="https://charityright.org.uk/cr-star-campaign/1011/school-meals-for-the-rohingya/?utm_source=Hmpg" class="" tabindex="0">Find out more</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-sm-12">
						<div class="turkey-part-image">
							<img src="../img/homeimages/rohingya-refugees.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="achieved-part">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12">
					<div class="achieved-title text-center">
						<h2>What WE have achieved so far</h2>
						<p>Over the past 6 years, we’ve changed the lives of children and adults in many different ways:</p>
						<div class="achieved-points">
							<h2>Distributed over 13 million school meals.</h2>
							<h2>Delivered over 40 thousand family food packs.</h2>
							<h2>Improved the lives of over 100 thousand people.</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="turkey-part actually-again">
		<div class="turkey-part-slider">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-sm-12">
						<div class="turkey-part-title">
							<h1>But what do you actually do again?</h1>
							<h3>Our main work focuses on providing regular meals in a school setting to children who often go without food and any kind of education.</h3>
							<h4>But we also provide food packs to families who have little or no means of sourcing their next meal.</h4>
							<div class="turkey-part-btn">
								<a href="/why-food/" class="" tabindex="0">Find out more</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-12">
						<div class="turkey-part-image">
							<!-- <img src="../img/homeimages/actually-image.png"> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="help-part">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="help-part-inner">
						<h2>How can I help?</h2>
						<h3>There are many ways you can change children’s lives. Just choose the right one for you, or get in touch if you have any questions. </h3>
						<div class="help-part-points-main">
							<div class="help-part-points col-md-4 col-sm-6 col-xs-12" onClick="window.location.href=\'donate?other_amout_inner=10&region=schools\'">
								<div class="main-point-count">
									<span class="points-count">1</span>
								</div>	
								<span class="points-count-des">Give £10 to provide a school child with <br> a meal every school day for 1 month</span>
							</div>
							<div class="help-part-points col-md-4 col-sm-6 col-xs-12" onClick="window.location.href=\'donate?other_amout_inner=20&region=hifzmeals\'">
								<div class="main-point-count">
									<span class="points-count">2</span>
								</div>	
								<span class="points-count-des">Give £20 to provide a Hifz student with <br> 3 meals every day for 1 month</span>
							</div>
							<div class="help-part-points col-md-4 col-sm-6 col-xs-12" onClick="window.location.href=\'donate&region=food_packs\'">
								<div class="main-point-count">
									<span class="points-count">3</span>
								</div>	
								<span class="points-count-des">Give any amount to support our <br> monthly family food programme </span>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="news-views">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12">
					<div class="news-views-inner">
						<h1>Our news & views</h1>
						<h3>Find out about Charity Right’s latest news, choose a blog post to read or learn more about a fundraising event. </h3>
						<div class="news-views-articles">
							<div class="each-article" onClick="window.location.href = \'https://charityright.org.uk/post/as-tensions-rise-over-hong-kong-the-tragedy-of-chinas-uyghurs-goes-unnoticed/\'">
								<div class="article-image">
									<img src="../img/homeimages/over-hong-kong.jpg">
								</div>
								<div class="article-info">
									<div class="article-des">
										<h3>As tensions rise over Hong Kong the tragedy of China’s Uyghurs goes unnoticed</h3>
									</div>
								</div>
							</div>
							<div class="each-article" onClick="window.location.href = \'https://charityright.org.uk/post/after-fleeing-mynamar-ibrahim-is-now-receiving-life-changing-support/\'">
								<div class="article-image">
									<img src="../img/homeimages/mayanmar-life-changing.jpg">
								</div>
								<div class="article-info">
									<div class="article-des">
										<h3>After Fleeing Mynamar, Ibrahim Is Now Receiving Life-Changing Support</h3>
									</div>
								</div>
							</div>
							<div class="each-article" onClick="window.location.href = \'https://charityright.org.uk/post/why-the-government-has-no-option-but-to-reopen-schools/\'">
								<div class="article-image">
									<img src="../img/homeimages/why-gov.jpg">
								</div>
								<div class="article-info">
									<div class="article-des">
										<h3>Why the government has no option but to reopen schools.</h3>
									</div>
								</div>
							</div>
							<div class="each-article" onClick="window.location.href = \'https://charityright.org.uk/post/rt-news-interview-with-ceo/\'">
								<div class="article-image">
									<img src="../img/homeimages/rt-news-ceo.jpg">
								</div>
								<div class="article-info">
									<div class="article-des">
										<h3>RT News Interview with CEO: The World Food Crisis.</h3>
									</div>
								</div>
							</div>
							<div class="each-article" onClick="window.location.href = \'https://charityright.org.uk/post/what-have-you-been-grateful-for-today/\'">
								<div class="article-image">
								<img src="../img/homeimages/grateful-today.jpg">
								</div>
								<div class="article-info">
									<div class="article-des">
										<h3>What Are You Grateful For Today?</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>';
			/*echo '<div class="container index_top"';
			if ($hasbanner && ($bannerlink = $this->InputSafeString($this->page->details['bannerlink'])))
			{	echo ' onclick="window.location.replace(\'', $bannerlink, '\')" style="cursor: pointer;"';
			}
			echo '>';
			//echo '<mtn-widget-button>Donate now</mtn-widget-button>';
			if ($hasbanner)
			{	echo '<img src="', $this->page->GetBannerSRC(), '" />';
			}
			if ($this->page->details['headertext'])
			{	echo stripslashes($this->page->details['headertext']);
			}
			echo '</div>';*/
		}
		//echo $this->MainBodyQuickDonateForm();
		/*if ($topcontent = $this->page->HTMLMainContent())
		{	echo '<div class="container index_second"><div class="container_inner">', $topcontent, '</div></div>';
		}*/
		//echo $this->MainBodyContentPageSections();//, '<div id="arrow-bottom" class="animated bounce infinite hinge"><a href="#bottom"><span class="icon-down-open"></span></a></div>';
	} // end of fn MainBodyContent

} // end of class IndexPage
?>