<?php
include_once('init.php');

class MyAccountPage extends BasePage
{	private $menupage = '';

	function __construct($menupage = '') // constructor
	{	parent::__construct('my-account');
		$this->menupage = $menupage;
		$this->css[] = 'cart.css';
		$this->css[] = 'myaccount.css';
		if ($this->customer->id)
		{	$this->LoggedInConstruct();
		}
	} // end of fn __construct, constructor
	
	public function LoggedInConstruct()
	{	
	} // end of fn LoggedInConstruct
	
	function MainBodyContent()
	{	echo '<div class="container"><div class="container_inner">';
		if ($this->customer->id)
		{	echo '<h1 class="page_heading">My Account</h1><div class="right_sidebar right_sidebar_myaccount">', $this->MyAccountMenu(), '</div><div class="left_content left_content_myaccount">', $this->MyAccountBody(), '</div><div class="clear"></div>';
		} else
		{	echo $this->LoginContent();
		}
		echo '</div></div>';
	} // end of fn MainBodyContent
	
	protected function MyAccountBody()
	{	ob_start();
	//	echo $this->MyDetailsForm();
		return ob_get_clean();
	} // end of fn MyAccountBody
	
	public function MyAccountMenu()
	{	ob_start();
		echo '<ul>';
		foreach ($this->MyAccountMenuOptions() as $menuname=>$menuitem)
		{	echo '<li', $menuname == $this->menupage ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
		}
		echo '</ul>';
		return ob_get_clean();
	} // end of fn MyAccountMenu
	
	public function MyAccountMenuOptions()
	{	$options = array();
		
		$options['details'] = array('text'=>'My Details', 'link'=>SITE_URL . 'mydetails.php');
		$options['orders'] = array('text'=>'My Purchases', 'link'=>SITE_URL . 'myorders.php');
		$options['logout'] = array('text'=>'Log Out', 'link'=>SITE_URL . 'index.php?logout=1');
		
		return $options;
	} // end of fn MyAccountMenuOptions
	
} // end of defn MyAccountPage
?>