<?php
include_once('init.php');

class PageContentPopup extends Base
{	private $page = '';

	public function __construct($pagename = '') // constructor
	{	parent::__construct();
		$this->page = new PageContent($pagename);
	} // end of fn __construct, constructor

	public function OutputAjax()
	{	return $this->page->HTMLMainContent();
	} // end of fn OutputAjax

	public function PopUpLink($linktext = '', $classes = '')
	{	ob_start();
		echo '<a onclick="pageContentPopup(\'', $this->page->details['pagename'], '\');" class="', is_array($classes) && $classes ? implode(' ', $classes) : $classes, '">', $this->InputSafeString($linktext ? $linktext : $this->page->details['htmltitle']), '</a>';
		static $popupdone = 0;
		if (!$popupdone++)
		{	echo '<div id="pagecontent_modal_popup" class="jqmWindow pagecontent_modal_popup"><a href="#" class="jqmClose">Close</a><div class="clear"></div><div id="pagecontent_modal_popup_inner" class="pagecontent_modal_popup_inner"></div></div><script>$("#pagecontent_modal_popup").jqm();</script>';
		}
		return ob_get_clean();
	} // end of fn PopUpLink
	
} // end of defn PageContentPopup
?>