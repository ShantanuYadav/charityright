<?php
class Language extends Base
{	
	var $code = "";
	var $details = array();
	
	function __construct($code = 0)
	{	parent::__construct();
		$this->Get($code);
	} //  end of fn __construct
	
	function Reset()
	{	$this->code = "";
		$this->details = array();
	} // end of fn Reset
	
	function Get($code = 0)
	{	$this->Reset();
		if (is_array($code))
		{	$this->details = $code;
			$this->code = $code["langcode"];
		} else
		{	if ($code = $this->SQLSafe($code))
			{	$sql = "SELECT * FROM languages WHERE langcode='" . $code . "'";
				if ($result = $this->db->Query($sql))
				{	if ($row = $this->db->FetchArray($result))
					{	return $this->Get($row);
					}
				}
			}
		}
		
	} // end of fn Get
	
} // end of defn Language
?>