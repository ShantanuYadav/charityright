<?php
class InfusionSoftCRM extends InfusionSoft
{	
	
	public function AddContactFromEmail($email = '')
	{	if (($email = $this->CRMEmailFromEmail($email)) && $email->EmailOK())
		{	$cp = new ContactPreferences();
			if ($contactID = $this->AddContact($email, false))
			{	return $contactID;
			}
		}
	} // fn AddContactFromEmail
	
	public function UploadDataFromPost($email = '')
	{	
		if (($email = $this->CRMEmailFromEmail($email)) && $email->EmailOK() && ($emailadd = $email->Email()))
		{	$data = array(	'Website' => SITE_URL . ADMIN_URL . '/crm_email.php?email=' . urlencode($emailadd),
							'Email' => $emailadd
						);
			return $data;
		}
	} // end of fn UploadDataFromPost
	
	protected function CRMEmailFromEmail($email = 0)
	{	if (is_a($email, 'CRMEmail'))
		{	return $email;
		} else
		{	return new CRMEmail($email);
		}
	} // fn CRMEmailFromEmail
	
	public function AddOptinTagsToContactFromValue($email = '', $contactpref = 0)
	{	$contactpref = (int)$contactpref;
		if (($email = $this->CRMEmailFromEmail($email)) && $email->EmailOK())
		{	if ($contactid = $this->AddContactFromEmail($email))
			{	$cp = new ContactPreferences();
				if ($cp->IsOptionsSetFromValue($contactpref, 'email'))
				{	$this->SetEmailOptIn($email->Email(), true);
				}
				$this->AddOptinTagsToContact($contactid, $cp, $contactpref);
			}
		}
	} // fn AddOptinTagsToContactFromValue

} // end of defn InfusionSoftCRM
?>