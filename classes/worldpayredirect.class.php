<?php
class WorldPayRedirect extends Base
{	var $instID = '1096495';
	var $remoteInstID = '1255504';
	var $authPW = 'qTDwD6sw'; // use for api calls
	var $callbackPW = '4bd21056wbQg';
	var $compName = 'Charity Right';
	var $wp_url = '';
	var $wp_url_live = 'https://secure.worldpay.com/wcc/';
	var $wp_url_test = 'https://secure-test.worldpay.com/wcc/';
	var $wp_purchase_url = 'purchase';
	var $wp_itransaction_url = 'itransaction';
	var $wp_iadmin_url = 'iadmin';
	var $notifyurl = 'wp_ear.php';
	var $live = true; //false;

	public function __construct($live = true)
	{	parent::__construct();
		$this->notifyurl = HTTP_PREFIX . $this->notifyurl;
		$this->SetLive($live);
	} // fn __construct

	protected function SetLive($live = false)
	{	if ($this->live == $live)
		{	$this->wp_url = $this->wp_url_live;
		} else
		{	$this->wp_url = $this->wp_url_test;
		}
		$this->wp_purchase_url = $this->wp_url . $this->wp_purchase_url;
		$this->wp_iadmin_url = $this->wp_url . $this->wp_iadmin_url;
		$this->wp_itransaction_url = $this->wp_url . $this->wp_itransaction_url;
	} // end of fn SetLive

	protected function SendIadminRequest($params = array())
	{	$params['instId'] = $this->remoteInstID;
		$params['authPW'] = $this->authPW;
		return  $this->GetRemoteRequest($params, $this->wp_iadmin_url);
	} // end of fn SendIadminRequest

	protected function SendITransactionRequest($params = array())
	{	$params['instId'] = $this->remoteInstID;
		$params['authPW'] = $this->authPW;
		return $this->GetRemoteRequest($params, $this->wp_itransaction_url);
	} // end of fn SendITransactionRequest
	
	public function RefundDonation($donation)
	{	$params = array();
		if (SITE_TEST)
		{	return array('result'=>'A');
		}
		// actual parameters needed here
		$params['cartId'] = 'Refund';
		$params['op'] = 'refund-full';
		$params['transId'] = $donation->WPTransactionId();
		$params['refund'] = 'Issue Refund';
		if (!$this->live)
		{	$params['testMode'] = '100';
		}
		
		return $this->SendITransactionRequest($params);
	} // end of fn RefundDonation

	protected function GetRemoteRequest($params = array(), $url = '')
	{	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POST, 1);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->Array2Query($params));
		//$response = $this->Query2Array(curl_exec($ch));
		if (curl_errno($ch))
		{	$response = array('result'=>'E', 'message'=>'CURL send a error during perform operation: ' . curl_errno($ch));
		} else
		{	$result = curl_exec($ch);
			$response = array('result'=>substr($result, 0, 1), 'message'=>substr($result, 2));
		}
		return $response;
	} // end of fn GetRemoteRequest
	
	protected function Array2Query($array)
	{	$qlist = array();
		foreach ($array as $key=>$value)
		{	$val = urlencode($value);
			if ($key && $val)
			{	$qlist[] = $key . '=' . $val;
			}
		}
		return implode("&", $qlist);
	} // end of fn Array2Query
	
	public function CRStarsDonationButton(CampaignDonation $donation)
	{	$campaign = new Campaign($donation->details['campaignid']);
		$callbackurl = $_SERVER['HTTP_HOST'] . SITE_SUB . '/wp_ear.php';
	//	$callbackurl = str_replace('charityright.org.uk', 'worldpay.charityright.org.uk', $callbackurl);
		echo '<form action="', $this->wp_purchase_url, '" method="post" target="_self" name="wp_form" id="wp_button">
			<input type="hidden" name="instId" value="', $this->instID, '" />
			<input type="hidden" name="M_subtype" value="donation_crstars" />
			<input type="hidden" name="MC_callback" value="', $callbackurl, '" />
			<input type="hidden" name="C_returnurl" value="', $campaign->ThankYouLink(), '" />
			<input type="hidden" name="C_cancelurl" value="', $campaign->Link(), '" />
			<input type="hidden" name="compName" value="', $this->compName, '" />
			<input type="hidden" name="desc" value="Donation to ', strip_tags($campaign->FullTitle()), '" />
			<input type="hidden" name="currency" value="', $donation->details['currency'], '" />
			<input type="hidden" name="hideCurrency" value="true">
			<input type="hidden" name="cartId" value="', $donation->id, '" />
			<input type="hidden" name="amount" value="', number_format($donation->details['amount'], 2, ".", ""), '" />
			<input type="hidden" name="noLanguageMenu" value="true" />
			<input type="hidden" name="name" value="', $this->InputSafeString($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname']), '" />
			<input type="hidden" name="address1" value="', $this->InputSafeString($donation->details['donoradd1']), '" />
			<input type="hidden" name="address2" value="', $this->InputSafeString($donation->details['donoradd2']), '" />
			<input type="hidden" name="town" value="', $this->InputSafeString($donation->details['donorcity']), '" />
			<input type="hidden" name="postcode" value="', $this->InputSafeString($donation->details['donorpostcode']), '" />
			<input type="hidden" name="country" value="', $this->InputSafeString($donation->details['donorcountry']), '" />
			<input type="hidden" name="email" value="', $donation->details['donoremail'], '" />';
		if ($donation->details['donorphone'])
		{	echo '<input type="hidden" name="tel" value="', $this->InputSafeString($donation->details['donorphone']), '" />';
		}
		if (!$this->live)
		{	echo '<input type="hidden" name="testMode" value="100" />';
		}
		echo '<noscript><input type="submit" class="submit" value="Pay now" /></noscript></form>';
	} // end of fn CRStarsDonationButton
	
	public function EventBookingOrderButton(BookOrder $bookorder)
	{	$cancelpage = new PageContent('events');
		$thankyoupage = new PageContent('event-booked-thank-you');
		$callbackurl = $_SERVER['HTTP_HOST'] . SITE_SUB . '/wp_ear.php';
	//	$callbackurl = str_replace('charityright.org.uk', 'worldpay.charityright.org.uk', $callbackurl);
		echo '<form action="', $this->wp_purchase_url, '" method="post" target="_self" name="wp_form" id="wp_button">
			<input type="hidden" name="instId" value="', $this->instID, '" />
			<input type="hidden" name="M_subtype" value="event_booking" />
			<input type="hidden" name="MC_callback" value="', $callbackurl, '" />
			<input type="hidden" name="C_returnurl" value="', substr(SITE_URL, 0, -1), $thankyoupage->Link(), $bookorder->id, '/" />
			<input type="hidden" name="C_cancelurl" value="', substr(SITE_URL, 0, -1), $cancelpage->Link(), '" />
			<input type="hidden" name="compName" value="', $this->compName, '" />
			<input type="hidden" name="desc" value="Charity Right event booking for ', $bookorder->TicketCount(), ' tickets" />
			<input type="hidden" name="currency" value="', $bookorder->details['currency'], '" />
			<input type="hidden" name="hideCurrency" value="true">
			<input type="hidden" name="cartId" value="', $bookorder->id, '" />
			<input type="hidden" name="amount" value="', number_format($bookorder->TotalPrice(), 2, ".", ""), '" />
			<input type="hidden" name="noLanguageMenu" value="true" />
			<input type="hidden" name="name" value="', $this->InputSafeString($bookorder->details['firstname'] . ' ' . $bookorder->details['lastname']), '" />
			<input type="hidden" name="address1" value="', $this->InputSafeString($bookorder->details['address1']), '" />
			<input type="hidden" name="address2" value="', $this->InputSafeString($bookorder->details['address2']), '" />
			<input type="hidden" name="town" value="', $this->InputSafeString($bookorder->details['city']), '" />
			<input type="hidden" name="postcode" value="', $this->InputSafeString($bookorder->details['postcode']), '" />
			<input type="hidden" name="country" value="', $this->InputSafeString($bookorder->details['country']), '" />
			<input type="hidden" name="email" value="', $bookorder->details['email'], '" />';
		if ($bookorder->details['phone'])
		{	echo '<input type="hidden" name="tel" value="', $this->InputSafeString($bookorder->details['phone']), '" />';
		}
		if (!$this->live)
		{	echo '<input type="hidden" name="testMode" value="100" />';
		}
		echo '<noscript><input type="submit" class="submit" value="Pay now" /></noscript></form>';
	} // end of fn EventBookingOrderButton
	
	public function OneOffDonationButton(Donation $donation)
	{	ob_start();
		$callbackurl = $_SERVER['HTTP_HOST'] . SITE_SUB . '/wp_ear.php';
	//	$callbackurl = str_replace('charityright.org.uk', 'worldpay.charityright.org.uk', $callbackurl);
		$donatepage = new PageContent('donate');
		echo '<form action="', $this->wp_purchase_url, '" method="post" target="_self" name="wp_form" id="wp_button">
			<input type="hidden" name="instId" value="', $this->instID, '" />
			<input type="hidden" name="M_subtype" value="donation_oneoff" />
			<input type="hidden" name="C_returnurl" value="', $donation->ThankYouLink(), '" />
			<input type="hidden" name="C_cancelurl" value="', $donatepage->FullLink(), '" />
			<input type="hidden" name="MC_callback" value="', $callbackurl, '" />
			<input type="hidden" name="compName" value="', $this->compName, '" />
			<input type="hidden" name="desc" value="Donation to Charity Right for ', $donation->GatewayTitle(), '" />
			<input type="hidden" name="currency" value="', $donation->details['currency'], '" />
			<input type="hidden" name="hideCurrency" value="true"><input type="hidden" name="cartId" value="', $donation->id, '" />
			<input type="hidden" name="amount" value="', number_format($donation->details['amount'] + $donation->details['adminamount'], 2, ".", ""), '" />
			<input type="hidden" name="name" value="', $this->InputSafeString($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname']), '" />
			<input type="hidden" name="address1" value="', $this->InputSafeString($donation->details['donoradd1']), '" />
			<input type="hidden" name="address2" value="', $this->InputSafeString($donation->details['donoradd2']), '" />
			<input type="hidden" name="town" value="', $this->InputSafeString($donation->details['donorcity']), '" />
			<input type="hidden" name="postcode" value="', $this->InputSafeString($donation->details['donorpostcode']), '" />
			<input type="hidden" name="country" value="', $this->InputSafeString($donation->details['donorcountry']), '" />
			<input type="hidden" name="noLanguageMenu" value="true">
			<input type="hidden" name="email" value="', $donation->details['donoremail'], '" />';
		if ($donation->details['donorphone'])
		{	echo '<input type="hidden" name="tel" value="', $this->InputSafeString($donation->details['donorphone']), '" />';
		}
		if (!$this->live)
		{	echo '<input type="hidden" name="testMode" value="100" />';
		}
		echo '<noscript><input type="submit" class="submit" value="Donate now" /></noscript></form>';
		return ob_get_clean();
	} // end of fn OneOffDonationButton
	
	public function CartOrderButton(CartOrder $cartorder)
	{	ob_start();
		$callbackurl = $_SERVER['HTTP_HOST'] . SITE_SUB . '/wp_ear.php';
		$callbackurl = str_replace('charityright.org.uk', 'wp.charityright.org.uk', $callbackurl);
		$params = $cartorder->WorldpayParams();
		if ($params['amount'] > 0)
		{	echo '<form action="', $this->wp_purchase_url, '" method="post" target="_self" name="wp_form" id="wp_button">
				<input type="hidden" name="instId" value="', $this->instID, '" />
				<input type="hidden" name="M_subtype" value="donation_cart" />
				<input type="hidden" name="C_returnurl" value="', $cartorder->ThankYouLink(), '" />
				<input type="hidden" name="C_cancelurl" value="', $cartorder->CancelLink(), '" />
				<input type="hidden" name="MC_callback" value="', $callbackurl, '" />
				<input type="hidden" name="compName" value="', $this->compName, '" />
				<input type="hidden" name="desc" value="Donation to Charity Right ', $params['desc'], '" />
				<input type="hidden" name="currency" value="', $cartorder->details['currency'], '" />
				<input type="hidden" name="hideCurrency" value="true"><input type="hidden" name="cartId" value="', $cartorder->id, '" />
				<input type="hidden" name="amount" value="', number_format($params['amount'], 2, '.', ''), '" />
				<input type="hidden" name="name" value="', $this->InputSafeString($cartorder->details['donortitle'] . ' ' . $cartorder->details['donorfirstname'] . ' ' . $cartorder->details['donorsurname']), '" />
				<input type="hidden" name="address1" value="', $this->InputSafeString($cartorder->details['donoradd1']), '" />
				<input type="hidden" name="address2" value="', $this->InputSafeString($cartorder->details['donoradd2']), '" />
				<input type="hidden" name="town" value="', $this->InputSafeString($cartorder->details['donorcity']), '" />
				<input type="hidden" name="postcode" value="', $this->InputSafeString($cartorder->details['donorpostcode']), '" />
				<input type="hidden" name="country" value="', $this->InputSafeString($cartorder->details['donorcountry']), '" />
				<input type="hidden" name="noLanguageMenu" value="true">
				<input type="hidden" name="email" value="', $cartorder->details['donoremail'], '" />';
			if ($cartorder->details['donorphone'])
			{	echo '<input type="hidden" name="tel" value="', $this->InputSafeString($cartorder->details['donorphone']), '" />';
			}
			if (!$this->live)
			{	echo '<input type="hidden" name="testMode" value="100" />';
			}
			echo '<noscript><input type="submit" class="submit" value="Donate now" /></noscript></form>';
		}// else $this->VarDump($params);
		return ob_get_clean();
	} // end of fn CartOrderButton
	
} // end of defn WorldPayRedirect
?>