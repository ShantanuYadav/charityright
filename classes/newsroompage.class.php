<?php 
class NewsRoomPage extends PagePage
{	
	public function __construct()
	{	parent::__construct('newsroom');
		$this->css[] = 'news.css';
	} // end of fn __construct
	
	protected function MiddleContent()
	{	ob_start();
		echo '<div id="newsroom_top"></div><div id="newsroom_middle">', $this->NewsRoomMiddleContent(), '</div><div id="newsroom_bottom"></div>';
		return ob_get_clean();
	} // end of fn MiddleContent
	
	protected function NewsRoomMiddleContent()
	{	ob_start();
		echo '<h2>';
		if ($_GET['catid'] && ($cat = new NewsCat($_GET['catid'])) && $cat->id)
		{	echo '<span>Newsroom: </span>', $this->InputSafeString($cat->details['catname']);
		} else
		{	echo '<span>Latest</span> News';
		}
		$news = new NewsStories(true, $cat->id);
		echo '</h2>', $this->NewsList($news->stories);
		return ob_get_clean();
	} // end of fn NewsRoomMiddleContent
	
	protected function NewsList($news_stories = array())
	{	ob_start();
		if (is_array($news_stories) && count($news_stories))
		{	echo '<ul id="newslist">';
			foreach ($news_stories as $story)
			{	$link = 'news.php?story=' . $story->id;
				echo '<li>';
				if ($photo_exists = $story->PhotoExists())
				{	echo '<div class="storyphoto"><a href="', $link, '"><img width="160px" src="', $story->ImageSRC(), '" alt="', $this->InputSafeString($story->details['headline']), '" title="', $this->InputSafeString($story->details['headline']), '" /></a></div>';
				}
				echo '<div class="storydetails', $photo_exists ? ' sd_withphoto' : '', '"><h3>', $this->InputSafeString($story->details['headline']), '</h3><p class="storytext">', $story->SummaryText(200), '</p><div class="storyfooter"><div class="storydate">', date('jS F Y', strtotime($story->details['submitted'])), '</div><div class="readmore"><a href="', $link, '">Read More &raquo;</a></div><div class="clear"></div></div></div><div class="clear"></div>';
				//print_r($story);
				echo '</li>';
			}
			echo '</ul>';
		}
		return ob_get_clean();
	} // end of fn NewsList
	
} // end of class NewsRoomPage
?>