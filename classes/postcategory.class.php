<?php
class PostCategory extends BlankItem
{	protected $imagelocation = '';
	protected $imagedir = '';
	protected $template_dir;
	
	public function __construct($id = 0, $slug = '')
	{	parent::__construct($id, 'postcategories', 'pcatid');
		$this->template_dir = CITDOC_ROOT . '/templates/postcats/';
		$this->imagelocation = SITE_URL . 'img/postcategory/';
		$this->imagedir = CITDOC_ROOT . '/img/postcategory/';
		if (!$this->id && $slug)
		{	$sql = 'SELECT * FROM postcategories WHERE slug="' . $this->SQLSafe($slug) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
	} // fn __construct
	
	public function Link()
	{	if ($this->id) return SITE_URL . 'posts/' . $this->details['slug'] . '/';
	} // fn Link
	
	public function GetPosts($filter = array(), $liveonly = true)
	{	if (!is_array($filter))
		{	$filter = array();
		}
		$tables = array('postsforcats'=>'postsforcats', 'posts'=>'posts');
		$fields = array('posts.*');
		$where = array('posts_link'=>'posts.postid=postsforcats.postid', 'pcatid'=>'postsforcats.pcatid=' . (int)$this->id);
		$orderby = array('posts.postdate DESC', 'posts.updated DESC');
		
		if ($filter['country'])
		{	$ctrysql = 'SELECT shortcode FROM countries WHERE postslug="' . $this->SQLSafe($filter['country']) . '"';
			if ($ctryresult = $this->db->Query($ctrysql))
			{	if ($ctryrow = $this->db->FetchArray($ctryresult))
				{	$where['country'] = 'posts.country="' . $ctryrow['shortcode'] . '"';
				}
			}
		}
		
		if ($liveonly)
		{	$where['live'] = 'posts.live=1';
		}
		
		//echo $this->db->BuildSQL($tables, $fields, $where, $orderby);
		
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'postid', true);
	} // fn GetPosts
	
	public function GetCountries($liveonly = true)
	{	if ($this->id)
		{	$tables = array('postsforcats'=>'postsforcats', 'posts'=>'posts', 'countries'=>'countries');
			$fields = array('countries.shortcode', 'countries.shortname', 'countries.postslug');
			$where = array('posts_link'=>'posts.postid=postsforcats.postid', 'countries_link'=>'posts.country=countries.shortcode', 'pcatid'=>'postsforcats.pcatid=' . $this->id, 'postslug'=>'NOT countries.postslug=""');
			$orderby = array('countries.shortname ASC');
			$groupby = array('countries.shortcode');
			
			if ($liveonly)
			{	$where['live'] = 'posts.live=1';
			}
			
			//echo $this->db->BuildSQL($tables, $fields, $where, $orderby, $groupby);
			
			return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby, $groupby), 'postslug', true);
		} else
		{	return array();
		}
	} // fn GetCountries
	
	public function HasImage()
	{	return $this->id && file_exists($this->GetImageFile());
	} // end of fn HasImage
	
	public function GetImageFile()
	{	return $this->imagedir . $this->id .'.png';
	} // end of fn GetImageFile
	
	public function GetImageSRC()
	{	if ($this->HasImage())
		{	return $this->imagelocation . $this->id . '.png';
		}
	} // end of fn GetImageSRC

	public function GetQuickDonateOptions($live_only = true)
	{	$options = json_decode($this->details['quickdonate'], true);
		return ($options['live'] || !$live_only) ? $options : array();
	} // end of fn GetQuickDonateOptions
	
	public function TemplateFile()
	{	return $this->template_dir . $this->details['template'] . '.php';
	} // fn TemplateFile
	
} // end of defn PostCategory
?>
