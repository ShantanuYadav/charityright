<?php
class AdminEventDate extends EventDate
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct
	
	public function Save($data = array(), $eid = 0)
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if (!$this->id)
		{	if (($eid = (int)$eid) && ($event = new Event($eid)) && $event->id)
			{	$fields[] = 'eid=' . $eid;
			} else
			{	$fail[] = 'the event is missing';
			}
		}
		
		if (($venueid = (int)$data['venue']) && ($venue = new EventVenue($venueid)) && $venue->id)
		{	$fields[] = 'venue=' . $venueid;
		} else
		{	$fail[] = 'the venue is missing';
		}
		
		$fields[] = 'allday=' . ($data['allday'] ? '1' : '0');
	
		// start date and time
		if (($d = (int)$data['dstart']) && ($m = (int)$data['mstart']) && ($y = (int)$data['ystart']))
		{	$starttime = $this->datefn->SQLDate(mktime(0,0,0,$m, $d, $y)) . ' ' . ($data['allday'] ? '00:00:00' : ($this->StringToTime($data['stime']) . ':00'));
			$fields['starttime'] = 'starttime="' . $starttime . '"';
		} else
		{	$fail[] = 'start date missing';
		}
		
		// end date and time
		if (($d = (int)$data['dend']) && ($m = (int)$data['mend']) && ($y = (int)$data['yend']))
		{	$endtime = $this->datefn->SQLDate(mktime(0,0,0,$m, $d, $y)) . ' ' . ($data['allday'] ? '23:59:59' : ($this->StringToTime($data['etime']) . ':00'));
			$fields['endtime'] = 'endtime="' . $endtime . '"';
		} else
		{	$fail[] = 'end date missing';
		}
		
		if (!(int)$endtime || !(int)$starttime || ($endtime < $starttime))
		{	unset($fields['endtime'], $fields['starttime']);
			$fail[] = 'event cannot end before it begins';
		}
		$fields[] = 'live=' . ($data['live'] ? '1' : '0');
		
		if ($data['currency'])
		{	if ($this->GetCurrency($data['currency']))
			{	$fields[] = 'currency="' . $data['currency'] . '"';
			} else
			{	$fail[] = 'currency not found';
			}
		} else
		{	$fail[] = 'currency is missing';
		}
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE eventdates SET ' . $set . ' WHERE edid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO eventdates SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new event date added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function CanDelete()
	{	return $this->id && !$this->GetTicketTypes();
	} // end of fn CanDelete
	
	public function InputForm($eventid = 0)
	{	ob_start();
		$data = $this->details;
		if ($data = $this->details)
		{	$stime = substr($data['starttime'], 11, 5);
			$etime = substr($data['endtime'], 11, 5);
		} else
		{	$data = $_POST;
			$stime = $this->StringToTime($data["stime"]);
			$etime = $this->StringToTime($data["etime"]);
			
			if (($d = (int)$data['dstart']) && ($m = (int)$data['mstart']) && ($y = (int)$data['ystart']))
			{	$data['starttime'] = $this->datefn->SQLDate(mktime(0,0,0,$m, $d, $y)) . ' ' . $stime;
			}
			
			if (($d = (int)$data['dend']) && ($m = (int)$data['mend']) && ($y = (int)$data['yend']))
			{	$data['endtime'] = $this->datefn->SQLDate(mktime(0,0,0,$m, $d, $y)) . ' ' . $etime;
			}
		}

		if ($this->id)
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		} else
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?eid=' . $eventid);
		}
		$form->AddSelectWithGroups('Venue', 'venue', $data['venue'], '', $this->EventVenuesList(), 1, 1, '');
		$form->AddCheckBox('All day event', 'allday', 1, $data['allday']);
		$form->AddMultiInput('Event starts', array(
						array('type'=>'DATE', 'name'=>'start', 'value'=>$data['starttime'], 'years'=>$years), 
						array('type'=>'TEXT', 'name'=>'stime', 'value'=>$stime, 'css'=>'short', 'maxlength'=>5)), 
					true);
		$form->AddMultiInput('Event ends', array(
						array('type'=>'DATE', 'name'=>'end', 'value'=>$data['endtime'], 'years'=>$years), 
						array('type'=>'TEXT', 'name'=>'etime', 'value'=>$etime, 'css'=>'short', 'maxlength'=>5)), 
					true);
		$form->AddSelectWithGroups('Currency for tickets', 'currency', $data['currency'], '', $this->CurrencyList(), 1, 1, '');
		$form->AddCheckBox('Live (visible in front end)', 'live', 1, $data['live']);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Date', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this date</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
	public function TicketTypesTable()
	{	ob_start();
		$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="5"><a href="tickettype.php?edid=', $this->id, '">Add new ticket type</a></th></tr><tr><th>Ticket type</th><th class="num">Price</th><th class="num">Booking limit</th><th class="num">Bookings</th><th>Actions</th></tr>';
		$filter = array('orderby'=>'name');
		if ($tickettypes = $this->GetTicketTypes($filter))
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			$currency = $this->GetCurrency($this->details['currency']);
			
			foreach ($tickettypes as $ttyperow)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$ttype = new AdminTicketType($ttyperow);
					echo '<tr class="stripe', $i++ % 2,  '"><td>', $this->InputSafeString($ttype->details['ttypename']), '</td><td class="num">', $currency['cursymbol'], number_format($ttype->details['price'], 2), ' [', $this->details['currency'], ']</td><td class="num">', $ttype->details['live'] ? '' : 'NOT LIVE<br />', $ttype->details['stockcontrol'] ? '' : '[not controlled] ', (int)$ttype->details['booklimit'], '</td><td class="num">', $bookcount = count($ttype->GetBookings()), '</td><td><a href="tickettype.php?id=', $ttype->id, '">edit</a>';
					if ($bookcount)
					{	echo '&nbsp;|&nbsp;<a href="tickettypebookings.php?id=', $ttype->id, '">bookings</a>';
					}
					if ($ttype->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="tickettype.php?id=', $ttype->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($tickettypes) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($tickettypes), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
		return ob_get_clean();
	} // end of fn TicketTypesTable
	
	public function BookingsTable()
	{	ob_start();
		$perpage = 30;
		if ($bookings = $this->GetBookings())
		{	echo '<table><tr><th class="num">Booking number</th><th>Ticket type</th><th class="num">Order number</th><th>Booked by</th><th>Booked on</th><th>Actions</th></tr>';
			if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			$tickets = $this->GetTicketTypes();
			$orders = array();
			
			foreach ($bookings as $booking)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					if (!$orders[$booking['orderitem']])
					{	$orders[$booking['orderitem']] = $this->OrderFromBooking($booking);
					}
					echo '<tr class="stripe', $i++ % 2,  '"><td class="num">', (int)$booking['bid'], '</td><td>', $this->InputSafeString($tickets[$booking['ttid']]['ttypename']), '</td><td class="num">', $orders[$booking['orderitem']]->id, '</td><td>', $this->InputSafeString($orders[$booking['orderitem']]->details['firstname'] . ' ' . $orders[$booking['orderitem']]->details['lastname']), ' (' . $orders[$booking['orderitem']]->details['email'] . ')</td><td>', date('d/m/y @H:i', strtotime($orders[$booking['orderitem']]->details['orderdate'])), '</td><td><a href="bookorder.php?id=', $orders[$booking['orderitem']]->id, '">view order</a></td></tr>';
				}
			}
			echo '</table>';
			if (count($bookings) > $perpage)
			{	$pagelink = $_SERVER['SCRIPT_NAME'];
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
					if ($get)
					{	$pagelink .= '?' . implode('&', $get);
					}
				}
				$pag = new Pagination($_GET['page'], count($bookings), $perpage, $pagelink, array(), 'page');
				echo '<div class="pagination">', $pag->Display(), '</div>';
			}
			echo '<p class="adminCSVDownload"><a href="eventdatebookings_csv.php?id=', $this->id, '">download csv of all bookings for this date</a></p>';
		}
		return ob_get_clean();
	} // end of fn BookingsTable
	
	protected function AssignBookOrder($orderrow = array())
	{	return new AdminBookOrder($orderrow);
	} // fn AssignBookOrder
	
	public function BookingsCSVOuput()
	{	$csv = new CSVReport();
		$csv->AddToHeaders(array('event', 'venue', 'start date', 'booking number', 'ticket type', 'customer name', 'customer email', 'booked', 'order number'));
		
		if ($bookings = $this->GetBookings())
		{	
			$tickets = $this->GetTicketTypes();
			$orders = array();
			
			foreach ($bookings as $booking)
			{	if (!$orders[$booking['orderitem']])
				{	$orders[$booking['orderitem']] = $this->OrderFromBooking($booking);
				}
				$row = array('"' . $this->InputSafeString($this->event['eventname']) . '"', '"' . $this->InputSafeString($this->venue['venuename']) . '"', '"' . substr($this->details['starttime'], 0, 10) . '"', (int)$booking['bid'], '"' . $this->InputSafeString($tickets[$booking['ttid']]['ttypename']) . '"' , '"' . $this->InputSafeString($orders[$booking['orderitem']]->details['firstname'] . ' ' . $orders[$booking['orderitem']]->details['lastname']) . '"', '"' . $orders[$booking['orderitem']]->details['email'] . '"', '"' . $orders[$booking['orderitem']]->details['orderdate'] . '"', $orders[$booking['orderitem']]->id);
				$csv->AddToRows($row);
			}
		}
		$csv->Output($this->BookingsCSVName());
	} // end of fn BookingsCSVOuput
	
	public function BookingsCSVName()
	{	return $this->event['slug'] . '_' . $this->venue['slug'] . '_' . date('Ymd', strtotime($this->details['starttime'])) . '.csv';
	} // end of fn BookingsCSVName
	
} // end of defn AdminEventVenue
?>