<?php 
class NewsPage extends BasePage
{	private $story;

	public function __construct()
	{	parent::__construct('newsfeed');
		$this->css[] = 'news.css';
		if ($_GET['story'] && ($this->story = new NewsStory($_GET['story'])) && $this->story->id)
		{	$this->title = $this->InputSafeString($this->story->details['headline']);
			$this->page->details['metadesc'] = $this->story->MetaDesc();
			$this->canonical_link = $this->story->Link();
		}
	} // end of fn __construct
	
	function MainBodyContent()
	{	echo '<div class="container"><div class="container_inner">';
		if ($this->story && $this->story->id)
		{	echo '<div class="page_heading"><a href="', SITE_URL, 'news.php">News</a></div>';
		} else
		{	echo '<h1 class="page_heading">News</h1>';
		}
		echo '<div class="left_content left_content_news">', $this->MiddleContent(), '</div><div class="clear"></div></div></div>';
	} // end of fn MemberBody
	
	protected function MiddleContent()
	{	ob_start();
		if ($this->story && $this->story->id)
		{	echo $this->story->FullStory(), $this->story->SocialButtons();
		} else
		{	$news = new NewsStories(true);
			echo $news->NewsPageList('h2');
		}
		return ob_get_clean();
	} // end of fn MiddleContent
	
} // end of defn NewsPage
?>