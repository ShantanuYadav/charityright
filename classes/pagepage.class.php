<?php
class PagePage extends BasePage
{	private $inc_content = '';

	public function __construct($pageName = '')
	{	if (substr($pageName, -4) == '.php')
		{	$pageName = substr($pageName, 0, -4);
		}
		parent::__construct($pageName);
		$this->css['page.css'] = 'page.css';

		if(!$this->page->Found())
		{	//echo $pageName, ' not found';
			//$this->varDump($this->page->details);
			//$this->varDump($_GET);
			//echo $_SERVER['QUERY_STRING'];
			header('location: ' . SITE_SUB . '/404.php');
			exit;
		}
		if ($this->page->details['redirectlinkforce'] && $this->page->details['redirectlink'])
		{	header('location: ' . $this->page->details['redirectlink']);
			exit;
		}
		if ($this->quickdonate = $this->page->GetQuickDonateOptions())
		{	$this->js['quickdonate'] = 'quickdonate.js';
			$this->css['quickdonate'] = 'quickdonate.css';
		}
		if ($this->page->details['includefile'] && $this->page->IncludeFileExists($this->page->details['includefile']))
		{	ob_start();
			include_once($this->page->IncludeFilePath());
			$this->inc_content = ob_get_clean();
			//echo $this->inc_content;
		}
	} // end of fn __construct

	function MainBodyContent()
	{	echo $this->MainBodyContentHeader(), $this->MainBodyQuickDonateForm();
		if ($topcontent = $this->page->HTMLMainContent())
		{	echo '<div class="container pageContentContainer"><div class="container_inner">', $topcontent, '</div></div>';
		}
		echo $this->MainBodyContentPageSections(), $this->inc_content;
	} // end of fn MemberBody

} // end of defn PagePage
?>