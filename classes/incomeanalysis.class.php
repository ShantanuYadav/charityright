<?php
class IncomeAnalysis extends Base
{	public $startdate = '2016-11-30';

	public function __construct()
	{	parent::__construct();
	} // fn __construct
	
	public function DatesFromFilter(&$filter)
	{	if (is_array($filter))
		{	if (!$filter['startdate'])
			{	$filter['startdate'] = $this->startdate;
			}
			if (!$filter['enddate'])
			{	$filter['enddate'] = $this->datefn->SQLDate();
			}
		}
	} // fn DatesFromFilter
	
	public function GetDonationPayments($filter = array())
	{	$donations = array();
		$tables = array('donationpayments'=>'donationpayments', 'donations'=>'donations');
		$fields = array('donationpayments.*', 'donations.donationtype', 'donations.donationcountry');
		$where = array('donations_link'=>'donationpayments.donationref=donations.donationref');
		$orderby = array('paydate'=>'donationpayments.paydate');
		
		if ($filter['startdate'])
		{	$where['startdate'] = 'donationpayments.paydate>="' . $filter['startdate'] . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'donationpayments.paydate<="' . $filter['enddate'] . ' 23:59:59"';
		}
		//return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'dpid');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$donations[$row['dpid']] = $row;
			}
		}
		return $donations;
	} // fn GetDonationPayments
	
	public function GetCampaignPayments($filter = array())
	{	$tables = array('campaigndonations'=>' campaigndonations');
		$fields = array('campaigndonations.*');
		$where = array('paid'=>'NOT campaigndonations.donationref=""');
		$orderby = array('paydate'=>'campaigndonations.donationconfirmed');
		
		if ($filter['startdate'])
		{	$where['startdate'] = 'campaigndonations.donationconfirmed>="' . $filter['startdate'] . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'campaigndonations.donationconfirmed<="' . $filter['enddate'] . ' 23:59:59"';
		}
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cdid');
	} // fn GetCampaignPayments
	
	public function GetEventPayments($filter = array())
	{	$tables = array('bookorders'=>' bookorders');
		$fields = array('bookorders.*');
		$where = array('paid'=>'NOT bookorders.payref=""', 'paidamount'=>'bookorders.gbpamountpaid>0');
		$orderby = array('paydate'=>'bookorders.paid');
		
		if ($filter['startdate'])
		{	$where['startdate'] = 'bookorders.paid>="' . $filter['startdate'] . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'bookorders.paid<="' . $filter['enddate'] . ' 23:59:59"';
		}
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'orderid');
	} // fn GetEventPayments
	
	public function IncomePerDay($filter = array())
	{	$this->DatesFromFilter($filter);
		$days = array();
		$day = $filter['startdate'];
		while ($day <= $filter['enddate'])
		{	$daystamp = strtotime($day);
			$days[$day] = array('disp'=>date('d-m-y', $daystamp), 'stamp'=>$daystamp, 'donations_count'=>0, 'donations_amount'=>0, 'donations_adminamount'=>0, 'crstars_count'=>0, 'crstars_amount'=>0, 'events_count'=>0, 'events_amount'=>0);
			$day = $this->datefn->SQLDate(strtotime($day . ' +1 day'));
		}
		
		foreach ($this->GetEventPayments($filter) as $payment)
		{	$days[$day = substr($payment['paid'], 0, 10)]['events_count']++;
			$days[$day]['events_amount'] += $payment['gbpamountpaid'];
		}
		
		foreach ($this->GetCampaignPayments($filter) as $payment)
		{	$days[$day = substr($payment['donationconfirmed'], 0, 10)]['crstars_count']++;
			$days[$day]['crstars_amount'] += $payment['gbpamount'];
		}
		
		foreach ($this->GetDonationPayments($filter) as $payment)
		{	$days[$day = substr($payment['paydate'], 0, 10)]['donations_count']++;
			if ($paymen['donationcountry'] == 'admin')
			{	$days[$day]['donations_adminamount'] += $payment['gbpamount'];
			} else
			{	$days[$day]['donations_amount'] += $payment['gbpamount'];
				$days[$day]['donations_adminamount'] += $payment['gbpadminamount'];
			}
		}
		
		return $days;
		
	} // fn IncomePerDay
	
	public function IncomePerMonth($filter = array())
	{	
		$months = array();
		$filter['enddate'] = date('Y-m-t', mktime(0, 0, 0,$filter['mend'], 1, $filter['yend']));
		$my = $filter['startdate'] = date('Y-m-d', mktime(0, 0, 0,$filter['mstart'], 1, $filter['ystart']));
		while ($my < $filter['enddate'])
		{	$mystamp = strtotime($my);
			$months[substr($my, 0, 7)] = array('disp'=>date('M-y', $mystamp), 'stamp'=>$mystamp, 'donations_count'=>0, 'donations_amount'=>0, 'donations_adminamount'=>0, 'crstars_count'=>0, 'crstars_amount'=>0, 'events_count'=>0, 'events_amount'=>0);
			$my = $this->datefn->SQLDateTime(strtotime($my . ' +1 month'));
		}

		foreach ($this->GetEventPayments($filter) as $payment)
		{	$months[$month = substr($payment['paid'], 0, 7)]['events_count']++;
			$months[$month]['events_amount'] += $payment['gbpamountpaid'];
		}
		
		foreach ($this->GetCampaignPayments($filter) as $payment)
		{	$months[$month = substr($payment['donationconfirmed'], 0, 7)]['crstars_count']++;
			$months[$month]['crstars_amount'] += $payment['gbpamount'];
		}
		
		foreach ($this->GetDonationPayments($filter) as $payment)
		{	$months[$month = substr($payment['paydate'], 0, 7)]['donations_count']++;
			if ($payment['donationcountry'] == 'admin')
			{	$months[$month]['donations_adminamount'] += $payment['gbpamount'];
			} else
			{	$months[$month]['donations_amount'] += $payment['gbpamount'];
				$months[$month]['donations_adminamount'] += $payment['gbpadminamount'];
			}
		}
		
		return $months;
		
	} // fn IncomePerMonth
	
	public function ConversionPerMonth($filter = array())
	{	
		$months = array();
		$filter['enddate'] = date('Y-m-t', mktime(0, 0, 0,$filter['mend'], 1, $filter['yend']));
		$my = $filter['startdate'] = date('Y-m-d', mktime(0, 0, 0,$filter['mstart'], 1, $filter['ystart']));
		while ($my < $filter['enddate'])
		{	$mystamp = strtotime($my);
			$months[substr($my, 0, 7)] = array('disp'=>date('M-y', $mystamp), 'stamp'=>$mystamp, 'donations_count'=>0, 'donations_paid'=>0, 'donations_rate'=>0, 'crstars_count'=>0, 'crstars_paid'=>0, 'crstars_rate'=>0, 'alldon_count'=>0, 'alldon_paid'=>0, 'alldon_rate'=>0, 'events_count'=>0, 'events_paid'=>0, 'events_rate'=>0);
			$my = $this->datefn->SQLDateTime(strtotime($my . ' +1 month'));
		}
		
		$tables = array('dontable'=>'donations AS dontable');
		$fields = array('donmonth'=>'LEFT(dontable.created, 7) AS donmonth', 'attempted'=>'COUNT(dontable.donationref) AS attempted', 'paid'=>'SUM(IF(dontable.donationref="", 0, 1)) AS paid');
		$where = array('startdate'=>'dontable.created>="' . $filter['startdate'] . ' 00:00:00"', 'enddate'=>'dontable.created<="' . $filter['enddate'] . ' 23:59:59"');
		$groupby = array('donmonth');
		
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, false, $groupby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$months[$row['donmonth']]['donations_count'] = $row['attempted'];
				$months[$row['donmonth']]['donations_paid'] = $row['paid'];
				$months[$row['donmonth']]['alldon_count'] += $row['attempted'];
				$months[$row['donmonth']]['alldon_paid'] += $row['paid'];
				if ($months[$row['donmonth']]['alldon_count'] > 0)
				{	$months[$row['donmonth']]['alldon_rate'] = $months[$row['donmonth']]['donations_rate'] = $months[$row['donmonth']]['alldon_paid'] / $months[$row['donmonth']]['alldon_count'];
				}
			}
		}
		
		$tables['dontable'] = 'campaigndonations AS dontable';
		$fields['donmonth'] = 'LEFT(dontable.donated, 7) AS donmonth';
		$where['startdate'] = 'dontable.donated>="' . $filter['startdate'] . ' 00:00:00"';
		$where['enddate'] = 'dontable.donated<="' . $filter['enddate'] . ' 23:59:59"';
		
		
		if ($result = $this->db->Query($sql = $this->db->BuildSQL($tables, $fields, $where, false, $groupby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$months[$row['donmonth']]['crstars_count'] = $row['attempted'];
				$months[$row['donmonth']]['crstars_paid'] = $row['paid'];
				$months[$row['donmonth']]['alldon_count'] += $row['attempted'];
				$months[$row['donmonth']]['alldon_paid'] += $row['paid'];
				if ($months[$row['donmonth']]['crstars_count'] > 0)
				{	$months[$row['donmonth']]['crstars_rate'] = $months[$row['donmonth']]['crstars_paid'] / $months[$row['donmonth']]['crstars_count'];
					$months[$row['donmonth']]['alldon_rate'] = $months[$row['donmonth']]['alldon_paid'] / $months[$row['donmonth']]['alldon_count'];
				}
			}
		}
		
		return $months;
		
	} // fn IncomePerMonth
	
} // end of defn IncomeAnalysis
?>