<?php 
class SitemapBuilder extends Base
{	var $filename = 'sitemap.xml';

	public function __construct()
	{	parent::__construct();
	} // end of fn __construct
	
	protected function Filepath()
	{	return CITDOC_ROOT . '/' . $this->filename;
	} // end of fn Filepath
	
	public function FileExists()
	{	return file_exists($this->Filepath());
	} // end of fn Filepath
	
	protected function FileAge()
	{	return @filemtime($this->Filepath());
	} // end of fn FileAge
	
	public function RobotsLine()
	{	if ($f = fopen(CITDOC_ROOT . '/robots.txt', 'r'))
		{	while ($line = fgets($f))
			{	if (substr(strtolower($line), 0, 8) == 'sitemap:')
				{	fclose($f);
					return $line;
				}
			}
			fclose($f);
		}
	} // end of fn RobotsLine
	
	public function FileAgeString()
	{	if ($changed = $this->FileAge())
		{	if ($seconds = time() - $changed)
			{	ob_start();
				if ($seconds > 60)
				{	if ($seconds > $this->datefn->secInHour)
					{	if ($seconds > $this->datefn->secInDay)
						{	$days = floor($seconds / $this->datefn->secInDay);
							echo $days, ' days, ', floor(($seconds - ($days * $this->datefn->secInDay)) / $this->datefn->secInHour), ' hours';
						} else
						{	$hours = floor($seconds / $this->datefn->secInHour);
							echo $hours, ' hours, ', floor(($seconds - ($hours * $this->datefn->secInHour)) / 60), ' minutes';
						}
					} else
					{	echo floor($seconds / 60), ' minutes';
					}
				} else
				{	echo $seconds, ' seconds';
				}
				
				echo ' old';
				return ob_get_clean();
			} else
			{	return 'new';
			}
		}
	} // end of fn FileAgeString
	
	public function FileContents()
	{	$links = array();
		if ($f = @file_get_contents($this->Filepath()))
		{	$xml = new SimpleXMLElement($f);
			foreach ($xml->url as $url)
			{	//$this->VarDump($url->loc[0]);
				$links[] = (string)$url->loc[0];
			}
		}
		return $links;
	} // end of fn FileContents
	
	public function Build()
	{	$pages = array_merge($this->GetPages(), $this->GetPosts());
		ob_start();
		echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
';
		foreach ($pages as $link)
		{	echo '<url>
	<loc>', $link, '</loc>
	<changefreq>always</changefreq>
</url>
';
		}
		echo '</urlset>';
		if ($contents = ob_get_clean())
		{	if ($f = fopen($this->Filepath(), 'w'))
			{	fputs($f, $contents);
				fclose($f);
				return true;
			}
		}
	} // end of fn Build
	
	protected function GetPages()
	{	$pages = array();
		$sql = 'SELECT * FROM pages WHERE pagelive=1 and headeronly=0';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$page = new PageContent($row);
				$link = $page->Link();
				if (substr($link, 0, 1) == '/')
				{	$link = substr(SITE_URL, 0, -1) . $link;
				} else
				{	if (!strstr($link, SITE_URL))
					{	continue;
					}
				}
				$pages['page' . $page->id] = $link;
			}
		}
		return $pages;
	} // end of fn GetPages
	
	protected function GetPosts()
	{	$posts = array();
		$sql = 'SELECT * FROM posts WHERE live=1';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$post = new Post($row);
				$posts['post' . $post->id] = $post->Link();
			}
		}
		return $posts;
	} // end of fn GetPosts
	
} // end of defn SitemapBuilder
?>