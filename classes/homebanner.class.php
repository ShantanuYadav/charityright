<?php
class HomeBanner extends Base
{	var $items = array();

	function __construct($liveonly = true)
	{	parent::__construct();
		$this->Get($liveonly);
	} // end of fn __construct
	
	function Reset()
	{	$this->items = array();
	} // end of fn Reset
	
	function Get($liveonly = true)
	{	$this->Reset();
		
		$where = array();
		
		if ($liveonly)
		{	$where[] = "live=1";
		}
	
		$sql = "SELECT * FROM homebanner";
		if ($wstr = implode(" AND ", $where))
		{	$sql .= " WHERE $wstr";
		}
		$sql .= " ORDER BY hborder";
		
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->items[] = $this->AssignItem($row);
			}
		}
		
	} // end of fn Get
	
	function AssignItem($row = array())
	{	return new HomeBannerItem($row);
	} // end of fn AssignItem
	
} // end of defn HomeBanner
?>