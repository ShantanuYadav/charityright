<?php
class FrontEndMenuItem extends BlankItem
{	$items = array();
	
	function __construct($id = 0)
	{	parent::__construct($id, 'menuitems', 'miid');
	} // fn __construct
	
	function ResetExtra()
	{	$this->items = array();
	} // end of fn ResetExtra
	
	function GetExtra()
	{	$sql = 'SELECT * FROM menuitems WHERE menuid=' . (int)$this->details['menuid'] . ' AND parentid=' . (int)$this->id . ' ORDER BY menuorder, miid';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->items[$row['miid']] = $this->AssignMenuItem($row);
			}
		}
	} // end of fn GetExtra
	
	function AssignMenuItem($row = array())
	{	return new FrontEndMenuItem($row);
	} // end of fn AssignMenuItem
	
} // end of defn FrontEndMenuItem
?>