<?php
require_once(CITDOC_ROOT . '/PHPMailer/PHPMailerAutoload.php');

class CR_PHPMailer extends Base
{	private $mailer;
	
	public function __construct()
	{	parent::__construct();
		$this->mailer = new PHPMailer();
		$this->mailer->IsSMTP();
		$this->mailer->CharSet = 'UTF-8';

		//$this->mailer->Host       = 'mail.gridhost.co.uk'; // SMTP server example
		$this->mailer->Host       = 'localhost'; // SMTP server example
		$this->mailer->setFrom('info@charityright.org.uk', 'Charity Right');
		$this->mailer->addReplyTo('info@charityright.org.uk', 'Charity Right');
		$this->mailer->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
		$this->mailer->SMTPAuth   = true;                  // enable SMTP authentication
		$this->mailer->SMTPSecure = '';//ssl';
		//$this->mailer->Port       = 465;                    // set the SMTP port for the GMAIL server
		$this->mailer->Port       = 25;                    // set the SMTP port for the GMAIL server
		$this->mailer->Username   = 'info@charityright.org.uk'; // SMTP account username example
		$this->mailer->Password   = 'CRwebsq1848';        // SMTP account password example
	} //  end of fn __construct
	
	public function SendMail($to = '', $subject = '', $htmlbody = '', $plainbody = '', $attachments = array())
	{	$this->mailer->addAddress($to);
		$this->mailer->isHTML(true);

		$this->mailer->Subject = $subject;
		$this->mailer->Body    = $htmlbody;
		$this->mailer->AltBody = $plainbody;
		
		if (is_array($attachments) && count($attachments))
		{	foreach ($attachments as $attachment)
			{	if (is_array($attachment) && $attachment['text'] && $attachment['name'])
				{	$this->mailer->addStringAttachment($attachment['text'], $attachment['name']);
				}
			}
		}

		if ($this->mailer->send())
		{	$this->LogEmailSent($to);
			return true;
		} else
		{	if (SITE_TEST)
			{	echo $this->mailer->ErrorInfo;
			}
		}
	} //  end of fn SendMail
	
	private function LogEmailSent($to = '')
	{	$fields = array();
		$fields['mailsent'] = 'mailsent="' . $this->datefn->SQLDateTime() . '"';
		$fields['sentto'] = 'sentto="' . $this->SQLSafe($to) . '"';
		$fields['mailheader'] = 'mailheader="' . $this->SQLSafe($this->mailer->Subject) . '"';
		$sql = 'INSERT INTO smtplog SET ' . implode(', ', $fields);
		$this->db->Query($sql);
	} //  end of fn LogEmailSent
	
} // end of defn CR_PHPMailer
?>