<?php
class NewsStories extends Base
{	var $stories = array();

	function __construct($liveonly = true, $limit = 0)
	{	parent::__construct();
		$this->Get($liveonly, $limit);
	} // fn __construct
	
	function Get($liveonly = 0, $limit = 0)
	{	$this->stories = array();
		
		$where = array();
		
		if ($liveonly)
		{	$where[] = 'live=1';
		}
		
		$sql = 'SELECT * FROM news';
		if ($wsql = implode(' AND ', $where))
		{	$sql .= ' WHERE ' . $wsql;
		}
		$sql .= ' ORDER BY submitted DESC';
		
		if ($limit = (int)$limit)
		{	$sql .= " LIMIT $limit";
		}
		
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->stories[$row["newsid"]] = $this->AssignStory($row);
			}
		}
		
	} // end of fn Get
	
	function AssignStory($newsid)
	{	return new NewsStory($newsid);
	} // end of fn AssignStory
	
	function FrontPageList()
	{	ob_start();
		if ($list = $this->NewsPageList())
		{	echo '<div class="box"><div class="boxtop"></div><h2 class="heading">Latest Posts</h2>', $list, '</div>';
		}
		return ob_get_clean();
	} // end of fn FrontPageList
	
	public function NewsPageList($headertag = 'h3')
	{	ob_start();
		if ($this->stories)
		{	echo '<ul>';
			foreach ($this->stories as $story)
			{	echo $story->FrontPage($headertag);
			}
			echo '</ul>';
		}
		return ob_get_clean();
	} // end of fn NewsPageList
	
	function HeadLineList()
	{	if ($this->stories)
		{	echo "<h2 id='headlines'>Headlines:</h2>\n<ul>\n";
			foreach ($this->stories as $story)
			{	$story->HeadlineLink();
			}
			echo "</ul>\n";
		}
	} // end of fn HeadLineList
	
} // end if class defn NewsStories
?>