<?php
class CITMail extends Base
{	var $from = '"Charity Right" <info@charityright.org.uk/>';
	var $replyto = '"Charity Right" <info@charityright.org.uk/>';
	var $HTMLHeader = "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\n";
	var $plainHeader = "MIME-Version: 1.0\r\nContent-type: text/plain; charset=iso-8859-1\r\n";

	function __construct()
	{	parent::__construct();
		if ($emailfrom = $this->GetParameter('emailfrom'))
		{	$this->from = '"Charity Right" <' . $emailfrom . '>';
			$this->replyto = '"Charity Right" <' . $emailfrom . '>';
		}
	} // end of fn __construct
	
	function StdFooter()
	{	$footer = "";
		return $footer;
	} // end of fn StdFooter
	
	function SendMail($subject="", $body="", $to = "", $headerlist = array())
	{	$addresslist = array();

		if ($to)
		{	if (is_array($to))
			{	$addresslist = $to;
			} else
			{	$addresslist = array($to);
			}
		}
		
		$addresses = implode(", ", $addresslist);
		
		if (!$addresses) return false;

		if ($this->replyto) array_unshift($headerlist, "Reply-To: $this->replyto");
		if ($this->from) array_unshift($headerlist, "From: $this->from");
		if (count($addresslist) > 1)
		{	$headerlist[] = "Bcc: $addresses";
			$addresses = "";
		}

		$header = implode("\n", $headerlist);
		
		if (SITE_TEST)
		{	//$this->VarDump("$addresses\n\n$header\n\n$subject\n\n$body\n\n"); // diagnostic
		} else
		{	
			if (strlen($header))
			{	return mail($addresses, $subject, $body, $header);
			} else
			{	return mail($addresses, $subject, $body);
			}
		}

	} // end of fn SendMail
	
} // end of defn CITMail
?>