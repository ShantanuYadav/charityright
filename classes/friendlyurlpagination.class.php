<?php
class FriendlyURLPagination extends Pagination
{
	
	function __construct($page = 0, $total = 0, $perPage = 20, $baseLink = '')
	{	parent::__construct($page, $total, $perPage, $baseLink, array(), 'page');
	} // end of constructor
	
	function SetBaseLink($baseLink = '')
	{	$this->baseLink = $baseLink;
	} // end of fn SetBaseLink
	
	function AddToStripVars($stripVars = array()){}
	
	function AssignPaginationPage($pstart = 0, $baseLink = '', $current = false, $pagename = '', $hashlink = '')
	{	return new FriendlyURLPaginationPage($pstart, $baseLink, $current, $pagename, $hashlink);
	} // end of fn AssignPaginationPage
	
	function DisplayPreviousLink()
	{	ob_start();
		if ($this->page)
		{	echo '<a href="', $this->baseLink, $this->page, '/', $this->hashlink ? ('#' . $this->hashlink) : '', '">&laquo;&nbsp;Prev</a>&nbsp;';
		}
		return ob_get_clean();
	} // end of fn DisplayPreviousLink
	
	function DisplayNextLink()
	{	ob_start();
		if ($this->page < $this->numOfPages - 1)
		{	echo '&nbsp;<a href="', $this->baseLink, $this->page + 2, '/', $this->hashlink ? ('#' . $this->hashlink) : '', '">Next&nbsp;&raquo;</a>';
		}
		return ob_get_clean();
	} // end of fn DisplayNextLink
	
} // end of class FriendlyURLPagination

class FriendlyURLPaginationPage extends PaginationPage
{
	function __construct($pagenum = 0, $baseLink = '', $current = false, $pagename = '', $hashlink = '')
	{	parent::__construct($pagenum, $baseLink, $current, $pagename, $hashlink);
	} // end of constructor
	
	function SetBaseLink($baseLink = '')
	{	if (!$this->current)
		{	ob_start();
			echo $baseLink, $this->pagenum + 1, '/', $this->hashlink ? ('#' . $this->hashlink) : '';
			$this->baseLink = ob_get_clean();
		}
	} // end of fn SetBaseLink
	
} // end of class  FriendlyURLPaginationPage

?>