<?php
class Campaign extends BlankItem
{	public $team_members = array();
	public $team = array();
	public $owner = array();
	private $donations = false;
	protected $imagesizes = array('thumb'=>array('w'=>100, 'h'=>70), 'small'=>array('w'=>201, 'h'=>140), 'medium'=>array('w'=>400, 'h'=>140), 'large'=>array('w'=>1000, 'h'=>400), 'og'=>array('w'=>600, 'h'=>315));
	protected $imagelocation = '';
	protected $imagedir = '';

	public function __construct($id = 0, $slug = '')
	{	parent::__construct($id, 'campaigns', 'cid');
		if (!$this->id && $slug)
		{	$sql = 'SELECT * FROM campaigns WHERE slug="' . $this->SQLSafe($slug) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
		$this->imagelocation = SITE_URL . 'img/campaigns/';
		$this->imagedir = CITDOC_ROOT . '/img/campaigns/';
	} // fn __construct

	public function ResetExtra()
	{	$this->team_members = array();
		$this->team = array();
		$this->owner = array();
	} // fn ResetExtra

	public function GetExtra()
	{	$tables = array('campaignmembers'=>'campaignmembers', 'campaigns'=>'campaigns');
		$fields = array('campaigns.*', 'campaignmembers.joinedteam');
		$where = array('campaigns_link'=>'campaignmembers.memberid=campaigns.cid', 'teamid'=>'campaignmembers.teamid=' . $this->id);
		$orderby = array('campaignmembers.joinedteam ASC');
		$this->team_members = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cid', true);
		if (!$this->details['isteam'])
		{	$where['campaigns_link'] = 'campaignmembers.teamid=campaigns.cid';
			$where['teamid'] = 'campaignmembers.memberid=' . $this->id;
			if ($result = $this->db->Query($sql = $this->db->BuildSQL($tables, $fields, $where)))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->team = $row;
				}
			}
		}
		if ($cuid = (int)$this->details['cuid'])
		{	$sql = 'SELECT campaignusers.* FROM campaignusers WHERE cuid=' . $cuid;
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->owner = $row;
				}
			}
		}
	} // fn GetExtra

	public function CanDelete()
	{	return $this->id && !$this->team_members && !$this->GetDonations();
	} // fn CanDelete

	public function DeleteExtra()
	{	$linksql = 'DELETE FROM campaignmembers WHERE memberid=' . $this->id;
		$this->db->Query($linksql);
		
		$followsql = 'DELETE FROM campaign_follows WHERE cid=' . $this->id;
		$this->db->Query($followsql);
		
		foreach ($this->imagesizes as $size_name=>$size)
		{	@unlink($this->GetImageFile($size_name));
		}
		
		$if = new InfusionSoftCampaignOwner();
		$if->DeleteCampaign($this);
		
	} // fn DeleteExtra	

	public function GetDonations($force_new = false, $notrefunded = false)
	{	if (!is_array($this->donations) || $force_new)
		{	$this->donations = array();
			$tables = array('campaigndonations'=>'campaigndonations');
			$fields = array('campaigndonations.*');
			$where = array('cid'=>'(campaigndonations.campaignid=' . $this->id . ' OR campaigndonations.teamid=' . $this->id . ')', 'donationref'=>'NOT donationref=""');
			$orderby = array('campaigndonations.donated DESC');
			$sql = $this->db->BuildSQL($tables ,$fields ,$where ,$orderby);
			// echo $sql;
			if ($result = $this->db->Query($sql))
			{	while ($row = $this->db->FetchArray($result))
				{	if (!$notrefunded)
					{	$refsql = 'SELECT * FROM donationrefunds WHERE dontable="campaigndonations" AND donid=' . $row['cdid'];
						if ($refresult = $this->db->Query($refsql))
						{	if ($refrow = $this->db->FetchArray($refresult))
							{	continue;
							}
						}
					}
					$this->donations[$row['cdid']] = $row;
				}
			}
		}
		return $this->donations;
	} // fn GetDonations

	public function GetDonationTotal()
	{	$total = 0;
		foreach ($this->GetDonations() as $donation_row)
		{	if ($donation_row['teamid'] == $this->id)
			{	$total += $donation_row['teamamount'];
			} else
			{	if ($donation_row['campaignid'] == $this->id)
				{	$total += $donation_row['campaignamount'];
				}
			}
		}
		return $total;
	} // fn GetDonationTotal

	public function GetDonationTotalsAll()
	{	$totals = array('raised'=>0, 'raised_gbp'=>0, 'giftaid'=>0, 'giftaid_gbp'=>0, 'manual'=>0, 'manual_gbp'=>0);
		foreach ($this->GetDonations() as $donation_row)
		{	if ($donation_row['teamid'] == $this->id)
			{	$totals['raised'] += $donation_row['teamamount'];
				$totals['raised_gbp'] += $donation_row['gbpamount'];
				if ($donation_row['giftaid'])
				{	$totals['giftaid'] += $donation_row['teamamount'];
					$totals['giftaid_gbp'] += $donation_row['gbpamount'];
				}
				if ($donation_row['gateway'] == 'manual')
				{	$totals['manual'] += $donation_row['teamamount'];
					$totals['manual_gbp'] += $donation_row['gbpamount'];
				}
			} else
			{	if ($donation_row['campaignid'] == $this->id)
				{	$totals['raised'] += $donation_row['campaignamount'];
					$totals['raised_gbp'] += $donation_row['gbpamount'];
					if ($donation_row['giftaid'])
					{	$totals['giftaid'] += $donation_row['campaignamount'];
						$totals['giftaid_gbp'] += $donation_row['gbpamount'];
					}
					if ($donation_row['gateway'] == 'manual')
					{	$totals['manual'] += $donation_row['campaignamount'];
						$totals['manual_gbp'] += $donation_row['gbpamount'];
					}
				}
			}
		}
		return $totals;
	} // fn GetDonationTotalsAll

	public function DescriptionHTML()
	{	ob_start();
		if ($desc = $this->FrontEndDescription())
		{	if (substr($desc, 0, 1) == '<')
			{	echo stripslashes($desc);
			} else
			{	foreach (explode("\n", $this->InputSafeString($desc)) as $desc_p)
				{	echo '<p>', $desc_p, '</p>';
				}
			}
		}
		return ob_get_clean();
	} // fn DescriptionHTML

	public function CanChangeType()
	{	//return (count($this->GetDonations()) == 0) && !$this->team && !$this->team_members;
		return !$this->team && !$this->team_members;
	} // fn CanChangeType

	public function AddToTeam($teamid = 0)
	{
		if ($this->CanJoinATeam() && ($team = new Campaign($teamid)) && $team->CanBeJoined($this->details['cuid']))
		{	$sql = 'INSERT INTO campaignmembers SET teamid=' . $team->id . ', memberid=' . $this->id . ', joinedteam="' . $this->datefn->SQLDateTime() . '"';
			if ($result = $this->db->Query($sql))
			{	return $this->db->AffectedRows();
			}
		}

	} // fn AddToTeam

	public function RemoveFromTeam()
	{
		$sql = 'DELETE FROM campaignmembers WHERE memberid=' . $this->id;
		if ($result = $this->db->Query($sql))
		{	return $this->db->AffectedRows();
		}

	} // fn RemoveFromTeam

	public function CanJoinATeam($userid = 0)
	{	if ($this->id && !$this->details['isteam'] && !$this->team)
		{	return !$userid || ($userid != $this->details['cuid']);
		}
		return false;
	} // fn CanJoinATeam

	public function CanBeJoined($userid = 0)
	{	if ($this->id && $this->details['isteam'] && $this->details['visible'] && $this->details['enabled'])
		{	if ($userid)
			{	if ($userid == $this->details['cuid'])
				{	return false;
				} else
				{	// check if member already joined in another campaign
					if ($this->team_members)
					{	foreach ($this->team_members as $team_member_row)
						{	if ($team_member_row['cuid'] == $userid)
							{	return false;
							}
						}
						return true;
					} else
					{	return true;
					}
				}
			} else
			{	return true;
			}
		}
		return false;
	} // fn CanBeJoined

	public function FullTitle()
	{	ob_start();
		echo '<span>', $this->owner ? $this->InputSafeString($this->owner['firstname']) :  'Charity Right', '\'s</span> ', $this->InputSafeString($this->details['campname']);
		return ob_get_clean();
	} // fn FullTitle

	public function TextSnippet($characters = 300)
	{	if ($desc = $this->FrontEndDescription())
		{	$lines = explode("\n", wordwrap(str_replace("\n", ' ', strip_tags($desc)), $characters));
			ob_start();
			echo $lines[0];
			if ($lines[1])
			{	echo ' ...';
			}
			return ob_get_clean();
		}
	} // fn TextSnippet

	public function FrontEndDescription()
	{	if (!$desc = $this->details['description'])
		{	if ($this->team)
			{	$desc = $this->team['description'];
			}
		}
		return stripslashes($desc);
	} // fn FrontEndDescription

	public function AddTeamContent()
	{	ob_start();
		if ($this->team)
		{	$team = new Campaign($this->team);
			echo '<h3>Raising money for <a href="', $team->Link(), '">', $team->FullTitle(), '</a></h3>';
		} else
		{	echo '<a onclick="CampaignAddTeamOpen(', $this->id, ');">Select a team to follow</a>';
		}
		return ob_get_clean();
	} // fn AddTeamContent

	public function Create($data = array(), $imagefile = array(), $cuid = 0, $team = false)
	{
		$fail = array();
		$success = array();
		$camp_fields = array('created'=>'created="' . $this->datefn->SQLDateTime() . '"');

		if ($campname = $this->SQLSafe($data['campname']))
		{	$camp_fields['campname'] = 'campname="' . $campname . '"';
			$camp_fields['slug'] = 'slug="' . $this->TextToSlug($data['campname']) . '"';
		} else
		{	$fail[] = 'Campaign name missing';
		}

		if ($description = $this->SQLSafe($data['description']))
		{	$camp_fields['description'] = 'description="' . $description . '"';
		} else
		{	$fail[] = 'You must provide a description';
		}

		if (($target = (int)$data['camptarget']) > 0)
		{	$camp_fields['target'] = 'target=' . $target;
		} else
		{	$fail[] = 'Campaign target missing';
		}

		if ($this->GetCurrency($data['campcurrency']))
		{	$camp_fields['currency'] = 'currency="' . $data['campcurrency'] . '"';
		} else
		{	$fail[] = 'Currency missing';
		}

		if ($cuid)
		{	if (($campaign_user = new CampaignUser($cuid)) && $campaign_user->id)
			{	$camp_fields['cuid'] = 'cuid=' . $campaign_user->id;
				$camp_fields['campusername'] = 'campusername="' . $this->SQLSafe($campaign_user->details['firstname'] . ' ' . $campaign_user->details['lastname']) . '"';
			} else
			{	$fail[] = 'User not found';
			}
		} else
		{	$camp_fields['campusername'] = 'campusername="Charity Right"';
		}

		if (is_a($team, 'Campaign') && $team->CanBeJoined($cuid))
		{	$team_to_join = $team->id;
		} else
		{	$camp_fields['isteam'] = 'isteam=' . ($data['isteam'] ? '1' : '0');
		}
		$camp_fields['visible'] = 'visible=' . ($data['visible'] ? '1' : '0');
		$camp_fields['enabled'] = 'enabled=' . ($data['enabled'] ? '1' : '0');
		$camp_fields['helpfromcr'] = 'helpfromcr=' . ($data['helpfromcr'] ? '1' : '0');

		if (!$team_to_join)
		{	if ($donationcountry = $this->SQLSafe($data['donation_country']))
			{	$camp_fields['donationcountry'] = 'donationcountry="' . $donationcountry . '"';
			} else
			{	$fail[] = 'donation country missing';
			}
			if ($donationproject = $this->SQLSafe($data['donation_project']))
			{	$camp_fields['donationproject'] = 'donationproject="' . $donationproject . '"';
			} else
			{	$fail[] = 'donation project missing';
			}
		}

		if ($imagefile['size'])
		{	if ($this->ValidPhotoUpload($imagefile))
			{	$image_to_upload = true;
			} else
			{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
			}
		} else
		{	if ($avatarid = (int)$data['avatarid'])
			{	$camp_fields['avatarid'] = 'avatarid=' . $avatarid;
			} else
			{	$fail[] = 'You must choose an image';
			}
		}

		if (!$fail && ($set = implode(', ', $camp_fields)))
		{	$sql = 'INSERT INTO campaigns SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
				{	if ($team_to_join)
					{	$teamsql = 'INSERT INTO campaignmembers SET teamid=' . $team_to_join . ', memberid=' . $id . ', joinedteam="' . $this->datefn->SQLDateTime() . '"';
						$this->db->Query($teamsql);
					}
					$this->Get($id);
					$success[] = 'New campaign created';
					$if = new InfusionSoftCampaignOwner();
					$if->AddCampaign($this);
					$this->SendCampaignCreateEmailToAdmin();
				}
			}// else $fail[] = $sql . ': ' . $this->db->Error();

			if ($this->id)
			{	if ($imagefile['size'])
				{	if ($image_to_upload)
					{	$photos_created = 0;
						foreach ($this->imagesizes as $size_name=>$size)
						{	if (!file_exists($this->ImageFileDirectory($size_name)))
							{	mkdir($this->ImageFileDirectory($size_name));
							}
							if ($this->ReSizePhotoPNG($imagefile['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($imagefile['type'], 'png') ? 'png' : 'jpg'))
							{	$photos_created++;
							}
						}
						unset($imagefile['tmp_name']);
						if ($photos_created)
						{	$success[] = 'Your image has been uploaded';
						}
					}
				}
			}
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn Create

	public function SendCampaignCreateEmailToAdmin()
	{	
		if ($this->id && ($email = $this->GetParameter('email_crstars')))
		{	$mail = new HTMLMail();
			$mail->SetSubject('New CR Stars campaign created');
			ob_start();
			echo '<p>New CR Stars campaign has been created</p>
<p><a href="', SITE_URL, ADMIN_URL, '/campaign.php?id=', $this->id, '">', $this->FullTitle(), '</a></p>';
			if ($this->details['helpfromcr'])
			{	echo '
<p>The owner has requested that Charity Right help with their fundraising.</p>';
			}
			$mail->Send($email, $htmlcontent = ob_get_clean(), strip_tags($htmlcontent));
		//	$mail->Send('tim@websquare.co.uk', $htmlcontent, strip_tags($htmlcontent));
		}
		
	} // fn SendCampaignCreateEmailToAdmin

	public function SendCampaignCRHelpEmailToAdmin()
	{	
		if ($this->id && ($email = $this->GetParameter('email_crstars')))
		{	$mail = new HTMLMail();
			$mail->SetSubject('CR Stars campaign request for help');
			ob_start();
			echo '<p>The owner of this campaign has requested that Charity Right help with their fundraising.</p>
<p><a href="', SITE_URL, ADMIN_URL, '/campaign.php?id=', $this->id, '">', $this->FullTitle(), '</a></p>';
			$mail->Send($email, $htmlcontent = ob_get_clean(), strip_tags($htmlcontent));
		//	$mail->Send('tim@websquare.co.uk', $htmlcontent, strip_tags($htmlcontent));
		}
		
	} // fn SendCampaignCRHelpEmailToAdmin

	public function SendCampaignCreateEmail()
	{	
		if ($this->id)
		{	ob_start();
			echo '<p>Dear ', $this->InputSafeString($this->owner['firstname']), ',</p>
<p>I hope this reaches you in the best of health and faith.</p>
<p>Thank you for setting up a fundraising page on our platform, CR Stars.</p>
<p>Have a look at your page here: <a href="', $link = $this->Link(), '">', $link, '</a></p>
<p>I\'m here to help with any fundraising needs you may have, so please don\'t hesitate to reply to this email.<p>
<p>JazakAllah Khayr and I hope to hear from you soon.</p>';
/*
<p>Thank you for setting up your new campaign "', $this->InputSafeString($this->details['campname']), '" to raise money for Charity Right</p>
<p>Let your friends know by sharing your campaign on Facebook and Twitter</p>';*/
			$mail = new HTMLMail();
			$mail->SetSubject('Your new Charity Right CR Stars campaign');
			$mail->SendFromTemplate($this->owner['email'], array('main_content'=>ob_get_clean()), 'default');
		}
		
	} // fn SendCampaignCreateEmail

	protected function CanEditHTML()
	{	if ($this->details['cuid'] == 0)
		{	return true;
		}
		return false;
	} // fn CanEditHTML

	public function Save($data = array(), $imagefile = array())
	{
		$fail = array();
		$success = array();
		$camp_fields = array();

		if ($campname = $this->SQLSafe($data['campname']))
		{	$camp_fields['campname'] = 'campname="' . $campname . '"';
			$camp_fields['slug'] = 'slug="' . $this->TextToSlug($data['campname']) . '"';
		} else
		{	$fail[] = 'Campaign name missing';
		}

		if ($data['adminedited'])
		{	$camp_fields['htmldesc'] = 'htmldesc=' . ($data['htmldesc'] ? '1' : '0');
		} else
		{	$data['htmldesc'] = $this->details['htmldesc'];
		}
		if (!$data['htmldesc'])
		{	$data['description'] = strip_tags($data['description']);
		}
		$camp_fields['description'] = 'description="' . $this->SQLSafe($data['description']) . '"';

		if (!$this->team)
		{	if ($donationcountry = $this->SQLSafe($data['donation_country']))
			{	$camp_fields['donationcountry'] = 'donationcountry="' . $donationcountry . '"';
			} else
			{	$fail[] = 'donation country missing';
			}
			if ($donationproject = $this->SQLSafe($data['donation_project']))
			{	$camp_fields['donationproject'] = 'donationproject="' . $donationproject . '"';
			} else
			{	$fail[] = 'donation project missing';
			}
		}

		if ($this->CanChangeCurrency())
		{	if (($currency = $this->GetCurrency($data['currency'])) || ($currency = $this->GetCurrency($data['campcurrency'])))
			{	$camp_fields['currency'] = 'currency="' . $currency['curcode'] . '"';
			} else
			{	$fail[] = 'Currency missing';
			}
		}

		if ($this->CanChangeType())
		{	if($data['isteam'])
			{	$camp_fields['isteam'] = 'isteam=1';
			} else
			{   $camp_fields['isteam'] = 'isteam=0';
			}
		}

		if ($data['type_change'] && $this->CanChangeType())
		{	$camp_fields['isteam'] = 'isteam=' . (1 - $this->details['isteam']);
		}

		if ($target = (int)$data['target'])
		{	$camp_fields['target'] = 'target=' . $target;
		} else
		{	$fail[] = 'Campaign target missing';
		}

		if ($data['enabled'])
		{	if ($data['visible'])
			{	$camp_fields['enabled'] = 'enabled=1';
				$camp_fields['visible'] = 'visible=1';
			} else
			{	$fail[] = 'To take donations your campaign must be visible';
			}
		} else
		{	$camp_fields['enabled'] = 'enabled=0';
			$camp_fields['visible'] = 'visible=' . ($data['visible'] ? '1' : '0');
		}
		
		if (isset($data['teamoftheday']))
		{	$camp_fields['teamoftheday'] = 'teamoftheday=' . (int)$data['teamoftheday'];
		}
		
		if (isset($data['listpriority']))
		{	$camp_fields['listpriority'] = 'listpriority=' . (int)$data['listpriority'];
		}

		if ($data['type_change'])
		{	$camp_fields['isteam'] = 'isteam=' . (1 - $this->details['isteam']);
		}
		$camp_fields['helpfromcr'] = 'helpfromcr=' . ($data['helpfromcr'] ? '1' : '0');
		if (!$this->details['helpfromcr'] && $data['helpfromcr'])
		{	$helpfromcr_added = true;
		}

		if (isset($data['campaign_picture_choosen']))
		{	if ($data['campaign_picture_choosen'] == 'campaign_picture_choosen_1'){
				if ($imagefile['size'])
				{	if ($this->ValidPhotoUpload($imagefile))
					{	$image_to_upload = true;
						$camp_fields['avatarid'] = 'avatarid=0';
					} else
					{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
					}
				}
			}else{
				if ($avatarid = (int)$data['avatarid'])
				{	$camp_fields['avatarid'] = 'avatarid=' . $avatarid;
				} else
				{	$fail[] = 'You must choose an image';
				}
			}
		} else
		{
			if ($imagefile['size'])
			{	if ($this->ValidPhotoUpload($imagefile))
				{	$image_to_upload = true;
				} else
				{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
				}
			} else
			{	if (!$this->HasImage())
				{	if ($avatarid = (int)$data['avatarid'])
					{	$camp_fields['avatarid'] = 'avatarid=' . $avatarid;
					} else
					{	$fail[] = 'You must choose an image';
					}
				}
			}
		}
		
		if ($set = implode(', ', $camp_fields))
		{	$sql = 'UPDATE campaigns SET ' . $set . ' WHERE cid=' . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->Get($this->id);
					$success[] = 'Your changes have been saved';
					$if = new InfusionSoftCampaignOwner();
					$if->UpdateCampaign($this);
					if ($helpfromcr_added)
					{	$this->SendCampaignCRHelpEmailToAdmin();
					}
				}
			} //else $fail[] = $sql . ': ' . $this->db->Error();
		}

		if ($image_to_upload && $imagefile['size'])
		{	if ($this->ValidPhotoUpload($imagefile))
			{	$photos_created = 0;
				foreach ($this->imagesizes as $size_name=>$size)
				{	if (!file_exists($this->ImageFileDirectory($size_name)))
					{	mkdir($this->ImageFileDirectory($size_name));
					}
					if ($this->ReSizePhotoPNG($imagefile['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($imagefile['type'], 'png') ? 'png' : 'jpg'))
					{	$photos_created++;
					}
				}
				unset($imagefile['tmp_name']);
				if ($photos_created)
				{	$success[] = 'Your image has been uploaded';
				}
			} else
			{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
			}
		}
		/* Image delete if avatar is choosen */
		if(($data['campaign_picture_choosen'] == 'campaign_picture_choosen_2') && $data['avatarid']){
			if ($this->HasImage()) {
				foreach ($this->imagesizes as $size_name=>$size)
				{	if (@unlink($this->GetImageFile($size_name)))
					{	$img_deleted++;
					}
				}
			}
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn Save

	public function CanChangeCurrency()
	{	return $this->id && !$this->GetDonations();
	} // fn CanChangeCurrency

	public function Link()
	{	if ($this->id) return SITE_URL . 'cr-star-campaign/' . $this->id . '/' . $this->details['slug'] . '/';
	} // fn Link

	public function ThankYouLink()
	{	if ($this->id) return $this->Link() . 'thankyou/';
	} // fn ThankYouLink

	public function HasImage($size = 'small')
	{	if ($this->id && file_exists($this->GetImageFile($size)))
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions

	public function ImageDimensions($size = 'small')
	{	if ($this->imagesizes[$size])
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions

	public function GetImageFile($size = 'small')
	{	return $this->ImageFileDirectory($size) . '/' . $this->id .'.png';
	} // end of fn GetImageFile

	public function ImageFileDirectory($size = 'small')
	{	return $this->imagedir . $this->InputSafeString($size);
	} // end of fn FunctionName

	public function GetImageSRC($size = 'small')
	{	if ($this->HasImage($size))
		{	return $this->imagelocation . $this->InputSafeString($size) . '/' . $this->id . '.png';
		}
	} // end of fn GetImageSRC

	public function GetAvatarSRC($size = 'small')
	{	if ($this->details['avatarid'] && ($avatar = new CampaignAvatar($this->details['avatarid'])) && $avatar->id)
		{	return $avatar->GetImageSRC($size);
		}
	} // end of fn GetAvatarSRC

	public function ImageHTML($size = 'small')
	{	if ($src = $this->GetImageSRC($size))
		{	ob_start();
			echo '<img src="', $src, '" alt="', $alt = $this->InputSafeString($this->details['imagedesc']), '" title="', $alt, '" />';
			return ob_get_clean();
		}
	} // end of fn ImageHTML

	public function DonateLink()
	{	if ($this->id) return $this->Link() . 'donate/';
	} // fn DonateLink

	public function LoginLink()
	{	if ($this->id) return $this->Link() . 'login/';
	} // fn LoginLink

	public function RegisterLink()
	{	if ($this->id) return $this->Link() . 'register/';
	} // fn RegisterLink

	public function JoinLink()
	{	if ($this->id) return $this->Link() . 'join/';
	} // fn JoinLink

	public function EditLink()
	{	if ($this->id) return SITE_URL . 'crstars_campaignedit.php?cid=' . $this->id;
	} // fn EditLink

	public function SetFBMeta(&$fb_meta)
	{	if ($this->id)
		{	$fb_meta['title'] = 'Support ' . $this->InputSafeString(strip_tags($this->FullTitle()));
			if (($src = $this->GetImageSRC('og')) || ($src = $this->GetAvatarSRC('og')))
			{	$fb_meta['image'] = $src;
			}
			$fb_meta['url'] = $this->Link();
			$fb_meta['description'] = $this->TextSnippet();
		}
	} // end of fn SetFBMeta

	public function RunningForDays($upto = '')
	{	if ($upto)
		{	if ((int)$upto !== $upto)
			{	$upto = strtotime($upto);
			}
		} else
		{	$upto = time();
		}
		if (($days =  floor(($upto - strtotime($this->details['created'])) / $this->datefn->secInDay)) > 0)
		{	return $days;
		}
		return 0;
	} // fn RunningForDays

	public function RunningForText($upto = '')
	{	return $this->RunningForDays($upto) . ' days';
	} // fn FullTitle

	public function SendTargetReachedEMail()
	{	if ($this->ValidEMail($this->owner['email']))
		{	if ($this->details['currency'] == 'GBP')
			{	$target_gbp = $this->details['target'];
			} else
			{	$target_gbp = $this->details['target'] * $this->GetCurrency($this->details['currency'], 'convertrate');
			}
			$mail = new HTMLMail();
			ob_start();
			echo '<p>Hello ', $this->InputSafeString($this->owner['firstname']), '</p><p>Congratulations on raising ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['target'], 2), '!</p><p>We would like to thank you for your support. The money you have raised will feed ';
			if ($target_gbp > 10000)
			{	echo 'a thousand children for a whole month';
			} else 
			{	if ($target_gbp > 5000)
				{	echo '500 children for a whole month';
				} else 
				{	echo 'dozens of children';
				}
			}
			echo '.</p><p>A massive thank you from the whole Charity Right team.</p>';
			$body = ob_get_clean();
			$mail->SetSubject('Congratulations on Reaching your CR Stars Target');
			$mail->SendFromTemplate($this->owner['email'], array('main_content'=>$body), 'default');
			$this->db->Query('UPDATE campaigns SET targetemailsent=1 WHERE cid=' . $this->id);
		}
	} // end of fn SendTargetReachedEMail

} // end of defn Campaign
?>