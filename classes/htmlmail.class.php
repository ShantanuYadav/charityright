<?php
class HTMLMail extends Base
{	var $headers = array();
	var $boundary = 'PHP-alt-';
	var $from = 'info@charityright.org.uk';
	var $subject = 'From Charity Right';
	
	function __construct()
	{	parent::__construct();
		$this->boundary .= md5(date('r'));
		$this->SetHeaders();
	} // end of fn __construct
	
	function SetFrom($from = '')
	{	$this->from = $from;
		$this->SetHeaders();
	} // end of fn SetFrom
	
	function SetHeaders()
	{	$this->headers = array('From: "' . $this->GetParameter('shorttitle') . '" <' . $this->from . '>', 'Reply-To: "' . $this->GetParameter('shorttitle') . '" <' . $this->from . '>', 'Return-Path: "' . $this->GetParameter('shorttitle') . '" <' . $this->from . '>', 'X-Mailer: ' . phpversion(), 'MIME-Version: 1.0', 'Content-Type: multipart/alternative; boundary=' . $this->boundary);
	} // end of fn SetHeaders
	
	function SetSubject($subject = '')
	{	$this->subject = $subject;
	} // end of fn SetSubject
	
	function SendSMTP($to = '', $htmlbody = '', $plainbody = '', $css = '', $attachments = array())
	{	if (SITE_TEST)
		{	$this->LogMailBody($htmlbody, $to);
		} else
		{	if (SITE_TEST)
			{	$to = 'tim@websquare.co.uk';
			}
			$mailer = new CR_PHPMailer();
			return $mailer->SendMail($to, $this->subject, $htmlbody, $plainbody, $attachments);
			
		}
		return false;
	} // end of fn SendSMTP
	
	function Send($to = '', $htmlbody = '', $plainbody = '', $css = '', $attachments = array())
	{	
		ob_start();
		echo "--", $this->boundary, "\nContent-Type: text/plain; charset=\"iso-8859-1\"\nContent-Transfer-Encoding: 7bit\n\n",$plainbody, "\n\n--", $this->boundary;
		if (is_array($attachments) && count($attachments))
		{	$html_boundary = 'PHP-alt-' . md5(date('r') . 'html');
			echo "\nContent-Type: multipart/related;\n boundary=", $html_boundary, "\n\n--", $html_boundary;
		}
		echo "\nContent-Type: text/html; charset=\"iso-8859-1\"\nContent-Transfer-Encoding: 7bit\n\n<html>\n";
		if ($css)
		{	if (is_array($css))
			{	foreach ($css as $cssfile)
				{	$css_path = CITDOC_ROOT . CSS_ROOT . $cssfile;
					if (file_exists($css_path))
					{	if (!$headdone++)
						{	echo "<head>\n<style type=\"text/css\">\n";
						}
						include($css_path);
					}
				}
				if ($headdone)
				{	echo "\n</style>\n</head>\n";
				}
			} else
			{	$css_path = CITDOC_ROOT . CSS_ROOT . $css;
				if (file_exists($css_path))
				{	echo "<head>\n<style type=\"text/css\">\n";
					include_once($css_path);
					echo "\n</style>\n</head>\n";
				}
			}
		}
		echo $htmlbody, "</html>\r\n\r\n";
		if (is_array($attachments) && count($attachments))
		{	foreach ($attachments as $attachment)
			{	echo "\r\n--", $html_boundary, "\r\n", $attachment;
			}
			echo "\r\n--", $html_boundary, "--";
		}
		echo "\r\n\r\n--{$this->boundary}--\r\n";
		$body = ob_get_clean();
		$headers = implode("\n", $this->headers);
		if (SITE_TEST)
		{	$this->LogMailBody($body, $to);
		} else
		{	mail($to, $this->subject, $body, $headers);
		}
		return true;
	} // end of fn Send
	
	public function LogMailBody($body = '', $address = '')
	{	$addr_filename = substr(str_replace(array('.', ' '), '_', $address), 0, 30);
		$filepath = CITDOC_ROOT . '/mail_log/' . $addr_filename . '_' . time() . '.html';
		if ($file = fopen($filepath, 'w'))
		{	fputs($file, '<h2>' . $this->subject . '</h2>' . "\n");
			fputs($file, $body);
			fclose($file);
		}
	} // end of fn LogMailBody
	
	public function SendFromTemplate($to = '', $content = array(), $template = 'default', $attachments = array())
	{	$template_dir = CITDOC_ROOT . '/mailtemplates/';
		if (is_array($content) && ($htmlbody = @file_get_contents($template_dir . $template . '.html')))
		{	// first add standard includes
			if (preg_match_all('|\{std_[^}\s]+\}|', $htmlbody, $std_matches))
			{	foreach (array_unique($std_matches[0]) as $std_template)
				{	if ($std_html = @file_get_contents($template_dir . str_replace(array('{', '}'), '', $std_template) . '.html'))
					{	$htmlbody = str_replace($std_template, $std_html, $htmlbody);
					}
				}
			}
			foreach ($content as $fieldname=>$fieldcontent)
			{	$htmlbody = str_replace('{' . $fieldname . '}', $fieldcontent, $htmlbody);
			}
			$plainbody = strip_tags($htmlbody);
			return $this->Send($to, $htmlbody, $plainbody, '', $attachments);
		}
	} // end of fn SendFromTemplate
	
} // end of defn HTMLMail
?>