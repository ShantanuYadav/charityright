<?php
class AdminEventVenue extends EventVenue
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct

	public function SlugExists($slug = '')
	{	$sql = 'SELECT evid FROM eventvenues WHERE slug="' . $this->SQLSafe($slug) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT evid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	public function Save($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($venuename = $this->SQLSafe($data['venuename']))
		{	$fields[] = 'venuename="' . $venuename . '"';
		} else
		{	$fail[] = 'venue name cannot be empty';
		}
		
		if ($data['country'])
		{	if ($this->GetCountry($data['country']))
			{	$fields[] = 'country="' . $data['country'] . '"';
			} else
			{	$fail[] = 'country not found';
			}
		} else
		{	$fail[] = 'country is missing';
		}
	
		$fields[] = 'metadesc="' . $this->SQLSafe($data['metadesc']) . '"';
		$fields[] = 'directions="' . $this->SQLSafe($data['directions']) . '"';
		$fields[] = 'address="' . $this->SQLSafe($data['address']) . '"';
		
		if ($this->id)
		{	$slug = $this->TextToSlug($data['slug']);
		} else
		{	if ($venuename)
			{	$slug = $this->TextToSlug($venuename);
			}
		}
		
		if ($slug)
		{	$suffix = '';
			while ($this->SlugExists($slug . $suffix))
			{	$suffix++;
			}
			$slug .= $suffix;
			
			$fields[] = 'slug="' . $slug . '"';
		} else
		{	if ($this->id || $venuename)
			{	$fail[] = 'slug missing';
			}
		}
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE eventvenues SET ' . $set . ' WHERE evid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO eventvenues SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new venue added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function CanDelete()
	{	return $this->id && !$this->GetDates();
	} // end of fn CanDelete
	
	public function InputForm()
	{	ob_start();
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddTextInput('Venue name', 'venuename', $this->InputSafeString($data['venuename']), 'long');
		if ($this->id)
		{	$form->AddTextInput('Slug', 'slug', $this->InputSafeString($data['slug']), 'long', 255, 1);
		}
		$form->AddTextArea('Address', 'address', $this->InputSafeString($data['address']), '', 0, 0, 5, 40);
		$form->AddSelectWithGroups('Country', 'country', $data['country'], '', $this->CountryList(), 1, 1, '');
		$form->AddTextArea('Directions', 'directions', $this->InputSafeString($data['directions']), '', 0, 0, 5, 40);
		$form->AddTextArea('Meta description', 'metadesc', $this->InputSafeString($data['metadesc']), '', 0, 0, 10, 40);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Venue', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this venue</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
} // end of defn AdminEventVenue
?>