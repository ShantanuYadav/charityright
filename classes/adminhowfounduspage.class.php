<?php
class AdminHowFoundUsPage extends CMSPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function CMSLoggedInConstruct()
	{	$this->HowFoundUsLoggedInConstructor();
	} // end of fn CMSLoggedInConstruct
	
	public function HowFoundUsLoggedInConstructor()
	{	$this->breadcrumbs->AddCrumb('howfoundusoptions.php', '"How you found us" options');
	} // end of fn HowFoundUsLoggedInConstructor
	
	function CMSBodyMain()
	{	$this->HowFoundUsLoggedInBody();
	} // end of fn CMSBodyMain
	
	public function HowFoundUsLoggedInBody()
	{	
	} // end of fn HowFoundUsLoggedInBody
	
} // end of defn AdminHowFoundUsPage
?>