<?php
class EazyCollectAPI extends Base
{	protected $clientCode = '';
	protected $clientCode_live = 'CRGHT';
	protected $clientCode_test = 'APITBA';
	protected $apiSecret = '';
	protected $apiSecret_live = '0PKJvepGPKA6w5nLrvGeKngqFQ';
	protected $apiSecret_test = '3FoE8J8nMoBQSECsYf4dEXHUxc0';
	protected $scheduleName = 'DD Dates 1/15 - Rolling';
	var $ez_url = '';
	var $ez_url_live = 'https://ecm3.eazycollect.co.uk/api/2.0/client/';
	var $ez_url_test = 'https://ecm3.eazycollect.co.uk/api/2.0/client/';
	var $live = false;

	public function __construct($live = true)
	{	parent::__construct();
		if ($this->live = (bool)$live)
		{	$this->clientCode = $this->clientCode_live;
			$this->apiSecret = $this->apiSecret_live;
			$this->ez_url = $this->ez_url_live;
		} else
		{	$this->clientCode = $this->clientCode_test;
			$this->apiSecret = $this->apiSecret_test;
			$this->ez_url = $this->ez_url_test;
		}
	} // fn __construct

	public function TestResponse($data)
	{	$this->GetPostResponse('add_customer', $data);
	} // end of fn TestResponse

	protected function LogResponse($data = '', $response = '')
	{	if ($f = fopen(CITDOC_ROOT . '/ezlog/ezlog_' . date('Y_m_d') . '.txt', 'a'))
		{	ob_start();
			echo $this->datefn->SQLDateTime(), "\ndata:";
			print_r($data);
			echo "response:";
			print_r($response);
			if ($json = json_decode($response, true))
			{	echo "repsonse json:";
				print_r($json);
			}
			echo "\n";
			fputs($f, ob_get_clean());
			fclose($f);
		}
	} // end of fn LogResponse

	public function getCustomer($email){

		$customer_id = '';
		if ($email)
		{							
			$url = 'https://ecm3.eazycollect.co.uk/api/v3/client/CRGHT/customer/?email='.urlencode($email);

			// Create a stream
			$opts = array(
			'http'=>array(
					'method'=>"GET",
					'header'=>"apiKey: " .$this->apiSecret
				)
			);

			$context = stream_context_create($opts);

			// Open the file using the HTTP headers set above
			$response = file_get_contents($url, false, $context);
			$response = json_decode($response,true);

			if(!empty($response['Customers'])){
				$customer = current($response['Customers']);
				$customer_id = $customer['Id'];
			}
		}

		return $customer_id;
	}

	protected function GetPostResponse($endpoint = '', $data = '')
	{	if ($endpoint)
		{	$url = $this->ez_url . $this->clientCode . '/' . $endpoint;
			$post = array();
			$data['apisecret'] = $this->apiSecret;
			foreach ($data as $key=>$value)
			{	$post[] = $key . '=' . urlencode($value);
			}
			//$post = implode('&', $post);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_POST, 1);

			curl_setopt($ch, CURLOPT_POSTFIELDS, implode('&', $post));
			if ($response_raw = curl_exec($ch))
			{	curl_close($ch);
				$this->LogResponse($data, $response_raw);
				return json_decode($response_raw, true);
			} else
			{	//echo $url;
				//$this->VarDump($post);
				//exit;
			}
			curl_close($ch);
			return array();
		}
	} // end of fn GetPostResponse

	public function GetPostResponseTest($endpoint = 'add_customer')
	{	$url = $this->ez_url . $this->clientCode . '/' . $endpoint;//'https://purematrimony.com/';
		
		$data = array('test'=>'fred', 'apisecret'=>$this->apiSecret);
		$data['surname'] = 'Testsn';
		$data['firstname'] = 'Testfn';
		$data['customerRef'] = 'xxxxxx';
		$data['line1'] = 'Carlisle Business Centre';
		$data['line2'] = 'Carlisle Rd';
		$data['line4'] = 'United Kingdom';
		$data['accountHolderName'] = 'Test Websquare';
		$data['accountNumber'] = '0123456';
		$data['bankSortCode'] = '123456';
		foreach ($data as $key=>$value)
		{	$post[] = $key . '=' . urlencode($value);
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, implode('&', $post));

		if ($response_raw = curl_exec($ch))
		{	echo 'response received: ', $url, '<br />';
			echo $this->InputSafeString($response_raw);
			$this->VarDump(json_decode($response_raw, true));
		} else
		{	echo 'no response from: ', $url;
		}
		curl_close($ch);
		exit;
		return array();
	} // end of fn GetPostResponse
	
	public function AddCustomer(Donation $donation, $data = array())
	{	$fail = array();
		$success = array();
		$return = array();
		
		if (is_a($donation, 'Donation') && $donation->id)
		{	$fields['customerRef'] = $donation->ID(false);
			if ($donation->details['donortitle'])
			{	$fields['title'] = $donation->details['donortitle'];
			}
			if (!$fields['firstName'] = $donation->details['donorfirstname'])
			{	$fail[] = 'first name missing';
			}
			if (!$fields['surname'] = $donation->details['donorsurname'])
			{	$fail[] = 'surname missing';
			}
			if ($donation->details['donoradd1'])
			{	$fields['line1'] = $donation->details['donoradd1'];
			}
			if ($donation->details['donoradd2'])
			{	$fields['line2'] = $donation->details['donoradd2'];
			}
			if ($donation->details['donorcity'])
			{	$fields['line3'] = $donation->details['donorcity'];
			}
			if ($country = $this->GetCountry($donation->details['donorcountry']))
			{	$fields['line4'] = $country;
			} else
			{	$fail[] = 'country missing';
			}
			if ($donation->details['donorpostcode'])
			{	$fields['postCode'] = $donation->details['donorpostcode'];
			}
			if ($donation->details['donorphone'])
			{	$fields['homePhoneNumber'] = $donation->details['donorphone'];
			}
			if ($donation->details['donoremail'])
			{	$fields['email'] = $donation->details['donoremail'];
			}
			if (!$fields['accountHolderName'] = $this->AccountNameSafe($data['dd_accountname']))
			{	$fail[] = 'account holder name missing';
			}
			if ($data['dd_accountsortcode'])
			{	if ($this->ValidSortCode($data['dd_accountsortcode']))
				{	$fields['bankSortCode'] = str_replace('-', '', $data['dd_accountsortcode']);
				} else
				{	$fail[] = 'sort code invalid';
				}
			} else
			{	$fail[] = 'sort code missing';
			}
			if ($fields['accountNumber'] = $data['dd_accountnumber'])
			{	if (!$this->ValidBankAccountNumber($fields['accountNumber']))
				{	$fail[] = 'account number invalid';
				}
			} else
			{	$fail[] = 'account number missing';
			}
			
			if (!$fail)
			{	if (false && SITE_TEST)
				{	$return['CustomerID'] = 'TEST-custid-' . substr(md5(time()), 0, 12);
				} else
				{	$result = $this->GetPostResponse('add_customer', $fields);
					if ($result['ID'])
					{	$return['CustomerID'] = $result['ID'];
					} else
					{	if ($result['error'])
						{	$fail[] = $result['error'];
						}
					}
				}
			}
		} else
		{	$fail[] = 'donation missing';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success), 'return'=>$return);
	} // end of fn AddCustomer
	
	public function AddCustomerFromOrder(CartOrder $cartorder, $data = array())
	{	$fail = array();
		$success = array();
		$return = array();
		
		if (is_a($cartorder, 'CartOrder') && $cartorder->id)
		{	$fields['customerRef'] = $cartorder->ID(false);
			if ($cartorder->details['donortitle'])
			{	$fields['title'] = $cartorder->details['donortitle'];
			}
			if (!$fields['firstName'] = $cartorder->details['donorfirstname'])
			{	$fail[] = 'first name missing';
			}
			if (!$fields['surname'] = $cartorder->details['donorsurname'])
			{	$fail[] = 'surname missing';
			}
			if ($cartorder->details['donoradd1'])
			{	$fields['line1'] = $cartorder->details['donoradd1'];
			}
			if ($cartorder->details['donoradd2'])
			{	$fields['line2'] = $cartorder->details['donoradd2'];
			}
			if ($cartorder->details['donorcity'])
			{	$fields['line3'] = $cartorder->details['donorcity'];
			}
			if ($country = $this->GetCountry($cartorder->details['donorcountry']))
			{	$fields['line4'] = $country;
			} else
			{	$fail[] = 'country missing';
			}
			if ($cartorder->details['donorpostcode'])
			{	$fields['postCode'] = $cartorder->details['donorpostcode'];
			}
			if ($cartorder->details['donorphone'])
			{	$fields['homePhoneNumber'] = $cartorder->details['donorphone'];
			}
			if ($cartorder->details['donoremail'])
			{	$fields['email'] = $cartorder->details['donoremail'];
			}
			if (!$fields['accountHolderName'] = $this->AccountNameSafe($data['dd_accountname']))
			{	$fail[] = 'account holder name missing';
			}
			if ($data['dd_accountsortcode'])
			{	if ($this->ValidSortCode($data['dd_accountsortcode']))
				{	$fields['bankSortCode'] = str_replace('-', '', $data['dd_accountsortcode']);
				} else
				{	$fail[] = 'sort code invalid';
				}
			} else
			{	$fail[] = 'sort code missing';
			}
			if ($fields['accountNumber'] = $data['dd_accountnumber'])
			{	if (!$this->ValidBankAccountNumber($fields['accountNumber']))
				{	$fail[] = 'account number invalid';
				}
			} else
			{	$fail[] = 'account number missing';
			}
			
			if (!$fail)
			{	if (false && SITE_TEST)
				{	$return['CustomerID'] = 'TEST-custid-' . substr(md5(time()), 0, 12);
				} else
				{	$result = $this->GetPostResponse('add_customer', $fields);
					if ($result['ID'])
					{	$return['CustomerID'] = $result['ID'];
					} else
					{	if ($result['error'])
						{	$fail[] = $result['error'];
						} else
						{	ob_start();
							print_r($result);
							$fail[] = 'ez customer failed: ' . ob_get_clean();
						}
					}
				}
			}
		} else
		{	$fail[] = 'order missing';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success), 'return'=>$return);
	} // end of fn AddCustomerFromOrder
	
	public function AddContract(Donation $donation, $customerID = '')
	{	$fail = array();
		$success = array();
		$return = array();
		$fields = array();
		
		if (is_a($donation, 'Donation') && $donation->id)
		{	if (!$customerID)
			{	$fail[] = 'no customer ID';
			}
			$fields['scheduleName'] = $this->scheduleName;
			$fields['terminationType'] = 'Until further notice';
			$startdate = $donation->NextWorkingDay();
			$paymentDayInMonth = date('j', $startdate);
			if ($paymentDayInMonth <= 15)
			{	$paymentDayInMonth = 15;
				$startdate = mktime(0,0,0,date('n', $startdate), $paymentDayInMonth = 15, date('Y', $startdate));
			} else
			{	$startdate = mktime(0,0,0,date('n', $startdate) + 1, $paymentDayInMonth = 1, date('Y', $startdate));
			}
			$fields['start'] = date('d/m/Y', $startdate);
			$fields['isGiftAid'] = ($donation->details['giftaid'] ? 'True' : 'False');
			$fields['atTheEnd'] = 'Switch to further notice';
			$fields['paymentDayInMonth'] = $paymentDayInMonth;
			$fields['paymentMonthInYear'] = date('n', $startdate);
			$fields['amount'] = number_format($donation->details['amount'] + $donation->details['adminamount'], 2, '.', '');
			
			if (!$fail)
			{	if (false && SITE_TEST)
				{	$return['DirectDebitRef'] = 'TEST-ddref-' . substr(md5(time()), 0, 12);
					$return['ContractID'] = 'TEST-contrid-' . substr(md5(time()), 0, 12);
					$return['StartDate'] = $startdate;
				} else
				{	$result = $this->GetPostResponse('customer/' . $customerID . '/add_contract', $fields);
					if ($result['DirectDebitRef'] && $result['ID'])
					{	$return['DirectDebitRef'] = $result['DirectDebitRef'];
						$return['ContractID'] = $result['ID'];
						$return['StartDate'] = $startdate;
					} else
					{	$fail[] = $result['error'];
					}
				}
			}
		} else
		{	$fail[] = 'donation missing';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success), 'return'=>$return);
	} // end of fn AddContract
	
	public function AddContractFromOrder(CartOrder $cartorder, $customerID = '')
	{	$fail = array();
		$success = array();
		$return = array();
		$fields = array();
		
		if (is_a($cartorder, 'CartOrder') && $cartorder->id)
		{	if (!$customerID)
			{	$fail[] = 'no customer ID';
			}
			$fields['scheduleName'] = $this->scheduleName;
			$fields['terminationType'] = 'Until further notice';
			$startdate = $cartorder->NextWorkingDay();
			$paymentDayInMonth = date('j', $startdate);
			if ($paymentDayInMonth <= 15)
			{	$paymentDayInMonth = 15;
				$startdate = mktime(0,0,0,date('n', $startdate), $paymentDayInMonth = 15, date('Y', $startdate));
			} else
			{	$startdate = mktime(0,0,0,date('n', $startdate) + 1, $paymentDayInMonth = 1, date('Y', $startdate));
			}
			$fields['start'] = date('d/m/Y', $startdate);
			$fields['isGiftAid'] = ($cartorder->details['giftaid'] ? 'True' : 'False');
			$fields['atTheEnd'] = 'Switch to further notice';
			$fields['paymentDayInMonth'] = $paymentDayInMonth;
			$fields['paymentMonthInYear'] = date('n', $startdate);
			$fields['amount'] = number_format($cartorder->details['total_monthly'], 2, '.', '');
			
			if (!$fail)
			{	if (false && SITE_TEST)
				{	$return['DirectDebitRef'] = 'TEST-ddref-' . substr(md5(time()), 0, 12);
					$return['ContractID'] = 'TEST-contrid-' . substr(md5(time()), 0, 12);
					$return['StartDate'] = $startdate;
				} else
				{	$result = $this->GetPostResponse('customer/' . $customerID . '/add_contract', $fields);
					if ($result['DirectDebitRef'] && $result['ID'])
					{	$return['DirectDebitRef'] = $result['DirectDebitRef'];
						$return['ContractID'] = $result['ID'];
						$return['StartDate'] = $startdate;
					} else
					{	$fail[] = $result['error'];
					}
				}
			}
		} else
		{	$fail[] = 'order missing';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success), 'return'=>$return);
	} // end of fn AddContractFromOrder
	
	public function AddPaymentRequest(Donation $donation, $date = '')
	{	$fail = array();
		$success = array();
		$return = array();
		$fields = array();
		
		if (is_a($donation, 'Donation') && $donation->id)
		{	
			$fields['date'] = date('d/m/Y', strtotime($date));
			$fields['amount'] = number_format($donation->details['amount'], 2, '.', '');
			$fields['comment'] = 'Monthly payment taken from website';
			
			if (!$fail)
			{	
				$result = $this->GetPostResponse('contract/' . $donation->extrafields['ContractID'] . '/add_payment', $fields);
				if ($result['ID'])
				{	$return['PaymentID'] = $result['ID'];
					$return['Amount'] = $fields['amount'];
				} else
				{	$fail[] = $result['error'];
				}
			//	ob_start();
			//	print_r($result);
			//	$fail[] = ob_get_clean();
			}
		} else
		{	$fail[] = 'donation missing';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success), 'return'=>$return);
	} // end of fn AddPaymentRequest
	
	public function OneOffDonationButton(Donation $donation)
	{	
	} // end of fn OneOffDonationButton
	
} // end of defn EazyCollectAPI
?>