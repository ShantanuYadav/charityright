<?php
class PaymentGateway extends Base
{	var $id = 0;
	var $details = array();
	
	function __construct($id = 0)
	{	parent::__construct();
		$this->Get($id);
	} // end of fn __construct
	
	function Reset()
	{	$this->id = 0;
		$this->details = array();
	} // end of fn Reset
	
	function Get($id = 0)
	{	$this->Reset();
		if (is_array($id))
		{	$this->id = $id['pgid'];
			$this->details = $id;
		} else
		{	if ($id = (int)$id)
			{	if ($result = $this->db->Query('SELECT * FROM paymentgateways WHERE pgid=' . (int)$id))
				{	if ($row = $this->db->FetchArray($result))
					{	$this->Get($row);
					}
				}
			}
		}
	} // end of fn Get
	
/*	public function InitialiseCountryGateways()
	{	$sql = 'SELECT ccode FROM countries';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->db->Query('INSERT INTO countrygateways SET ccode="' . $row['ccode'] . '", pgid=1');
			}
		}
	} // end of fn InitialiseCountryGateways*/
	
	public function CanDelete()
	{	return $this->id && !$this->CountriesUsedIn();
	} // end of fn CanDelete
	
	public function CountriesUsedIn()
	{	$countries = array();
		$sql = 'SELECT countries.* FROM countrygateways, countries WHERE countrygateways.ccode=countries.ccode AND countrygateways.pgid=' . $this->id;
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$countries[$row['ccode']] = $row;
			}
		}
		return $countries;
	} // end of fn CountriesUsedIn
	
	public function CountriesList()
	{	ob_start();
		if ($this->id)
		{	echo '<div class="clear"></div>';
			if ($countries = $this->CountriesUsedIn())
			{	echo '<h3>Currently used in ...</h3><ul>';
				foreach ($countries as $country)
				{	echo '<li><a href="ctryedit.php?ctry=', $country['ccode'], '">', $this->InputSafeString($country['shortname']), '</a></li>';
				}
				echo '</ul>';
			} else
			{	echo '<h3>Not currently used</h3>';
			}
		}
		return ob_get_clean();
	} // end of fn CountriesList
	
	public function Delete()
	{	if ($this->CanDelete())
		{	if ($result = $this->db->Query('DELETE FROM paymentgateways WHERE pgid=' . (int)$this->id))
			{	$this->Reset();
				return $this->db->AffectedRows();
			}
		}
		return false;
	} // end of fn Delete
	
	public function Save($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if ($gateway = $this->SQLSafe($data['gateway']))
		{	$fields[] = 'gateway="' . $gateway . '"';
		} else
		{	$fail[] = 'Name missing';
		}
		
		$fields[] = 'buttonfunction="' . $this->SQLSafe($data['buttonfunction']) . '"';
		$fields['live'] = 'live=' . (($this->id  && ($data['live'] || !$this->CanDelete())) ? '1' : '0');
		$fields['suspended'] = 'suspended=' . ($data['suspended'] ? '1' : '0');
		
		if ($this->id || !$fail)
		{	if ($set = implode(', ', $fields))
			{	$date = $this->datefn->SQLDateTime();
				if ($this->id)
				{	$sql = 'UPDATE paymentgateways SET ' . $set . ' WHERE pgid=' . (int)$this->id;
				} else
				{	$sql = 'INSERT INTO paymentgateways SET ' . $set;
				}
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows())
					{	
						if ($this->id)
						{	$success[] = 'changes saved';
							$this->Get($this->id);
						} else
						{	$success[] = 'new payment gateway created';
							$this->Get($this->db->InsertID());
						}
					}
				} //else echo '<p>', $this->db->Error(), '</p>';
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save

	public function DeleteLink()
	{	ob_start();
		if ($this->CanDelete())
		{	
			echo '<p><a href="gateway.php?id=', $this->id, '&delete=1', $_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'do you really want to ' : '', 'delete this payment gateway</a></p>';
		}
		return ob_get_clean();
	} // end of fn DeleteLink
	
	public function InputForm()
	{	ob_start();
		if (!$data = $this->details)
		{	$data = $_POST;
		}
		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id , '');
		$form->AddTextInput('Name', 'gateway', $this->InputSafeString($data['gateway']), 'long', 255);
		$form->AddTextInput('Function name', 'buttonfunction', $this->InputSafeString($data['buttonfunction']), 'long', 255);
		if ($this->CanDelete())
		{	$form->AddCheckBox('Live (available to add to countries)', 'live', 1, $this->details['live']);
		}
		$form->AddCheckBox('Suspend for new payments', 'suspended', 1, $this->details['suspended']);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create New Payment Gateway', 'submit');
		
		echo $this->DeleteLink();
		$form->Output();
		
		return ob_get_clean();
	} // end of fn InputForm
	
} // end of defn PaymentGateway
?>