<?php
class CampaignAvatar extends BlankItem
{	protected $imagesizes = array('thumb'=>array('w'=>100, 'h'=>70), 'small'=>array('w'=>201, 'h'=>140), 'medium'=>array('w'=>400, 'h'=>140), 'large'=>array('w'=>1000, 'h'=>400), 'og'=>array('w'=>600, 'h'=>315));
	protected $imagelocation = '';
	protected $imagedir = '';

	function __construct($id = 0)
	{	parent::__construct($id, 'campaignavatars', 'caid');
		$this->imagelocation = SITE_URL . 'img/cavatars/';
		$this->imagedir = CITDOC_ROOT . '/img/cavatars/';
	} //  end of fn __construct
	
	public function UsedCount()
	{	$sql = 'SELECT COUNT(campaigns.cid) AS used_count FROM campaigns WHERE avatarid=' . $this->id;
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return $row['used_count'];
			}
		}
	} // end of fn UsedCount
	
	public function HasImage($size = 'small')
	{	if ($this->id && file_exists($this->GetImageFile($size)))
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn HasImage
	
	public function ImageDimensions($size = 'small')
	{	if ($this->imagesizes[$size])
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions
	
	public function GetImageFile($size = 'small')
	{	return $this->ImageFileDirectory($size) . '/' . $this->id .'.png';
	} // end of fn GetImageFile
	
	public function ImageFileDirectory($size = 'small')
	{	return $this->imagedir . $this->InputSafeString($size);
	} // end of fn FunctionName
	
	public function GetImageSRC($size = 'small')
	{	if ($this->HasImage($size))
		{	return $this->imagelocation . $this->InputSafeString($size) . '/' . $this->id . '.png';
		}
	} // end of fn GetImageSRC
	
	public function ImageHTML($size = 'small')
	{	if ($src = $this->GetImageSRC($size))
		{	ob_start();
			echo '<img src="', $src, '" alt="', $alt = $this->InputSafeString($this->details['imagedesc']), '" title="', $alt, '" />';
			return ob_get_clean();
		}
	} // end of fn ImageHTML
	
} // end of defn CampaignAvatar
?>