<?php
class EventsPage extends EventsBasePage
{
	public function __construct()
	{	parent::__construct();
	//	$this->js['events'] = 'events.js';
		$this->css['page_events'] = 'page_events.css';
	} // end of fn __construct

	function MainBodyContent()
	{
		$crstars_page = new PageContent('cr-stars');
		echo '<div class="container event-page-header-container"><div class="container_inner"><h1 class="page_heading">'.$this->page->PageTitleDisplay().'</h1><h2 class="page_sub_heading">Join our fight against hunger by attending one of our regular events.</h2></div></div>';
		echo '<div class="container event-page-content-container"><div class="container_inner">';

		if ($eventdates = $this->GetEventDates()) {
			$events = array();
			$venues = array();
			echo '<div class="event-listing">';

			foreach ($eventdates as $event_row) {
				$eventdate = new EventDate($event_row);
				if (!$events[$eventdate->details['eid']]) {
					$events[$eventdate->details['eid']] = new Event($eventdate->details['eid']);
				}
				if (!$venues[$eventdate->details['venue']]){
					$venues[$eventdate->details['venue']] = new EventVenue($eventdate->details['venue']);
				}
				if ($image = $events[$eventdate->details['eid']]->GetImageSRC('small')) {
					$img_style = 'style="background-image: url(\''.$image.'\');"';
				}else{
					$img_style = '';
				}

				echo '<div class="event-listing-item"><div class="event-listing-item-inner">';
					echo '<div class="event-listing-item-image" '.$img_style.'>';
						echo '<a href="'.$eventdate->Link().'">';
						echo '</a>';
					echo '</div>';
					echo '<div class="event-list-item-content">';
						echo '<a href="'.$eventdate->Link().'">';
							echo '<h3>'.$this->InputSafeString($events[$eventdate->details['eid']]->details['eventname']).'</h3>';
							echo '<p>'.$eventdate->DatesString(). ' at '. $venues[$eventdate->details['venue']]->LocationString().'</p>';
						echo '</a>';
					echo '</div></div>';
				echo '<div class="clear"></div></div>';
			}
			$create_your_own_event = new PageContent('create-your-own-event');

			echo '<div class="event-listing-item event-listing-item-create"><div class="event-listing-item-inner">';
				echo '<div class="event-list-item-content">';
					echo '<h3>SUPPORT US AT YOUR EVENT</h3>';
					echo '<p>Could you organise and host an event that will help us to feed hungry people?</p>';
					echo '<a href="'.$create_your_own_event ->Link().'" class="button-find-more">Find Out More</a>';
				echo '</div>';
			echo '</div></div>';

			echo '<div class="clear"></div></div>';
		}else{
			echo '<h3 class="alert-box alert-box-orange no-events">No events coming up. Why not create an event? <a href="'.$crstars_page->Link().'">Find out more</a>.</h3>';
		}
		if ($previous = $this->GetPreviousLink()){
			echo '<div class="button-container"><a href="', $previous['link'], '" class="button-events-link">', $previous['label'], '</a></div>';
		}
		echo '</div></div>';
		/*
		echo '<div class="container containerEventsPage"><div class="container_inner"><h1 class="page_heading">', $this->page->PageTitleDisplay(), '</h1>';
		echo '<h2 class="page_sub_heading">Join our fight against hunger by attending one of our regular events.</h2>';
		if ($eventdates = $this->GetEventDates())
		{	$events = array();
			$venues = array();
			echo '<div class="eventdates_listing"><ul>';
			foreach ($eventdates as $event_row)
			{	$eventdate = new EventDate($event_row);
				if (!$events[$eventdate->details['eid']])
				{	$events[$eventdate->details['eid']] = new Event($eventdate->details['eid']);
				}
				if (!$venues[$eventdate->details['venue']])
				{	$venues[$eventdate->details['venue']] = new EventVenue($eventdate->details['venue']);
				}
				echo '<li><div class="edlImage"';
				if ($image = $events[$eventdate->details['eid']]->GetImageSRC('small'))
				{	echo ' style="background-image: url(\'', $image, '\');"';
				}
				echo '><a href="', $eventdate->Link(), '"></a></div><div class="edlDetails"><a href="', $eventdate->Link(), '"><h2>', $this->InputSafeString($events[$eventdate->details['eid']]->details['eventname']), '</h2><p>', $eventdate->DatesString(), ' at ', $venues[$eventdate->details['venue']]->LocationString(), '</p></a></div></li>';
			}
			$crstars_page = new PageContent('cr-stars');
			echo '<li class="eventsCRLink"><div><h2>Create an Event</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p><a href="', $crstars_page->Link(), '">Find Out More</a></div></li></ul><div class="clear"></div></div>';
		} else
		{	echo '<h3>No events coming up.</h3>';
		}
		if ($previous = $this->GetPreviousLink())
		{	echo '<div class="eventsPreviousLink"><a href="', $previous['link'], '">', $previous['label'], '</a></div>';
		}
		echo '</div></div>';
		*/
	} // end of fn MainBodyContent

	protected function GetPreviousLink()
	{	if (($previous_page = new PageContent('previous-events')) && $previous_page->id)
		{	return array('label'=>'Previous Events', 'link'=>$previous_page->Link());
		}
	} // end of fn GetPreviousLink

	protected function GetEventDates()
	{	$tables = array('eventdates'=>'eventdates');
		$fields = array('eventdates.*');
		$where = array('eventdates.endtime>"' . $this->datefn->SQLDateTime() . '"', 'eventdates.live=1');
		$orderby = array('endtime'=>'eventdates.endtime ASC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables ,$fields ,$where ,$orderby), 'edid', true);
	} // end of fn GetEventDates

} // end of class EventsPage
?>