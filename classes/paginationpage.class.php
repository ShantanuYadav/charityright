<?php
class PaginationPage
{	protected $pagenum = 0;
	protected $baseLink = '';
	protected $current = false;
	protected $pagename = 'page';
	protected $hashlink = '';

	function __construct($pagenum = 0, $baseLink = '', $current = false, $pagename = '', $hashlink = '')
	{	$this->pagenum = (int)$pagenum;
		if ($pagename)
		{	$this->pagename = $pagename;
		}
		if ($hashlink)
		{	$this->hashlink = $hashlink;
		}
		$this->current = $current;
		$this->SetBaseLink($baseLink);
	} // end of constructor
	
	function SetBaseLink($baseLink = '')
	{	if (!$this->current)
		{	ob_start();
			echo $baseLink, strstr($baseLink, '?') ? '&' : '?', $this->pagename, '=', $this->pagenum + 1, $this->hashlink ? ('#' . $this->hashlink) : '';
			$this->baseLink = ob_get_clean();
		}
	} // end of fn SetBaseLink
	
	function Display()
	{	ob_start();
		echo '<span', ($this->baseLink ? ' class="paginlink"' : ''), '>';
		if ($this->baseLink)
		{	echo '<a href="', $this->baseLink, '">';
		}
		echo $this->PageNum(), $this->baseLink ? '</a>' : '', '</span>';
		return ob_get_clean();
	} // end of fn Display
	
	function PageNum()
	{	return $this->pagenum + 1;
	} // end of fn PageNum
	
	function Link()
	{	return $this->baseLink;
	} // end of fn GetLink
	
} // end of class  PaginationPage
?>