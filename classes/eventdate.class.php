<?php
class EventDate extends BlankItem
{	public $event = array();
	public $venue = array();

	public function __construct($id = 0)
	{	parent::__construct($id, 'eventdates', 'edid');
	} // fn __construct
	
	public function GetExtra()
	{	$sql = $this->db->BuildSQL(array('events'), array('events.*'), array('events.eid=' . (int)$this->details['eid']));
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	$this->event = $row;
			}
		}
		$sql = $this->db->BuildSQL(array('eventvenues'), array('eventvenues.*'), array('eventvenues.evid=' . (int)$this->details['venue']));
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	$this->venue = $row;
			}
		}
	} // fn GetExtra
	
	public function GetTicketTypes($filter = array())
	{	if ($filter['live'])
		{	if ($this->details['endtime'] < $this->datefn->SQLDateTime())
			{	return array();
			}
		}
		$tables = array('tickettypes'=>'tickettypes');
		$fields = array('tickettypes.*');
		$where = array('eventdate'=>'tickettypes.edid=' . (int)$this->id);
		$orderby = array();
		switch ($filter['orderby'])
		{	case 'name':
				$orderby[] = 'tickettypes.ttypename ASC';
				break;
			default:
				$orderby[] = 'tickettypes.price DESC';
		}
		if ($filter['live'])
		{	$where['live'] = 'tickettypes.live=1';
		}
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'ttid', true);
	} // fn GetTicketTypes
	
	public function GetTicketCount()
	{	$count = 0;
		if ($tickets = $this->GetTicketTypes())
		foreach ($tickets as $ticket_row)
		{	$count += $ticket_row['booklimit'];
		}
		return $count;
	} // fn GetTicketCount
	
	public function GetBookings()
	{	$tables = array('bookings'=>'bookings', 'tickettypes'=>'tickettypes');
		$where = array('tickettypes_link'=>'tickettypes.ttid=bookings.ttid', 'edid'=>'tickettypes.edid=' . (int)$this->id);
		$orderby = array('bookings.bid ASC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, array('bookings.*'), $where, $orderby), 'bid', true);
	} // fn GetBookings
	
	public function Link()
	{	if ($this->id) return SITE_URL . 'event-date/' . $this->id . '/' . $this->event['slug'] . '/' . $this->venue['slug'] . '/';
	} // fn Link
	
	public function DatesString()
	{	ob_start();
		if ($this->details['allday'])
		{	echo date('d M Y', strtotime($this->details['starttime']));
		} else
		{	if (substr($this->details['starttime'], 0, 10) === substr($this->details['endtime'], 0, 10))
			{	echo date('d M Y g:i a', strtotime($this->details['starttime'])), ' to ', date('g:i a', strtotime($this->details['endtime']));
			} else
			{	echo date('d M Y g:i a', strtotime($this->details['starttime'])), ' to ', date('d M Y g:i a', strtotime($this->details['endtime']));
			}
		}
		return ob_get_clean();
	} // fn DatesString
	
	public function OrderFromBooking($bookingrow = array())
	{	$tables = array('bookorderitems'=>'bookorderitems', 'bookorders'=>'bookorders');
		$fields = array('bookorders.*');
		$where = array('bookorders_link'=>'bookorders.orderid=bookorderitems.orderid', 'orderitem'=>'bookorderitems.orderitem=' . (int)$bookingrow['orderitem']);
		
		if ($result = $this->db->Query($sql = $this->db->BuildSQL($tables, $fields, $where)))
		{	if ($row = $this->db->FetchArray($result))
			{	return $this->AssignBookOrder($row);
			}
		}
	} // fn OrderFromBooking
	
	protected function AssignBookOrder($orderrow = array())
	{	return new BookOrder($orderrow);
	} // fn AssignBookOrder
	
} // end of defn EventDate
?>