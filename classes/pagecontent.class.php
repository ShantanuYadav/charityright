<?php
class PageContent extends Base
{	public $id = 0;
	public $details = array();
	public $language = '';
	public $liveonly = false;
	public $includepath = '';
	protected $bannerlocation = 'img/pages/';
	protected $bannerdir = '';
	protected $bannerpath = '';
	protected $pagesections = array();
	
	function __construct($id = 0, $liveonly = true)
	{	parent::__construct();
		$this->bannerdir = CITDOC_ROOT . '/' . $this->bannerlocation;
		$this->bannerpath = SITE_URL . $this->bannerlocation;
		$this->includepath = CITDOC_ROOT . '/pageinc/';
		$this->liveonly = ($liveonly ? true : false);
		$this->AssignPageLanguage();
		
		$this->Get($id);
	} //  end of fn __construct
	
	function AssignPageLanguage()
	{	$this->language = $this->lang;
	} // end of fn AssignPageLanguage
	
	function Reset()
	{	$this->id = 0;
		$this->details = array();
	} // end of fn Reset
	
	function Get($id = 0)
	{	$this->Reset();
		if (is_array($id))
		{	$this->details = $id;
			$this->id = $id['pageid'];
			$this->AddDetailsForLang($this->language);
			$this->GetPageSections();
			$this->details['pagename'] = strtolower($this->details['pagename']);
		} else
		{	if ($id)
			{	if ((int)$id && (int)$id == $id)
				{	// get by id
					$sql = 'SELECT * FROM pages WHERE pageid=' . (int)$id;
				} else
				{	// try to get by name
					$sql = 'SELECT * FROM pages WHERE pagename="' . $this->SQLSafe($id) . '"';
				}
				if ($this->liveonly)
				{	$sql .= ' AND pagelive=1';
				}
				if ($result = $this->db->Query($sql))
				{	if ($row = $this->db->FetchArray($result))
					{	$this->Get($row);
					}
				}
			}
		}
		
	} // end of fn Get
	
	protected function GetPageSections($liveonly = true)
	{	$this->pagesections = array();
		$sql = 'SELECT * FROM pagesections WHERE parentsection=0 AND pageid=' . $this->id;
		if ($liveonly)
		{	$sql .= ' AND live=1';
		}
		$sql .= ' ORDER BY listorder, psid';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->pagesections[$row['psid']] = $row;
			}
		}
	} // end of fn GetPageSections
	
	public function RemovePageSections()
	{	$this->pagesections = array();
	} // end of fn RemovePageSections
	
	public function PageSections()
	{	return $this->pagesections;
	} // end of fn PageSections
	
	function AddDetailsForLang($lang = '')
	{	$sql = 'SELECT * FROM pages_lang WHERE pageid=' . $this->id . ' AND lang="' . $lang . '"';
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	foreach ($row as $field=>$value)
				{	$this->details[$field] = $value;
				}
			} else
			{	if ($lang == $this->def_lang)
				{	// as last resort go for english
					if ($lang != 'en') // only if default language not english
					{	$sql = 'SELECT * FROM pages_lang WHERE pageid=' . $this->id . ' AND lang="en"';
						if ($result = $this->db->Query($sql))
						{	if ($row = $this->db->FetchArray($result))
							{	foreach ($row as $field=>$value)
								{	$this->details[$field] = $value;
								}
							}
						}
					}
				} else
				{	$this->AddDetailsForDefaultLang();
				}
			}
		}
	} // end of fn AddDetailsForLang
	
	function AddDetailsForDefaultLang()
	{	$this->AddDetailsForLang($this->def_lang);
	} // end of fn AddDetailsForDefaultLang
	
	public function IncludeFilePath()
	{	if ($this->details['includefile'])
		{	return $this->includepath . $this->details['includefile'];
		}
	} // end of fn IncludeFilePath

	function IncludeFileExists($filename = "")
	{	return file_exists($this->includepath . $filename);
	} // end of fn IncludeFileExists
	
	function HTMLMainContent()
	{	ob_start();
		echo stripslashes($this->details['pagetext']);
	//	echo $this->details["includefile"];
	//	if ($this->details["includefile"] && $this->IncludeFileExists($this->details["includefile"]))
	//	{	include($this->includepath . $this->details["includefile"]);
	//	}
		return ob_get_clean();
	} // end of fn HTMLMainContent
	
	function BodyName()
	{	return $this->details['pagename'];
	} // end of fn BodyName
	
	function SampleText($tofind = '')
	{	$text = $this->RawText();
		if ($tofind)
		{	return $text;
		} else
		{	return $text;
		}
	} // end of fn SampleText
	
	function RawText()
	{	return strip_tags(str_replace(array('</h2>', '</h3>'), ' - ', stripslashes($this->details['pagetext'])));
	} // end of fn RawText
	
	function Link()
	{	if ($this->details['redirectlink'])
		{	if (strstr($this->details['redirectlink'], 'http') || strstr($this->details['redirectlink'], 'https') || strstr($this->details['redirectlink'], 'mailto:'))
			{	return $this->details['redirectlink'];
			} else
			{	return SITE_SUB . (substr($this->details['redirectlink'], 0, 1) == '/' ? '' : '/') . $this->details['redirectlink'];
			}
		} else
		{	return SITE_SUB . '/' . $this->details['pagename'] . '/';
		}
	} // end of fn Link
	
	function FullLink()
	{	return 'http://' . $_SERVER['HTTP_HOST'] . $this->Link();
	} // end of fn FullLink
		
	function GetMenusUsedIn()
	{	$tables = array('femenus', 'femenuitems');
		$fields = array('femenus.*');
		$where = array('femenuitems_link'=>'femenuitems.menuid=femenus.menuid', 'pageid'=>'femenuitems.pageid=' . $this->id);
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where), 'menuid');
	} // end of fn GetMenusUsedIn
	
	// Check if a page was found
	function Found()
	{	return (isset($this->details['pagename']) && $this->details['pagename'] != '') ? true : false;	
	} // end of fn Found
	
	public function PageTitleDisplay()
	{	//return parent::TitleDisplay($this->details["pagetitle"]);
		return $this->InputSafeString($this->details['pagetitle']);
	} // end of fn PageTitleDisplay
	
	public function GetSidebarBlocks()
	{	$blocks = array();
		if ($this->id)
		{	$sql='SELECT sidebarblocks.*, pagesidebar.sborder FROM pagesidebar, sidebarblocks WHERE pagesidebar.sbid=sidebarblocks.sbid AND pagesidebar.pageid=' . (int)$this->id . ' ORDER BY pagesidebar.sborder';
			if ($result = $this->db->Query($sql))
			{	while ($row = $this->db->FetchArray($result))
				{	$blocks[$row['sbid']] = $row;
				}
			}
		}
		return $blocks;
	} // end of fn GetSidebarBlocks

	public function HasBanner()
	{	return $this->id && file_exists($this->GetBannerFile());
	} // end of fn HasBanner

	public function GetBannerFile()
	{	return $this->bannerdir . $this->id .'.png';
	} // end of fn GetBannerFile

	public function GetBannerSRC()
	{	if ($this->HasBanner())
		{	return $this->bannerpath . $this->id . '.png';
		}
	} // end of fn GetBannerSRC

	public function GetQuickDonateOptions($live_only = true)
	{	$options = json_decode($this->details['quickdonate'], true);
		return ($options['live'] || !$live_only) ? $options : array();
	} // end of fn GetQuickDonateOptions
	
} // end of defn PageContent
?>