<?php
/*
PHP class Google Currency Converter
Feb 06 2009
webmaster@chazzuka.com
*/
class GoogleCurrencyConverter extends Base
{
	// GOOGLE URL
	//CONST GOOGLE_URL = "https://www.google.com/finance/converter?a=%d&from=%s&to=%s";
	CONST GOOGLE_URL = "https://finance.google.co.uk/bctzjpnsun/converter?a=%d&from=%s&to=%s";
	CONST MSN_URL = 'https://www.msn.com/en-us/money/currencydetails/fi-%s%s';
	/*
	Fetch with CURL
	params: amount, 3 Digit Currency Code From,3 Digit Currency Code to
	*/
	private function load($a,$from,$to)
	{	$url = self::ConvertURL($a,$from,$to);
		//return $url;
		$ch = curl_init ();
			  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			  curl_setopt($ch, CURLOPT_URL, $url);
			  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			  $response = curl_exec($ch);
			  curl_close($ch);
		return $response;
	} // end of fn load
	
	public function ConvertURL($a,$from,$to)
	{	//return sprintf(self::GOOGLE_URL,$a,$from,$to);
		return sprintf(self::MSN_URL,$from,$to);
	} // end of fn ConvertURL
	
	/*
	Try to Convert
	params: amount, 3 Digit Currency Code From,3 Digit Currency Code to
	*/
	public static function convert($a,$from,$to)
	{	return self::convertMSN($a,$from,$to);
		
		$response = self::load($a,$from,$to);
		$return_value = false;
		if($response)
		{	foreach (explode("\n", $response) as $line)
			{	$pattern = '|>([\d]+[.]?[\d]+) ' . $to . '<|';
				if (preg_match($pattern, $line, $matches))
				{	$return_value = $matches[1];
					break;
				}
			}
		}
		return $return_value;
	} // end of fn convert

	public static function convertMSN($a,$from,$to)
	{	
		$response = self::load($a,$from,$to);
		$return_value = false;
		if($response)
		{	foreach (explode("\n", $response) as $line)
			{	if (strstr($line, 'cc-details-value unchanged'))
				{	$header_found = true;
				} else
				{	if ($header_found)
					{	$pattern = '|>([\d]+[.]?[\d]+)<|';
						if (preg_match($pattern, str_replace(',', '', $line), $matches))
						//if (preg_match($pattern, $line, $matches))
						{	$return_value = $matches[1];
							break;
						}
					}
				}
			}
		} 
		return $return_value;
	} // end of fn convertMSN

	public static function Test()
	{	
		if ($value = self::convert(1,'GBP','USD'))
		{	echo '<p>current rate GBP to USD: ', $value, '</p>';
		} else
		{	$url = self::ConvertURL(1,'GBP','USD');
			echo '<h4>currently failing check <a href="', $url, '" target="_blank">', $url, '</a> for potential redirect (opens in new tab)</h4><p>Return was ...</p><pre>', self::load(1,'GBP','USD'), '</pre>';
			
		}
		
	} // end of fn Test
	
} // end of class GoogleCurrencyConverter
?>