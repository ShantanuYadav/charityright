<?php
class AdminWorldPayEar extends WorldPayEar
{
	function __construct()
	{	parent::__construct();
	} // end of fn __construct
	
	function LogRecord($text = ''){}
	function LogFullPost($text = ''){}
	
	protected function DefaultAction()
	{	$this->failmessage = 'appropriate action not found';
	} // end of fn DefaultAction
	
	private function PostFromText($text = '')
	{	$post = array();
		if ($parameters = explode('&', $text))
		{	foreach($parameters as $parameter_full)
			{	if (count($parameter = explode('=', $parameter_full)) == 2)
				{	$post[trim($parameter[0])] = urldecode(trim($parameter[1]));
				}
			}
		}
		return $post;
	} // end of fn PostFromText
	
	public function PostFromTextRaw($text = '')
	{	$post = array();
		if ($parameters = explode('&', $text))
		{	foreach($parameters as $parameter_full)
			{	if (count($parameter = explode('=', $parameter_full)) == 2)
				{	$post[trim($parameter[0])] = trim($parameter[1]);
				}
			}
		}
		return $post;
	} // end of fn PostFromTextRaw
	
	public function ProcessPostText($text = '')
	{	if ($post = $this->PostFromText($text))
		{	$oldpost = $_POST;
			$_POST = $post;
			if ($this->Verify())
			{	$this->Action();
				//$this->VarDump($_POST);
			} else
			{	$this->failmessage = 'verification failed';
			}
			$_POST = $oldpost;
		} else
		{	$this->failmessage = 'no parameters found in text';
		}
	} // end of fn ProcessPostText
	
} // end of defn AdminWorldPayEar
?>