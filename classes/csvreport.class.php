<?php
class CSVReport extends Base
{	private $headers = array();
	private $rows = array();

	public function __construct()
	{	parent::__construct();
	} // fn __construct
	
	public function AddToHeaders($header = '')
	{	if (is_array($header))
		{	foreach($header as $fieldname)
			{	$this->headers[] = trim($fieldname);
			}
		} else
		{	$this->headers[] = trim($header);
		}
	} // end of fn AddToHeaders
	
	public function AddToRows($row = array())
	{	if (is_array($row) && $row)
		{	$this->rows[] = $row;
		}
	} // end of fn AddToRows
	
	public function Output($filename = '')
	{	header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . ($filename ? $filename : ('charityright_' . date('Y-m-d_H-i') . '.csv')) . '"');
		if ($this->headers && is_array($this->headers))
		{	echo implode(',', $this->headers), "\n";
		}
		if ($this->rows && is_array($this->rows))
		{	foreach ($this->rows as $row)
			{	echo implode(',', $row), "\n";
			}
		}
		exit;
	} // end of fn Output
	
} // end of defn CSVReport
?>