<?php
class BasePage extends BaseFunctions
{
	var $title = '';
	var $pageName = '';
	var $user;
	var $page;
	var $bodyOnLoadJS = array();
	var $bodyOnUnLoadJS = array();
	var $canonical_link = '';
	public $fb_meta = array();
	protected $stages = array();
	protected $stage = 1;
	protected $quickdonate;
	protected $do_infform_footer = true;
	protected $suppress_google_analytics = false;

	function __construct($pageName = '', $title = '') // constructor
	{
		parent::__construct();
		$this->SetUTMValues();

		$this->js['jquery'] = 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js';
		$this->js['paymentvarified'] = 'jquery-3.4.1.js';
		$this->js['maps.api'] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAHImaffLZPznceJFmDYimIN9Rd9tuJGtc&libraries=places';
		$this->js['jquery.googlemap'] = 'https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5/jquery.googlemap.min.js';
		$this->js['fitvids'] = 'jquery.fitvids.js';
		$this->js['jquery.idTabs'] = 'jquery.idTabs.min.js';
		//$this->js['jquery.mmenu'] = 'jquery.mmenu.min.all.js';
		$this->js['jquery.mmenu'] = 'jquery.mmenu.all.js';
		$this->css['jquery.mmenu'] = 'jquery.mmenu.all.css';
		$this->css['jqModal'] = 'jqModal.css';
		//$this->js['jqModal'] = 'jqModal.js';
		$this->js['jqModal'] = 'https://cdnjs.cloudflare.com/ajax/libs/jqModal/1.4.2/jqModal.min.js';
		$this->css['magnific-popup'] = 'magnific-popup.css';
		$this->js['magnific-popup'] = 'jquery.magnific-popup.min.js';
		$this->js['placeholder'] = 'jquery.placeholder.min.js';
		$this->js['global'] = 'global.js';
		$this->css['global'] = 'global.css';
		$this->js['social-share-kit'] = 'social-share-kit.js';
		$this->css['social-share-kit'] = 'social-share-kit.css';
		$this->css['website-icons-embedded'] = 'website-icons-embedded.css';
		$this->css['slick-theme'] = 'slick-theme.css';

		$this->css['slick'] = 'slick.css';
		$this->css['select2'] = 'select2.min.css';
		$this->css['bootstrap'] = 'bootstrap.min.css';
		$this->css['animate'] = 'animate.css';
		$this->css['font-awesome'] = 'https://use.fontawesome.com/releases/v5.7.2/css/all.css';
		$this->css['jquery-ui'] = '//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css';
		$this->css['style-ui'] = 'style.css';

		$this->js['paymentvarifieds'] = 'paymentvarified.js';
		$this->js['paymentvarified'] = 'jquery.payform.min.js';


		$this->js['jquery-uijs'] = '//code.jquery.com/ui/1.12.1/jquery-ui.js';
		$this->js['popperjs'] = 'popper.min.js';
		$this->js['bootstrap.min.js'] = 'bootstrap.min.js';
		$this->js['select2.full.min.js'] = 'select2.full.min.js';
		$this->js['jquery.validate.min.js'] = 'jquery.validate.min.js';
		$this->js['slick.min.js'] = 'slick.min.js';
		$this->js['wow.min.js'] = 'wow.min.js';
		$this->js['custom.js'] = 'custom.js';
		//$this->css['icons-embedded'] = 'icons-embedded.css';
		$this->pageName = $pageName;
		$this->page = new PageContent($this->pageName);

		$this->title = $this->InputSafeString($this->GetParameter('comptitle'));
		if ($title) {
			$this->title = $this->InputSafeString($title);
		} else {
			if ($this->page->details['htmltitle']) {
				$this->title = $this->InputSafeString($this->page->details['htmltitle']);
			}
		}

		if (!$this->holding) {
			$this->SetFrontEndUsers();
		}
		$this->BaseConstructFunctions();
		$this->SetCanonicalLink();
		$this->SetFBMeta();
	} // end of fn __construct, constructor

	protected function SetUTMValues()
	{
		if (isset($_GET['utm_source'])) {
			$_SESSION['utm'] = array();
			foreach ($_GET as $key => $value) {
				if (substr($key, 0, 4) == 'utm_') {
					$_SESSION['utm'][$key] = $value;
				}
			}
		}
	} // end of fn SetUTMValues

	protected function BaseConstructFunctions()
	{
	} // end of fn BaseConstructFunctions

	public function SetCanonicalLink()
	{
		if ($this->page->id) {
			$this->canonical_link = $this->page->Link();
			if (file_exists(CITDOC_ROOT . '/' . CSS_ROOT . 'page_' . $this->pageName . '.css')) {
				$this->css['page_' . $this->pageName] = 'page_' . $this->pageName . '.css';
			}
			if (file_exists(CITDOC_ROOT . '/js/' . ($jsname = 'page_' . $this->pageName . '.js'))) {
				$this->js[$jsname] = $jsname;
			}
		}
	} // end of fn SetCanonicalLink

	function Header()
	{
		$cart = new Cart();
		$basket = $cart->PopUpBasket();
		$crs_search = $HeaderLinks = '';
		if (isset($_GET["crs_search"]) && $_GET["crs_search"] != '') {
			$crs_search = $_GET["crs_search"];
		}

		//<li class="nav-item"><a class="nav-link" data-toggle="modal" data-target="#cr-search-modal" title="search"><i class="icon icon-search"></i></a></li>
		$HeaderLinks = '';
		if ($this->camp_customer->id) {
			$HeaderLinks .= '
         	<li class="nav-item"><a class="nav-link" href="/my-cr-stars/" title="my crstars"><i class="icon icon-user"></i></a></li>
			<li class="nav-item"><a class="nav-link" href="/cr-stars/log-out/" title="log out"><i class="icon icon-logout"></i></a></li>
         	';
		} else {
			$HeaderLinks .= '
         	<li class="nav-item"><a class="nav-link" href="/cr-stars/log-in/" title="log in"><i class="icon icon-login"></i></a></li>';
		}
		echo '<div class="donate-page"><header>
		<nav class="navbar navbar-expand-lg navbar-light">
		   	<a class="navbar-brand" href="', SITE_URL, '"><img src="/img/homeimages/header-logo.png"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <!-- <span class="navbar-toggler-icon"></span> -->
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
		   	<div class="collapse navbar-collapse" id="navbarNav">
		      	<ul class="navbar-nav">
		         
				 <li class="nav-item drop-main">
		            <a class="nav-link main-dropdown-header dropbtn" href="#">What We Do</a>
					  <div class="dropdown-content">
					  	<a href="/post/uyghur-in-turkey/">Uyghur in Turkey</a>
						<a href="/post/sudan/">Eritreans in Sudan</a>
						<a href="/post/rohingya/">Rohingya</a>
						<a href="/post/bangladesh/">Bangladesh Slums</a>
						<a href="/post/pakistan/">Thar Desert Pakistan</a>
					  </div>
		         </li>
				 <li class="nav-item drop-main">
		            <a class="nav-link main-dropdown-header dropbtn" href="#">Get involved</a>
					  <div class="dropdown-content">
					  	<a href="/join-our-team/">Volunteer</a>
						<a href="/careers/">Careers</a>
						<a href="https://tn172-b3d915.pages.infusionsoft.net/">Volunteer Experience</a>
					  </div>
		         </li>
		         <li class="nav-item drop-main">
		            <a class="nav-link main-dropdown-header dropbtn" href="/cr-stars/">Fundraise</a>
					  <div class="dropdown-content">
					    <a href="/cr-stars/log-in/">Start a Campaign</a>
					    <a href="/cr-stars/campaigns/">Find a Campaign</a>
						<a href="/yemen66/">Yemen66</a>
					  </div>
		         </li>
		         <li class="nav-item">
		            <a class="nav-link" href="/posts/blog/">News and views</a>
		         </li>
		         <li class="nav-item drop-main">
						<a class="nav-link main-dropdown-header dropbtn" href="#">About Us</a>
						  <div class="dropdown-content">
							<a href="/why-food/">Why Food</a>
							<a href="/who-we-are/">Who Are We</a>
							<a href="/our-approach/">Our Approach</a>
							<a href="/who-we-are/#trustees">Our People</a>
							<a href="/who-we-are/#contact-us">Contact Us</a>
						  </div>
					 </li><li class="nav-item donate-btn">
		            <a class="nav-link donate-link" href="/donate/">Donate</a>
		         </li>', $HeaderLinks, '
		      	</ul>
		   </div>
		</nav>
	</header>
	<div class="modal fade cr-search-modal" id="cr-search-modal" tabindex="-1" role="dialog" aria-labelledby="bank-aacount-modalLabel" aria-hidden="true">
		  	<div class="modal-dialog" role="document">
			    <div class="modal-content">
			    	<!-- <div class="modal-header">
			    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          		<span aria-hidden="true"><img src="../img/close.png"></span>
		                </button>
			      	</div> -->
			      	<a class="nav-link hrlSearchPopupCloser" data-dismiss="modal"><i class="icon icon-cancel"></i></a>
			      	<div class="modal-body">
			      		<form id="crSearchMainBar" method="get" action="/cr-stars/search/" onsubmit="return CRSearchCanSubmit(\'crSearchMainBar\');">
							<input type="text" class="search-input" placeholder="Search CR Stars Campaigns..." name="crs_search" value="', $crs_search, '" /><button type="submit" class="search-button"><i class="icon icon-search"></i></button>
						</form>
			      	</div>
			    </div>
		 	</div>
		</div>';
		/*echo '<div class="container container_header"><a class="header_logo" href="', SITE_URL, '"></a>
			<div class="container_inner"><div class="header_menuopener hidden-lg"><a href="#kpMMmenu"><i class="icon-menu"></i>&nbsp;</a></div>', $this->HeaderMenu(), '<div class="header_cartopener hidden-lg">', $basket['mobile'], '</div><div class="clear"></div></div><div class="header_desktop_right visible-lg">', $this->HeaderLinks(), $this->HeaderSocialMedia() ,'</div></div>';*/
	} // end of fn Header

	protected function HeaderMenu()
	{
		ob_start();
		$hmenu = new FrontEndMenu();
		$hmenu->GetMenuByName('headermenu');
		if ($menuitems = $hmenu->MenuItems()) {
			echo '<div class="header_mainmenu visible-lg"><ul>';
			foreach ($menuitems as $menuitem) {
				$page = new PageContent($menuitem['item']['pageid']);
				$classes = array();
				if ($this->HeaderMenuIsSelected($page)) {
					$classes[] = 'selected';
				}
				if ($menuitem['item']['menuclass']) {
					$classes[] = $menuitem['item']['menuclass'];
				}
				echo '<li', $classes ? (' class="' . implode(' ', $classes) . '"') : '', '>';
				if ($page->details['headeronly']) {
					echo '<span';
					if ($menuitem['item']['menuclass']) {
						echo ' class="', $menuitem['item']['menuclass'], '"';
					}
					echo '>';
				} else {
					echo '<a href="', $page->Link(), '"';
					if ($menuitem['item']['menuclass']) {	//echo ' class="', $menuitem['item']['menuclass'], '"';
					}
					echo '>';
				}
				echo $this->InputSafeString($menuitem['item']['itemtitle'] ? $menuitem['item']['itemtitle'] : $page->details['pagetitle']), $page->details['headeronly'] ? '</span>' : '</a>';
				if ($menuitem['submenu']) {
					echo '<ul>';
					foreach ($menuitem['submenu'] as $subitem) {
						$subpage = new PageContent($subitem['item']['pageid']);
						$subclasses = array();
						if ($this->HeaderMenuIsSelected($subpage)) {
							$subclasses[] = 'selected';
						}
						echo '<li', $subclasses ? (' class="' . implode(' ', $subclasses) . '"') : '', '><a href="', $subpage->Link(), '">', $this->InputSafeString($subitem['item']['itemtitle'] ? $subitem['item']['itemtitle'] : $subpage->details['pagetitle']), '</a></li>';
					}
					echo '</ul>';
				}
				echo '</li>';
			}
			echo '</ul>';
			echo '<div class="clear"></div></div>';
		}
		$hmenu = new FrontEndMenu();
		$hmenu->GetMenuByName('headermenumobile');
		if ($menuitems = $hmenu->MenuItems()) {	//$this->VarDump($menuitems);
			echo '<div id="kpMMmenu" class="kpMMmenu mm-menu"><ul>';
			echo $this->MobileExtraLinksTop();
			foreach ($menuitems as $menuitem) {
				$page = new PageContent($menuitem['item']['pageid']);
				$classes = array();
				if ($page->details['pagename'] == $this->pageName) {
					$classes[] = 'selected';
				}
				echo '<li', $classes ? (' class="' . implode(' ', $classes) . '"') : '', '>';
				if ($page->details['headeronly']) {
					echo '<span';
					if ($menuitem['item']['menuclass']) {
						echo ' class="', $menuitem['item']['menuclass'], '"';
					}
					echo '>';
				} else {
					echo '<a href="', $page->Link(), '"';
					if ($menuitem['item']['menuclass']) {
						echo ' class="', $menuitem['item']['menuclass'], '"';
					}
					echo '>';
				}
				echo $this->InputSafeString($menuitem['item']['itemtitle'] ? $menuitem['item']['itemtitle'] : $page->details['pagetitle']), $page->details['headeronly'] ? '</span>' : '</a>';
				if ($menuitem['submenu']) {
					echo '<ul>';
					foreach ($menuitem['submenu'] as $subitem) {
						$subpage = new PageContent($subitem['item']['pageid']);
						echo '<li><a href="', $subpage->Link(), '">', $this->InputSafeString($subitem['item']['itemtitle'] ? $subitem['item']['itemtitle'] : $subpage->details['pagetitle']), '</a></li>';
					}
					echo '</ul>';
				}
				echo '</li>';
			}
			echo $this->MobileExtraLinks(), '</ul>';
			echo '</div>';
		}
		return ob_get_clean();
	} // end of fn HeaderMenu

	protected function HeaderMenuIsSelected(PageContent $page)
	{
		if ($page->details['pagename'] == $this->pageName) {
			return true;
		}
	} // end of fn HeaderMenuIsSelected

	protected function MobileExtraLinksTop()
	{
	} // end of fn MobileExtraLinks

	protected function MobileExtraLinks()
	{
	} // end of fn MobileExtraLinks

	protected function HeaderSocialMedia()
	{
		ob_start();

		echo '<div class="headerSocialMedia visible-lg"><ul>';

		if ($facebook_link = $this->GetParameter('facebook_link')) {
			echo '<li><a href="', $facebook_link, '" target="_blank"><i class="icon-facebook"></i></a></li>';
		}
		if ($twitter_link = $this->GetParameter('twitter_link')) {
			echo '<li><a href="', $twitter_link, '" target="_blank"><i class="icon-twitter"></i></a></li>';
		}
		if ($instagram_link = $this->GetParameter('instagram_link')) {
			echo '<li><a href="', $instagram_link, '" target="_blank"><i class="icon-instagram"></i></a></li>';
		}
		if ($youtube_link = $this->GetParameter('youtube_link')) {
			echo '<li><a href="', $youtube_link, '" target="_blank"><i class="icon-youtube"></i></a></li>';
		}
		echo '</ul></div>';
		return ob_get_clean();
	} // end of fn HeaderSocialMedia

	protected function HeaderLinks()
	{
		ob_start();
		echo $this->CartHeaderLink();
		/*if ($_SESSION['events'])
		{	$cart = new BookingCart($_SESSION['events']);
			if ($cart->tickets)
			{	echo $this->EventsHeaderLink();//'<div class="header_rightlinks hidden-xs"><ul><li><a href="', SITE_SUB, '/bookcart.php" title="tickets"><i class="icon icon-ticket"></i></a></li></ul></div>';
			}
		}*/
		return ob_get_clean();
	} // end of fn HeaderLinks

	protected function CartHeaderLink()
	{
		ob_start();
		$cart = new Cart();
		$basket = $cart->PopUpBasket();
		echo '<div class="header_rightlinks hidden-xs"><ul><li class="hrLinksCart">', $basket['desktop'], '</li></ul></div>';
		return ob_get_clean();
	} // end of fn EventsHeaderLink

	protected function EventsHeaderLink()
	{
		ob_start();
		echo '<div class="header_rightlinks hidden-xs"><ul><li><a href="', SITE_SUB, '/bookcart.php" title="tickets"><i class="icon icon-ticket"></i></a></li></ul></div>';
		return ob_get_clean();
	} // end of fn EventsHeaderLink

	protected function RegisterBreadcrumbs($pagelink = '')
	{
		ob_start();
		if ($this->stages) {
			echo '<div class="container register_breadcrumbs"><div class="container_inner"><ul>';
			$width = round(100 / count($this->stages), 2);
			foreach ($this->stages as $id => $stage) {
				echo '<li', $id == $this->stage ? ' class="register_breadcrumbs_current"' : '', ' style="z-index: ', 1000 - $id, '; width: ', $width, '%;">';
				if ($linked = ($stage['linkable'] && ($id < $this->stage) && $pagelink)) {
					echo '<a href="', $pagelink, $id, '/" style="z-index: ', 1000 - $id, ';">';
				} else {
					echo '<div style="z-index: ', 1000 - $id, ';">';
				}
				echo $this->InputSafeString($stage['heading']), '<span></span>', $linked ? '</a>' : '</div>', '</li>';
			}
			echo '</ul><div class="clear" /></div></div></div>';
		}
		return ob_get_clean();
	} // end of fn RegisterBreadcrumbs

	public function DonationBreadcrumbs($pagelink = '')
	{
		ob_start();
		if ($this->stages) {
			echo '<div class="container donation_breadcrumbs"><div class="container_inner"><ul>';
			$width = floor(100 / count($this->stages));
			foreach ($this->stages as $id => $stage) {
				echo '<li', $id == $this->stage ? ' class="donation_breadcrumbs_current"' : '', ' style="z-index: ', 1000 - $id, '; width: ', $width, '%;">';
				if ($linked = ($stage['linkable'] && ($id < $this->stage) && $pagelink)) {
					echo '<a href="', $pagelink, $id, '/" style="z-index: ', 1000 - $id, ';">';
				} else {
					echo '<div style="z-index: ', 1000 - $id, ';">';
				}
				echo $this->InputSafeString($stage['heading']), '<span></span>', $linked ? '</a>' : '</div>', '</li>';
			}
			echo '</ul><div class="clear" /></div></div></div>';
		}
		return ob_get_clean();
	} // end of fn Breadcrumbs

	function FailMessage() //
	{
		if ($this->failmessage) {
			echo '<div class="failmessage"><div class="failmessage_top"></div>', $this->failmessage, '</div>';
		}
	} // end of fn FailMessage

	function SuccessMessage() //
	{
		if ($this->successmessage) {
			echo '<div class="successmessage"><div class="successmessage_top"></div>', $this->successmessage, '</div>';
		}
	} // end of fn SuccessMessage

	function MainBody() //
	{
		$this->Messages();
		$this->MainBodyContent();
	} // end of fn MainBody

	function MainBodyContent()
	{
	} // end of fn MemberBody

	function BodyDefn() // display actual page
	{
		echo "<body";
		if (is_array($this->bodyOnLoadJS) && count($this->bodyOnLoadJS)) {
			echo " onload='";
			foreach ($this->bodyOnLoadJS as $js) {
				echo $js, ";";
			}
			echo "'";
		}
		if (is_array($this->bodyOnUnLoadJS) && count($this->bodyOnUnLoadJS)) {
			echo " onunload='";
			foreach ($this->bodyOnUnLoadJS as $js) {
				echo $js, ";";
			}
			echo "'";
		}
		echo ">\n";
	} // end of fn BodyDefn

	function gtm_body_tag(){
		ob_start();
		echo '<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PX5XJXX"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->';
		return ob_get_clean();
	}

	function FacbookInit()
	{
		ob_start();
		echo '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>
';
		return ob_get_clean();
	} // end of fn FacbookInit

	function Page() // display actual page
	{
		$this->HTMLHeader();
		$this->DisplayTitle();
		$this->BodyDefn();
		echo $this->gtm_body_tag();
		echo $this->MyTenNightsCode(),
			$this->MyTenDaysCode(),
			$this->GoogleTagManagerCode(),
			$this->TwitterUniversalTagCodeSnippet(),
			$this->SnapPixelCode(),
			$this->PintrestTagCode();
		$this->Header();
		$this->MainBody();
		$this->Footer();
		echo $this->ZohoSalesIq();
		//echo $this->FacebookSDKFooter(), $this->LiveChatFooter(), '</body></html>';
		echo $this->FacebookSDKFooter(), $this->adRollCodeSnippet();
		if($this->firstUriSegment() == 'donate-thank-you'){
			$cart_id = $_SESSION[$this->cart_session_name]['orderid'];
			$cartorder = new CartOrder($cart_id);
			echo $cartorder->AdRollPurchaseValueCode();
		}
		echo '</body></html>';
		if($this->firstUriSegment() == 'donate-thank-you' && !SITE_TEST){
			unset($_SESSION[$this->cart_session_name]); 
		}
	} // end of fn Page

	protected function ZohoSalesIq()
	{
		if (strtolower($this->firstUriSegment()) != 'best10') {
			ob_start();
			echo '<!-- Start of LiveChat (www.livechatinc.com) code -->
					<script type="text/javascript">
					var $zoho=$zoho || {};
					$zoho.salesiq = $zoho.salesiq || {widgetcode:"0c59fe0ac7260564ee58d19b0c505caa63977c5c6332851e8b5bd79ee94ad5c2d26184bd399c39c25760b4f3715a450b", 
						values:{},ready:function(){}};
					var d=document;
					s=d.createElement("script");
					s.type="text/javascript";s.id="zsiqscript";
					s.defer=true;
					s.src="https://salesiq.zoho.com/widget";
					t=d.getElementsByTagName("script")[0];
					t.parentNode.insertBefore(s,t);
					d.write("<div id=\'zsiqwidget\'></div>");
					</script>
					<!-- End of LiveChat code -->';
			return ob_get_clean();
		}
	} // end of fn ZohoSalesIq

	protected function LiveChatFooter()
	{
		ob_start();
		echo '<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
  window.__lc = window.__lc || {};
  window.__lc.license = 8879779;
  (function() {
    var lc = document.createElement(\'script\'); lc.type = \'text/javascript\'; lc.async = true;
    lc.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'cdn.livechatinc.com/tracking.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(lc, s);
  })();
</script>
<noscript>
<a href="https://www.livechatinc.com/chat-with/8879779/" rel="nofollow">Chat with us</a>,
powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript>
<!-- End of LiveChat code -->';
		return ob_get_clean();
	} // end of fn LiveChatFooter

	protected function FacebookSDKFooter()
	{
		ob_start();
		echo '<!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script type="text/javascript">(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, "script", "facebook-jssdk"));</script>';
		return ob_get_clean();
	} // end of fn FacebookSDKFooter

	protected function GoogleTagManagerCode()
	{
		ob_start();
		echo '<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PX5XJXX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->';
		return ob_get_clean();
	} // end of fn GoogleTagManagerCode

	protected function PintrestTagCode()
	{
		ob_start();
		$user_email = @$_SESSION[SITE_NAME]['camp_customer_email'];
		$ip = @$_SERVER['REMOTE_ADDR'];
		$em = !empty($user_email) ? md5($user_email) : md5($ip . date('ymd'));
		echo '<!-- Pinterest Tag -->
		<script>
		!function(e){if(!window.pintrk){window.pintrk = function () {
		window.pintrk.queue.push(Array.prototype.slice.call(arguments))};var
		  n=window.pintrk;n.queue=[],n.version="3.0";var
		  t=document.createElement("script");t.async=!0,t.src=e;var
		  r=document.getElementsByTagName("script")[0];
		  r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js");
		pintrk(\'load\', \'2612558765069\', {em: \'', $em, '\'});
		pintrk(\'page\');
		</script>
		<!-- end Pinterest Tag -->';
		return ob_get_clean();
	}

	function Messages()
	{
		if ($this->successmessage || $this->failmessage || $this->warningmessage) {
			if ($this->successmessage) {
				echo '<div class="container alert-container alert-container-success"><div class="container_inner">';
				echo '<div class="alert-message alert-message-success"><div class="successmessage_top"></div>', $this->successmessage, '</div>';
				echo '<a href="#" class="close-alert-container"><i class="icon icon-cancel"></i></a>';
				echo '</div></div>';
			}
			if ($this->failmessage) {
				echo '<div class="container alert-container alert-container-failed"><div class="container_inner">';
				echo '<div class="alert-message alert-message-failed"><div class="failmessage_top"></div>', $this->failmessage, '</div>';
				echo '<a href="#" class="close-alert-container"><i class="icon icon-cancel"></i></a>';
				echo '</div></div>';
			}
			if ($this->warningmessage) {
				echo '<div class="container alert-container alert-container-warning"><div class="container_inner">';
				echo '<div class="alert-message alert-message-warning"><div class="warningmessage_top"></div>', $this->warningmessage, '</div>';
				echo '<a href="#" class="close-alert-container"><i class="icon icon-cancel"></i></a>';
				echo '</div></div>';
			}
			/*
			echo '<div class="container"><div class="container_inner">';
			if ($this->successmessage)
			{	echo '<div class="successmessage"><div class="successmessage_top"></div>', $this->successmessage, '</div>';
			}
			if ($this->failmessage)
			{	echo '<div class="failmessage"><div class="failmessage_top"></div>', $this->failmessage, '</div>';
			}
			if ($this->warningmessage)
			{	echo '<div class="warningmessage"><div class="warningmessage_top"></div>', $this->warningmessage, '</div>';
			}
			echo '</div></div>';
			*/
		}
	} // end of fn Messages

	function GoogleAnalytics()
	{
		ob_start();
		if (!$this->suppress_google_analytics) {
			echo "<!-- Google Analytics -->
			<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-62069931-1', 'auto');
			ga('send', 'pageview');
			</script>
			<!-- End Google Analytics -->";
		}
		return ob_get_clean();
	} // end of fn GoogleAnalytics

	function Footer()
	{
		echo '<footer>
		<div class="container">
			<div class="footer-logo">
				<img src="/img/homeimages/footer-logo.png">
			</div>
			<ul class="main-ul">
		         <li class="footer-li">
		            <a class="nav-link" hr ef="#">Our Work</a>
		            <ul class="footer-inner-ul">
		            	<li><a href="https://charityright.org.uk/posts/projects/">Projects</a></li>
		            	<li><a href="https://charityright.org.uk/posts/stories/">Stories</a></li>
		            </ul>
		         </li>
		         <li class="footer-li">
		            <a class="nav-link" href="#">Get involved</a>
		            <ul class="footer-inner-ul">
		            	<li><a href="https://charityright.org.uk/donate/">Give a regular gift</a></li>
		            	<li><a href="https://charityright.org.uk/events/">Hold an event</a></li>
		            	<li><a href="https://charityright.org.uk/join-our-team/">Become a volunteer</a></li>
		            	<li><a href="https://charityright.org.uk/donate/">Give a one-off gift</a></li>
		            	<li><a href="https://charityright.org.uk/posts/projects/">Leave a legacy</a></li>
		            	<li><a href="https://charityright.org.uk/join-our-team/">Become a partner</a></li>
		            </ul>
		         </li>

		         <li class="footer-li">
		            <a class="nav-link" href="https://charityright.org.uk/cr-stars/">Fundraise</a>
		         </li>
		         <li class="footer-li">
		            <a class="nav-link" href="#">News and views</a>
		         </li>
		         <li class="footer-li">
		            <a class="nav-link" href="#">About us</a>
		            <ul class="footer-inner-ul">
		            	<li><a href="https://charityright.org.uk/who-we-are/">Who We Are</a></li>
		            	<li><a href="https://charityright.org.uk/why-food/">Why Food?</a></li>
		            	<li><a href="https://charityright.org.uk/our-approach/">Our Approach</a></li>
		            	<li><a href="https://charityright.org.uk/refund-policy/">Refund Policy</a></li>
		            	<li><a href="https://charityright.org.uk/use-of-cookies/">Privacy & Cookies</a></li>
		            </ul>
		         </li>
		         <li class="footer-li">
		            <a class="nav-link" href="#">Get in touch</a>
		         </li>
		         <li class="footer-li donate-btn">
		            <a class="nav-link donate-link" href="/donate">Donate</a>
		         </li>
		    </ul>
		    <p class="charity-right">© Charity Right - Registered Charity Number: 1163944</p>
		</div>
	</footer></div>';
		/*$inf_form = new InfusionSoftContactForm();
		if ($this->do_infform_footer)
		{	echo '<div class="container footer_top">
				<div class="container_inner" id="infFormContainer">', $inf_form->Form(), '</div>
			</div>';
		}
		echo '<div class="container footer_middle">
				<div class="container_inner">
					<ul>
						<li class="footerLogo">
							<span><img src="', SITE_SUB, '/img/template/logo_white_footer.png" /></span>',
						//	'<span><img src="', SITE_SUB, '/img/template/give-confidence.png" /></span>',
						'</li>';
		echo '<li><span>Connect</span><ul><li><span>', $this->GetParameter('compphone'), '</span></li><li><a href="mailto:', $email = $this->GetParameter('compemail'), '">', $email, '</a></li></ul></li>';
		$fmenu = new FrontEndMenu();
		$fmenu->GetMenuByName('footermenu');
		if ($menuitems = $fmenu->MenuItems())
		{	$socialmenu = array();
			if ($twitter_link = $this->GetParameter('twitter_link'))
			{	$socialmenu[] = array('link'=>$twitter_link, 'text'=>'Twitter');
			}
			if ($facebook_link = $this->GetParameter('facebook_link'))
			{	$socialmenu[] = array('link'=>$facebook_link, 'text'=>'Facebook');
			}
			if ($instagram_link = $this->GetParameter('instagram_link'))
			{	$socialmenu[] = array('link'=>$instagram_link, 'text'=>'Instagram');
			}
			if ($youtube_link = $this->GetParameter('youtube_link'))
			{	$socialmenu[] = array('link'=>$youtube_link, 'text'=>'YouTube');
			}
			if ($socialmenu)
			{	echo '<li><span>Get to know us</span><ul>';
				foreach ($socialmenu as $socialmenuitem)
				{	echo '<li><a href="', $socialmenuitem['link'], '" target="_blank">', $socialmenuitem['text'], '</a></li>';
				}
				echo '</ul></li>';
			}

			foreach ($menuitems as $menuitem)
			{	$page = new PageContent($menuitem['item']['pageid']);
				echo '<li>';
				if ($page->details['headeronly'])
				{	echo '<span>';
				} else
				{	echo '<a href="', $page->Link(), '">';
				}
				echo $this->InputSafeString($menuitem['item']['itemtitle'] ? $menuitem['item']['itemtitle'] : $page->details['pagetitle']), $page->details['headeronly'] ? '</span>' : '</a>';
				if ($menuitem['submenu'])
				{	echo '<ul>';
					foreach ($menuitem['submenu'] as $subitem)
					{	$subpage = new PageContent($subitem['item']['pageid']);
						echo '<li><a href="', $subpage->Link(), '">', $this->InputSafeString($subitem['item']['itemtitle'] ? $subitem['item']['itemtitle'] : $subpage->details['pagetitle']), '</a></li>';
					}
					echo '</ul>';
				}
				echo '</li>';
			}

		}*/
		/*echo '</ul><div class="clear"></div></div></div><div class="container footer_bottom"><div class="container_inner"><p>&copy; Charity Right - Fighting Hunger, One Child at a Time. Registered Charity Number 1163944</p></div></div>';*/
	} // end of fn Footer

	function DisplayTitle()
	{
		echo "<head prefix='og: https://ogp.me/ns#'>";

		// echo "<base href = \"",(IS_LOCALHOST?'http://localhost/charityright':'/'),"\" />";

		$this->PageSenseCodeSnippet();
		echo "\n<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />\n<meta name='viewport' content='width=device-width, initial-scale=1.0' />\n<title>", SITE_TEST ? "{dev} " : "", $this->title, "</title>\n", $this->MetaTags(), "\n";
		if (SITE_DEMO) echo '<meta name="robots" content="noindex,nofollow" />';
		echo '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="viewport" content="width=device-width, initial-scale=1.0">';
		echo '<link rel="shortcut icon" href="', CIT_FULLLINK, 'favicon.ico"><!--[if IE]><link rel="shortcut icon" href="', CIT_FULLLINK, 'favicon.ico"><![endif]-->';
		$this->CSSInclude();
		$this->JSInclude();
		echo "\n<script>jsSiteRoot='", $this->jsSiteRootValue(), "';</script>\n";
		if ($this->canonical_link) {
			echo '<link rel="canonical" href="', $this->canonical_link, '" />';
		}
		echo '<script type="text/javascript">var switchTo5x=true;</script>', $this->PrivyWidgetCode();

		if (false || !SITE_TEST) {
		}

		echo $this->GoogleAnalytics();
		echo $this->GoogleTagManager(), $this->GoogleAdsSiteTag(), $this->FaceBookPixel();
		echo $this->criteoTrackingCodes();
		if($this->firstUriSegment() == 'donate-thank-you'){
			$cart_id = $_SESSION[$this->cart_session_name]['orderid'];
			$cartorder = new CartOrder($cart_id);
			echo $cartorder->GASingleTransactionTrackingCode();
			echo $cartorder->GAConversionCode();
		}

		echo "</head>\n";
	} // end of fn DisplayTitle
	
	protected function MyTenDaysCode()
	{
		ob_start();
		if (strtolower($this->firstUriSegment()) == 'best10') {
			echo '<mtn-widget-modal charity-id="charity-right-uk" locale="en-GB"></mtn-widget-modal>
	<script src="https://mytendays.com/widget.js" defer></script>';
			echo '<script>
				function openMyTenDays() {
					document.querySelector("mtn-widget-modal").open();
				}
			</script>';
		}
		if (strtolower($this->firstUriSegment()) == 'best10') {
			echo '<script>
			$(function(){
				openMyTenDays();
				$(".page-header").hide();
				$("header").addClass(\'sticky keepsticky\');
			});
			</script>';
		}
		return ob_get_clean();
	} // end of fn MyTenNightsCode

	protected function MyTenNightsCode()
	{
		ob_start();
		if (strtolower($this->firstUriSegment()) == 'mtn' || strtolower($this->firstUriSegment()) == 'mytennights') {
			echo '<mtn-widget-modal charity-id="charity-right-uk" locale="en-GB"></mtn-widget-modal>
	<script src="https://www.mytennights.com/widget/script.js" defer></script>';
			echo '<script>
				function openMyTenNights() {
					document.querySelector("mtn-widget-modal").open();
				}
			</script>';
		}
		if (strtolower($this->firstUriSegment()) == 'mtn') {
			echo '<script>
			$(function(){
				openMyTenNights();
				$(".page-header").hide();
				$("header").addClass(\'sticky keepsticky\');
			});
			</script>';
		}
		return ob_get_clean();
	} // end of fn MyTenNightsCode

	public function UriSegments()
	{
		return explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
	}

	public function firstUriSegment()
	{
		$uriSegments = $this->UriSegments();
		return @$uriSegments['1'];
	}

	protected function GoogleAdsSiteTag()
	{
		ob_start();
		echo "<!-- Global site tag (gtag.js) - Google Ads: 925998688 -->
		<script async src=\"https://www.googletagmanager.com/gtag/js?id=AW-925998688\"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		
		  gtag('config', 'AW-925998688');
		</script>
		<!-- Global site tag (gtag.js) - Google Ads: 876060108 -->
		<script async src=\"https://www.googletagmanager.com/gtag/js?id=AW-876060108\"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'AW-876060108');
		</script>
		";
		return ob_get_clean();
	}

	protected function GoogleAdsConversionTag()
	{
		ob_start();
		if (isset($_GET['token']) && $this->firstUriSegment() == 'donate-thank-you') {

			@$donation_values = json_decode(base64_decode($_GET['token']), true);
			if (isset($donation_values['amount']) && isset($donation_values['currency'])) {
				// echo "<!-- Event snippet for Completed Donation conversion page -->
				// 		<script>
				// 		gtag('event', 'conversion', {
				// 		'send_to': 'AW-925998688/rJJRCO2R530Q4LzGuQM',
				// 		'value': " . $donation_values['amount'] . ",
				// 		'currency': '" . $donation_values['currency'] . "',
				// 		'transaction_id': '" . (SITE_TEST ? 'testid' . rand(0, 467) : "") . "'
				// 		});
				// 		</script>";
			}
		}
		return ob_get_clean();
	}

	protected function criteoTrackingCodes()
	{

		$request_uri = ltrim(rtrim($_SERVER['REQUEST_URI'], '/'), '/');

		$criteo_preengag_snippet = '<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true">   </script>
				<script type="text/javascript">
				window.criteo_q = window.criteo_q || [];
				var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
				window.criteo_q.push(
				 { event: "setAccount", account: 70936 },
				 { event: "setSiteType", type: deviceType },
				 { event: "viewList", item: ["1"] }
				);
				</script>';

		$criteo_homepage_snippet = '<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true">   </script>
									<script type="text/javascript">
									window.criteo_q = window.criteo_q || [];
									var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
									window.criteo_q.push(
										{ event: "setAccount", account: 70936 },
										{ event: "setSiteType", type: deviceType },
										{ event: "viewHome" },
										{event: "viewItem", item: "1"}
									);
									</script>';


		ob_start();

		if ($this->firstUriSegment() == 'ramadan2020') {
			echo $criteo_homepage_snippet;
		}

		switch ($request_uri) {
			case 'post/uyghur-in-turkey':
			case 'post/sudan':
			case 'post/rohingya':
			case 'post/bangladesh':
			case 'post/pakistan':
				echo $criteo_preengag_snippet;
				break;


			case 'join-our-team':
			case 'careers':
			case 'why-food':
			case 'who-we-are':
			case 'who-we-are/#trustees':
			case 'our-approach':
			case 'posts/blog':
				echo $criteo_homepage_snippet;
				break;

			case 'donate':
				echo '<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true">   </script>
				<script type="text/javascript">
				window.criteo_q = window.criteo_q || [];
				var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
				window.criteo_q.push(
				 { event: "setAccount", account: 70936 },
				 { event: "setSiteType", type: deviceType },
				 { event: "viewItem", item: "1" }
				);
				</script>';
				break;

			case 'donate-thank-you':
				break;
			default:

				break;
		}

		if ($this->firstUriSegment() == 'donate-thank-you') {
			$cart_id = $_SESSION[$this->cart_session_name]['orderid'];
			$cartorder = new CartOrder($cart_id);

			$donation_totals = $cartorder->getSessionDonationForAnalytics();
			$donoremail = @$cartorder->details['donoremail'];

			if (SITE_TEST) {
				// var_dump($_SESSION);
				// print_r($donation_totals);
				// echo 'BASE_PAGEHEAD';
			}

			echo '<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true">   </script>
			<script type="text/javascript">
			window.criteo_q = window.criteo_q || [];
			var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
			window.criteo_q.push(
				{ event: "setAccount", account: 70936 },
				{ event: "setSiteType", type: deviceType },
				{ event: "setEmail", email: "', $donoremail, '" },
				{ event: "trackTransaction", id: "', $donation_totals['donation_id'], '", item: [
				{ id: "1", price: "', $donation_totals['amount'], '", quantity: ', $donation_totals['quantity'], ' } ]}
			);
			</script>';
		}

		return ob_get_clean();
	}

	protected function SnapPixelCode()
	{
		ob_start();
		$user_email = @$_SESSION[SITE_NAME]['camp_customer_email'];
		echo "<!-- Snap Pixel Code -->
		<script type='text/javascript'>
		(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
		{a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
		a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
		r.src=n;var u=t.getElementsByTagName(s)[0];
		u.parentNode.insertBefore(r,u);})(window,document,
		'https://sc-static.net/scevent.min.js');
	
		snaptr('init', 'e3fc8fa5-d8f9-47b6-a384-1859c44e5594', {
		'user_email': '", $user_email, "',
		'user_hashed_email': '", hash('sha256', $user_email), "',
		});
	
		snaptr('track', 'PAGE_VIEW');
	
		</script>
		<!-- End Snap Pixel Code -->";
		return ob_get_clean();
	}

	protected function TwitterUniversalTagCodeSnippet()
	{
		ob_start();
		echo "<!-- Twitter universal website tag code -->
		<script>
		!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
		},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
		a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
		// Insert Twitter Pixel ID and Standard Event data below
		twq('init','nv0yr');
		twq('track','PageView');
		</script>
		<!-- End Twitter universal website tag code -->";
		return ob_get_clean();
	}

	protected function adRollCodeSnippet()
	{
		ob_start();
		echo '<script type="text/javascript">
		adroll_adv_id = "TYFC5JMQS5FG5MGYQ64EYJ";
		adroll_pix_id = "2UNEFFNLRZFZVBQT2S4OBV";
	
		(function () {
			var _onload = function(){
				if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
				if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
				var scr = document.createElement("script");
				var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
				scr.setAttribute(\'async\', \'true\');
				scr.type = "text/javascript";
				scr.src = host + "/j/roundtrip.js";
				((document.getElementsByTagName(\'head\') || [null])[0] ||
					document.getElementsByTagName(\'script\')[0].parentNode).appendChild(scr);
			};
			if (window.addEventListener) {window.addEventListener(\'load\', _onload, false);}
			else {window.attachEvent(\'onload\', _onload)}
		}());
	</script>';

		return ob_get_clean();
	}
	protected function FaceBookPixel()
	{
		ob_start();
		$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
		$thank_you_page = @$uriSegments['1'];
		//echo $thank_you_page;
		echo '';
		return ob_get_clean();
	} // end of fn FaceBookPixel


	protected function PageSenseCodeSnippet()
	{	//ob_start();
		// to be included right after <head> start tag.
		echo '<script src="https://cdn-eu.pagesense.io/js/biggorillaapps/c86fcb9d0f1d4e45bec7b8eda31a04f7.js"></script>';
		//return ob_get_clean();
	} // end of fn PageSenseCodeSnippet


	protected function PrivyWidgetCode()
	{
		ob_start();
		if (!SITE_TEST) {
			echo '
<!-- BEGIN PRIVY WIDGET CODE -->
<script type="text/javascript"> var _d_site = _d_site || "0041993206B9F42E67E17825"; </script>
<script src="//widget.privy.com/assets/widget.js"></script>
<!-- END PRIVY WIDGET CODE -->
';
		}
		return ob_get_clean();
	} // end of fn PrivyWidgetCode

	protected function SetFBMeta()
	{
		$this->fb_meta['title'] = $this->InputSafeString($this->page->details['htmltitle']);
		$this->fb_meta['type'] = 'website';
		$this->fb_meta['image'] = CIT_FULLLINK . 'img/template/logo_og.png';
		$this->fb_meta['url'] = $this->page->Link();
		if ($this->page->details['metadesc']) {
			$this->fb_meta['description'] = $this->InputSafeString($this->page->details['metadesc']);
		} else {
			$this->fb_meta['description'] = 'Charity Right is an international food programme providing life-changing school meals to the most vulnerable children in the world. Our vision is to see every child receiving a nutritious school meal in every place of education.';
		}
	} // end of fn SetFBMeta

	protected function GoogleTagManager()
	{
		ob_start();

		$cart_id = $_SESSION[$this->cart_session_name]['orderid'];
		$cartorder = new CartOrder($cart_id);
		$donation_totals = $cartorder->getSessionDonationForAnalytics();

		// if(!empty($donation_totals['currency']) && $this->firstUriSegment() == 'donate-thank-you'){
?>
		<script>
		dataLayer = [{
			'fbq_purchase_value': '<?=$donation_totals['amount']?>',
			'fbq_purchase_currency': '<?=$donation_totals['currency']?>'
		}];
		</script>
<?php 	
		// } 	
?>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PX5XJXX');</script>
		<!-- End Google Tag Manager -->

		<script type='text/javascript'>
			window.__lo_site_id = 98686;

			(function() {
				var wa = document.createElement('script');
				wa.type = 'text/javascript';
				wa.async = true;
				wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wa, s);
			})();
		</script>
<?php
		return ob_get_clean();
	} // end of fn GoogleTagManager

	protected function jsSiteRootValue()
	{
		return SITE_SUB . '/';
	} // end of fn jsSiteRootValue

	public function MetaTags()
	{
		ob_start();
		if ($this->page->details['metakey']) {
			echo '<meta name="KEYWORDS" content="', $this->InputSafeString($this->page->details['metakey']), '" />';
		}
		echo '<meta name="DESCRIPTION" content="';
		if ($metadesc = $this->GetMetaDesc()) {
			echo $this->InputSafeString($metadesc);
		} else {
			echo $this->InputSafeString($this->GetParameter('defaultmeta')); //'default meta description for charity right';
		}
		echo '" />';
		if ($this->fb_meta) {
			foreach ($this->fb_meta as $property => $content) {
				if (is_array($content)) {
					foreach ($content as $content_element) {
						echo '<meta property="og:', $property, '" content="', $this->InputSafeString($content_element), '"  />
					';
					}
				} else {
					echo '<meta property="og:', $property, '" content="', $this->InputSafeString($content), '"  />
			';
				}
			}
		}
		echo '<meta name="theme-color" content="#ec277a">';
		return ob_get_clean();
	} // end of fn MetaTags

	public function GetMetaDesc()
	{
		return $this->page->details['metadesc'];
	} // end of fn GetMetaDesc

	function Redirect($url = '')
	{
		header('location: ' . SITE_SUB . '/' . $url);
		exit;
	} // end of fn Redirect

	private function LoginForm()
	{
		echo '<form id="customer-login" action="', $_SERVER['REQUEST_URI'], '" method="post" class="login form"><h3>Existing Customer Login</h3><p><label>Email:</label><input type="email" name="username" size="40" /><div class="clear"></div></p><p><label>Password:</label><input type="password" name="pass" /><div class="clear"></div></p><p><label>&nbsp;</label><input type="submit" class="submit" value="Log in" /><div class="clear"></div></p><p><a href="forgotten.php">Forgot your password?</a></p></form>';
	} // end of fn BHDLoginForm

	protected function MainBodyContentHeader()
	{
		ob_start();
		if (!$this->page->details['noheader']) {
			if ($this->page->details['headertext']) {
				echo '<div class="container page-header"';
				if ($this->page->HasBanner() && ($banner_src = $this->page->GetBannerSRC())) {
					echo ' style="background-image: url(\'', $banner_src, '\');';
					if ($this->page->details['bannerlink']) {
						echo 'cursor: pointer;"  onclick="window.location.replace(\'', $this->InputSafeString($this->page->details['bannerlink']), '\');';
					}
					echo '"';
				}
				echo '><div class="container_inner">', stripslashes($this->page->details['headertext']), '</div></div>';
			} else {
				echo '<div class="container page-header';
				if ($this->page->HasBanner() && ($banner_src = $this->page->GetBannerSRC())) {
					echo '" style="background-image: url(\'', $banner_src, '\');';
				}
				echo '"><div class="container_inner"><h1 class="page-header-title">', $this->page->PageTitleDisplay(), '</h1></div></div>';
			}
		}
		return ob_get_clean();
	} // end of fn MainBodyContentHeader

	protected function MainBodyContentPageSections()
	{
		ob_start();
		if (is_a($this->page, 'PageContent') && ($sections = $this->page->PageSections())) {	//$this->VarDump($sections);
			foreach ($sections as $section_row) {
				$section = new PageSection($section_row);
				switch ($section->details['psclass']) {
					case 'index_third':
					case 'index_fourth':
						//case 'index_fourth index_fourth_db':
						//case 'index_fourth index_fourth_or':
						$section_parts = array();
						echo '<div class="container ', $this->InputSafeString($section->details['psclass']), '"';
						if ($section->details['bgcolour']) {
							echo ' style="background-color: #', $section->details['bgcolour'], '"';
						}
						echo '><div class="container_inner container_inner_half">';
						ob_start();
						echo '<div class="container_inner_half_left container_inner_half_bg"';
						if ($section->HasImage()) {
							echo ' style="background-image: url(\'', $section->GetImageSRC(), '\');"';
						}
						echo '><div class="container_inner_half_content">&nbsp;</div></div>';
						$section_parts[] = ob_get_clean();
						ob_start();
						echo '<div class="container_inner_half_right"><div class="container_inner_half_content">', stripslashes($section->details['pstext']), '</div></div>';
						$section_parts[] = ob_get_clean();
						switch ($section->details['pstextpos']) {
							case 'L':
								echo implode('', array_reverse($section_parts));
								break;
							case 'R':
							default:
								echo implode('', $section_parts);
								break;
						}
						echo '<div class="clear"></div></div></div>';
						break;
					case 'index_fifth':
						echo '<div class="container ', $this->InputSafeString($section->details['psclass']), '"';
						if ($section->HasImage()) {
							echo ' style="background-image: url(\'', $section->GetImageSRC(), '\'); background-size: cover;"';
						}
						echo '><div class="container_inner">', stripslashes($section->details['pstext']), '</div></div>';
						break;
					case 'page-section-horizontal-container':
						echo '<div class="container page-section ', $this->InputSafeString($section->details['psclass']), '" style="background-color: #', $section->details['bgcolour'], ';"><div class="container_inner">';
						foreach ($section->SubSections() as $subsection_row) {
							$subsection = new PageSection($subsection_row);
							switch ($subsection_row['psclass']) {
								case 'page-section-horizontal_half_embed':
									echo '<div class="', $this->InputSafeString($subsection_row['psclass']), '" style="background-color: #', $subsection_row['bgcolour'], ';';
									if ($do_image = (!strstr($subsection_row['psclass'], '-water') && $subsection->HasImage())) {
										echo ' background-image: url(\'', $subsection->GetImageSRC(), '\');';
									}
									echo '">',
										stripslashes($subsection_row['pstext']),
										'</div>';
									break;
								default:
									$subsection_row['pstext'] = str_replace('<p><a ', '<p><a style="background: #' . $subsection_row['buttonbg'] . '; color: #' . $subsection_row['buttontext'] . ';" ', $subsection_row['pstext']);
									echo '<div class="', $this->InputSafeString($subsection_row['psclass']), '" style="background-color: #', $subsection_row['bgcolour'], '; color: #', $subsection_row['textcolour'], ';">';
									if ($do_image = (!strstr($subsection_row['psclass'], '-water') && $subsection->HasImage())) {
										echo '<div class="pshcImage" style="background-image: url(\'', $subsection->GetImageSRC(), '\');"></div>';
									}
									echo '<div class="pshc', $do_image ? 'Text' : 'Full', '">', stripslashes($subsection_row['pstext']), '</div></div>';
							}
						}
						echo '<div class="clear"></div></div></div>';
						break;
					case 'page-section-inf_form':
						$inf_form = new InfusionSoftContactForm();
						echo '<div class="container page-section ', $this->InputSafeString($section->details['psclass']), '" style="background-color: #', $section->details['bgcolour'], '; color: #', $section->details['textcolour'], ';"><div class="container_inner" id="infFormContainer">', stripslashes($section->details['pstext']), $inf_form->MinimalSectionForm(), '</div></div>';
						break;
					case 'page-section-narrow_plain':
						echo '<div class="container page-section ', $this->InputSafeString($section->details['psclass']), '" style="background-color: #', $section->details['bgcolour'], '; color: #', $section->details['textcolour'], ';"><div class="container_inner">', stripslashes($section->details['pstext']), '</div></div>';
						break;
					default:
						echo '<div class="container page-section ', $this->InputSafeString($section->details['psclass']), '"';
						if ($section->HasImage()) {
							echo ' style="background-image: url(\'', $section->GetImageSRC(), '\'); background-size: cover;"';
						}
						echo '><div class="container_inner">', stripslashes($section->details['pstext']), '</div></div>';
				}
			}
		}
		return ob_get_clean();
	} // end of fn MainBodyContentPageSections

	function MainBodyQuickDonateForm()
	{
		ob_start();
		if ($this->quickdonate) {
			$types = array('oneoff' => 'Single donation', 'monthly' => 'Monthly donation');
			// check for valid types ...
			if (($country = new DonationOption((int) $this->quickdonate['country'])) && $country->id) {
				if (!$country->details['oneoff'] || !$country->details['monthly']) {
					if ($country->details['oneoff']) {
						$set_type = 'oneoff';
					} else {
						if ($country->details['monthly']) {
							$set_type = 'monthly';
						}
					}
				}
			}
			echo '<div class="container pageContentQuickDonate"><div class="container_inner">';
			//$this->VarDump($this->quickdonate);
			echo '<form onsubmit="QDFormSubmit(); return false;">
				<input type="hidden" id="qd_country" value="', (int) $this->quickdonate['country'], '" />
				<input type="hidden" id="qd_project" value="', (int) $this->quickdonate['project'], '" />
				<input type="hidden" id="qd_zakat" value="', (int) $this->quickdonate['zakat'], '" />
				<input type="hidden" id="qd_quick" value="', (int) $this->quickdonate['quick'], '" />';
			if (false && $set_type) {
				echo '<input type="hidden" id="qd_type" value="', $set_type, '" />';
			} else {
				echo '<div class="qdFormElement infusion-field infusion-field-select qdFormElementType"><i class="select-arrow icon icon-angle-down">&nbsp;</i><select id="qd_type" onchange="QDTypeChange();" class="infusion-field infusion-field-input-container">';
				foreach ($types as $key => $text) {
					echo '<option value="', $key, '"', $key == $this->quickdonate['type'] ? ' selected="selected"' : '', '>', $text, '</option>';
				}
				echo '</select></div>';
			}

			echo '<div class="qdFormAmountContainer"></div>
				<div class="qdFormElement qdFormElementCurSymbol"><div class="qdFormCurSymbol">', $this->GetCurrency($this->quickdonate['currency'], 'cursymbol'), '</div></div>
				<div class="qdFormElement infusion-field infusion-field-input"><input type="text" class="qdFormAmount infusion-field-input-container" id="qd_amount" value="', $this->quickdonate['amount'] ? (int) $this->quickdonate['amount'] : '', '" placeholder="Amount" /></div>
				<div class="qdFormElement qdFormElementDividerContainer"><div class="qdFormElementDivider"></div></div>
				<div class="qdFormElement qdFormElementCurrency infusion-field infusion-field-select">
					<i class="select-arrow icon icon-angle-down">&nbsp;</i><select id="qd_currency" class="infusion-field infusion-field-input-container" onchange="QDCurrencyChange();">';
			foreach ($this->GetQDCurrencies($this->quickdonate['type']) as $curcode => $currency) {
				echo '<option value="', $curcode, '"', $curcode == $this->quickdonate['currency'] ? ' selected="selected"' : '', '>', $curcode, '</option>';
			}
			echo '</select></div>
				<div class="clear visible-xs"></div>
				<div class="qdFormElement infusion-submit infusion-field-submit"><input type="submit" value="Quick Donate" /></div><div class="clear"></div></form></div></div>';
		}
		return ob_get_clean();
	} // end of fn MainBodyQuickDonateForm

} // end of defn BasePage
?>