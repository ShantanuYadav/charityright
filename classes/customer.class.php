<?php
class Customer extends Base
{	
	public $id = 0;
	public $details = array();
	public $regfail = "";
	
	public function __construct($id = 0)
	{	parent::__construct();
		$this->Get($id);
	} //  end of fn __construct
	
	public function Reset()
	{	$this->id = 0;
		$this->details = array();
	} // end of fn Reset
	
	public function Get($id = 0)
	{	$this->Reset();
		if (is_array($id))
		{	$this->details = $id;
			$this->id = $id['custid'];
			
		} else
		{	if ($id = (int)$id)
			{	$sql = 'SELECT * FROM customers WHERE custid=' . $id;
				if ($result = $this->db->Query($sql))
				{	if ($row = $this->db->FetchArray($result))
					{	return $this->Get($row);
					}
				}
			}
		}
		
	} // end of fn Get
	
	public function Create($data = array())
	{	
		$fail = array();
		$fields = array();
		
		if ($this->ValidEMail($data['regemail']))
		{	//check for duplicate
			if ($result = $this->db->Query('SELECT * FROM customers WHERE email="' . $data['regemail'] . '"'))
			{	$emailused = $this->db->NumRows($result);
			}
			if ($emailused)
			{	$fail[] = 'email address has already been used';
			} else
			{	$fields[] = 'email="' . $data['regemail'] . '"';
			}
		} else
		{	$fail[] = 'invalid email address given';
		}
		
		if ($_POST['regpass'] !== $_POST['regretype'])
		{	$fail[] = 'password mistyped';
		} else
		{	if ($this->AcceptablePW($_POST['regpass']))
			{	$fields[] = 'pword=MD5("' . $_POST['regpass'] . '")';
			} else
			{	$fail[]= 'password must be 8 to 12 letters or numbers';
			}
		}
		
		$now = $this->datefn->SQLDateTime();
		$fields[] = 'regdate="' . $now . '"';
		
		if ((!$this->regfail = implode(', ', $fail)) && ($set = implode(', ', $fields)))
		{	$sql = 'INSERT INTO customers SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && $id = $this->db->InsertID())
				{	$this->Get($id);
					return $this->id;
				} else
				{	$fail[] = 'registration failed';
				}
			}
		}
		
		$this->failmessage = implode(', ', $fail);
		
	} // end of fn Create
	
	public function SavePassword($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($_POST['password'] !== $_POST['rtpassword'])
		{	$fail[] = 'password mistyped';
		} else
		{	if ($this->AcceptablePW($_POST['password']))
			{	$fields[] = 'pword=MD5("' . $_POST['password'] . '")';
			} else
			{	$fail[]= 'password must be 8 to 12 letters or numbers';
			}
		}
		
		if (!$fail && ($set = implode(', ', $fields)))
		{	$sql = 'UPDATE customers SET ' . $set . ' WHERE custid=' . (int)$this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$success[] = 'Your password has been changed';
				}
			} else echo '<p>', $this->db->Error(), '</p>';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn SavePassword
	
	public function PasswordForm()
	{	ob_start();
		echo '<form id="pay-form" class="payform form" method="post" action="', SITE_URL, 'mypassword.php"><h3>Change your password</h3><p class="clearfix"><label>New password:</label><input type="password" name="password" class="pftext" /></p><p class="clearfix"><label>Please retype:</label><input type="password" name="rtpassword" class="pftext" /></p><p class="clearfix"><label>&nbsp;</label><input type="submit" class="submit" value="Save password" /></p><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn PasswordForm
	
	public function OrdersTable()
	{	ob_start();
		$orders = new SCOrders($this);
		if ($orders->orders)
		{	$attributes = array();
			echo '<table class="ordersTable"><tr><th>Order number</th><th>Date</th><th>Item</th><th>&nbsp;</th><th>Price</th><th>Delivery</th><th>Total</th><th></th></tr>';
			foreach ($orders->orders as $order)
			{	$ordertotal = 0;
				$itemcount = 0;
				$extra_items_text = '';
				if (!$rowspan = count($order->items) + count($order->plates))
				{	$rowspan = 1;
				}
				echo '<tr class="orderHeader"><td class="ot_ordernum" rowspan="', $rowspan, '">', $order->OrderNumber(), '</td><td class="ot_date" rowspan="', $rowspan, '">', date('d-m-y', strtotime($order->details['orderdate'])), '</td>';
				foreach ($order->plates as $pid=>$plate)
				{	$ordertotal += $plate['price'];
					ob_start();
					echo '<td class="ot_plate" colspan="2">';
					$plate_vars = $order->PlateBuilderVars($plate);
					$plates = array();

					// Determine which plates to build
					if(isset($plate_vars['type']))
					{	if($plate_vars['type'] == 'both')
						{	$plates[] = 'front';
							$plates[] = 'rear';
						} else
						{	$plates[] = $plate_vars['type'];
						}
					}

					if(sizeof($plates))
					{	foreach($plates as $key => $current_plate)
						{	echo '<img width="100px" src="', SITE_URL, $order->PlateImageFilename($pid, $current_plate), '?', time(), '" />';
						}
					}
					echo '</td><td class="ot_price">&pound;', number_format($plate['price'], 2), '</td>';
					$itemline = ob_get_clean();
					if ($itemcount++)
					{	$extra_items_text .= '<tr>' . $itemline . '</tr>';
					} else
					{	echo $itemline;
					}
				}
				foreach ($order->items as $item)
				{	$product = $order->ItemProductDetails($item);
					$ordertotal += $itemprice = $item['price'] * $item['quantity'];
					ob_start();
					echo '<td class="ot_product"><h4>', $this->InputSafeString($product->details['prodname']), '</h4>';
					if ($item['avid'])
					{	foreach ($item['avid'] as $avid)
						{	$att_value = new ProductAttributeValue($avid);
							if (!$attributes[$att_value->details['attid']])
							{	$attributes[$att_value->details['attid']] = new ProductAttribute($att_value->details['attid']);
							}
							echo '<p><label>', $this->InputSafeString($attributes[$att_value->details['attid']]->details['attname']), '</label>: ', $this->InputSafeString($att_value->details['avname']), '</p>';
						}
					}
					if ($product->details['addinfo'] && $item['addinfo'])
					{	echo '<p><label>', $this->InputSafeString($product->details['addinfo']), '</label>: ', $this->InputSafeString($item['addinfo']), '</p>';
					}
					echo '</td><td class="ot_qty">', $item['quantity'], ' @ &pound;', number_format($item['price'], 2), ' = </td><td class="ot_price">&pound;', number_format($itemprice, 2), '</td>';
					$itemline = ob_get_clean();
					if ($itemcount++)
					{	$extra_items_text .= '<tr>' . $itemline . '</tr>';
					} else
					{	echo $itemline;
					}
				}
				$ordertotal += $order->details['delprice'];
				echo '<td class="ot_del" rowspan="', $rowspan, '">&pound;', number_format($order->details['delprice'], 2), '</td><td class="ot_total" rowspan="', $rowspan, '">&pound;', number_format($ordertotal, 2), '</td><td class="ot_actions" rowspan="', $rowspan, '"><a href="', SITE_URL, 'myorder.php?orderid=', $order->id, '">view</a></td></tr>', $extra_items_text;
			}
			echo '</table>';
		} else
		{	echo '<h3>You have made no Klick Plates purchases yet</h3>';
		}
		return ob_get_clean();
	} // end of fn OrdersTable
	
	public function OrdersList()
	{	ob_start();
		$orders = new SCOrders($this);
		if ($orders->orders)
		{	$attributes = array();
			echo '<div class="ma_orders_row ma_orders_rowheader clearfix"><div class="ma_orders_order">Order</div><div class="ma_orders_date">Date</div><div class="ma_orders_items">Item</div><div class="ma_orders_price">Price</div><div class="ma_orders_del">Delivery</div><div class="ma_orders_total">Total</div><div class="ma_orders_actions">&nbsp;</div></div>';
			foreach ($orders->orders as $order)
			{	$itemcount = 0;
				$extra_items_text = '';
				if (!$rowspan = count($order->items) + count($order->plates))
				{	$rowspan = 1;
				}
				echo '<div class="ma_orders_row clearfix"><div class="ma_orders_order">', $order->OrderNumber(), '<div class="hidden-lg">', date('d-m-y', strtotime($order->details['orderdate'])), '</div></div><div class="ma_orders_date">', date('d-m-y', strtotime($order->details['orderdate'])), '</div><div class="ma_orders_items">';
				foreach ($order->plates as $pid=>$plate)
				{	$plate_vars = $order->PlateBuilderVars($plate);
					$plates = array();

					// Determine which plates to build
					if(isset($plate_vars['type']))
					{	if($plate_vars['type'] == 'both')
						{	$plates[] = 'front';
							$plates[] = 'rear';
						} else
						{	$plates[] = $plate_vars['type'];
						}
					}

					echo '<div class="ma_orders_item_row clearfix"><div class="ma_orders_item_plate">';
					if(sizeof($plates))
					{	foreach($plates as $key => $current_plate)
						{	echo '<img width="50%" src="', SITE_URL, $order->PlateImageFilename($pid, $current_plate), '?', time(), '" />';
						}
					}
					echo '</div><div class="ma_orders_item_price">&pound;', number_format($plate['price'], 2), '</div></div>';
				}
				foreach ($order->items as $item)
				{	$product = $order->ItemProductDetails($item);
					echo '<div class="ma_orders_item_row clearfix"><div class="ma_orders_item_desc"><h4>', $this->InputSafeString($product->details['prodname']), '</h4>';
					if ($item['avid'])
					{	foreach ($item['avid'] as $avid)
						{	$att_value = new ProductAttributeValue($avid);
							if (!$attributes[$att_value->details['attid']])
							{	$attributes[$att_value->details['attid']] = new ProductAttribute($att_value->details['attid']);
							}
							echo '<p><label>', $this->InputSafeString($attributes[$att_value->details['attid']]->details['attname']), '</label>: ', $this->InputSafeString($att_value->details['avname']), '</p>';
						}
					}
					if ($product->details['addinfo'] && $item['addinfo'])
					{	echo '<p><label>', $this->InputSafeString($product->details['addinfo']), '</label>: ', $this->InputSafeString($item['addinfo']), '</p>';
					}
					echo '</div><div class="ma_orders_item_qty">', $item['quantity'], ' @ &pound;', number_format($item['price'], 2), ' = </div><div class="ma_orders_item_price">&pound;', number_format($item['quantity'] * $item['price'], 2), '</div></div>';
				}
				if ($discamount = $order->DiscountAmount())
				{	echo '<div class="ma_orders_item_row clearfix"><div class="ma_orders_item_desc"><h4>Discount</h4></div><div class="ma_orders_item_qty">', $this->InputSafeString($order->details['disccode']), '</div><div class="ma_orders_item_price">&pound;', number_format($discamount, 2), '</div></div>';
				}
				echo '</div>'; // end of items container
				echo '<div class="ma_orders_del">&pound;', number_format($order->details['delprice'], 2), '</div><div class="ma_orders_total">&pound;', number_format($ordertotal = $order->TotalPrice(), 2), '</div><div class="ma_orders_actions"><a href="', SITE_URL, 'myorder.php?orderid=', $order->id, '">view</a></div></div>';
			}
		} else
		{	echo '<h3>You have made no purchases yet</h3>';
		}
		return ob_get_clean();
	} // end of fn OrdersList

	public function DeliveryAddressForm($header = 'Delivery Address', $submit_value = 'Use this address', $password = false)
	{	ob_start();
		echo '<h2 class="heading">', $this->InputSafeString($header), '</h2><form id="pay-form" class="payform form" method="post" action="', $_SERVER["SCRIPT_NAME"], '"><p class="clearfix"><label>Full name:</label><input type="text" name="fullname" class="pftext" value="', $this->InputSafeString($this->details['fullname']), '" /></p>', $password ? '<p class="clearfix"><label>Password (if changing):</label><input type="password" name="chpass" /></p><div class="clear"></div><p class="clearfix"><label>Retype password:</label><input type="password" name="rtchpass" /></p>' : '', '<p class="clearfix"><label>Street:</label><input type="text" name="addr1" class="pftext" value="', $this->InputSafeString($this->details['addr1']), '" /></p><p class="clearfix"><label class="hidden-xs">&nbsp;</label><input type="text" name="addr2" class="pftext" value="', $this->InputSafeString($this->details['addr2']), '" /></p><p class="clearfix"><label>Town / City:</label><input type="text" name="city" class="pftext" value="', $this->InputSafeString($this->details['city']), '" /></p><p class="clearfix"><label>County:</label><input type="text" name="county" class="pftext" value="', $this->InputSafeString($this->details['county']), '" /></p><p class="clearfix"><label>Postcode:</label><input type="text" name="postcode" class="detinput" value="', $this->InputSafeString($this->details['postcode']), '" /></p>';
		if ($countries = $this->CountryList(false))
		{	if (count($countries) > 1)
			{
				echo '<p class="clearfix"><label>Country:</label><select name="countrycode"><option value="">-- choose --</option>';
				foreach($countries as $ccode=>$cname)
				{	echo '<option value="', $ccode, '"', $ccode == $this->details['countrycode'] ? ' selected="selected"' : '', '>', $this->InputSafeString($cname), '</option>';
				}
				echo '</select></p>';
			} else
			{	foreach($countries as $ccode=>$cname)
				{	
					echo '<p class="clearfix"><label>Country:</label><input type="hidden" name="countrycode" value="', $ccode, '"><span class="soleCtry">', $this->InputSafeString($cname), '</span></p>';
				}
			}
		}
		echo '<p><label>Phone:</label><input type="text" name="phone" class="pftext" value="', $this->InputSafeString($this->details['phone']), '" /></p><div class="clear"></div><p class="clearfix"><label>&nbsp;</label><input type="submit" class="submit" value="', $this->InputSafeString($submit_value), '" /></p></form>';
		return ob_get_clean();
	} // end of fn DeliveryAddressForm
	
	function SaveDeliveryAddress($data = array())
	{	$fields = array();
		$fail = array();
		$success = array();
		
		$addr1 = $this->SQLSafe($data['addr1']);
		$addr2 = $this->SQLSafe($data['addr2']);
		if ($addr1 || $addr2)
		{	$fields[] = 'addr1="' . $addr1 . '"';
			$fields[] = 'addr2="' . $addr2 . '"';
		} else
		{	$fail[] = 'Your address is missing';
		}

		if ($fullname = $this->SQLSafe($data['fullname']))
		{	$fields[] = 'fullname="' . $fullname . '"';
		} else
		{	$fail[] = 'you must give your name';
		}

		if ($city = $this->SQLSafe($data['city']))
		{	$fields[] = 'city="' . $city . '"';
		} else
		{	$fail[] = 'you must give your town or city';
		}

		if ($county = $this->SQLSafe($data['county']))
		{	$fields[] = 'county="' . $county . '"';
		} else
		{	$fail[] = 'you must give your county';
		}

		if ($postcode = $this->SQLSafe($data['postcode']))
		{	$fields[] = 'postcode="' . $postcode . '"';
		} else
		{	$fail[] = 'your postcode is missing';
		}

		if (isset($data['phone']))
		{	$fields[] = 'phone="' . $this->SQLSafe($data['phone']) . '"';
		}
		
		if ($countrycode = $data['countrycode'])
		{	$countries = $this->CountryList();
			if ($countries[$countrycode])
			{	$fields[] = 'countrycode="' . $countrycode . '"';
			} else
			{	$fail[] = 'your country is missing';
			}
		} else
		{	$fail[] = 'your country is missing';
		}
		
		// password if changed
		if ($data['chpass'])
		{	if ($data['chpass'] !== $data['rtchpass'])
			{	$fail[] = 'password mistyped';
			} else
			{	if ($this->AcceptablePW($data['chpass']))
				{	$fields[] = 'pword=MD5("' . $data['chpass'] . '")';
				} else
				{	$fail[]= 'password must be 8 to 12 letters or numbers';
				}
			}
		}
		
		if ($set = implode(', ', $fields))
		{	$sql = 'UPDATE customers SET ' . $set . ' WHERE custid=' . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->Get($this->id);
					$success[] = 'Your changes have been saved';
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn SaveDeliveryAddress
	
} // end of defn Customer
?>