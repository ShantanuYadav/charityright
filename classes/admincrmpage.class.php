<?php
class AdminCRMPage extends AdminPage
{	
	public function __construct()
	{	parent::__construct('CRM');
	} //  end of fn __construct
	
	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('crm'))
		{	$this->css['admincrm'] = 'admincrm.css';
			$this->breadcrumbs->AddCrumb('crm_emailsearch.php', 'CRM');
			$this->AdminCRMLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AdminCRMLoggedInConstruct()
	{	
	} // end of fn AdminCRMLoggedInConstruct
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('crm'))
		{	$this->AdminCRMBodyMain();
		}
	} // end of fn AdminBodyMain
	
	protected function AdminCRMBodyMain()
	{	
	} // end of fn AdminCRMBodyMain
	
} // end of defn AdminCRMPage
?>