<?php
class WorldPayEar extends WorldPayRedirect
{	public $failmessage = '';
	public $successmessage = '';

	function __construct()
	{	parent::__construct();
	} // end of fn __construct
	
	function Verify()
	{	//if (SITE_TEST)
		//{	return true;
		//}
		//$this->LogFullPost('verify tried');
		
		return $_POST['callbackPW'] == $this->callbackPW;
 	} // end of fn Verify
	
	function LogRecord($text = '')
	{	//mail('tim@websquare.co.uk', 'diagnostics from wpear mat', $text);
		$text .= "\n\n" . 'Request from IP ' . "\n";
		$text .= 'HTTP_X_REAL_IP: ' . $_SERVER['HTTP_X_REAL_IP'] . "\n";
		$text .= 'HTTP_X_FORWARDED_FOR: ' . $_SERVER['HTTP_X_FORWARDED_FOR'] . "\n";
		$text .= 'REMOTE_ADDR: ' . $_SERVER['REMOTE_ADDR'] . "\n";
		mail('tim@websquare.co.uk', 'CR worldpay listener', $text);
	//	if (SITE_TEST)
	//	{	echo $text;
	//	}
	} // end of fn LogRecord
	
	public function Action()
	{	switch ($_POST['M_subtype'])
		{	case 'donation_cart': // one-off donation paid
				$cartorder = new CartOrder($_POST['cartId']);
				if ($cartorder->id && ($_POST['authMode'] == 'A') && ($_POST['transStatus'] == 'Y'))
				{	$result = $cartorder->UpdateFromWorldPayPost($_POST);
					if ($this->failmessage = $result['failmessage'])
					{	$this->LogFullPost($result['failmessage']);
					}
					$this->successmessage = $result['successmessage'];
				} //else $this->LogFullPost('failed to find or something');
				break;
			case 'donation_oneoff': // one-off donation paid
				$donation = new Donation($_POST['cartId']);
				if ($donation->id && ($_POST['authMode'] == 'A') && ($_POST['transStatus'] == 'Y'))
				{	$result = $donation->UpdateOneOffFromWorldPayPost($_POST);
					if ($this->failmessage = $result['failmessage'])
					{	$this->LogFullPost($result['failmessage']);
					}
					$this->successmessage = $result['successmessage'];
				} //else $this->LogFullPost('failed to find or something');
				break;
			case 'event_booking': // payment of event booking order
				$bookorder = new BookOrder($_POST['cartId']);
				if ($bookorder->id && ($_POST['authMode'] == 'A') && ($_POST['transStatus'] == 'Y'))
				{	$result = $bookorder->UpdateFromWorldPayPost($_POST);
					if ($this->failmessage = $result['failmessage'])
					{	$this->LogFullPost($result['failmessage']);
					}
					$this->successmessage = $result['successmessage'];
				} //else $this->LogFullPost('failed to find or something');
				break;
			case 'donation_crstars': // campaign donation paid
				$donation = new CampaignDonation($_POST['cartId']);
				if ($donation->id && ($_POST['authMode'] == 'A') && ($_POST['transStatus'] == 'Y'))
				{	$result = $donation->UpdateFromWorldPayPost($_POST);
					if ($this->failmessage = $result['failmessage'])
					{	$this->LogFullPost($result['failmessage']);
					}
					$this->successmessage = $result['successmessage'];
				} //else $this->LogFullPost('failed to find or something');
				break;
			default:
				//$this->DefaultAction();
				break;
		}
	} // end of fn Action
	
	protected function DefaultAction()
	{	$this->LogFullPost('action not found');
	} // end of fn DefaultAction
	
	function LogFullPost($text = "")
	{	ob_start();
		print_r($_POST);
		$this->LogRecord("$text ... \r\n" . ob_get_clean());
	} // end of fn LogFullPost
	
} // end of defn WorldPayEar
?>