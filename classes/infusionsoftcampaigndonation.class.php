<?php
class InfusionSoftCampaignDonation extends InfusionSoft
{	
	
	public function AddContactFromDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id)
		{	$cp = new ContactPreferences();
			if ($contactID = $this->AddContact($donation->details, $cp->IsOptionsSetFromValue($donation->details['contactpref'], 'email')))
			{	return $contactID;
			}
		}
	} // fn AddContactFromDonation
	
	public function RefundDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id && $donation->details['infusionsoftid'])
		{	return $this->RefundInvoice($donation->details['infusionsoftid']);
		}
	} // fn RefundDonation
	
	public function AmendDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id && $donation->details['infusionsoftid'])
		{	if ($ifid = $this->AddContactFromDonation($donation))
			{	
				$extradata = array();
				$this->AddContactFromDonation($donation);
				$extradata['_IsyourdonationZakat'] = ($donation->details['zakat'] ? 'Yes' : 'No');
				$extradata['_donorgiftaid'] = ($donation->details['giftaid'] ? 'Yes' : 'No');

				$this->AddExtraToInvoice($extradata, $donation->details['infusionsoftid']);
				if ($donation->details['giftaid'])
				{	$this->AddTagToContact($ifid, 'don_giftaid');
				} else
				{	$this->RemoveTagFromContact($ifid, 'don_giftaid');
				}
				if ($donation->details['zakat'])
				{	$this->AddTagToContact($ifid, 'don_zakat');
				}
			}
		}
	} // fn AmendDonation
	
	public function AddDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id)
		{	$invoicedata = array();
			$itemdata = array();
			$extradata = array();
			
			if ($ifid = $this->AddContactFromDonation($donation))
			{	
				$dummy = new Donation();
				$countries = $dummy->GetDonationCountries();
				$cpref = new ContactPreferences();
			
				if ($donation->details['teamid'])
				{	$campaign = new Campaign($donation->details['teamid']);
					$donationcountry = $campaign->details['donationcountry'];
					$donationproject = $campaign->details['donationproject'];
				} else
				{	if ($donation->details['campaignid'])
					{	$campaign = new Campaign($donation->details['campaignid']);
						$donationcountry = $campaign->details['donationcountry'];
						$donationproject = $campaign->details['donationproject'];
					}
				}
			
				$extradata['_Currency0'] = $donation->details['currency'];
				$extradata['_IsyourdonationZakat'] = ($donation->details['zakat'] ? 'Yes' : 'No');
				$extradata['_Causes0'] = $countries[$donationcountry]['shortname'];
				$extradata['_Whichareawouldyouliketodonateto'] = $countries[$donationcountry]['projects'][$donationproject]['projectname'];
				$extradata['_administrationcost'] = floatval(0);
				$extradata['_DonationAmount'] = floatval($donation->details['amount']);
				$extradata['_donorgiftaid'] = ($donation->details['giftaid'] ? 'Yes' : 'No');
				$extradata['_DonationType'] = 'oneoff';
				$extradata['_Howwouldyouliketoreceiveupdates'] = $cpref->DisplayChosenPreferences($donation->details['contactpref'], ',', 'cpcode');
				$extradata['_AdminLink'] = SITE_URL . ADMIN_URL . '/campaigndonation.php?id=' . (int)$donation->id;
			
				if ($gbpamount = $donation->details['gbpamount'])
				{	$invoicedata['contactID'] = $ifid;
					$invoicedata['name'] = 'Website CR Stars donation';
					$invoicedata['date'] = $donation->details['donated'];
					
					$itemdata['gbpamount'] = $gbpamount;
					$itemdata['quantity'] = 1;
					$itemdata['description'] = 'CR Stars donation';
					$itemdata['notes'] = $countries[$donationcountry]['shortname'] . ', ' . $countries[$donationcountry]['projects'][$donationproject]['projectname'];
				}

				if ($invoicedata && $itemdata)
				{	if ($invid = (int)$this->AddBlankInvoice($invoicedata))
					{	$updatesql = 'UPDATE campaigndonations SET infusionsoftid=' . $invid . ' WHERE cdid=' . $donation->id;
						$this->db->Query($updatesql);
						$this->AddDonationToInvoice($itemdata, $invid);
						if ($extradata)
						{	$this->AddExtraToInvoice($extradata, $invid);
						}
						$this->AddOptinTagsToContactFromDonation($donation, $ifid);
						return $invid;
					}
				}
			}
		}
	} // fn AddDonation
	
	public function PayDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id)
		{	$paymentdata = array();
			
			if ($ifid = $this->AddContactFromDonation($donation))
			{	if ($donation->details['donationref'])
				{	if ($gbpamount = $donation->details['gbpamount'])
					{	
						$paymentdata['gbpamount'] = $gbpamount;
						$paymentdata['date'] = $donation->details['donated'];
						$paymentdata['paytype'] = 'Credit card';
						$paymentdata['description'] = 'Website payment';
						if (($invid = (int)$donation->details['infusionsoftid']) || ($invid = (int)$this->AddBlankInvoice($invoicedata)))
						{	$this->AddPaymentToInvoice($paymentdata, $invid);
							$this->AddTagToContact($ifid, 'don_crstars');
							$this->AddDonationYearTagToContact($ifid, $donation->details['created']);
							if ($donation->details['giftaid'])
							{	$this->AddTagToContact($ifid, 'don_giftaid');
							} else
							{	$this->RemoveTagFromContact($ifid, 'don_giftaid');
							}
							if ($donation->details['zakat'])
							{	$this->AddTagToContact($ifid, 'don_zakat');
							}
						}
					}
				}
			}
		}
	} // fn PayDonation
	
	public function UploadDataFromPost($post = array())
	{	if ($post['donoremail'])
		{	$data = array(	'FirstName' => 	$post['donorfirstname'],
							'LastName' => 	$post['donorsurname'],
							'Email' => 		$post['donoremail'],
							'Title' => 		$post['donortitle'],
							'StreetAddress1' => $post['donoradd1'],
							'StreetAddress2' => $post['donoradd2'],
							'PostalCode' => $post['donorpostcode'],
							'City' => 		$post['donorcity'],
							'Website' => SITE_URL . ADMIN_URL . '/crm_email.php?email=' . urlencode($post['donoremail']),
							'Country' =>	$this->GetCountry($post['donorcountry'])
						);
			if ($post['donorphone'])
			{	$data['Phone1'] = $post['donorphone'];
			}
			return $data;
		}
	} // end of fn UploadDataFromPost
	
	public function DeleteDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id && $donation->details['infusionsoftid'])
		{	$this->DeleteInvoice($donation->details['infusionsoftid']);
		}
	} // fn DeleteDonation
	
	protected function DonationFromID($donation = 0)
	{	if (is_a($donation, 'CampaignDonation'))
		{	return $donation;
		} else
		{	return new CampaignDonation($donation);
		}
	} // fn DonationFromID
	
	public function AddOptinTagsToContactFromDonation($donation = 0, $contactid = 0)
	{	
		if (($donation = $this->DonationFromID($donation)) && $donation->id)
		{	if (!$contactid = (int)$contactid)
			{	$contactid = $this->AddContactFromDonation($donation);
			}
			if ($contactid)
			{	$cp = new ContactPreferences(array('phone', 'text', 'post'));
				$this->AddOptinTagsToContact($contactid, $cp, $donation->details['contactpref']);
			}
		}
	} // fn AddOptinTagsToContactFromDonation

} // end of defn InfusionSoftCampaignDonation
?>