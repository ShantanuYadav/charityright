<?php
class MyCRStarsPage extends CRStarsPage
{	protected $mypagename = '';

	public function __construct($mypagename = '')
	{	parent::__construct();
		$this->mypagename = $mypagename;
		if (!$this->camp_customer->id)
		{	header('location: ' . SITE_SUB . '/cr-stars/log-in/');
			exit;
		}
	} // end of fn __construct
	
	protected function HeaderLinksSearch(){}
	protected function MobileExtraLinksTop(){}

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		if ($this->camp_customer->id)
		{	$this->camp_customer->SetFBMeta($this->fb_meta);
		}
	} // end of fn SetFBMeta

	function MainBodyContent()
	{	$fullname = $this->InputSafeString($this->camp_customer->details['firstname']) . ' <br>' . $this->InputSafeString($this->camp_customer->details['lastname']);
		$links = array();
		$links['dashboard'] = array('text'=>'My Dashboard', 'link'=>SITE_SUB . '/my-cr-stars/');
		$links['create'] = array('text'=>'Start a new campaign', 'link'=>SITE_SUB . '/my-cr-stars/create-campaign/');
		$links['edit'] = array('text'=>'Edit my profile', 'link'=>$this->camp_customer->EditLink());
		$links['password'] = array('text'=>'Change my password', 'link'=>SITE_SUB . '/my-cr-stars/password/');

		$total_raised = array();
		$total_raised_gbp = 0;
		if ($campaign_rows = $this->camp_customer->GetCampaigns())
		{	$campaign_count = count($campaign_rows);
			foreach ($campaign_rows as $campaign_row)
			{	$campaign = new Campaign($campaign_row);
				$raised = $campaign->GetDonationTotal();
				$total_raised[$campaign_row['currency']] += $raised;
				if ($campaign_row['currency'] == 'GBP')
				{	$total_raised_gbp += $raised;
				} else
				{	$total_raised_gbp += round($raised / $this->GetCurrency($campaign_row['currency'], 'convertrate'), 2);
				}
			}
		}else
		{
			$campaign_count = 0;
		}

		echo '<div class="container cr-stars-dashboard-header"><div class="container_inner">';
			echo '<div class="cr-stars-dashboard-header-inner cr-stars-row">';
				echo '<div class="cr-stars-row-half">';
					echo '<div class="cr-stars-dashboard-header-left-container">';
						echo '<div class="cr-stars-dashboard-header-left-left">';
							echo '<div class="cr-stars-dashboard-header-left-image">';
								if(!$src = $this->camp_customer->GetImageSRC('large')){
									$src = SITE_URL .'img/template/avatar.png';
								}

								echo '<img src="'.$src.'" alt="'.$fullname.'" title="'.$fullname.'" />';
								echo '<a href="', SITE_URL ,'my-cr-stars/edit/#my-new-photo" class="cr-stars-dashboard-header-left-image-edit icon-camera">&nbsp;</a>';
							echo '</div>';
						echo '</div>';
						echo '<div class="cr-stars-dashboard-header-left-right">';
							echo '<div class="cr-stars-dashboard-header-left-name">';
								echo '<h1>'.$fullname.'</h1>';
							echo '</div>';
							echo '<div class="cr-stars-dashboard-header-left-raised">';
								echo '<div class="cr-stars-dashboard-header-raised-bar">';
									echo '<span class="raised-title">raised</span><span class="raised-amount"> ';
									if (count($total_raised) > 1)
									{	echo '&pound;', number_format($total_raised_gbp);
									} else
									{	foreach ($total_raised as $currency=>$raised)
										echo $this->GetCurrency($currency, 'cursymbol'), number_format($raised);
									}
									echo '</span>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '<div class="clear"></div></div>';
				echo '</div>';
				echo '<div class="cr-stars-row-half">';
					echo '<div class="cr-stars-dashboard-header-right-container">';
						echo '<div class="cr-stars-dashboard-header-search">';
							echo '<form id="crSearchDashboardHeader" method="get" action="'.SITE_URL.'cr-stars/search/"><input type="text" placeholder="Search CR Stars Campaigns..." name="crs_search" value="" required><button type="submit" class="icon-search">&nbsp;</button></form>';
						echo '</div>';
						echo '<div class="clear"></div>';
						echo '<div class="cr-stars-dashboard-header-right-bottom-container">';
							echo '<div class="cr-stars-dashboard-header-right-left">';
								echo '<div class="cr-stars-dashboard-header-campaign-count"><div class="cr-stars-dashboard-header-campaign-count-title">campaigns</div><div class="cr-stars-dashboard-header-campaign-count-count"><span>'.$campaign_count.'</span></div></div>';
							echo '</div>';
							echo '<div class="cr-stars-dashboard-header-right-right">';
								echo '<div class="cr-stars-dashboard-header-right-navigation">';
									echo '<ul>';
									foreach ($links as $linkname=>$link)
									{	echo '<li', $linkname == $this->mypagename ? ' class="selected"' : '', '><a href="', $link['link'], '">', $link['text'], '<span class="icon-right-open"></span></a></li>';
									}
									echo '</ul>';
								echo '</div>';
							echo '</div>';
						echo '<div class="clear"></div></div>';
					echo '<div class="clear"></div></div>';
				echo '</div>';
			echo '<div class="clear"></div></div>';
		echo '</div></div>';
		echo '<div class="container campuserMain cr-stars-main">';
			echo '<div class="container_inner">', $this->MemberBodyContent(), '</div>';
		echo '</div>';
		/*
		echo '<div class="container campDashboardInnerTop">
			<div class="container_inner">
				<div class="campDashboardInnerTopLeft"><div class="campDashboardInnerTopLeftImage"><div>';
		if ($src = $this->camp_customer->GetImageSRC('medium'))
		{	echo '<img src="', $src, '" alt="', $fullname, '" title="', $fullname, '" />';
		}
		echo '</div></div><div class="campDashboardInnerTopLeftText">Welcome ', $fullname, '</div><div class="campuserAmountBar"><span class="campuserAmountBarLabel">Raised</span><span class="campuserAmountBarAmount">';
		if (count($total_raised) > 1)
		{	echo '&pound;', number_format($total_raised_gbp);
		} else
		{	foreach ($total_raised as $currency=>$raised)
			echo $this->GetCurrency($currency, 'cursymbol'), number_format($raised);
		}
		echo '</span></div></div>
				<div class="campDashboardInnerTopRight"><ul class="campDashboardMenu">';
		foreach ($links as $linkname=>$link)
		{	echo '<li', $linkname == $this->mypagename ? ' class="selected"' : '', '><span></span><span></span><a href="', $link['link'], '">', $link['text'], '</a></li>';
		}
		echo '</ul></div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="container campuserMain">
			<div class="container_inner">', $this->MemberBodyContent(), '</div>
		</div>';
		*/
	} // end of fn MainBodyContent

	function MemberBodyContent()
	{
	} // end of fn MainBodyContent

} // end of class MyCRStarsPage
?>