<?php
class PaginationJS extends Pagination
{	protected $jsFunction = '';
	
	function __construct($page = 0, $total = 0, $perPage = 20, $pagename = 'p', $jsFunction = '')
	{	if ($jsFunction)
		{	$this->jsFunction = $jsFunction;
		}
		parent::__construct($page, $total, $perPage, '', array(), $pagename);
	} // end of constructor
	
	function AddToStripVars($stripVars = array()){}
	function SetBaseLink($baseLink = ''){}
	
	function setPages()
	{	$this->pages = array();
		$pstart = 0;
		do
		{	$this->pages[] = new PaginationJSPage($pstart, $this->jsFunction, $pstart == $this->page, $this->pagename);
		} while ((++$pstart * $this->perPage) < $this->total);
	} // end of fn setPages
	
	function Display($separator = '')
	{	$pagination = array();
		ob_start();
		foreach ($this->pages as $page)
		{	$pagination[] = $page->Display();
		}
		if ($this->page)
		{	echo '<a onclick="', $this->jsFunction, '(', $this->page, ');">&laquo;&nbsp;Prev</a>&nbsp;';
		}
		echo implode($separator, $pagination);
		if ($this->page < $this->numOfPages - 1)
		{	echo '&nbsp;<a onclick="', $this->jsFunction, '(', $this->page + 2, ');">Next&nbsp;&raquo;</a>';
		}
		return ob_get_clean();
	} // end of fn Display
	
} // end of class PaginationJS

class PaginationJSPage extends PaginationPage
{	protected $jsFunction = '';

	function __construct($pagenum = 0, $jsFunction = '', $current = false, $pagename = '')
	{	if ($jsFunction)
		{	$this->jsFunction = $jsFunction;
		}
		parent::__construct($pagenum, '', $current, $pagename);
		
	} // end of constructor
	
	function SetBaseLink($baseLink = ''){}
	
	function Display()
	{	ob_start();
		echo '<span', (!$this->current && $this->jsFunction) ? ' class="paginlink"' : '', '>';
		if (!$this->current && $this->jsFunction)
		{	echo '<a onclick="', $this->jsFunction, '(', $this->PageNum(), ');">';
		}
		echo $this->PageNum(), (!$this->current && $this->jsFunction) ? '</a>' : '', '</span>';
		return ob_get_clean();
	} // end of fn Display
	
} // end of class  PaginationJSPage

?>