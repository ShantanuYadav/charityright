<?php
class AdminPeoplePage extends AdminPage
{	protected $person;
	protected $menuarea;

	function __construct()
	{	parent::__construct('CMS');
	} //  end of fn __construct

	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('web content'))
		{	$this->AdminPeopleLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AdminPeopleLoggedInConstruct()
	{	//$this->css['adminposts'] = 'adminposts.css';
		//$this->js['adminposts'] = 'adminposts.js';
		$this->AssignPerson();
		$this->breadcrumbs->AddCrumb('people.php', 'People');
		$this->PeopleConstructFunctions();
		if ($this->person->id)
		{	$this->breadcrumbs->AddCrumb('person.php?id=' . $this->person->id, $this->InputSafeString($this->person->details['pname']));
		}
	} // end of fn AdminPeopleLoggedInConstruct
	
	protected function PeopleConstructFunctions()
	{	
	} // end of fn PeopleConstructFunctions
	
	protected function AssignPerson()
	{	$this->person = new AdminPerson($_GET['id']);
	} // end of fn AssignPerson
	
	protected function AdminPeopleBody()
	{	/*if ($this->person->id)
		{	if ($menu = $this->GetPersonMenu())
			{	echo '<div class="adminItemSubMenu"><ul>';
				foreach ($menu as $menuarea=>$menuitem)
				{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
				}
				echo '</ul><div class="clear"></div></div>';
			}
		}*/
	} // end of fn AdminPeopleBody
	
	protected function GetPersonMenu()
	{	$menu = array();
	//	$menu['edit'] = array('text'=>'Edit', 'link'=>'person.php?id=' . $this->person->id);
	//	$menu['cats'] = array('text'=>'Categories', 'link'=>'postcats.php?id=' . $this->person->id);
	//	$menu['images'] = array('text'=>'Images', 'link'=>'postimages.php?id=' . $this->person->id);
		return $menu;
	} // end of fn GetPersonMenu
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('web content'))
		{	$this->AdminPeopleBody();
		}
	} // end of fn AdminBodyMain
	
} // end of defn AdminPeoplePage
?>