<?php
class Volunteer extends BlankItem
{	public $earliest = '2017-06-01 16:00:00';

	public function __construct($id = 0)
	{	parent::__construct($id, 'volunteers', 'volid');
	} // fn __construct

	public function Create($data = array(), $campaignimage = array(), $team = false)
	{
		$fail = array();
		$success = array();
		$user_fields = array('regdate="' . $this->datefn->SQLDateTime() . '"');

		if ($this->ValidEMail($email = $data[$_SESSION['jotFormPrefix'] . 'email']))
		{	$user_fields['email'] = 'email="' . $this->SQLSafe($email) . '"';
		} else
		{	$fail[] = 'You must give a valid email address';
		}

		if ($firstname = $data[$_SESSION['jotFormPrefix'] . 'fname'])
		{	$user_fields['firstname'] = 'firstname="' . $this->SQLSafe($firstname) . '"';
		} else
		{	$fail[] = 'First name missing';
		}

		if ($surname = $data[$_SESSION['jotFormPrefix'] . 'sname'])
		{	$user_fields['surname'] = 'surname="' . $this->SQLSafe($surname) . '"';
		} else
		{	$fail[] = 'Surname missing';
		}

		if ($phone = $data[$_SESSION['jotFormPrefix'] . 'phone'])
		{	$user_fields['phone'] = 'phone="' . $this->SQLSafe($phone) . '"';
		} else
		{	$fail[] = 'Phone number missing or invalid';
		}
		$user_fields['volnotes'] = 'volnotes="' . $this->SQLSafe($data[$_SESSION['jotFormPrefix'] . 'notes']) . '"';

		if (!$fail && ($set = implode(', ', $user_fields)))
		{	$sql = 'INSERT INTO ' . $this->tablename . ' SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
				{	$this->Get($id);
					$this->SendAdminEmail();
					$this->SendVoluteerEmail();
					$success[] = 'Thank you for your interest, we will contact you shortly';
				}
			} //else $fail[] = $sql . ': ' . $this->db->Error();
		}

	//	$this->VarDump($cart);

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn Create

	public function SendVoluteerEmail()
	{	if ($this->id)
		{	ob_start();
			echo '<p>Dear ', $this->InputSafeString($this->details['firstname']), '</p><p>Thank you for your interest in working with Charity Right. Someone from Charity Right will contact you as soon as possible about joining our team.</p>';
			$body = ob_get_clean();
			$mail = new HTMLMail();
			$mail->SetSubject('Thank you for your enquiry');
			$mail->SendFromTemplate($this->details['email'], array('main_content'=>$body), 'default');
		}
	} // fn SendVoluteerEmail

	public function SendAdminEmail()
	{	if ($this->id)
		{	ob_start();
			echo '<p>Contact form filled in on "Join our team" page:</p><p>Name: ', $this->InputSafeString($this->details['firstname'] . ' ' . $this->details['surname']), '</p><p>Email: <a href="mailto:', $this->details['email'], '">', $this->details['email'], '</a></p><p>Phone: ', $this->InputSafeString($this->details['phone']), '</p><p>Notes ...<br />', $this->details['volnotes'] ? nl2br($this->InputSafeString($this->details['volnotes'])) : '{no notes submitted}', '</p>';

			$body = ob_get_clean();
			$mail = new HTMLMail();
			$mail->SetSubject('Charity Right volunteer contact form');
			$mail->SendFromTemplate($this->GetParameter('siteemails'), array('main_content'=>$body), 'admin');
		}
	} // fn SendAdminEmail

} // end of defn Volunteer
?>