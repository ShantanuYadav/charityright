<?php
class AdminPost extends Post
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct
	
	public function ResetUpdated()
	{	$sql = 'UPDATE posts SET updated="' . $this->datefn->SQLDateTime() . '" WHERE postid=' . $this->id;
		$this->db->Query($sql);
	} // end of fn CategoryRemove

	public function SlugExists($slug = '')
	{	$sql = 'SELECT postid FROM posts WHERE slug="' . $this->SQLSafe($slug) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT postid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	public function DeleteExtra()
	{	$catsql = 'DELETE FROM postsforcats WHERE postid=' . $this->id;
		$this->db->Query($catsql);
		$videosql = 'DELETE FROM postvideos WHERE postid=' . $this->id;
		$this->db->Query($videosql);
		$postssql = 'DELETE FROM postposts WHERE parentid=' . $this->id;
		$this->db->Query($postssql);
		if ($this->images)
		{	foreach ($this->images as $image_row)
			{	$image = new AdminPostImage($image_row);
				$image->Delete();
			}
		}
	} // end of fn DeleteExtra
	
	public function Save($data = array(), $photo_image = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		$fields['updated'] = 'updated="' . ($now = $this->datefn->SQLDateTime()) . '"';
		if ($this->id)
		{	if (($d = (int)$data['dpostdate']) && ($m = (int)$data['mpostdate']) && ($y = (int)$data['ypostdate']))
			{	$fields['postdate'] = 'postdate="' . $this->datefn->SQLDate(mktime(0, 0, 0, $m, $d, $y)) . '"';
			}
		} else
		{	$fields['posted'] = 'posted="' . $now . '"';
			$fields['postdate'] = 'postdate="' . $this->datefn->SQLDate() . '"';
		}
		
		if ($posttitle = $this->SQLSafe($data['posttitle']))
		{	$fields['posttitle'] = 'posttitle="' . $posttitle . '"';
		} else
		{	$fail[] = 'title cannot be empty';
		}
		
		if ($posttext = $this->SQLSafe($data['posttext']))
		{	$fields['posttext'] = 'posttext="' . $posttext . '"';
		} else
		{	$fail[] = 'main text cannot be empty';
		}
	
		$metadesc = $this->SQLSafe($data['metadesc']);
		$fields['metadesc'] = 'metadesc="' . $metadesc . '"';
	
		if (!$snippet = $data['snippet'])
		{	if ($data['posttext'])
			{	if (strlen($snippet = strip_tags($data['posttext'])) > 300)
				{	$snippet = substr($snippet, 0, 300) . ' ...';
				}
			}
		}
		$fields['snippet'] = 'snippet="' . $this->SQLSafe($snippet) . '"';
		$fields['live'] = 'live=' . ($data['live'] ? '1' : '0');
		
		if ($data['country'])
		{	$countries = $this->PossibleCountries();
			if ($countries[$data['country']])
			{	$fields['country'] = 'country="' . $data['country'] . '"';
			} else
			{	$fail[] = 'invalid country';
			}
		} else
		{	$fields['country'] = 'country=""';
		}
	
		$templates = $this->PossibleTemplates();
		if ($templates[$data['template']])
		{	$fields['template'] = 'template="' . $data['template'] . '"';
		} else
		{	$fail[] = 'invalid template';
		}
	
		if ($this->id)
		{	$slug = $this->TextToSlug($data['slug']);
		} else
		{	if ($posttitle)
			{	$slug = $this->TextToSlug($posttitle);
			}
		}
		
		if ($slug)
		{	$suffix = '';
			while ($this->SlugExists($slug . $suffix))
			{	$suffix++;
			}
			if ($suffix)
			{	$slug .= '_' . $suffix;
			}
			
			$fields['slug'] = 'slug="' . $slug . '"';
		} else
		{	if ($this->id || $posttitle)
			{	$fail[] = 'slug missing';
			}
		}
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE posts SET ' . $set . ' WHERE postid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO posts SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new post added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}
		
		if ($this->id)
		{	if($photo_image['size'])
			{	if ($this->ValidPhotoUpload($photo_image))
				{	$photos_created = 0;
					$imagesize = getimagesize($photo_image['tmp_name']);
					if ($this->ReSizePhotoPNG($photo_image['tmp_name'], $this->GetImageFile(), $imagesize[0], $imagesize[1], stristr($photo_image['type'], 'png') ? 'png' : 'jpg'))
					{	$success[] = 'banner image uploaded';
					}
					unset($photo_image['tmp_name']);
				} else
				{	$fail[] = 'image upload failed';
				}
			} else
			{	if ($data['deleteimage'])
				{	if (@unlink($this->GetImageFile()))
					{	$success[] = 'banner image deleted';
					}
				}
			}
		}
	
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function SavePostsListOrder($posts = array())
	{	
		$fail = array();
		$success = array();
		
		if ($this->id || is_array($posts))
		{	$changed = 0;
			foreach ($posts as $postid=>$listorder)
			{	if ($this->posts[$postid])
				{	$sql = 'UPDATE postposts SET listorder=' . (int)$listorder . ' WHERE parentid=' . (int)$this->id . ' AND postid=' . (int)$postid;
					if ($result = $this->db->Query($sql))
					{	if ($this->db->AffectedRows())
						{	$changed++;
						}
					}
				} else
				{	$fail[] = 'Post id ' . (int)$postid . ' not found in list';
				}
			}
			if ($changed)
			{	$success[] = 'Order to list posts has been changed';
				$this->GetPosts();
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn SavePostsListOrder
	
	public function PossibleTemplates()
	{	$templates = array();
		$sql = 'SELECT * FROM posttemplates ORDER BY listorder';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$templates[$row['ptname']] = $row['ptdesc'];
			}
		}
		return $templates;
	} // end of fn PossibleTemplates
	
	public function CanDelete()
	{	return $this->id && !$this->GetParentPosts();
	} // end of fn CanDelete
	
	public function GetParentPosts()
	{	$parents = array();
		if ($this->id)
		{	$tables = array('postposts');
			$fields = array('postposts.*');
			$where = array('postid'=>'postposts.postid=' . $this->id);
			if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where)))
			{	while ($row = $this->db->FetchArray($result))
				{	$parents[$row['parentid']] = $row;
				}
			}
		}
		return $parents;
	} // end of fn GetParentPosts
	
	public function InputForm()
	{	ob_start();
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddTextInput('Post title', 'posttitle', $this->InputSafeString($data['posttitle']), 'long');
		if ($this->id)
		{	$form->AddTextInput('Slug', 'slug', $this->InputSafeString($data['slug']), '', 255, 1);
		}
		$form->AddCheckBox('Live (visible in front end)', 'live', 1, $data['live']);
		if ($this->id)
		{	$postyears = array();
			$ystart = date('Y') - 1;
			$yend = date('Y');
			if (($existingy = date('Y', strtotime($data['postdate']))) < $ystart)
			{	$ystart = $existingy;
			}
			$form->AddDateInput('Post Date', 'postdate', $data['postdate'], $postyears);
		}
		$form->AddSelect('Template', 'template', $this->details['template'], '', $this->PossibleTemplates(), 0, 0);
		$form->AddSelect('Country (if any)', 'country', $this->details['country'], '', $this->PossibleCountries(), 1, 0);
		$form->AddTextArea('Post text', 'posttext', stripslashes($data['posttext']), 'tinymce', 0, 0, 30, 60);
		$form->AddTextArea('Snippet (try to keep below <span class="textCharLimit textCharLimitSnippet' . (strlen($data['snippet']) > 300 ? ' textCharLimitOver' : '') . '">300</span> characters)', 'snippet', $this->InputSafeString($data['snippet']), '', 0, 0, 5, 60, 'onkeyup="TextCharLimit(this, \'.textCharLimitSnippet\', 300);"');
		$form->AddTextArea('Meta description', 'metadesc', $this->InputSafeString($data['metadesc']), '', 0, 0, 10, 60);
		$form->AddFileUpload('Image file (jpg or png only, about 1200px wide)<br />Only used if the template includes a banner', 'imagefile');
		if ($this->id && ($src = $this->GetImageSRC()))
		{	$bannersize = getimagesize($this->GetImageFile());
			$form->AddRawText('<p><label>Current banner image (w=' . $bannersize[0] . 'px, h=' . $bannersize[1] . 'px)</label><img style="width: 400px;" src="' . $src . '" /><br /></p>');
			$form->AddCheckBox('Remove this banner image', 'deleteimage', 1);
		}
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Post', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this post</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
	public function PossibleExtraSections()
	{	$sections = array();
		if ($this->categories)
		{	foreach ($this->categories as $cat)
			{	if ($cat['postsections'])
				{	foreach(explode(',', $cat['postsections']) as $section)
					{	if ($section = trim($section))
						{	$sections[$section] = $section;
						}
					}
				}
			}
		}
		return $sections;
	} // end of fn PossibleExtraSections
	
	public function PossibleCategories()
	{	$tables = array('postcategories'=>'postcategories');
		$fields = array('postcategories.*');
		$where = array();
		$orderby = array('postcategories.catname ASC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'pcatid');
	} // end of fn PossibleCategories
	
	public function CategoriesTableContainer()
	{	ob_start();
		echo '<div id="postCatsContainer">', $this->CategoriesTable(), '</div><script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$("#pcats_modal_popup").jqm();});</script>',
			'<div id="pcats_modal_popup" class="jqmWindow" style=""><a href="#" class="jqmClose submit">Close</a><div id="pcatsModalInner"></div></div>';
		return ob_get_clean();
	} // end of fn CategoriesTableContainer
	
	public function CategoriesAddList()
	{	ob_start();
		if ($cats = $this->PossibleCategories())
		{	echo '<ul>';
			foreach ($cats as $catid=>$cat_row)
			{	echo '<li>';
				if ($this->categories[$catid])
				{	echo '<span>already used</span>';
				} else
				{	echo '<a onclick="PostAddCatAdd(', $this->id, ',', $catid, ');">add this</a>';
				}
				echo ' - ', $this->InputSafeString($cat_row['catname']), '</li>';
			}
			echo '</ul>';
		}
		return ob_get_clean();
	} // end of fn CategoriesAddList
	
	public function CategoriesTable()
	{	ob_start();
		echo '<table><tr class="newlink"><th colspan="3"><a onclick="PostAddCatPopup(', $this->id, ');">Add new category</a></th></tr><tr><th>Catgeory name</th><th>Catgeory link</th><th>Action</th></tr>';
		foreach ($this->categories as $cat_row)
		{	$cat = new AdminPostCategory($cat_row);
			echo '<tr><td>', $this->InputSafeString($cat->details['catname']), '</td><td><a href="', $link = $cat->Link(), '" target="_blank">', $link, '</a></td><td><a onclick="PostAddCatRemove(', $this->id, ',', $cat->id, ');">remove</a></td></tr>';
		}
		echo '</table>';
		return ob_get_clean();
	} // end of fn CategoriesTable
	
	public function ImagesTable()
	{	ob_start();
		echo '<table><tr class="newlink"><th colspan="4"><a href="postimage.php?postid=', $this->id, '">Add new image</a></th></tr><tr><th></th><th>Description</th><th class="centre">List order</th><th>Action</th></tr>';
		foreach ($this->images as $image_row)
		{	$image = new AdminPostImage($image_row);
			echo '<tr><td><img src="', $image->GetImageSRC('thumb'), '" /></td><td>', $this->InputSafeString($image->details['imagedesc']), '</td><td class="centre">', (int)$image->details['listorder'], '</td><td><a href="postimage.php?id=', $image->id, '">edit</a>&nbsp;|&nbsp;<a href="postimage.php?id=', $image->id, '&delete=1">delete</a></td></tr>';
		}
		echo '</table>';
		return ob_get_clean();
	} // end of fn ImagesTable
	
	public function SectionTable()
	{	ob_start();
		if ($possible = $this->PossibleExtraSections())
		{	echo '<table><tr><th>Section name</th><th>Text</th><th>Action</th></tr>';
			foreach ($this->PossibleExtraSections() as $psection)
			{	$mysection = $this->sections[$psection];
				echo '<tr><td>', $this->InputSafeString($psection), '</td><td>', $mysection ? nl2br($this->InputSafeString(substr(strip_tags($mysection['psectiontext']), 0, 100))) : '{not created}', '</td><td>';
				if ($mysection)
				{	echo '<a href="postsection.php?id=', $mysection['psid'], '">edit</a>&nbsp;|&nbsp;<a href="postsection.php?id=', $mysection['psid'], '&delete=1">delete</a>';
				} else
				{	echo '<a href="postsection.php?postid=', $this->id, '&psection=', $psection, '">create</a>';
				}
				echo '</td></tr>';
			}
			echo '</table>';
		}
		return ob_get_clean();
	} // end of fn SectionTable
	
	public function CategoryAdd($catid = 0)
	{	if (!$this->categories[$catid])
		{	if (($cat = new PostCategory($catid)) && $cat->id)
			{	$sql = 'INSERT INTO postsforcats SET pcatid=' . $cat->id . ', postid=' . $this->id;
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows())
					{	$this->GetCategories();
						return true;
					}
				}
			}
		}
	} // end of fn CategoryAdd
	
	public function PossibleCountries()
	{	$countries = array();
		$tables = array('countries'=>'countries');
		$fields = array('countries.shortcode', 'countries.postslug', 'countries.shortname');
		$where = array('postslug'=>'NOT countries.postslug=""');
		$orderby = array('countries.shortname');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$countries[$row['shortcode']] = $row['shortname'];
			}
		}
		return $countries;
	} // end of fn PossibleCountries
	
	public function CategoryRemove($catid = 0)
	{	if ($this->categories[$catid])
		{	$sql = 'DELETE FROM postsforcats WHERE pcatid=' . $catid . ' AND postid=' . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->GetCategories();
					return true;
				}
			}
		}
	} // end of fn CategoryRemove
	
	public function VideoAdd($vid = 0)
	{	if (!$this->videos[$vid])
		{	if (($video = new Video($vid)) && $video->id)
			{	$sql = 'INSERT INTO postvideos SET vid=' . $video->id . ', postid=' . $this->id;
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows())
					{	$this->GetVideos();
						return true;
					}
				}
			}
		}
	} // end of fn VideoAdd
	
	public function VideoRemove($vid = 0)
	{	if ($this->videos[$vid])
		{	$sql = 'DELETE FROM postvideos WHERE vid=' . $vid . ' AND postid=' . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->GetVideos();
					return true;
				}
			}
		}
	} // end of fn VideoRemove
	
	public function VideosTableContainer()
	{	ob_start();
		echo '<div id="postVideosContainer">', $this->VideosTable(), '</div><script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$("#pvids_modal_popup").jqm();});</script>',
			'<div id="pvids_modal_popup" class="jqmWindow" style=""><a href="#" class="jqmClose submit">Close</a><div id="pvidsModalInner"></div></div>';
		return ob_get_clean();
	} // end of fn VideosTableContainer
	
	private function PossibleVideos()
	{	$videos = array();
		$tables = array('videos'=>'videos');
		$fields = array('videos.*');
		$orderby = array('videos.uploaded DESC');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	if (!$this->videos[$row['vid']])
				{	$videos[$row['vid']] = $row;
				}
			}
		}
		return $videos;
	} // end of fn PossibleVideos
	
	public function VideosAddList()
	{	ob_start();
		if ($videos = $this->PossibleVideos())
		{	echo '<table>';
			foreach ($videos as $vid=>$video_row)
			{	$video = new Video($video_row);
				echo '<tr><td>';
				if ($this->videos[$video->id])
				{	echo 'already used';
				} else
				{	echo '<a onclick="PostAddVidsAdd(', $this->id, ',', $video->id, ');">add this</a>';
				}
				echo '</td><td>';
				if ($src = $video->GetImageSRC('thumb'))
				{	echo '<img src="', $src, '" />';
				}
				echo '</td><td>', $this->InputSafeString($video->details['vtitle']), '</td></tr>';
			}
			echo '</table>';
		}
		return ob_get_clean();
	} // end of fn VideosAddList
	
	public function VideosTable()
	{	ob_start();
		echo '<table><tr class="newlink"><th colspan="5"><a onclick="PostAddVideoPopup(', $this->id, ');">Add new video</a></th></tr><tr><th></th><th>Video title</th><th>Youtube ID</th><th>Uploaded</th><th>Action</th></tr>';
		foreach ($this->videos as $video_row)
		{	$video = new AdminVideo($video_row);
			echo '<tr><td>';
			if ($src = $video->GetImageSRC('thumb'))
			{	echo '<img src="', $src, '" />';
			}
			echo '</td><td>', $this->InputSafeString($video->details['vtitle']), '</td><td>', $video->YoutubeID(), '</td><td>', date('d/m/y', strtotime($video->details['uploaded'])), '</td><td><a onclick="PostVideoRemove(', $this->id, ',', $video->id, ');">remove</a>&nbsp;|&nbsp;<a href="video.php?id=', $video->id, '">edit</a></td></tr>';
		}
		echo '</table>';
		return ob_get_clean();
	} // end of fn VideosTable
	
	public function PostAdd($postid = 0)
	{	if (!$this->posts[$postid])
		{	if (($post = new Post($postid)) && $post->id)
			{	$sql = 'INSERT INTO postposts SET postid=' . $post->id . ', parentid=' . $this->id;
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows())
					{	$this->GetPosts();
						return true;
					}
				}
			}
		}
	} // end of fn PostAdd
	
	public function PostRemove($postid = 0)
	{	if ($this->posts[$postid])
		{	$sql = 'DELETE FROM postposts WHERE postid=' . $postid . ' AND parentid=' . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->GetPosts();
					return true;
				}
			}
		}
	} // end of fn PostRemove
	
	public function PostsTableContainer()
	{	ob_start();
		echo '<div id="postVideosContainer">', $this->PostsTable(), '</div><script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$("#pvids_modal_popup").jqm();});</script>',
			'<div id="pvids_modal_popup" class="jqmWindow" style=""><a href="#" class="jqmClose submit">Close</a><div id="pvidsModalInner"></div></div>';
		return ob_get_clean();
	} // end of fn PostsTableContainer
	
	private function PossiblePosts()
	{	$posts = array();
		$tables = array('posts'=>'posts');
		$fields = array('posts.*');
		$where = array('postid'=>'NOT postid=' . $this->id);
		$orderby = array('posts.postdate DESC');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	if (!$this->posts[$row['postid']])
				{	$posts[$row['postid']] = $row;
				}
			}
		}
		return $posts;
	} // end of fn PossiblePosts
	
	public function PostsAddList()
	{	ob_start();
		if ($posts = $this->PossiblePosts())
		{	echo '<table>';
			foreach ($posts as $postid=>$post_row)
			{	$post = new AdminPost($post_row);
				echo '<tr><td><a onclick="PostAddPostsAdd(', $this->id, ',', $post->id, ');">add this</a></td><td>', $this->InputSafeString($post->details['posttitle']), '</td><td>', $post->CategoriesList(', '), '</td></tr>';
			}
			echo '</table>';
		}
		return ob_get_clean();
	} // end of fn PostsAddList
	
	public function PostsTable()
	{	ob_start();
		echo '<form method="post" action="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '"><table><tr class="newlink"><th colspan="5"><a onclick="PostAddPostPopup(', $this->id, ');">Add new post</a></th></tr><tr><th>Post title</th><th>Categories</th><th>Post date</th><th>List order</th><th>Action</th></tr>';
		if ($this->posts)
		{	foreach ($this->posts as $post_row)
			{	$post = new AdminPost($post_row);
				echo '<tr><td>', $this->InputSafeString($post->details['posttitle']), '</td><td>', $post->CategoriesList(', '), '</td><td>', date('d/m/y', strtotime((int)$post->details['postdate'] ? $post->details['postdate'] : $post->details['updated'])), '</td><td><input type="text" class="number" name="listorder[', $post->id, ']" value="', $post->details['listorder'], '" /></td><td><a onclick="PostPostRemove(', $this->id, ',', $post->id, ');">remove</a>&nbsp;|&nbsp;<a href="post.php?id=', $post->id, '">edit</a></td></tr>';
			}
			echo '<tr><th colspan="3">&nbsp;</th><th colspan="2"><input type="submit" class="submit" value="Save order" /></th></tr>';
		}
		echo '</table></form>';
		return ob_get_clean();
	} // end of fn PostsTable
	
	public function ParentsTable()
	{	ob_start();
		echo '<table><tr><th>Post title</th><th>Categories</th><th>Post date</th><th>Action</th></tr>';
		foreach ($this->GetParentPosts() as $post_row)
		{	$post = new AdminPost($post_row['parentid']);
			echo '<tr><td>', $this->InputSafeString($post->details['posttitle']), '</td><td>', $post->CategoriesList(', '), '</td><td>', date('d/m/y', strtotime((int)$post->details['postdate'] ? $post->details['postdate'] : $post->details['updated'])), '</td><td><a href="post.php?id=', $post->id, '">edit</a></td></tr>';
		}
		echo '</table>';
		return ob_get_clean();
	} // end of fn ParentsTable

	public function GetQuickDonateOptions($live_only = false)
	{	return parent::GetQuickDonateOptions($live_only);
	} // end of fn GetQuickDonateOptions
	
	public function AdminQuickDonateForm()
	{	ob_start();
		$data = $this->GetQuickDonateOptions();
		$donation = new Donation();
		
		$countries = $donation->GetDonationCountries('oneoff', true);
		$types = array('oneoff'=>'one-off', 'monthly'=>'monthly (DD)');
		echo '<form method="post" action="postdonateform.php?id=', (int)$this->id, '">
				<label>Show quick donate on post</label><input type="checkbox" name="live"', $data['live'] ? ' checked="checked"' : '', ' /><br />
				<label>Type</label><select id="dlb_type" name="type" onchange="DLBTypeChange(false);">';
		$def_type = $data['type'];
		foreach ($types as $key=>$text)
		{	if (!$def_type)
			{	$def_type = $key;
			}
			echo '<option value="', $key, '"', $data['type'] == $key ? ' selected="selected"' : '', '>', $text, '</option>';
		}
		echo '</select><br />
				<label>Cause</label><select id="dlb_country" name="country" onchange="DLBChangeCountry(false);">';
		$def_country = $data['country'];
		foreach ($countries as $country)
		{	if (!$def_country)
			{	$def_country = $country['dcid'];
			}
			if ($country['dcid'] == $def_country)
			{	$projects = $country['projects'];
			}
			echo '<option value="', $country['dcid'], '"', $country['dcid'] == $def_country ? ' selected="selected"' : '', '>', $this->InputSafeString($country['shortname']), '</option>';
		}
		echo '</select><br />
				<label>Area</label><select id="dlb_project" name="project">';
		
		$def_project = $data['project'];
		foreach ($projects as $project)
		{	if (!$def_project)
			{	$def_project = $project['dpid'];
			}
			echo '<option value="', $project['dpid'], '"', $project['dpid'] == $def_project ? ' selected="selected"' : '', '>', $this->InputSafeString($project['projectname']), '</option>';
		}
		echo '</select><br />
				<label>Currency</label><select id="dlb_currency" name="currency">';
		$def_currency = $data['currency'];
		foreach ($this->GetCurrencies() as $curcode=>$currency)
		{	if (!$def_currency)
			{	$def_currency = $curcode;
			}
			echo '<option value="', $curcode, '"', $def_currency == $curcode ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
		}
		echo '</select><br />
				<label>Amount (GBP)</label><input type="text" name="amount" id="dlb_amount" class="number" value="', (int)$data['amount'], '" /><br />
				<label>Zakat</label><input type="checkbox" name="zakat" id="dlb_zakat"', $data['zakat'] ? ' checked="checked"' : '', ' /><br />
				<label>Quick donate</label><input type="checkbox" id="dlb_quick" name="quick"', $data['quick'] ? ' checked="checked"' : '', ' /><br />
				<label>&nbsp;</label><input type="submit" class="submit" value="Save Quick Donate Options" /><br />
			</form></div>';
//		$this->VarDump($countries);
		return ob_get_clean();
	} // end of fn AdminQuickDonateForm
	
	function AdminQuickDonateSave($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = $this->GetQuickDonateOptions();
		
		$fields['live'] = (bool)$data['live'];
		$fields['zakat'] = (bool)$data['zakat'];
		$fields['quick'] = (bool)$data['quick'];
		$fields['amount'] = (int)$data['amount'];
		
		$fields['country'] = $data['country'];
		$fields['project'] = $data['project'];
		$fields['currency'] = $data['currency'];
		$fields['type'] = $data['type'];
	
		$quickdonate = json_encode($fields);
		$sql = 'UPDATE posts SET quickdonate="' . $this->SQLSafe($quickdonate) . '" WHERE postid=' . (int)$this->id;
		if ($result = $this->db->Query($sql))
		{	if ($this->db->AffectedRows())
			{	$success[] = 'Changes saved';
				$this->Get($this->id);
			} else
			{	$fail[] = $sql;
			}
		} else $fail[] = $this->db->Error();
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn AdminQuickDonateSave

	private function GetCurrencies($type = 'oneoff')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies
	
} // end of defn AdminPost
?>