<?php
class Video extends BlankItem
{	protected $imagesizes = array('thumb'=>array('w'=>126, 'h'=>71), 'default'=>array('w'=>379, 'h'=>214), 'og'=>array('w'=>600, 'h'=>315));
	protected $imagelocation = '';
	protected $imagedir = '';
	
	public function __construct($id = 0, $slug = '')
	{	parent::__construct($id, 'videos', 'vid');
		$this->imagelocation = SITE_URL . 'img/videos/';
		$this->imagedir = CITDOC_ROOT . '/img/videos/';
		if (!$this->id && $slug)
		{	$sql = 'SELECT * FROM videos WHERE slug="' . $this->SQLSafe($slug) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
	} // fn __construct
	
	public function ResetExtra()
	{	
	} // end of fn ResetExtra
	
	public function GetExtra()
	{	
	} // end of fn GetExtra
	
	public function Link($post = false)
	{	if ($this->id)
		{	$link = SITE_URL . 'videos/' . $this->details['slug'] . '/';
			if (is_a($post, 'Post') && $post->id)
			{	$link .= $post->details['slug'] . '/';
			}
			return $link;
		}
	} // fn Link
	
	public function HasImage($size = 'default')
	{	if ($this->id && file_exists($this->GetImageFile($size)))
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions
	
	public function ImageDimensions($size = 'default')
	{	if ($this->imagesizes[$size])
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions
	
	public function GetImageFile($size = 'default')
	{	return $this->ImageFileDirectory($size) . '/' . $this->id .'.png';
	} // end of fn GetImageFile
	
	public function ImageFileDirectory($size = 'default')
	{	return $this->imagedir . $this->InputSafeString($size);
	} // end of fn FunctionName
	
	public function GetImageSRC($size = 'default')
	{	if ($this->HasImage($size))
		{	return $this->imagelocation . $this->InputSafeString($size) . '/' . $this->id . '.png';
		}
	} // end of fn GetImageSRC
	
	public function ImageHTML($size = 'default')
	{	if ($src = $this->GetImageSRC($size))
		{	ob_start();
			echo '<img src="', $src, '" alt="', $alt = $this->InputSafeString($this->details['vtitle']), '" title="', $alt, '" />';
			return ob_get_clean();
		}
	} // end of fn ImageHTML
	
	public function FrontEndImage($size = 'default')
	{	if (!$src = $this->GetImageSRC($size))
		{	return 'https://img.youtube.com/vi/' . $this->YoutubeID() . '/mqdefault.jpg';
		}
		return $src;
	} // end of fn FrontEndImage
	
	public function EmbedCodeWithOptions($options = array(), $newwidth = 0)
	{	$embedcode = $this->EmbedCode($newwidth);
		$parameters = array('wmode=transparent');
		if (is_array($options))
		{	foreach ($options as $option)
			{	switch ($option)
				{	case 'autoplay':
						$parameters[] = 'autoplay=1';
						break;
				}
			}
		}
		if ($ytid = $this->YoutubeID())
		{	$embedcode = str_replace($ytid, $ytid . '?' . implode('&', $parameters), $embedcode);
		}
		return $embedcode;
	} // end of fn FrontEndImage
	
	public function YoutubeImageURL()
	{	return 'https://img.youtube.com/vi/' . $this->YoutubeID() . '/mqdefault.jpg';
	} // end of fn YoutubeID
	
	public function YoutubeID()
	{	$embedcode = stripslashes($this->details['iframecode']);
		if ($startpos = strpos($embedcode, '/embed/'))
		{	$embedcode = substr($embedcode, $startpos + 7);
			if ($endpos = strpos($embedcode, '"'))
			{	return substr($embedcode, 0, $endpos);
			}
		}
	} // end of fn YoutubeID
	
	public function GetPosts()
	{	$tables = array('posts'=>'posts', 'postvideos'=>'postvideos');
		$fields = array('posts.*');
		$where = array('postvideos_link'=>'posts.postid=postvideos.postid', 'vid'=>'postvideos.vid=' . $this->id);
		$orderby = array('posts.posted DESC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'postid', true);
	} // end of fn GetPosts
	
	public function EmbedCode($newwidth = 0, $options = array())
	{	$embedcode = stripslashes($this->details['iframecode']);
		if ($newwidth = (int)$newwidth)
		{	if (($widthpos = strpos($embedcode, ' width="')) && ($oldwidth = (int)substr($embedcode, $widthpos + 8)))
			{	if ($heightpos = strpos($embedcode, ' height="') && ($oldheight = (int)substr($embedcode, $heightpos + 9)))
				{	if ($newheight = (int)(($oldheight * $newwidth) / $oldwidth))
					{	ob_start();
						echo substr($embedcode, 0, $pos = $widthpos + 8), $newwidth, substr($embedcode, $pos = $pos + strlen($oldwidth), 10), $newheight, substr($embedcode, $pos = $pos + strlen($oldheight) + 10);
						$embedcode = ob_get_clean();
					}
				}
			}
		}
		$parameters = array('wmode=transparent');
		if (is_array($options))
		{	foreach ($options as $option)
			{	switch ($option)
				{	case 'autoplay':
						$parameters[] = 'autoplay=1';
						break;
				}
			}
		}
		if ($ytid = $this->YoutubeID())
		{	$embedcode = str_replace($ytid, $ytid . '?' . implode('&', $parameters), $embedcode);
		}
		return $embedcode;
	} // end of fn EmbedCode
	
} // end of defn Video
?>