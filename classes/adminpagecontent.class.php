<?php
class AdminPageContent extends PageContent
{	var $adminuser = false;
	var $admintitle = '';
	var $langused = array();

	function __construct($id = 0, $adminuser = false)
	{	$this->adminuser = (bool)$adminuser;
		parent::__construct($id, false);
		$this->GetLangUsed();
		$this->GetAdminTitle();
	} //  end of fn __construct
	
	function GetAdminTitle()
	{	if ($this->id)
		{	if (!$this->admintitle = $this->details['pagetitle'])
			{	if ($details = $this->GetDefaultDetails())
				{	$this->admintitle = $details['pagetitle'];
				}
			}
		}
	} // end of fn GetAdminTitle
	
	function AssignPageLanguage()
	{	if (!$this->language = $_GET['lang'])
		{	$this->language = $this->def_lang;
		}
	} // end of fn AssignPageLanguage
	
	function AddDetailsForDefaultLang(){}
	
	function GetLangUsed()
	{	$this->langused = array();
		if ($this->id)
		{	if ($result = $this->db->Query('SELECT lang FROM pages_lang WHERE pageid=' . $this->id))
			{	while ($row = $this->db->FetchArray($result))
				{	$this->langused[$row['lang']] = true;
				}
			}
		}
	} // end of fn GetLangUsed
	
	function GetDefaultDetails()
	{	$sql = 'SELECT * FROM pages_lang WHERE pageid=' . $this->id . ' AND lang="' . $this->def_lang . '"';
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return $row;
			}
		}
		return array();
	} // end of fn GetDefaultDetails
	
	function AssignPage($subpage = 0)
	{	return new AdminPageContent($subpage);
	} // end of fn AssignPage
	
	function PageNameFromTitle($pagetitle = '')
	{	//$pagetitle = str_replace(array('?', '"', "'"), '', $pagetitle);
		$pagetitle = preg_replace('|[^\d\w ]|', '', $pagetitle);
		while (strstr($pagetitle, '  '))
		{	$pagetitle = str_replace('  ', ' ', $pagetitle);
		}
		return strtolower(str_replace(' ', '-', $pagetitle));
	} // end of fn PageNameFromTitle
	
	function PageNameExists($pagename = '')
	{	if ($pagename == $this->details['pagename'])
		{	return false;
		}
		$sql = 'SELECT pageid FROM pages WHERE pagename="' . $pagename . '"';
		if ($this->id)
		{	$sql .= ' AND NOT pageid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	return $this->db->NumRows($result);
		}
		
	} // end of fn PageNameExists
	
	function Save($data = array(), $image = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		$l_fields = array();
		
		if ($pagetitle = $this->SQLSafe($data['pagetitle']))
		{	if ($pagename = $this->PageNameFromTitle($pagetitle))
			{	if (!$this->id && $this->PageNameExists($pagename))
				{	$fail[] = 'page title ' . $pagename . ' has already been used';
				} else
				{	$l_fields[] = 'pagetitle="' . $pagetitle . '"';
					if (!$this->id)
					{	$fields[] = 'pagename="' . $pagename . '"';
					}
				}
			} else
			{	$fail[] = 'invalid title';
			}
		} else
		{	$fail[] = 'page title missing';
		}
		
		if ($pagetext = $this->SQLSafe($data['pagetext']))
		{	$l_fields[] = 'pagetext="' . $pagetext . '"';
		} else
		{	$l_fields[] = 'pagetext=""';
		}
		
		if ($headertext = $this->SQLSafe($data['headertext']))
		{	$l_fields[] = 'headertext="' . $headertext . '"';
		} else
		{	$l_fields[] = 'headertext=""';
		}
		
		if ($this->CanAdminUser('technical'))
		{	if ($includefile = $this->SQLSafe($data['includefile']))
			{	if ($this->IncludeFileExists($includefile))
				{	$fields[] = 'includefile="' . $includefile . '"';
				} else
				{	$fail[] = 'file to include does not exist';
				}
			} else
			{	$fields[] = 'includefile=""';
			}
		}
		
		if (isset($data['htmltitle']))
		{	$l_fields[] = 'htmltitle="' . $this->SQLSafe($data['htmltitle']) . '"';
		}
		
		if (isset($data['metakey']))
		{	$l_fields[] = 'metakey="' . $this->SQLSafe($data['metakey']) . '"';
		}
		
		if (isset($data['metadesc']))
		{	$l_fields[] = 'metadesc="' . $this->SQLSafe($data['metadesc']) . '"';
		}
		
		$fields[] = 'redirectlink="' . $this->SQLSafe($data['redirectlink']) . '"';
		$fields['redirectlinkforce'] = 'redirectlinkforce=0';
		if ($data['redirectlinkforce'])
		{	if ($data['redirectlink'])
			{	$fields['redirectlinkforce'] = 'redirectlinkforce=1';
			} else
			{	$fail[] = 'You cannot redirect without defining the link to redirect to';
			}
		}
		
		$fields[] = 'pagelive=' . ($data['pagelive'] ? '1' : '0');
		$fields[] = 'headeronly=' . ($data['headeronly'] ? '1' : '0');
		$fields[] = 'noheader=' . ($data['noheader'] ? '1' : '0');
		$fields[] = 'bannerlink="' . $this->SQLSafe($data['bannerlink']) . '"';
		
		if ((!$fail || $this->id) && $set = implode(', ', $fields))
		{	
			if ($this->id)
			{	$sql = 'UPDATE pages SET ' . $set . ' WHERE pageid=' . (int)$this->id;
			} else
			{	$sql = 'INSERT INTO pages SET ' . $set;
			}
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if (!$this->id)
					{	$this->id = $this->db->InsertID();
						$success[] = 'New page created';
					} else
					{	$record_changes = true;
						$success[] = 'Changes saved';
					}
					$this->Get($this->id);
				}
				
				if ($this->id)
				{	
					if ($set = implode(', ', $l_fields))
					{	if ($this->langused[$this->language])
						{	$sql = 'UPDATE pages_lang SET ' . $set . ' WHERE pageid=' . $this->id . ' AND lang="' . $this->language . '"';
						} else
						{	$sql = 'INSERT INTO pages_lang SET ' . $set . ', pageid=' . $this->id . ', lang="' . $this->language . '"';
						}
						if ($result = $this->db->Query($sql))
						{	if ($this->db->AffectedRows())
							{	$success[] = 'text changes saved';
							}
						}
					}
					$this->Get($this->id);
					$this->GetLangUsed();
					
				}
			} else echo $sql, ':', $this->db->Error();
			$this->GetAdminTitle();
		}
		
		if ($this->id)
		{	if($image['size'])
			{	if ($this->ValidPhotoUpload($image))
				{	$isize = getimagesize($image['tmp_name']);
					if ($this->ReSizePhotoPNG($image['tmp_name'], $this->GetBannerFile(), $isize[0], $isize[1], stristr($image['type'], 'png') ? 'png' : 'jpg'))
					{	$success[] = 'banner image uploaded';
					}
					unset($image['tmp_name']);
				} else
				{	$fail[] = 'image upload failed';
				}
			} else
			{	if ($data['deletebanner'])
				{	if (@unlink($this->GetBannerFile()))
					{	$success[] = 'banner image deleted';
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function ValidPhotoUpload($photo_image = array())
	{	return is_array($photo_image) && (stristr($photo_image['type'], 'jpeg') || stristr($photo_image['type'], 'jpg') || stristr($photo_image['type'], 'png')) && !$photo_image['error'] && $photo_image['size'];
	} // end of fn ValidPhotoUpload
	
	function ReSizePhotoPNG($uploadfile = '', $file = '', $maxwidth = 0, $maxheight = 0, $imagetype = '')
	{	$isize = getimagesize($uploadfile);
		$ratio = $maxwidth / $isize[0];
		$h_ratio = $maxheight / $isize[1];
		if ($h_ratio > $ratio)
		{	$ratio = $h_ratio;
		}
		switch ($imagetype)
		{	case 'png': $oldimage = imagecreatefrompng($uploadfile);
							break;
			case 'jpg':
			case 'jpeg': $oldimage = imagecreatefromjpeg($uploadfile);
							break;
		}
		
		if ($oldimage)
		{	$w_new = ceil($isize[0] * $ratio);
			$h_new = ceil($isize[1] * $ratio);
			
			if ($maxwidth && $maxheight && $ratio != 1)
			{	$newimage = imagecreatetruecolor($w_new,$h_new);
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
				imagecopyresampled($newimage,$oldimage,0,0,0,0,$w_new, $h_new, $isize[0], $isize[1]);
			} else
			{	$newimage = $oldimage;
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
			}
			
			// now get middle chunk - horizontally
			if ($maxwidth && $maxheight && ($w_new > $maxwidth || $h_new > $maxheight))
			{	$resizeimg = imagecreatetruecolor($maxwidth, $maxheight);
				if ($imagetype == 'png')
				{	imagealphablending($resizeimg, false);
					imagesavealpha($resizeimg, true);
				}
				$leftoffset = floor(($w_new - $maxwidth) / 2);
				imagecopyresampled($resizeimg, $newimage, 0, 0, floor(($w_new - $maxwidth) / 2), floor(($h_new - $maxheight) / 2), $maxwidth, $maxheight, $maxwidth, $maxheight);
				$newimage = $resizeimg;
			}
			
			ob_start();
			imagepng($newimage, NULL, 3);
			return file_put_contents($file, ob_get_clean());
		}
	} // end of fn ReSizePhotoPNG
	
	function Delete()
	{
		$fail = array();
		$success = array();
		
		$sql = 'DELETE FROM pages WHERE pageid=' . $this->id;
		if ($result = $this->db->Query($sql))
		{	if ($this->db->AffectedRows())
			{	$this->db->Query('DELETE FROM pages_lang WHERE pageid=' . $this->id);
				$this->db->Query('DELETE FROM pagesections WHERE pageid=' . $this->id);
				@unlink($this->GetBannerFile());
				$success[] = 'page "' . $this->details['pagename'] . '" has been deleted';
				$this->RecordAdminAction(array('tablename'=>'pages', 'tableid'=>$this->id, 'area'=>'pages', 'action'=>'deleted'));
				$this->Reset();
			}
		}
		
		if ($this->id)
		{	$fail[] = 'delete failed';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Delete
	
	function CanDelete()
	{	return $this->id && !$this->GetMenusUsedIn() && $this->CanAdminUserDelete();
	} // end of fn CanDelete
	
	function InputForm()
	{	
		if ($this->id)
		{	$data = $this->details;
			if (!$this->langused[$this->language])
			{	if ($_POST)
				{	// initialise details from this
					foreach ($_POST as $field=>$value)
					{	$data[$field] = $value;
					}
				}
			}
		} else
		{	$data = $_POST;
			if (!$data)
			{	$data = array('live'=>1);
			}
		}
		if ($this->id)
		{	$data = $this->details;
		} else
		{	$data = $_POST;
		}
		
		if ($this->CanDelete())
		{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
				$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
				'delete this page</a></p>';
		}
		
		$form = new Form('pageedit.php?id=' . (int)$this->id . '&lang=' . $this->language, 'pageedit');
		$form->AddTextInput('Page name', 'pagetitle', $this->InputSafeString($data['pagetitle']), '', 50);
		$form->AddTextInput('Page title (html header)', 'htmltitle', $this->InputSafeString($data['htmltitle']), 'long', 2550);
		
		if ($this->CanAdminUser('technical'))
		{	$form->AddTextInput('Extra page to include', 'includefile', $this->InputSafeString($data['includefile']), '', 50);
		}
		$form->AddCheckBox('Make live', 'pagelive', 1, $data['pagelive']);
		$form->AddCheckBox('Menu header only', 'headeronly', 1, $data['headeronly']);
		$form->AddTextInput('Link for page (full address if external)', 'redirectlink', $this->InputSafeString($data['redirectlink']), 'long', 255);
		$form->AddCheckBox('Redirect page to the above URL', 'redirectlinkforce', 1, $data['redirectlinkforce']);
		if ($this->id && ($link = $this->Link()))
		{	$form->AddRawText('<p><label>Link to page</label><span><a href="' . $link . '" target="_blank">' . $link . '</a></span><br /></p>');
		}
		$form->AddTextArea('Header content', $name = 'headertext', stripslashes($data['headertext']), 'tinymce', 0, 0, 5, 60);
		$form->AddCheckBox('Completely suppress header', 'noheader', 1, $data['noheader']);
		$form->AddTextArea('Page content', $name = 'pagetext', stripslashes($data['pagetext']), 'tinymce', 0, 0, 40, 60);
		$form->AddRawText('<p><label></label><a href="#" onclick="javascript:window.open(\'newsimagelist.php\', \'newsimages\', \'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=550\'); return false;">view available images</a></p>');
		$form->AddTextArea('Meta - description', $name = 'metadesc', stripslashes($data['metadesc']), '', 0, 0, 5, 60);
		$form->AddTextArea('Meta - keywords', $name = 'metakey', stripslashes($data['metakey']), '', 0, 0, 5, 60);
		$form->AddFileUpload('Banner image file (jpg or png only)', 'imagefile');
		if ($this->id && ($src = $this->GetBannerSRC()))
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $src . '" width="500" /><br /></p>');
			$form->AddTextInput('Link for banner (blank for no link)', 'bannerlink', $this->InputSafeString($data['bannerlink']), 'long', 255);
			$form->AddCheckBox('Delete banner', 'deletebanner', 1, false);
		}
		$form->AddSubmitButton('', $this->id ? 'Save' : 'Create', 'submit');
		$form->Output();
	} // end of fn InputForm
	
	public function SectionsTable()
	{	ob_start();
		echo '<table><tr class="newlink"><th colspan="5"><a href="pagesection.php?pageid=', $this->id, '">Add new section</a></th></tr><tr><th></th><th>Text</th><th>Class</th><th class="centre">List order</th><th>Action</th></tr>';
		foreach ($this->pagesections as $section_row)
		{	$section = new AdminPageSection($section_row);
			$options = array();
			$content = array();
			$class = $section->ClassFromClassName($section->details['psclass']);
			if ($sectioncount = count($section->SubSections()))
			{	$content[] = '<a href="pagesubsections.php?id=' . $section->id . '">[' . $sectioncount . ' Subsections]</a>';
			}
			if ($pstext = strip_tags($section->details['pstext']))
			{	$content[] = $this->InputSafeString(substr($pstext,0, 150));
			}
			if ($class['psodesc'])
			{	$options[] = $this->InputSafeString($class['psodesc']);
			}
			if ($textpos = $section->TextPosOptionText())
			{	$options[] = $this->InputSafeString($textpos);
			}
			echo '<tr><td>';
			if ($section->HasImage())
			{	echo '<img src="', $section->GetImageSRC(), '" width="150" />';
			}
			echo '</td><td style="';
			if ($class['can_bgcolour'])
			{	echo 'background-color: #', $section->details['bgcolour'], ' !important;';
			}
			if ($class['can_textcolour'])
			{	echo 'color: #', $section->details['textcolour'], ' !important;';
			}
			echo '">', implode('<br />', $content), '</td><td>', implode(', ', $options), '</td><td class="centre">', $section->details['live'] ? '' : 'NOT LIVE<br />', (int)$section->details['listorder'], '</td><td><a href="pagesection.php?id=', $section->id, '">edit</a>&nbsp;|&nbsp;<a href="pagesection.php?id=', $section->id, '&delete=1">delete</a></td></tr>';
		}
		echo '</table>';
		return ob_get_clean();
	} // end of fn SectionsTable

	public function GetQuickDonateOptions($live_only = false)
	{	return parent::GetQuickDonateOptions($live_only);
	} // end of fn GetQuickDonateOptions
	
	protected function GetPageSections($liveonly = false)
	{	parent::GetPageSections($liveonly);
	} // end of fn GetPageSections
	
	public function AdminQuickDonateForm()
	{	ob_start();
		$data = $this->GetQuickDonateOptions();
		$donation = new Donation();
		
		$countries = $donation->GetDonationCountries('oneoff', true);
		$types = array('oneoff'=>'one-off', 'monthly'=>'monthly (DD)');
		echo '<form method="post" action="pagedonateform.php?id=', (int)$this->id, '">
				<label>Show quick donate on page</label><input type="checkbox" name="live"', $data['live'] ? ' checked="checked"' : '', ' /><br />
				<label>Type</label><select id="dlb_type" name="type" onchange="DLBTypeChange(false);">';
		$def_type = $data['type'];
		foreach ($types as $key=>$text)
		{	if (!$def_type)
			{	$def_type = $key;
			}
			echo '<option value="', $key, '"', $data['type'] == $key ? ' selected="selected"' : '', '>', $text, '</option>';
		}
		echo '</select><br />
				<label>Cause</label><select id="dlb_country" name="country" onchange="DLBChangeCountry(false);">';
		$def_country = $data['country'];
		foreach ($countries as $country)
		{	if (!$def_country)
			{	$def_country = $country['dcid'];
			}
			if ($country['dcid'] == $def_country)
			{	$projects = $country['projects'];
			}
			echo '<option value="', $country['dcid'], '"', $country['dcid'] == $def_country ? ' selected="selected"' : '', '>', $this->InputSafeString($country['shortname']), '</option>';
		}
		echo '</select><br />
				<label>Area</label><select id="dlb_project" name="project">';
		
		$def_project = $data['project'];
		foreach ($projects as $project)
		{	if (!$def_project)
			{	$def_project = $project['dpid'];
			}
			echo '<option value="', $project['dpid'], '"', $project['dpid'] == $def_project ? ' selected="selected"' : '', '>', $this->InputSafeString($project['projectname']), '</option>';
		}
		echo '</select><br />
				<label>Currency</label><select id="dlb_currency" name="currency">';
		$def_currency = $data['currency'];
		foreach ($this->GetCurrencies() as $curcode=>$currency)
		{	if (!$def_currency)
			{	$def_currency = $curcode;
			}
			echo '<option value="', $curcode, '"', $def_currency == $curcode ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
		}
		echo '</select><br />
				<label>Amount (GBP)</label><input type="text" name="amount" id="dlb_amount" class="number" value="', (int)$data['amount'], '" /><br />
				<label>Zakat</label><input type="checkbox" name="zakat" id="dlb_zakat"', $data['zakat'] ? ' checked="checked"' : '', ' /><br />
				<label>Quick donate</label><input type="checkbox" id="dlb_quick" name="quick"', $data['quick'] ? ' checked="checked"' : '', ' /><br />
				<label>&nbsp;</label><input type="submit" class="submit" value="Save Quick Donate Options" /><br />
			</form></div>';
//		$this->VarDump($countries);
		return ob_get_clean();
	} // end of fn AdminQuickDonateForm
	
	function AdminQuickDonateSave($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = $this->GetQuickDonateOptions();
		
		$fields['live'] = (bool)$data['live'];
		$fields['zakat'] = (bool)$data['zakat'];
		$fields['quick'] = (bool)$data['quick'];
		$fields['amount'] = (int)$data['amount'];
		
		$fields['country'] = $data['country'];
		$fields['project'] = $data['project'];
		$fields['currency'] = $data['currency'];
		$fields['type'] = $data['type'];
	
		$quickdonate = json_encode($fields);
		$sql = 'UPDATE pages SET quickdonate="' . $this->SQLSafe($quickdonate) . '" WHERE pageid=' . (int)$this->id;
		if ($result = $this->db->Query($sql))
		{	if ($this->db->AffectedRows())
			{	$success[] = 'Changes saved';
				$this->Get($this->id);
			} else
			{	//$fail[] = $sql;
			}
		} else $fail[] = $this->db->Error();
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn AdminQuickDonateSave

	private function GetCurrencies($type = 'oneoff')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies
	
} // end of defn AdminPageContent
?>