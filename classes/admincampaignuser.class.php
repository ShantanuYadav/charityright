<?php
class AdminCampaignUser extends CampaignUser
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} // fn __construct
	
	public function Create($data = array(), $campaignimage = array(), $team = false)
	{	
		if ($this->id)
		{	return array('failmessage'=>'User already created');
		} else
		{	$data['password'] = $this->CreatePassword(10);
			$pref = new ContactPreferences(array('email', 'phone', 'text'));
			$data['contactpref'] = $pref->PreferencesValueFromArray($data['contactpref']);
			$data['donation_country'] = $data['donationcountry'];
			$data['donation_project'] = $data['donationproject'];
			$saved = parent::Create($data);
			
			if ($this->id)
			{	foreach ($this->GetCampaigns() as $camp_row)
				{	$this->SendUserCreateEmail($camp_row, $data['password']);
				}
			}
			return $saved;
		}
		
	} // fn Create
	
	protected function LogInOnCreate(){}

	public function SendUserCreateEmail($camp_row = array(), $password = '')
	{	
		if ($this->id)
		{	ob_start();
			echo '<p>Dear ', $this->InputSafeString($this->details['firstname']), ',</p>
<p>We have created a CR Stars campaign for you ... "', $this->InputSafeString($camp_row['campname']), '"</p>
<p>To manage this campaign log in to <a href="">Charity Right CR Stars</a> with:</p>
<ul><li>your email address: ', $this->InputSafeString($this->details['email']), '</li>
<li>password: ', $this->InputSafeString($password), '</li></ul>';
			$mail = new HTMLMail();
			$mail->SetSubject('Welcome to Charity Right CR Stars');
			$mail->SendFromTemplate($this->details['email'], array('main_content'=>ob_get_clean()), 'default');
		}
		
	} // fn SendUserCreateEmail
	
	public function ShowDetails()
	{	ob_start();
		if ($this->id)
		{	$pref = new ContactPreferences();
			echo '<div id="itemblock-disp">
					<div><ul>
						<li><span class="itemblock-label">Name</span><span class="itemblock-details">', $this->InputSafeString($this->details['firstname'] . ' ' . $this->details['lastname']), '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Email / log in</span><span class="itemblock-details">';
			if ($this->CanAdminUser('crm'))
			{	echo '<a href="crm_email.php?email=', urlencode($this->details['email']), '">', $this->InputSafeString($this->details['email']), '</a>';
			} else
			{	echo $this->InputSafeString($this->details['email']);
			}
			echo '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Registered</span><span class="itemblock-details">', date('d/m/y @H:i', strtotime($this->details['registered'])), '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Phone</span><span class="itemblock-details">', $this->InputSafeString($this->details['phone']), '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Image</span><span class="itemblock-details">';
			if ($src = $this->GetImageSRC('large'))
			{	echo '<img src="', $src, '" />';
			} else
			{	echo 'No image uploaded';
			}
			echo '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Contact methods approved</span><span class="itemblock-details">', $this->details['contactpref'] ? $pref->DisplayChosenPreferences($this->details['contactpref']) : 'none', '</span></li>
					</ul></div></div>';
		} else
		{	echo $this->InputForm();
		}
		return ob_get_clean();
	} // fn ShowDetails
	
	public function InputForm()
	{	ob_start();
		$pref = new ContactPreferences(array('email', 'phone', 'text'));
		if ($data = $_POST)
		{	$data['contactpref'] = $pref->PreferencesValueFromArray($data['contactpref']);
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddTextInput('User first name', 'firstname', $this->InputSafeString($data['firstname']), 'long');
		$form->AddTextInput('User last name', 'lastname', $this->InputSafeString($data['lastname']), 'long');
		$form->AddTextInput('User email address', 'email', $this->InputSafeString($data['email']), 'long');
		$form->AddTextInput('User phone number', 'phone', $this->InputSafeString($data['phone']), 'long');
		$form->AddTextInput('Campaign name', 'campname', $this->InputSafeString($data['campname']), 'long');
		$form->AddTextArea('Campaign description', 'description', $this->InputSafeString($data['description']), '', 0, 0, 6, 60);
		$form->AddSelectWithGroups('Currency for target', 'campcurrency', $data['campcurrency'], '', $this->CurrencyList(), 1, 1, '');
		$form->AddTextInput('Target', 'camptarget', (int)$data['camptarget'], 'number');
		ob_start();
		$donation = new Donation();
		if ($countries = $donation->GetDonationCountries('crstars', true))
		{	echo '<label for="donation_country">Which area should campaign donate to</label>
					<select id="donation_country" name="donationcountry" onchange="CRRegCountryChange();">';
			foreach ($countries as $country=>$cdetails)
			{	if (!$data['donationcountry'])
				{	$data['donationcountry'] = $country;
				}
				echo '<option value="', $this->InputSafeString($country), '"', ($country == $data['donationcountry']) ? ' selected="selected"' : "", '>', $this->InputSafeString($cdetails['shortname']), '</option>';
			}
			echo '</select><br />';
			echo '<div id="countryProjectContainer">', $this->CountryProjectsList($data['donationcountry'], $data['donationproject']), '</div>';
		}
		$form->AddRawText(ob_get_clean());
		if ($avatars_raw = $this->GetCampaignAvatarsAvailable())
		{	$avatars = array();
			ob_start();
			echo '<p><label>Choose an avatar</label><select name="avatarid"><option value="0">-- choose an avatar--</option>';
			foreach ($avatars_raw as $avatar_row)
			{	$avatar = new CampaignAvatar($avatar_row);
				echo '<option value="', $avatar->id, '" style="background: url(\'', $avatar->GetImageSRC('thumb'), '\') no-repeat left center; padding-left: 65px; height: 50px; background-size: 60px;"', $avatar->id == $data['avatarid'] ? ' selected="selected"' : '', '>', $this->InputSafeString($avatar->details['imagedesc']), '</option>';
			}
			echo '</select><br /></p>';
			$form->AddRawText(ob_get_clean());
		}
		$pref->AdminFormElements($form, $data['contactpref']);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Campaign', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this campaign</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // fn InputForm
	
	public function CountryProjectsList($country = '', $donation_project = '')
	{	ob_start();
		$donation = new Donation();
		$countries = $donation->GetDonationCountries('crstars', true);
		if ($projects = $countries[$country]['projects'])
		{	if (false && (count($projects) == 1))
			{	foreach ($projects as $project=>$details)
				{	echo '<input type="hidden" id="donation_project" name="donationproject" value="', $project, '" />';
				}
			} else
			{	echo '<label for="donationproject">Level 2 option</label><select id="donation_project" name="donationproject">';
				foreach ($projects as $project=>$details)
				{	echo '<option value="', $project, '"', $project == $donation_project ? ' selected="selected"' : '', '>', $this->InputSafeString($details['projectname']), '</option>';
				}
				echo '</select><br />';
			}
		}
		return ob_get_clean();
	} // end of fn CountryProjectsList
	
	public function PasswordForm()
	{	ob_start();
		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddPasswordInput('Password (8 to 20 letters or numbers)', 'pword', '', 20);
		$form->AddPasswordInput('... retype', 'rtpword', '', 20);
		$form->AddSubmitButton('','Save New Password', 'submit');
		$form->Output();
		return ob_get_clean();
	} // fn PasswordForm
	
	public function ChangePassword($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if ($data['pword'] || $data['rtpword'])
		{	if ($data['pword'] !== $data['rtpword'])
			{	$fail[] = 'password mistyped';
			} else
			{	if ($this->AcceptablePW($data['pword']))
				{	$fields['pword'] = 'password=MD5("' . $data['pword'] . '")';
				} else
				{	$fail[]= 'password not acceptable';
				}
			}
		} else
		{	if (!$this->userid)
			{	$fail[] = 'password needed';
			}
		}
		
		if (!$fail)
		{	
			$usersql = 'UPDATE campaignusers SET ' . implode(', ', $fields) . ' WHERE cuid=' . $this->id;
			if ($userresult = $this->db->Query($usersql))
			{	if ($this->db->AffectedRows())
				{	$success[] = 'Password changed';
				} else
				{	$fail[] = 'No change made';
				}
			}
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn ChangePassword
	
	public function SuspensionForm()
	{	ob_start();
		$data = $_POST;
		
		echo '<h3>Currently ', $this->details['suspended'] ? '' : 'NOT ', 'suspended.', $this->details['suspended'] ? '' : ' Suspending the user will prevent log in and also hide all of their campaigns.', '</h3>';

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddHiddenInput('suspflag', $this->details['suspended'] ? '0' : '1');
		$form->AddTextArea('Your notes', 'suspnotes', $this->InputSafeString($data['description']), '', 0, 0, 4, 40);
		$form->AddSubmitButton('', $this->details['suspended'] ? 'Unsuspend' : 'Suspend', 'submit');
		$form->Output();
		return ob_get_clean();
	} // fn SuspensionForm
	
	public function Suspend($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		$suspfields = array();
		
		if ($data['suspflag'])
		{	if ($this->details['suspended'])
			{	$fail[] = 'User is already suspended';
			}
		} else
		{	if (!$this->details['suspended'])
			{	$fail[] = 'User is not currently suspended';
			}
		}
		
		if (!$fail)
		{	$suspfields['suspended'] = $fields['suspended'] = 'suspended=' . ($data['suspflag'] ? '1' : '0');
			$suspfields['custime'] = 'custime="' . $this->datefn->SQLDateTime() . '"';
			$suspfields['suspnotes'] = 'suspnotes="' . $this->SQLSafe($data['suspnotes']) . '"';
			$suspfields['cuid'] = 'cuid=' . (int)$this->id;
			$suspfields['adminuser'] = 'adminuser=' . (int)$_SESSION[SITE_NAME]['auserid'];
			
			$usersql = 'UPDATE campaignusers SET ' . implode(', ', $fields) . ' WHERE cuid=' . $this->id;
			if ($userresult = $this->db->Query($usersql))
			{	if ($this->db->AffectedRows())
				{	$suspsql = 'INSERT INTO campaignusersusp SET ' . implode(', ', $suspfields);
					if ($suspresult = $this->db->Query($suspsql))
					{	if ($this->db->AffectedRows())
						{	$this->Get($this->id);
							$success[] = 'User has been ' . ($this->details['suspended'] ? '' : 'un') . 'suspended';
							$campsql = 'UPDATE campaigns SET visible=0 WHERE cuid=' . $this->id;
							if ($suspresult = $this->db->Query($campsql))
							{	if ($camps = $this->db->AffectedRows())
								{	$success[] = $camps . ' campaigns have been changed to NOT live';
								}
							}
						}
					}
				} else
				{	$fail[] = 'No change made';
				}
			}
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn Suspend
	
	public function SuspensionHistoryTable()
	{	ob_start();
		if ($history = $this->GetSuspensionHistory())
		{	$adminusers = array();
			echo '<table><tr><th>Date</th><th>Action</th><th>by</th><th>Their notes</th></tr>';
			foreach ($history as $susp)
			{	if (!$adminusers[$susp['adminuser']])
				{	$adminusers[$susp['adminuser']] = new AdminUser($susp['adminuser']);
				}
				echo '<tr><td>', date('d/m/y @H:i', strtotime($susp['custime'])), '</td><td>', $susp['suspended'] ? 'Suspended' : 'Suspension removed', '</td><td>', $this->InputSafeString($adminusers[$susp['adminuser']]->details['ausername']), '</td><td>', nl2br($this->InputSafeString($susp['suspnotes'])), '</td></tr>';
			}
			echo '</table>';
		}
		return ob_get_clean();
	} // fn SuspensionHistoryTable
	
	public function GetSuspensionHistory()
	{	$tables = array('campaignusersusp'=>'campaignusersusp');
		$fields = array('campaignusersusp.*');
		$where = array('cuid'=>'campaignusersusp.cuid=' . $this->id);
		$orderby = array('custime'=>'campaignusersusp.custime DESC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cusid', true);
	} // fn GetSuspensionHistory
	
} // end of defn SuspensionForm
?>