<?php
class AdminHowFoundUsOption extends HowFoundUsOption
{
	function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct
	
	private function ValueAlreadyUsed($field = '', $value = '')
	{	$sql = 'SELECT hfid FROM howfoundus WHERE ' . $field . '="' . $this->SQLSafe($value) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT hfid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
	} //  end of fn ValueAlreadyUsed
	
	function Save($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if ($hfvalue = $this->SQLSafe($data['hfvalue']))
		{	if ($this->ValueAlreadyUsed('hfvalue', $hfvalue))
			{	$fail[] = 'This recorded value has already been used';
			} else
			{	$fields[] = 'hfvalue="' . $hfvalue . '"';
			}
		} else
		{	$fail[] = 'Recorded value missing';
		}
		
		if ($hfdesc = $this->SQLSafe($data['hfdesc']))
		{	if ($this->ValueAlreadyUsed('hfdesc', $hfdesc))
			{	$fail[] = 'This front end description has already been used';
			} else
			{	$fields[] = 'hfdesc="' . $hfdesc . '"';
			}
		} else
		{	$fail[] = 'Front end description missing';
		}
		
		$fields[] = 'hforder=' . (int)$data['hforder'];
		
		// then save or create
		if ($this->id || !$fail)
		{	if ($set = implode(', ', $fields))
			{	if ($this->id)
				{	$sql = 'UPDATE howfoundus SET ' . $set . ' WHERE hfid=' . $this->id;
				} else
				{	$sql = 'INSERT INTO howfoundus SET ' . $set;
				}
				
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows())
					{	if ($this->id)
						{	$success[] = 'changes saved';
							$this->Get($this->id);
						} else
						{	if ($id = $this->db->InsertID())
							{	$this->Get($id);
								$success[] = 'new option created';
							}
						}
						//$this->BuildDropDownList();
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	private function BuildDropDownList()
	{	$options = array();
		if ($result = $this->db->Query('SELECT * FROM howfoundus ORDER BY hforder, hfvalue'))
		{	while ($row = $this->db->FetchArray($result))
			{	$options[$row['hfvalue']] = $row['hfdesc'];
			}
		}
		if ($options)
		{	if ($f = fopen(CITDOC_ROOT . '/dbcache/howfoundus_dropdown.txt', 'w'))
			{	fputs($f, json_encode($options));
				fclose($f);
			}
		}
	} // end of fn BuildDropDownList

	function CanDelete()
	{	return $this->id;
	} // end of fn CanDelete
	
	function InputForm()
	{	ob_start();
		
		if (!$data = $this->details)
		{	$data = $_POST;
		}
		
		if ($this->CanDelete())
		{	echo '<p><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', $_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you want to ' : '', 'delete this option</a></p>';
		}
		
		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id, 'yposForm');
		$form->AddTextInput('Value to be recorded', 'hfvalue', $this->InputSafeString($data['hfvalue']), '', 255, 1);
		$form->AddTextInput('Front end description', 'hfdesc', $this->InputSafeString($data['hfdesc']), 'long', 255, 1);
		$form->AddTextInput('List order', 'hforder', (int)$data['hforder'], 'number', 5);
		$form->AddSubmitButton('', $this->id ? 'Update' : 'Create New Option', 'submit');
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
} // end of class AdminHowFoundUsOption
?>