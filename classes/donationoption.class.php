<?php
class DonationOption extends BlankItem
{	public $projects = array();
	
	public function __construct($id = 0)
	{	parent::__construct($id, 'donation_countries', 'dcid');
	} // fn __construct
	
	public function GetFromSlug($slug = '')
	{	$this->country = array();
		$tables = array('donation_countries'=>'donation_countries');
		$fields = array('donation_countries.*');
		$where = array('slug'=>'donation_countries.slug="' . $this->InputSafeString($slug) . '"');
		if ($result = $this->db->Query($sql = $this->db->BuildSQL($tables, $fields, $where)))
		{	if ($row = $this->db->FetchArray($result))
			{	$this->Get($row);
			}
		}
	} // fn GetFromSlug
	
	public function ResetExtra()
	{	$this->projects = array();
	} // end of fn ResetExtra
	
	public function GetExtra()
	{	$this->GetProjects();
	} // end of fn GetExtra
	
	public function GetProjects()
	{	$tables = array('donation_projects'=>'donation_projects');
		$fields = array('donation_projects.*');
		$where = array('dpid'=>'donation_projects.dcid=' . $this->id);
		$orderby = array('donation_projects.listorder', 'donation_projects.dpid');
		$this->projects = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'dpid', true);
	} // fn GetProjects
	
	public function LiveProjects()
	{	$projects = array();
		if ($this->projects)
		{	foreach ($this->projects as $dpid=>$project)
			{	if ($project['oneoff'] || $project['monthly'] || $project['crstars'])
				{	$projects[$dpid] = $project;
				}
			}
		}
		return $projects;
	} // fn LiveProjects
	
	public function GetDonations()
	{	$donations = array();
		$tables = array('donations'=>'donations');
		$fields = array('donations.*');
		$where = array('country'=>'donations.donationcountry="' . $this->InputSafeString($this->details['dccode']) . '"');
		$orderby = array('donations.created ASC');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$donations[$row['did']] = $row;
			}
		}
		return $donations;
	} // fn GetDonations
	
	public function GetCampaigns()
	{	$campaigns = array();
		$tables = array('campaigns'=>'campaigns');
		$fields = array('campaigns.*');
		$where = array('country'=>'campaigns.donationcountry="' . $this->InputSafeString($this->details['dccode']) . '"');
		$orderby = array('campaigns.created ASC');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$campaigns[$row['cid']] = $row;
			}
		}
		return $campaigns;
	} // fn GetCampaigns
	
} // end of defn DonationOption
?>