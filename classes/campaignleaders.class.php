<?php
class CampaignLeaders extends Base
{	

	public function __construct()
	{	parent::__construct();
	} // fn __construct
	
	public function Leaders($limit = 20, $days_back = 0)
	{	$leaders = array();
		$tables = array('campaigndonations'=>'campaigndonations');
		$fields = array('campaigndonations.campaignid', 'campaigndonations.teamid', 'campaigndonations.gbpamount');
		$where = array('paid'=>'NOT campaigndonations.donationref=""');
		
		echo $sql = $this->db->BuildSQL($tables, $fields, $where);
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where)))
		{	while ($row = $this->db->FetchArray($result))
			{	if (!$leaders[$row['campaignid']])
				{	$leaders[$row['campaignid']] = array('cid'=>$row['campaignid'], 'amount'=>0);
				}
				$leaders[$row['campaignid']]['amount'] += $row['gbpamount'];
				if ($row['teamid'])
				{	if (!$leaders[$row['teamid']])
					{	$leaders[$row['teamid']] = array('cid'=>$row['teamid'], 'amount'=>0);
					}
					$leaders[$row['teamid']]['amount'] += $row['gbpamount'];
				}
			}
		}
		
		uasort($leaders, array($this, 'UASortByAmount'));
		
		return $leaders;
	} // fn Leaders
	
	public function TeamLeaders($limit = 20, $days_back = 0)
	{	$leaders = array();
		$tables = array('campaigndonations'=>'campaigndonations');
		$fields = array('campaigndonations.campaignid', 'campaigndonations.teamid', 'campaigndonations.gbpamount');
		$where = array('paid'=>'NOT campaigndonations.donationref=""');
		
		echo $sql = $this->db->BuildSQL($tables, $fields, $where);
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where)))
		{	while ($row = $this->db->FetchArray($result))
			{	if (!$leaders[$row['campaignid']])
				{	$leaders[$row['campaignid']] = array('cid'=>$row['campaignid'], 'amount'=>0);
				}
				$leaders[$row['campaignid']]['amount'] += $row['gbpamount'];
				if ($row['teamid'])
				{	if (!$leaders[$row['teamid']])
					{	$leaders[$row['teamid']] = array('cid'=>$row['teamid'], 'amount'=>0);
					}
					$leaders[$row['teamid']]['amount'] += $row['gbpamount'];
				}
			}
		}
		
		uasort($leaders, array($this, 'UASortByAmount'));
		
		return $leaders;
	} // fn TeamLeaders
	
	private function UASortByAmount($a, $b)
	{	if ($a['amount'] == $b['amount'])
		{	return $a['cid'] < $b['cid'];
		} else
		{	return $a['amount'] < $b['amount'];
		}
	} // end of fn UASortByAmount
	
} // end of defn CampaignLeaders
?>