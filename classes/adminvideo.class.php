<?php
class AdminVideo extends Video
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct

	public function SlugExists($slug = '')
	{	$sql = 'SELECT vid FROM videos WHERE slug="' . $this->SQLSafe($slug) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT vid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	public function Save($data = array(), $photo_image = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($vtitle = $this->SQLSafe($data['vtitle']))
		{	$fields['vtitle'] = 'vtitle="' . $vtitle . '"';
			$slug = $this->TextToSlug($data['slug']);
		} else
		{	$fail[] = 'title cannot be empty';
		}
		
		if ($iframecode = $this->SQLSafe($data['iframecode']))
		{	$fields['iframecode'] = 'iframecode="' . $iframecode . '"';
		} else
		{	$fail[] = 'embed code cannot be empty';
		}
		
		if ($this->id)
		{	$slug = $this->TextToSlug($data['slug']);
			// end date and time
			if (($d = (int)$data['duploaded']) && ($m = (int)$data['muploaded']) && ($y = (int)$data['yuploaded']))
			{	$uploaded = $this->datefn->SQLDate(mktime(0,0,0,$m, $d, $y));
			} else
			{	$fail[] = 'uploaded date missing';
			}
		} else
		{	$fields['uploaded'] = 'uploaded="' . $this->datefn->SQLDateTime() . '"';
			if ($vtitle)
			{	$slug = $this->TextToSlug($vtitle);
			}
		}
		$fields['description'] = 'description="' . $this->SQLSafe($data['description']) . '"';
		$fields['metadesc'] = 'metadesc="' . $this->SQLSafe($data['metadesc']) . '"';
		$fields['live'] = 'live=' . ($data['live'] ? '1' : '0');
		
		if ($slug)
		{	$suffix = '';
			while ($this->SlugExists($slug . $suffix))
			{	$suffix++;
			}
			if ($suffix)
			{	$slug .= '_' . $suffix;
			}
			
			$fields['slug'] = 'slug="' . $slug . '"';
		} else
		{	if ($this->id || $vtitle)
			{	$fail[] = 'slug missing';
			}
		}
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE videos SET ' . $set . ' WHERE vid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO videos SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new video added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
			
			if ($this->id)
			{	if($photo_image['size'])
				{	if ($this->ValidPhotoUpload($photo_image))
					{	$photos_created = 0;
						foreach ($this->imagesizes as $size_name=>$size)
						{	if (!file_exists($this->ImageFileDirectory($size_name)))
							{	mkdir($this->ImageFileDirectory($size_name));
							}
							if ($this->ReSizePhotoPNG($photo_image['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($photo_image['type'], 'png') ? 'png' : 'jpg'))
							{	$photos_created++;
							}
						}
						unset($photo_image['tmp_name']);
						if ($photos_created)
						{	$success[] = 'image uploaded';
						}
					} else
					{	$fail[] = 'image upload failed';
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function ValidPhotoUpload($photo_image = array())
	{	return is_array($photo_image) && (stristr($photo_image['type'], 'jpeg') || stristr($photo_image['type'], 'jpg') || stristr($photo_image['type'], 'png')) && !$photo_image['error'] && $photo_image['size'];
	} // end of fn ValidPhotoUpload
	
	function ReSizePhotoPNG($uploadfile = '', $file = '', $maxwidth = 0, $maxheight = 0, $imagetype = '')
	{	$isize = getimagesize($uploadfile);
		$ratio = $maxwidth / $isize[0];
		$h_ratio = $maxheight / $isize[1];
		if ($h_ratio > $ratio)
		{	$ratio = $h_ratio;
		}
		switch ($imagetype)
		{	case 'png': $oldimage = imagecreatefrompng($uploadfile);
							break;
			case 'jpg':
			case 'jpeg': $oldimage = imagecreatefromjpeg($uploadfile);
							break;
		}
		
		if ($oldimage)
		{	$w_new = ceil($isize[0] * $ratio);
			$h_new = ceil($isize[1] * $ratio);
			
			if ($maxwidth && $maxheight && $ratio != 1)
			{	$newimage = imagecreatetruecolor($w_new,$h_new);
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
				imagecopyresampled($newimage,$oldimage,0,0,0,0,$w_new, $h_new, $isize[0], $isize[1]);
			} else
			{	$newimage = $oldimage;
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
			}
			
			// now get middle chunk - horizontally
			if ($maxwidth && $maxheight && ($w_new > $maxwidth || $h_new > $maxheight))
			{	$resizeimg = imagecreatetruecolor($maxwidth, $maxheight);
				if ($imagetype == 'png')
				{	imagealphablending($resizeimg, false);
					imagesavealpha($resizeimg, true);
				}
				$leftoffset = floor(($w_new - $maxwidth) / 2);
				imagecopyresampled($resizeimg, $newimage, 0, 0, floor(($w_new - $maxwidth) / 2), floor(($h_new - $maxheight) / 2), $maxwidth, $maxheight, $maxwidth, $maxheight);
				$newimage = $resizeimg;
			}
			
			ob_start();
			imagepng($newimage, NULL, 3);
			return file_put_contents($file, ob_get_clean());
		}
	} // end of fn ReSizePhotoPNG
	
	public function DeleteExtra()
	{	foreach ($this->imagesizes as $size_name=>$size)
		{	@unlink($this->GetImageFile($size_name));
		}
	} // end of fn DeleteExtra
	
	public function PostsTable()
	{	ob_start();
		echo '<table><tr class="newlink"><th colspan="9"><a href="post.php">Add new post</a></th></tr><tr><th></th><th>Title</th><th>Link</th><th>Created</th><th>Changed</th><th>Categories</th><th>Country</th><th>Live?</th><th>Actions</th></tr>';
		if ($posts = $this->GetPosts())
		{	
			foreach ($posts as $post_row)
			{	$post = new AdminPost($post_row);
				echo '<tr class="stripe', $i++ % 2,  '"><td>';
				if ($image_url = $post->MainImageSource('thumb'))
				{	echo '<img src="', $image_url, '" />';
				}
				echo '</td><td>', $this->InputSafeString($post->details['posttitle']), '</td><td>', $post->Link(), '</td><td>', date('d/m/y @H:i', strtotime($post->details['posted'])), '</td><td>', $post->details['posted'] == $post->details['updated'] ? '' : date('d/m/y @H:i', strtotime($post->details['updated'])), '</td><td>', $post->CategoriesList('<br />'), '</td><td>', $post->details['country'] ? $this->InputSafeString($this->GetCountry($post->details['country'], 'shortname')) : '', '</td><td>', $post->details['live'] ? 'Live' : '', '</td><td><a href="post.php?id=', $post->id, '">edit</a></td></tr>';
			}
		}
		echo '</table>';
		return ob_get_clean();
	} // end of fn PostsTable
	
	public function CanDelete()
	{	return $this->id && !$this->GetPosts();
	} // end of fn CanDelete
	
	public function InputForm()
	{	ob_start();
		$data = $this->details;
		if ($data = $this->details)
		{			
			$startyear = date('Y') - 5;
			$endyear = date('Y');
			if (($starttimeyear = date('Y', strtotime($this->details['uploaded']))) < $startyear)
			{	$startyear = $starttimeyear;
			}
		
			$years = array();
			for ($y = $startyear; $y <= $endyear; $y++)
			{	$years[] = $y;
			}

		} else
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddTextInput('Youtube embed code (iframe)', 'iframecode', $this->InputSafeString($data['iframecode']), 'long');
		$form->AddTextInput('Video title', 'vtitle', $this->InputSafeString($data['vtitle']), 'long');
		if ($this->id)
		{	$form->AddTextInput('Slug', 'slug', $this->InputSafeString($data['slug']), '', 255, 1);
			$form->AddDateInput('Uploaded', 'uploaded', $data['uploaded'], $years, 0, 0, 0, 0, date('Y'));
		}
		$form->AddCheckBox('Live (visible in front end)', 'live', 1, $data['live']);
		$form->AddTextArea('Description', 'description', $this->InputSafeString($data['description']), '', 0, 0, 5, 60);
		$form->AddFileUpload('Image file (jpg or png only, and as square as possible)', 'imagefile');
		if ($this->id && ($src = $this->GetImageSRC('thumb')))
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $src . '" /><br /></p>');
		}
		$form->AddTextArea('Meta description', 'metadesc', $this->InputSafeString($data['metadesc']), '', 0, 0, 5, 60);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Video', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this video</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
} // end of defn AdminVideo
?>