<?php
class InfusionSoftDonation extends InfusionSoft
{	
	
	public function AddContactFromDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id)
		{	$cp = new ContactPreferences();
			if ($contactID = $this->AddContact($donation->details, $cp->IsOptionsSetFromValue($donation->details['contactpref'], 'email')))
			{	return $contactID;
			}
		}
	} // fn AddContactFromDonation
	
	public function RefundDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id && $donation->details['infusionsoftid'])
		{	return $this->RefundInvoice($donation->details['infusionsoftid']);
		}
	} // fn RefundDonation
	
	public function AmendDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id && $donation->details['infusionsoftid'])
		{	if ($ifid = $this->AddContactFromDonation($donation))
			{	
				$extradata = array();
				$this->AddContactFromDonation($donation);
				$extradata['_IsyourdonationZakat'] = ($donation->details['zakat'] ? 'Yes' : 'No');
				$extradata['_donorgiftaid'] = ($donation->details['giftaid'] ? 'Yes' : 'No');

				$this->AddExtraToInvoice($extradata, $donation->details['infusionsoftid']);
				if ($donation->details['giftaid'])
				{	$this->AddTagToContact($ifid, 'don_giftaid');
				} else
				{	$this->RemoveTagFromContact($ifid, 'don_giftaid');
				}
				if ($donation->details['zakat'])
				{	$this->AddTagToContact($ifid, 'don_zakat');
				}
			}
		}
	} // fn AmendDonation
	
	public function AddDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id)
		{	$invoicedata = array();
			$itemdata = array();
			$extradata = array();
			
			if ($ifid = $this->AddContactFromDonation($donation))
			{	$countries = $donation->GetDonationCountries();
				$cpref = new ContactPreferences();
				
				$extradata['_Currency0'] = $donation->details['currency'];
				$extradata['_IsyourdonationZakat'] = ($donation->details['zakat'] ? 'Yes' : 'No');
				$extradata['_Causes0'] = $countries[$donation->details['donationcountry']]['shortname'];
				$extradata['_Whichareawouldyouliketodonateto'] = $countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projectname'];
				$extradata['_projectSubOption'] = $countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projects'][$donation->details['donationproject2']]['projectname'];
				$extradata['_administrationcost'] = floatval($donation->details['adminamount']);
				$extradata['_DonationAmount'] = floatval($donation->details['amount']);
				$extradata['_donorgiftaid'] = ($donation->details['giftaid'] ? 'Yes' : 'No');
				$extradata['_DonationType'] = $donation->details['donationtype'];
				$extradata['_Howwouldyouliketoreceiveupdates'] = $cpref->DisplayChosenPreferences($donation->details['contactpref'], ',', 'cpcode');
				$extradata['_AdminLink'] = SITE_URL . ADMIN_URL . '/donation.php?id=' . (int)$donation->id;
				
				if ($donation->details['donationtype'] == 'monthly')
				{	$invoicedata['contactID'] = $ifid;
					$invoicedata['name'] = 'Website monthly direct debit';
					$invoicedata['date'] = $donation->details['created'];
					
					$itemdata['gbpamount'] = $donation->details['amount'] + $donation->details['adminamount'];
					$itemdata['quantity'] = 1;
					$itemdata['description'] = 'Direct debit';
					$itemdata['notes'] = $countries[$donation->details['donationcountry']]['shortname'] . ', ' . $countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projectname'];
					
				} else
				{	if ($donation->details['currency'] == 'GBP')
					{	$gbpamount = $donation->details['amount'] + $donation->details['adminamount'];
					} else
					{	if ($convertrate = $this->GetCurrency($donation->details['currency'], 'convertrate'))
						{	$gbpamount = round(($donation->details['amount'] + $donation->details['adminamount']) / $convertrate, 2);
						}
					}
					if ($gbpamount)
					{	$invoicedata['contactID'] = $ifid;
						$invoicedata['name'] = 'Website one-off donation';
						$invoicedata['date'] = $donation->details['created'];
						
						$itemdata['gbpamount'] = $gbpamount;
						$itemdata['quantity'] = 1;
						$itemdata['description'] = 'One-off donation';
						$itemdata['notes'] = $countries[$donation->details['donationcountry']]['shortname'] . ', ' . $countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projectname'];
					}
				}
				if ($invoicedata && $itemdata)
				{	if ($invid = (int)$this->AddBlankInvoice($invoicedata))
					{	$updatesql = 'UPDATE donations SET infusionsoftid=' . $invid . ' WHERE did=' . $donation->id;
						$this->db->Query($updatesql);
						$this->AddDonationToInvoice($itemdata, $invid);
						if ($extradata)
						{	$this->AddExtraToInvoice($extradata, $invid);
						}
						$this->AddOptinTagsToContactFromDonation($donation, $ifid);
						return $invid;
					}
				}
			}
		}
	} // fn AddDonation
	
	public function AddExtraDataToDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id && ($invid = (int)$donation->details['infusionsoftid']))
		{	$extradata = array();
		
			$countries = $donation->GetDonationCountries();
			$cpref = new ContactPreferences();
			
			$extradata['_Currency0'] = $donation->details['currency'];
			$extradata['_IsyourdonationZakat'] = ($donation->details['zakat'] ? 'Yes' : 'No');
			$extradata['_Causes0'] = $countries[$donation->details['donationcountry']]['shortname'];
			$extradata['_Whichareawouldyouliketodonateto'] = $countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projectname'];
			$extradata['_projectSubOption'] = $countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projects'][$donation->details['donationproject2']]['projectname'];
			$extradata['_administrationcost'] = floatval($donation->details['adminamount']);
			$extradata['_DonationAmount'] = floatval($donation->details['amount']);
			$extradata['_donorgiftaid'] = ($donation->details['giftaid'] ? 'Yes' : 'No');
			$extradata['_DonationType'] = $donation->details['donationtype'];
			$extradata['_Howwouldyouliketoreceiveupdates'] = $cpref->DisplayChosenPreferences($donation->details['contactpref'], ',', 'cpcode');
			$extradata['_AdminLink'] = SITE_URL . ADMIN_URL . '/donation.php?id=' . (int)$donation->id;
			
			return $this->AddExtraToInvoice($extradata, $invid, true);
		}
	} // fn AddExtraDataToDonation
	
	public function AddOptinTagsToContactFromDonation($donation = 0, $contactid = 0)
	{	
		if (($donation = $this->DonationFromID($donation)) && $donation->id)
		{	if (!$contactid = (int)$contactid)
			{	$contactid = $this->AddContactFromDonation($donation);
			}
			if ($contactid)
			{	$cp = new ContactPreferences(array('phone', 'text', 'post'));
				$this->AddOptinTagsToContact($contactid, $cp, $donation->details['contactpref']);
			}
		}
	} // fn AddOptinTagsToContactFromDonation
	
	public function ConfirmDirectDebit($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id && ($donation->details['donationtype'] == 'monthly'))
		{	
			if ($ifid = $this->AddContactFromDonation($donation))
			{	
				if ($donation->details['donationref'])
				{	$extradata = array();
					$extradata['_DirectDebitConfirmed'] = true;
					$extradata['_ContractID'] = $donation->extrafields['ContractID'];
					$extradata['_CustomerID'] = $donation->extrafields['CustomerID'];
					$extradata['_DirectDebitRef'] = $donation->extrafields['DirectDebitRef'];
					$extradata['_StartDate2'] = date('Ymd', strtotime($donation->extrafields['StartDate']));
					$extradata['_StartDay'] = (int)$donation->extrafields['StartDay'];
					$extradata['_AccountHolder'] = $donation->extrafields['AccountHolder'];
					$extradata['_AccountNumber'] = $donation->extrafields['AccountNumber'];
					$extradata['_AccountSortCode'] = $donation->extrafields['AccountSortCode'];
					
					if (($invid = (int)$donation->details['infusionsoftid']) || ($invid = $this->AddDonation($donation)))
					{	$this->AddExtraToInvoice($extradata, $invid);
						$this->AddTagToContact($ifid, 'don_monthly');
						$this->AddDonationYearTagToContact($ifid, $donation->details['created']);
						if ($donation->details['adminamount'] > 0)
						{	$this->AddTagToContact($ifid, 'don_admin');
						}
						if ($donation->details['giftaid'])
						{	$this->AddTagToContact($ifid, 'don_giftaid');
						} else
						{	$this->RemoveTagFromContact($ifid, 'don_giftaid');
						}
						if ($donation->details['zakat'])
						{	$this->AddTagToContact($ifid, 'don_zakat');
						}
						return $payid;
					}
				}
			}
		}
	} // fn ConfirmDirectDebit
	
	public function PayDonation($donation = 0)
	{	if (($donation = $this->DonationFromID($donation)) && $donation->id)
		{	$paymentdata = array();
			
			if ($ifid = $this->AddContactFromDonation($donation))
			{	if ($donation->details['currency'] == 'GBP')
				{	$gbpamount = $donation->details['amount'] + $donation->details['adminamount'];
				} else
				{	if ($convertrate = $this->GetCurrency($donation->details['currency'], 'convertrate'))
					{	$gbpamount = round(($donation->details['amount'] + $donation->details['adminamount']) / $convertrate, 2);
					}
				}
				if ($donation->details['donationref'] && $gbpamount && ($donation->details['donationtype'] == 'oneoff'))
				{	
					$paymentdata['gbpamount'] = $gbpamount;
					$paymentdata['date'] = $donation->details['created'];
					$paymentdata['paytype'] = 'Credit card';
					$paymentdata['description'] = 'Website payment';
					
					if (($invid = (int)$donation->details['infusionsoftid']) || ($invid = $this->AddDonation($donation)))
					{	
						$payid = $this->AddPaymentToInvoice($paymentdata, $invid);
						$this->AddTagToContact($ifid, 'don_oneoff');
						$this->AddDonationYearTagToContact($ifid, $donation->details['created']);
						if ($donation->details['adminamount'] > 0)
						{	$this->AddTagToContact($ifid, 'don_admin');
						}
						if ($donation->details['giftaid'])
						{	$this->AddTagToContact($ifid, 'don_giftaid');
						} else
						{	$this->RemoveTagFromContact($ifid, 'don_giftaid');
						}
						if ($donation->details['zakat'])
						{	$this->AddTagToContact($ifid, 'don_zakat');
						}
						return $payid;
					}
				}
			}
		}
	} // fn PayDonation
	
	public function UploadDataFromPost($post = array())
	{	if ($post['donoremail'])
		{	$data = array(	'FirstName' => 	$post['donorfirstname'],
							'LastName' => 	$post['donorsurname'],
							'Title' => 		$post['donortitle'],
							'StreetAddress1' => $post['donoradd1'],
							'StreetAddress2' => $post['donoradd2'],
							'PostalCode' => $post['donorpostcode'],
							'City' => 		$post['donorcity'],
							'Email' => 		$post['donoremail'],
							'Website' =>	SITE_URL . ADMIN_URL . '/crm_email.php?email=' . urlencode($post['donoremail']),
							'Country' =>	$this->GetCountry($post['donorcountry'])
						);
			if ($post['donorphone'])
			{	$data['Phone1'] = $post['donorphone'];
			}
			return $data;
		}
	} // end of fn UploadDataFromPost
	
	protected function DonationFromID($donation = 0)
	{	if (is_a($donation, 'Donation'))
		{	return $donation;
		} else
		{	return new Donation($donation);
		}
	} // fn DonationFromID

} // end of defn InfusionSoftDonation
?>