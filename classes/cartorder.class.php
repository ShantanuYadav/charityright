<?php
class CartOrder extends BlankItem
{	protected $donations = array();
	protected $crsdonations = array();
	public $extrafields = array();
	
	public function __construct($id = 0)
	{	parent::__construct($id, 'cartorders', 'orderid');
	} // fn __construct

	public function ID($adddate = true)
	{	ob_start();
		echo 'CR_CART_', str_pad($this->id, 7, '0', STR_PAD_LEFT), $adddate ? date(' m/y ', strtotime($this->details['orderdate'])) : '', $this->details['giftaid'] ? ' GA' : '';
		return ob_get_clean();
	} // fn ResetExtra

	public function ResetExtra()
	{	$this->donations = array();
		$this->crsdonations = array();
		$this->extrafields = array();
	} // fn ResetExtra

	public function GetExtra()
	{	$this->GetMainDonations();
		$this->GetCRStarsDonations();
		$this->GetExtraFields();
	} // fn GetExtra
	
	protected function GetExtraFields()
	{	$this->extrafields = array();
		if ($orderid = (int)$this->id)
		{	$sql = 'SELECT order_extras.* FROM order_extras WHERE orderid=' . $orderid;
			if ($result = $this->db->Query($sql))
			{	while ($row = $this->db->FetchArray($result))
				{	$this->extrafields[$row['extrafield']] = $row['extravalue'];
				}
			}
		}
	} // fn GetExtraFields
	
	protected function GetMainDonations()
	{	if ($orderid = (int)$this->id)
		{	$tables = array('donations'=>'donations');
			$fields = array('donations.*');
			$where = array('orderid'=>'donations.orderid=' . $orderid);
			$orderby = array('did'=>'donations.did');
			$this->donations = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'did');
		} else
		{	$this->donations = array();
		}
	} // fn GetMainDonations
	
	protected function GetCRStarsDonations()
	{	if ($orderid = (int)$this->id)
		{	$tables = array('campaigndonations'=>'campaigndonations');
			$fields = array('campaigndonations.*');
			$where = array('orderid'=>'campaigndonations.orderid=' . $orderid);
			$orderby = array('cdid'=>'campaigndonations.cdid');
			$this->crsdonations = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cdid');
		} else
		{	$this->crsdonations = array();
		}
	} // fn GetCRStarsDonations
	
	public function MainDonations()
	{	return $this->donations;
	} // fn MainDonations
	
	public function CRStarsDonations()
	{	return $this->crsdonations;
	} // fn CRStarsDonations
	
	public function IsPaid()
	{	foreach ($this->donations as $donation)
		{	if ($donation['donationref'])
			{	return true;
			}
		}
		foreach ($this->crsdonations as $donation)
		{	if ($donation['donationref'])
			{	return true;
			}
		}
		return false;
	} // fn IsPaid

	public function HowHeardText()
	{	$howheard = '';
		if ($this->details['donorhowheard'])
		{	$sql = 'SELECT hfdesc FROM howfoundus WHERE hfvalue="' . $this->SQLSafe($this->details['donorhowheard']) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$howheard = $row['hfdesc'];
				}
			}
			if (!$howheard)
			{	$howheard = $this->details['donorhowheard'];
			}
			if ($this->details['donorhowheardtext'])
			{	$howheard .= ': ' . $this->details['donorhowheardtext'];
			}
		}
		return $howheard;
	} // fn HowHeardText
	
	public function CreateFromCart($data = array())
	{	$fail = array();
		$success = array();
		$fields = array('orderdate'=>'orderdate="' . ($now = $this->datefn->SQLDateTime()) . '"');
		$donation_base = array('created'=>'created="' . $now . '"');
		$crsdonation_base = array('donated'=>'donated="' . $now . '"');
		$donations = array();
		$crsdonations = array();
		$oneoff_amount = 0;
		$monthly_amount = 0;

		if ($this->GetCurrency($data['currency']))
		{	$fields['currency'] = 'currency="' . $data['currency'] . '"';
			$donation_base['donation_currency'] = $data['currency'];
			$crsdonation_base['donation_currency'] = $data['currency'];
		} else
		{	$fail[] = 'currency missing';
		}

		if ($donortitle = $this->SQLSafe($data['donor_title']))
		{	$fields['donortitle'] = 'donortitle="' . $donortitle . '"';
			$donation_base['donor_title'] = $data['donor_title'];
			$crsdonation_base['donor_title'] = $data['donor_title'];
		} else
		{	$fail[] = 'your title is missing';
		}
		if ($donorfirstname = $this->SQLSafe($data['donor_firstname']))
		{	$fields['donorfirstname'] = 'donorfirstname="' . $donorfirstname . '"';
			$donation_base['donor_firstname'] = $data['donor_firstname'];
			$crsdonation_base['donor_firstname'] = $data['donor_firstname'];
		} else
		{	$fail[] = 'first name missing';
		}
		if ($donorsurname = $this->SQLSafe($data['donor_lastname']))
		{	$fields['donorsurname'] = 'donorsurname="' . $donorsurname . '"';
			$donation_base['donor_lastname'] = $data['donor_lastname'];
			$crsdonation_base['donor_lastname'] = $data['donor_lastname'];
		} else
		{	$fail[] = 'first name missing';
		}

		if ($data['donor_country'] && $this->GetCountry($data['donor_country']))
		{	$fields['donorcountry'] = 'donorcountry="' . $data['donor_country'] . '"';
			$donation_base['donor_country'] = $data['donor_country'];
			$crsdonation_base['donor_country'] = $data['donor_country'];
			if ($data['donor_country'] == 'GB')
			{	if ($data['donor_giftaid'])
				{	$fields['giftaid'] = 'giftaid=1';
					$donation_base['donor_giftaid'] = 1;
					$crsdonation_base['donor_giftaid'] = 1;
				}
				$add_prefix = 'pca_';
			}

			if ($donoradd3 = $this->SQLSafe($data[$add_prefix . 'donor_address3']))
			{	$donoradd1lines = array();
				if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
				{	$donoradd1lines[] = $donoradd1;
				}
				if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
				{	$donoradd1lines[] = $donoradd2;
				}
				if ($donoradd1lines)
				{	$fields['donoradd1'] = 'donoradd1="' . ($donoradd1 = implode(', ', $donoradd1lines)) . '"';
					$donation_base[$add_prefix . 'donor_address1'] = $donoradd1;
					$crsdonation_base[$add_prefix . 'donor_address1'] = $donoradd1;
				}
				$fields['donoradd2'] = 'donoradd2="' . $donoradd3 . '"';
				$donation_base[$add_prefix . 'donor_address2'] = $donoradd3;
				$crsdonation_base[$add_prefix . 'donor_address2'] = $donoradd3;
				$address_done++;
			} else
			{	if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
				{	$fields['donoradd1'] = 'donoradd1="' . $donoradd1 . '"';
					$donation_base[$add_prefix . 'donor_address1'] = $donoradd1;
					$crsdonation_base[$add_prefix . 'donor_address1'] = $donoradd1;
					$address_done++;
				}
				if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
				{	$fields['donoradd2'] = 'donoradd2="' . $donoradd2 . '"';
					$donation_base[$add_prefix . 'donor_address2'] = $donoradd2;
					$crsdonation_base[$add_prefix . 'donor_address2'] = $donoradd2;
					$address_done++;
				}
			}
			if (!$address_done)
			{	$fail[] = 'address missing';
			}

			if ($donorpostcode = $this->SQLSafe($data[$add_prefix . 'donor_postcode']))
			{	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
				$donation_base[$add_prefix . 'donor_postcode'] = $donorpostcode;
				$crsdonation_base[$add_prefix . 'donor_postcode'] = $donorpostcode;
			} else
			{	$fail[] = 'post code missing';
			}
			if ($donorcity = $this->SQLSafe($data[$add_prefix . 'donor_city']))
			{	$fields['donorcity'] = 'donorcity="' . $donorcity . '"';
				$donation_base[$add_prefix . 'donor_city'] = $donorcity;
				$crsdonation_base[$add_prefix . 'donor_city'] = $donorcity;
			} else
			{	$fail[] = 'city/town missing';
			}
		} else
		{	$fail[] = 'country missing';
		}

		if ($donoremail = $this->SQLSafe($data['donor_email']))
		{	$fields['donoremail'] = 'donoremail="' . $donoremail . '"';
			$donation_base['donor_email'] = $data['donor_email'];
			$crsdonation_base['donor_email'] = $data['donor_email'];
		} else
		{	$fail[] = 'email missing';
		}
		$fields['donorphone'] = 'donorphone="' . $this->SQLSafe($data['donor_phone']) . '"';
		$donation_base['donor_phone'] = $data['donor_phone'];
		$crsdonation_base['donor_phone'] = $data['donor_phone'];
		
		$fields['contactpref'] = 'contactpref=' . (int)$data['contactpref'];
		$donation_base['contactpref'] = (int)$data['contactpref'];
		$crsdonation_base['contactpref'] = (int)$data['contactpref'];

		//$fields['ddsolesig'] = 'ddsolesig=' . ($data['dd_confirm'] ? '1' : '0');
	 	$fields['ddsolesig'] = 'ddsolesig=1';

		$fields['donorhowheard'] = 'donorhowheard="' . $this->SQLSafe($data['donor_howheard']) . '"';
		$fields['donorhowheardtext'] = 'donorhowheardtext="' . $this->SQLSafe($data['donor_howheardtext']) . '"';
	 
	
		// now go through main donations
		if ($data['donations'] && is_array($data['donations']))
		{	foreach ($data['donations'] as $donation)
			{	$row = $donation_base;
				if (($row['donation_type'] = $donation['donation_type']) == 'monthly')
				{	$monthly_amount += $donation['donation_amount'];
					$monthly_amount += $donation['donation_admin'];
					$row['dd_confirm'] = $data['dd_confirm'];
				} else
				{	$oneoff_amount += $donation['donation_amount'];
					$oneoff_amount += $donation['donation_admin'];
				}
				$row['donation_amount'] = $donation['donation_amount'];
				$row['donation_admin'] = $donation['donation_admin'];
				$row['donation_country'] = $donation['donation_country'];
				$row['donation_project'] = $donation['donation_project'];
				$row['donation_project2'] = $donation['donation_project2'];
				$row['donation_zakat'] = $donation['donation_zakat'];
				$row['donation_quantity'] = $donation['donation_quantity'];
				$donations[] = $row;
			}
		}
		
		// now go through cr stars donations
		if ($data['crsdonations'] && is_array($data['crsdonations']))
		{	foreach ($data['crsdonations'] as $donation)
			{	$row = $crsdonation_base;
				if (($row['campaign'] = new Campaign($donation['cid'])) && $row['campaign']->id)
				{	$oneoff_amount += ($row['donation_amount'] = $donation['donation_amount']);
					$row['donor_comment'] = $donation['donor_comment'];
					$row['donor_anon'] = $donation['donor_anon'];
					$row['donor_showto'] = $donation['donor_showto'];
					$row['donation_zakat'] = $donation['donation_zakat'];
					$crsdonations[] = $row;
					
				} else
				{	$fail[] = 'Cr Stars Campaign Not Found';
				}
			}
		}
		
		$fields['total_oneoff'] = 'total_oneoff=' . round($oneoff_amount, 2);
		$fields['total_monthly'] = 'total_monthly=' . round($monthly_amount, 2);
		
		//$fail[] = 'test ' . count($fields) . ':' . count($donation_base) . ':' . count($crsdonation_base) . ':' . count($donations) . ':' . count($crsdonations);
		
		echo '<pre>';print_r($crsdonations);die;
		if ($donations || $crsdonations)
		{	if (!$fail)
			{	$sql = 'INSERT INTO '. $this->tablename . ' SET ' . implode(', ', $fields);
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
					{	$success[] = 'New order created';
						if ($donations)
						{	foreach ($donations as $donation_data)
							{	$donation_data['orderid'] = $id;
								$donation = new Donation();
								$donsaved = $donation->CreateFromSession($donation_data);
								if ($donsaved['failmessage'])
								{	$fail[] = 'Donation: ' . $donsaved['failmessage'];
								}
							}
						}
						if ($crsdonations)
						{	foreach ($crsdonations as $donation_data)
							{	$donation_data['orderid'] = $id;
								$donation = new CampaignDonation();
								$donsaved = $donation->Create($donation_data, $donation_data['campaign']);
								if ($donsaved['failmessage'])
								{	$fail[] = 'CR Stars Donation :' . $donsaved['failmessage'];
								}
							}
						}
						$this->Get($id);
						$_SESSION[$this->cart_session_name]['orderid'] = $this->id;
					}
					//$fail[] = 'saved: ' . $sql;
				} else $fail[] = $sql . ': ' . $this->db->Error();
			}
		} else
		{	$fail[] = 'No donations';
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn CreateFromCart

	public function CreateFromCartNew($data = array())
	{	
	
		//$this->VarDump($data);die;
		$fail = array();
		$success = array();
		$fields = array('orderdate'=>'orderdate="' . ($now = $this->datefn->SQLDateTime()) . '"');
		$donation_base = array('created'=>'created="' . $now . '"');
		$crsdonation_base = array('donated'=>'donated="' . $now . '"');
		$donations = array();
		$crsdonations = array();
		$oneoff_amount = 0;
		$monthly_amount = 0;
	 
	 	

		if ($this->GetCurrency($data['currency']))
		{	$fields['currency'] = 'currency="' . $data['currency'] . '"';
			$donation_base['donation_currency'] = $data['currency'];
			$crsdonation_base['donation_currency'] = $data['currency'];
		} else
		{	$fail[] = 'currency missing';
		}

		if ($donortitle = $this->SQLSafe($data['donor_title']))
		{	$fields['donortitle'] = 'donortitle="' . $donortitle . '"';
			$donation_base['donor_title'] = $data['donor_title'];
			$crsdonation_base['donor_title'] = $data['donor_title'];
		} else
		{	$fail[] = 'your title is missing';
		}
		if ($donorfirstname = $this->SQLSafe($data['donor_firstname']))
		{	$fields['donorfirstname'] = 'donorfirstname="' . $donorfirstname . '"';
			$donation_base['donor_firstname'] = $data['donor_firstname'];
			$crsdonation_base['donor_firstname'] = $data['donor_firstname'];
		} else
		{	$fail[] = 'first name missing';
		}
		
		if ($donorsurname = $this->SQLSafe($data['donor_lastname']))
		{	$fields['donorsurname'] = 'donorsurname="' . $donorsurname . '"';
			$donation_base['donor_lastname'] = $data['donor_lastname'];
			$crsdonation_base['donor_lastname'] = $data['donor_lastname'];
		} else
		{	$fail[] = 'last name missing';
		}
		

		if ($data['donor_country'] && $this->GetCountry($data['donor_country']))
		{	$fields['donorcountry'] = 'donorcountry="' . $data['donor_country'] . '"';
			$donation_base['donor_country'] = $data['donor_country'];
			$crsdonation_base['donor_country'] = $data['donor_country'];
			if ($data['donor_country'] == 'GB')
			{	
				if ($data['donor_giftaid'])
				{	$fields['giftaid'] = 'giftaid=1';
					$donation_base['donor_giftaid'] = 1;
					$crsdonation_base['donor_giftaid'] = 1;
				}
				$add_prefix = 'pca_';
			}


			if ($donor_address1 = $this->SQLSafe($data['billing_address']))
			{	$fields['donoradd1'] = 'donoradd1="' . $donor_address1 . '"';
				$donation_base[$add_prefix . 'donor_address1'] = $donor_address1;
				$crsdonation_base[$add_prefix . 'donor_address1'] = $donor_address1;
			} else
			{	$fail[] = 'address missing';
			}

			if ($donorpostcode = $this->SQLSafe($data['billing_post_code']))
			{	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
				$donation_base[$add_prefix . 'donor_postcode'] = $donorpostcode;
				$crsdonation_base[$add_prefix . 'donor_postcode'] = $donorpostcode;
			} else
			{	$fail[] = 'post code missing';
			}

			// if ($donoradd3 = $this->SQLSafe($data[$add_prefix . 'donor_address3']))
			// {	$donoradd1lines = array();
			// 	if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
			// 	{	$donoradd1lines[] = $donoradd1;
			// 	}
			// 	if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
			// 	{	$donoradd1lines[] = $donoradd2;
			// 	}
			// 	if ($donoradd1lines)
			// 	{	$fields['donoradd1'] = 'donoradd1="' . ($donoradd1 = implode(', ', $donoradd1lines)) . '"';
			// 		$donation_base[$add_prefix . 'donor_address1'] = $donoradd1;
			// 		$crsdonation_base[$add_prefix . 'donor_address1'] = $donoradd1;
			// 	}
			// 	$fields['donoradd2'] = 'donoradd2="' . $donoradd3 . '"';
			// 	$donation_base[$add_prefix . 'donor_address2'] = $donoradd3;
			// 	$crsdonation_base[$add_prefix . 'donor_address2'] = $donoradd3;
			// 	$address_done++;
			// } else
			// {	if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
			// 	{	$fields['donoradd1'] = 'donoradd1="' . $donoradd1 . '"';
			// 		$donation_base[$add_prefix . 'donor_address1'] = $donoradd1;
			// 		$crsdonation_base[$add_prefix . 'donor_address1'] = $donoradd1;
			// 		$address_done++;
			// 	}
			// 	if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
			// 	{	$fields['donoradd2'] = 'donoradd2="' . $donoradd2 . '"';
			// 		$donation_base[$add_prefix . 'donor_address2'] = $donoradd2;
			// 		$crsdonation_base[$add_prefix . 'donor_address2'] = $donoradd2;
			// 		$address_done++;
			// 	}
			// }
			// if (!$address_done)
			// {	$fail[] = 'address missing';
			// }

			// if ($donorpostcode = $this->SQLSafe($data[$add_prefix . 'donor_postcode']))
			// {	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
			// 	$donation_base[$add_prefix . 'donor_postcode'] = $donorpostcode;
			// 	$crsdonation_base[$add_prefix . 'donor_postcode'] = $donorpostcode;
			// } else
			// {	$fail[] = 'post code missing';
			// }
			// if ($donorcity = $this->SQLSafe($data[$add_prefix . 'donor_city']))
			// {	$fields['donorcity'] = 'donorcity="' . $donorcity . '"';
			// 	$donation_base[$add_prefix . 'donor_city'] = $donorcity;
			// 	$crsdonation_base[$add_prefix . 'donor_city'] = $donorcity;
			// } else
			// {	$fail[] = 'city/town missing';
			// }
			
			
		} else
		{	$fail[] = 'country missing';
		}

		if ($donoremail = $this->SQLSafe($data['donor_email']))
		{	$fields['donoremail'] = 'donoremail="' . $donoremail . '"';
			$donation_base['donor_email'] = $data['donor_email'];
			$crsdonation_base['donor_email'] = $data['donor_email'];
		} else
		{	$fail[] = 'email missing';
		}
		// $fields['donorphone'] = 'donorphone="' . $this->SQLSafe($data['donor_phone']) . '"';
		// $donation_base['donor_phone'] = $data['donor_phone'];
		// $crsdonation_base['donor_phone'] = $data['donor_phone'];
		
		// $fields['contactpref'] = 'contactpref=' . (int)$data['contactpref'];
		// $donation_base['contactpref'] = (int)$data['contactpref'];
		// $crsdonation_base['contactpref'] = (int)$data['contactpref'];

		$fields['ddsolesig'] = 'ddsolesig=' . ($data['dd_confirm'] ? '1' : '0');

		// $fields['donorhowheard'] = 'donorhowheard="' . $this->SQLSafe($data['donor_howheard']) . '"';
		// $fields['donorhowheardtext'] = 'donorhowheardtext="' . $this->SQLSafe($data['donor_howheardtext']) . '"';
		
		// now go through main donations
		if ($data['donations'] && is_array($data['donations']))
		{	foreach ($data['donations'] as $donation)
			{	$row = $donation_base;
				if (($row['donation_type'] = $donation['donation_type']) == 'monthly')
				{	$monthly_amount += $donation['donation_amount'];
					$monthly_amount += $donation['donation_admin'];
					// $row['dd_confirm'] = $data['dd_confirm'];
				} else
				{	$oneoff_amount += $donation['donation_amount'];
					$oneoff_amount += $donation['donation_admin'];
				}
				$row['donation_amount'] = $donation['donation_amount'];
				$row['donation_admin'] = $donation['donation_admin'];
				$row['donation_country'] = $donation['donation_country'];
				$row['donation_project'] = $donation['donation_project'];
				$row['donation_project2'] = $donation['donation_project2'];
				$row['donation_zakat'] = $donation['donation_zakat'];
				$row['donation_quantity'] = $donation['donation_quantity'];
				$donations[] = $row;
			}
		}
		
		// now go through cr stars donations
		if ($data['crsdonations'] && is_array($data['crsdonations']))
		{	foreach ($data['crsdonations'] as $donation)
			{	$row = $crsdonation_base;
				if (($row['campaign'] = new Campaign($donation['cid'])) && $row['campaign']->id)
				{	$oneoff_amount += ($row['donation_amount'] = $donation['donation_amount']);
					$row['donor_comment'] = $donation['donor_comment'];
					$row['donor_anon'] = $donation['donor_anon'];
					$row['donor_showto'] = $donation['donor_showto'];
					$row['donation_zakat'] = $donation['donation_zakat'];
					$crsdonations[] = $row;
					
				} else
				{	$fail[] = 'Cr Stars Campaign Not Found';
				}
			}
		}
		
		$fields['total_oneoff'] = 'total_oneoff=' . round($oneoff_amount, 2);
		$fields['total_monthly'] = 'total_monthly=' . round($monthly_amount, 2);
		
		//$fail[] = 'test ' . count($fields) . ':' . count($donation_base) . ':' . count($crsdonation_base) . ':' . count($donations) . ':' . count($crsdonations);
		// echo '<pre>';print_r($crsdonations);die;

		if ($donations || $crsdonations)
		{	if (!$fail)
			{	$sql = 'INSERT INTO '. $this->tablename . ' SET ' . implode(', ', $fields);
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
					{	$success[] = 'New order created';
						if ($donations)
						{	foreach ($donations as $donation_data)
							{	$donation_data['orderid'] = $id;
								$donation = new Donation();
								$donsaved = $donation->CreateFromSessionNew($donation_data);
								if ($donsaved['failmessage'])
								{	$fail[] = 'Donation: ' . $donsaved['failmessage'];
								}
							}
						}
						if ($crsdonations)
						{	foreach ($crsdonations as $donation_data)
							{	$donation_data['orderid'] = $id;
								$donation = new CampaignDonation();
								$donsaved = $donation->Createnew($donation_data, $donation_data['campaign']);
								if ($donsaved['failmessage'])
								{	$fail[] = 'CR Stars Donation :' . $donsaved['failmessage'];
								}
							}
						}
						$this->Get($id);
						$_SESSION[$this->cart_session_name]['orderid'] = $this->id;
					}
					//$fail[] = 'saved: ' . $sql;
				} else $fail[] = $sql . ': ' . $this->db->Error();
			}
		} else
		{	$fail[] = 'No donations';
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn CreateFromCart
	
	public function WorldpayParams()
	{	$params = array('desc'=>'', 'amount'=>0, 'items'=>0);
		foreach ($this->donations as $donation_row)
		{	$donation = new Donation($donation_row);
			if ($donation->CanBePaid() && ($donation->details['donationtype'] != 'monthly'))
			{	$params['amount'] += $donation->details['amount'];
				$params['amount'] += $donation->details['adminamount'];
				$params['adminamount'] += $donation->details['adminamount'];
				if (!$params['items']++)
				{	$params['desc'] = $donation->GatewayTitle();
				}
			}
		}
		foreach ($this->crsdonations as $donation_row)
		{	$donation = new CampaignDonation($donation_row);
			if ($donation->CanBePaid())
			{	$params['amount'] += $donation->details['amount'];
				if (!$params['items']++)
				{	$campaign = new Campaign($donation->details['campaignid']);
					$params['desc'] = strip_tags($campaign->FullTitle());
				}
			}
		}
		if ($params['items'] > 1)
		{	$params['desc'] = 'for ' . $params['items'] . ' single donations';
		}
		return $params;
	} // fn WorldpayParams
	
	public function DirectDebitParams()
	{	$params = array('desc'=>'', 'amount'=>0, 'items'=>0);
		foreach ($this->donations as $donation_row)
		{	$donation = new Donation($donation_row);
			if ($donation->CanBePaid() && ($donation->details['donationtype'] == 'monthly'))
			{	$params['amount'] += $donation->details['amount'];
				$params['amount'] += $donation->details['adminamount'];
				$params['adminamount'] += $donation->details['adminamount'];
				if (!$params['items']++)
				{	$params['desc'] = $donation->GatewayTitle();
				}
			}
		}
		if ($params['items'] > 1)
		{	$params['desc'] = $params['items'] . ' monthly donations';
		}
		return $params;
	} // fn DirectDebitParams
	
	public function PaymentButton()
	{	$wp = new WorldPayRedirect();
		echo $wp->CartOrderButton($this);
	} // fn PaymentButton

	public function ThankYouLink()
	{	$thankyoupage = new PageContent('donate-thank-you');
		return $thankyoupage->FullLink();
	} // end of fn ThankYouLink

	public function CancelLink()
	{	$cartpage = new PageContent('cart');
		return $cartpage->FullLink();
	} // end of fn CancelLink

	public function UpdateFromWorldPayPost($post = array())
	{	
		$fail = array();
		$success = array();
		
		foreach ($this->donations as $donation_row)
		{	$donation = new Donation($donation_row);
			if ($donation->CanBePaid() && ($donation->details['donationtype'] != 'monthly'))
			{	$post['amount'] = ($donation->details['amount'] + $donation->details['adminamount']);
				$result = $donation->UpdateOneOffFromWorldPayPost($post);
				if ($this->failmessage = $result['failmessage'])
				{	$fail[] = 'donation #' . $donation->id . ': ' . $result['failmessage'];
				}
			}
		}
		foreach ($this->crsdonations as $donation_row)
		{	$donation = new CampaignDonation($donation_row);
			if ($donation->CanBePaid())
			{	$post['amount'] = $donation->details['amount'];
				$result = $donation->UpdateFromWorldPayPost($post);
				if ($this->failmessage = $result['failmessage'])
				{	$fail[] = 'cr stars donation #' . $donation->id . ' : ' . $result['failmessage'];
				}
			}
		}
		
		$this->GetMainDonations();
		if ($this->QurbaniPaid())
		{	$this->SendQurbaniEmail();
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn UpdateFromWorldPayPost
	
	public function QurbaniPaid()
	{	foreach ($this->donations as $donation_row)
		{	if ($donation_row['donationref'] && ($donation_row['donationcountry'] == 'cr_130'))
			{	return true;
			}
		}
	} // fn QurbaniPaid
	
	public function SendQurbaniEmail($email = '')
	{	$mail = new HTMLMail();
		ob_start();
		echo '<p>Salaam,</p>
<p>We\'ve received your Qurbani donation and we want to say <b>thank you</b>.</p>
<p>This Eid, your donation will give vulnerable families the chance to taste meat again - something they can\'t afford throughout the rest of the year.</p>
<p>Did you know that you can also <b>feed an entire family for a month</b> by donating one of our family food packs?</p>
<p>All you have to do is add &pound;35 to your generous Qurbani donation.</p>
<p><a href="https://charityright.org.uk/feedafamily">I want to feed a family.</a></p>
<p>P.S. Don\'t forget to look out for your text on Eid day. We\'ll be in touch to let you know your sacrifice has been performed.</p>';
		$body = ob_get_clean();
		$mail->SetSubject('You\'ve just made someone\'s Eid');
		$mail->SendFromTemplate($email ? $email : $this->details['donoremail'], array('main_content'=>$body), 'default');
	} // fn SendQurbaniEmail
	
	public function CanPayOneOffDonations()
	{	$donation_count = 0;
		foreach ($this->donations as $donation_row)
		{	if ($donation_row['donationtype'] != 'monthly')
			{	$donation = new Donation($donation_row);
				if ($donation->CanBePaid())
				{	$donation_count++;
				} else
				{	return false;
				}
			}
		}
		foreach ($this->crsdonations as $donation_row)
		{	$donation = new CampaignDonation($donation_row);
			if ($donation->CanBePaid())
			{	$donation_count++;
			} else
			{	return false;
			}
		}
		return $donation_count;
	} // fn CanPayOneOffDonations
	
	public function CanPayDirectDebitDonations()
	{	$donation_count = 0;
		foreach ($this->donations as $donation_row)
		{	if ($donation_row['donationtype'] == 'monthly')
			{	$donation = new Donation($donation_row);
				if ($donation->CanBePaid())
				{	$donation_count++;
				} else
				{	return false;
				}
			}
		}
		return $donation_count;
	} // fn CanPayDirectDebitDonations

	public function IsMonthlyUnconfirmed()
	{	$unconfirmed_count = 0;
		foreach ($this->donations as $donation_row)
		{	if ($donation_row['donationtype'] == 'monthly')
			{	$donation = new Donation($donation_row);
				if ($donation->IsMonthlyUnconfirmed())
				{	$unconfirmed_count++;
				} else
				{	return false;
				}
			}
		}
		return $unconfirmed_count;
	} // end of fn IsMonthlyUnconfirmed

	public function firstUriSegment(){
		$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
		return @$uriSegments['1'];
	}

	public function getSessionDonationForAnalytics(){
		$donated_amount_total = [];
		$currency = '';
		$donation_id = 0;
		$quantity = 0;
		foreach ($this->donations as $donation_row){
			$donation = new Donation($donation_row);
			$donation_id = $donation->ID();
			$quantity += (float)$donation->details['quantity'];
			$amount = $donation->details['amount']+$donation->details['adminamount'];
			$currency = $donation->details['currency'];

			if(!isset($donated_amount_total[$currency])){
				$donated_amount_total[$currency] = 0;
			}
			$donated_amount_total[$currency] += $amount;
		}

		foreach ($this->crsdonations as $donation_row)
		{	
			$donation = new CampaignDonation($donation_row);
			$donation_id = $donation->ID();
			$quantity += (float)$donation->details['quantity'];
			$amount = $donation->details['amount']+$donation->details['adminamount'];
			$currency = $donation->details['currency'];

			if(!isset($donated_amount_total[$currency])){
				$donated_amount_total[$currency] = 0;
			}
			$donated_amount_total[$currency] += $amount;
		}

		$amount = @round($donated_amount_total[$currency],2);
		$quantity = $quantity>0?$quantity:1;

		$totals = @['quantity' => $quantity,'amount' => $amount, 'currency' => $currency,'donation_id' => $donation_id,'time'=> date('Y-m-d h:i A')];
		if(!empty($donation_id) && $this->firstUriSegment() == 'donate-thank-you'){
			$txt = json_encode($totals).' - '.json_encode($donated_amount_total).' FROMIP: '.$_SERVER['REMOTE_ADDR'];
			file_put_contents('logs.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
		}
		return $totals;
	}

	public function PintrestTagPurchaseCode(){
		ob_start();
		if($this->firstUriSegment() == 'donate-thank-you'){

			$donation_total = $this->getSessionDonationForAnalytics();

			echo "<script>
			pintrk('track', 'checkout', {
			order_id: '",$donation_total['donation_id'],"',
			value: ",$donation_total['amount'],",
			order_quantity: ",$donation_total['quantity'],",
			currency: '",$donation_total['currency'],"'
			});
			</script>";
		}
		return ob_get_clean();
	}

	public function SnapPixelCode(){

		ob_start();
		if($this->firstUriSegment() == 'donate-thank-you'){

			$donation_total = $this->getSessionDonationForAnalytics();
			$user_email = @$this->details['donoremail'];

			echo "<!-- Snap Pixel Code -->
			<script type='text/javascript'>
			(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
			{a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
			a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
			r.src=n;var u=t.getElementsByTagName(s)[0];
			u.parentNode.insertBefore(r,u);})(window,document,
			'https://sc-static.net/scevent.min.js');
			
			snaptr('init', 'e3fc8fa5-d8f9-47b6-a384-1859c44e5594', {
			'user_email': '",$user_email,"',
			'user_hashed_email': '",hash('sha256',$user_email),"',
			});
			
			snaptr('track','PURCHASE',{'currency':'",$donation_total['currency'],"','price':",$donation_total['amount'],",'transaction_id':'",$donation_total['donation_id'],"'});
			
			</script>
			<!-- End Snap Pixel Code -->";
		}
		return ob_get_clean();
	}

	public function AdRollPurchaseValueCode(){
		ob_start();
		$donation_total = $this->getSessionDonationForAnalytics();
		echo '<script type="text/javascript">
				adroll_conversion_value = ',$donation_total['amount'],';
				adroll_currency = "',$donation_total['currency'],'";
			  </script>';
		return ob_get_clean();
	}

	public function TwitterPurchaseCode(){
		ob_start();
		if($this->firstUriSegment() == 'donate-thank-you'){

			$donation_total = $this->getSessionDonationForAnalytics();

			echo "<!-- Twitter universal website tag code -->
			<script>
			!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
			},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
			a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
			// Insert Twitter Pixel ID and Standard Event data below
			twq('init','nv0yr');
			twq('track','Purchase', {
				//required parameters
				value: '",$donation_total['amount'],"',
				currency: '",$donation_total['currency'],"',
			});
			</script>
			<!-- End Twitter universal website tag code -->";
		}
		return ob_get_clean();
	}
 
 	public function FBPixelTrackPurchaseCode(){
		$donation_total = $this->getSessionDonationForAnalytics();
		ob_start();
		//echo "<script>fbq('track', 'Purchase',{currency: '",$donation_total['currency'],"', value: ",$donation_total['amount'],"});</script>";
		return ob_get_clean();
	}

	public function GAConversionCode(){
		$donation_total = $this->getSessionDonationForAnalytics();
		ob_start();
		echo "<!-- Event snippet for Donation Complete conversion page -->
		<script>
		  gtag('event', 'conversion', {
			  'send_to': 'AW-925998688/rJJRCO2R530Q4LzGuQM',
			  'value': ",$donation_total['amount'],",
			  'currency': '",$donation_total['currency'],"',
			  'transaction_id': '",$donation_total['donation_id'],"'
		  });
		</script>
		<script>
		  gtag('event', 'conversion', {
			  'send_to': 'AW-876060108/jTMXCL7FvM8BEMy73qED',
			  'value': ",$donation_total['amount'],",
			  'currency': '",$donation_total['currency'],"',
			  'transaction_id': '",$donation_total['donation_id'],"'
		  });
		</script>
		";
		return ob_get_clean();
	}

	public function GASingleTransactionTrackingCode()
	{	
		
		// if (false || !SITE_TEST){
			
			$items = array();
			$totalamount = 0;
			foreach ($this->donations as $donation_row)
			{	if ($donation_row['donationtype'] != 'monthly')
				{	$donation = new Donation($donation_row);
					$totalamount += $donation->details['amount'];
					$totalamount += $donation->details['adminamount'];
					ob_start();
					echo "ga('ecommerce:addItem', {               // Provide product details in an productFieldObject.
						 	'id': '", $donation->ID() ,"',
						 	'name': '",$donation->details['donationtype']," donation',
						 	'price': '",round($donation->details['amount'] + $donation->details['adminamount'], 2),"',
						 	'quantity': ",$donation->details['quantity'] ? $donation->details['quantity'] : '1',"
					     });";
					$items[] = ob_get_clean();
				}
			}
			foreach ($this->crsdonations as $donation_row)
			{	$donation = new CampaignDonation($donation_row);
				$totalamount += $donation->details['amount'];
				ob_start();
				echo "ga('ecommerce:addItem', {               // Provide product details in an productFieldObject.
					'id': '", $donation->ID() ,"',
					'name': 'Cr Stars Donation',
					'price': '",round($donation->details['amount'], 2),"',
					'quantity': '1'
				});";
				$items[] = ob_get_clean();
			}
			if ($items)
			{	ob_start();
				echo "
				<script type=\"text/javascript\">
					ga('require', 'ecommerce');
					ga('ecommerce:addTransaction', {          // Transaction details are provided in an actionFieldObject.
						'id': '",$this->ID(),"', 
						'affiliation': 'Charity Right', 
						'revenue': '",round($totalamount, 2),"', 
					});",implode('', $items), 'ga("ecommerce:send")',"
				</script>";
				return ob_get_clean();
			}
		// } // end if SITE_TEST
	} // fn GASingleTransactionTrackingCode

	public function GAMonthlyTransactionTrackingCode()
	{	if (true || !SITE_TEST)
		{	$items = array();
			$totalamount = 0;
			foreach ($this->donations as $donation_row)
			{	if ($donation_row['donationtype'] == 'monthly')
				{	$donation = new Donation($donation_row);
					$totalamount += $donation->details['amount'];
					$totalamount += $donation->details['adminamount'];
					ob_start();
					echo 'ga("ecommerce:addItem", {
"id": "', $donation->ID(), ' DD",
"name": "', $donation->details['donationtype'], ' donation",
"price": "', round($donation->details['amount'] + $donation->details['adminamount'], 2), '",
"quantity": "', $donation->details['quantity'] ? $donation->details['quantity'] : '1', '"
});
';
					$items[] = ob_get_clean();
				}
			}
			if ($items)
			{	ob_start();
				echo '
<script type="text/javascript">
ga("require", "ecommerce");
ga("ecommerce:addTransaction", {
"id": "', $id = $this->ID(), ' DD",
"affiliation": "Charity Right",
"revenue": "', round($totalamount, 2), '",
"currency": "', $this->details['currency'], '"
});
', implode('', $items), '
ga("ecommerce:send");
</script>
';
				return ob_get_clean();
			}// else return '<h3>--- no monthly donations apparently ---</h3>';
		}
	} // fn GAMonthlyTransactionTrackingCode

	public function NextWorkingDay($starttime = '', $days_on = 10)
	{	if ($starttime)
		{	if ((int)$starttime != $starttime)
			{	$starttime = strtotime($starttime);
			}
		} else
		{	$starttime = time();
		}

		$endtime = $starttime + ($this->datefn->secInHour * 6);
		if ($days_on = (int)$days_on)
		{	$workingdays = 0;
			$bankhols = $this->GetBankHols();
			while ($workingdays < $days_on)
			{	$endtime += $this->datefn->secInDay;
				if ((date('N', $endtime) < 6) && !$bankhols[$this->datefn->SQLDate($endtime)])
				{	$workingdays++;
				}
			}
		}
		return $endtime;
	} // end of fn NextWorkingDay

	public function AddressOutput($sep = '<br />')
	{	$address = array();
		if ($this->details['donoradd1'])
		{	$address[] = $this->InputSafeString($this->details['donoradd1']);
		}
		if ($this->details['donoradd2'])
		{	$address[] = $this->InputSafeString($this->details['donoradd2']);
		}
		if ($this->details['donorcity'])
		{	$address[] = $this->InputSafeString($this->details['donorcity']);
		}
		if ($this->details['donorpostcode'])
		{	$address[] = $this->InputSafeString($this->details['donorpostcode']);
		}
		if ($country = $this->GetCountry($this->details['donorcountry']))
		{	$address[] = $this->InputSafeString($country);
		}
		return implode($sep, $address);
	} // end of fn AddressOutput

	public function RecordUnpaidDirectDebit($data = array())
	{	$fail = array();
		$success = array();
		$extras = array();

		$extras['AccountHolder'] = $data['dd_accountname'];
		$extras['AccountNumber'] = $data['dd_accountnumber'];
		$extras['AccountSortCode'] = $data['dd_accountsortcode'];
	
		$startdate = $this->NextWorkingDay();
		$paymentDayInMonth = date('j', $startdate);
		if ($paymentDayInMonth <= 15)
		{	$startdate = mktime(0,0,0,date('n', $startdate), $paymentDayInMonth = 15, date('Y', $startdate));
		} else
		{	$startdate = mktime(0,0,0,date('n', $startdate) + 1, $paymentDayInMonth = 1, date('Y', $startdate));
		}
		
		$extras['StartDate'] = $this->datefn->SQLDate($startdate);
		$extras['StartDay'] = $paymentDayInMonth;

		foreach ($extras as $name=>$value)
		{	echo $extras_sql = 'INSERT INTO order_extras SET orderid=' . $this->id  . ', extrafield="' . $this->SQLSafe($name) . '", extravalue="' . $this->SQLSafe($value) . '"';
			$this->db->Query($extras_sql);
		}

		$this->Get($this->id);
		$this->SendDirectDebitForm();
		$success[] = 'Direct debit record created';

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn RecordUnpaidDirectDebit

	public function SendDirectDebitForm()
	{	ob_start();
		echo '<p>In order to set up the direct debit for your monthly donation to Charity Right please print and complete the following form. This should then be sent to the address on the form.</p>';
		$body = ob_get_clean();
		$mail = new HTMLMail();
		$mail->SetSubject('The Next Step in Your Direct Debit Setup');
		$mail->SendFromTemplate($this->details['donoremail'], array('main_content'=>$body), 'default', array(array('text'=>$this->DirectDebitFormPDFContents(), 'name'=>'charity_right_direct_debit.pdf')));
		if (SITE_TEST)
		{	$this->DirectDebitFormPDFToFile();
		}
	} // end of fn SendDirectDebitForm

	public function BuildPDFAttachment()
	{	ob_start();
		echo 'Content-Type: application/pdf; name="charity_right_directdebit.pdf"
Content-Transfer-Encoding: base64
Content-Disposition: inline;  filename="charity_right_directdebit.pdf"

';
		echo chunk_split(base64_encode($this->DirectDebitFormPDFContents()));
		return ob_get_clean();
	} // end of fn BuildPDFAttachment

	public function DirectDebitFormPDFContents()
	{	include_once(CITDOC_ROOT . '/html2pdf/html2pdf.class.php');
		//return 'dfdsfdsf';
		$html2pdf = new HTML2PDF('P', 'A4', 'en');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($this->DirectDebitFormHTMLContents());
		return $html2pdf->Output('', 'S');
	} // end of fn DirectDebitFormPDFContents

	public function DirectDebitFormPDFToFile()
	{	include_once(CITDOC_ROOT . '/html2pdf/html2pdf.class.php');
		$html2pdf = new HTML2PDF('P', 'A4', 'en');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($this->DirectDebitFormHTMLContents());
		return $html2pdf->Output(CITDOC_ROOT . '/mail_log/' . time() . '.pdf', 'F');
	} // end of fn DirectDebitFormPDFContents

	public function DirectDebitFormHTMLContents()
	{	ob_start();
		if ($dd_details = $this->DirectDebitParams())
		{	if (SITE_TEST)
			{	$inc_root = 'https://charityright.org.uk/';
			} else
			{	$inc_root = SITE_URL;
			}
			$inc_root_img = $inc_root . 'img/ddpdf/';
			$inc_root_img = 'https://purematrimony.com/img/cright/';
			echo '
			<style type="text/css">
			*
			{	font-size: 11px;
				-webkit-box-sizing: border-box;
				-moz-box-sizing: border-box;
				box-sizing: border-box;
			}
			.clear
			{	clear: both;
			}
			.number
			{	text-align: right;
			}
			.centre
			{	text-align: center;
			}
			ul
			{	list-style: circle; width: 170mm;
			}
			ul li
			{	padding: 5px 0px 5px 15px;
			}
			table.topTable
			{	border: 2px solid #000;
			}
			table.topTable td.topTableDiv
			{	width: 90mm; border: 1px solid #666; vertical-align: top; padding: 5px;
			}
			table.topTable td.topTableDiv.topTableCustomer
			{	height: 70mm;
			}
			table.topTable td.topTableDiv.topTableDD
			{	height: 40mm;
			}
			td.customerInfoBox
			{	padding: 5px 10px;
				border: 1px solid #666;
				background: #eee;
				color: #000;
			}
			td.customerInfoBox span
			{	color: #666;
			}
			.customerInfoHeader
			{	font-weight: bold;
			}
			td.customerInfoBox.customerInfoBoxCheck
			{	width: 35px;
				height: 20px;
				padding: 2px;
			}
			.addressBox
			{	margin: 10px 30px; background: #EEE; padding: 10px 30px;
			}
			.addressBox p
			{	padding: 5px 0px;
			}
			.bankDetailsTable
			{	width: 100%;
				border: 2px solid #000;
			}
			.bankDetailsTable .bankDetailsTableDiv
			{	width: 50%;
				padding: 10px;
			}
			.bankDetailsTable .bankDetailsTableDiv .bankDetailsTableDivLabel
			{	padding-top: 10px;
			}
			.bankDetailsTable .bankDetailsTableDiv tr:first-child .bankDetailsTableDivLabel
			{	padding-top: 0px;
			}
			</style>
			<page>
			<table class="topTable">
				<tr>
					<td class="topTableDiv topTableCustomer">
						<table style="width: 100%;">
							<tr>
								<td class="customerInfoHeader">Customer Ref:</td><td class="customerInfoBox">', $this->ID(), '</td>
							</tr>
							<tr>
								<td colspan="2" class="customerInfoBox"><span>Customer name: </span>', $this->InputSafeString($this->details['donorfirstname'] . ' ' . $this->details['donorsurname']), '</td>
							</tr>
							<tr>
								<td colspan="2" class="customerInfoBox">
									<table style="border-collapse: collapse;"><tr><td><span>Customer address: </span></td><td style="width: 55mm;">', $this->AddressOutput(), '</td></tr></table>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="customerInfoBox"><span>Email: </span>', $this->InputSafeString($this->details['donoremail']), '</td>
							</tr>
							<tr>
								<td colspan="2" class="customerInfoBox" style="width: 100%;"><span>Phone: </span>', $this->InputSafeString($this->details['donorphone']), '</td>
							</tr>
						</table>
					</td>
					<td class="topTableDiv" rowspan="2" style="padding-top: 20px;"><div style="text-align: center;"><img src=\'', $inc_root_img, 'logo_pink240.png\' /></div><div class="addressBox"><p>', nl2br($this->InputSafeString($this->GetParameter('compaddress'))), '</p><p>tel: ', $this->InputSafeString($this->GetParameter('compphone')), '</p></div><div style="padding: 5px 30px;">Please complete your details, sign the Direct Debit Instruction and return to Charity Right at the address above</div></td>
				</tr>
				<tr><td class="topTableDiv topTableDD"><table style="width: 100%;">
				<tr><td style="width: 55%;">1st direct debit amount</td><td style="width: 45%;" class="customerInfoBox number">', $currency = $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($dd_details['amount'], 2, '.', ''), '</td></tr>',
					'<tr><td>Subsequent direct debit amount</td><td class="customerInfoBox number">', $currency, number_format($dd_details['amount'], 2, '.', ''), '</td></tr>
					<tr><td>Payment date</td><td><table><tr><td>1st</td><td class="customerInfoBox customerInfoBoxCheck customerInfoBoxCheck"><img src=\'', $inc_root_img, ($this->extrafields['StartDay'] == 1) ? 'check_checked' : 'check_blank', '.jpg\' /></td><td style="width:20px;"></td><td>15th</td><td class="customerInfoBox customerInfoBoxCheck"><img src=\'', $inc_root_img, ($this->extrafields['StartDay'] == 15) ? 'check_checked' : 'check_blank', '.jpg\' /></td></tr></table></td></tr>',
					'<tr><td>DD start date</td><td class="customerInfoBox centre">', date('d-M-Y', strtotime($this->extrafields['StartDate'])), '</td></tr>
					<tr><td>Frequency</td><td class="customerInfoBox centre">Monthly</td></tr>
					<tr><td>Total number of payments</td><td class="customerInfoBox centre">Continuous</td></tr>
				</table></td></tr>
			</table>';
		//return ob_get_clean();
		echo '<table style="width: 190mm;">
				<tr><td style="width: 20%;">&nbsp;</td><td style="width: 60%; vertical-align: center; text-align: center; padding: 10px 0px; font-weight: bold;">Instruction to your Bank or Building Society to pay by Direct Debit</td><td style="width: 20%; text-align: right; padding: 10px 10px 10px 0px;"><img src="', $inc_root_img, 'DirectDebitLogo.jpg" height="30" /></td></tr>
				<tr><td colspan="3">
					<table class="bankDetailsTable">
						<tr>
							<td class="bankDetailsTableDiv"><table style="width: 100%;">
								<tr><td class="bankDetailsTableDivLabel" style="width: 100%;">Name(s) of account holder</td></tr>
								<tr><td class="customerInfoBox">', $this->InputSafeString($this->extrafields['AccountHolder']), '</td></tr>
								<tr><td class="bankDetailsTableDivLabel">Bank / Building Society Account Number</td></tr>
								<tr><td><table style="width: 100%; border-collapse: collapse;"><tr>';
		foreach (str_split($this->extrafields['AccountNumber']) as $acnumber)
		{	echo '<td class="customerInfoBox" style="width: 12.5%; text-align: center;">', $acnumber, '</td>';
		}
		echo '</tr></table></td></tr>
								<tr><td class="bankDetailsTableDivLabel">Bank Sort Code</td></tr>
								<tr><td><table style="width: 100%; border-collapse: collapse;"><tr>';
		foreach (str_split($this->extrafields['AccountSortCode']) as $acnumber)
		{	echo '<td class="customerInfoBox" style="width: 16.6%; text-align: center;">', $acnumber, '</td>';
		}
		echo '</tr></table></td></tr>
								<tr><td class="bankDetailsTableDivLabel">Name of your Bank or Building Society</td></tr>
								<tr><td class="customerInfoBox">&nbsp;</td></tr>
								<tr><td class="bankDetailsTableDivLabel">Address of your Bank or Building Society</td></tr>
								<tr><td class="customerInfoBox">&nbsp;</td></tr>
							</table></td>
							<td class="bankDetailsTableDiv"><table style="width: 100%;">
								<tr><td colspan="2" style="width: 100%;">
									<table style="width: 100%; border-collapse: collapse;"><tr><td style="width: 39%;">Service User Number</td>';
		foreach (str_split(EZC_SUN) as $sun_bit)
		{	echo '<td class="customerInfoBox" style="width: 10%; text-align: center;">', $sun_bit, '</td>';
		}
		echo '</tr></table>
								</td></tr>
								<tr><td colspan="2" style="width: 100%;">Instruction to your Bank or Building Society<br />Please pay Eazy Collect Re Charity Right from the account detailed in this instruction subject to the safeguards assured by the Direct Debit Guarantee. I understand that this instruction may remain with Eazy Collect Re Charity Right and, if so, details will be passed electronically to my Bank or Building Society.</td></tr>
								<tr><td colspan="2" class="bankDetailsTableDivLabel">Account Holder (s) Signature (s)</td></tr>
								<tr><td colspan="2" class="customerInfoBox" style="height: 50px;">&nbsp;</td></tr>
								<tr><td style="width: 50%; padding-right: 10px; text-align: right">Date</td><td style="width: 50%;" class="customerInfoBox">&nbsp;</td></tr>
							</table></td>
						</tr>
					</table>
				</td></tr>
				<tr><td colspan="3" style="padding-top: 3px; border-bottom: 1px dotted #333;">&nbsp;</td></tr>
				<tr><td colspan="3" style="padding-top: 3px;"><div style="text-align: center; padding: 5px 0px;">This Guarantee should be detached and retained by the payer</div></td></tr>
				<tr><td style="width: 20%;">&nbsp;</td><td style="width: 60%; vertical-align: bottom; text-align: center; padding: 10px 0px; font-weight: bold;">THE DIRECT DEBIT GUARANTEE</td><td style="width: 20%; text-align: right; padding: 10px 10px 10px 0px;"><img src="', $inc_root_img, 'DirectDebitLogo.jpg" height="30" /></td></tr>
				<tr><td colspan="3" style="padding: 10px 20px 5px 20px;><ul>
					<li>This Guarantee is offered by all Banks and Building Societies that accept instructions to pay Direct Debits.</li>
					<li>If there are any changes to the amount, date or frequency of your Direct Debit, Eazy Collect Re Charity Right will notify you 10 days in advance of your account being debited or as otherwise agreed. If you request Eazy Collect Re Charity Right to collect a payment, confirmation of the amount and date will be given to you at the time of the request.</li>
					<li>If an error is made in the payment of your Direct Debit by either Eazy Collect Re Charity Right or your Bank or Building Society, you are entitled to a full and immediate refund of the amount paid from your Bank or Building Society. - If you receive a refund you are not entitled to, you must pay it back when Eazy Collect Re Charity Right asks you to.</li>
					<li>You can cancel your Direct Debit at any time by simply contacting your Bank or Building Society. Written confirmation may be required. Please also notify us.</li></ul>
				</td></tr>
			</table></page>';
		}
		return ob_get_clean();
	} // end of fn DirectDebitFormHTMLContents

	public function ConfirmDirectDebit($session = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		$extras = array();
		$updated = 0;

		if ($session['ezc_ContractID'])
		{	$extras['ContractID'] = $session['ezc_ContractID'];
		} else
		{	$fail[] = 'Contract ID missing';
		}

		if ($session['ezc_CustomerID'])
		{	$extras['CustomerID'] = $session['ezc_CustomerID'];
		} else
		{	$fail[] = 'Customer ID missing confirming order';
		}

		if ($session['ezc_DirectDebitRef'])
		{	$extras['DirectDebitRef'] = $session['ezc_DirectDebitRef'];
		} else
		{	$fail[] = 'DirectDebitRef missing';
		}

		if ($session['ezc_StartDate'])
		{	$extras['StartDate'] = $this->datefn->SQLDate($session['ezc_StartDate']);
			$extras['StartDay'] = date('j', $session['ezc_StartDate']);
		} else
		{	$fail[] = 'Start date missing';
		}

		$extras['AccountHolder'] = $session['dd_accountname'];
		$extras['AccountNumber'] = $session['dd_accountnumber'];
		$extras['AccountSortCode'] = $session['dd_accountsortcode'];
		
		if (!$fail)
		{	foreach ($this->donations as $donation_row)
			{	if ($donation_row['donationtype'] == 'monthly')
				{	$donation = new AdminDonation($donation_row);
					if ($donation->IsMonthlyUnconfirmed())
					{	$saved = $donation->ConfirmDirectDebit($session, false);
					}
				}
			}
			foreach ($extras as $name=>$value)
			{	$extras_sql = 'INSERT INTO order_extras SET orderid=' . $this->id  . ', extrafield="' . $this->SQLSafe($name) . '", extravalue="' . $this->SQLSafe($value) . '"';
				$this->db->Query($extras_sql);
			}
			$this->Get($this->id);
			$success[] = 'Direct debit created';
			$this->SendDirectDebitEmailConfirmation();
			$this->SendDirectDebitEmailConfirmationToAdmin();
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn ConfirmDirectDebit

	public function SendDirectDebitEmailConfirmation()
	{	ob_start();
		$confirmed = $this->details['orderdate'];
		$zakat = 0;
		$monthly_donations = array();
		$project_text = '';
		
		foreach ($this->donations as $donation_row)
		{	if ($donation_row['donationtype'] == 'monthly')
			{	$donation = new Donation($donation_row);
				if ($donation->IsMonthlyUnconfirmed())
				{	if (count($monthly_donations))
					{	$project_text = '';
					} else
					{	$project_text = $donation->DirectDebitEmailText();
					}
					$monthly_donations[] = $donation_row;
					if ($donation_row['zakat'])
					{	$zakat++;
					}
					if ((int)$donation_row['donationconfirmed'])
					{	$confirmed = $donation_row['donationconfirmed'];
					}
				}
			}
		}
		
		echo '<h3>Confirmation of your monthly donation</h3>
<p>Thank you for setting up a Direct Debit for Charity Right for ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['total_monthly'], 2), ' per month';
		if ($project_text)
		{	echo ' ', $project_text;
		}
		echo '.</p>';
		if ($zakat)
		{	echo '<p>We appreciate that ';
			if ($zakat == count($monthly_donations))
			{	echo 'this is a Zakat donation. We\'d like to reassure you that this donation';
			} else
			{	echo $zakat > 1 ? 'some of your donations are Zakat donations' : 'one of your donation is a Zakat donation', '. We\'d like to reassure you that ', $zakat > 1 ? 'these donations' : 'this donation';
			}
			echo ' will be distributed in line with the Islamic principles of Zakat.</p>';
		}
		echo '<p>We can confirm that your Direct Debit details have now been successfully received and will be securely submitted to our appointed Direct Debit Bureau, Eazy Collect Services, for final setup with your bank or building society. You will receive an invoice accordingly for each billing period confirming the amount of Direct Debit to be taken. Please note "Eazy Collect DD" will appear on your bank statement for this direct debit payment.</p>
<p>Your details are as follows</p><ul>
<li>Date: ', date('j M Y', strtotime($confirmed)), '</li>
<li>Your reference: ', $this->InputSafeString($this->extrafields['DirectDebitRef']), '</li>
<li>Account holder name: ', $this->InputSafeString($this->extrafields['AccountHolder']), '</li>
<li>Account number: ****', $this->InputSafeString(substr($this->extrafields['AccountNumber'], -4)), '</li>
<li>Account sort code: ', $this->InputSafeString($this->extrafields['AccountSortCode']), '</li>
<li>First payment: On or after ', date('j M Y', strtotime($this->extrafields['StartDate'])), '</li>
<p>Your Direct Debit payments have been set up as follows:</p>
<table style="border-collapse: collapse;"><tr><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">First Amount</th><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">First Collection Date</th><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Subsequent Amounts</th><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Frequency</th><th style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Number of Payments</th></tr>
<tr><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['total_monthly'], 2), '</td><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">', date('j M Y', strtotime($this->extrafields['StartDate'])), '</td><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['total_monthly'], 2), '</td><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Monthly</td><td style="padding: 5px 10px; border: 1px solid #666; text-align: center; vertical-align: top;">Continuous</td></tr></table>
<p>If your payment falls due on a weekend or Bank Holiday, it will be collected on the next working day. If any of these details are incorrect or if you have any questions regarding your Direct Debit, please do not hesitate to contact us on ', $this->InputSafeString($this->GetParameter('compphone')), ' or email ', $this->InputSafeString($this->GetParameter('compemail')), '. However, if your details are correct you need do nothing and your Direct Debit will be processed as normal. You have the right to cancel your Direct Debit at any time.</p>';
		$body = ob_get_clean();
		$mail = new HTMLMail();
		$mail->SetSubject('Confirmation of Direct Debit Setup');
		$mail->SendFromTemplate($this->details['donoremail'], array('main_content'=>$body), 'default');

	} // end of fn SendDirectDebitEmailConfirmation

	public function SendDirectDebitEmailConfirmationToAdmin()
	{	if ($email = $this->GetParameter('email_dd'))
		{	ob_start();
			echo '<h3>Confirmation of monthly donation set up</h3>
<p>Donation set up for ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['total_monthly'], 2), ' per month.</p>
<p>Details are as follows</p><ul>
<li>Date: ', date('j M Y', strtotime($this->details['orderdate'])), '</li>
<li>Reference: ', $this->InputSafeString($this->extrafields['DirectDebitRef']), '</li>
<li>Account holder name: ', $this->InputSafeString($this->extrafields['AccountHolder']), '</li>
<li>Account number: ', $this->InputSafeString($this->extrafields['AccountNumber']), '</li>
<li>Account sort code: ', $this->InputSafeString($this->extrafields['AccountSortCode']), '</li>
<li>First payment: On or after ', date('j M Y', strtotime($this->extrafields['StartDate'])), '</li>
<p><a href="', SITE_URL, ADMIN_URL, '/cartorder.php?id=', $this->id, '">View in admin panel</a></p>';
			$body = ob_get_clean();
			$mail = new HTMLMail();
			$mail->SetSubject('Online Direct Debit Setup');
			$mail->Send($email, $body, strip_tags($body));
		//	$mail->Send('tim@websquare.co.uk', $body, strip_tags($body));
		}
	} // end of fn SendDirectDebitEmailConfirmationToAdmin

	public function EZCRefFromContactID($contractid = '')
	{	return 'EZC~' . $contractid;
	} // end of fn EZCRefFromContactID

} // end of defn CartOrder
