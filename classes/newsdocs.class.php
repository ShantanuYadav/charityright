<?php
class NewsDocs extends NewsDocBase
{	var $docs = array();

	function __construct()
	{	parent::__construct();
		$this->Get();
	} //  end of fn __construct
	
	function Get($id = 0)
	{	$this->ReSet();
		
		$sql = "SELECT * FROM documentfiles ORDER BY uploaddate DESC";
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->docs[$row["docid"]] = new NewsDoc($row);
			}
		}
		
	} // end of fn Get
	
	function ReSet()
	{	$this->docs = array();
	} // end of fn ReSet
	
	function AdminListDocs()
	{	echo "<h3><a href='newsdoc.php'>Add new document</a></h3><br class='clear'/>\n";
		if ($this->docs)
		{	echo "<table>\n<tr>\n<th>Document</th>\n<th>Link Location</th>\n<th>Uploaded</th>\n<th>Actions</th>\n</tr>\n";
			foreach ($this->docs as $doc)
			{	echo "<tr class='stripe", $i++ % 2, "'>\n<td>", $this->InputSafeString($doc->details["docname"]), "</td>\n<td>", 
						$doc->DocLink(), "</td>\n<td>", date("d-M-Y @ H:i", strtotime($doc->details["uploaddate"])), 
						"</td>\n<td><a href='newsdoc.php?id=", $doc->id, "'>edit</a>";
				if ($doc->CanDelete())
				{	echo "&nbsp;|&nbsp;<a href='newsdocs.php?deldoc=", $doc->id, "'>delete</a>";
				}
				echo "</td>\n</tr>\n";
			}
			echo "</table>\n";
		}
	} // end of fn AdminListImages
	
} // end of defn NewsDocs
?>