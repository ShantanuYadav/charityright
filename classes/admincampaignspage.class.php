<?php
class AdminCampaignsPage extends AdminPage
{	protected $campaign;
	protected $menuarea;

	function __construct()
	{	parent::__construct('CAMPAIGNS');
	} //  end of fn __construct

	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('campaigns'))
		{	$this->AdminCampaignsLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AdminCampaignsLoggedInConstruct()
	{	$this->AssignCampaign();
		$this->breadcrumbs->AddCrumb('campaigns.php', 'Campaigns');
		$this->CampaignConstructFunctions();
		if ($this->campaign->id)
		{	$this->breadcrumbs->AddCrumb('campaign.php?id=' . $this->campaign->id, $this->campaign->FullTitle());
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function CampaignConstructFunctions()
	{	
	} // end of fn CampaignConstructFunctions
	
	protected function AssignCampaign()
	{	$this->campaign = new AdminCampaign($_GET['id']);
	} // end of fn AssignCampaign
	
	protected function AdminCampaignBody()
	{	if ($menu = $this->GetCampaignMenu())
		{	echo '<div class="adminItemSubMenu"><ul>';
			foreach ($menu as $menuarea=>$menuitem)
			{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '>';
				if ($menuitem['link'])
				{	echo '<a href="', $menuitem['link'], '"', $menuitem['blank'] ? ' target="_blank"' : '', '>';
				} else
				{	echo '<span>';
				}
				echo $this->InputSafeString($menuitem['text']), $menuitem['link'] ? '</a>' : '</span>', '</li>';
			}
			echo '</ul><div class="clear"></div></div>';
		}
	} // end of fn AdminCampaignBody
	
	protected function GetCampaignMenu()
	{	$menu = array();
		if ($this->campaign->id)
		{	$menu['view'] = array('text'=>strip_tags($this->campaign->FullTitle()), 'link'=>'campaign.php?id=' . $this->campaign->id);
			if ($this->campaign->AdminCanUpdate())
			{	$menu['edit'] = array('text'=>'Edit', 'link'=>'campaignedit.php?id=' . $this->campaign->id);
			}
			$menu['page'] = array('text'=>'Page', 'link'=>$this->campaign->Link(), 'blank'=>true);
			if ($this->campaign->details['cuid'])
			{	$menu['owner'] = array('text'=>'Owner', 'link'=>'campaignuser.php?id=' . $this->campaign->details['cuid']);
			}
			if ($this->CanAdminUser('accounts'))
			{	$menu['donations'] = array('text'=>'Donations', 'link'=>'campaigndonations.php?id=' . $this->campaign->id);
			}
			if ($this->campaign->team_members)
			{	$menu['members'] = array('text'=>'Members', 'link'=>'campaignmembers.php?id=' . $this->campaign->id);
				if ($this->CanAdminUser('accounts'))
				{	$menu['movements'] = array('text'=>'Movements report', 'link'=>'campaignmovements.php?id=' . $this->campaign->id);
				}
			} else
			{	if ($this->campaign->team)
				{	$menu['team'] = array('text'=>'Team (' . $this->InputSafeString($this->campaign->team['campname']) . ')', 'link'=>'campaign.php?id=' . $this->campaign->team['cid']);
					$menu['unlink'] = array('text'=>'Unlink from team', 'link'=>'campaignunlink.php?id=' . $this->campaign->id);
				} else
				{	if ($this->campaign->CanJoinATeam())
					{	$menu['team_add'] = array('text'=>'Join to another team', 'link'=>'campaign_addtoteam.php?id=' . $this->campaign->id);
					}
				}
			}
			if ($this->campaign->details['infusionsoftid'])
			{	$if = new Infusionsoft();
				if ($iflink = $if->GetInfusionsoftOrderLink($this->campaign->details['infusionsoftid']))
				{	$menu['infusionsoft'] = array('text'=>'at Infusionsoft', 'link'=>$iflink, 'blank'=>true);
				}
			}
		}
		return $menu;
	} // end of fn GetCampaignMenu
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('campaigns'))
		{	$this->AdminCampaignBody();
		}
	} // end of fn AdminBodyMain
	
	protected function CampaignsList($campaigns = array(), $perpage = 0, $newlink = true, $cuid = 0)
	{	ob_start();
		echo '<table>';
		if ($newlink)
		{	echo '<tr class="newlink"><th colspan="14"><a href="campaignedit.php';
			if ($cuid = (int)$cuid)
			{	echo '?cuid=', $cuid;
			}
			echo '">Create new campaign</a></th></tr>';
		}
		echo '<tr><th></th><th>Campaign</th><th>Link</th><th>Owner</th><th>Created</th><th>Status</th><th class="num">Target</th><th class="num">Raised</th><th class="num">Raised GBP</th><th class="num">Giftaid donations</th><th class="num">Giftaid donations GBP</th><th class="num">Offline donations</th><th class="num">Offline donations GBP</th><th>Actions</th></tr>';
		if ($campaigns)
		{	if ($perpage = (int)$perpage)
			{	if ($_GET['page'] > 1)
				{	$start = ($_GET['page'] - 1) * $perpage;
				} else
				{	$start = 0;
				}
				$end = $start + $perpage;
			} else
			{	$start = 0;
			}
			
			if ($do_totals = $cuid)
			{	$totals = array('raised_gbp'=>0, 'giftaid_gbp'=>0, 'manual_gbp'=>0);
			}
			
			foreach ($campaigns as $campaign_row)
			{	$campaign = new AdminCampaign($campaign_row);
				$raised = $campaign->GetDonationTotalsAll();
				if ($do_totals)
				{	foreach ($totals as $key=>$dummy)
					{	$totals[$key] += $raised[$key];
					}
				}
				if (++$count > $start)
				{	if (!$perpage || ($count <= $end))
					{	echo '<tr class="stripe', $i++ % 2,  '"><td>';
						if ($image_url = $campaign->GetImageSRC('thumb'))
						{	echo '<img src="', $image_url, '" />';
						} else
						{	if ($avatar = $campaign->GetAvatarSRC('thumb'))
							{	echo '<img src="', $avatar, '" />';
							}
						}
						echo '</td><td>', $this->InputSafeString($campaign->details['campname']), '</td><td><a href="', $campaign->Link(), '" target="_blank">', $campaign->Link(), '</a></td><td>';
						if ($campaign->details['cuid'])
						{	if ($campaign->owner)
							{	echo '<a href="campaignuser.php?id=', $campaign->details['cuid'], '">', $this->InputSafeString($campaign->owner['firstname'] . ' ' . $campaign->owner['lastname']), '</a>';
							} else
							{	echo 'owner not found';
							}
						} else
						{	echo 'Charity Right';
						}
						echo '</td><td>', date('d/m/y H:i', strtotime($campaign->details['created'])), '</td><td>';
						$status = array();
						if ($campaign->details['teamoftheday'])
						{	$status[] = '[TEAM OF THE DAY ' . (int)$campaign->details['teamoftheday'] . ']';
						}
						if ($campaign->details['listpriority'])
						{	$status[] = '[LIST PRIORITY ' . (int)$campaign->details['listpriority'] . ']';
						}
						if ($campaign->team && ($team = new Campaign($campaign->team)) && $team->id)
						{	$status[] = 'Raising for <a href="campaign.php?id=' . $team->id . '">' . $team->FullTitle() . '</a>';
						} else
						{	if ($campaign->details['isteam'])
							{	ob_start();
								echo 'Team campaign ';
								if ($campaign->team_members)
								{	echo '<a href="campaignmembers.php?id=', $campaign->id, '">', $tmcount = count($campaign->team_members), ' member', $tmcount == 1 ? '' : 's', '</a>';
								} else
								{	echo 'no members yet';
								}
								$status[] = ob_get_clean();
							} else
							{	$status[] = 'Solo campaign';
							}
						}
						
						$status[] = $campaign->details['visible'] ? 'Visible on site' : 'Hidden';
						if ($campaign->details['visible'])
						{	$status[] = $campaign->details['enabled'] ? 'Taking donations' : 'Not taking donations';
						}
						echo implode('<br />', $status), '</td><td class="num">', $cursymbol = $this->GetCurrency($campaign->details['currency'], 'cursymbol'), number_format($campaign->details['target']), '</td><td class="num">', $cursymbol, number_format($raised['raised'], 2), '</td><td class="num">&pound;', number_format($raised['raised_gbp'], 2), '</td><td class="num">', $cursymbol, number_format($raised['giftaid'], 2), '</td><td class="num">&pound;', number_format($raised['giftaid_gbp'], 2), '</td><td class="num">', $cursymbol, number_format($raised['manual'], 2), '</td><td class="num">&pound;', number_format($raised['manual_gbp'], 2), '</td><td><a href="campaign.php?id=', $campaign->id, '">view</a>';
						if ($campaign->CanDelete())
						{	echo '&nbsp;|&nbsp;<a href="campaignedit.php?id=', $campaign->id, '&delete=1">delete</a>';
						}
						echo '</td></tr>';
					}
				}
			}
			if ($do_totals)
			{	echo '<tr><th colspan="8">Total raised</th><th class="num">&pound;', number_format($totals['raised_gbp'], 2), '</th><th></th><th class="num">&pound;', number_format($totals['giftaid_gbp'], 2), '</th><th></th><th class="num">&pound;', number_format($totals['manual_gbp'], 2), '</th><th></th></tr>';
			}
		}
		echo '</table>';
		if ($perpage && (count($campaigns) > $perpage))
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($campaigns), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
		return ob_get_clean();
	} // end of fn CampaignsList
	
	protected function DonationsList($donations = array(), $perpage = 20)
	{	ob_start();
		if ($donations || $this->campaign->id)
		{	if ($perpage = (int)$perpage)
			{	if ($_GET['page'] > 1)
				{	$start = ($_GET['page'] - 1) * $perpage;
				} else
				{	$start = 0;
				}
				$end = $start + $perpage;
			} else
			{	$start = 0;
			}
			
			$campaigns = array();
			$currencies = $this->CurrencyList('cursymbol');
		
			echo '<table>';
			if ($this->campaign->id)
			{	echo '<tr class="newlink"><th colspan="12"><a href="campaigndonation_add.php?id=', $this->campaign->id, '">Add offline donation</a></th></tr>';
			}
			$total = 0;
			echo '<tr><th>ID</th><th>Date</th><th>Payment ref</th><th>Status</th><th>Donor</th><th class="num">Amount</th><th>Team</th><th>Campaign</th><th>GBP Amount</th><th>Info</th><th>Donor comments</th><th>Actions</th></tr>';
			foreach ($donations as $donation_row)
			{	if (++$count > $start)
				{	$total += $donation_row['gbpamount'];
					if (!$perpage || ($count <= $end))
					{	
						$donation = new CampaignDonation($donation_row);
						$extras = array();
						if ($donation->details['zakat'])
						{	$extras[] = 'Zakat';
						}
						if ($donation->details['giftaid'])
						{	$extras[] = 'Giftaid';
						}
						if ($donation->details['teamid'] && !$campaigns[$donation->details['teamid']])
						{	$campaigns[$donation->details['teamid']] = new AdminCampaign($donation->details['teamid']);
						}
						if ($donation->details['campaignid'] && !$campaigns[$donation->details['campaignid']])
						{	$campaigns[$donation->details['campaignid']] = new AdminCampaign($donation->details['campaignid']);
						}
						$donor = array();
						if ($donation->details['adminuser'])
						{	$adminuser = new AdminUser($donation->details['adminuser'], 1);
							$donor[] = 'Created by admin (' . $adminuser->details['ausername'] . ')';
						}
						if ($donation->details['donoranon'])
						{	if ($donation->details['donorshowto'])
							{	$donor[] = '(Name showing to owner only)';
							} else
							{	$donor[] = '(Name hidden in front end)';
							}
						}
						if ($donation->details['donorfirstname'] || $donation->details['donorsurname'])
						{	$donor[] = $this->InputSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname']));
						}
						if ($donation->details['donoradd1'])
						{	$donor[] = nl2br($this->InputSafeString($donation->details['donoradd1']));
						}
						if ($donation->details['donoradd2'])
						{	$donor[] = nl2br($this->InputSafeString($donation->details['donoradd2']));
						}
						if ($donation->details['donorcity'])
						{	$donor[] = nl2br($this->InputSafeString($donation->details['donorcity']));
						}
						if ($donation->details['donorpostcode'])
						{	$donor[] = nl2br($this->InputSafeString($donation->details['donorpostcode']));
						}
						if ($donation->details['donorcountry'] && ($country = $this->GetCountry($donation->details['donorcountry'])))
						{	$donor[] = $this->InputSafeString($country);
						}
						if ($donation->details['donorphone'])
						{	$donor[] = 'tel. ' . $this->InputSafeString($donation->details['donorphone']);
						}
						if ($donation->details['donoremail'])
						{	$donor[] = 'email: ' . $this->InputSafeString($donation->details['donoremail']);
						}
						echo '<tr class="stripe', $i++ % 2,  '"><td>', $donation->ID(), '</td><td>', date('d/m/y H:i', strtotime($donation->details['donated'])), '</td><td>', $this->InputSafeString($donation->details['donationref']), '</td><td>', $donation->Status(), '</td><td>', implode('<br />', $donor), '</td><td class="num">', $currencies[$donation->details['currency']], number_format($donation->details['amount'], 2), '</td><td class="num">';
						if ($donation->details['teamid'])
						{	echo '<a href="campaign.php?id=', $campaigns[$donation->details['teamid']]->id, '">', $campaigns[$donation->details['teamid']]->FullTitle(), '</a><br />', $currencies[$campaigns[$donation->details['teamid']]->details['currency']], number_format($donation->details['teamamount'], 2);
						}
						echo '</td><td class="num">';
						if ($donation->details['campaignid'] && ($donation->details['campaignid'] != $donation->details['teamid']))
						{	echo '<a href="campaign.php?id=', $campaigns[$donation->details['campaignid']]->id, '">', $campaigns[$donation->details['campaignid']]->FullTitle(), '</a><br />', $currencies[$campaigns[$donation->details['campaignid']]->details['currency']], number_format($donation->details['campaignamount'], 2);
						}
						echo '</td><td class="num">&pound;', number_format($donation->details['gbpamount'], 2), '</td><td>', implode('<br />', $extras), '</td><td>', nl2br($this->InputSafeString($donation->details['donorcomment'])), '</td><td><a href="campaigndonation.php?id=', $donation->id, '">view</a></td></tr>';
					}
				}
			}
			if ($donations)
			{	echo '<tr><th colspan="8">Totals (for whole selection)</th><th class="num">&pound;', number_format($total, 2), '</th><th		colspan="3"></th></tr>';
			}
			echo '</table>';
		}
		if ($perpage && (count($donations) > $perpage))
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($donations), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
		return ob_get_clean();
	} // end of fn DonationsList
	
	protected function DonationsFilterForm()
	{	ob_start();
		class_exists('Form');
		$admindonations = new AdminDonations();
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get">';
		if ($this->campaign->id)
		{	echo '<input type="hidden" name="id" value="', $this->campaign->id, '" />';
		}
		echo '<span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<span>show unpaid</span><input type="checkbox" name="showunpaid" value="1"', $_GET['showunpaid'] ? ' checked="checked"' : '', ' /><input type="submit" class="submit" value="Get" /><div class="clear"></div><span>zakat only</span><input type="checkbox" name="zakat" value="1"', $_GET['zakat'] ? ' checked="checked"' : '', ' /><span>giftaid only</span><input type="checkbox" name="giftaid" value="1"', $_GET['giftaid'] ? ' checked="checked"' : '', ' /><span>offline only</span><input type="checkbox" name="offline" value="1"', $_GET['offline'] ? ' checked="checked"' : '', ' /><span>sort by</span><select name="orderby">';
		foreach ($admindonations->sortOptions as $key=>$value)
		{	echo '<option value="', $key, '"', $key == $_GET['orderby'] ? ' selected="selected"' : '', '>', $value, '</option>';
		}
		echo '</select><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn DonationsFilterForm
	
	protected function CampaignsFilterForm()
	{	ob_start();
		$types = array('solo'=>'Solo campaign', 'team'=>'Team campaign', 'member'=>'Member of a team', 'teamoftheday'=>'Team of the Day', 'listpriority'=>'Priority List');
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get">';
		echo '<span>Created from</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<div class="clear"></div><span>Campaign name</span><input type="text" name="cname" value="', $this->InputSafeString($_GET['cname']), '" /><input type="submit" class="submit" value="Get" /><div class="clear"></div><span>Visible</span><input type="checkbox" name="visible" value="1" ', $_GET['visible'] ? 'checked="checked" ' : '', '/><span>Taking donations</span><input type="checkbox" name="enabled" value="1" ', $_GET['enabled'] ? 'checked="checked" ' : '', '/><span>Help from CR requested</span><input type="checkbox" name="helpfromcr" value="1" ', $_GET['helpfromcr'] ? 'checked="checked" ' : '', '/><span>Type of campaign</span><select name="ctype"><option value="">--- all ---</option>';
		foreach ($types as $type=>$label)
		{	echo '<option value="', $type, '"', $_GET['ctype'] === $type ? ' selected="selected"' : '', '>', $this->InputSafeString($label), '</option>';
		}
		echo '</select><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn CampaignsFilterForm
	
	protected function GetCampaigns()
	{	$tables = array('campaigns'=>'campaigns');
		$fields = array('campaigns.*');
		$where = array();
		$orderby = array();
		
		if ($this->startdate)
		{	$where['startdate'] = 'campaigns.created>="' . $this->startdate . ' 00:00:00"';
		}
		if ($this->enddate)
		{	$where['enddate'] = 'campaigns.created<="' . $this->enddate . ' 23:59:59"';
		}
		if ($_GET['cname'])
		{	$where['cname'] = 'campaigns.campname LIKE "%' . $_GET['cname'] . '%"';
		}
		if ($_GET['visible'])
		{	$where['visible'] = 'campaigns.visible=1';
		}
		if ($_GET['enabled'])
		{	$where['enabled'] = 'campaigns.enabled=1';
		}
		if ($_GET['helpfromcr'])
		{	$where['helpfromcr'] = 'campaigns.helpfromcr=1';
		}
		switch ($_GET['ctype'])
		{	case 'solo':	
				$where['team'] = 'campaigns.isteam=0';
				$tables['campaigns'] = 'campaigns LEFT JOIN campaignmembers ON campaignmembers.memberid=campaigns.cid';
				$where['solo'] = 'campaignmembers.teamid IS NULL';
				break;
			case 'team':	
				$where['team'] = 'campaigns.isteam=1';
				break;
			case 'member':	
				$where['team'] = 'campaigns.isteam=0';
				$tables['campaignmembers'] = 'campaignmembers';
				$where['campaignmembers_link'] = 'campaignmembers.memberid=campaigns.cid';
				break;
			case 'teamoftheday':	
				$where['teamoftheday'] = 'campaigns.teamoftheday>1';
				$orderby['teamoftheday'] = 'campaigns.teamoftheday DESC';
				break;
			case 'listpriority':	
				$where['listpriority'] = 'campaigns.listpriority>1';
				$orderby['listpriority'] = 'campaigns.listpriority DESC';
				break;
		}
		$not_cuid = (int)$_GET['not_cuid'];
		
		$orderby['created'] = 'campaigns.created DESC';
		$campaigns = array();
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	if ($not_cuid)
				{	$linksql = 'SELECT campaignmembers.teamid FROM campaignmembers, campaigns WHERE campaignmembers.memberid=campaigns.cid AND campaignmembers.teamid=' . $row['cid'] . ' AND campaigns.cuid=' . $not_cuid;
					if ($linkresult = $this->db->Query($linksql))
					{	if ($linkrow = $this->db->FetchArray($linkresult))
						{	echo $row['cid'], ' not ok: ', $linksql, '<br />';
							continue;
						} else
						{	//echo $row['cid'], ' is ok: ', $linksql, '<br />';
						}
					}
				}
				$campaigns[$row['cid']] = $row;
			}
		}
		return $campaigns;
	//	return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cid');
	} // end of fn GetCampaigns
	
	protected function GetDonations()
	{	
		$donations = new AdminDonations();
		$filter = ($_GET ? $_GET : array());
		if ($this->startdate)
		{	$filter['startdate'] = $this->startdate;
		}
		if ($this->enddate)
		{	$filter['enddate'] = $this->enddate;
		}
		if ($this->campaign->id)
		{	$filter['campaignid'] = $this->campaign->id;
		}
		return $donations->GetCampaignDonations($filter);
	} // end of fn GetDonations
	
	protected function AddDonationsHeaderToCSV(&$csv)
	{	$csv->AddToHeaders(array('id', 'payment ref', 'status', 'donation date', 'donor', 'donor name', 'donor address', 'donor city/town', 'donor postcode', 'donor country', 'donor email', 'donor phone', 'currency', 'amount', 'amount paid', 'donation area', 'donation project', 'team', 'team currency', 'team amount', 'team amount paid', 'campaign', 'campaign currency', 'campaign amount', 'campaign amount paid', 'GBP amount', 'GBP amount paid', 'giftaid', 'zakat', 'utm codes', 'comments'));
	} // end of fn AddDonationsHeaderToCSV
	
	protected function AddDonationsRowsToCSV(&$csv, $donations = array())
	{	if ($donations)
		{	$campaigns = array();
			$dummy_donation = new Donation();
			$countries = $dummy_donation->GetDonationCountries();

			foreach ($donations as $donation_row)
			{	$donation = new CampaignDonation($donation_row);
				if ($donation->details['teamid'] && !$campaigns[$donation->details['teamid']])
				{	$campaigns[$donation->details['teamid']] = new AdminCampaign($donation->details['teamid']);
				}
				if ($donation->details['campaignid'] && !$campaigns[$donation->details['campaignid']])
				{	$campaigns[$donation->details['campaignid']] = new AdminCampaign($donation->details['campaignid']);
				}
				$address = array();
				if ($donation->details['donoradd1'])
				{	$address[] = $this->CSVSafeString($donation->details['donoradd1']);
				}
				if ($donation->details['donoradd2'])
				{	$address[] = $this->CSVSafeString($donation->details['donoradd2']);
				}
				$utm_lines = array();
				if ($donation->extrafields)
				{	foreach($donation->extrafields as $key=>$value)
					{	if (substr($key, 0, 4) == 'utm_')
						{	$utm_lines[] = $key . '=' . $value;
						}
					}
				}
				
				$donationcountry = $donationproject = '';
				if ($campaigns[$donation->details['teamid']])
				{	$donationcountry = $campaigns[$donation->details['teamid']]->details['donationcountry'];
					$donationproject = $campaigns[$donation->details['teamid']]->details['donationproject'];
				} else
				{	if ($campaigns[$donation->details['campaignid']])
					{	$donationcountry = $campaigns[$donation->details['campaignid']]->details['donationcountry'];
						$donationproject = $campaigns[$donation->details['campaignid']]->details['donationproject'];
					}
				}
				
			/*	ob_start();
				echo 'donor here';
				$donor_col = ob_get_clean();*/
				$donor = array();
				if ($donation->details['adminuser'])
				{	$adminuser = new AdminUser($donation->details['adminuser'], 1);
					$donor[] = 'Created by admin (' . $adminuser->details['ausername'] . ')';
				}
				if ($donation->details['donoranon'])
				{	if ($donation->details['donorshowto'])
					{	$donor[] = '(Name showing to owner only)';
					} else
					{	$donor[] = '(Name hidden in front end)';
					}
				}
				if ($donation->details['donorfirstname'] || $donation->details['donorsurname'])
				{	$donor[] = $this->InputSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname']));
				}
				if ($donation->details['donoradd1'])
				{	$donor[] = nl2br($this->InputSafeString($donation->details['donoradd1']));
				}
				if ($donation->details['donoradd2'])
				{	$donor[] = nl2br($this->InputSafeString($donation->details['donoradd2']));
				}
				if ($donation->details['donorcity'])
				{	$donor[] = nl2br($this->InputSafeString($donation->details['donorcity']));
				}
				if ($donation->details['donorpostcode'])
				{	$donor[] = nl2br($this->InputSafeString($donation->details['donorpostcode']));
				}
				if ($donation->details['donorcountry'] && ($country = $this->GetCountry($donation->details['donorcountry'])))
				{	$donor[] = $this->InputSafeString($country);
				}
				if ($donation->details['donorphone'])
				{	$donor[] = 'tel. ' . $this->InputSafeString($donation->details['donorphone']);
				}
				if ($donation->details['donoremail'])
				{	$donor[] = 'email: ' . $this->InputSafeString($donation->details['donoremail']);
				}

				$row = array('"' . $donation->ID() . '"', 
						'"' . $this->CSVSafeString($donation->details['donationref']) . '"', 
						'"' . $donation->Status() . '"', 
						'"' . date('d/m/Y @H:i', strtotime($donation->details['donated'])) . '"', 
						'"' . implode("\n", $donor) . '"', 
						'"' . $this->CSVSafeString($donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname']) . '"', 
						'"' . implode(', ', $address) . '"', 
						'"' . $this->CSVSafeString($donation->details['donorcity']) . '"', 
						'"' . $this->CSVSafeString($donation->details['donorpostcode']) . '"', 
						'"' . $this->CSVSafeString($this->GetCountry($donation->details['donorcountry'])) . '"', 
						'"' . $this->CSVSafeString($donation->details['donoremail']) . '"', 
						'"' . $this->CSVSafeString($donation->details['donorphone']) . '"', 
						'"' . $donation->details['currency'] . '"', 
						number_format($donation->details['amount'], 2, '.', ''), 
						$donation->details['donationref'] ? number_format($donation->details['amount'], 2, '.', '') : '0.00', 
						'"' . $this->CSVSafeString($countries[$donationcountry]['shortname']). '"', 
						'"' . $this->CSVSafeString($countries[$donationcountry]['projects'][$donationproject]['projectname']). '"', 
						'"' . ($donation->details['teamid'] ? strip_tags($campaigns[$donation->details['teamid']]->FullTitle()) : '') . '"', 
						'"' . ($donation->details['teamid'] ? $campaigns[$donation->details['teamid']]->details['currency'] : '') . '"', 
						($donation->details['teamid'] ? number_format($donation->details['teamamount'], 2, '.', '') : '0.00'), 
						(($donation->details['teamid'] && $donation->details['donationref']) ? number_format($donation->details['teamamount'], 2, '.', '') : '0.00'), 
						'"' . ($donation->details['campaignid'] ? strip_tags($campaigns[$donation->details['campaignid']]->FullTitle()) : '') . '"', 
						'"' . ($donation->details['campaignid'] ? $campaigns[$donation->details['campaignid']]->details['currency'] : '') . '"', 
						$donation->details['campaignid'] ? number_format($donation->details['campaignamount'], 2, '.', '') : '0.00', 
						($donation->details['campaignid'] && $donation->details['donationref']) ? number_format($donation->details['campaignamount'], 2, '.', '') : '0.00', 
						number_format($donation->details['gbpamount'], 2, '.', ''), 
						$donation->details['donationref'] ? number_format($donation->details['gbpamount'], 2, '.', '') : '0.00', 
						$donation->details['giftaid'] ? '1' : '0', 
						$donation->details['zakat'] ? '1' : '0',
						'"' . implode("\n", $utm_lines) . '"', 
						'"' . $this->CSVSafeString($donation->details['donorcomment']). '"');
				$csv->AddToRows($row);
			}
		}
	} // end of fn AddDonationsHeaderToCSV
	
	protected function AddCampaignsHeaderToCSV(&$csv)
	{	$csv->AddToHeaders(array('id', 'campaign name', 'owner', 'created date', 'visible on site', 'taking donations', 'campaign type', 'team members', 'team', 'currency', 'target', 'raised', 'raised GBP', 'giftaid donations', 'giftaid donations GBP', 'offline donations', 'offline donations GBP'));
	} // end of fn AddCampaignsHeaderToCSV
	
	protected function AddCampaignsRowsToCSV(&$csv, $campaigns = array())
	{	if ($campaigns)
		{	foreach ($campaigns as $campaign_row)
			{	$campaign = new AdminCampaign($campaign_row);
				$raised = $campaign->GetDonationTotalsAll();
				$status = array();
				if ($campaign->team && ($team = new Campaign($campaign->team)) && $team->id)
				{	$status[] = 'Raising for ' . strip_tags($team->FullTitle());
				} else
				{	if ($campaign->details['isteam'])
					{	ob_start();
						
						$status[] = ob_get_clean();
					}
				}
				$team = false;
				$row = array($campaign->id, 
						'"' . $this->CSVSafeString($campaign->details['campname']) . '"', 
						'"' . ($campaign->details['cuid'] ? ($campaign->owner ? $this->CSVSafeString($campaign->owner['firstname'] . ' ' . $campaign->owner['lastname']) : 'owner not found') : 'Charity Right') . '"', 
						'"' . date('d/m/Y @H:i', strtotime($campaign->details['created'])) . '"', 
						'"' . ($campaign->details['visible'] ? 'Yes' : 'No') . '"', 
						'"' . ($campaign->details['visible'] && $campaign->details['enabled'] ? 'Yes' : 'No') . '"', 
						'"' . (($campaign->team && ($team = new Campaign($campaign->team)) && $team->id) ? 'member' : ($campaign->details['isteam'] ? 'team' : 'solo')) . '"', 
						count($campaign->team_members),
						'"' . ($team->id ? strip_tags($team->FullTitle()) : '') . '"',
						'"' . $campaign->details['currency'] . '"', 
						number_format($campaign->details['target'], 2, '.', ''), 
						number_format($raised['raised'], 2, '.', ''), 
						number_format($raised['raised_gbp'], 2, '.', ''), 
						number_format($raised['giftaid'], 2, '.', ''), 
						number_format($raised['giftaid_gbp'], 2, '.', ''), 
						number_format($raised['manual'], 2, '.', ''), 
						number_format($raised['manual_gbp'], 2, '.', '')
					);
				$csv->AddToRows($row);
			}
		}

		} // end of fn AddCampaignsRowsToCSV
	
} // end of defn AdminCampaignsPage
?>