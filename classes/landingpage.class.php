<?php 
class LandingPage extends IndexPage
{	
	public function __construct($pageName = 'home')
	{	parent::__construct($pageName);
		$this->css[] = 'page.css';
	} // end of fn __construct
	
	function MainBodyContent()
	{	$form = new PlateBuilderInterface();
		echo '<div class="container container_white container_platebuilder"><div class="container_inner">', $form->HPBuildForm('div'), $this->SCartModalWindow(), '</div></div><div class="container"><div class="container_inner"><h1 class="page_heading">', $this->page->PageTitleDisplay(), '</h1><div class="left_content">', $this->page->HTMLMainContent(), '</div><div class="clear"></div></div></div>';
	} // end of fn MemberBody
	
} // end of class LandingPage
?>