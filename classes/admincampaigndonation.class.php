<?php
class AdminCampaignDonation extends CampaignDonation
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} // fn __construct
	
	public function SaveChanges($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if ($donortitle = $this->SQLSafe($data['donortitle']))
		{	$fields['donortitle'] = 'donortitle="' . $donortitle . '"';
		} else
		{	$donor_missing = true;
			$fields['donortitle'] = 'donortitle=""';
		}
		if ($donorfirstname = $this->SQLSafe($data['donorfirstname']))
		{	$fields['donorfirstname'] = 'donorfirstname="' . $donorfirstname . '"';
		} else
		{	$donor_missing = true;
			$fields['donorfirstname'] = 'donorfirstname=""';
		}
		if ($donorsurname = $this->SQLSafe($data['donorsurname']))
		{	$fields['donorsurname'] = 'donorsurname="' . $donorsurname . '"';
		} else
		{	$donor_missing = true;
			$fields['donorsurname'] = 'donorsurname=""';
		}
		
		if ($donoradd1 = $this->SQLSafe($data['donoradd1']))
		{	$fields['donoradd1'] = 'donoradd1="' . $donoradd1 . '"';
			$address_done++;
		} else
		{	$fields['donoradd1'] = 'donoradd1=""';
		}
		if ($donoradd2 = $this->SQLSafe($data['donoradd2']))
		{	$fields['donoradd2'] = 'donoradd2="' . $donoradd2 . '"';
			$address_done++;
		} else
		{	$fields['donoradd2'] = 'donoradd2=""';
		}
		if (!$address_done)
		{	$donor_missing = true;
		}

		if ($donorpostcode = $this->SQLSafe($data['donorpostcode']))
		{	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
		} else
		{	$donor_missing = true;
			$fields['donorpostcode'] = 'donorpostcode=""';
		}
		
		if ($donorcity = $this->SQLSafe($data['donorcity']))
		{	$fields['donorcity'] = 'donorcity="' . $donorcity . '"';
		} else
		{	$donor_missing = true;
			$fields['donorcity'] = 'donorcity=""';
		}

		$fields['giftaid'] = 'giftaid=' . ($data['giftaid'] ? 1 : 0);
		if ($data['donorcountry'] && $this->GetCountry($data['donorcountry']))
		{	$fields['donorcountry'] = 'donorcountry="' . $data['donorcountry'] . '"';
			if ($data['giftaid'])
			{	if ($data['donorcountry'] != 'GB')
				{	$fail[] = 'Giftaid is only available for the UK';
					$fields['giftaid'] = 'giftaid=0';
				}
			}
		} else
		{	$donor_missing = true;
			$fields['donorcountry'] = 'donorcountry=""';
		}
		
		if ($data['giftaid'] && $donor_missing)
		{	$fail[] = 'All donor details must be entered for a giftaid donation';
			unset($fields['giftaid'], $fields['donortitle'], $fields['donorfirstname'], $fields['donorsurname'], $fields['donoradd1'], $fields['donoradd2'], $fields['donorpostcode'], $fields['donorcity'], $fields['donorcountry']);
		}
		
		$fields['zakat'] = 'zakat=' . ($data['zakat'] ? 1 : 0);
		$fields['donoranon'] = 'donoranon=' . ($data['donoranon'] ? 1 : 0);
		$fields['donorshowto'] = 'donorshowto=' . ($data['donorshowto'] ? 1 : 0);
		
		$fields['donoremail'] = 'donoremail="' . $this->SQLSafe($data['donoremail']) . '"';
		$fields['donorphone'] = 'donorphone="' . $this->SQLSafe($data['donorphone']) . '"';
		$fields['donorcomment'] = 'donorcomment="' . $this->SQLSafe($data['donorcomment']) . '"';
		
//$fail[] = 'test';
		if (!$fail && $set = implode(', ', $fields))
		{	$sql = 'UPDATE campaigndonations SET ' . $set . ' WHERE cdid=' . $this->id;
		//	echo $sql;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->Get($this->id);
					$success[] = 'Changes saved';
					$if = new InfusionSoftCampaignDonation();
					$if->AmendDonation($this);
				}
			} else $fail[] = $sql . ': ' . $this->db->Error();
		}
//	$this->VarDump($fields);
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	
	} // fn SaveChanges
	
	public function CreateManual($data = array(), $campaign)
	{	$fail = array();
		$success = array();
		$fields = array('donated'=>'donated="' . ($now = $this->datefn->SQLDateTime()) . '"', 'donationconfirmed'=>'donationconfirmed="' . $now . '"', 'gateway'=>'gateway="manual"');
		
		if (($adminuser = $this->GetAdminUser()) && $adminuser->userid)
		{	$fields['adminuser'] = 'adminuser=' . $adminuser->userid;
		} else
		{	$fail[] = 'admin user missing';
		}
		
		if ($this->GetCurrency($data['currency']) && ($currency = $data['currency']))
		{	$fields['currency'] = 'currency="' . $currency . '"';
		} else
		{	$fail[] = 'Currency missing';
		}
		
		if (($amount = round($data['amount'], 2)) > 0)
		{	$fields['amount'] = 'amount=' . $amount;
			if ($currency)
			{	if ($currency == 'GBP')
				{	$fields['gbpamount'] = 'gbpamount=' . $amount;
				} else
				{	$fields['gbpamount'] = 'gbpamount=' . round($amount / $this->GetCurrency($currency, 'convertrate'), 2);
				}
			}
		} else
		{	$fail[] = 'Amount missing';
		}
		
		if (is_a($campaign, 'Campaign') && $campaign->id)
		{	$fields['campaignid'] = 'campaignid=' . $campaign->id;
			if ($amount && $currency)
			{	if ($campaign->details['currency'] == $currency)
				{	$fields['campaignamount'] = 'campaignamount=' . $amount;
				} else
				{	$fields['campaignamount'] = 'campaignamount=' . round(($amount * $this->GetCurrency($campaign->details['currency'], 'convertrate')) / $this->GetCurrency($currency, 'convertrate'), 2);
				}
			}
			if ($teamid = (int)$campaign->team['cid'])
			{	$fields['teamid'] = 'teamid=' . $teamid;
				if ($amount && $currency)
				{	if ($campaign->team['currency'] == $currency)
					{	$fields['teamamount'] = 'teamamount=' . $amount;
					} else
					{	$fields['teamamount'] = 'teamamount=' . round(($amount * $this->GetCurrency($campaign->team['currency'], 'convertrate')) / $this->GetCurrency($currency, 'convertrate'), 2);
					}
				}
			}
		} else
		{	$fail[] = 'Campaign missing';
		}
		
		if ($donortitle = $this->SQLSafe($data['donortitle']))
		{	$fields['donortitle'] = 'donortitle="' . $donortitle . '"';
		} else
		{	$donor_missing = true;
		}
		if ($donorfirstname = $this->SQLSafe($data['donorfirstname']))
		{	$fields['donorfirstname'] = 'donorfirstname="' . $donorfirstname . '"';
		} else
		{	$donor_missing = true;
		}
		if ($donorsurname = $this->SQLSafe($data['donorsurname']))
		{	$fields['donorsurname'] = 'donorsurname="' . $donorsurname . '"';
		} else
		{	$donor_missing = true;
		}
		
		if ($donoradd1 = $this->SQLSafe($data['donoradd1']))
		{	$fields['donoradd1'] = 'donoradd1="' . $donoradd1 . '"';
			$address_done++;
		}
		if ($donoradd2 = $this->SQLSafe($data['donoradd2']))
		{	$fields['donoradd2'] = 'donoradd2="' . $donoradd2 . '"';
			$address_done++;
		}
		if (!$address_done)
		{	$donor_missing = true;
		}

		if ($donorpostcode = $this->SQLSafe($data['donorpostcode']))
		{	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
		} else
		{	$donor_missing = true;
		}
		if ($donorcity = $this->SQLSafe($data['donorcity']))
		{	$fields['donorcity'] = 'donorcity="' . $donorcity . '"';
		} else
		{	$donor_missing = true;
		}

		if ($data['donorcountry'] && $this->GetCountry($data['donorcountry']))
		{	$fields['donorcountry'] = 'donorcountry="' . $data['donorcountry'] . '"';
			if ($data['giftaid'])
			{	if ($giftaid = ($data['donorcountry'] == 'GB'))
				{	$fields['giftaid'] = 'giftaid=1';
				} else
				{	$fail[] = 'Giftaid is only available for the UK';
				}
			}
		} else
		{	$donor_missing = true;
		}
		
		if ($data['giftaid'] && $donor_missing)
		{	$fail[] = 'All donor details must be entered for a giftaid donation';
		}
		
		$fields['zakat'] = 'zakat=' . ($data['zakat'] ? 1 : 0);
		$fields['donoranon'] = 'donoranon=' . ($data['donoranon'] ? 1 : 0);
		$fields['donorshowto'] = 'donorshowto=' . ($data['donorshowto'] ? 1 : 0);
		
		$fields['donoremail'] = 'donoremail="' . $this->SQLSafe($data['donoremail']) . '"';
		$fields['donorphone'] = 'donorphone="' . $this->SQLSafe($data['donorphone']) . '"';
		$fields['donorcomment'] = 'donorcomment="' . $this->SQLSafe($data['donorcomment']) . '"';
		
		if (is_array($data['contactpref']) && $data['contactpref'])
		{	$pref = new ContactPreferences();
			$fields['contactpref'] = 'contactpref=' . $pref->PreferencesValueFromArray($data['contactpref']);
		}
		
//$fail[] = 'test';
		if (!$fail && $set = implode(', ', $fields))
		{	$sql = 'INSERT INTO campaigndonations SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
				{	$payref = 'MANUAL~' . str_pad($id, 10, '0', STR_PAD_LEFT);
					$paysql = 'UPDATE campaigndonations SET donationref="' . $payref . '" WHERE cdid=' . $id;
					$this->db->Query($paysql);
					$this->Get($id);
					$this->SendDonationEmail();
					$success[] = 'New donation created';
					$if = new InfusionSoftCampaignDonation();
					$if->AddDonation($this);
					$if->PayDonation($this->id);
				}
			} else $fail[] = $sql . ': ' . $this->db->Error();
		}
//	$this->VarDump($fields);
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	
	} // fn CreateManual
	
	public function CanDelete()
	{	return $this->id && $this->details['adminuser'];
	} // end of fn CanDelete
	
	public function DeleteExtra()
	{	$if = new InfusionSoftCampaignDonation();
		$if->DeleteDonation($this);
	} // end of fn DeleteExtra
	
	public function InputForm($campaign)
	{	ob_start();
		$pref = new ContactPreferences();
		if ($data = $_POST)
		{	$data['contactpref'] = $pref->PreferencesValueFromArray($data['contactpref']);
		} else
		{	$data = array('currency'=>$campaign->details['currency'], 'donorshowto'=>1);
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $campaign->id);
		$form->AddSelectWithGroups('Currency for donation', 'currency', $data['currency'], '', $this->CurrencyList(), 1, 1, '');
		$form->AddTextInput('Donation amount', 'amount', number_format($data['amount'], 2, '.', ''), 'number');
		$form->AddCheckBox('Giftaid (UK only, and donor info must be entered)', 'giftaid', 1, $data['giftaid']);
		$form->AddCheckBox('Zakat', 'zakat', 1, $data['zakat']);
		$form->AddTextInput('Donor title', 'donortitle', $this->InputSafeString($data['donortitle']), '');
		$form->AddTextInput('Donor first name', 'donorfirstname', $this->InputSafeString($data['donorfirstname']), 'long');
		$form->AddTextInput('Donor last name', 'donorsurname', $this->InputSafeString($data['donorsurname']), 'long');
		$form->AddTextInput('Donor address', 'donoradd1', $this->InputSafeString($data['donoradd1']), 'long');
		$form->AddTextInput('&nbsp;', 'donoradd2', $this->InputSafeString($data['donoradd2']), 'long');
		$form->AddTextInput('Donor town / city', 'donorcity', $this->InputSafeString($data['donorcity']), 'long');
		$form->AddTextInput('Donor postcode', 'donorpostcode', $this->InputSafeString($data['donorpostcode']), 'long');
		$form->AddSelectWithGroups('Donor country', 'donorcountry', $data['donorcountry'], '', $this->CountryList(), 1, 1, '');
		$form->AddTextInput('Donor email', 'donoremail', $this->InputSafeString($data['donoremail']), 'long');
		$form->AddTextInput('Donor phone', 'donorphone', $this->InputSafeString($data['donorphone']), 'long');
		$pref->AdminFormElements($form, $data['contactpref']);
		$form->AddCheckBox('Hide name in front end', 'donoranon', 1, $data['donoranon']);
		$form->AddCheckBox('Show details to fundraiser', 'donorshowto', 1, $data['donorshowto']);
		$form->AddTextArea('Donor comments', 'donorcomment', $this->InputSafeString($data['donorcomment']), '', 0, 0, 2, 40);
		$form->AddSubmitButton('', 'Create donation', 'submit');
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
} // end of defn AdminCampaignDonation
?>