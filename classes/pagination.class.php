<?php
class Pagination
{
	protected $stripVars = array('p'); // variables to strip from link
	protected $perPage = 0; // number of listings per page
	protected $total = 0; // number of listings
	protected $numOfPages; // number of pages
	protected $page = 0; // number of current page
	protected $pages = array(); //list of pages
	protected $baseLink = ''; // link to add pagination link to
	protected $pagename = 'p'; // name of get variable carrying page number
	protected $hashlink = ''; // place in page link, to go after page number
	
	function __construct($page = 0, $total = 0, $perPage = 20, $baseLink = '', $stripVars = array(), $pagename = 'p', $hashlink = '')
	{
		$this->total = intval($total);
		if ($pagename)
		{	$this->pagename = $pagename;
		}
		if ($hashlink)
		{	$this->hashlink = $hashlink;
		}
		$this->perPage = intval($perPage);
		if ($this->page = intval($page))
		{	$this->page--;
		}
		
		$this->numOfPages = ceil($this->total/$this->perPage);
		if($page > $this->numOfPages)
		{	$this->page = $this->numOfPages;	
		}

		$this->AddToStripVars($stripVars);
		$this->SetBaseLink($baseLink);
		
		$this->setPages();
		
	//	var_dump($this);
	} // end of constructor
	
	function AddToStripVars($stripVars = array())
	{	if (is_array($stripVars))
		{	foreach ($stripVars as $var)
			{	$this->stripVars[] = $var;
			}
		}
	} // end of fn AddToStripVars
	
	function SetBaseLink($baseLink = '')
	{	
		if (!$baseLink)
		{	$baseLink = $_SERVER['REQUEST_URI'];
		}
		
		// now strip out any existing page
		if ($querypos = strpos($baseLink, '?'))
		{	$this->baseLink = substr($baseLink, 0, $querypos);
			$query = array();
			foreach (explode('&', html_entity_decode(substr($baseLink, $querypos + 1), ENT_NOQUOTES)) as $q)
			{	$qbits = explode('=', $q);
				if (!in_array($qbits[0], $this->stripVars))
				{	$query[] = $q;
				}
			}
			if ($qstring = implode('&', $query))
			{	$this->baseLink .= '?' . $qstring;
			}
		} else
		{	$this->baseLink = $baseLink;
		}
	} // end of fn SetBaseLink
	
	function setPages()
	{	
		$this->pages = array();
		$pstart = 0;
		do
		{	//$this->pages[] = new PaginationPage($pstart, $this->baseLink, $pstart == $this->page, $this->pagename, $this->hashlink);
			$this->pages[] = $this->AssignPaginationPage($pstart, $this->baseLink, $pstart == $this->page, $this->pagename, $this->hashlink);
		} while ((++$pstart * $this->perPage) < $this->total);
		
	} // end of fn setPages
	
	function AssignPaginationPage($pstart = 0, $baseLink = '', $current = false, $pagename = '', $hashlink = '')
	{	return new PaginationPage($pstart, $baseLink, $current, $pagename, $hashlink);
	} // end of fn AssignPaginationPage
	
	function GetLimits()
	{
		$lower = $this->page * $this->perPage;
		$upper = $lower + $this->perPage;
		
		return array($lower, $upper);
	} // end of fn  GetLimits
	
	function Display($separator = '')
	{	$pagination = array();
		ob_start();
		//print_r($this->pages);
		foreach ($this->pages as $page)
		{	$pagination[] = $page->Display();
		}
		echo $this->DisplayPreviousLink(), implode($separator, $pagination), $this->DisplayNextLink();
		return ob_get_clean();
	} // end of fn Display
	
	function DisplayMaxPages($separator = '', $maxpages = 5)
	{	$pagination = array();
		ob_start();
		//print_r($this->pages);
		$do_before = floor($maxpages / 2);
		foreach ($this->pages as $page)
		{	$pcount++;
			if ($pcount == 1)
			{	$pagination[] = $page->Display();
				$pcount_done++;
			} else
			{	if ($pcount < (($this->page + 1) - $do_before))
				{	if (!$beforespacer++)
					{	$pagination[] = $this->UnseenPagesSpacer();
					}
				} else
				{	if (($pcount > (($this->page + 1) + $do_before)) && ($pcount_done >= $maxpages))
					{	if (!$afterspacer++)
						{	$pagination[] = $this->UnseenPagesSpacer();
						}
					} else
					{	$pagination[] = $page->Display();
						$pcount_done++;
					}
				}
			}
		}
		echo $this->DisplayPreviousLink(), implode($separator, $pagination), $this->DisplayNextLink();
		return ob_get_clean();
	} // end of fn Display
	
	function UnseenPagesSpacer()
	{	ob_start();
		echo '<span>&nbsp;....&nbsp;</span>';
		return ob_get_clean();
	} // end of fn DisplayPreviousLink
	
	function DisplayPreviousLink()
	{	ob_start();
		if ($this->page)
		{	echo '<a href="', $this->baseLink, strstr($this->baseLink, '?') ? '&' : '?', $this->pagename, '=', $this->page, $this->hashlink ? ('#' . $this->hashlink) : '', '">&laquo;&nbsp;Prev</a>&nbsp;';
		}
		return ob_get_clean();
	} // end of fn DisplayPreviousLink
	
	function DisplayNextLink()
	{	ob_start();
		if ($this->page < $this->numOfPages - 1)
		{	echo '&nbsp;<a href="', $this->baseLink, strstr($this->baseLink, '?') ? '&' : '?', $this->pagename, '=', $this->page + 2, $this->hashlink ? ('#' . $this->hashlink) : '', '">Next&nbsp;&raquo;</a>';
		}
		return ob_get_clean();
	} // end of fn DisplayNextLink
	
	function PageList()
	{	$pages = array();
		foreach ($this->pages as $page)
		{	$pages[] = $page->Display();
		}
		return $pages;
	} // end of fn PageList
	
	function PageLinks()
	{	$pages = array();
		foreach ($this->pages as $page)
		{	$pages[$page->PageNum()] = $page->Link();
		}
		return $pages;
	} // end of fn PageLinks
	
	function DisplayList($list = array())
	{	$newlist = array();
		if (is_array($list))
		{	$newlist = array_slice($list, $this->page * $this->perPage, $this->perPage);
		}
		return $newlist;
	} // end of fn DisplayList
	
} // end of class Pagination
?>