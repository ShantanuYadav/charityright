<?php
class AdminPostImage extends PostImage
{	

	function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct
	
	function Save($data = array(), $photo_image = array(), $postid = 0)
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($imagedesc = $this->SQLSafe($data['imagedesc']))
		{	$fields[] = 'imagedesc="' . $imagedesc . '"';
		} else
		{	$fail[] = 'description cannot be empty';
		}
		$fields[] = 'listorder=' . (int)$data['listorder'];
		
		if (!$this->id)
		{	if (($postid = (int)$postid) && ($post = new Post($postid)) && $post->id)
			{	$fields[] = 'postid=' . $post->id;
			} else
			{	$fail[] = 'post missing for new image';
			}
			// check for image uploaded and valid
			if (!$this->ValidPhotoUpload($photo_image))
			{	$fail[] = 'valid image not uploaded';
			}
		}
		
	//	$fail[] = 'test';
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE postimages SET ' . $set . ' WHERE piid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO postimages SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new image added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			}
			
			if ($this->id)
			{	if($photo_image['size'])
				{	if ($this->ValidPhotoUpload($photo_image))
					{	$photos_created = 0;
						foreach ($this->imagesizes as $size_name=>$size)
						{	if (!file_exists($this->ImageFileDirectory($size_name)))
							{	mkdir($this->ImageFileDirectory($size_name));
							}
							if ($this->ReSizePhotoPNG($photo_image['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($photo_image['type'], 'png') ? 'png' : 'jpg'))
							{	$photos_created++;
							}
						}
						unset($photo_image['tmp_name']);
						if ($photos_created)
						{	$success[] = 'image uploaded';
						}
					} else
					{	$fail[] = 'image upload failed';
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function DeleteExtra()
	{	foreach ($this->imagesizes as $size_name=>$size)
		{	@unlink($this->GetImageFile($size_name));
		}
	} // end of fn DeleteExtra
	
	function CanDelete()
	{	return $this->id;
	} // end of fn CanDelete
	
	function InputForm($postid = 0)
	{	 
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		if ($this->id)
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id, '');
		} else
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?postid=' . (int)$postid, '');
		}
		$form->AddTextInput('Description (for alt tags, etc.)', 'imagedesc', $this->InputSafeString($data['imagedesc']), 'long');
		$form->AddTextInput('List order', 'listorder', (int)$data['listorder'], 'number');
		$form->AddFileUpload('Image file (jpg or png only, and as square as possible)', 'imagefile');
		if ($this->id && ($src = $this->GetImageSRC('small')))
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $src . '" /><br /></p>');
		}
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Image', 'submit');
		if ($this->id)
		{	echo '<h3>Editing ... "', $this->InputSafeString($data['imagedesc']), '"</h3>';
			if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this image</a></p>';
			}
		}
		$form->Output();
	} // end of fn InputForm
	
} // end of defn AdminPostImage
?>