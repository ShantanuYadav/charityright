<?php
class InfusionSoftCampaignOwner extends InfusionSoft
{	
	
	public function AddContactFromCampaignOwner($cuid = 0)
	{	if (($cowner = $this->CampaignOwnerFromID($cuid)) && $cowner->id)
		{	$cp = new ContactPreferences();
			if ($contactID = $this->AddContact($cowner->details, $cp->IsOptionsSetFromValue($cowner->details['contactpref'], 'email')))
			{	$this->AddOptinTagsToContactFromCampaignOwner($cowner, $contactID);
				return $contactID;
			}
		}
	} // fn AddContactFromCampaignOwner
	
	public function AddCampaign($cid = 0)
	{	if (($campaign = $this->CampaignFromID($cid)) && $campaign->id)
		{	if (($cowner = $this->CampaignOwnerFromID($campaign->details['cuid'])) && $cowner->id)
			{	
				if ($ifid = $this->AddContactFromCampaignOwner($cowner))
				{	$dummy = new Donation();
					$countries = $dummy->GetDonationCountries();
					
					$invoicedata = array();
					$extradata = array();
					
					$invoicedata['contactID'] = $ifid;
					$invoicedata['name'] = 'Website CR Stars campaign';
					$invoicedata['date'] = $campaign->details['created'];
					
					$extradata['_CampaignName'] = $campaign->details['campname'];
					$extradata['_CampaignTargetCurrency'] = $campaign->details['currency'];
					$extradata['_CamaignTarget'] = floatval($campaign->details['target']);
					$extradata['_CampaignHelpRequested'] = (bool)$campaign->details['helpfromcr'];
					$extradata['_AdminLink'] = SITE_URL . ADMIN_URL . '/campaign.php?id=' . (int)$campaign->id;
					
					if ($campaign->team['cid'])
					{	if (($team = new Campaign($campaign->team['cid'])) && $team->id)
						{	$extradata['_CampaignCause'] = $countries[$team->details['donationcountry']]['shortname'];
							$extradata['_CampaignArea'] = $countries[$team->details['donationcountry']]['projects'][$team->details['donationproject']]['projectname'];
						}
					} else
					{	$extradata['_CampaignCause'] = $countries[$campaign->details['donationcountry']]['shortname'];
						$extradata['_CampaignArea'] = $countries[$campaign->details['donationcountry']]['projects'][$campaign->details['donationproject']]['projectname'];
					}
					
					if ($invid = (int)$this->AddBlankInvoice($invoicedata))
					{	$updatesql = 'UPDATE campaigns SET infusionsoftid=' . $invid . ' WHERE cid=' . $campaign->id;
						$this->db->Query($updatesql);
						if ($extradata)
						{	$this->AddExtraToInvoice($extradata, $invid);
						}
						$this->AddTagToContact($ifid, 'campowner');
						return $invid;
					}
				}
			}
		}
	} // fn AddCampaign
	
	public function DeleteCampaign($cid = 0)
	{	if (($campaign = $this->CampaignFromID($cid)) && $campaign->id && $campaign->details['infusionsoftid'])
		{	$this->DeleteInvoice($campaign->details['infusionsoftid']);
		}
	} // fn DeleteCampaign
	
	public function UpdateCampaign($cid = 0)
	{	if (($campaign = $this->CampaignFromID($cid)) && $campaign->id && $campaign->details['infusionsoftid'])
		{	
	
			$dummy = new Donation();
			$countries = $dummy->GetDonationCountries();
			
			$extradata = array();
			$extradata['_CampaignName'] = $campaign->details['campname'];
			$extradata['_CampaignTargetCurrency'] = $campaign->details['currency'];
			$extradata['_CamaignTarget'] = floatval($campaign->details['target']);
			$extradata['_CampaignHelpRequested'] = (bool)$campaign->details['helpfromcr'];
			$extradata['_AdminLink'] = SITE_URL . ADMIN_URL . '/campaign.php?id=' . (int)$campaign->id;
			
			if ($campaign->team['cid'])
			{	if (($team = new Campaign($campaign->team['cid'])) && $team->id)
				{	$extradata['_CampaignCause'] = $countries[$team->details['donationcountry']]['shortname'];
					$extradata['_CampaignArea'] = $countries[$team->details['donationcountry']]['projects'][$team->details['donationproject']]['projectname'];
				}
			} else
			{	$extradata['_CampaignCause'] = $countries[$campaign->details['donationcountry']]['shortname'];
				$extradata['_CampaignArea'] = $countries[$campaign->details['donationcountry']]['projects'][$team->details['donationproject']]['projectname'];
			}
			
			$this->AddExtraToInvoice($extradata, $campaign->details['infusionsoftid']);
		}
	} // fn UpdateCampaign
	
	public function UploadDataFromPost($post = array())
	{	if ($post['email'])
		{	$data = array(	'FirstName' => 	$post['firstname'],
							'LastName' => 	$post['lastname'],
							'Website' => SITE_URL . ADMIN_URL . '/crm_email.php?email=' . urlencode($post['email']),
							'Email' => 		$post['email']
						);
			if ($post['phone'])
			{	$data['Phone1'] = $post['phone'];
			}
			return $data;
		}
	} // end of fn UploadDataFromPost
	
	protected function CampaignOwnerFromID($cuid = 0)
	{	if (is_a($cuid, 'CampaignUser'))
		{	return $cuid;
		} else
		{	return new CampaignUser($cuid);
		}
	} // fn CampaignOwnerFromID
	
	protected function CampaignFromID($cid = 0)
	{	if (is_a($cid, 'Campaign'))
		{	return $cid;
		} else
		{	return new Campaign($cid);
		}
	} // fn CampaignFromID
	
	public function AddOptinTagsToContactFromCampaignOwner($owner = 0, $contactid = 0)
	{	
		if (($owner = $this->CampaignOwnerFromID($owner)) && $owner->id)
		{	if (!$contactid = (int)$contactid)
			{	$contactid = $this->AddContactFromCampaignOwner($owner, false);
			}
			if ($contactid)
			{	$cp = new ContactPreferences(array('phone', 'text'));
				$this->AddOptinTagsToContact($contactid, $cp, $owner->details['contactpref']);
			}
		}
	} // fn AddOptinTagsToContactFromCampaignOwner

} // end of defn InfusionSoftCampaignOwner
?>