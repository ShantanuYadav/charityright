<?php
class PayPalStandard extends Base
{	var $ourpaypalid = '';
	var $ourpaypalid_live = '';
	var $ourpaypalid_test = 'PHRWHN7P63BMN';
	var $ourpaypalemail = '';
	var $ourpaypalemail_live = '';
	var $ourpaypalemail_test = 'matsel_1268820520_biz@websquare.co.uk';
	var $returnurl = '';
	var $cancelurl = '';
	var $paypal_url = '';
	var $paypal_url_live = 'https://www.paypal.com/cgi-bin/webscr';
	var $paypal_url_test = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	var $notifyurl = '';
	var $notifyurl_live = '';
	var $notifyurl_test = '';
	var $verify_url = '';
	var $verify_url_live = 'https://www.paypal.com/cgi-bin/webscr';
	var $verify_url_test = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	var $live = false;
	var $paypal_lang = 'GB';

	function __construct($live = true)
	{	parent::__construct();
		$this->returnurl = SITE_URL . 'myorder.php';
		$this->cancelurl = SITE_URL . 'cart.php';
		$this->notifyurl_live = SITE_URL . 'pp_ear.php';
		$this->notifyurl_test = SITE_URL . 'pp_ear.php';
		$this->SetLive($live);
		$this->GetPayPalLanguage();
	} // fn __construct

	function GetPayPalLanguage()
	{	
	} // end of fn GetPayPalLanguage
	
	function SetLive($live = false)
	{	if ($this->live = $live)
		{	$this->paypal_url = $this->paypal_url_live;
			$this->notifyurl = $this->notifyurl_live;
			$this->ourpaypalid = $this->ourpaypalid_live;
			$this->ourpaypalemail = $this->ourpaypalemail_live;
			$this->verify_url = $this->verify_url_live;
		} else
		{	$this->paypal_url = $this->paypal_url_test;
			$this->notifyurl = $this->notifyurl_test;
			$this->ourpaypalid = $this->ourpaypalid_test;
			$this->ourpaypalemail = $this->ourpaypalemail_test;
			$this->verify_url = $this->verify_url_test;
		}
	} // end of fn SetLive
	
	function OneOffDonationButton(Donation $donation)
	{	
		$donatepage = new PageContent('donate');
		echo "<form action='". $this->paypal_url ."' method='post' name='paypal_form' id='paypal_button'>\n",
				"<input type='hidden' name='cmd' value='_cart' />\n",
				"<input type='hidden' name='upload' value='1' />\n",
				"<input type='hidden' name='business' value='", $this->ourpaypalid, "' />\n",
				"<input type='hidden' name='no_shipping' value='1' />\n";
		echo "<input type='hidden' name='quantity_1' value='1' />\n",
				"<input type='hidden' name='item_name_1' value='Donation to Charity Right for ", $donation->GatewayTitle(), "' />\n",
				"<input type='hidden' name='amount_1' value='", number_format($donation->details['amount'] + $donation->details['adminamount'], 2, ".", ""), "' />\n";
		echo "<input type='hidden' name='no_note' value='0' />\n",
				"<input type='hidden' name='currency_code' value='", $donation->details['currency'], "' />\n",
				"<input type='hidden' name='lc' value='", $this->paypal_lang, "' />\n",
				"<input type='hidden' name='custom' value='oneoff_", $donation->id, "' />\n",
				"<input type='hidden' name='notify_url' value='", $this->notifyurl, "' />\n",
				"<input type='hidden' name='return' value='", $donation->ThankYouLink(), "' />\n",
				"<input type='hidden' name='cancel_return' value='", $donatepage->FullLink(), "' />\n",
				"<input type='hidden' name='rm' value='2' />\n",
				"<noscript><input type='image' style='border:none;' src='https://www.paypal.com/en_US/i/btn/btn_buynow_LG.gif' ",
								"alt='PayPal - The safer, easier way to pay online' /></noscript> ",
				"<img alt='' border='0' width='1' height='1' src='https://www.paypal.com/en_US/i/scr/pixel.gif' />",
				"</form>\n";
	} // end of fn OneOffDonationButton
	
	function PPDateToSQL($date)
	{	return $this->datefn->SQLDateTime(strtotime($date));
	} // end of fn PPDateToSQL
	
} // end if class defn PayPalStandard
?>