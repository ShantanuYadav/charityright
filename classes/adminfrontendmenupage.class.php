<?php

class AdminFrontEndMenuPage extends CMSPage
{	protected $femenu = '';
	private $page_option = '';

	function __construct()
	{	parent::__construct('CMS');
	} //  end of fn __construct

	function CMSLoggedInConstruct($page_option = '')
	{	parent::CMSLoggedInConstruct();
		$this->page_option = $page_option;
		
		$this->AssignFEMenu();
		$this->ConstructFunctions();
		
	//	$this->css[] = 'admincoursepage.css';
		$this->css[] = 'adminfemenus.css';
	//	$this->js[] = 'admin_pageedit.js';
		
		$this->breadcrumbs->AddCrumb('femenus.php', 'Front end menus');
		$this->breadcrumbs->AddCrumb('femenuedit.php?id=' . $this->femenu->id, $this->femenu->id ? $this->femenu->admintitle : 'New Front End Menu');
		
		$this->css[] = 'adminpages.css';
	} // end of fn CMSLoggedInConstruct
	
	protected function ConstructFunctions()
	{	
	} // end of fn ConstructFunctions
	
	protected function AssignFEMenu()
	{	$this->femenu = new AdminFrontEndMenu($_GET['id']);
	} // end of fn AssignFEMenu
	
	function CMSBodyMain()
	{	echo $this->PageBodyMenu(), $this->FEMenuBodyContent();
	} // end of fn CMSBodyMain
	
	protected function FEMenuBodyContent()
	{	
	} // end of fn FEMenuBodyContent
	
	private function PageBodyMenu()
	{	ob_start();
		if ($this->femenu->id)
		{	echo '<div class="adminItemSubMenu"><ul>';
			foreach ($this->BodyMenuOptions() as $key=>$option)
			{	echo '<li', $this->page_option == $key ? ' class="selected"' : '', '><a href="', $option['link'], '">', $option['text'], '</a></li>';
			}
			echo '</ul><div class="clear"></div></div><div class="clear"></div>';
		}
		return ob_get_clean();
	} // end of fn PageBodyMenu
	
	protected function BodyMenuOptions()
	{	$options = array();
		if ($this->femenu->id)
		{	$options['edit'] = array('link'=>'femenuedit.php?id=' . $this->femenu->id, 'text'=>'Edit');
			$options['items'] = array('link'=>'femenuitems.php?id=' . $this->femenu->id, 'text'=>'Menu Items (' . count($this->femenu->GetAllMenuItems()) . ')');
		}
		return $options;
	} // end of fn BodyMenuOptions
	
} // end of defn AdminFrontEndMenuPage
?>