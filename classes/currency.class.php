<?php
class Currency extends Base
{	var $code = 0;
	var $details = array();
	var $history = array();	

	function __construct($code = '')
	{	parent::__construct();
		$this->Get($code);
	} //  end of fn __construct
	
	function Get($code = '')
	{	$this->ReSet();
		
		if (is_array($code))
		{	$this->code = $code['curcode'];
			$this->details = $code;
			$this->GetHistory();
		} else
		{	if ($result = $this->db->Query('SELECT * FROM currencies WHERE curcode="' . $code . '"'))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->code = $row['curcode'];
					$this->details = $row;
					$this->GetHistory();
				}
			}
		}
		
	} // end of fn Get
	
	function GetHistory()
	{	$this->history = array();
		if ($result = $this->db->Query('SELECT * FROM currency_rates WHERE curcode="' . $this->code . '" ORDER BY `when` DESC LIMIT 30'))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->history[] = $row;
			}
		}
	} // end of fn GetHistory
	
	function ReSet()
	{	
		$this->code = 0;
		$this->details = array();
		$this->history = array();
	} // end of fn ReSet
	
	function Save($data = array())
	{	
		$fail = array();
		$success = array();
		
		$fields = array();
		
		if (!$this->code)
		{	if ($curcode = strtoupper($this->SQLSafe($data['curcode'])))
			{	$fields[] = 'curcode="' . $curcode . '"';
			} else
			{	$fail[] = 'currency code cannot be empty';
			}
		}
		
		if ($curname = $this->SQLSafe($data['curname']))
		{	$fields[] = 'curname="' . $curname . '"';
		} else
		{	$fail[] = 'currency name cannot be empty';
		}
		
		if ($cursymbol = $this->SQLSafe($data['cursymbol']))
		{	$fields[] = 'cursymbol="' . $cursymbol . '"';
		} else
		{	$fail[] = 'symbol cannot be empty';
		}
		
		$fields[] = 'curorder=' . (int)$data['curorder'];
		$fields[] = 'defdonation=' . (int)$data['defdonation'];
		$fields[] = 'use_oneoff=' . ($data['use_oneoff'] ? '1' : '0');
		$fields[] = 'use_monthly=' . ($data['use_monthly'] ? '1' : '0');
		$fields[] = 'use_campaigns=' . ($data['use_campaigns'] ? '1' : '0');
		
		if ($convertrate = (float)$data['convertrate'])
		{	$fields[] = 'convertrate=' . (float)$data['convertrate'];
			if ($convertrate != $this->details['convertrate'])
			{	$rate_changed = true;
			}
		} else
		{	$fail[] = 'zero conversion rate not allowed';
		}
		
		if ($this->code || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->code)
			{	$sql = 'UPDATE currencies SET ' . $set . ' WHERE curcode="' . $this->code . '"';
			} else
			{	$sql = 'INSERT INTO currencies SET ' . $set;
			}

			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->code)
					{	$success[] = 'changes saved';
						$this->Get($this->code);
					} else
					{	$this->Get($curcode);
						$success[] = 'new currency added';
					}
					if ($rate_changed)
					{	// now record manual change
						$this->RecordNewRate($this->details['convertrate'], true);
					}
				}
			}
			
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	function GoogleUpdateRate()
	{	if ($this->CanUpdate())
		{	if ($rate = GoogleCurrencyConverter::convert(1,'GBP',$this->code));
			{	$sql = 'UPDATE currencies SET convertrate=' . $rate . ' WHERE curcode="' . $this->code . '"';
				if ($result = $this->db->Query($sql))
				{	$this->RecordNewRate($rate);
					$this->details['convertrate'] = $rate;
				}
			}
		}
	} // end of fn GoogleUpdateRate
	
	function RecordNewRate($rate, $manual = false)
	{	$now = $this->datefn->SQLDateTime();
		$recordsql = 'INSERT INTO currency_rates SET curcode="' . $this->code . '", convertrate=' . $rate . ', `when`="' . $now . '"';
		if ($manual)
		{	$recordsql .= ', manual_change=1';
		}
		$this->db->Query($recordsql);
		$this->GetHistory();
	} // end of fn RecordNewRate
	
	function HistoryTable()
	{	if ($this->history || $this->CanUpdate())
		{	echo '<table id="history"><tr><th class="num">Rate</th><th>Applied from';
			if ($this->CanUpdate())
			{	echo '<br /><a href="', $_SERVER['SCRIPT_NAME'], '?code=', $this->code, '&update=1">update now</a>';
			}
			echo '</th></tr>';
			foreach ($this->history as $rate)
			{	echo '<tr><td class="num">', number_format($rate['convertrate'], 4),'</td><td>', date('d/m/y @H:i', strtotime($rate['when'])), $rate['manual_change'] ? ' (manual)' : '', '</td></tr>';
			}
			echo '</table>';
		}
	} // end of fn HistoryTable
	
	function CountriesList()
	{	if ($countries = $this->GetCountries())
		{	echo '<p>Used in ...<ul>';
			foreach ($countries as $ctry)
			{	echo '<li><a href="ctryedit.php?ctry=', $ctry->code, '">', $ctry->details['shortname'], '</a></li>';
			}
			echo '</ul></p>';
		} else
		{	echo '<p>This currency is not used for any countries yet - <a href="countries.php">add from list<a></p>';
		}
	} // end of fn CountriesList
	
	function GetCountries()
	{	$countries = array();
		if ($result = $this->db->Query('SELECT * FROM countries WHERE currency="' . $this->details['curcode'] . '"'))
		{	while ($row = $this->db->FetchArray($result))
			{	$countries[] = new AdminCountry($row);
			}
		}
		
		return $countries;
	} // end of fn GetCountries
	
	function CanUpdate()
	{	return $this->details['rateupdateable'];
	} // end of fn CanUpdate
	
	function InputForm()
	{	 
		$form = new Form($_SERVER['SCRIPT_NAME'] . '?code=' . $this->code);
		if ($this->code)
		{	$form->AddLabelLine('<h4>' . $this->code . ' (' . $this->details['cursymbol'] . ')</h4>');
			$data = $this->details;
		} else
		{	$form->AddTextInput('3 letter code', 'curcode', $this->InputSafeString(strtoupper($_POST['curcode'])));
			$data = $_POST;
		}
		$form->AddTextInput('Symbol', 'cursymbol', htmlentities($data['cursymbol']));
		$form->AddTextInput('Name', 'curname', $this->InputSafeString($data['curname']));
		$form->AddTextInput('Order listed', 'curorder', (int)$data['curorder'], 'number');
		$form->AddTextInput('Conversion Rate', 'convertrate', round($data['convertrate'], 4));
		$form->AddCheckBox('Use for one-off donations', 'use_oneoff', 1, $data['use_oneoff']);
		$form->AddCheckBox('Use for monthly donations', 'use_monthly', 1, $data['use_monthly']);
		$form->AddCheckBox('Use for campaign targets', 'use_campaigns', 1, $data['use_campaigns']);
		$form->AddTextInput('Default donation', 'defdonation', (int)$data['defdonation'], 'number');
		
		$form->AddSubmitButton('', $this->code ? 'Save Changes' : 'Create Currency', 'submit');
		
		$form->Output();
		echo '<br />';
	} // end of fn InputForm
	
} // end of defn Currency
?>