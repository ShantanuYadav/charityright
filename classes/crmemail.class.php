<?php
class CRMEmail extends Base
{	private $email = '';
	private $email_ok = false;
	
	public function __construct($email = '')
	{	parent::__construct();
		$this->email_ok = $this->ValidEMail($this->email = $email);
	} //  end of fn __construct

	public function Email()
	{	return $this->email;
	} // end of fn Email
	
	public function EmailOK()
	{	return $this->email_ok;
	} //  end of fn EmailOK

	public function GetActivity()
	{	$rows = array();
		if ($this->EmailOK())
		{	$sql = 'SELECT * FROM campaignusers WHERE email="' . $this->SQLSafe($this->Email()) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$rows[] = array('type'=>'campaignowner', 'typetext'=>'Campaign owner', 'id'=>$row['cuid'], 'time'=>$row['lastupdated'], 'data'=>$row);
				}
			}
			
			$donations = array();
			$sql = 'SELECT * FROM donations WHERE donoremail="' . $this->SQLSafe($this->Email()) . '" ORDER BY created DESC';
			if ($result = $this->db->Query($sql))
			{	while ($row = $this->db->FetchArray($result))
				{	$donations[] = array('type'=>'donation', 'typetext'=>'Donation', 'id'=>$row['did'], 'time'=>$row['created'], 'data'=>$row);
				}
			}
			
			$sql = 'SELECT * FROM campaigndonations WHERE donoremail="' . $this->SQLSafe($this->Email()) . '" ORDER BY donated DESC';
			if ($result = $this->db->Query($sql))
			{	while ($row = $this->db->FetchArray($result))
				{	$donations[] = array('type'=>'crdonation', 'typetext'=>'CR Stars donation', 'id'=>$row['cdid'], 'time'=>$row['donated'], 'data'=>$row);
				}
			}
			
			if ($donations)
			{	uasort($donations, array($this, 'UASortDonationsByDate'));
			}
			
			$rows = array_merge($rows, $donations);
		}
		return $rows;
	} // end of fn GetActivity

	public function UASortDonationsByDate($a, $b)
	{	return $a['time'] < $b['time'];
	} // end of fn UASortDonationsByDate

	public function GetInfusionsoftID()
	{	if ($this->EmailOK())
		{	$sql = 'SELECT * FROM infusionsoftcontacts WHERE email="' . $this->SQLSafe($thid->Email()) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	return $row['ifid'];
				}
			}
		}
		return 0;
	} // end of fn GetInfusionsoftID
	
} // end of defn CRMEmail
?>