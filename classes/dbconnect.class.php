<?php
Class DbConnect
{	private $server; // name of database to connect to
	private $mysqli; // database object
	private $database;
	private $phpver = 0;
	static $st_parameters = array();
	static $st_mysqli = false;

	public function __construct($directory = '') // constructor
	{	
		// if (!$directory)
		// {	$directory = SITE_OFFROOT;
		// }
		$parameters = $this->GetParameters($directory);
		
		if (isset(self::$st_parameters[$directory]) && self::$st_parameters[$directory])
		{	$parameters = self::$st_parameters[$directory];
		} else
		{	$parameters = $this->GetParameters($directory);
			self::$st_parameters[$directory] = $parameters;
		}
		
		$this->Connect($parameters['host'], $parameters['user'], $parameters['password'], $this->database = $parameters['database']);
	} // end of fn __construct

	function GetParameters() // assign database connection parameters
	{	$parameters = array("host" => DB_HOST, 
							"user" => DB_USER,
							"password" => DB_PASS,
							"database" => DB_NAME);
		return $parameters;
	} // end of fn GetParameters

	public function Query($sql = '')
	{	if ($this->mysqli)
		{	$qstart = microtime(true);
			$result = $this->mysqli->query($sql);
			if (($qtime = (microtime(true) - $qstart)) >= 0.5)
			{	$this->SlowQueryLog($sql, $qtime);
			}
			if (SITE_TEST && ($error = $this->Error()))
			{	$this->LogError($sql, $error);
			}
			return $result;
		} else
		{	return false;
		}
	} // end of fn Query

	private function LogError($sql = '', $error = '')
	{	/*if ($f = fopen(CITDOC_ROOT . '/slowq/sql_error.txt', 'a'))
		{	fputs($f, date('Y-m-d H:i:s') . '|' . $sql . '|' . $error . '|' . $_SERVER['REQUEST_URI'] . "\r\n");
			fclose($f);
		}*/
	} // end of fn LogError

	private function SlowQueryLog($sql = '', $qtime = 0)
	{	if ($f = @fopen(CITDOC_ROOT . '/slowq/' . date('Y_m_d_H') . '.txt', 'a'))
		{	fputs($f, date('Y-m-d H:i:s') . '|' . $sql . '|' . number_format($qtime, 3) . "\n");
			fclose($f);
		}
	} // end of fn SlowQueryLog

	public function Connect($server = '', $user = '', $pass = '', $database = '')
	{	if (self::$st_mysqli)
		{	$this->mysqli = self::$st_mysqli;
		} else
		{	$this->mysqli = @mysqli_connect($server, $user, $pass, $database);
			self::$st_mysqli = $this->mysqli;
		}
		
	} // end of fn Connect

	public function Close()
	{	if ($this->mysqli) $this->mysqli->close();
	} // end of fn Close

	public function DataTypes($result)
	{	if (is_a($result, 'mysqli_result'))
		{	$fields = array();
			for ($fcount = 0; $fcount < $result->field_count; $fcount++)
			{	$fields[$fcount] = $result->fetch_field_direct($fcount);
			}
			return $fields;
		}
		return false;
	} // end of fn DataTypes

	public function ConvertDataTypesInRow($row = array(), $types = array())
	{	if (is_array($row))
		{	if (is_a($types, 'mysqli_result'))
			{	$types = $this->DataTypes($types);
			}
			$output = $row;
			if (is_array($types))
			{	$fcount = 0;
				foreach ($row as $key=>$value)
				{	switch ($types[$fcount]->type)
					{	case 1: // 'tinyint':
						case 2: // 'smallint':
						case 3: // 'int':
						case 8: // 'bigint':
						case 9: // 'mediumint':
						case 7: // 'timestamp':
							$output[$key] = (int)$value;
							break;
						case 4: // 'float':
							$output[$key] = (float)$value;
							break;
						case 5: // 'double':
							$output[$key] = (double)$value;
							break;
						default:
							$output[$key] = $value;
					}
					$fcount++;
				}
			}
			return $output;
		}
		return array();
	} // end of fn ConvertDataTypesInRow

	public function FetchArray($result)
	{	if (is_a($result, 'mysqli_result'))
		{	return $result->fetch_assoc();
		}
		return false;
	} // end of fn FetchArray

	public function NumRows($result)
	{	if (is_a($result, 'mysqli_result'))
		{	return $result->num_rows;
		}
		return 0;
	} // end of fn NumRows

	public function AffectedRows()
	{	if (is_a($this->mysqli, 'mysqli'))
		{	return $this->mysqli->affected_rows;
		}
		return 0;
	} // end of fn AffectedRows
	
	public function InsertID()
	{	if (is_a($this->mysqli, 'mysqli'))
		{	return $this->mysqli->insert_id;
		}
		return 0;
	} // end of fn InsertID

	public function Error()
	{	if (is_a($this->mysqli, 'mysqli'))
		{	return $this->mysqli->error;
		}
		return '';
	} // end of fn Error

	public function RealEscapeString($string = '')
	{	if (is_a($this->mysqli, 'mysqli'))
		{	return $this->mysqli->real_escape_string($string);
		}
		return $string;
	} // end of fn RealEscapeString

	public function FreeResult(&$result)
	{	if (is_a($result, 'mysqli_result'))
		{	$result->free();
			unset($result);
			return true;
		}
	} // end of fn FreeResult
	
	public function Version()
	{	if (is_a($this->mysqli, 'mysqli'))
		{	if ($ver = $this->mysqli->server_info)
			{	$verstr = substr($ver, 0, strpos($ver, '-'));
				return $verstr;
			}
		}
		return '';
	} // end of fn Version

	public function BuildSQL($tables = array(), $fields = array(), $where = array(), $orderby = array(), $groupby = array(), $having = array())
	{	if (is_array($tables) && ($table_list = implode(', ', $tables)) && is_array($fields) && ($field_list = implode(', ', $fields)))
		{	$sql = 'SELECT ' . $field_list . ' FROM ' . $table_list;
			if (is_array($where) && $where)
			{	$sql .= ' WHERE ' . implode(' AND ', $where);
			}
			if (is_array($groupby) && $groupby)
			{	$sql .= ' GROUP BY ' . implode(', ', $groupby);
			}
			if (is_array($having) && $having)
			{	$sql .= ' HAVING ' . implode(' AND ', $having);
			}
			if (is_array($orderby) && $orderby)
			{	$sql .= ' ORDER BY ' . implode(', ', $orderby);
			}
			return $sql;
		}
	} // end of fn BuildSQL
	
	public function ResultsArrayFromSQL($sql = '', $idfield = '', $debug = false)
	{	$results = array();
		if ($result = $this->Query($sql))
		{	while ($row = $this->FetchArray($result))
			{	$results[$idfield ? $row[$idfield] : count($results)] = $row;
			}
		} else
		{	if ($debug)
			{	echo '<p>', $sql, ': ', $this->Error(), '</p>';
			}
		}
		return $results;
	} // end of fn ResultsArrayFromSQL
	
	public function KeyExists($table = '', $keyname = '', $keyname_field = 'Key_name')
	{	$sql = 'SHOW KEYS FROM ' . $table . ' WHERE ' . $keyname_field . '="' . $keyname . '"';
		if ($result = $this->Query($sql))
		{	return $this->NumRows($result);
		}
	} // end of fn KeyExists
	
} // end of class defn DbConnect
?>