<?php
class AdminGatewayPage extends AdminPage
{	
	function __construct()
	{	parent::__construct('ADMIN');
	} //  end of fn __construct

	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('technical'))
		{	$this->AdminGatewayLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	function AdminGatewayLoggedInConstruct()
	{	$this->breadcrumbs->AddCrumb('gateways.php', 'Payment gateways');
	} // end of fn AdminGatewayLoggedInConstruct
	
	function AdminGatewayBody()
	{	
	} // end of fn AdminGatewayBody
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('technical'))
		{	$this->AdminGatewayBody();
		}
	} // end of fn AdminBodyMain
	
} // end of defn AdminGatewayPage
?>