<?php
class AdminPageSection extends PageSection
{	private $class_options = array(
					'1'=>array('psoclass'=>'index_fourth', 'psodesc'=>'L/R Blocks', 'can_lr'=>1, 'can_bgcolour'=>1, 'can_textcolour'=>0, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>0), 
					'6'=>array('psoclass'=>'index_fifth', 'psodesc'=>'index_fifth (background image)', 'can_lr'=>0, 'can_bgcolour'=>0, 'can_textcolour'=>0, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>0),
					'7'=>array('psoclass'=>'page-section-orange page-section-custom-3', 'psodesc'=>'orange background, full width', 'can_lr'=>0, 'can_bgcolour'=>0, 'can_textcolour'=>0, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>0),
					'8'=>array('psoclass'=>'page-section-custom-1', 'psodesc'=>'custom-1 (why food sub-sections)', 'can_lr'=>0, 'can_bgcolour'=>0, 'can_textcolour'=>0, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>0),
					'9'=>array('psoclass'=>'page-section-white page-section-custom-2', 'psodesc'=>'custom-2 (why food twin sections)', 'can_lr'=>0, 'can_bgcolour'=>0, 'can_textcolour'=>0, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>0),
					'10'=>array('psoclass'=>'page-section-horizontal-container', 'psodesc'=>'Horizontal section container', 'can_lr'=>0, 'can_bgcolour'=>1, 'can_textcolour'=>0, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>1, 'subsection'=>0),
					'11'=>array('psoclass'=>'page-section-horizontal-container-inner', 'psodesc'=>'Horizontal section inner', 'can_lr'=>0, 'can_bgcolour'=>1, 'can_textcolour'=>1, 'can_buttonbg'=>1, 'can_buttontext'=>1, 'sectionsallowed'=>0, 'subsection'=>1),
					'12'=>array('psoclass'=>'page-section-horizontal-container-water', 'psodesc'=>'Horizontal section inner water', 'can_lr'=>0, 'can_bgcolour'=>1, 'can_textcolour'=>1, 'can_buttonbg'=>1, 'can_buttontext'=>1, 'sectionsallowed'=>0, 'subsection'=>1),
					'13'=>array('psoclass'=>'page-section-inf_form', 'psodesc'=>'Infusionsoft contact form', 'can_lr'=>0, 'can_bgcolour'=>1, 'can_textcolour'=>1, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>0),
					'14'=>array('psoclass'=>'page-section-narrow_plain', 'psodesc'=>'Narrow plain section', 'can_lr'=>0, 'can_bgcolour'=>1, 'can_textcolour'=>1, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>0),
					'15'=>array('psoclass'=>'page-section-horizontal_half_text', 'psodesc'=>'Half section text', 'can_lr'=>0, 'can_bgcolour'=>1, 'can_textcolour'=>1, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>1),
					'16'=>array('psoclass'=>'page-section-horizontal_half_embed', 'psodesc'=>'Half section embed', 'can_lr'=>0, 'can_bgcolour'=>1, 'can_textcolour'=>0, 'can_buttonbg'=>0, 'can_buttontext'=>0, 'sectionsallowed'=>0, 'subsection'=>1)
				);
	private $textpos_options = array('L'=>'Left', 'R'=>'Right');

	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct
	
	public function SubSections($liveonly = false)
	{	return parent::SubSections($liveonly);
	} // end of fn SubSections
	
	public function CanHaveSubSections()
	{	$class = $this->ClassFromClassName($this->details['psclass']);
		return $class['sectionsallowed'];
	} // end of fn SubSections
	
	public function Save($data = array(), $pageid = 0, $psid = 0, $imagefile = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if (!$this->id)
		{	if ($pageid && ($page = new PageContent($pageid)) && $page->id)
			{	$fields[] = 'pageid=' . $page->id;
				$fields[] = 'parentsection=' . (int)$psid;
			} else
			{	$fail[] = 'page missing';
			}
		}
		
		$fields[] = 'pstext="' . $this->SQLSafe($data['pstext']) . '"';
		$fields[] = 'listorder=' . (int)$data['listorder'];
		$fields[] = 'live=' . ($data['live'] ? '1' : '0');
		$fields[] = 'bgcolour="' . $this->SQLSafe($data['bgcolour']) . '"';
		$fields[] = 'textcolour="' . $this->SQLSafe($data['textcolour']) . '"';
		$fields[] = 'buttonbg="' . $this->SQLSafe($data['buttonbg']) . '"';
		$fields[] = 'buttontext="' . $this->SQLSafe($data['buttontext']) . '"';
		
		if (!$data['pstextpos'] || $this->textpos_options[$data['pstextpos']])
		{	$fields[] = 'pstextpos="' . $this->SQLSafe($data['pstextpos']) . '"';
		} else
		{	$fail[] = 'invalid text position';
		}
		
		$classoptions = $this->ClassOptionDropdown();
		if (!$data['psclass'] || $classoptions[$data['psclass']])
		{	$fields[] = 'psclass="' . $this->SQLSafe($data['psclass']) . '"';
		} else
		{	$fail[] = 'invalid class';
		}
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE pagesections SET ' . $set . ' WHERE psid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO pagesections SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new section added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}
		
		if ($this->id)
		{	if($imagefile['size'])
			{	if ($this->ValidPhotoUpload($imagefile))
				{	$isize = getimagesize($imagefile['tmp_name']);
					if ($this->ReSizePhotoPNG($imagefile['tmp_name'], $this->GetImageFile(), $isize[0], $isize[1], stristr($imagefile['type'], 'png') ? 'png' : 'jpg'))
					{	$success[] = 'image uploaded';
					}
					unset($imagefile['tmp_name']);
				} else
				{	$fail[] = 'image upload failed';
				}
			} else
			{	if ($data['deleteimage'])
				{	if (@unlink($this->GetImageFile()))
					{	$success[] = 'image deleted';
					}
				}
			}
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function CanDelete()
	{	return $this->id && !$this->SubSections();
	} // end of fn CanDelete
	
	public function DeleteExtra()
	{	@unlink($this->GetImageFile());
	} // end of fn DeleteExtra
	
	public function ClassOptionText()
	{	$class = $this->ClassFromClassName($this->details['psclass']);
		return $class['psodesc'];
	} // end of fn ClassOptionText
	
	public function ClassOptionDropdown($filter = array())
	{	$dropdown = array();
		if (is_array($this->class_options))
		{	foreach ($this->class_options as $id=>$option)
			{	if ($filter['sectionsallowed'] && !$option['sectionsallowed'])
				{	continue;
				}
				if (isset($filter['subsections']))
				{	if ($filter['subsections'] && !$option['subsection'])
					{	continue;
					}
					if (!$filter['subsections'] && $option['subsection'])
					{	continue;
					}
				}
				$dropdown[$option['psoclass']] = $option['psodesc'];
			}
		}
		return $dropdown;
	} // end of fn ClassOptionDropdown
	
	public function ClassFromClassName($classname = '')
	{	if (is_array($this->class_options))
		{	foreach ($this->class_options as $id=>$option)
			{	if ($option['psoclass'] == $classname)
				{	return $option;
				}
			}
		}
	} // end of fn ClassFromClassName
	
	public function TextPosOptionText()
	{	return $this->textpos_options[$this->details['pstextpos']];
	} // end of fn TextPosOptionText

	public function InputForm($pageid = 0, $psid = 0)
	{	ob_start();
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}
		if (!$data['bgcolour'])
		{	$data['bgcolour'] = 'FFFFFF';
		}
		if (!$data['textcolour'])
		{	$data['textcolour'] = '000000';
		}
		if (!$data['buttonbg'])
		{	$data['buttonbg'] = 'FFFFFF';
		}
		if (!$data['buttontext'])
		{	$data['buttontext'] = '000000';
		}
		
		$class = $this->ClassFromClassName($data['psclass']);

		if ($this->id)
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		} else
		{	if ($psid = (int)$psid)
			{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?psid=' . $psid);
			} else
			{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?pageid=' . (int)$pageid);
			}
		}
		$form->AddTextArea('Section text', 'pstext', stripslashes($data['pstext']), 'tinymce', 0, 0, 10, 60);
		$form->AddTextInput('List order', 'listorder', (int)$data['listorder'], 'number');
		$form->AddCheckBox('Live', 'live', 1, $data['live']);
		$form->AddSelect('Class of section', 'psclass', $data['psclass'], '', $this->ClassOptionDropdown(array('sectionsallowed'=>($this->id && count($this->SubSections())), 'subsections'=>(bool)$psid)), 1, 0, 'onchange="psFormClassChange();"');
		
		$form->AddRawText('<div class="psFormContainer_lr' . ($class['can_lr'] ? '' : ' psFormContainerHidden') . '">');
		$form->AddSelect('Text position (if appropriate)', 'pstextpos', $data['pstextpos'], '', $this->textpos_options, 1, 0);
		$form->AddRawText('</div>');
		
		$form->AddRawText('<div class="psFormContainer_bgcolour' . ($class['can_bgcolour'] ? '' : ' psFormContainerHidden') . '">');
		$form->AddTextInput('Background colour', 'bgcolour', $data['bgcolour'], 'colourpicker');
		$form->AddRawText('</div>');
		
		$form->AddRawText('<div class="psFormContainer_textcolour' . ($class['can_textcolour'] ? '' : ' psFormContainerHidden') . '">');
		$form->AddTextInput('Text colour', 'textcolour', $data['textcolour'], 'colourpicker');
		$form->AddRawText('</div>');
		
		$form->AddRawText('<div class="psFormContainer_buttonbg' . ($class['can_buttonbg'] ? '' : ' psFormContainerHidden') . '">');
		$form->AddTextInput('Button background colour', 'buttonbg', $data['buttonbg'], 'colourpicker');
		$form->AddRawText('</div>');
		
		$form->AddRawText('<div class="psFormContainer_buttontext' . ($class['can_buttontext'] ? '' : ' psFormContainerHidden') . '">');
		$form->AddTextInput('Button text colour', 'buttontext', $data['buttontext'], 'colourpicker');
		$form->AddRawText('</div>');
		
		$form->AddFileUpload('Upload section image (if any)', 'imagefile');
		if ($this->HasImage())
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $this->GetImageSRC() . '" width="300" /><br /></p>');
			$form->AddCheckBox('Delete this image', 'deleteimage', 1, false);
		}
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Section', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this section</a></p>';
			}
		}
		echo "<script>$(function() {
	$('.colourpicker').jPicker({window:{position:{x:'30',y:'center'}}, images:{clientPath: '../img/jpicker/'}});
});</script>
";
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
	public function SectionsTable()
	{	ob_start();
		echo '<table><tr class="newlink"><th colspan="5"><a href="pagesubsection.php?psid=', $this->id, '">Add new subsection</a></th></tr><tr><th></th><th>Text</th><th>Class</th><th class="centre">List order</th><th>Action</th></tr>';
		foreach ($this->SubSections() as $section_row)
		{	$section = new AdminPageSection($section_row);
			$options = array();
			$content = array();
			$class = $section->ClassFromClassName($section->details['psclass']);
			if ($sectioncount = count($section->SubSections()))
			{	$content[] = '<a href="pagesubsections.php?id=' . $section->id . '">[' . $sectioncount . ' Subsections]</a>';
			}
			if ($pstext = strip_tags($section->details['pstext']))
			{	$content[] = $this->InputSafeString(substr($pstext,0, 150));
			}
			if ($class['psodesc'])
			{	$options[] = $this->InputSafeString($class['psodesc']);
			}
			if ($textpos = $section->TextPosOptionText())
			{	$options[] = $this->InputSafeString($textpos);
			}
			echo '<tr><td>';
			if ($section->HasImage())
			{	echo '<img src="', $section->GetImageSRC(), '" width="150" />';
			}
			echo '</td><td style="';
			if ($class['can_bgcolour'])
			{	echo 'background-color: #', $section->details['bgcolour'], ' !important;';
			}
			if ($class['can_textcolour'])
			{	echo 'color: #', $section->details['textcolour'], ' !important;';
			}
			echo '">', implode('<br />', $content), '</td><td>', implode(', ', $options), '</td><td class="centre">', $section->details['live'] ? '' : 'NOT LIVE<br />', (int)$section->details['listorder'], '</td><td><a href="pagesubsection.php?id=', $section->id, '">edit</a>&nbsp;|&nbsp;<a href="pagesubsection.php?id=', $section->id, '&delete=1">delete</a></td></tr>';
		}
		echo '</table>';
		return ob_get_clean();
	} // end of fn SectionsTable
	
} // end of defn AdminPageSection
?>