<?php
class HowHeardStats extends Base
{	public $startdate = '2018-07-13';
	
	public function __construct() // constructor
	{	parent::__construct();
	} // end of fn __construct
	
	public function GetHowHeardOptions()
	{	$howheard = array();
		$sql = 'SELECT * FROM howfoundus ORDER BY hforder, hfid';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$howheard[$row['hfvalue']] = $row;
			}
		}
		return $howheard;
	} // end of fn GetHowHeardOptions
	
	public function GetHowHeardOption($option = '')
	{	$sql = 'SELECT * FROM howfoundus WHERE hfvalue="' . $this->SQLSafe($option) . '"';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	return $row;
			}
		}
	} // end of fn GetHowHeardOption
	
	function OrdersPerOption($filter = array())
	{	$howheard = array('totals'=>array('count'=>0), 'options'=>array());
		
		$tables = array('cartorders'=>'cartorders');
		$fields = array('cartorders.*');
		$where = array();
		
		if ($filter['startdate'])
		{	$where['startdate'] = 'orderdate>="' . $this->SQLSafe($filter['startdate']) . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'orderdate<="' . $this->SQLSafe($filter['enddate']) . ' 23:59:59"';
		}
		if ($filter['country'])
		{	$where['country'] = 'donorcountry="' . $this->SQLSafe($filter['country']) . '"';
		}
		
		$sql = $this->db->BuildSQL($tables, $fields, $where);
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	if ($filter['paidonly'])
				{	$cartorder = new CartOrder($row);
					if (!$cartorder->IsPaid())
					{	continue;
					}
				}
				
				if (!$row['donorhowheard'])
				{	$row['donorhowheard'] = '--none--';
				}
				if (!$howheard['options'][$row['donorhowheard']])
				{	$hhdesc = $row['donorhowheard'];
					if (!in_array($hhdesc, array('other', '--none--')))
					{	if (($howheard_option = $this->GetHowHeardOption($hhdesc)) && $howheard_option['hfdesc'])
						{	$hhdesc = $howheard_option['hfdesc'];
						}
					}
					$howheard['options'][$row['donorhowheard']] = array('key'=>$row['donorhowheard'], 'desc'=>$hhdesc, 'count'=>0);
				}
				$howheard['options'][$row['donorhowheard']]['count']++;
				$howheard['totals']['count']++;
			}
		}
		
		if ($howheard['options'])
		{	uasort($howheard['options'], array($this, 'UASortOptions'));
			foreach ($howheard['options'] as $key=>$option)
			{	$howheard['options'][$key]['proportion'] = $option['count'] / $howheard['totals']['count'];
			}
		}
		
		return $howheard;
	} // end of fn OrdersPerOption
	
	private function UASortOptions($a, $b)
	{	if ($a['count'] == $b['count'])
		{	return strtolower($a['desc']) >	 strtolower($b['desc']);
		} else
		{	return $a['count'] < $b['count'];
		}
	} // end of fn UASortOptions
	
} // end of defn HowHeardStats
?>