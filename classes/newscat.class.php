<?php
class NewsCat extends Base
{	var $id = 0;
	var $details = array();
	var $language = '';

	function __construct($id = 0)
	{	parent::__construct();
		$this->AssignNewsLanguage();
		$this->Get($id);
	} //  end of fn __construct
	
	function AssignNewsLanguage()
	{	$this->language = $this->lang;
	} // end of fn AssignNewsLanguage
	
	function Get($id = 0)
	{	$this->ReSet();
		
		if (is_array($id))
		{	$this->id = (int)$id['catid'];
			$this->details = $id;
			// get text from language file
			$this->AddDetailsForLang($this->language);
		} else
		{	if ($result = $this->db->Query("SELECT * FROM newscats WHERE catid=" . (int)$id))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
		
	} // end of fn Get
	
	function AddDetailsForLang($lang = "")
	{	$sql = "SELECT * FROM newscats_lang WHERE catid={$this->id} AND lang='$lang'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	foreach ($row as $field=>$value)
				{	$this->details[$field] = $value;
				}
			} else
			{	if ($lang == $this->def_lang)
				{	// as last resort go for english
					if ($lang != "en") // only if default language not english
					{	$sql = "SELECT * FROM newscats_lang WHERE catid=$this->id AND lang='en'";
						if ($result = $this->db->Query($sql))
						{	if ($row = $this->db->FetchArray($result))
							{	foreach ($row as $field=>$value)
								{	$this->details[$field] = $value;
								}
							}
						}
					}
				} else
				{	$this->AddDetailsForDefaultLang();
				}
			}
		}
	
	} // end of fn AddDetailsForLang
	
	function AddDetailsForDefaultLang()
	{	$this->AddDetailsForLang($this->def_lang);
	} // end of fn AddDetailsForDefaultLang
	
	function ReSet()
	{	
		$this->id = 0;
		$this->details = array();
	} // end of fn ReSet
	
	public function GetStories()
	{	$stories = array();
		
		return $stories;
	} // end of fn GetStories
	
} // end of defn NewsCat
?>