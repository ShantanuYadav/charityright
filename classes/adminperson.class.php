<?php
class AdminPerson extends Person
{	

	function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct
	
	function Save($data = array(), $photo_image = array(), $postid = 0)
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($pname = $this->SQLSafe($data['pname']))
		{	$fields['pname'] = 'pname="' . $pname . '"';
		} else
		{	$fail[] = 'name cannot be empty';
		}
		$fields['ptitle'] = 'ptitle="' . $this->SQLSafe($data['ptitle']) . '"';
		$fields['ptype'] = 'ptype="' . $this->SQLSafe($data['ptype']) . '"';
		$fields['listorder'] = 'listorder=' . (int)$data['listorder'];
		
		if (!$this->id)
		{	if (!$this->ValidPhotoUpload($photo_image))
			{	$fail[] = 'valid image not uploaded';
			}
		}
		$fields['live'] = 'live=' . ($data['live'] ? '1' : '0');
		
	//	$fail[] = 'test';
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE people SET ' . $set . ' WHERE pid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO people SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new image added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			}
			
			if ($this->id)
			{	if($photo_image['size'])
				{	if ($this->ValidPhotoUpload($photo_image))
					{	$photos_created = 0;
						foreach ($this->imagesizes as $size_name=>$size)
						{	if (!file_exists($this->ImageFileDirectory($size_name)))
							{	mkdir($this->ImageFileDirectory($size_name));
							}
							if ($this->ReSizePhotoPNG($photo_image['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($photo_image['type'], 'png') ? 'png' : 'jpg'))
							{	$photos_created++;
							}
						}
						unset($photo_image['tmp_name']);
						if ($photos_created)
						{	$success[] = 'image uploaded';
						}
					} else
					{	$fail[] = 'image upload failed';
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function ValidPhotoUpload($photo_image = array())
	{	return is_array($photo_image) && (stristr($photo_image['type'], 'jpeg') || stristr($photo_image['type'], 'jpg') || stristr($photo_image['type'], 'png')) && !$photo_image['error'] && $photo_image['size'];
	} // end of fn ValidPhotoUpload
	
	function ReSizePhotoPNG($uploadfile = '', $file = '', $maxwidth = 0, $maxheight = 0, $imagetype = '')
	{	$isize = getimagesize($uploadfile);
		$ratio = $maxwidth / $isize[0];
		$h_ratio = $maxheight / $isize[1];
		if ($h_ratio > $ratio)
		{	$ratio = $h_ratio;
		}
		switch ($imagetype)
		{	case 'png': $oldimage = imagecreatefrompng($uploadfile);
							break;
			case 'jpg':
			case 'jpeg': $oldimage = imagecreatefromjpeg($uploadfile);
							break;
		}
		
		if ($oldimage)
		{	$w_new = ceil($isize[0] * $ratio);
			$h_new = ceil($isize[1] * $ratio);
			
			if ($maxwidth && $maxheight && $ratio != 1)
			{	$newimage = imagecreatetruecolor($w_new,$h_new);
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
				imagecopyresampled($newimage,$oldimage,0,0,0,0,$w_new, $h_new, $isize[0], $isize[1]);
			} else
			{	$newimage = $oldimage;
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
			}
			
			// now get middle chunk - horizontally
			if ($maxwidth && $maxheight && ($w_new > $maxwidth || $h_new > $maxheight))
			{	$resizeimg = imagecreatetruecolor($maxwidth, $maxheight);
				if ($imagetype == 'png')
				{	imagealphablending($resizeimg, false);
					imagesavealpha($resizeimg, true);
				}
				$leftoffset = floor(($w_new - $maxwidth) / 2);
				imagecopyresampled($resizeimg, $newimage, 0, 0, floor(($w_new - $maxwidth) / 2), floor(($h_new - $maxheight) / 2), $maxwidth, $maxheight, $maxwidth, $maxheight);
				$newimage = $resizeimg;
			}
			
			ob_start();
			imagepng($newimage, NULL, 3);
			return file_put_contents($file, ob_get_clean());
		}
	} // end of fn ReSizePhotoPNG
	
	public function DeleteExtra()
	{	foreach ($this->imagesizes as $size_name=>$size)
		{	@unlink($this->GetImageFile($size_name));
		}
	} // end of fn DeleteExtra
	
	function CanDelete()
	{	return $this->id;
	} // end of fn CanDelete
	
	function InputForm($postid = 0)
	{	 
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id, '');
		$form->AddTextInput('Name', 'pname', $this->InputSafeString($data['pname']), 'long');
		$form->AddTextInput('Title', 'ptitle', $this->InputSafeString($data['ptitle']), 'long');
		$form->AddSelect('Type', 'ptype', $this->details['ptype'], '', $this->GetPersonTypes(), 1, 0);
		$form->AddTextInput('List order', 'listorder', (int)$data['listorder'], 'number');
		$form->AddCheckBox('Live (visible in front end)', 'live', 1, $data['live']);
		$form->AddFileUpload('Image file (jpg or png only, and as square as possible)', 'imagefile');
		if ($this->id && ($src = $this->GetImageSRC('default')))
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $src . '" /><br /></p>');
		}
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Person', 'submit');
		if ($this->id)
		{	echo '<h3>Editing ... "', $this->InputSafeString($data['pname']), '"</h3>';
			if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this person</a></p>';
			}
		}
		$form->Output();
	} // end of fn InputForm
	
} // end of defn AdminPerson
?>