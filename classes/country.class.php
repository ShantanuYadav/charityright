<?php
class Country extends Base
{	var $details = array();
	var $code = '';
	var $language = '';
	
	function __construct($ctrycode = '')
	{	parent::__construct();
		$this->AssignPageLanguage();
		$this->Get($ctrycode);
	} // fn __construct
	
	function AssignPageLanguage()
	{	$this->language = $this->lang;
	} // end of fn AssignPageLanguage
	
	function Reset()
	{	$this->details = array();
		$this->code = '';
	} // end of fn Reset
	
	function Get($ctrycode = '')
	{	$this->Reset();
		if (is_array($ctrycode))
		{	$this->details = $ctrycode;
			$this->code = $ctrycode['ccode'];
			
			// get text from language file
			$this->AddDetailsForLang($this->language);
		} else
		{	if ($result = $this->db->Query('SELECT * FROM countries WHERE ccode="' . $this->SQLSafe($ctrycode) . '"'))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
		
	} // end of fn Get
	
	function AddDetailsForLang($lang = "")
	{	$sql = "SELECT * FROM countries_lang WHERE ccode={$this->code} AND lang='$lang'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	foreach ($row as $field=>$value)
				{	$this->details[$field] = $value;
				}
			} else
			{	if ($lang != $this->def_lang)
				{	$this->AddDetailsForDefaultLang();
				}
			}
		}
	
	} // end of fn AddDetailsForLang
	
	function AddDetailsForDefaultLang()
	{	$this->AddDetailsForLang($this->def_lang);
	} // end of fn AddDetailsForDefaultLang
	
	function GetCurrency($curcode = '', $field = '')
	{	if ($this->details["currency"])
		{	return new Currency($this->details["currency"]);
		} else
		{	return new Currency($this->def_currency);
		}
	} // end of fn GetCurrency
	
} // end of defn AdminCountry
?>
