<?php
class AdminPagesPage extends CMSPage
{	protected $editpage;
	protected $pagesection;
	protected $pagesubsection;
	protected $menuarea;

	function CMSLoggedInConstruct()
	{	parent::CMSLoggedInConstruct();
		$this->AdminPagesLoggedInConstruct();
	} // end of fn CMSLoggedInConstruct
	
	protected function AdminPagesLoggedInConstruct()
	{	$this->AssignPage();
		$this->css[] = 'adminpages.css';
		$this->breadcrumbs->AddCrumb('pagelist.php', 'Pages');
		$this->AdminPageConstructFunctions();
		if ($this->editpage->id)
		{	$this->breadcrumbs->AddCrumb('pageedit.php?id=' . $this->editpage->id, $this->editpage->admintitle);
		}
	} // end of fn AdminPagesLoggedInConstruct
	
	protected function AdminPageConstructFunctions()
	{	
	} // end of fn AdminPageConstructFunctions
	
	protected function AssignPage()
	{	$this->editpage = new AdminPageContent($_GET['id']);
	} // end of fn AssignPage
	
	protected function AdminPagesBody()
	{	if ($this->editpage->id)
		{	if ($menu = $this->GetPageMenu())
			{	echo '<div class="adminItemSubMenu"><ul>';
				foreach ($menu as $menuarea=>$menuitem)
				{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
				}
				echo '</ul><div class="clear"></div></div>';
			}
		}
	} // end of fn AdminPagesBody
	
	protected function GetPageMenu()
	{	$menu = array();
		$menu['edit'] = array('text'=>'Edit', 'link'=>'pageedit.php?id=' . $this->editpage->id);
		if (!$this->editpage->details['redirectlink'] || ($this->editpage->details['redirectlink'] == '/'))
		{	$menu['donateform'] = array('text'=>'Quick Donate', 'link'=>'pagedonateform.php?id=' . $this->editpage->id);
		}
		$menu['sections'] = array('text'=>'Sections', 'link'=>'pagesections.php?id=' . $this->editpage->id);
		if ($this->pagesection->id)
		{	$menu['section'] = array('text'=>'Section #' . $this->pagesection->id, 'link'=>'pagesection.php?id=' . $this->pagesection->id);
			if ($this->pagesection->CanHaveSubSections())
			{	$menu['subsections'] = array('text'=>'Subsections', 'link'=>'pagesubsections.php?id=' . $this->pagesection->id);
				if ($this->pagesubsection->id)
				{	$menu['subsection'] = array('text'=>'Subsection #' . $this->pagesubsection->id, 'link'=>'pagesection.php?id=' . $this->pagesubsection->id);
				}
			}
		}
		return $menu;
	} // end of fn GetPageMenu
	
	function CMSBodyMain()
	{	$this->AdminPagesBody();
	} // end of fn CMSBodyMain
	
} // end of defn AdminEventsPage
?>