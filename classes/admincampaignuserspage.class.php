<?php
class AdminCampaignUsersPage extends AdminCampaignsPage
{	protected $campaignuser;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('campaignusers.php', 'Campaign owners');
		if ($this->campaignuser->id)
		{	
			$this->breadcrumbs->AddCrumb('campaignuser.php?id=' . $this->campaignuser->id, $this->InputSafeString($this->campaignuser->details['firstname'] . ' ' . $this->campaignuser->details['lastname']));
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AssignCampaign()
	{	$this->campaignuser = new AdminCampaignUser($_GET['id']);
	} // end of fn AssignCampaign
	
	protected function GetCampaignMenu()
	{	$menu = array();
		if ($this->campaignuser->id)
		{	
			$menu['edit'] = array('text'=>$this->InputSafeString($this->InputSafeString($this->campaignuser->details['firstname'] . ' ' . $this->campaignuser->details['lastname'])), 'link'=>'campaignuser.php?id=' . $this->campaignuser->id);
			$menu['page'] = array('text'=>'Page', 'link'=>$this->campaignuser->Link(), 'blank'=>true);
			$menu['campaigns'] = array('text'=>'Campaigns', 'link'=>'campaignusercampaigns.php?id=' . $this->campaignuser->id);
			if ($teamcount = count($this->campaignuser->GetTeams()))
			{	$menu['teams'] = array('text'=>'Teams joined', 'link'=>'campaignuserteams.php?id=' . $this->campaignuser->id);
			}
			$menu['susp'] = array('text'=>'Suspension', 'link'=>'campaignusersuspend.php?id=' . $this->campaignuser->id);
			$menu['password'] = array('text'=>'Change Password', 'link'=>'campaignuserpassword.php?id=' . $this->campaignuser->id);
		}
		return $menu;
	} // end of fn GetCampaignMenu
	
} // end of defn AdminCampaignsPage
?>
