<?php
class AdminEvent extends Event
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct

	public function SlugExists($slug = '')
	{	$sql = 'SELECT eid FROM events WHERE slug="' . $this->SQLSafe($slug) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT eid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	public function Save($data = array(), $imagefile = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($eventname = $this->SQLSafe($data['eventname']))
		{	$fields[] = 'eventname="' . $eventname . '"';
		} else
		{	$fail[] = 'event name cannot be empty';
		}
		
		if ($description = $this->SQLSafe($data['description']))
		{	$fields[] = 'description="' . $description . '"';
		} else
		{	$fail[] = 'description cannot be empty';
		}
	
		$metadesc = $this->SQLSafe($data['metadesc']);
		$fields[] = 'metadesc="' . $metadesc . '"';
	
		$snippet = $this->SQLSafe($data['snippet']);
		$fields[] = 'snippet="' . $snippet . '"';
		
		if ($this->id)
		{	$slug = $this->TextToSlug($data['slug']);
		} else
		{	if ($eventname)
			{	$slug = $this->TextToSlug($eventname);
			}
		}
		
		if ($slug)
		{	$suffix = '';
			while ($this->SlugExists($slug . $suffix))
			{	$suffix++;
			}
			$slug .= $suffix;
			
			$fields[] = 'slug="' . $slug . '"';
		} else
		{	if ($this->id || $eventname)
			{	$fail[] = 'slug missing';
			}
		}
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE events SET ' . $set . ' WHERE eid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO events SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new event added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}
		
		/*if ($this->id && $imagefile['size'])
		{	if ($this->ImageOKToUpload($imagefile))
			{	if (substr(strtolower($imagefile['name']), -4) == '.png')
				{	if (move_uploaded_file($imagefile['tmp_name'], $this->ImageFileLocation()))
					{	$success[] = 'Your image has been uploaded';
					}
				} else
				{	// copy to png format
					$temp_image = imagecreatefromjpeg($imagefile['tmp_name']);
					$new_image = imagecreatetruecolor(imagesx($temp_image), imagesy($temp_image));
					if (imagecopy($new_image, $temp_image, 0, 0, 0, 0, imagesx($temp_image), imagesy($temp_image)) && imagepng($new_image, $this->ImageFileLocation()))
					{	$success[] = 'Your image has been uploaded';
					}
					
				}
			} else
			{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
			}
		}*/
		
		if ($this->id)
		{	if($imagefile['size'])
			{	if ($this->ValidPhotoUpload($imagefile))
				{	$photos_created = 0;
					foreach ($this->imagesizes as $size_name=>$size)
					{	if (!file_exists($this->ImageFileDirectory($size_name)))
						{	mkdir($this->ImageFileDirectory($size_name));
						}
						if ($this->ReSizePhotoPNG($imagefile['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($imagefile['type'], 'png') ? 'png' : 'jpg'))
						{	$photos_created++;
						}
					}
					unset($imagefile['tmp_name']);
					if ($photos_created)
					{	$success[] = 'image uploaded';
					}
				} else
				{	$fail[] = 'image upload failed';
				}
			}
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function ValidPhotoUpload($photo_image = array())
	{	return is_array($photo_image) && (stristr($photo_image['type'], 'jpeg') || stristr($photo_image['type'], 'jpg') || stristr($photo_image['type'], 'png')) && !$photo_image['error'] && $photo_image['size'];
	} // end of fn ValidPhotoUpload
	
	function ReSizePhotoPNG($uploadfile = '', $file = '', $maxwidth = 0, $maxheight = 0, $imagetype = '')
	{	$isize = getimagesize($uploadfile);
		$ratio = $maxwidth / $isize[0];
		$h_ratio = $maxheight / $isize[1];
		if ($h_ratio > $ratio)
		{	$ratio = $h_ratio;
		}
		switch ($imagetype)
		{	case 'png': $oldimage = imagecreatefrompng($uploadfile);
							break;
			case 'jpg':
			case 'jpeg': $oldimage = imagecreatefromjpeg($uploadfile);
							break;
		}
		
		if ($oldimage)
		{	$w_new = ceil($isize[0] * $ratio);
			$h_new = ceil($isize[1] * $ratio);
			
			if ($maxwidth && $maxheight && $ratio != 1)
			{	$newimage = imagecreatetruecolor($w_new,$h_new);
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
				imagecopyresampled($newimage,$oldimage,0,0,0,0,$w_new, $h_new, $isize[0], $isize[1]);
			} else
			{	$newimage = $oldimage;
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
			}
			
			// now get middle chunk - horizontally
			if ($maxwidth && $maxheight && ($w_new > $maxwidth || $h_new > $maxheight))
			{	$resizeimg = imagecreatetruecolor($maxwidth, $maxheight);
				if ($imagetype == 'png')
				{	imagealphablending($resizeimg, false);
					imagesavealpha($resizeimg, true);
				}
				$leftoffset = floor(($w_new - $maxwidth) / 2);
				imagecopyresampled($resizeimg, $newimage, 0, 0, floor(($w_new - $maxwidth) / 2), floor(($h_new - $maxheight) / 2), $maxwidth, $maxheight, $maxwidth, $maxheight);
				$newimage = $resizeimg;
			}
			
			ob_start();
			imagepng($newimage, NULL, 3);
			return file_put_contents($file, ob_get_clean());
		}
	} // end of fn ReSizePhotoPNG
		
	protected function ImageOKToUpload($file = array())
	{	return !$file['error'] && ((substr(strtolower($file['name']), -5) == '.jpeg') || (substr(strtolower($file['name']), -4) == '.jpg') || (substr(strtolower($file['name']), -4) == '.png'));
	} // end of fn ImageOKToUpload
	
	public function CanDelete()
	{	return $this->id && !$this->GetDates();
	} // end of fn CanDelete
	
	public function DeleteExtra()
	{	foreach ($this->imagesizes as $size_name=>$size)
		{	@unlink($this->GetImageFile($size_name));
		}
	} // end of fn DeleteExtra
	
	public function InputForm()
	{	ob_start();
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddTextInput('Event name', 'eventname', $this->InputSafeString($data['eventname']), 'long');
		if ($this->id)
		{	$form->AddTextInput('Slug', 'slug', $this->InputSafeString($data['slug']), 'long', 255, 1);
		}
		$form->AddTextArea('Snippet (try to keep below 300 characters)', 'snippet', $this->InputSafeString($data['snippet']), '', 0, 0, 5, 60);
		$form->AddTextArea('Description', 'description', stripslashes($data['description']), 'tinymce', 0, 0, 30, 60);
		$form->AddFileUpload('Upload event image (ideally 500px wide by 620px)', 'eventimage');
		if ($this->id && ($src = $this->GetImageSRC('thumb')))
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $src . '" /><br /></p>');
		}
		$form->AddTextArea('Meta description', 'metadesc', $this->InputSafeString($data['metadesc']), '', 0, 0, 10, 60);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Event', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this event</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
	public function DatesTable($filter = array())
	{	ob_start();
		$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="7"><a href="eventdate.php?eid=', $this->id, '">Add new date</a></th></tr><tr><th>Venue</th><th>Start</th><th>End</th><th>Live?</th><th>Link</th><th>Booked</th><th>Actions</th></tr>';
		$filter['orderby'] = 'latest';
		if ($dates = $this->GetDates($filter))
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($dates as $daterow)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$date = new AdminEventDate($daterow);
					echo '<tr class="stripe', $i++ % 2,  '"><td>', $this->InputSafeString($date->venue['venuename'] . ' [' . $this->GetCountry($date->venue['country']) . ']'), '</td><td>', date($date->details['allday'] ? 'd-M-y' : 'd-M-y H:i', strtotime($date->details['starttime'])), '</td><td>', date($date->details['allday'] ? 'd-M-y' : 'd-M-y H:i', strtotime($date->details['endtime'])), '</td><td>', $date->details['live'] ? 'Live' : '', '</td><td>', $date->Link(), '</td><td class="num">', ($bookcount = count($date->GetBookings())), ' of ', $date->GetTicketCount(), '</td><td><a href="eventdate.php?id=', $date->id, '">edit</a>';
					if ($bookcount)
					{	echo '&nbsp;|&nbsp;<a href="eventdatebookings.php?id=', $date->id, '">bookings</a>';
					}
					if ($date->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="eventdate.php?id=', $date->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($dates) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($dates), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
		return ob_get_clean();
	} // end of fn DatesTable
	
} // end of defn AdminEvent
?>