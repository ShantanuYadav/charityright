<?php
class BlankItem extends Base
{	var $details = array();
	var $id = '';
	var $admintitle = '';
	var $langused = array();
	var $tablename = '';
	var $idfield = '';
	var $language = '';
	
	function __construct($id = 0, $tablename = '', $idfield = '')
	{	parent::__construct();
		$this->AssignObjectLanguage();
		$this->tablename = $tablename;
		$this->idfield = $idfield;
		if ($this->tablename && $this->idfield)
		{	$this->Get($id);
		}
	} // fn __construct
	
	function AssignObjectLanguage()
	{	$this->language = $this->lang;
	} // end of fn AssignObjectLanguage
	
	function AdminAssignObjectLanguage()
	{	if (!$this->language = $_GET['lang'])
		{	$this->language = $this->def_lang;
		}
	} // end of fn AssignHBLanguage
	
	function Reset()
	{	$this->details = array();
		$this->id = '';
		$this->ResetExtra();
	} // end of fn Reset
	
	function ResetExtra()
	{	
	} // end of fn ResetExtra
	
	function Get($id = '')
	{	$this->Reset();
		if (is_array($id))
		{	$this->details = $id;
			if ($this->id = $id[$this->idfield])
			{	$this->AddDetailsForLang($this->language);
				$this->GetExtra();
				return true;
			}
		} else
		{	if ($id = (int)$id)
			{	if ($result = $this->db->Query('SELECT * FROM ' . $this->tablename . ' WHERE ' . $this->idfield . '=' . $id))
				{	if ($row = $this->db->FetchArray($result))
					{	return $this->Get($row);
					}
				}
			}
		}
		
	} // end of fn Get
	
	function AddDetailsForLang($lang = "")
	{	$sql = "SELECT * FROM {$this->tablename}_lang WHERE {$this->idfield}={$this->id} AND lang='$lang'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	foreach ($row as $field=>$value)
				{	$this->details[$field] = $value;
				}
			} else
			{	if ($lang == $this->def_lang)
				{	// as last resort go for english
					if ($lang != "en") // only if default language not english
					{	$sql = "SELECT * FROM {$this->tablename}_lang WHERE {$this->idfield}=$this->id AND lang='en'";
						if ($result = $this->db->Query($sql))
						{	if ($row = $this->db->FetchArray($result))
							{	foreach ($row as $field=>$value)
								{	$this->details[$field] = $value;
								}
							}
						}
					}
				} else
				{	$this->AddDetailsForDefaultLang();
				}
			}
		}
	
	} // end of fn AddDetailsForLang
	
	function AddDetailsForDefaultLang()
	{	$this->AddDetailsForLang($this->def_lang);
	} // end of fn AddDetailsForDefaultLang
	
	function GetExtra()
	{	
	} // end of fn GetExtra
	
	function CanDelete()
	{	return false;
	} // end of fn CanDelete
	
	function Delete()
	{	if ($this->CanDelete())
		{	$sql = 'DELETE FROM ' . $this->tablename . ' WHERE ' . $this->idfield . '=' . (int)$this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->db->Query('DELETE FROM ' . $this->tablename . '_lang WHERE ' . $this->idfield . '=' . (int)$this->id);
					$this->DeleteExtra();
					$this->Reset();
					return true;
				}
			}
		}
	} // end of fn Delete
	
	function DeleteExtra()
	{	
	} // end of fn DeleteExtra
	
	function GetLangUsed()
	{	$this->langused = array();
		if ($this->id)
		{	if ($result = $this->db->Query("SELECT lang FROM {$this->tablename}_lang WHERE {$this->idfield}=$this->id"))
			{	while ($row = $this->db->FetchArray($result))
				{	$this->langused[$row["lang"]] = true;
				}
			}
		}
	} // end of fn GetLangUsed
	
	function GetDefaultDetails()
	{	$sql = "SELECT * FROM {$this->tablename}_lang WHERE {$this->idfield}=$this->id AND lang='{$this->def_lang}'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return $row;
			}
		}
		return array();
	} // end of fn GetDefaultDetails
	
	function GetAdminTitle($title_field = "")
	{	if ($this->id && $title_field)
		{	if (!$this->admintitle = $this->details[$title_field])
			{	if ($details = $this->GetDefaultDetails())
				{	$this->admintitle = $details[$title_field] . " [{$this->language}]";
				}
			}
		}
	} // end of fn GetAdminTitle
	
	public function ValidPhotoUpload($photo_image = array())
	{	return is_array($photo_image) && (stristr($photo_image['type'], 'jpeg') || stristr($photo_image['type'], 'jpg') || stristr($photo_image['type'], 'png')) && !$photo_image['error'] && $photo_image['size'];
	} // end of fn ValidPhotoUpload
	
	function ReSizePhotoPNG($uploadfile = '', $file = '', $maxwidth = 0, $maxheight = 0, $imagetype = '')
	{	$isize = getimagesize($uploadfile);
		$ratio = $maxwidth / $isize[0];
		$h_ratio = $maxheight / $isize[1];
		if ($h_ratio > $ratio)
		{	$ratio = $h_ratio;
		}
		switch ($imagetype)
		{	case 'png': $oldimage = imagecreatefrompng($uploadfile);
							break;
			case 'jpg':
			case 'jpeg': $oldimage = imagecreatefromjpeg($uploadfile);
							break;
		}
		
		if ($oldimage)
		{	$w_new = ceil($isize[0] * $ratio);
			$h_new = ceil($isize[1] * $ratio);
			
			if ($maxwidth && $maxheight && $ratio != 1)
			{	$newimage = imagecreatetruecolor($w_new,$h_new);
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
				imagecopyresampled($newimage,$oldimage,0,0,0,0,$w_new, $h_new, $isize[0], $isize[1]);
			} else
			{	$newimage = $oldimage;
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
			}
			
			// now get middle chunk - horizontally
			if ($maxwidth && $maxheight && ($w_new > $maxwidth || $h_new > $maxheight))
			{	$resizeimg = imagecreatetruecolor($maxwidth, $maxheight);
				if ($imagetype == 'png')
				{	imagealphablending($resizeimg, false);
					imagesavealpha($resizeimg, true);
				}
				$leftoffset = floor(($w_new - $maxwidth) / 2);
				imagecopyresampled($resizeimg, $newimage, 0, 0, floor(($w_new - $maxwidth) / 2), floor(($h_new - $maxheight) / 2), $maxwidth, $maxheight, $maxwidth, $maxheight);
				$newimage = $resizeimg;
			}
			
			ob_start();
			imagepng($newimage, NULL, 3);
			return @file_put_contents($file, ob_get_clean());
		}
	} // end of fn ReSizePhotoPNG
	
} // end of defn BlankItem
?>