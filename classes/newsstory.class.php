<?php
class NewsStory extends Base
{	var $id = 0;
	var $details = array();
	var $language = '';
	var $imagelocation = "";
	var $imagedir = "";
	var $image_w = 250;

	function __construct($id = 0)
	{	parent::__construct();
		$this->imagelocation = SITE_URL . "img/news/";
		$this->imagedir = CITDOC_ROOT . "/img/news/";
		$this->AssignNewsLanguage();
		$this->Get($id);
	} //  end of fn __construct
	
	function AssignNewsLanguage()
	{	$this->language = $this->lang;
	} // end of fn AssignNewsLanguage
	
	function Get($id = 0)
	{	$this->ReSet();
		
		if (is_array($id))
		{	$this->id = (int)$id['newsid'];
			$this->details = $id;
			// get text from language file
			$this->AddDetailsForLang($this->language);
		} else
		{	if ($result = $this->db->Query("SELECT * FROM news WHERE newsid=" . (int)$id))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
		
	} // end of fn Get
	
	function AddDetailsForLang($lang = "")
	{	$sql = "SELECT * FROM news_lang WHERE newsid={$this->id} AND lang='$lang'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	foreach ($row as $field=>$value)
				{	$this->details[$field] = $value;
				}
			} else
			{	if ($lang == $this->def_lang)
				{	// as last resort go for english
					if ($lang != "en") // only if default language not english
					{	$sql = "SELECT * FROM news_lang WHERE newsid=$this->id AND lang='en'";
						if ($result = $this->db->Query($sql))
						{	if ($row = $this->db->FetchArray($result))
							{	foreach ($row as $field=>$value)
								{	$this->details[$field] = $value;
								}
							}
						}
					}
				} else
				{	$this->AddDetailsForDefaultLang();
				}
			}
		}
	
	} // end of fn AddDetailsForLang
	
	function AddDetailsForDefaultLang()
	{	$this->AddDetailsForLang($this->def_lang);
	} // end of fn AddDetailsForDefaultLang
	
	function ReSet()
	{	
		$this->id = 0;
		$this->details = array();
	} // end of fn ReSet
	
	public function FrontPage($headertag = 'h3', $maxtext = 350)
	{	ob_start();
		echo '<li class="news_story"><div class="news_date">', date('M \'y<\s\p\a\n>d</\s\p\a\n>', strtotime($this->details['submitted'])), '</div><div class="news_body"><', $headertag, '>', $this->InputSafeString($this->details['headline']), '</', $headertag, '><p>', $this->SummaryText($maxtext, false), ' ... </p><a class="news_link" href="', $this->Link(), '">Read more&nbsp;&raquo;</a></div><div class="clear"></div></li>';
		return ob_get_clean();
	} // end of fn FrontPage
	
	public function MetaDesc()
	{	if ($this->details['metadesc'])
		{	return $this->InputSafeString($this->details['metadesc']);
		} else
		{	return str_replace(array("\r\n", "\n", "\r", "  ", "\t"), ' ', $this->SummaryText(350, false)) . ' ...';
		}
	} // end of fn MetaDesc
	
	public function FullStory()
	{	ob_start();
		echo '<div class="news_story"><h2 class="news_date">', date('M \'y<\s\p\a\n>d</\s\p\a\n>', strtotime($this->details['submitted'])), '</h2><div class="news_body"><h1>', $this->InputSafeString($this->details['headline']), '</h1>', stripslashes($this->details['newstext']), '<a class="news_link" href="', SITE_URL, 'news.php">More stories</a></div><div class="clear"></div></div>';
		return ob_get_clean();
	} // end of fn FullStory
	
	public function SocialButtons()
	{	ob_start();
		echo '<div class="newsSocial"><div class="fb-like" data-href="', $this->Link(), '" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div></div>';
		return ob_get_clean();
	} // end of fn SocialButtons
	
	public function SummaryText($climit = 80, $include_header = false)
	{	ob_start();
		if ($climit = 80)
		{	if ($include_header)
			{	$header = substr($this->details['headline'], 0, $climit);
				echo '<span class="news_storyheader">', $this->InputSafeString($header), ' ... </span>';
			}
			if ($this->details['snippet'])
			{	echo $this->InputSafeString($this->details['snippet']);
			} else
			{	echo $this->InputSafeString(substr(html_entity_decode(strip_tags($this->details['newstext']), ENT_QUOTES, "utf-8"), 0, $climit - strlen($header)));
			}
		}
		return ob_get_clean();
	} // end of fn SummaryText

	public function FullText()
	{	return stripslashes($this->details['newstext']);
	} // end of fn NewsPageText
	
	function Text2Para($text = '')
	{	$paras = array();
		if ($text)
		{	foreach (explode("\n", $text) as $para)
			{	$paras[] = '<p>' . $this->InputSafeString($para) . '</p>';
			}
		}
		return implode('', $paras);
	} // end of fn Text2Para
	
	public function ImageFile()
	{	return $this->imagedir . (int)$this->id . ".jpg";
	} // end of fn ImageFile
	
	public function ImageSRC()
	{	return $this->imagelocation . (int)$this->id . ".jpg";
	} // end of fn ImageSRC
	
	public function PhotoExists()
	{	return file_exists($this->ImageFile());
	} // end of fn PhotoExists
	
	public function Link()
	{	return SITE_URL . 'news/' . $this->id . '/' . $this->details['slug'] . '/';
	} // end of fn Link
	
} // end of defn NewsStory
?>