<?php
class AdminPostCatsPage extends AdminPostsPage
{	protected $category;
	
	protected function AssignPost()
	{	$this->category = new AdminPostCategory($_GET['id']);
	} // end of fn AssignPost
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('postcatslist.php', 'Categories');
		if ($this->category->id)
		{	$this->breadcrumbs->AddCrumb('postcat.php?id=' . $this->category->id, $this->InputSafeString($this->category->details['catname']));
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function GetPostMenu()
	{	$menu = array();
		if ($this->category->id)
		{	$menu['catedit'] = array('text'=>'Edit', 'link'=>'postcat.php?id=' . $this->category->id);
			$menu['catposts'] = array('text'=>'Posts', 'link'=>'posts.php?postcat=' . $this->category->id);
			$menu['donateform'] = array('text'=>'Quick Donate', 'link'=>'postcatdonateform.php?id=' . $this->category->id);
		}
		return $menu;
	} // end of fn GetPostMenu
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('posts'))
		{	$this->AdminPostsBody();
		}
	} // end of fn AdminBodyMain
	
} // end of defn AdminPostsPage
?>
