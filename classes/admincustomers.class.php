<?php
class AdminCustomers extends Base
{	var $customers = array();
	
	function __construct($email = '')
	{	parent::__construct();
		$this->Get($email);
	} //  end of fn __construct
	
	function Reset()
	{	$this->customers = array();
	} // end of fn Reset
	
	function Get($email = '')
	{	
		$where = array();
		if ($email = $this->SQLSafe($email))
		{	$where[] = 'email LIKE "%' . $email . '%"';
		}
		
		$sql = 'SELECT * FROM customers';
		if ($wstr = implode(' AND ', $where))
		{	$sql .= ' WHERE ' . $wstr;
		}
		
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->customers[$row['custid']] = new AdminCustomer($row);
			}
		}
		
	} // end of fn Get
	
	function ListCustomers()
	{	echo "<table id='pagelist'><tr><th>email</th><th>Registered</th><th>Actions</th></tr>";
		foreach ($this->customers as $customer)
		{	echo "<tr class='stripe", $i++ % 2, "'><td><a href='customer.php?id=", $customer->id, "'>", $this->InputSafeString($customer->details["email"]), "</a></td><td>", date("d-M-y @H:i", strtotime($customer->details["regdate"])), "</td><td><a href='customer.php?id=", $customer->id, "'>view</a></td></tr>";
		}
		if ($this->customers)
		{	echo "<tr><th colspan='3'><a target='_blank' href='customerscsv.php?email={$_GET["email"]}'>Download CSV</a></th></tr>";
		}
		echo "</table>";
	} // end of fn ListCustomers
	
	function CSVDownload()
	{	header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"blackhorse-customers.csv\"");
		echo "customer id,email,name,registered,company name,city,country,phone\n";
		foreach ($this->customers as $customer)
		{	
			echo $customer->id, ",\"", $customer->details["email"], "\",\"", stripslashes($customer->details["fullname"]), "\",", date("d-m-Y", strtotime($customer->details["regdate"])), ",\"", stripslashes($customer->details["compname"]), "\",\"", stripslashes($customer->details["city"]), "\",\"", stripslashes($customer->details["country"]), "\",\"", stripslashes($customer->details["phone"]), "\"\n";
		}
		
	} // end of fn CSVDownload
	
} // end of defn AdminCustomer
?>