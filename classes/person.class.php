<?php
class Person extends BlankItem
{	protected $imagesizes = array('default'=>array('w'=>120, 'h'=>120));
	protected $imagelocation = '';
	protected $imagedir = '';

	function __construct($id = 0)
	{	parent::__construct($id, 'people', 'pid');
		$this->imagelocation = SITE_URL . 'img/people/';
		$this->imagedir = CITDOC_ROOT . '/img/people/';
	} //  end of fn __construct
	
	public function ImageDimensions($size = 'default')
	{	if ($this->imagesizes[$size])
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions
	
	public function GetImageFile($size = 'default')
	{	return $this->ImageFileDirectory($size) . '/' . $this->id .'.png';
	} // end of fn GetImageFile
	
	public function ImageFileDirectory($size = 'default')
	{	return $this->imagedir . $this->InputSafeString($size);
	} // end of fn FunctionName
	
	public function GetImageSRC($size = 'default')
	{	return $this->imagelocation . $this->InputSafeString($size) . '/' . $this->id . '.png';
	} // end of fn GetImageSRC
	
	public function ImageHTML($size = 'default')
	{	if ($src = $this->GetImageSRC($size))
		{	ob_start();
			echo '<img src="', $src, '" alt="', $alt = $this->InputSafeString($this->details['imagedesc']), '" title="', $alt, '" />';
			return ob_get_clean();
		}
	} // end of fn ImageHTML
	
	public function GetPersonTypes()
	{	$types = array('trustee'=>'trustee', 'dummy'=>'dummy');
		return $types;
	} // end of fn GetPersonTypes
	
} // end of defn Person
?>