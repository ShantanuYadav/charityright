<?php
class AdminCampaignText extends CampaignText
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} // fn __construct
	
	function Save($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if ($ctname = $this->SQLSafe($data['ctname']))
		{	$fields[] = 'ctname="' . $ctname . '"';
		} else
		{	$fail[] = 'name cannot be empty';
		}
		if ($cttext = $this->SQLSafe($data['cttext']))
		{	$fields[] = 'cttext="' . $cttext . '"';
		} else
		{	$fail[] = 'sample text cannot be empty';
		}
		$fields[] = 'listorder=' . (int)$data['listorder'];
		
		if ($this->id || !$fail)
		{	$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE campaigntext SET ' . $set . ' WHERE ctid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO campaigntext SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$record_changes = true;
						$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new sample text added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
				
				if ($this->id)
				{	$this->Get($this->id);
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function InputForm()
	{	ob_start();
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddTextInput('Name of sample text', 'ctname', $this->InputSafeString($data['ctname']), 'long');
		$form->AddTextArea('Sample text', 'cttext', $this->InputSafeString($data['cttext']), '', 0, 0, 5, 60);
		$form->AddTextInput('Order in list', 'listorder', (int)$data['listorder'], 'number');
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create sample text', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this sample text</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // fn InputForm
	
	public function CanDelete()
	{	return true;
	} // fn CanDelete
	
} // end of defn AdminCampaignText
?>