<?php
class AdminCampaignAvatar extends CampaignAvatar
{	

	function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct
	
	function Save($data = array(), $photo_image = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($imagedesc = $this->SQLSafe($data['imagedesc']))
		{	$fields[] = 'imagedesc="' . $imagedesc . '"';
		} else
		{	$fail[] = 'description cannot be empty';
		}
		
		if (!$this->id)
		{	// check for image uploaded and valid
			if (!$this->ValidPhotoUpload($photo_image))
			{	$fail[] = 'valid image not uploaded';
			}
		}
		
		$fields['listorder'] = 'listorder=' . (int)$data['listorder'];
		$fields['live'] = 'live=' . ($data['live'] ? '1' : '0');
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE campaignavatars SET ' . $set . ' WHERE caid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO campaignavatars SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new avatar added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $this->db->Error();
			
			if ($this->id)
			{	if($photo_image['size'])
				{	if ($this->ValidPhotoUpload($photo_image))
					{	$photos_created = 0;
						foreach ($this->imagesizes as $size_name=>$size)
						{	if (!file_exists($this->ImageFileDirectory($size_name)))
							{	mkdir($this->ImageFileDirectory($size_name));
							}
							if ($this->ReSizePhotoPNG($photo_image['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($photo_image['type'], 'png') ? 'png' : 'jpg'))
							{	$photos_created++;
							}
						}
						unset($photo_image['tmp_name']);
						if ($photos_created)
						{	$success[] = 'image uploaded';
						}
					} else
					{	$fail[] = 'image upload failed';
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function DeleteExtra()
	{	foreach ($this->imagesizes as $size_name=>$size)
		{	@unlink($this->GetImageFile($size_name));
		}
	} // end of fn DeleteExtra
	
	function CanDelete()
	{	return $this->id && !$this->UsedCount();
	} // end of fn CanDelete
	
	function InputForm()
	{	 
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id, '');
		$form->AddTextInput('Admin name', 'imagedesc', $this->InputSafeString($data['imagedesc']), 'long');
		$form->AddTextInput('List order', 'listorder', (int)$data['listorder'], 'number');
		$form->AddCheckBox('Live (selectable in front end)', 'live', 1, $data['live']);
		$form->AddFileUpload('Image file (jpg or png only)', 'imagefile');
		if ($this->id && ($src = $this->GetImageSRC('small')))
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $src . '" /><br /></p>');
		}
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Avatar', 'submit');
		
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this avatar</a></p>';
			}
		}
		$form->Output();
	} // end of fn InputForm
	
} // end of defn AdminCampaignAvatar
?>