<?php
class AdminNewsCat extends NewsCat
{	public $admintitle = "";
	public $langused = array();

	function __construct($id = 0)
	{	parent::__construct($id);
		$this->GetLangUsed();
		$this->GetAdminTitle();
	} //  end of fn __construct
	
	function GetAdminTitle()
	{	if ($this->id)
		{	if (!$this->admintitle = $this->details["catname"])
			{	if ($details = $this->GetDefaultDetails())
				{	$this->admintitle = $details["catname"] . " [{$this->language}]";
				}
			}
		}
	} // end of fn GetAdminTitle
	
	function GetDefaultDetails()
	{	$sql = "SELECT * FROM newscats_lang WHERE catid=$this->id AND lang='{$this->def_lang}'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return $row;
			}
		}
		return array();
	} // end of fn GetDefaultDetails
	
	function AssignNewsLanguage()
	{	if (!$this->language = $_GET["lang"])
		{	$this->language = $this->def_lang;
		}
	} // end of fn AssignNewsLanguage
	
	function AddDetailsForDefaultLang(){}
	
	function GetLangUsed()
	{	$this->langused = array();
		if ($this->id)
		{	if ($result = $this->db->Query("SELECT lang FROM newscats_lang WHERE catid=$this->id"))
			{	while ($row = $this->db->FetchArray($result))
				{	$this->langused[$row["lang"]] = true;
				}
			}
		}
	} // end of fn GetLangUsed
	
	function Save($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		$l_fields = array();
		$admin_actions = array();
		
		if ($catname = $this->SQLSafe($data["catname"]))
		{	$l_fields[] = "catname='$catname'";
			if ($this->id && ($data["catname"] != $this->details["catname"]))
			{	$admin_actions[] = array("action"=>"Name ({$this->language})", "actionfrom"=>$this->details["catname"], "actionto"=>$data["catname"]);
			}
		} else
		{	$fail[] = "category name cannot be empty";
		}
	
		$listorder = (int)$data["listorder"];
		$fields[] = "listorder=" . $listorder;
		if ($this->id && ($listorder != (int)$this->details["listorder"]))
		{	$admin_actions[] = array("action"=>"List order", "actionfrom"=>$this->details["listorder"], "actionto"=>$listorder);
		}
	
		if ($this->id || !$fail)
		{	
			$set = implode(", ", $fields);
			if ($this->id)
			{	$sql = "UPDATE newscats SET $set WHERE catid=" . $this->id;
			} else
			{	$sql = "INSERT INTO newscats SET $set";
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$record_changes = true;
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = "new story added";
							$this->RecordAdminAction(array("tablename"=>"newscats", "tableid"=>$this->id, "area"=>"news category", "action"=>"created"));
							$this->GetAdminTitle();
						} else
						{	$fail[] = "insert failed";
						}
					}
				}
				
				if ($this->id)
				{	
					if ($set = implode(", ", $l_fields))
					{	if ($this->langused[$this->language])
						{	$sql = "UPDATE newscats_lang SET $set WHERE catid=$this->id AND lang='$this->language'";
						} else
						{	$sql = "INSERT INTO newscats_lang SET $set, catid=$this->id, lang='$this->language'";
						}
						if ($result = $this->db->Query($sql))
						{	if ($this->db->AffectedRows())
							{	$success[] = "text changes saved";
								$record_changes = true;
							}
						}
					}
					$this->Get($this->id);
					$this->GetLangUsed();
				}
				
				if ($record_changes)
				{	$base_parameters = array("tablename"=>"newscats", "tableid"=>$this->id, "area"=>"news category");
					if ($admin_actions)
					{	foreach ($admin_actions as $admin_action)
						{	$this->RecordAdminAction(array_merge($base_parameters, $admin_action));
						}
					}
				}
			}
			
		}
		
		return array("failmessage"=>implode(", ", $fail), "successmessage"=>implode(", ", $success));
		
	} // end of fn Save
	
	function Delete()
	{	if ($this->CanDelete())
		{	$sql = "DELETE FROM newscats WHERE catid=" . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->db->Query("DELETE FROM newscats_lang WHERE catid=$this->id");
					$this->RecordAdminAction(array("tablename"=>"newscats", "tableid"=>$this->id, "area"=>"news category", "action"=>"deleted"));
					$this->Reset();
					return true;
				}
			}
		}
	} // end of fn Delete
	
	function CanDelete()
	{	return $this->id && $this->CanAdminUserDelete() && !count($this->GetStories());
	} // end of fn CanDelete
	
	function InputForm()
	{	 
		$data = $this->details;
		if ($this->id)
		{	
			if (!$this->langused[$this->language])
			{	if ($_POST)
				{	// initialise details from this
					foreach ($_POST as $field=>$value)
					{	$data[$field] = $value;
					}
				}
			}
		} else
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id . '&lang=' . $this->language, 'newsform');
		$form->AddTextInput('Category name', 'catname', $this->InputSafeString($data['catname']));
		$form->AddTextInput('Order in list', 'listorder', (int)$data['listorder'], 'num', 4);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Category', 'submit');
		if ($histlink = $this->DisplayHistoryLink('newscats', $this->id))
		{	echo '<p>', $histlink, '</p>';
		}
		if ($this->id)
		{	echo '<h3>Editing ... ', $this->InputSafeString($this->admintitle), '</h3>';
			if ($this->CanDelete())
			{	echo '<p class='adminItemDeleteLink'><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this category</a></p>';
			}
			$this->AdminEditLangList();
		}
		$form->Output();
	} // end of fn InputForm
	
} // end of defn AdminNewsCat
?>