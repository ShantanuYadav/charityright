<?php
class_exists('Form');

class AdminPage extends BasePage
{	var $usertypes = array();
	var $adminarea = '';

	function __construct($adminarea = '')
	{	parent::__construct();
		$this->adminarea = $adminarea;
		$this->SetUser();
		$this->css = array('adminpage.css');
		$this->title = $this->GetParameter('shorttitle') . ' admin';
		$this->breadcrumbs = new BreadCrumbs('', 'Admin');
		$this->breadcrumbs->AddRightLink('../index.php', 'Live Site');
		$this->headerMenuButtons = array();
		if ($this->user->loggedin)
		{	$this->LoggedInConstruct();
		}
	//	ob_start();
	//	$this->VarDump($_SESSION);
	//	$this->LogInDiagnostics(ob_get_clean());
	} //  end of fn __construct

	function LoggedInConstruct()
	{	$this->css[] = 'dropdown.css';
		$this->css[] = 'admindropdown.css';
		$this->css[] = 'jqModal.css';
		$this->js['jqModal'] = 'https://cdnjs.cloudflare.com/ajax/libs/jqModal/1.4.2/jqModal.min.js';
		$this->js[] = 'admin_actions.js';
	} // end of fn LoggedInConstruct
	
	protected function jsSiteRootValue()
	{	return SITE_SUB . '/' . ADMIN_URL . '/';
	} // end of fn jsSiteRootValue
	
	function HeaderMenu(){}
	function SetDefaultCountry(){}
	function ShareThisJS(){}
	protected function SetUTMValues(){}
	protected function GoogleTagManager(){}
	protected function PrivyWidgetCode(){}
	function GoogleAnalytics(){}// no analytics on admin
	protected function FaceBookPixel(){}
	protected function MyTenNightsCode(){}
	protected function GoogleTagManagerCode(){}
	protected function FacebookSDKFooter(){}
	protected function LiveChatFooter(){}
	
	function Messages()
	{	
		if ($this->successmessage)
		{	echo '<div class="successmessage">', $this->successmessage, '</div>';
		}
		if ($this->failmessage)
		{	echo '<div class="failmessage">', $this->failmessage, '</div>';
		}
		if ($this->warningmessage)
		{	echo '<div class="warningmessage">', $this->warningmessage, '</div>';
		}
	} // end of fn Messages
	
	function AdminMenu()
	{	$adminmenu = new AdminMenu($this->user);
		if ($adminmenu->menuitems)
		{	echo '<div id="ddheader-menu"><ul class="dropdown dropdown-horizontal" id="ddmenu">';
			foreach ($adminmenu->menuitems as $item)
			{	$this->AdminMenuButton($item);
			}
			echo '</ul><div class="clear"></div></div><div class="clear"></div>';
		}
	} // end of fn AdminMenu
	
	function AdminMenuButton(AdminMenuItem $item)
	{	echo '<li><a', $this->adminarea && (strtoupper($item->details['menuarea']) == $this->adminarea) ? ' class="selected"' : '', ' href="', $item->details['menulink'] ? $item->details['menulink'] : ('rawmenu.php?id=' . $item->id), '">', $this->InputSafeString($item->details['menutext']), '</a>', $this->SubMenu($item->submenu), '</li>';
	} // end of fn AdminMenuButton
	
	function SubMenu($submenu = array())
	{	ob_start();
		if (is_array($submenu->menuitems) && count($submenu->menuitems))
		{	echo "<ul>";
			foreach ($submenu->menuitems as $item)
			{	echo "<li><a href='", $item->details["menulink"] ? $item->details["menulink"] : "rawmenu.php?id={$item->id}", 
					"'>", $this->InputSafeString($item->details["menutext"]), "</a>";
				if (is_array($item->submenu->menuitems) && count($item->submenu->menuitems))
				{	echo $this->SubMenu($item->submenu);
				}
				echo "</li>\n";
			}
			echo "</ul>";
		}
		return ob_get_clean();
	} // end of fn SubMenu
	
	function SetUser()
	{	if ($_GET['logout'])
		{	unset($_SESSION[SITE_NAME]['auserid']);
		} else
		{	if ($_POST['ausername'] && $_POST['apass'])
			{	$_SESSION[SITE_NAME]['auserid'] = $this->LogIn();
				//$this->LogInDiagnostics($_SESSION[SITE_NAME]['auserid'] . ' logged in and added to session');
			}
		}
	//	$this->user = $this->GetAdminUser();
		$this->user = new AdminUser((int)$_SESSION[SITE_NAME]['auserid']);
		ob_start();
		
	} // end of fn SetUser
	
	function LogIn()
	{	$userid = 0;
		$username = $_POST['ausername'];
		$pass = $_POST['apass'];
		if ($result = $this->db->Query($sql = 'SELECT auserid FROM adminusers WHERE ausername="' . $username . '" AND upassword=MD5("' . $pass . '")'))
		{	if ($row = $this->db->FetchArray($result))
			{	$userid = (int)$row['auserid'];
			} else
			{	$this->failmessage = 'log in failed';
				
			}
		} else
		{	$this->failmessage = 'log in failed db ' . $this->db->Error();
		}
		return $userid;
	} // end of fn LogIn
	
	function LogInDiagnostics($message = '')
	{	$mail = new HTMLMail();
		$mail->SetSubject('admin login fail');
		$mail->Send('tim@websquare.co.uk', $message, strip_tags($message));
	} // end of fn LogInDiagnostics

	function MainBody() //
	{	
		if ($this->user->loggedin)
		{	$this->AdminMenu();
			$this->DisplayBreadcrumbs();
			$this->Messages();
		}
		if ($this->user->loggedin)
		{	$this->AdminBodyMainContainer();
			if ($this->CanSeeHistory())
			{	echo $this->HistoryPopUp();
			}
		}
	} // end of fn MainBody

	public function FacbookInit(){}
	public function MetaTags(){}
	
	function HistoryPopUp()
	{	ob_start();
		echo '<script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow")); $("#aa_modal_popup").jqm({trigger:".historyOpener"});});</script><!-- START user info modal popup --><div id="aa_modal_popup" class="jqmWindow" style="padding-bottom: 5px; width: 840px; height: 470px; margin-left: -420px; top: 10px;"><a href="#" class="jqmClose submit">Close</a><div id="aaModalInner"></div></div><!-- EOF invite code modal popup -->';
		return ob_get_clean();
	} // end of fn HistoryPopUp
	
	function DisplayBreadcrumbs()
	{	if ($this->user->loggedin)
		{	$this->breadcrumbs->Display();
		}
	} // end of fn DisplayBreadcrumbs
	
	function AdminBodyMainContainer()
	{	echo "<div id='container'>";
		$this->AdminBodyMain();
		echo "<br class='clear' /></div>";
	} // end of fn AdminBodyMainContainer
	
	function AdminBodyMain()
	{	$this->HeaderMenu();
	} // end of fn AdminBodyMain
	
	function Header()
	{	echo '<div id="header"><h1>Administration Tools</h1>';
		if (!$this->user->loggedin)
		{	$this->LoginForm();
		}
		echo '<div class="clear"></div></div>';
	} // end of fn Header
	
	function LogInForm() // overrides existing
	{	echo '<div id="login"><div id="content"><form action="index.php" method="post" name="login"',
				'<label for="ausername">Username:</label>',
				'<input name="ausername" id="ausername" type="text" size="40" />',
				'<label for="apass">Password:</label>',
				'<input name="apass" id="apass" type="password" size="30" />',
				'<p>&nbsp;</p>',
				'<input name="submit" class="submit" type="submit" value="Log in" />',
			'</form></div></div>';
		
	} // end of fn LogInForm

	function LoggedInHeader()
	{	echo '<div id="login"><div id="content"><h2>Welcome ', $this->user->details["firstname"], '</h2>';
		$this->LogOutLink();
		echo '</div></div>';
	} // end of fn LoggedInHeader
	
	function TableFromQuery($sql = '')
	{	if ($result = $this->db->Query($sql))
		{	if ($numrows = $this->db->NumRows($result))
			{	echo '<table>';
				while ($row = $this->db->FetchArray($result))
				{	if (!$rcount++)
					{	// then do header row
						$fcount = count($row);
						echo '<tr>';
						foreach ($row as $field=>$value)
						{	echo '<th>', $field, '</th>';
						}
						echo '</tr>';
					}
					echo '<tr>';
					foreach ($row as $field=>$value)
					{	echo '<td>', $this->InputSafeString($value), '</td>';
					}
					echo '</tr>';
				}
				echo '<tr><td colspan="', $fcount, '">Records found: ', (int)$numrows, '</td></tr></table>';
			}
		}
	
	} // end of fn TableFromQuery
	
	function Footer()
	{	echo '<div id="footer">Websquare IT Solutions - Copyright &copy; ', @date('Y'), '</div>';
	} // end of fn Footer
	
	function Redirect($url = '')
	{	header('location: ' . SITE_SUB . '/' . ADMIN_URL . '/' . $url);
		exit;
	} // end of fn Redirect
	
} // end of defn AdminPage
?>