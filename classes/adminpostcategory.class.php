<?php
class AdminPostCategory extends PostCategory
{	
	
	public function __construct($id = 0, $slug = '')
	{	parent::__construct($id, $slug);
	} // fn __construct
	
	public function CanDelete()
	{	return $this->id && !$this->GetPosts();
	} // fn CanDelete
	
	public function GetPosts($filter = array(), $liveonly = false)
	{	return parent::GetPosts($filter = array(), false);
	} // fn GetPosts

	public function SlugExists($slug = '')
	{	$sql = 'SELECT pcatid FROM postcategories WHERE slug="' . $this->SQLSafe($slug) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT pcatid=' . $this->id;
		}
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return true;
			}
		}
		return false;
	} // end of fn SlugExists
	
	public function Save($data = array(), $imagefile = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($catname = $this->SQLSafe($data['catname']))
		{	$fields['catname'] = 'catname="' . $catname . '"';
		} else
		{	$fail[] = 'title cannot be empty';
		}
		
		if (!$pagetitle = $this->SQLSafe($data['pagetitle']))
		{	$pagetitle = 'Charity Right ' . $catname;
		}
		$fields['pagetitle'] = 'pagetitle="' . $pagetitle . '"';
	
		$fields['metadesc'] = 'metadesc="' . $this->SQLSafe($data['metadesc']) . '"';
		$fields['pagecontent'] = 'pagecontent="' . $this->SQLSafe($data['pagecontent']) . '"';
		if ($this->CanAdminUser('technical'))
		{	$fields['postsections'] = 'postsections="' . $this->SQLSafe($data['postsections']) . '"';
		}
	
		$templates = $this->PossibleTemplates();
		if ($templates[$data['template']])
		{	$fields['template'] = 'template="' . $data['template'] . '"';
		} else
		{	$fail[] = 'invalid template';
		}
		
		if ($this->id)
		{	$slug = $this->TextToSlug($data['slug']);
		} else
		{	if ($catname)
			{	$slug = $this->TextToSlug($catname);
			}
		}
		
		if ($slug)
		{	$suffix = '';
			while ($this->SlugExists($slug . $suffix))
			{	$suffix++;
			}
			if ($suffix)
			{	$slug .= '_' . $suffix;
			}
			
			$fields['slug'] = 'slug="' . $slug . '"';
		} else
		{	if ($this->id || $catname)
			{	$fail[] = 'slug missing';
			}
		}
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE postcategories SET ' . $set . ' WHERE pcatid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO postcategories SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new category added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			} else echo $sql, ':', $this->db->Error();
		}
		
		if ($this->id)
		{	if($imagefile['size'])
			{	if ($this->ValidPhotoUpload($imagefile))
				{	$isize = getimagesize($imagefile['tmp_name']);
					if ($this->ReSizePhotoPNG($imagefile['tmp_name'], $this->GetImageFile(), $isize[0], $isize[1], stristr($imagefile['type'], 'png') ? 'png' : 'jpg'))
					{	$success[] = 'image uploaded';
					}
					unset($imagefile['tmp_name']);
				} else
				{	$fail[] = 'image upload failed';
				}
			} else
			{	if ($data['deleteimage'])
				{	if (@unlink($this->GetImageFile()))
					{	$success[] = 'image deleted';
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	public function PossibleTemplates()
	{	$templates = array();
		$sql = 'SELECT * FROM postcattemplates ORDER BY listorder';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$templates[$row['ptname']] = $row['ptdesc'];
			}
		}
		return $templates;
	} // end of fn PossibleTemplates
	
	public function InputForm()
	{	ob_start();
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		$form->AddTextInput('Category name', 'catname', $this->InputSafeString($data['catname']), 'long');
		if ($this->id)
		{	$form->AddTextInput('Slug', 'slug', $this->InputSafeString($data['slug']), '', 255, 1);
			$form->AddTextInput('Page title', 'pagetitle', $this->InputSafeString($data['pagetitle']), 'long');
		}
		$form->AddSelect('Template', 'template', $this->details['template'], '', $this->PossibleTemplates(), 0, 0);
		$form->AddTextArea('Page header text (if any)', 'pagecontent', stripslashes($data['pagecontent']), 'tinymce', 0, 0, 10, 60);
		$form->AddTextArea('Meta description', 'metadesc', $this->InputSafeString($data['metadesc']), '', 0, 0, 10, 60);
		if ($this->CanAdminUser('technical'))
		{	$form->AddTextInput('Extra post sections (comma separated)', 'postsections', $this->InputSafeString($data['postsections']), 'long');
		}
		$form->AddFileUpload('Upload section image (if any)', 'imagefile');
		if ($this->HasImage())
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $this->GetImageSRC() . '" width="300" /><br /></p>');
			$form->AddCheckBox('Delete this image', 'deleteimage', 1, false);
		}
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Category', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this category</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm

	public function GetQuickDonateOptions($live_only = false)
	{	return parent::GetQuickDonateOptions($live_only);
	} // end of fn GetQuickDonateOptions
	
	public function AdminQuickDonateForm()
	{	ob_start();
		$data = $this->GetQuickDonateOptions();
		$donation = new Donation();
		
		$countries = $donation->GetDonationCountries('oneoff', true);
		$types = array('oneoff'=>'one-off', 'monthly'=>'monthly (DD)');
		echo '<form method="post" action="postcatdonateform.php?id=', (int)$this->id, '">
				<label>Show quick donate on post category page</label><input type="checkbox" name="live"', $data['live'] ? ' checked="checked"' : '', ' /><br />
				<label>Type</label><select id="dlb_type" name="type" onchange="DLBTypeChange(false);">';
		$def_type = $data['type'];
		foreach ($types as $key=>$text)
		{	if (!$def_type)
			{	$def_type = $key;
			}
			echo '<option value="', $key, '"', $data['type'] == $key ? ' selected="selected"' : '', '>', $text, '</option>';
		}
		echo '</select><br />
				<label>Cause</label><select id="dlb_country" name="country" onchange="DLBChangeCountry(false);">';
		$def_country = $data['country'];
		foreach ($countries as $country)
		{	if (!$def_country)
			{	$def_country = $country['dcid'];
			}
			if ($country['dcid'] == $def_country)
			{	$projects = $country['projects'];
			}
			echo '<option value="', $country['dcid'], '"', $country['dcid'] == $def_country ? ' selected="selected"' : '', '>', $this->InputSafeString($country['shortname']), '</option>';
		}
		echo '</select><br />
				<label>Area</label><select id="dlb_project" name="project">';
		
		$def_project = $data['project'];
		foreach ($projects as $project)
		{	if (!$def_project)
			{	$def_project = $project['dpid'];
			}
			echo '<option value="', $project['dpid'], '"', $project['dpid'] == $def_project ? ' selected="selected"' : '', '>', $this->InputSafeString($project['projectname']), '</option>';
		}
		echo '</select><br />
				<label>Currency</label><select id="dlb_currency" name="currency">';
		$def_currency = $data['currency'];
		foreach ($this->GetCurrencies() as $curcode=>$currency)
		{	if (!$def_currency)
			{	$def_currency = $curcode;
			}
			echo '<option value="', $curcode, '"', $def_currency == $curcode ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
		}
		echo '</select><br />
				<label>Amount (GBP)</label><input type="text" name="amount" id="dlb_amount" class="number" value="', (int)$data['amount'], '" /><br />
				<label>Zakat</label><input type="checkbox" name="zakat" id="dlb_zakat"', $data['zakat'] ? ' checked="checked"' : '', ' /><br />
				<label>Quick donate</label><input type="checkbox" id="dlb_quick" name="quick"', $data['quick'] ? ' checked="checked"' : '', ' /><br />
				<label>&nbsp;</label><input type="submit" class="submit" value="Save Quick Donate Options" /><br />
			</form></div>';
//		$this->VarDump($countries);
		return ob_get_clean();
	} // end of fn AdminQuickDonateForm
	
	function AdminQuickDonateSave($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = $this->GetQuickDonateOptions();
		
		$fields['live'] = (bool)$data['live'];
		$fields['zakat'] = (bool)$data['zakat'];
		$fields['quick'] = (bool)$data['quick'];
		$fields['amount'] = (int)$data['amount'];
		
		$fields['country'] = $data['country'];
		$fields['project'] = $data['project'];
		$fields['currency'] = $data['currency'];
		$fields['type'] = $data['type'];
	
		$quickdonate = json_encode($fields);
		$sql = 'UPDATE postcategories SET quickdonate="' . $this->SQLSafe($quickdonate) . '" WHERE pcatid=' . (int)$this->id;
		if ($result = $this->db->Query($sql))
		{	if ($this->db->AffectedRows())
			{	$success[] = 'Changes saved';
				$this->Get($this->id);
			} else
			{	$fail[] = $sql;
			}
		} else $fail[] = $this->db->Error();
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn AdminQuickDonateSave

	private function GetCurrencies($type = 'oneoff')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies
	
} // end of defn AdminPostCategory
?>