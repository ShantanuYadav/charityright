<?php
class AdminOrdersPage extends AccountsMenuPage
{	protected $order;
	protected $menuarea;

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->AdminOrdersLoggedInConstruct();
	} // end of fn AccountsLoggedInConstruct
	
	protected function AdminOrdersLoggedInConstruct()
	{	$this->css['admindonations.css'] = 'admindonations.css';
		$this->AssignOrder();
		$this->breadcrumbs->AddCrumb('cartorders.php', 'Cart orders');
		$this->OrdersConstructFunctions();
		if ($this->order->id)
		{	$this->breadcrumbs->AddCrumb('cartorder.php?id=' . $this->order->id, $this->order->AdminTitle());
		}
	} // end of fn AdminOrdersLoggedInConstruct
	
	protected function OrdersConstructFunctions()
	{	
	} // end of fn OrdersConstructFunctions
	
	protected function AssignOrder()
	{	$this->order = new AdminCartOrder($_GET['id']);
	} // end of fn AssignOrder
	
	protected function AdminOrdersBody()
	{	if ($this->order->id)
		{	if ($menu = $this->GetOrdersMenu())
			{	echo '<div class="adminItemSubMenu"><ul>';
				foreach ($menu as $menuarea=>$menuitem)
				{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
				}
				echo '</ul><div class="clear"></div></div>';
			}
		}
	} // end of fn AdminOrdersBody
	
	protected function GetOrdersMenu()
	{	$menu = array();
		$menu['view'] = array('text'=>'View', 'link'=>'cartorder.php?id=' . $this->order->id);
	//	if ($this->order->payments || $this->donation->CanBeCollectedMonthly())
	//	{	$menu['payments'] = array('text'=>'Payments', 'link'=>'donationpayments.php?id=' . $this->donation->id);
	//	}
		if ($this->order->IsMonthlyUnconfirmed())
		{	$menu['ddconfirm'] = array('text'=>'Confirm Direct Debit', 'link'=>'cartorder_ddconfirm.php?id=' . $this->order->id);
		}
		return $menu;
	} // end of fn GetOrdersMenu
	
	function AdminBodyMain()
	{	$this->AdminOrdersBody();
	} // end of fn AdminBodyMain
	
} // end of defn AdminOrdersPage
?>