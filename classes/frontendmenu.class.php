<?php
class FrontEndMenu extends BlankItem
{	protected $items = array();
	
	public function __construct($id = 0)
	{	parent::__construct($id, 'femenus', 'menuid');
	} // fn __construct
	
	public function ResetExtra()
	{	$this->items = array();
	} // end of fn ResetExtra
	
	public function GetExtra()
	{	$this->InitialiseMenuItems();
	} // end of fn GetExtra
	
	protected function InitialiseMenuItems()
	{	$this->items = $this->db->ResultsArrayFromSQL('SELECT * FROM femenuitems WHERE menuid=' . (int)$this->id . ' ORDER BY menuorder, miid', 'miid');
	} // end of fn InitialiseMenuItems
	
	public function GetMenuByName($name = '')
	{	foreach ($this->GetMenuAssignments() as $position)
		{	if ($name == $position['maname'])
			{	$sql = 'SELECT * FROM femenus WHERE menuassigned=' . $position['maid'];
				if ($result = $this->db->Query($sql))
				{	if ($row = $this->db->FetchArray($result))
					{	$this->Get($row);
						return $this->id;
					}
				}
				break;
			}
		}
	} // end of fn GetMenuByName
	
	public function GetAllMenuItems()
	{	return $this->items;
	} // end of fn GetAllMenuItems
	
	public function MenuItems()
	{	$items = array();
		$items_available = $this->items;
		while ($items_available)
		{	foreach ($items_available as $itemkey=>$item_available)
			{	$this->MenuItemsSetItem($items, $items_available, $item_available);
			}
			if ($count++ > 200)
			{	break;
			}
		}
		return $items;
	} // end of fn MenuItems
	
	Private function MenuItemsSetItem(&$items, &$items_available, $item_available)
	{	if ($item_available['menuparent'])
		{	// check any existing and add if there
			foreach ($items as $itemkey=>$item)
			{	if ($this->MenuItemsSetItemAsChild($items[$itemkey], $items_available, $item_available))
				{	break;
				}
			}
		} else
		{	$items[$item_available['miid']] = array('item'=>$item_available, 'submenu'=>array());
			unset ($items_available[$item_available['miid']]);
			return;
		}
	} // end of fn MenuItemsSetItem
	
	public function MenuItemsSetItemAsChild(&$item, &$items_available, $item_available)
	{	if ($item_available['menuparent'] == $item['item']['miid'])
		{	$item['submenu'][$item_available['miid']] = array('item'=>$item_available, 'submenu'=>array());
			unset ($items_available[$item_available['miid']]);
			return true;
		} else
		{	if ($item['submenu'])
			{	foreach ($item['submenu'] as $itemkey=>$subitem)
				{	if ($this->MenuItemsSetItemAsChild($item['submenu'][$itemkey], $items_available, $item_available))
					{	return true;
					}
				}
			}
		}
	} // end of fn MenuItemsSetItemAsChild
	
	protected function GetMenuAssignments()
	{	$options = array();
		$options[1] = array('maid'=>1, 'maname'=>'headermenu', 'malabel'=>'Main header menu');
		$options[2] = array('maid'=>2, 'maname'=>'headermenumobile', 'malabel'=>'Mobile header menu');
		$options[3] = array('maid'=>3, 'maname'=>'footermenu', 'malabel'=>'Main footer menu');
		$options[4] = array('maid'=>4, 'maname'=>'mobilefootermenu', 'malabel'=>'Mobile footer menu');
		$options[5] = array('maid'=>5, 'maname'=>'qurbani2019header', 'malabel'=>'Qurbani 2019 landing page header menu');
		return $options;
	} // end of fn GetMenuAssignments
	
	public function AssignedMenu($field = '')
	{	if ($this->details['menuassigned'])
		{	$options = $this->GetMenuAssignments();
			if ($options[$this->details['menuassigned']])
			{	if ($field && isset($options[$this->details['menuassigned']][$field]))
				{	return $options[$this->details['menuassigned']][$field];
				} else
				{	return $options[$this->details['menuassigned']];
				}
			}
		}
	} // end of fn AssignedMenu
	
} // end of defn FrontEndMenu
?>