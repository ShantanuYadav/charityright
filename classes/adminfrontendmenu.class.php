<?php
class AdminFrontEndMenu extends FrontEndMenu
{	public $admintitle = '';

	function __construct($id = 0)
	{	parent::__construct($id);
		$this->GetAdminTitle();
	} //  end of fn __construct
	
	function GetAdminTitle($title = '')
	{	$this->admintitle = $this->details['menuname'];
	} // end of fn GetAdminTitle
	
	function Save($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($menuname = $this->SQLSafe($data['menuname']))
		{	$fields[] = 'menuname="' . $menuname . '"';
		} else
		{	$fail[] = 'menu name cannot be empty';
		}
		
		if ($menuassigned = (int)$data['menuassigned'])
		{	$options = $this->PossibleMenuAssignments();
			if ($options[$menuassigned])
			{	$fields[] = 'menuassigned=' . $menuassigned;
			} else
			{	$fail[] = 'assignment invalid';
			}
		} else
		{	$fields[] = 'menuassigned=0';
		}
	
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE femenus SET ' . $set . ' WHERE menuid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO femenus SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$record_changes = true;
						$this->Get($this->id);
						$success[] = 'Changes saved';
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new menu added';
							$this->GetAdminTitle();
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
				
				if ($record_changes)
				{	$base_parameters = array('tablename'=>'femenus', 'tableid'=>$this->id, 'area'=>'femenus');
				}
				
				// unassign any other assignments to same menu
				if ($this->details['menuassigned'])
				{	$sql = 'UPDATE femenus SET menuassigned=0 WHERE menuassigned=' . $this->details['menuassigned'] . ' AND NOT menuid=' . $this->id;
					$this->db->Query($sql);
				}
			} else echo '<p>', $sql, ': ', $this->db->Error(), '</p>';
			
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	function CanDelete()
	{	return $this->id && !$this->items;
	} // end of fn CanDelete
	
	function InputForm()
	{	 
		if (!$data = $this->details)
		{	$data = $_POST;
		}

		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id, 'newsform');
		$form->AddTextInput('Menu name', 'menuname', $this->InputSafeString($data['menuname']));
		$form->AddSelect('Assigned to', 'menuassigned', $data['menuassigned'], '', $this->PossibleMenuAssignments(), 1, 0, '', '-- not assigned --');
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Menu', 'submit');
		if ($this->id)
		{	echo '<h3>Editing ... ', $this->InputSafeString($this->admintitle), '</h3>';
			if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this menu</a></p>';
			}
		}
		$form->Output();
	} // end of fn InputForm
	
	protected function PossibleMenuAssignments()
	{	$options = array();
		foreach ($this->GetMenuAssignments() as $option)
		{	$options[$option['maid']] = $this->InputSafeString($option['malabel']);
		}
		return $options;
	} // end of fn PossibleMenuAssignments
	
	public function MenuItemsContainer()
	{	ob_start();
		echo '<div class="bandisplay"><div id="banContainer">', $this->MenuItemsTable(), '</div><script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$("#rlp_modal_popup").jqm();});</script>',
	'<!-- START banners list modal popup --><div id="rlp_modal_popup" class="jqmWindow" style="padding-bottom: 5px; width: 640px; margin-left: -320px; top: 10px; height: 600px; "><a href="#" class="jqmClose">Close</a><div id="rlpModalInner" style="height: 500px; overflow:auto;"></div></div></div>';
		return ob_get_clean();
	} // end of fn MenuItemsContainer
	
	public function MenuItemsTable()
	{	ob_start();
		echo '<form target="" method="post"><table><tr class="newlink"><th colspan="7"><a onclick="PagesPopUp(', $this->id, ');">Add menu item</a></th></tr><tr><th>Parent</th><th>Page</th><th>Override title</th><th>CSS Class</th><th>Menu order</th><th>Open in new tab</th><th>Actions</th></tr>';
		if ($items = $this->MenuItems())
		{	foreach ($items as $item)
			{	echo $this->MenuItemsTableRow($item);
			}
			echo '<tr><th colspan="7"><input type="submit" class="submit" value="Save Details" /></th></tr>';
		}
		echo '</table></form>';
		return ob_get_clean();
	} // end of fn MenuItemsTable
	
	private function PossibleItemParents($excludeid = 0)
	{	$parents = array();
		if ($items = $this->MenuItems())
		{	foreach ($items as $item)
			{	$this->PossibleItemParentsAddItem($parents, $item, '', $excludeid);
			}
		}
		return $parents;
	} // end of fn PossibleItemParents
	
	private function PossibleItemParentsAddItem(&$parents, $item = array(), $prefix = '', $excludeid = 0)
	{	if ($item['item']['miid'] != $excludeid)
		{	$page = new PageContent($item['item']['pageid']);
			$title = $prefix . $this->InputSafeString($item['item']['itemtitle'] ? $item['item']['itemtitle'] : $page->details['pagetitle']);
			$parents[$item['item']['miid']] = $title;
			if ($item['submenu'])
			{	foreach ($item['submenu']as $subitem)
				{	$this->PossibleItemParentsAddItem($parents, $subitem, $title . '&nbsp;&raquo;&nbsp;', $excludeid);
				}
			}
		}
	} // end of fn PossibleItemParentsAddItem
	
	public function MenuItemsTableRow($menuitem = array())
	{	ob_start();
		$page = new PageContent($menuitem['item']['pageid']);
		echo '<tr><td><select name="menuparent[', $menuitem['item']['miid'], ']"><option value="0">-- top level --</option>';
		foreach ($this->PossibleItemParents($menuitem['item']['miid']) as $parentid=>$parenttext)
		{	echo '<option value="', $parentid, '"', $menuitem['item']['menuparent'] == $parentid ? ' selected="selected"' : '', '>', $parenttext, '</option>';
		}
		echo '</select></td><td>', $this->InputSafeString($page->details['pagetitle']), $page->details['headeronly'] ? '<br />**header only**' : '', '</td><td><input type="text" style="width:240px;" name="itemtitle[', $menuitem['item']['miid'], ']" value="', str_replace('"', '&quot;', $menuitem['item']['itemtitle']), '" /></td><td><input type="text" style="width:140px;" name="menuclass[', $menuitem['item']['miid'], ']" value="', str_replace('"', '&quot;', $menuitem['item']['menuclass']), '" /></td><td><input type="text" class="number" name="menuorder[', $menuitem['item']['miid'], ']" value="', (int)$menuitem['item']['menuorder'], '" /></td><td><input type="checkbox" name="openblank[', $menuitem['item']['miid'], ']" value="1" ', $menuitem['item']['openblank'] ? 'checked="checked" ' : '', '/></td><td>';
		if (!$menuitem['submenu'])
		{	echo '<a onclick="PageRemove(', $this->id, ',', $menuitem['item']['miid'], ')">Remove from menu</a>';
		}
		echo '</td></tr>';
		if ($menuitem['submenu'])
		{	foreach ($menuitem['submenu']as $subitem)
			{	echo $this->MenuItemsTableRow($subitem);
			}
		}
		return ob_get_clean();
	} // end of fn MenuItemsTableRow
	
	public function RemoveMenuItem($miid = 0)
	{	if ($miid = (int)$miid)
		{	$sql = 'DELETE FROM femenuitems WHERE miid=' . (int)$miid . ' AND menuid=' . $this->id;
			$this->db->Query($sql);
			$this->InitialiseMenuItems();
		}
	} // end of fn RemoveSlide
	
	public function AddPageAsItem($pageid = 0)
	{	if ($pageid = (int)$pageid)
		{	echo $sql = 'INSERT INTO femenuitems SET menuid=' . $this->id . ', pageid=' . $pageid;
			$this->db->Query($sql);
		}
	} // end of fn AddSlide
	
	public function SaveMenuItems($data = array())
	{	$savecount = 0;
		if (is_array($data['menuorder']) && $data['menuorder'])
		{	foreach ($data['menuorder'] as $miid=>$menuorder)
			{	$fields = array();
				$fields[] = 'menuorder=' . (int)$menuorder;
				$fields[] = 'menuparent=' . (int)$data['menuparent'][$miid];
				$fields[] = 'openblank=' . ($data['openblank'][$miid] ? '1' : '0');
				$fields[] = 'itemtitle="' . $this->SQLSafe($data['itemtitle'][$miid]) . '"';
				$fields[] = 'menuclass="' . $this->SQLSafe($data['menuclass'][$miid]) . '"';
				$sql = 'UPDATE femenuitems SET ' . implode(',', $fields) . ' WHERE miid=' . (int)$miid;
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows())
					{	$savecount++;
					}
				} else echo $this->db->Error();
			}
		}
		if ($savecount)
		{	$this->InitialiseMenuItems();
		}
		return $savecount;
	} // end of fn SaveMenuItems
	
} // end of defn AdminFrontEndMenu
?>