<?php
class Cart extends Base
{	
	
	public function AddMainDonationToCart($data = array())
	{	$fail = array();
		$fields = array();
		$success = false;
		
		$donation = new Donation();
		
		if ($donation->GetDonationType($data['donation_type']))
		{	$fields['donation_type'] = $data['donation_type'];
			$currencies = $this->GetCurrencies($data['donation_type']);
			if ($_SESSION[$this->cart_session_name]['currency'] && $currencies[$_SESSION[$this->cart_session_name]['currency']])
			{	$fields['donation_currency'] = $_SESSION[$this->cart_session_name]['currency'];
			} else
			{	if ($currencies[$data['donation_currency']])
				{	$fields['donation_currency'] = $data['donation_currency'];
				} else
				{	$fail[] = 'Donation currency missing';
				}
			}
			
			if (($donation_amount = round($data['donation_amount'], 2)) > 0)
			{	$fields['donation_amount'] = $donation_amount;
			} else
			{	$fail[] = 'Donation amount missing';
			}
			$fields['donation_admin'] = round($data['donation_admin'], 2);

			$countries = $donation->GetDonationCountries($data['donation_type']);
			if ($countries[$data['donation_country']])
			{	$fields['donation_country'] = $data['donation_country'];
				if ($countries[$data['donation_country']]['projects'])
				{	if ($countries[$data['donation_country']]['projects'][$data['donation_project']])
					{	$fields['donation_project'] = $data['donation_project'];
						if ($countries[$data['donation_country']]['projects'][$data['donation_project']]['projects'])
						{	if ($countries[$data['donation_country']]['projects'][$data['donation_project']]['projects'][$data['donation_project2']])
							{	$fields['donation_project2'] = $data['donation_project2'];
							} else
							{	$fail[] = 'Project incomplete';
							}
						} else
						{	$fields['donation_project2'] = '';
						}
					} else
					{	$fail[] = 'Option 2 missing ' . $data['donation_project'];
					}
				} else
				{	$fields['donation_project'] = '';
				}
			} else
			{	$fail[] = 'Option 1 missing';
			}

			$fields['donation_zakat'] = ($data['donation_zakat'] ? 1 : 0);
			$fields['donation_quantity'] = (int)$data['donation_quantity'];
		} else
		{	$fail[] = 'Donation type missing';
		}
		
		if (!$fail && $fields)
		{	if (!$_SESSION[$this->cart_session_name] || !is_array($_SESSION[$this->cart_session_name]))
			{	$_SESSION[$this->cart_session_name] = array();
			}
			if (!$_SESSION[$this->cart_session_name]['donations'] || !is_array($_SESSION[$this->cart_session_name]['donations']))
			{	$_SESSION[$this->cart_session_name]['donations'] = array();
			}
			
			
			if (!$this->AddMainDonationToExistingItem($fields))
			{	$_SESSION[$this->cart_session_name]['donations'][] = $fields;
				if (!$_SESSION[$this->cart_session_name]['currency'])
				{	$_SESSION[$this->cart_session_name]['currency'] = $data['donation_currency'];
				}
			}
			$success = true;
		}
		
		return array('fail'=>$fail, 'success'=>$success);
	} // fn AddMainDonationToCart

	public function AddMainDonationToCartNew($data = array())
	{	$fail = array();
		$fields = array();
		$success = false;
		
		if (isset($_POST['cid']) && $_POST['cid'] != '') {
			if (($campaign = new Campaign($_POST['cid'])) && $campaign->id)
			{	$fields['cid'] = $campaign->id;
				$currencies = $this->GetCurrencies('oneoff');
				
				if ($_SESSION[$this->cart_session_name]['currency'] && $currencies[$_SESSION[$this->cart_session_name]['currency']])
				{	$fields['donation_currency'] = $_SESSION[$this->cart_session_name]['currency'];
				} else
				{	if ($currencies[$data['donation_currency']])
					{	$fields['donation_currency'] = $data['donation_currency'];
					} else
					{	$fail[] = 'Donation currency missing';
					}
				}
				
				if (($donation_amount = round($data['donation_amount'], 2)) > 0)
				{	$fields['donation_amount'] = $donation_amount;
				} else
				{	$fail[] = 'Donation amount missing';
				}

				$fields['donation_zakat'] = ($data['donation_zakat'] ? 1 : 0);
				$fields['donor_anon'] = ($data['donor_anon'] ? 1 : 0);
				$fields['donor_showto'] = ($data['donor_showto'] ? 1 : 0);
				// $fields['donor_comment'] = $data['donor_comment'];
				
				if (!$fail && $fields)
				{	if (!$_SESSION[$this->cart_session_name] || !is_array($_SESSION[$this->cart_session_name]))
					{	$_SESSION[$this->cart_session_name] = array();
					}
					if (!$_SESSION[$this->cart_session_name]['crsdonations'] || !is_array($_SESSION[$this->cart_session_name]['crsdonations']))
					{	$_SESSION[$this->cart_session_name]['crsdonations'] = array();
					}
					$_SESSION[$this->cart_session_name] = array();
					$_SESSION[$this->cart_session_name]['crsdonations'][] = $fields;
					if (!$_SESSION[$this->cart_session_name]['currency'])
					{	$_SESSION[$this->cart_session_name]['currency'] = $data['donation_currency'];
					}
					$_SESSION[$this->cart_session_name]['stripe_desc'] = $campaign->details['campname'];
					// if (!$this->AddCRDonationToExistingItem($fields))
					// {	$_SESSION[$this->cart_session_name] = array();
					// 	$_SESSION[$this->cart_session_name]['crsdonations'][] = $fields;
					// 	if (!$_SESSION[$this->cart_session_name]['currency'])
					// 	{	$_SESSION[$this->cart_session_name]['currency'] = $data['donation_currency'];
					// 	}
					// }
					$success = true;
				}
			} else
			{	$fail[] = 'campaign not found';
			}
		}else{
			$donation = new Donation();
			$donation_country_arr = array("schools"=>'School meals',
								"food_packs"=>'Family food packs',
								"girls"=>'Educate a girl',
								"hifzmeals"=>'Hifz',
								"most_needed"=>'Help where it’s most needed',);
			if ($donation->GetDonationType($data['donation_type']))
			{	$fields['donation_type'] = $data['donation_type'];
				$currencies = $this->GetCurrencies($data['donation_type']);
				if ($_SESSION[$this->cart_session_name]['currency'] && $currencies[$_SESSION[$this->cart_session_name]['currency']])
				{	$fields['donation_currency'] = $_SESSION[$this->cart_session_name]['currency'];
				} else
				{	if ($currencies[$data['donation_currency']])
					{	$fields['donation_currency'] = $data['donation_currency'];
					} else
					{	$fail[] = 'Donation currency missing';
					}
				}
				
				if (($donation_amount = round($data['donation_amount'], 2)) > 0)
				{	$fields['donation_amount'] = $donation_amount;
				} else
				{	$fail[] = 'Donation amount missing';
				}
				$fields['donation_admin'] = round($data['donation_admin'], 2);

				$countries = $donation->GetDonationCountries($data['donation_type']);
				// if(SITE_TEST){
				// 	print_r($data['donation_country']);
				// 	print_r($countries);
				// }
				if ($countries[$data['donation_country']])
				{	$fields['donation_country'] = $data['donation_country'];
					$fields['donation_project2'] = '';
					$fields['donation_project'] = '';
				} else
				{	$fail[] = 'Please select an area to donate.';
				}

				$fields['donation_zakat'] = ($data['donation_zakat'] ? 1 : 0);
				$fields['donation_quantity'] = (int)$data['donation_quantity'];
			} else
			{	$fail[] = 'Donation type missing';
			}
			
			if (!$fail && $fields)
			{	if (!$_SESSION[$this->cart_session_name] || !is_array($_SESSION[$this->cart_session_name]))
				{	$_SESSION[$this->cart_session_name] = array();
				}
				if (!$_SESSION[$this->cart_session_name]['donations'] || !is_array($_SESSION[$this->cart_session_name]['donations']))
				{	$_SESSION[$this->cart_session_name]['donations'] = array();
				}
				
				$_SESSION[$this->cart_session_name] = array();
				$_SESSION[$this->cart_session_name]['donations'][] = $fields;
				if (!$_SESSION[$this->cart_session_name]['currency'])
				{	$_SESSION[$this->cart_session_name]['currency'] = $data['donation_currency'];
				}
				$_SESSION[$this->cart_session_name]['stripe_desc'] = $donation_country_arr[$data['donation_country']];
				// if (!$this->AddMainDonationToExistingItem($fields))
				// {	$_SESSION[$this->cart_session_name] = array();
				// 	$_SESSION[$this->cart_session_name]['donations'][] = $fields;
				// 	if (!$_SESSION[$this->cart_session_name]['currency'])
				// 	{	$_SESSION[$this->cart_session_name]['currency'] = $data['donation_currency'];
				// 	}
				// }
				$success = true;
			}
		}
		
		return array('fail'=>$fail, 'success'=>$success);
	} // fn AddMainDonationToCartNew

	public function AddMainDonationToCartNewStep2($data = array())
	{	$fail = array();
		$fields = array();
		$success = false;
		
		$donation = new Donation();
		
		if ($donation->GetDonationType($data['donation_type']))
		{	$donation_type = $data['donation_type'];
			if ($data['donor_title'] == '') {
				$fail[] = 'your title is missing';
			}
			if ($data['donor_firstname'] == '') {
				$fail[] = 'first name missing';
			}
			if ($data['donor_lastname'] == '') {
				$fail[] = 'last name missing';
			}
			if ($data['donor_email'] == '') {
				$fail[] = 'email missing';
			}else{
				if (!filter_var($data['donor_email'], FILTER_VALIDATE_EMAIL)) {
					$fail[] = 'invalid email format';
				}
			}
			if ($donation_type == 'oneoff') {
				if ($data['cardNumber'] == '') {
					$fail[] = 'card number missing';
				}
				if ($data['expiry_month'] == '') {
					$fail[] = 'card expiry month missing';
				}
				if ($data['expiry_year'] == '') {
					$fail[] = 'card expiry year missing';
				}
				if ($data['cvv'] == '') {
					$fail[] = 'card cvc missing';
				}
			}else if ($donation_type == 'monthly') {
				if (!$data['dd_accountnumber']) {
					$fail[] = 'Account number missing';
				}else{
					$data['dd_accountname'] = $data['donor_firstname'].' '.$data['donor_lastname'];
					if (!$this->ValidBankAccountNumber($data['dd_accountnumber'])){
						$fail[] = 'Invalid account number (must be 8 numbers)';
					}
				}

				if ($data['dd_accountsortcode']){
					if (!$this->ValidSortCode($data['dd_accountsortcode'])){
						$fail[] = 'Sort code invalid';
					}
				}else {	
					$fail[] = 'Sort code missing';
				}
			}
			
		} else
		{	$fail[] = 'Donation type missing';
		}
		
		if (!$fail)
		{	if (!$_SESSION[$this->cart_session_name] || !is_array($_SESSION[$this->cart_session_name]))
			{	$_SESSION[$this->cart_session_name] = array();
			}
			if (!$_SESSION[$this->cart_session_name]['donations'] || !is_array($_SESSION[$this->cart_session_name]['donations']))
			{	$_SESSION[$this->cart_session_name]['donations'] = array();
			}
			
			$_SESSION[$this->cart_session_name]['donor_title'] = $data['donor_title'];
			$_SESSION[$this->cart_session_name]['donor_firstname'] = $data['donor_firstname'];
			$_SESSION[$this->cart_session_name]['donor_lastname'] = $data['donor_lastname'];
			$_SESSION[$this->cart_session_name]['donor_email'] = $data['donor_email'];
			if ($donation_type == 'oneoff') {
				$_SESSION[$this->cart_session_name]['cardNumber'] = $data['cardNumber'];
				$_SESSION[$this->cart_session_name]['expiry_month'] = $data['expiry_month'];
				$_SESSION[$this->cart_session_name]['expiry_year'] = $data['expiry_year'];
				$_SESSION[$this->cart_session_name]['cvv'] = $data['cvv'];
			}else if ($donation_type == 'monthly') {
				$_SESSION[$this->cart_session_name]['dd_accountname'] = $data['dd_accountname'];
				$_SESSION[$this->cart_session_name]['dd_accountnumber'] = $data['dd_accountnumber'];
				$_SESSION[$this->cart_session_name]['dd_accountsortcode'] = $data['dd_accountsortcode'];
			}
			$success = true;
		}
		
		return array('fail'=>$fail, 'success'=>$success);
	} // fn AddMainDonationToCartNewStep2

	public function AddMainDonationToCartNewStep3($data = array())
	{	$fail = array();
		$fields = array();
		$success = false;
		
		$donation = new Donation();
		
		if ($donation->GetDonationType($data['donation_type']))
		{	$donation_type = $data['donation_type'];
			if ($data['billing_address'] == '') {
				$fail[] = 'your billing address is missing';
			}
			if ($data['billing_post_code'] == '') {
				$fail[] = 'your billing post code is missing';
			}
			if ($data['donor_country'] == '') {
				$fail[] = 'donor country missing';
			}
			$donor_giftaid = $data['donor_giftaid'];
			if ($donor_giftaid == 1) {
				if ($data['home_address'] == '') {
					$fail[] = 'home address missing';
				}
				if ($data['home_post_code'] == '') {
					$fail[] = 'home post code missing';
				}
			}
			
		} else
		{	$fail[] = 'Donation type missing';
		}
		if ($donation_type == 'monthly') {
			$pca = new PCAPredictAPI();
			$cart = $this->GetCart();
			$pca_result = $pca->CheckBankAccountNew($cart);
			
			
			if(SITE_TEST){
			  $pca_result['successmessage'] = 'Testing fake success!';
			}
			if ($pca_result['successmessage'])
			{	$this->SetProperty('dd_checked', true);
			} else
			{	$fail[] = $pca_result['failmessage'];
				$this->SetProperty('dd_checked', false);
			}
		}
		
		if (!$fail)
		{	if (!$_SESSION[$this->cart_session_name] || !is_array($_SESSION[$this->cart_session_name]))
			{	$_SESSION[$this->cart_session_name] = array();
			}
			if (!$_SESSION[$this->cart_session_name]['donations'] || !is_array($_SESSION[$this->cart_session_name]['donations']))
			{	$_SESSION[$this->cart_session_name]['donations'] = array();
			}
			
			$_SESSION[$this->cart_session_name]['billing_address'] = $data['billing_address'];
			$_SESSION[$this->cart_session_name]['billing_post_code'] = $data['billing_post_code'];
			$_SESSION[$this->cart_session_name]['donor_country'] = $data['donor_country'];
			$_SESSION[$this->cart_session_name]['home_address'] = $data['home_address'];
			$_SESSION[$this->cart_session_name]['home_post_code'] = $data['home_post_code'];
			$_SESSION[$this->cart_session_name]['donor_giftaid'] = $data['donor_giftaid'];
			
			@$_SESSION[$this->cart_session_name]['donor_anon'] = $data['donor_anon'];
			@$_SESSION[$this->cart_session_name]['donor_comment'] = $data['donor_comment'];
			
			$success = true;
		}
		
		return array('fail'=>$fail, 'success'=>$success);
	} // fn AddMainDonationToCartNewStep3
	
	public function AddMainDonationToExistingItem($new = array())
	{	if ($new && is_array($new))
		{	if ($_SESSION[$this->cart_session_name]['donations'] && is_array($_SESSION[$this->cart_session_name]['donations']))
			{	foreach ($_SESSION[$this->cart_session_name]['donations'] as $key=>$existing)
				{	if (($existing['donation_type'] == $new['donation_type']) && ($existing['donation_country'] == $new['donation_country']) && ($existing['donation_project'] == $new['donation_project']) && ($existing['donation_project2'] == $new['donation_project2']) && ($existing['donation_zakat'] == $new['donation_zakat']))
					{	$_SESSION[$this->cart_session_name]['donations'][$key]['donation_quantity'] += (int)$new['donation_quantity'];
						$_SESSION[$this->cart_session_name]['donations'][$key]['donation_amount'] += round($new['donation_amount'], 2);
						$_SESSION[$this->cart_session_name]['donations'][$key]['donation_admin'] += round($new['donation_admin'], 2);
						return true;
					}
				}
			}
		}
		return false;
	} // fn AddMainDonationToExistingItem
	
	public function RemoveMainDonationFromCart($id = 0)
	{	if ($_SESSION[$this->cart_session_name]['donations'][(int)$id])
		{	unset($_SESSION[$this->cart_session_name]['donations'][(int)$id]);
			if (!$_SESSION[$this->cart_session_name]['crsdonations'] && !$_SESSION[$this->cart_session_name]['donations'])
			{	unset($_SESSION[$this->cart_session_name]['currency']);
			}
			return true;
		}
	} // fn RemoveMainDonationFromCart
	
	public function RemoveCRDonationFromCart($id = 0)
	{	if ($_SESSION[$this->cart_session_name]['crsdonations'][(int)$id])
		{	unset($_SESSION[$this->cart_session_name]['crsdonations'][(int)$id]);
			if (!$_SESSION[$this->cart_session_name]['crsdonations'] && !$_SESSION[$this->cart_session_name]['donations'])
			{	unset($_SESSION[$this->cart_session_name]['currency']);
			}
			return true;
		}
	} // fn RemoveCRDonationFromCart
	
	public function AlterQuantityMainDonation($id = 0, $quantity = 0)
	{	if ($_SESSION[$this->cart_session_name]['donations'][(int)$id] && $_SESSION[$this->cart_session_name]['donations'][(int)$id]['donation_quantity'] && ($quantity = (int)$quantity))
		{	$unit_amount = $_SESSION[$this->cart_session_name]['donations'][(int)$id]['donation_amount'] / $_SESSION[$this->cart_session_name]['donations'][(int)$id]['donation_quantity'];
			$_SESSION[$this->cart_session_name]['donations'][(int)$id]['donation_quantity'] += $quantity;
			if ($_SESSION[$this->cart_session_name]['donations'][(int)$id]['donation_quantity'] > 0)
			{	$_SESSION[$this->cart_session_name]['donations'][(int)$id]['donation_amount'] = $_SESSION[$this->cart_session_name]['donations'][(int)$id]['donation_quantity'] * $unit_amount;
			} else
			{	$this->RemoveMainDonationFromCart($id);
			}
			return true;
		}
	} // fn AlterQuantityMainDonation
	
	public function AddCRDonationToExistingItem($new = array())
	{	if ($new && is_array($new))
		{	if ($_SESSION[$this->cart_session_name]['crsdonations'] && is_array($_SESSION[$this->cart_session_name]['crsdonations']))
			{	foreach ($_SESSION[$this->cart_session_name]['crsdonations'] as $key=>$existing)
				{	if (($existing['cid'] == $new['cid']) && ($existing['donation_zakat'] == $new['donation_zakat']) && ($existing['donor_anon'] == $new['donor_anon']) && ($existing['donor_showto'] == $new['donor_showto']) && !$existing['donor_comment'] && !$new['donor_comment'])
					{	$_SESSION[$this->cart_session_name]['crsdonations'][$key]['donation_amount'] += round($new['donation_amount'], 2);
						return true;
					}
				}
			}
		}
		return false;
	} // fn AddCRDonationToExistingItem
	
	public function AddCRDonationToCart($data = array())
	{	$fail = array();
		$fields = array();
		$success = false;
		
	
		if (($campaign = new Campaign($_GET['cid'])) && $campaign->id)
		{	$fields['cid'] = $campaign->id;
			$currencies = $this->GetCurrencies('oneoff');
			
			if ($_SESSION[$this->cart_session_name]['currency'] && $currencies[$_SESSION[$this->cart_session_name]['currency']])
			{	$fields['donation_currency'] = $_SESSION[$this->cart_session_name]['currency'];
			} else
			{	if ($currencies[$data['donation_currency']])
				{	$fields['donation_currency'] = $data['donation_currency'];
				} else
				{	$fail[] = 'Donation currency missing';
				}
			}
			
			if (($donation_amount = round($data['donation_amount'], 2)) > 0)
			{	$fields['donation_amount'] = $donation_amount;
			} else
			{	$fail[] = 'Donation amount missing';
			}

			$fields['donation_zakat'] = ($data['donation_zakat'] ? 1 : 0);
			$fields['donor_anon'] = ($data['donor_anon'] ? 1 : 0);
			$fields['donor_showto'] = ($data['donor_showto'] ? 1 : 0);
			$fields['donor_comment'] = $data['donor_comment'];
			
			if (!$fail && $fields)
			{	if (!$_SESSION[$this->cart_session_name] || !is_array($_SESSION[$this->cart_session_name]))
				{	$_SESSION[$this->cart_session_name] = array();
				}
				if (!$_SESSION[$this->cart_session_name]['crsdonations'] || !is_array($_SESSION[$this->cart_session_name]['crsdonations']))
				{	$_SESSION[$this->cart_session_name]['crsdonations'] = array();
				}
				if (!$this->AddCRDonationToExistingItem($fields))
				{	$_SESSION[$this->cart_session_name]['crsdonations'][] = $fields;
					if (!$_SESSION[$this->cart_session_name]['currency'])
					{	$_SESSION[$this->cart_session_name]['currency'] = $data['donation_currency'];
					}
				}
				$success = true;
			}
		} else
		{	$fail[] = 'campaign not found';
		}
		
		return array('fail'=>$fail, 'success'=>$success);
	} // fn AddCRDonationToCart
	
	public function GetCart()
	{	return $_SESSION[$this->cart_session_name];
	} // fn GetCart
	
	public function ReadyForCheckout()
	{	if ($cart = $this->GetCart())
		{	if ($cart['crsdonations'] || $cart['donations'])
			{	return true;
			}
		}
	} // fn ReadyForCheckout
	
	public function CheckoutCount()
	{	if ($cart = $this->GetCart())
		{	return count($cart['crsdonations']) && count($cart['donations']);
		}
		return 0;
	} // fn CheckoutCount
	
	public function OneoffCRStarsDonations()
	{	if (($cart = $this->GetCart()) && $cart['crsdonations'] && is_array($cart['crsdonations']))
		{	return $cart['crsdonations'];
		}
		return array();
	} // fn OneoffCRStarsDonations
	
	public function OneoffMainDonations()
	{	$donations = array();
		if (($cart = $this->GetCart()) && $cart['donations'] && is_array($cart['donations']))
		{	foreach ($cart['donations'] as $key=>$donation)
			{	if ($donation['donation_type'] == 'oneoff')
				{	$donations[$key] = $donation;
				}
			}
		}
		return $donations;
	} // fn OneoffMainDonations
	
	public function MonthlyMainDonations()
	{	$donations = array();
		if (($cart = $this->GetCart()) && $cart['donations'] && is_array($cart['donations']))
		{	foreach ($cart['donations'] as $key=>$donation)
			{	if ($donation['donation_type'] == 'monthly')
				{	$donations[$key] = $donation;
				}
			}
		}
		return $donations;
	} // fn MonthlyMainDonations
	
	public function RemoveMonthlyDonations()
	{	$donations = array();
		if ($donations = $this->MonthlyMainDonations())
		{	foreach ($donations as $key=>$donation)
			{	$this->RemoveMainDonationFromCart($key);
			}
		}
		return $donations;
	} // fn RemoveMonthlyDonations

	private function GetCurrencies($type = '')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies
	
	public function PopUpBasket()
	{	$return = array('desktop'=>'', 'mobile'=>'');
		if ($cart = $this->GetCart())
		{	if ($cart['donations'] && is_array($cart['donations']))
			{	$don_dummy = new Donation();
				$countries = $don_dummy->GetDonationCountries();
				$cursymbol = $this->GetCurrency($cart['currency'], 'cursymbol');
				foreach ($cart['donations'] as $key=>$donation)
				{	ob_start();
					if ($do_monthly = ($donation['donation_type'] == 'monthly'))
					{	$monthly_total += ($subamount = $donation['donation_amount'] + $donation['donation_admin']);
					} else
					{	$oneoff_total += ($subamount = $donation['donation_amount'] + $donation['donation_admin']);
					}
					echo '<li class="cartPopUpItemRow cartPopUp', $do_monthly ? 'Monthly' : 'Oneoff', '"><div class="cartPopUpItemImage"></div><div class="cartPopUpItemDesc">';
					if ($country = $countries[$donation['donation_country']])
					{	echo '<h4>', $this->InputSafeString($country['shortname']), '</h4><p>';
						if ($country['projects'] && ($project = $country['projects'][$donation['donation_project']]))
						{	echo $this->InputSafeString($project['projectname']);
							if ($project['projects'] && ($project2 = $project['projects'][$donation['donation_project2']]))
							{	echo ', ', $this->InputSafeString($project2['projectname']);
							}
						}
						//$this->VarDump($country);
					} else
					{	echo '<p>donation details not found';
					}
					if (($quantity = (int)$donation['donation_quantity']) > 1)
					{	echo ' &times; ', $quantity;
					}
					echo ' (', $donation['donation_zakat'] ? 'Zakat' : 'Sadaqah', ')</p></div><div class="cartPopUpItemAmount">', $cursymbol, number_format($subamount, 2), '</div><div></div></li>';
					if ($do_monthly)
					{	$monthly_lines[] = ob_get_clean();
					} else
					{	$oneoff_lines[] = ob_get_clean();
					}
				}
			}
			if ($cart['crsdonations'] && is_array($cart['crsdonations']))
			{	foreach ($cart['crsdonations'] as $key=>$donation)
				{	ob_start();
					$oneoff_total += ($subamount = $donation['donation_amount']);
					$campaign = new Campaign($donation['cid']);
					echo '<li class="cartPopUpItemRow cartPopUpCRStars"><div class="cartPopUpItemImage">', $campaign->ImageHTML('small'), '</div><div class="cartPopUpItemDesc"><h4>', strip_tags($campaign->FullTitle()), '</h4><p>(', $donation['donation_zakat'] ? 'Zakat' : 'Sadaqah', ')</p></div><div class="cartPopUpItemAmount">', $cursymbol, number_format($subamount, 2), '</div><div></div></li>';
					$oneoff_lines[] = ob_get_clean();
				}
			}

			if ($itemcount = count($oneoff_lines) + count($monthly_lines))
			{	ob_start();
				$cartpage = new PageContent('cart');
				$donatepage = new PageContent('donate');
				echo '<div><span class="hrLinksCartCount">', $itemcount, '</span><div class="BasketPopup"><div class="BasketPopupBlocker"></div><h3>Your Basket <span>(', $itemcount, ' item', $itemcount == 1 ? '' : 's', ')</span></h3><ul>';
				if ($oneoff_total && $oneoff_lines)
				{	$oneoff_lines = array_reverse($oneoff_lines);
					foreach ($oneoff_lines as $key=>$line)
					{	$oneoff_lines[$key] = str_replace('cartPopUpItemRow', 'cartPopUpItemRow cartPopUpItemRowLast', $line);
						break;
					}
					$oneoff_lines = array_reverse($oneoff_lines);
					echo '<li class="cartPopUpTotal"><div class="cartPopUpTotalLabel">Total Single Donations</div><div class="cartPopUpTotalAmount">', $cursymbol, number_format($oneoff_total, 2), '</div><div></div></li>', implode('', $oneoff_lines);
				}
				if ($monthly_total && $monthly_lines)
				{	$monthly_lines = array_reverse($monthly_lines);
					foreach ($monthly_lines as $key=>$line)
					{	$monthly_lines[$key] = str_replace('cartPopUpItemRow', 'cartPopUpItemRow cartPopUpItemRowLast', $line);
						break;
					}
					$monthly_lines = array_reverse($monthly_lines);
					echo '<li class="cartPopUpTotal"><div class="cartPopUpTotalLabel">Total Monthly Donations</div><div class="cartPopUpTotalAmount">', $cursymbol, number_format($monthly_total, 2), '</div><div></div></li>', implode('', $monthly_lines);
				}
				echo '</ul><p class="cartPopUpButtons"><a href="', $cartpage->Link(), '">View Basket</a></p><p class="cartPopUpButtons"><a href="', $donatepage->Link(), '">Make Another Donation</a></p></div></div>';
				$return['desktop'] = ob_get_clean();
				ob_start();
				echo '<a href="', $cartpage->Link(), '"><span>', $itemcount, '</span></a>';
				$return['mobile'] = ob_get_clean();
			}
		}
		return $return;
	} // fn PopUpBasket
	
	public function GetProperty($fieldname = '')
	{	return $_SESSION[$this->cart_session_name][$fieldname];
	} // fn GetProperty
	
	public function SetProperty($fieldname = '', $value = '')
	{	return $_SESSION[$this->cart_session_name][$fieldname] = $value;
	} // fn SetProperty
	
	public function CartOrder()
	{	return new CartOrder($this->GetProperty('orderid'));
	} // fn CartOrder
	
} // end of defn Cart
?>