<?php
class AdminBookOrder extends BookOrder
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} // fn __construct
	
	public function DisplayOrder()
	{	//$this->SendTickets();
		echo '<div id="itemblock-disp">
					<div><h3>Order</h3><ul>
						<li><span class="itemblock-label">Order number</span><span class="itemblock-details">', $this->id, '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Ordered</span><span class="itemblock-details">', date('d/m/y @H:i', strtotime($this->details['orderdate'])), '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Customer name</span><span class="itemblock-details">', $this->InputSafeString($this->details['firstname'] . ' ' . $this->details['lastname']), '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Email</span><span class="itemblock-details">', $this->InputSafeString($this->details['email']), '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Customer address</span><span class="itemblock-details">', nl2br($this->InputSafeString($this->details['firstname'])), '<br />', $this->InputSafeString($this->details['city']), '<br />', $this->InputSafeString($this->details['postcode']), '<br />', $this->InputSafeString($this->GetCountry($this->details['country'])), '</span><br class="clear" /></li>
						<li><span class="itemblock-label">Phone</span><span class="itemblock-details">', $this->InputSafeString($this->details['phone']), '</span><br class="clear" /></li>';
		$cursymbol = $this->GetCurrency($this->details['currency'], 'cursymbol');
		if (($price = $this->TotalPrice()) > 0)
		{	echo '<li><span class="itemblock-label">Total price</span><span class="itemblock-details">', $cursymbol, number_format($price, 2), '</span><br class="clear" /></li>';
		}
		echo '</ul></div>
					<div><h3>Payment</h3><ul>';
		if ($this->details['payref'])
		{	echo '<li><span class="itemblock-label">Reference</span><span class="itemblock-details">', $this->InputSafeString($this->details['payref']), '</span><br class="clear" /></li>';
			if ($this->PaymentNeeded())
			{	echo '<li><span class="itemblock-label">Paid</span><span class="itemblock-details">', date('d/m/y @H:i', strtotime($this->details['paid'])), '</span><br class="clear" /></li>';
			}
		} else
		{	echo '<li><span class="itemblock-label">Not paid</span><br class="clear" /></li>';
		}
		echo '</ul></div>
					<div><h3>Tickets ';
		if ($this->details['payref'])
		{	echo 'booked <a onclick="BookingResendConfirm(', $this->id, ');">Resend tickets</a>';
		} else
		{	echo 'in cart';
		}
		echo '</h3><ul>';
		$can_events = $this->CanAdminUser('events');
	
		foreach ($this->items as $item)
		{	$ticket = new AdminTicketType($item['ttid']);
			$eventdate = new AdminEventDate($ticket->eventdate);
			echo '<li><span class="itemblock-label">';
			if ($can_events)
			{	echo '<a href="eventdate.php?id=', $ticket->eventdate['edid'], '">';
			}
			echo $this->InputSafeString($ticket->event['eventname']), ', ', $this->InputSafeString($ticket->venue['venuename']), '<br />', $eventdate->DatesString(), $can_events ? '</a>' : '', '</span><span class="itemblock-details">', $this->InputSafeString($ticket->details['ttypename']), ' &times; ', $item['quantity'];
			if ($item['price'] > 0)
			{	echo ' @ ', $cursymbol, number_format($item['price'], 2), ' = ', $cursymbol, number_format($item['price'] * $item['quantity'], 2);
			}
			echo '</span><br class="clear" /></li>';
		}
		echo '</ul></div>
				</div>';
		echo '<script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$("#adbook_modal_popup").jqm();});</script>',
	'<!-- START banners list modal popup --><div id="adbook_modal_popup" class="jqmWindow" style="padding-bottom: 5px; width: 420px; margin-left: -210px; top: 150px;"><a href="#" class="jqmClose">Close</a><div id="adbookModalInner"></div>';
		//$this->VarDump($this->details);
		//$this->VarDump($this->items);
	} // fn DisplayOrder
	
} // end of defn AdminBookOrder
?>