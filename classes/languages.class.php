<?php
class Languages extends Base
{	var $languages = false;
	var $liveonly = 0;
	
	function __construct($liveonly = false)
	{	parent::__construct();
		$this->liveonly = (bool)$liveonly;
		$this->Get();
	} //  end of fn __construct
	
	function Reset()
	{	$this->languages = array();
	} // end of fn Reset
	
	function GetSQL()
	{	$where = array();
		
		if ($this->liveonly)
		{	$where[] = "live=1";
		}
		
		$sql = "SELECT * FROM languages";
		if ($wstr = implode(" AND ", $where))
		{	$sql .= " WHERE $wstr";
		}
		$sql .= " ORDER BY disporder";
		return $sql;
	} // end of fn GetSQL
	
	function Get()
	{	$this->Reset();
		if ($result = $this->db->Query($this->GetSQL()))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->languages[$row["langcode"]] = $this->AssignLanguage($row);
			}
		}
		
	} // end of fn Get
	
	function AssignLanguage($lang = array())
	{	return new Language($lang);
	} // end of fn AssignLanguage
	
} // end of defn Orders
?>