<?php
class AdminVolunteersPage extends AdminPage
{	protected $volunteer;
	protected $menuarea;
	protected $startdate = '';
	protected $enddate = '';

	function __construct()
	{	parent::__construct('CAMPAIGNS');
	} //  end of fn __construct

	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('campaigns'))
		{	$this->AdminVolunteersLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AdminVolunteersLoggedInConstruct()
	{	$this->AssignVolunteer();
		$this->breadcrumbs->AddCrumb('volunteers.php', 'Volunteers');
		$this->VolunteerConstructFunctions();
		if ($this->volunteer->id)
		{	$this->breadcrumbs->AddCrumb('volunteer.php?id=' . $this->volunteer->id, $this->volunteer->FullName());
		}
	} // end of fn AdminVolunteersLoggedInConstruct
	
	protected function VolunteerConstructFunctions()
	{	
	} // end of fn VolunteerConstructFunctions

	protected function AssignVolunteer()
	{	$this->volunteer = new AdminVolunteer($_GET['id']);
	} // end of fn AssignVolunteer
	
	protected function AdminVolunteersBody()
	{	if ($menu = $this->GetVolunteerMenu())
		{	echo '<div class="adminItemSubMenu"><ul>';
			foreach ($menu as $menuarea=>$menuitem)
			{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '>';
				if ($menuitem['link'])
				{	echo '<a href="', $menuitem['link'], '"', $menuitem['blank'] ? ' target="_blank"' : '', '>';
				} else
				{	echo '<span>';
				}
				echo $this->InputSafeString($menuitem['text']), $menuitem['link'] ? '</a>' : '</span>', '</li>';
			}
			echo '</ul><div class="clear"></div></div>';
		}
	} // end of fn AdminVolunteersBody
	
	protected function GetVolunteerMenu()
	{	$menu = array();
		if ($this->volunteer->id)
		{	$menu['view'] = array('text'=>'View ' . $this->InputSafeString($this->volunteer->FullName()), 'link'=>'volunteer.php?id=' . $this->volunteer->id);
		}
		return $menu;
	} // end of fn GetVolunteerMenu
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('campaigns'))
		{	$this->AdminVolunteersBody();
		}
	} // end of fn AdminBodyMain
	
	protected function GetVolunteers()
	{	$tables = array('volunteers'=>'volunteers');
		$fields = array('volunteers.*');
		$where = array('start'=>'volunteers.regdate>="' . $this->startdate . ' 00:00:00"', 'end'=>'volunteers.regdate<="' . $this->enddate . ' 23:59:59"');
		$orderby = array('volunteers.regdate DESC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'volid', true);
	} // end of fn GetVolunteers
	
} // end of defn AdminCampaignsPage
?>