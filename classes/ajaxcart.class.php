<?php
class AjaxCart extends BasePage
{	var $cart;

	function __construct()
	{	parent::__construct();
		$this->cart = new SCCart();
		$this->cart->InitialiseFromSession();
	} // end of fn __construct
	
	function Output(){}
	
	function CartButtons()
	{	ob_start();
		echo '<div class="cart_buttons"><a class="scbutton_long" href="', SITE_URL, 'cart.php">', $this->GetTranslatedText('popupcartlink'), '</a><a class="scbutton_long" href="', SITE_URL, 'sc_address.php', '">', $this->GetTranslatedText('proceedcheckout'), '</a></div>';
		return ob_get_clean();
	} // end of fn CartButtons
	
} // end of defn AjaxCart
?>