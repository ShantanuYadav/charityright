<?php
class AdminMailTemplate extends MailTemplate
{	var $langused = array();

	function __construct($id = 0)
	{	parent::__construct($id);
		$this->GetLangUsed();
	} //  end of fn __construct
	
	function AssignMailLanguage()
	{	if (!$this->language = $_GET["lang"])
		{	$this->language = $this->def_lang;
		}
	} // end of fn AssignMailLanguage
	
	function AddDetailsForDefaultLang(){}
	
	function GetLangUsed()
	{	$this->langused = array();
		if ($this->id)
		{	if ($result = $this->db->Query("SELECT lang FROM mailtemplates_lang WHERE mailid=$this->id"))
			{	while ($row = $this->db->FetchArray($result))
				{	$this->langused[$row["lang"]] = true;
				}
			}
		}
	} // end of fn GetLangUsed
	
	function CanDelete()
	{	return false;
	} // end of fn CanDelete
	
	function Delete()
	{	return false;
	} // end of fn Delete

	function Save($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		$l_fields = array();
		$admin_actions = array();

		if ($subject = $this->SQLSafe($data["subject"]))
		{	$l_fields[] = "subject='$subject'";
			if ($this->id && ($data["subject"] != $this->details["subject"]))
			{	$admin_actions[] = array("action"=>"Subject ({$this->language})", "actionfrom"=>$this->details["subject"], "actionto"=>$data["subject"]);
			}
		} else
		{	$fail[] = "subject missing";
		}

		if ($htmltext = $this->SQLSafe($data["htmltext"]))
		{	$l_fields[] = "htmltext='$htmltext'";
			if ($this->id && ($data["htmltext"] != $this->details["htmltext"]))
			{	$admin_actions[] = array("action"=>"HTML Text ({$this->language})", "actionfrom"=>$this->details["htmltext"], "actionto"=>$data["htmltext"]);
			}
		} else
		{	$fail[] = "HTML text missing";
		}

		if ($plaintext = $this->SQLSafe($data["plaintext"]))
		{	$l_fields[] = "plaintext='$plaintext'";
			if ($this->id && ($data["plaintext"] != $this->details["plaintext"]))
			{	$admin_actions[] = array("action"=>"Plain Text ({$this->language})", "actionfrom"=>$this->details["plaintext"], "actionto"=>$data["plaintext"]);
			}
		} else
		{	$fail[] = "plain text missing";
		}
		
		if (!$fail || $this->langused[$this->language])
		{	
			if ($set = implode(", ", $l_fields))
			{	if ($this->langused[$this->language])
				{	$sql = "UPDATE mailtemplates_lang SET $set WHERE mailid=$this->id AND lang='$this->language'";
				} else
				{	$sql = "INSERT INTO mailtemplates_lang SET $set, mailid=$this->id, lang='$this->language'";
				}
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows())
					{	$success[] = "text changes saved";
						$base_parameters = array("tablename"=>"mailtemplates", "tableid"=>$this->id, "area"=>"mail templates");
						if ($admin_actions)
						{	foreach ($admin_actions as $admin_action)
							{	$this->RecordAdminAction(array_merge($base_parameters, $admin_action));
							}
						}
					}
					$this->Get($this->id);
					$this->GetLangUsed();
				}else echo "<p>", $this->db->Error(), "</p>\n";
			}
		}
		
		return array("failmessage"=>implode(", ", $fail), "successmessage"=>implode(", ", $success));
		
	} // end of fn Save
	
	function InputForm($courseid = 0)
	{	ob_start();

		if ($data = $this->details)
		{	
			if (!$this->langused[$this->language])
			{	if ($_POST)
				{	// initialise details from this
					foreach ($_POST as $field=>$value)
					{	$data[$field ] = $value;
					}
				}
			}
		} else
		{	$data = $_POST;
		}
		
		$form = new Form($_SERVER["SCRIPT_NAME"] . "?id=" . $this->id . "&lang=" . $this->language, "emtInputForm");
		$form->AddTextInput("Mail subject", "subject", $this->InputSafeString($data["subject"]), "long", 255, 1);
		$form->AddTextArea("HTML Text", $name = "htmltext", stripslashes($data["htmltext"]), "tinymce", 0, 0, 20, 40);
		$form->AddTextArea("Plain Text", $name = "plaintext", stripslashes($data["plaintext"]), "", 0, 0, 20, 40);
		$form->AddSubmitButton("", "Save Changes", "submit");
		echo "<h3>Editing ... ", $this->InputSafeString($this->details["mailname"]), "</h3>";
		$this->AdminEditLangList();
		$form->Output();
		echo "<div id='emtFieldsList'>\n";
		if ($this->fields)
		{	echo "<h4>Available fields:</h4>\n<ul>\n";
			foreach ($this->fields as $field)
			{	echo "<li><input value='{", $field["fieldname"], "}' /></li>\n";
			}
			echo "</ul>\n";
		}
		echo "</div>\n<div class='clear'></div>\n<script>\n$('#emtFieldsList>ul>li>input').click(function() {\n$(this).select();\n});\n</script>\n<div class='emtSample'><h4>Sample HTML text (as saved)</h4>\n<div>", 
				$this->BuildHTMLEmailText(), 
				"</div></div>\n<div class='emtSample'><h4>Sample Plain text (as saved)</h4>\n<div>", 
				nl2br(htmlentities($this->BuildHTMLPlainText())), 
				"</div></div>";
		return ob_get_clean();
	} // end of fn InputForm
	
} // end of defn AdminMailTemplate
?>