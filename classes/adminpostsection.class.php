<?php
class AdminPostSection extends PostSection
{	

	function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct
	
	function Save($data = array(), $postid = 0)
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($psectiontext = $this->SQLSafe($data['psectiontext']))
		{	$fields[] = 'psectiontext="' . $psectiontext . '"';
		} else
		{	$fail[] = 'text missing - use "delete" to remove this section';
		}
		
		if (!$this->id)
		{	if (($postid = (int)$postid) && ($post = new Post($postid)) && $post->id)
			{	$fields[] = 'postid=' . $post->id;
			} else
			{	$fail[] = 'post missing for new section';
			}
			if ($psection = $this->SQLSafe($data['psection']))
			{	$fields[] = 'psection="' . $psection . '"';
			} else
			{	$fail[] = 'section missing for new section';
			}
		}
		
	//	$fail[] = 'test';
		
		if ($this->id || !$fail)
		{	
			$set = implode(', ', $fields);
			if ($this->id)
			{	$sql = 'UPDATE postsections SET ' . $set . ' WHERE psid=' . $this->id;
			} else
			{	$sql = 'INSERT INTO postsections SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->id)
					{	$success[] = 'changes saved';
						$this->Get($this->id);
					} else
					{	if ($id = $this->db->InsertID())
						{	$this->Get($id);
							$success[] = 'new section added';
						} else
						{	$fail[] = 'insert failed';
						}
					}
				}
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	function CanDelete()
	{	return $this->id;
	} // end of fn CanDelete
	
	function InputForm($postid = 0, $psection = '')
	{	 
		$data = $this->details;
		if (!$data = $this->details)
		{	$data = $_POST;
			if (!$data['psection'])
			{	$data['psection'] = $psection;
			}
		}

		if ($this->id)
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id, '');
		} else
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?postid=' . (int)$postid, '');
		}
		$form->AddHiddenInput('psection', $data['psection']);
		$form->AddTextArea('Section text for "' . $data['psection'] . '"', 'psectiontext', stripslashes($data['psectiontext']), 'tinymce', 0, 0, 20, 60);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Section', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this section</a></p>';
			}
		}
		$form->Output();
	} // end of fn InputForm
	
} // end of defn AdminPostSection
?>