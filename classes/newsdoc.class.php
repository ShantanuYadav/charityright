<?php
class NewsDoc extends NewsDocBase
{	var $id = 0;
	var $maxSize = 5000;
	var $details = array();

	function __construct($id = 0)
	{	parent::__construct();
		$this->Get($id);
	} //  end of fn __construct
	
	function Get($id = 0)
	{	$this->ReSet();

		if (is_array($id))
		{	$this->details = $id;
			return $this->id = $id["docid"];
		} else
		{	if ($result = $this->db->Query("SELECT * FROM documentfiles WHERE docid=" . (int)$id))
			{	if ($row = $this->db->FetchArray($result))
				{	return $this->Get($row);
				}
			}
		}
		
	} // end of fn Get
	
	function ReSet()
	{	$this->id = 0;
		$this->details = array();
	} // end of fn ReSet
	
	function FullFilePath()
	{	return $this->filedir . $this->details["filename"];
	} // end of fn FullFilePath
	
	function FileExists()
	{	if ($this->details["filename"])
		{	return file_exists($this->FullFilePath());
		}
	} // end of fn FileExists
	
	function UploadFile($file = array())
	{	
		$fail = array();
		$success = array();

		if ($file["size"])
		{	if ($file["size"] > $this->maxSize * 1024)
			{	$fail[] = "file is too big (" . number_format($file["size"] / 1024, 1) . "kB)";
			} else
			{	
				if ($this->FileExists())
				{	unlink($this->FullFilePath());
				}
				
				$this->details["filename"] = $file["name"];
				if (move_uploaded_file($file["tmp_name"], $this->FullFilePath()))
				{	$success[] = "new file uploaded";
					// update filetype
					$this->db->Query("UPDATE documentfiles SET filename='{$this->details["filename"]}' WHERE docid=$this->id");
				} else
				{	$fail[] = "file upload failed";
				}
			}
		} else
		{	$fail[] = "no file to upload";
		}
		
		return array("failmessage"=>implode(", ", $fail), "successmessage"=>implode(", ", $success));

	} // end of fn UploadFile
	
	function Save($data, $file)
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		
		if (!$this->id && !$file["size"])
		{	$fail[] = "no document uploaded";
		}
		
		$fields[] = "uploaddate='" . $this->datefn->SQLdateTime() . "'";
		$fields[] = "docname='" . $this->SQLSafe($data["docname"]) . "'";
		if ($this->id || !$fail)
		{	$set = implode(", ", $fields);

			if ($this->id)
			{	$sql = "UPDATE documentfiles SET $set WHERE docid=$this->id";
			} else
			{	$sql = "INSERT INTO documentfiles SET $set";
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->id || $this->db->AffectedRows())
				{	if ($this->id)
					{	// existing ...
						if ($this->db->AffectedRows)
						{	$success[] = "changes saved";
						}
						if ($file["size"])
						{	$uploaded = $this->UploadFile($file);
							if ($uploaded["successmessage"])
							{	$success[] = $uploaded["successmessage"];
							} else
							{	if ($uploaded["failmessage"])
								{	$fail[] = $uploaded["failmessage"];
								}
							}
						}
						$this->Get($this->id);
					} else
					{	if ($this->Get($this->db->InsertID()))
						{	$uploaded = $this->UploadFile($file);
							if ($uploaded["successmessage"])
							{	$success[] = $uploaded["successmessage"];
							} else
							{	if ($uploaded["failmessage"])
								{	$fail[] = $uploaded["failmessage"];
								}
								$this->Delete();
							}
						} else
						{	$fail[] = "file creation failed";
						}
					}
				}
			}
			
		}
		
		return array("failmessage"=>implode(", ", $fail), "successmessage"=>implode(", ", $success));

	} // end of fn Save
	
	function DocLink()
	{	return $this->linkdir . $this->details["filename"];
	} // end of fn ImageLink
	
	function CanDelete()
	{	
		// scan all news
		$doclink = $this->DocLink();
		if ($result = $this->db->Query("SELECT newstext FROM news"))
		{	while ($row = $this->db->FetchArray($result))
			{	if (strstr($row["newstext"], $doclink))
				{	return false;
				}
			}
		}
		
		// scan all pages
		if ($result = $this->db->Query("SELECT pagetext FROM pages"))
		{	while ($row = $this->db->FetchArray($result))
			{	if (strstr($row["pagetext"], $doclink))
				{	return false;
				}
			}
		}
		
		return true;
	} // end of fn CanDelete
	
	function Delete()
	{	$fail = array();
		$success = array();

		if ($this->CanDelete())
		{	$sql = "DELETE FROM documentfiles WHERE docid=$this->id";
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$success[] = "document \"{$this->details["docname"]}\" has been deleted";
					if ($this->FileExists())
					{	unlink($this->FullFilePath());
					}
					$this->Reset();
				}
			}
		} else
		{	$fail[] = "can't delete document (in use)";
		}
		
		return array("failmessage"=>implode(", ", $fail), "successmessage"=>implode(", ", $success));

	} // end of fn Delete

	function UploadForm()
	{	echo "<div id='stories'>\n";
		$form = new Form($_SERVER["SCRIPT_NAME"] . "?id=" . $this->id, "imageform");
		$form->AddTextInput("Document description", "docname", $this->InputSafeString($this->details["docname"]), "", 50);
		$form->AddFileUpload($this->id ? "Replace file with" : "New file", "newsdoc");
		$form->AddSubmitButton("", $this->id ? "Save Replacement" : "Create Document", "submit");
		$form->Output();
		if ($this->id && $this->FileExists($this->id))
		{	echo "<p><a href='", $this->DocLink(), "' />preview document</a></p>\n";
			$this->ListUses();
		}
		echo "</div>\n";
	} // end of fn UploadForm
	
	function ListUses()
	{	
		echo "<div id='newsuses'><p>Currently used in ...</p>\n<ul>";
	
		if ($doclink = $this->DocLink())
		{	// scan all news
			if ($result = $this->db->Query("SELECT * FROM news"))
			{	while ($row = $this->db->FetchArray($result))
				{	if (strstr($row["newstext"], $doclink))
					{	echo "<li>new story: <a href='newsstory.php?id=", $row["newsid"], "'>", $row["headline"], "</a></li>\n";
						$usedcount++;
					}
				}
			}
			
			// scan all pages
			if ($result = $this->db->Query("SELECT * FROM pages"))
			{	while ($row = $this->db->FetchArray($result))
				{	if (strstr($row["pagetext"], $doclink))
					{	echo "<li>page: <a href='pageedit.php?id=", $row["pageid"], "'>", $row["pagetitle"], "</a></li>\n";
						$usedcount++;
					}
				}
			}
		}
		
		if (!$usedcount)
		{	echo "<li>not used in any stories or pages</li>\n";
		}
		echo "</ul></div>";
	} // end of fn ListUses
	
} // end of class NewsDoc
?>