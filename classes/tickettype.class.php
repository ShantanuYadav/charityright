<?php
class TicketType extends BlankItem
{	public $event = array();
	public $venue = array();
	public $eventdate = array();

	public function __construct($id = 0)
	{	parent::__construct($id, 'tickettypes', 'ttid');
	} // fn __construct
	
	public function GetExtra()
	{	$sql = $this->db->BuildSQL(array('eventdates'), array('eventdates.*'), array('eventdates.edid=' . (int)$this->details['edid']));
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	$this->eventdate = $row;
				$sql = $this->db->BuildSQL(array('events'), array('events.*'), array('events.eid=' . (int)$this->eventdate['eid']));
				if ($result = $this->db->Query($sql))
				{	if ($row = $this->db->FetchArray($result))
					{	$this->event = $row;
					}
				}
				$sql = $this->db->BuildSQL(array('eventvenues'), array('eventvenues.*'), array('eventvenues.evid=' . (int)$this->eventdate['venue']));
				if ($result = $this->db->Query($sql))
				{	if ($row = $this->db->FetchArray($result))
					{	$this->venue = $row;
					}
				}
			}
		}
	} // fn GetExtra
	
	public function CanBook($number = 0)
	{	if ($this->details['live'])
		{	if ($this->details['stockcontrol'])
			{	if ((count($this->GetBookings) + (int)$number) > $this->details['booklimit'])
				{	return false;
				} else
				{	return true;
				}
			} else
			{	return true;
			}
		} else
		{	return false;
		}
	} // fn CanBook
	
	public function TicketsLeft($in_cart = 0)
	{	return $this->details['booklimit'] - count($this->GetBookings()) - (int)$in_cart;
	} // fn TicketsLeft
	
	public function GetBookings()
	{	$tables = array('bookings'=>'bookings');
		$where = array('ttid'=>'ttid=' . (int)$this->id);
		$orderby = array('bookings.bid ASC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, array('bookings.*'), $where, $orderby), 'bid', true);
	} // fn GetBookings
	
	public function OrderFromBooking($bookingrow = array())
	{	$tables = array('bookorderitems'=>'bookorderitems', 'bookorders'=>'bookorders');
		$fields = array('bookorders.*');
		$where = array('bookorders_link'=>'bookorders.orderid=bookorderitems.orderid', 'orderitem'=>'bookorderitems.orderitem=' . (int)$bookingrow['orderitem']);
		
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where)))
		{	if ($row = $this->db->FetchArray($result))
			{	return $this->AssignBookOrder($row);
			}
		}
	} // fn OrderFromBooking
	
	protected function AssignBookOrder($orderrow = array())
	{	return new BookOrder($orderrow);
	} // fn AssignBookOrder
	
	public function AddToCart($number = 0)
	{	$fail = array();
		$success = array();
		
		if ($number = (int)$number)
		{	if ($this->CanBook($number + (int)$_SESSION['events']['event_tickets'][$this->id]))
			{	if (!$_SESSION['events']['currency'] || ($this->details['price'] <= 0) || ($_SESSION['events']['currency'] == $this->eventdate['currency']))
				{	$_SESSION['events']['event_tickets'][$this->id] += $number;
					$success[] = 'tickets booked';
					if ($this->details['price'] > 0)
					{	$_SESSION['events']['currency'] = $this->eventdate['currency'];
					}
				} else
				{	$fail[] = 'You can\'t book tickets with different currencies at the same time';
				}
			} else
			{	$fail[] = 'Not enough tickets left';
			}
		} else
		{	$fail[] = 'Number of tickets is missing';
		}
		
		return array('successmessage'=>implode(', ', $success), 'failmessage'=>implode(', ', $fail));
	} // fn AddToCart
	
} // end of defn TicketType
?>