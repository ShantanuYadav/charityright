<?php
class NewsImage extends NewsImageBase
{	var $filename = 0;
	var $maxSize = 5000;

	function __construct($filename = '')
	{	parent::__construct();
		$this->Get($filename);
	} //  end of fn __construct
	
	function Get($filename)
	{	$this->ReSet();
		if ($this->FileExists($filename))
		{	$this->filename = $filename;
		}
	} // end of fn Get
	
	function ReSet()
	{	$this->filename = '';
	} // end of fn ReSet
	
	function FullFilePath($filename = '')
	{	return $this->filedir . $filename;
	} // end of fn FullFilePath
	
	function FileExists($filename = '')
	{	return file_exists($filepath = $this->FullFilePath($filename)) && !is_dir($filepath);
	} // end of fn FileExists
	
	function Upload($file)
	{	
		$fail = array();
		$success = array();

		$uploadSizeLimit = 1024 * $this->maxSize;
		
		if ($file['error'] || !$file['size'])
		{	$fail[] = 'error on file upload';
		} else
		{	if ($file['size'] > $uploadSizeLimit)
			{	$fail[] = 'file is too big (' . number_format($file['size'] / 1024, 1) . 'kB)';
			} else
			{	
				foreach ($this->suffixes as $suffix=>$suffix_details)
				{	
					foreach ($suffix_details['type_id'] as $type=>$type_ok)
					{	if (stristr($file['type'], $type))
						{	$type_found = true;
							if (move_uploaded_file($file['tmp_name'], $this->FullFilePath($file['name'])))
							{	$success[] = 'new image uploaded';
								$this->Get($file['name']);
							} else
							{	$fail[] = 'file upload failed';
							}
							break;
						}
					}
					if ($type_found)
					{	break;
					}
				}
				if (!$type_found)
				{	$fail[] = 'file type invalid';
				}
			}
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn Save
	
	function ImageLink()
	{	if ($this->FileExists($this->filename))
		{	return $this->imagedir . $this->filename;
		}
	} // end of fn ImageLink
	
	function Delete()
	{	if ($this->CanDelete())
		{	if (unlink($this->FullFilePath($this->filename)))
			{	return true;
			}
		}
	} // end of fn Delete

	function UploadForm()
	{	echo '<div id="stories">';
		$form = new Form($_SERVER['SCRIPT_NAME']);
		$form->AddFileUpload('New image', 'newsimage');
		$form->AddSubmitButton('', 'Upload Image', 'submit');
		$form->Output();
		echo '</div>';
	} // end of fn UploadForm
	
	public function SizeString()
	{	ob_start();
		if ($size = getimagesize($this->FullFilePath($this->filename)))
		{	echo (int)$size[0].'px * ', (int)$size[1], 'px';
		} else
		{	echo 'size not found';
		}
		return ob_get_clean();
	} // end of fn SizeString
	
	function ListUses()
	{	
		if ($uses = $this->GetUses())
		{	echo '<ul class="newsuses">';
			foreach ($uses as $row)
			{	switch ($row['row_type'])
				{	case 'page':
						echo '<li>page: <a href="pageedit.php?id=', $row['pageid'], '">', $this->InputSafeString($row['pagetitle']), '</a></li>';
						break;
					case 'news':
						echo '<li>news: <a href="newsedit.php?id=', $row['newsid'], '">', $this->InputSafeString($row['headline']), '</a></li>';
						break;
					case 'plate_value':
						echo '<li>plate value: <a href="plate_value.php?id=', $row['id'], '">', $this->InputSafeString($row['display_value']), '</a></li>';
						break;
				}
			}
			echo '</ul>';
		} else
		{	echo 'not used in any pages';
		}
	} // end of fn ListUses
	
	public function CanDelete()
	{	return $this->filename && !$this->GetUses();
	} // end of fn CanDelete
	
	public function GetUses()
	{	$uses = array();
		if ($imagelink = $this->ImageLink())
		{	if ($result = $this->db->Query('SELECT * FROM pages_lang'))
			{	while ($row = $this->db->FetchArray($result))
				{	if (strstr($row['pagetext'], $imagelink))
					{	$row['row_type'] = 'page';
						$uses[] = $row;
					}
				}
			}
			if ($result = $this->db->Query('SELECT * FROM news_lang'))
			{	while ($row = $this->db->FetchArray($result))
				{	if (strstr($row['newstext'], $imagelink))
					{	$row['row_type'] = 'news';
						$uses[] = $row;
					}
				}
			}
			if ($result = $this->db->Query('SELECT * FROM plate_attribute_value'))
			{	while ($row = $this->db->FetchArray($result))
				{	if (strstr($row['pbhelp'], $imagelink))
					{	$row['row_type'] = 'plate_value';
						$uses[] = $row;
					}
				}
			}
		}
		return $uses;
	} // end of fn GetUses
	
} // end of class NewsImage
?>