<?php
class AdminUser extends Base
{	var $userid = 0;
	var $loggedin = false;
	var $infoonly = false;
	var $accessAreas = array('administration'=>0, 'web content'=>1, 'events'=>9, 'campaigns'=>4, 'posts'=>5, 'accounts'=>3, 'accounts admin'=>7, 'technical'=>2, 'crm'=>6);
	var $confirmed = 1;
	var $details = array();

	public function __construct($userid = 0, $infoonly = 0)
	{	parent::__construct();
		$this->infoonly = (int)$infoonly;
		$this->Get($userid);
		if (!$this->infoonly && $this->userid) $this->loggedin = true;
	} // fn __construct

	protected function Get($userid = 0)
	{	
		if (is_array($userid))
		{	$this->details = $userid;
			return $this->userid = $this->details['auserid'];
		} else
		{	$sql = 'SELECT * FROM adminusers WHERE auserid=' . (int)$userid;
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	return $this->Get($row);
				}
			}
		}
	} // end of fn Get

	public function FullName($area = '')
	{	return trim($this->details['firstname'] . ' ' . $this->details['surname']);
	} // end of fn FullName

	public function CanUserAccess($area = '')
	{	return $this->details['useraccess'] & pow(2, $this->accessAreas[$area]);
	} // end of fn CanUserAccess
	
	public function UserAccessList()
	{	$areas = array();
		foreach ($this->accessAreas as $area=>$digit)
		{	if ($this->CanUserAccess($area))
			{	$areas[] = $area;
			}
		}
		return implode(', ', $areas);
	} // end of fn UserAccessList
	
	public function Save($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if ($data['pword'] || $data['rtpword'])
		{	if ($data['pword'] !== $data['rtpword'])
			{	$fail[] = 'password mistyped';
			} else
			{	if ($this->AcceptablePW($data['pword'], 8, 20))
				{	$fields['pword'] = 'upassword=MD5("' . $data['pword'] . '")';
				} else
				{	$fail[]= 'password not acceptable';
				}
			}
		} else
		{	if (!$this->userid)
			{	$fail[] = 'password needed';
			}
		}

		if ($data['ausername'])
		{	if (preg_match('{^[A-Za-z0-9]{3,30}$}i', $data['ausername']))
			{	$fields['ausername'] = 'ausername="' . $data['ausername'] . '"';
			} else
			{	$fail[] = 'invalid username (3 to 30 numbers and letters only)';
			}
		} else
		{	$fail[] = 'username is missing';
		}

		if ($data['firstname'])
		{	$fields['firstname'] = 'firstname="' . $this->SQLSafe($data['firstname']) . '"';
		} else
		{	$fail[] = 'first name missing';
		}

		if ($data['surname'])
		{	$fields['surname'] = 'surname="' . $this->SQLSafe($data['surname']) . '"';
		} else
		{	$fail[] = 'surname missing';
		}
		
		if ($data['email'])
		{	if ($this->ValidEMail($data['email']))
			{	$fields['email'] = 'email="' . $data['email'] . '"';
			} else
			{	$fail[] = 'invalid e-mail';
			}
		}
		
		if (!$fail || $this->userid)
		{	$set = implode(', ', $fields);
			if ($this->userid)
			{	$sql = 'UPDATE adminusers SET ' . $set . ' WHERE auserid=' . $this->userid;
			} else
			{	$sql = 'INSERT INTO adminusers SET ' . $set;
			}
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->userid)
					{	$success[] = 'changes saved';
						$this->Get($this->userid);
					} else
					{	if ($userid = $this->db->InsertID())
						{	$this->Get($userid);
							$success[] = 'new user created';
						}
					}
				}
			} else echo $sql, ': ', $this->db->Error();
		}
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn Save
	
	public function AccessAreasForm($adminuser)
	{	if ($this->userid)
		{	$editform = new Form('useraccess.php?userid=' . $this->userid, 'regform');
			foreach ($this->accessAreas as $area=>$digit)
			{	$editform->AddCheckBox($area, 'access[' . $digit . ']', pow(2, $digit), $this->CanUserAccess($area), '');
			}
			$editform->AddHiddenInput('savinguser', $adminuser->userid);
			$editform->AddSubmitButton('', 'Save Changes', 'submit');
			$editform->Output();
		}
	} // end of fn AccessAreasForm
	
	public function SaveAccessAreas($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();

		$accessnumber = (is_array($data['access']) ? (int)array_sum($data['access']) : 0);
		
		$fields['ausername'] = 'useraccess=' . $accessnumber;
		
		if (($data['savinguser'] == $this->userid) && !$data['access'][$this->accessAreas['administration']])
		{	$fail[] = 'you cannot remove your own administration privileges';
		}
		
		if (!$fail)
		{	$set = implode(', ', $fields);
			$sql = 'UPDATE adminusers SET ' . $set . ' WHERE auserid=' . $this->userid;
			
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$success[] = 'changes saved';
					$this->Get($this->userid);
				}
			}
		}
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn Save
	
	public function InputForm($adminuser)
	{	$editform = new Form('useredit.php?userid=' . $this->userid, 'regform');
		if ($data = $this->details)
		{	if (($this->userid != $adminuser->userid) && $this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="useredit.php?userid=', $this->userid, '&del=1', $_GET['del'] ? '&confirm=1' : '' , '">', 
					$_GET['del'] ? 'confirm you want to ' : '' , 'delete this user</a></p>';
			}
		} else
		{	echo '<h3>new user</h3>';
			$data = $_POST;
		}
		$editform->AddTextInput('Log in username (3 to 30 letters or numbers)', 'ausername', $data['ausername'], '');
		$editform->AddTextInput('First name', 'firstname', $data['firstname'], '');
		$editform->AddTextInput('Surname', 'surname', $data['surname'], '');
		$editform->AddTextInput('Email', 'email', $data['email'], '');
		$editform->AddPasswordInput('Password (8 to 20 letters or numbers)', 'pword', '', 20);
		$editform->AddPasswordInput('... retype', 'rtpword', '', 20);
		$editform->AddLabelLine('User has access to ...', '');
		$editform->AddSubmitButton('', $this->userid ? 'Save Changes' : 'Create New User', 'submit');
		$editform->Output();
	} // end of fn InputForm
	
	public function CanDelete()
	{	return $this->userid;
	} // end of fn CanDelete
	
	public function Delete()
	{	if ($this->CanDelete())
		{	$sql = 'DELETE FROM adminusers WHERE auserid=' . (int)$this->userid;
			if ($result = $this->db->Query($sql))
			{	return $this->db->AffectedRows();
			}
		}
	} // end of fn Delete
	
	public function DeleteLink($text = 'delete this user')
	{	ob_start();
		if ($this->userid)
		{	echo '<a class="adminItemDeleteLink" href="useredit.php?userid=', $this->userid, '&del=1', $_GET['del'] ? '&confirm=1' : '' , '">', 
					$_GET['del'] ? 'confirm you want to ' : '' , $text, '</a>';
		}
		return ob_get_clean();
	} // end of fn DeleteLink
	
} // end if class defn AdminUser
?>