<?php
class AdminCampaignTextPage extends AdminCampaignsPage
{	protected $campaigntext;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('campaigntextlist.php', 'Sample text');
		if ($this->campaigntext->id)
		{	
			$this->breadcrumbs->AddCrumb('campaigntext.php?id=' . $this->campaigntext->id, $this->InputSafeString($this->campaigntext->details['ctname']));
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AssignCampaign()
	{	$this->campaigntext = new AdminCampaignText($_GET['id']);
	} // end of fn AssignCampaign
	
	protected function GetCampaignMenu()
	{	$menu = array();
		if ($this->campaigntext->id)
		{	
			$menu['edit'] = array('text'=>$this->InputSafeString($this->InputSafeString($this->campaigntext->details['ctname'])), 'link'=>'campaigntext.php?id=' . $this->campaigntext->id);
		}
		return $menu;
	} // end of fn GetCampaignMenu
	
} // end of defn AdminCampaignTextPage
?>