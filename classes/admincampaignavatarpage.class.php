<?php
class AdminCampaignAvatarPage extends AdminCampaignsPage
{	protected $campaignavatar;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('campaignavatars.php', 'Avatars');
		if ($this->campaignavatar->id)
		{	
			$this->breadcrumbs->AddCrumb('campaignavatar.php?id=' . $this->campaignavatar->id, $this->InputSafeString($this->campaignavatar->details['imagedesc']));
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AssignCampaign()
	{	$this->campaignavatar = new AdminCampaignAvatar($_GET['id']);
	} // end of fn AssignCampaign
	
	protected function GetCampaignMenu()
	{	$menu = array();
		if ($this->campaignavatar->id)
		{	
			$menu['edit'] = array('text'=>$this->InputSafeString($this->InputSafeString($this->campaignavatar->details['imagedesc'])), 'link'=>'campaignavatar.php?id=' . $this->campaignavatar->id);
		}
		return $menu;
	} // end of fn GetCampaignMenu
	
} // end of defn AdminCampaignAvatarPage
?>