<?php
class AdminEventsPage extends AdminPage
{	protected $event;
	protected $eventdate;
	protected $tickettype;
	protected $menuarea;

	function __construct()
	{	parent::__construct('EVENTS');
	} //  end of fn __construct

	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('events'))
		{	$this->AdminEventsLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AdminEventsLoggedInConstruct()
	{	$this->AssignEvent();
		$this->breadcrumbs->AddCrumb('events.php', 'Events');
		$this->EventConstructFunctions();
		if ($this->event->id)
		{	$this->breadcrumbs->AddCrumb('event.php?id=' . $this->event->id, $this->InputSafeString($this->event->details['eventname']));
		}
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function EventConstructFunctions()
	{	
	} // end of fn EventConstructFunctions
	
	protected function AssignEvent()
	{	$this->event = new AdminEvent($_GET['id']);
	} // end of fn AssignEvent
	
	protected function AdminEventsBody()
	{	if ($this->event->id)
		{	if ($menu = $this->GetEventMenu())
			{	echo '<div class="adminItemSubMenu"><ul>';
				foreach ($menu as $menuarea=>$menuitem)
				{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
				}
				echo '</ul><div class="clear"></div></div>';
			}
		}
	} // end of fn AdminEventsBody
	
	protected function GetEventMenu()
	{	$menu = array();
		$menu['edit'] = array('text'=>'Edit', 'link'=>'event.php?id=' . $this->event->id);
		$menu['dates'] = array('text'=>'Dates', 'link'=>'eventdates.php?id=' . $this->event->id);
		if ($this->eventdate->id)
		{	$menu['dateedit'] = array('text'=>$this->InputSafeString($this->eventdate->venue['venuename'] . ' ' . date('d/m/y', strtotime($this->eventdate->details['starttime']))), 'link'=>'eventdate.php?id=' . $this->eventdate->id);
			if ($date_bookings = count($this->eventdate->GetBookings()))
			{	$menu['edbookings'] = array('text'=>'Bookings', 'link'=>'eventdatebookings.php?id=' . $this->eventdate->id);
			}
			$menu['tickettypes'] = array('text'=>'Ticket types', 'link'=>'tickettypes.php?id=' . $this->eventdate->id);
			if ($this->tickettype->id)
			{	$menu['tickettypeedit'] = array('text'=>$this->InputSafeString($this->tickettype->details['ttypename']), 'link'=>'tickettype.php?id=' . $this->tickettype->id);
				if ($ticket_bookings = count($this->tickettype->GetBookings()))
				{	$menu['ttbookings'] = array('text'=>$this->InputSafeString($this->tickettype->details['ttypename']) . ' Bookings', 'link'=>'tickettypebookings.php?id=' . $this->tickettype->id);
				}
			}
		}
		return $menu;
	} // end of fn GetEventMenu
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('events'))
		{	$this->AdminEventsBody();
		}
	} // end of fn AdminBodyMain
	
} // end of defn AdminEventsPage
?>