<?php
class AdminNewsCats extends NewsCats
{
	function __construct()
	{	parent::__construct(false);
	} // fn __construct
	
	function AssignCat($catid)
	{	return new AdminNewsCat($catid);
	} // end of fn AssignCat
	
} // end if class defn AdminNewsCats
?>