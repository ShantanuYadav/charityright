<?php
class AdminPostsPage extends AdminPage
{	protected $post;
	protected $menuarea;

	function __construct()
	{	parent::__construct('POSTS');
	} //  end of fn __construct

	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('posts'))
		{	$this->AdminPostsLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AdminPostsLoggedInConstruct()
	{	$this->css['adminposts'] = 'adminposts.css';
		$this->js['adminposts'] = 'adminposts.js';
		$this->AssignPost();
		$this->breadcrumbs->AddCrumb('posts.php', 'Posts');
		$this->PostConstructFunctions();
		if ($this->post->id)
		{	$this->breadcrumbs->AddCrumb('post.php?id=' . $this->post->id, $this->InputSafeString($this->post->details['posttitle']));
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function PostConstructFunctions()
	{	
	} // end of fn PostConstructFunctions
	
	protected function AssignPost()
	{	$this->post = new AdminPost($_GET['id']);
	} // end of fn AssignPost
	
	protected function AdminPostsBody()
	{	if ($menu = $this->GetPostMenu())
		{	echo '<div class="adminItemSubMenu"><ul>';
			foreach ($menu as $menuarea=>$menuitem)
			{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
			}
			echo '</ul><div class="clear"></div></div>';
		}
	} // end of fn AdminPostsBody
	
	protected function GetPostMenu()
	{	$menu = array();
		if ($this->post->id)
		{	$menu['edit'] = array('text'=>'Edit', 'link'=>'post.php?id=' . $this->post->id);
			$menu['cats'] = array('text'=>'Categories', 'link'=>'postcats.php?id=' . $this->post->id);
			$menu['images'] = array('text'=>'Images', 'link'=>'postimages.php?id=' . $this->post->id);
			$menu['videos'] = array('text'=>'Videos', 'link'=>'postvideos.php?id=' . $this->post->id);
			$menu['donateform'] = array('text'=>'Quick Donate', 'link'=>'postdonateform.php?id=' . $this->post->id);
			if ($this->post->PossibleExtraSections())
			{	$menu['sections'] = array('text'=>'Extra sections', 'link'=>'postsections.php?id=' . $this->post->id);
			}
			$menu['posts'] = array('text'=>'Featured Posts', 'link'=>'postposts.php?id=' . $this->post->id);
			if ($this->post->GetParentPosts())
			{	$menu['parents'] = array('text'=>'Parent Posts', 'link'=>'postparents.php?id=' . $this->post->id);
			}
		}
		return $menu;
	} // end of fn GetPostMenu
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('posts'))
		{	$this->AdminPostsBody();
		}
	} // end of fn AdminBodyMain
	
} // end of defn AdminPostsPage
?>
