<?php
class CampaignDonation extends BlankItem
{	public $campaign = array();
	public $team = array();
	public $extrafields = array();

	public function __construct($id = 0)
	{	parent::__construct($id, 'campaigndonations', 'cdid');
	} // fn __construct

	public function ID()
	{	if ($this->id)
		{	return 'CR_STARS_' . str_pad($this->id, 7, '0', STR_PAD_LEFT);
		}
	} // fn ID

	public function Status()
	{	return $this->details['donationref'] ? 'COMPLETED' : 'PENDING';
	} // fn Status

	public function ResetExtra()
	{	$this->campaign = array();
		$this->team = array();
	} // fn ResetExtra

	public function GetExtra()
	{	if ($cid = (int)$this->details['campaignid'])
		{	$sql = 'SELECT campaigns.* FROM campaigns WHERE cid=' . $cid;
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->campaign = $row;
				}
			}
			if ($teamid = (int)$this->details['teamid'])
			{	if ($cid != $teamid)
				{	$sql = 'SELECT campaigns.* FROM campaigns WHERE cid=' . $teamid;
					if ($result = $this->db->Query($sql))
					{	if ($row = $this->db->FetchArray($result))
						{	$this->team = $row;
						}
					}
				}
			}
		}
		if ($this->id)
		{	$sql = 'SELECT campaigndonation_extras.* FROM campaigndonation_extras WHERE cdid=' . $this->id;
			$this->extrafields = array();
			if ($result = $this->db->Query($sql))
			{	while ($row = $this->db->FetchArray($result))
				{	$this->extrafields[$row['extrafield']] = $row['extravalue'];
				}
			}
		}
	} // fn GetExtra

	public function Create($data = array(), Campaign $campaign)
	{
		$fail = array();
		$success = array();
		$fields = array('donated'=>'donated="' . $this->datefn->SQLDateTime() . '"', 'gateway'=>'gateway="worldpay"');

		if ($this->GetCurrency($data['donation_currency']) && ($currency = $data['donation_currency']))
		{	$fields['currency'] = 'currency="' . $currency . '"';
		} else
		{	$fail[] = 'Currency missing';
		}

		if (($amount = round($data['donation_amount'], 2)) > 0)
		{	$fields['amount'] = 'amount=' . $amount;
			if ($currency)
			{	if ($currency == 'GBP')
				{	$fields['gbpamount'] = 'gbpamount=' . $amount;
				} else
				{	$fields['gbpamount'] = 'gbpamount=' . round($amount / $this->GetCurrency($currency, 'convertrate'), 2);
				}
			}
		} else
		{	$fail[] = 'Amount missing';
		}

		if (is_a($campaign, 'Campaign') && $campaign->id)
		{	$fields['campaignid'] = 'campaignid=' . $campaign->id;
			if ($amount && $currency)
			{	if ($campaign->details['currency'] == $currency)
				{	$fields['campaignamount'] = 'campaignamount=' . $amount;
				} else
				{	$fields['campaignamount'] = 'campaignamount=' . round(($amount * $this->GetCurrency($campaign->details['currency'], 'convertrate')) / $this->GetCurrency($currency, 'convertrate'), 2);
				}
			}
			if ($teamid = (int)$campaign->team['cid'])
			{	$fields['teamid'] = 'teamid=' . $teamid;
				if ($amount && $currency)
				{	if ($campaign->team['currency'] == $currency)
					{	$fields['teamamount'] = 'teamamount=' . $amount;
					} else
					{	$fields['teamamount'] = 'teamamount=' . round(($amount * $this->GetCurrency($campaign->team['currency'], 'convertrate')) / $this->GetCurrency($currency, 'convertrate'), 2);
					}
				}
			}
		} else
		{	$fail[] = 'Campaign missing';
		}
		$fields['donorcomment'] = 'donorcomment="' . $this->SQLSafe($data['donor_comment']) . '"';

		if ($donortitle = $this->SQLSafe($data['donor_title']))
		{	$fields['donortitle'] = 'donortitle="' . $donortitle . '"';
		} else
		{	$fail[] = 'your title is missing';
		}
		if ($donorfirstname = $this->SQLSafe($data['donor_firstname']))
		{	$fields['donorfirstname'] = 'donorfirstname="' . $donorfirstname . '"';
		} else
		{	$fail[] = 'first name missing';
		}
		if ($donorsurname = $this->SQLSafe($data['donor_lastname']))
		{	$fields['donorsurname'] = 'donorsurname="' . $donorsurname . '"';
		} else
		{	$fail[] = 'surname missing';
		}

		if ($data['donor_country'] && $this->GetCountry($data['donor_country']))
		{	$fields['donorcountry'] = 'donorcountry="' . $data['donor_country'] . '"';
			if (($data['donor_country'] == 'GB') && $data['donor_giftaid'])
			{	$fields['giftaid'] = 'giftaid=1';
			}
			$add_prefix = 'pca_';
		} else
		{	$fail[] = 'country missing';
		}

		if ($donoradd3 = $this->SQLSafe($data[$add_prefix . 'donor_address3']))
		{	$donoradd1lines = array();
			if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
			{	$donoradd1lines[] = $donoradd1;
			}
			if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
			{	$donoradd1lines[] = $donoradd2;
			}
			if ($donoradd1lines)
			{	$fields['donoradd1'] = 'donoradd1="' . implode(', ', $donoradd1lines) . '"';
			}
			$fields['donoradd2'] = 'donoradd2="' . $donoradd3 . '"';
			$address_done++;
		} else
		{	if ($donoradd1 = $this->SQLSafe($data[$add_prefix . 'donor_address1']))
			{	$fields['donoradd1'] = 'donoradd1="' . $donoradd1 . '"';
				$address_done++;
			}
			if ($donoradd2 = $this->SQLSafe($data[$add_prefix . 'donor_address2']))
			{	$fields['donoradd2'] = 'donoradd2="' . $donoradd2 . '"';
				$address_done++;
			}
		}

		if (!$address_done)
		{	$fail[] = 'address missing';
		}

		if ($donorpostcode = $this->SQLSafe($data[$add_prefix . 'donor_postcode']))
		{	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
		} else
		{	$fail[] = 'post code missing';
		}
		if ($donorcity = $this->SQLSafe($data[$add_prefix . 'donor_city']))
		{	$fields['donorcity'] = 'donorcity="' . $donorcity . '"';
		} else
		{	$fail[] = 'city/name missing';
		}

		$fields['zakat'] = 'zakat=' . ($data['donation_zakat'] ? 1 : 0);
		$fields['namehidden'] = 'namehidden=' . ($data['donation_namehidden'] ? 1 : 0);

		if ($donoremail = $this->SQLSafe($data['donor_email']))
		{	$fields['donoremail'] = 'donoremail="' . $donoremail . '"';
		} else
		{	$fail[] = 'email missing';
		}
		$fields['donorphone'] = 'donorphone="' . $this->SQLSafe($data['donor_phone']) . '"';
		$fields['donoranon'] = 'donoranon=' . ($data['donor_anon'] ? 1 : 0);

		if (!$fail && $set = implode(', ', $fields))
		{	$sql = 'INSERT INTO campaigndonations SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
				{	if ($this->Get($id))
					{	$success[] = 'Donation has been created';
						$this->RecordUTMData();
					}
				}
			} else $fail[] = $sql . ': ' . $this->db->Error();
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn Create

	protected function RecordUTMData()
	{	if (is_array($_SESSION['utm']) && $_SESSION['utm'])
		{	foreach ($_SESSION['utm'] as $name=>$value)
			{	$extras_sql = 'INSERT INTO campaigndonation_extras SET cdid=' . $this->id  . ', extrafield="' . $this->SQLSafe($name) . '", extravalue="' . $this->SQLSafe($value) . '"';
				$this->db->Query($extras_sql);
			}
		}
	} // end of fn RecordUTMData

	public function CanBePaid()
	{	return $this->id && !$this->details['gatewayref'];
	} // fn CanBePaid

	public function PaymentButton()
	{	ob_start();
		$wp = new WorldPayRedirect();
		echo $wp->CRStarsDonationButton($this);
		return ob_get_clean();
	} // end of fn PaymentButton

	public function UpdateFromWorldPayPost($post = array())
	{	$fail = array();
		$success = array();

		$subfields = array('gateway="worldpay"');

		if ($this->id)
		{	if ($this->details['donationref'])
			{	$fail[] = 'donation already paid';
			} else
			{	$ppsubid = 'WP~' . $post['transId'];
				$subfields[] = 'donationref="' . $ppsubid . '"';
				$subfields[] = 'donationconfirmed="' . $this->datefn->SQLDateTime() . '"';

				$fullamount = round($post['amount'], 2);
				if ($fullamount != ($this->details['amount']))
				{	$fail[] = 'amount/mc_gross doesn\'t match (' . $this->details['amount'] . ':' . $post['amount'] . ')';
				}

				if (!$fail)
				{	$subset = implode(', ', $subfields);
					$subsql = 'UPDATE campaigndonations SET ' . $subset . ' WHERE cdid=' . $this->id;
					if (($result = $this->db->Query($subsql)) && $this->db->AffectedRows())
					{	$success[] = 'crstars donation ' . $this->id . ' updated';
						$this->Get($this->id);
						$this->SendDonationEmail();
					} else $fail[] = $subsql . '---' . $this->db->Error();
				}
			}
		} else
		{	$fail[] = 'crstars donation not found';
		}
		//return array();
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn UpdateFromWorldPayPost

	public function SendDonationEmail()
	{	$campaign = new Campaign($this->campaign);
		$mail = new HTMLMail();
		if ($this->ValidEMail($this->details['donoremail']))
		{	ob_start();
			if ($this->details['donorfirstname'])
			{	echo '<p>Dear ', $this->InputSafeString($this->details['donorfirstname']), '</p>';
			}
			echo '<p>Thank you for your donation of ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['amount'], 2), ' to ', strip_tags($campaign->FullTitle()), '. Your donation is on it\'s way to help needy people.</p>';
			if ($this->details['zakat'])
			{	echo '<p>We appreciate that this is a Zakat donation. We\'d like to reassure you that this donation will be distributed in line with the Islamic principles of Zakat.</p>';
			}
			if ($this->details['giftaid'])
			{	echo '<p>Thank you for confirming that your donation is eligible for Gift Aid. This means that we can claim an extra 25% from the government for this great cause.</p>';
			}
			$body = ob_get_clean();
			$mail->SetSubject('Thank you for your donation to Charity Right');
			$mail->SendFromTemplate($this->details['donoremail'], array('main_content'=>$body), 'default');
		}
		// check campaign target reached
		if ($campaign->GetDonationTotal() > $campaign->details['target'])
		{	if (!$campaign->details['targetemailsent'])
			{	$campaign->SendTargetReachedEMail();
			}
		} else
		{	if ($campaign->details['targetemailsent'])
			{	// then reset this
				$this->db->Query('UPDATE campaigns SET targetemailsent=0 WHERE cid=' . $campaign->id);
			}
		}

		// check team target reached (if appropriate)
		if ($this->team && ($team = new Campaign($this->team)) && $team->id)
		{	if ($team->GetDonationTotal() > $team->details['target'])
			{	if (!$team->details['targetemailsent'])
				{	$team->SendTargetReachedEMail();
				}
			} else
			{	if ($team->details['targetemailsent'])
				{	// then reset this
					$this->db->Query('UPDATE campaigns SET targetemailsent=0 WHERE cid=' . $team->id);
				}
			}
		}
	} // end of fn SendDonationEmail

} // end of defn CampaignDonation
?>