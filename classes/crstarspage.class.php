<?php
class CRStarsPage extends BasePage
{
	public function __construct()
	{	parent::__construct('cr-stars');
		$this->css[] = 'cr-stars.css';
		$this->js[] = 'cr-stars.js';
	} // end of fn __construct

	function MainBodyContent()
	{
	} // end of fn MemberBody

	protected function HeaderLinks()
	{	ob_start();
		$cart = new Cart();
		$basket = $cart->PopUpBasket();
		echo '<div class="header_rightlinks hidden-xs"><ul>
			<li class="hrLinksCart">', $basket['desktop'], '</li>', $this->HeaderLinksSearch();
		if ($this->camp_customer->id)
		{	echo '<li><a href="', SITE_SUB, '/my-cr-stars/" title="my crstars"><i class="icon icon-user"></i></a></li>
				<li><a href="', SITE_SUB, '/cr-stars/log-out/" title="log out"><i class="icon icon-logout"></i></a></li>';
		} else
		{	echo '<li><a href="', SITE_SUB, '/cr-stars/register/" title="register"><i class="icon icon-user-plus"></i></a></li>
					<li><a href="', SITE_SUB, '/cr-stars/log-in/" title="log in"><i class="icon icon-login"></i></a></li>';
		}
		echo '</ul></div>';
		return ob_get_clean();
	} // end of fn HeaderLinks

	protected function HeaderLinksSearch()
	{	ob_start();
		echo '<li class="hrlSearch"><a onclick="CRSearchOpen();" title="search"><i class="icon icon-search"></i></a>
				<div class="hrlSearchPopup">
					<form id="crSearchMainBar" method="get" action="', SITE_URL, 'cr-stars/search/" onsubmit="return CRSearchCanSubmit(\'crSearchMainBar\');">
						<input type="text" placeholder="Search CR Stars Campaigns..." name="crs_search" value="', $this->InputSafeString($_GET['crs_search']), '" /><button type="submit"><i class="icon icon-search"></i></button>
					<a class="hrlSearchPopupCloser" onclick="CRSearchClose();"><i class="icon icon-cancel"></i></a>
					</form>
				</div>
			</li>';
		return ob_get_clean();
	} // end of fn HeaderLinksSearch

	protected function MobileExtraLinksTop()
	{	ob_start();
		echo '<li class="hrlSearch"><span><form id="crSearchSideBar" method="get" action="', SITE_URL, 'cr-stars/search/" onsubmit="return CRSearchCanSubmit(\'crSearchSideBar\');"><button type="submit"><i class="icon icon-search"></i></button><input type="text" name="crs_search" value="', $this->InputSafeString($_GET['crs_search']), '" placeholder="Search CR Stars Campaigns..." /></form></span></li>';
		return ob_get_clean();
	}

	protected function MobileExtraLinks()
	{	ob_start();

		if ($this->camp_customer->id)
		{	echo '<li><a href="', SITE_SUB, '/my-cr-stars/" title="my crstars"><i class="icon icon-user"></i></a></li>
				<li><a href="', SITE_SUB, '/cr-stars/log-out/" title="log out"><i class="icon icon-logout"></i></a></li>';
		} else
		{	echo '<li><a href="', SITE_SUB, '/cr-stars/register/" title="register"><i class="icon icon-user-plus"></i></a></li>
					<li><a href="', SITE_SUB, '/cr-stars/log-in/" title="log in"><i class="icon icon-login"></i></a></li>';
		}
		return ob_get_clean();
	} // end of fn MobileExtraLinks

	protected function RegisterForm($data = array(), $team = false)
	{	ob_start();
		$pref = new ContactPreferences(array('email', 'phone', 'text'));
		$pp_page = new PageContent('use-of-cookies');
		echo '<form action="" method="post" class="form-rows">
				<p><input type="text" name="firstname" value="', $this->InputSafeString($data['firstname']), '" placeholder="First Name" required="required" /></p>
				<p><input type="text" name="lastname" value="', $this->InputSafeString($data['lastname']), '" placeholder="Surname" required="required" /></p>
				<p><input type="tel" name="phone" value="', $this->InputSafeString($data['phone']), '" placeholder="Phone" pattern="', $this->ValidPhoneNumberPatternInner(), '" title="Please enter a valid phone number" required="required" /></p>
				<p><input type="email" name="email" value="', $this->InputSafeString($data['email']), '" placeholder="Email" required="required" /></p>
				<p><input type="password" name="password" value="" placeholder="Password" pattern="', $this->AcceptablePWPatternInner(), '" title="Please enter a password of between 8 and 20 letters and numbers" required="required" /></p>';
		if (true || SITE_TEST)
		{	echo '<div class="form-row crRegFormContactPref">
					<p class="cprefTitle">How would you like to receive updates?<br /><span>(Optional)</span></p>
					<p class="cprefHelp">We\'d love to tell you about how your donation helps and keep you in the loop with what Charity Right is up to. Your details will be kept safe and never shared with other organisations. See our <a href="', $pp_page->Link(), '">privacy policy</a>.</p>
					', $pref->FormElementsList($_POST['contactpref']), '
					<p class="cprefHelp">Don\'t forget, you can change your preferences at any time by emailing us at info@charityright.org.uk or in your profile after you have created your campaign.</p>
				</div>
				';
		}
		if (false && SITE_TEST)
		{	echo '<div class="form-row crRegFormContactPref">
					<div class="form-col form-col-4">
						<div class="form-col-inner"><label class="title-label">How would you like to receive updates?<br /><span style="font-style: italic;">(Optional)</span></label></div>
					</div>
					<div class="form-col form-col-75">
						<div class="form-col-inner">', $pref->FormElementsList($_POST['contactpref']), '</div>
					</div>
				</div>
				<p class="cprefHelp">We\'d love to tell you about how your donation helps and keep you in the loop with what Charity Right is up to. Your details will be kept safe and never shared with other organisations. See our <a href="', $pp_page->Link(), '">privacy policy</a>.</p>
				<p class="cprefHelp">Don\'t forget, you can change your preferences at any time by emailing us at info@charityright.org.uk or in your profile after you have created your campaign.</p>';
		}
		echo '<p class="regformButtons"><button type="submit">SIGN UP</button></p>
			</form>

			<p class="registerFormLogin">Already a CR STAR? <a href="', $team->id ? $team->LoginLink() : (SITE_URL . 'cr-stars/log-in/'), '">Log in now!</a></p>
		';
		return ob_get_clean();
	} // end of fn RegisterForm

	protected function GetCurrencies($type = '')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies

	protected function NewCampaignForm($data = array(), $team = false)
	{	ob_start();
		if (!$current_campname = $data['campname'])
		{	if ($team->id)
			{	$current_campname = $team->details['campname'];
			}
		}
		echo '<form class="newCampaignForm" action="" method="post" enctype="multipart/form-data">';

		echo '<div class="form-row">';
			echo '<div class="form-row-inner">';
				echo '<div class="form-row-half">';
					echo '<label for="campname">Campaign Name</label>';
				echo '</div>';
				echo '<div class="form-row-half">';
					echo '<input type="text" id="campname" name="campname" value="', $this->InputSafeString($current_campname), '" required />';
				echo '</div>';
			echo '<div class="clear"></div></div>';
		echo '</div>';

		echo '<div class="form-row">';
			echo '<div class="form-row-inner">';
				echo '<div class="form-row-half">';
					echo '<label for="campcurrency">Currency</label>';
				echo '</div>';
				echo '<div class="form-row-half ">';
					echo '<div class="form-select-container">';
					echo '<select id="campcurrency" name="campcurrency" onchange="CRRegCurrencyChange();" required><option value="">-- Choose Campaign Currency --</option>';
					if (!$current_currency = $data['campcurrency'])
					{	if ($team->id)
						{	$current_currency = $team->details['currency'];
						}
					}
					foreach ($this->GetCurrencies('campaigns') as $curcode=>$currency)
					{	echo '<option value="', $curcode, '"', $curcode == $current_currency ? ' selected="selected"' : '', '>', $this->InputSafeString($currency['curname']), ' - ', $currency['cursymbol'], '</option>';
					}
					echo '</select>';
					echo '</div>';
				echo '</div>';
			echo '<div class="clear"></div></div>';
		echo '</div>';

		echo '<div class="form-row">';
			echo '<div class="form-row-inner">';
				echo '<div class="form-row-half">';
					echo '<label for="camptarget">How much would you like to raise?</label>';
				echo '</div>';
				echo '<div class="form-row-half">';
					echo '<div class="campCurrencySpanContainer">';
					echo '<span class="campCurrencySpan">';
					if ($current_currency) {
						echo $this->GetCurrency($current_currency, 'cursymbol');
					}
					echo '</span>';
					echo '<input type="number" id="camptarget" name="camptarget" class="number" value="', (int)$data['camptarget'], '" required />';
					echo '</div>';
				echo '</div>';
			echo '<div class="clear"></div></div>';
		echo '</div>';

		echo '<div class="form-row">';
			echo '<div class="form-row-inner">';
				echo '<div class="form-row-half">';
					echo '<label for="">Campaign Summary</label>';
					echo '<p>Write your own summary or choose from the already written options</p>';
				echo '</div>';
				echo '<div class="form-row-half">';
					if ($sampletext = $this->GetCampaignSampleText()){
					echo '<script type="text/javascript">var crsSamples = new Array();</script>';
					echo '<ul class="crstarsRegSampleText radio-list">';
					$samplecount = 0;
					echo '<li><input type="radio" name="sample_chosen" id="sample_chosen_99999" value="99999" onclick="CRRegSampleChooser();" checked="checked"><label for="sample_chosen_99999">Create my own summary</label><div class="clear"></div></li><script type="text/javascript">crsSamples[99999] = " ";</script>';
					foreach($sampletext as $sampleid=>$sampletext_row){
						echo '<li><input type="radio" name="sample_chosen" id="sample_chosen_', $sampleid, '" value="', $samplecount, '" onclick="CRRegSampleChooser();"><label for="sample_chosen_', $sampleid, '">', $this->InputSafeString($sampletext_row['ctname']), '</label><div class="clear"></div><script type="text/javascript">crsSamples[', $samplecount, ']="', $this->InputSafeString($sampletext_row['cttext']), '";</script></li>';
						$samplecount++;
					}
					echo '</ul>';
					}
				echo '</div>';
				echo '<div class="clear"></div>';
				echo '<div class="form-row-full">';
				echo '<textarea name="description" onkeyup="$(\'.crstarsRegSampleText input[type=radio]\').prop(\'checked\', false);$(\'#sample_chosen_99999\').prop(\'checked\', true);" placeholder="Campaign Summary" required>', $this->InputSafeString($data['description']), '</textarea>';
				echo '</div>';
			if (!$team->id)
			{	$donation = new Donation();
				if ($countries = $donation->GetDonationCountries('oneoff', true))
				{	echo '<div class="clear"></div><div class="form-row-half">
								<label for="">Which causes would you like to support?</label>
							</div>
							<div class="form-row-half"><div class="form-select-container"><select id="donation_country" name="donation_country" onchange="CRRegCountryChange();">';
					foreach ($countries as $country=>$cdetails)
					{	if (!$data['donation_country'])
						{	$data['donation_country'] = $country;
						}
						echo '<option value="', $this->InputSafeString($country), '"', ($country == $data['donation_country']) ? ' selected="selected"' : "", '>', $this->InputSafeString($cdetails['shortname']), '</option>';
					}
					echo '</select></div></div>';
					echo '<div id="countryProjectContainer">', $this->CountryProjectsList($data['donation_country'], $data['donation_project']), '</div>';
				}
			}
			echo '<div class="clear"></div>';
		echo '</div>';

		echo '<div class="form-row">';
			echo '<div class="form-row-inner">';
				echo '<div class="form-row-half">';
					echo '<label for="">Campaign Picture</label>';
					echo '<p>Upload your own picture or choose one from our bank of images</p>';
				echo '</div>';
				echo '<div class="form-row-half">';
					echo '<ul class="radio-list">';
					echo '<li><input type="radio" name="campaign_picture_choosen" id="campaign_picture_choosen_1" value="campaign_picture_choosen_1" onclick="$(\'.campaign_picture_choosen_2_container\').hide();$(\'.campaign_picture_choosen_1_container\').slideDown();" checked="checked"><label for="campaign_picture_choosen_1">Upload my own picture</label><div class="clear"></div></li>';
					echo '<li><input type="radio" name="campaign_picture_choosen" id="campaign_picture_choosen_2" value="campaign_picture_choosen_2" onclick="$(\'.campaign_picture_choosen_1_container\').hide();$(\'.campaign_picture_choosen_2_container\').slideDown();$(\'#campimage\').val(\'\');"><label for="campaign_picture_choosen_2">Choose an image</label><div class="clear"></div></li>';
					echo '</ul>';
				echo '</div>';
				echo '<div class="clear"></div>';
				echo '<div class="form-row-full campaign_picture_choosen_1_container">';
					echo '<div class="input-box-container">';
					echo '<input type="file" id="campimage" name="campimage" onchange="" />';
					echo '</div>';
				echo '</div>';
				echo '<div class="clear"></div>';
				if ($avatars = $this->GetCampaignAvatarsAvailable()){
				echo '<div class="form-row-full campaign_picture_choosen_2_container" style="overflow: hidden; display: none;">';
					echo '<div class="input-box-container">';
					echo '<ul class="campaign-avatars">';
					foreach ($avatars as $avatar_row) {
						$avatar = new CampaignAvatar($avatar_row);
						echo '<li id="crAvatar_', $avatar->id, '" class="', $data['avatarid'] == $avatar->id ? 'crAvatarSelected' : '', '">
						<a href="#crAvatar_', $avatar->id, '" onclick="CRRegAvatarChooser(', $avatar->id, ');return false;">
						<img src="', $avatar->GetImageSRC('small'), '" alt="', $alt = $this->InputSafeString($avatar->details['imagedesc']), '" title="', $alt, '" />
						</a>
						</li>';
					}
					echo '</ul>';

					echo '<input type="hidden" name="avatarid" value="', (int)$data['avatarid'], '" />';
					echo '<div class="clear"></div>';
					echo '</div>';
				echo '</div>';
				}
			echo '<div class="clear"></div></div>';
		echo '</div>';

		if (!$team->id) {
		echo '<div class="form-row">';
			echo '<div class="form-row-inner">';
				echo '<div class="form-row-half">';
					echo '<label for="">Type Of Campaign</label>';
					echo '<p>Will you be campaigning on your own or within a team?</p>';
				echo '</div>';
				echo '<div class="form-row-half">';
					echo '<ul class="radio-list">';
						echo '<li><input type="radio" name="isteam" id="isteam_0" value="0" ', $data['isteam'] ? '' : ' checked="checked"', '><label for="isteam_0">Individual Campaign</label><div class="clear"></div></li>';
						echo '<li><input type="radio" name="isteam" id="isteam_1" value="1" ', $data['isteam'] ? ' checked="checked"' : '', '><label for="isteam_1">Team Campaign</label><div class="clear"></div></li>';
					echo '</ul>';
				echo '</div>';
			echo '<div class="clear"></div></div>';
		echo '</div>';
		}

		echo '<div class="form-row crRegFormContactPref"><div class="form-row-inner"><div class="form-row-full"><div class="custom-checkbox clearfix"><input type="checkbox" name="helpfromcr" id="helpfromcr" value="1" ', $data['helpfromcr'] ? 'checked="checked" ' : '', '/><label for="helpfromcr">I\'d like Charity Right to help me with my fundraising</label></div></div></div></div>';

		echo '<div class="form-row form-row-no-border">';
			echo '<div class="form-row-full" style="text-align:center;">';

				echo '<input type="submit" class="newCampaignFormSubmit" value="Next" />';

			echo '<div class="clear"></div></div>';
		echo '</div>';

		if (isset($data['password'])){
			echo '<input type="hidden" name="password" value="', $this->InputSafeString($data['password']), '" />';
			echo '<input type="hidden" name="email" value="', $this->InputSafeString($data['email']), '" />';
			echo '<input type="hidden" name="firstname" value="', $this->InputSafeString($data['firstname']), '" />';
			echo '<input type="hidden" name="lastname" value="', $this->InputSafeString($data['lastname']), '" />';
			echo '<input type="hidden" name="phone" value="', $this->InputSafeString($data['phone']), '" />';
			echo '<input type="hidden" name="contactpref" value="', (int)$data['contactpref'], '" />';
		}
		echo '</form>';
		return ob_get_clean();
	} // end of fn NewCampaignForm

	public function CountryProjectsList($country = '', $donation_project = '')
	{	ob_start();
		$donation = new Donation();
		$countries = $donation->GetDonationCountries('crstars', true);
		if ($projects = $countries[$country]['projects'])
		{	if (false && (count($projects) == 1))
			{	foreach ($projects as $project=>$details)
				{	echo '<input type="hidden" id="donation_project" name="donation_project" value="', $project, '" />';
				}
			} else
			{	echo '<div class="clear"></div><div class="form-row-half">
							<label for="">', $this->InputSafeString($countries[$country]['formlabelcrstars']), '</label>
						</div><div class="form-row-half"><div class="form-select-container"><select id="donation_project" name="donation_project">';
				foreach ($projects as $project=>$details)
				{	echo '<option value="', $project, '"', $project == $donation_project ? ' selected="selected"' : '', '>', $this->InputSafeString($details['projectname']), '</option>';
				}
				echo '</select></div></div>';
			}
		}
		return ob_get_clean();
	} // end of fn CountryProjectsList

	protected function CampaignLoginForm()
	{	ob_start();
		echo '<div class="container crstarsRegister crstarsLogin"><div class="container_inner">
			<h2>LOG IN</h2>
			<h3>Log in into your CR STARS account.</h3>
			<form action="" method="post">
			<p><input type="email" name="campusername" value="', $this->InputSafeString($_POST['campusername']), '" placeholder="Email" title="Your username is the email address you gave us when you registered" required /></p>
			<p><input type="password" name="camppass" value="" placeholder="Password" required /></p>
				<p class="crstarsRegisterCheckbox crstarsRegisterKeepMe"><input type="checkbox" id="keeploggedin" name="keeploggedin" value="1" /><label for="keeploggedin">Remember me</label></p>
				<p class="regformButtons"><button type="submit">Log in</button></p>
				<p class="regformForgotLink"><a href="', SITE_URL, 'cr-stars/forgot-password/">Forgot your password?</a></p>
			</form>
			<p class="registerFormLogin">Not yet a CR STAR? <a href="', $team->id ? $team->LoginLink() : (SITE_URL . 'cr-stars/register/'), '">Sign up now!</a></p>
			</div></div>';
		return ob_get_clean();
	} // end of fn CampaignLoginForm

	public function MainCampaignsList($campaigns = array(), $start = 0, $end = 0)
	{	ob_start();
		echo '<ul class="campaignsList campaignsListMain">';
		foreach ($campaigns as $campaign_row)
		{	if (++$count > $start)
			{	if ($count <= $end)
				{
					$campaign = new Campaign($campaign_row);
					$raised = $campaign->GetDonationTotal();
					$cursymbol = $this->GetCurrency($campaign->details['currency'], 'cursymbol');
					$link = $campaign->Link();
					if ($campaign->owner)
					{	$owner = new CampaignUser($campaign->owner);
						$name_snippet = substr($fullname = $owner->FullName(), 0, 50);
						if (strlen($fullname) > 50)
						{	$name_snippet .= ' ...';
						}
					}
					
					$buttons = array();
					
					if ($campaign->team)
					{	$team = new Campaign($campaign->team);
						//$buttons[] = '<a href="' . $team->Link() . '">View Team</a>';
						if ($team->CanBeJoined($this->camp_customer->id))
						{	ob_start();
							echo '<a href="', $this->camp_customer->id ? $team->JoinLink() : $team->RegisterLink(), '">Join Team</a>';
							$buttons[] = ob_get_clean();
						}
					} else
					{	if ($campaign->CanBeJoined($this->camp_customer->id))
						{	ob_start();
							echo '<a href="', $this->camp_customer->id ? $campaign->JoinLink() : $campaign->RegisterLink(), '">Join Team</a>';
							$buttons[] = ob_get_clean();
						}
					}
					
					ob_start();
					echo '<a href="', $link, '">View</a>';
					$buttons[] = ob_get_clean();
					
					echo '<li>
						<div class="clRightTop"><h2><a href="', $link, '">', $fulltitle = $campaign->FullTitle(), '</a></h2><p>', $textsnippet = $campaign->TextSnippet(), '</p></div>
						<div class="clListTopMobile visible-xs">';
					if ($campaign->owner)
					{	echo '<a class="clOwnerLink" href="', $owner->Link(), '"><span>';
						if ($src = $owner->GetImageSRC('thumb'))
						{	echo '<img src="', $src, '" alt="', $fullname, '" title="', $fullname, '" />';
						}
						echo '</span>', $name_snippet, '</a>';
					} else
					{	echo '<span class="clCROwned">Campaign by Charity Right</span>';
					}
					echo '<div class="crListShare">

						<div class="custom-share-buttons">
							SHARE
							<div class="ssk-group ssk-xs" data-url="', $link, '" data-title="', strip_tags($fulltitle), '">
								<a href="" class="ssk ssk-icon ssk-facebook"></a>
								<a href="" class="ssk ssk-icon ssk-twitter"></a>
							</div>
							<div class="clear"></div>
						</div>
						
					</div><div class="clear"></div></div>
					<div class="clear visible-xs"></div>
					
					<a href="', $link, '" class="clLeft"><div class="clLeftTop"';
					if ($image = $campaign->GetImageSRC('medium'))
					{	echo ' style="background-image: url(\'', $image, '\');"';
					} else
					{	if ($avatar = $campaign->GetAvatarSRC('medium'))
						{	echo ' style="background-image: url(\'', $avatar, '\');"';
						}
					}
					echo '><div class="clLeftTopPercentage">', $raised > $campaign->details['target'] ? '100' : number_format((100 * $raised) / $campaign->details['target']), '%</div></div><div class="clLeftBottom"><span class="cclRaised">', $cursymbol, number_format($raised), '</span> out of ', $cursymbol, number_format($campaign->details['target']), '</div></a>
					
						<div class="clRightButtons clRightButtons_', count($buttons), '">', implode('', $buttons), '</div>
						
						<div class="clRightBottom hidden-xs">';
					if ($campaign->owner)
					{	echo '<a class="clOwnerLink" href="', $owner->Link(), '"><span>';
						if ($src = $owner->GetImageSRC('thumb'))
						{	echo '<img src="', $src, '" alt="', $fullname, '" title="', $fullname, '" />';
						}
						echo '</span>', $name_snippet, '</a>';
					} else
					{	echo '<span class="clCROwned">Campaign by Charity Right</span>';
					}
					echo '<div class="crListShare">

						<div class="custom-share-buttons">
							SHARE
							<div class="ssk-group ssk-xs" data-url="', $link, '" data-title="', strip_tags($fulltitle), '">

								<a href="" class="ssk ssk-icon ssk-facebook"></a>
								<a href="" class="ssk ssk-icon ssk-twitter"></a>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div></li>';
				} else
				{	break;
				}
			}
		}
		echo '</ul>';
		return ob_get_clean();
	} // end of fn MainCampaignsList

	public function xxxxMainCampaignsList($campaigns = array(), $start = 0, $end = 0)
	{	ob_start();
		echo '<ul class="campaignsList">';
		foreach ($campaigns as $campaign_row)
		{	if (++$count > $start)
			{	if ($count <= $end)
				{
					$campaign = new Campaign($campaign_row);
					$raised = $campaign->GetDonationTotal();
					$cursymbol = $this->GetCurrency($campaign->details['currency'], 'cursymbol');
					echo '<li><div class="clRight"><div class="clRightTop"><h2><a href="', $link = $campaign->Link(), '">', $fulltitle = $campaign->FullTitle(), '</a></h2><p class="hidden-xs">', $textsnippet = $campaign->TextSnippet(), '</p></div><div class="clRightBottom">';
					if ($campaign->owner)
					{	$owner = new CampaignUser($campaign->owner);
						$fullname = $owner->FullName();
						echo '<a class="clOwnerLink" href="', $owner->Link(), '"><span>';
						if ($src = $owner->GetImageSRC('thumb'))
						{	echo '<img src="', $src, '" alt="', $fullname, '" title="', $fullname, '" />';
						}
						echo '</span>', $fullname, '</a>';
					} else
					{	echo '<span class="clCROwned">Campaign by Charity Right</span>';
					}
					echo '<div class="crListShare">

						<div class="custom-share-buttons">
							SHARE
							<div class="ssk-group ssk-xs" data-url="', $link, '" data-title="', strip_tags($fulltitle), '">

								<a href="" class="ssk ssk-icon ssk-facebook"></a>
								<a href="" class="ssk ssk-icon ssk-twitter"></a>
							</div>
						</div>
					</div><div class="clear"></div>
					</div></div><a href="', $link, '" class="clLeft"><div class="clLeftTop"';
					if ($image = $campaign->GetImageSRC('medium'))
					{	echo ' style="background-image: url(\'', $image, '\');"';
					} else
					{	if ($avatar = $campaign->GetAvatarSRC('medium'))
						{	echo ' style="background-image: url(\'', $avatar, '\');"';
						}
					}
					echo '><div class="clLeftTopPercentage">', $raised > $campaign->details['target'] ? '100' : number_format((100 * $raised) / $campaign->details['target']), '%</div></div><div class="clLeftBottom"><span class="cclRaised">', $cursymbol, number_format($raised), '</span> out of ', $cursymbol, number_format($campaign->details['target']), '</div></a>
					<p class="clMobileTextSnippet visible-xs">', $textsnippet, '</p>
					<div class="clear"></div></li>';
				} else
				{	break;
				}
			}
		}
		echo '</ul>';
		return ob_get_clean();
	} // end of fn xxxxMainCampaignsList

	protected function SideListCampaignDisplay(Campaign $campaign, $my_campaign = false, $hide_name = false)
	{	ob_start();
		$campaign->raised = $campaign->GetDonationTotal();
		$cursymbol = $this->GetCurrency($campaign->details['currency'], 'cursymbol');
		echo '<li><div class="clRight"><div class="clRightTop"><h2><a href="', $campaign->Link(), '">', $campaign->FullTitle(), '</a></h2><p>', $campaign->TextSnippet(), '</p>';
		if ($my_campaign)
		{	echo '<a class="clrtEditLink" href="', $campaign->EditLink(), '">edit</a>';
		}
		echo '</div><div class="clRightBottom">';
		if (!$hide_name)
		{	if ($campaign->owner)
			{	$owner = new CampaignUser($campaign->owner);
				echo '<a class="clOwnerLink" href="', $owner->Link(), '"><span>';
				if ($src = $owner->GetImageSRC('thumb'))
				{	echo '<img src="', $src, '" alt="', $fullname, '" title="', $fullname, '" />';
				}
				echo '</span>', $owner->FullName(), '</a>';
			} else
			{	echo '<span class="clCROwned">Campaign by Charity Right</span>';
			}
		}
		echo '<div class="crListShare"><div class="custom-share-buttons">
						SHARE
						<div class="ssk-group ssk-xs" data-url="'.$link.'" data-title="'.strip_tags($fulltitle).'">

							<a href="" class="ssk ssk-icon ssk-facebook"></a>
							<a href="" class="ssk ssk-icon ssk-twitter"></a>
						</div>
					</div></div></div></div><a href="', $campaign->Link(), '" class="clLeft"><div class="clLeftTop"';
		if ($image = $campaign->GetImageSRC())
		{	echo ' style="background-image: url(\'', $image, '\'); background-size: auto 100%;"';
		} else
		{	if ($avatar = $campaign->GetAvatarSRC())
			{	echo ' style="background-image: url(\'', $avatar, '\'); background-size: auto 100%;"';
			}
		}
		echo '><div class="clLeftTopPercentage">', $campaign->raised > $campaign->details['target'] ? '100' : number_format((100 * $campaign->raised) / $campaign->details['target']), '%</div></div><div class="clLeftBottom"><span class="cclRaised">', $cursymbol, number_format($campaign->raised), '</span> out of ', $cursymbol, number_format($campaign->details['target']), '</div></a><div class="clear"></div>';
		echo '</li>';
		return ob_get_clean();
	} // end of fn SideListCampaignDisplay

	protected function SideListCampaignsList($campaigns = array(), $my_campaign = false, $hide_name = false)
	{	ob_start();
		echo '<div class="campuserMainContent campuserCampaigns"><ul class="campaignsList">';
		foreach ($campaigns as $camp)
		{	echo $this->SideListCampaignDisplay(is_a($camp, 'Campaign') ? $camp : new Campaign($camp), $my_campaign, $hide_name);
		}
		echo '</ul></div>';
		return ob_get_clean();
	} // end of fn SideListCampaignDisplay

	protected function GetCampaigns()
	{	$tables = array('campaigns'=>'campaigns');
		$fields = array('campaigns.*');
		$where = array('visible'=>'campaigns.visible=1');

		$campaigns = array();
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where)))
		{	while ($row = $this->db->FetchArray($result))
			{	$row['last_donation'] = '';
				$donsql = 'SELECT MAX(campaigndonations.donated) AS last_donation FROM campaigndonations WHERE campaignid=' . $row['cid'];
				if ($donresult = $this->db->Query($donsql))
				{	if ($donrow = $this->db->FetchArray($donresult))
					{	$row['last_donation'] = $donrow['last_donation'];
					}
				}
				$campaigns[$row['cid']] = $row;
			}
		}
		uasort($campaigns, array($this, 'UASortPriorityList'));
		return $campaigns;
	} // end of fn GetCampaigns

	protected function UASortPriorityList($a, $b)
	{	if ($a['listpriority'] == $b['listpriority'])
		{	return $this->UASortLastDonation($a, $b);
		} else
		{	return $a['listpriority'] < $b['listpriority'];
		}
	} // end of fn UASortPriorityList

	protected function UASortLastDonation($a, $b)
	{	if ($a['last_donation'] == $b['last_donation'])
		{	return $a['cid'] < $b['cid'];
		} else
		{	return $a['last_donation'] < $b['last_donation'];
		}
	} // end of fn UASortLastDonation

	protected function ListCampaignDonations($donations = array(), $start = 0, $listcount = 5)
	{	ob_start();
		$cursymbols = array();
		$end = $start + $listcount;
		$is_owner = $this->camp_customer->id && ($this->camp_customer->id == $this->campaign->details['cuid']);
		foreach ($donations as $donation)
		{	if (++$count > $end)
			{	if (true || $is_owner)
				{	echo '<li class="campDonationsMore"><a class="button" onclick="CRcampaignDonationsMore(', $this->campaign->id, ',', $end, ',', $listcount, ');">Show More</a></li>';
				}
				break;
			}
			if ($count > $start)
			{	if (!$cursymbols[$donation['currency']])
				{	$cursymbols[$donation['currency']] = $this->GetCurrency($donation['currency'], 'cursymbol');
				}
				echo '<li class="campDonationsItem"><div class="campDonationsItemTitle"><span>', $cursymbols[$donation['currency']], number_format($donation['amount'], 2), '</span> from ', $this->InputSafeString((($donation['donorfirstname'] || $donation['donorsurname']) && (!$donation['donoranon']) || ($is_owner && $donation['donorshowto'])) ? trim($donation['donorfirstname'] . ' ' . $donation['donorsurname']) : 'Anonymous');
				if ($is_owner && $donation['donorshowto'] && $donation['donoremail'])
				{	echo '<br /><a href="mailto:', $donation['donoremail'], '">', $donation['donoremail'], '</a>';
				}
				echo '</div>';
				if (true)
				{	echo '<div class="campDonationsItemDate">', $this->TimefromDateDisplay($donation['donated']), '</div>';
				}
				if ($donation['donorcomment'])
				{	echo '<div class="campDonationsItemComment">"', nl2br($this->InputSafeString($donation['donorcomment'])), '"</div>';
				}
				echo '</li>';
			}
		}
		return ob_get_clean();
	} // end of fn ListCampaignDonations

	protected function CampaignListPageHeader()
	{	ob_start();
		echo '<div class="container campaignsPageHeader"><div class="container_inner"><h1>', $this->page->PageTitleDisplay(), '</h1><form action="/cr-stars/search/"><input type="text" method="get" name="crs_search" value="', $this->InputSafeString($_GET['crs_search']), '" placeholder="Search CR Stars Campaigns ..." /></form></div></div>';
		return ob_get_clean();
	} // end of fn CampaignListPageHeader

} // end of class CRStarsPage
?>