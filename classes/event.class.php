<?php
class Event extends BlankItem
{	protected $imagesizes = array('thumb'=>array('w'=>80, 'h'=>100), 'small'=>array('w'=>279, 'h'=>300), 'full'=>array('w'=>500, 'h'=>620), 'og'=>array('w'=>600, 'h'=>315));
	protected $imagelocation = '';
	protected $imagedir = '';
	
	public function __construct($id = 0)
	{	parent::__construct($id, 'events', 'eid');
		$this->imagelocation = SITE_URL . 'img/events/';
		$this->imagedir = CITDOC_ROOT . '/img/events/';
	} // fn __construct
	
	public function GetDates($filter = array())
	{	$tables = array('eventdates'=>'eventdates');
		$fields = array('eventdates.*');
		$where = array('event'=>'eventdates.eid=' . (int)$this->id);
		$orderby = array();
		switch ($filter['orderby'])
		{	case 'latest':
				$orderby[] = 'eventdates.starttime DESC';
			case 'next':
			default:
				$orderby[] = 'eventdates.starttime ASC';
		}
		
		if ($filter['startdate'])
		{	$where['startdate'] = 'eventdates.endtime>="' . $filter['startdate'] . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'eventdates.starttime<="' . $filter['enddate'] . ' 23:59:59"';
		}
		
		if ($filter['live'])
		{	$where['live'] = 'eventdates.endtime>="' . $this->datefn->SQLDateTime() . '"';
		}
		if ($filter['venue'])
		{	$where['venue'] = 'eventdates.venue=' . (int)$filter['venue'];
		}
		if ($filter['country'])
		{	$where['venues_link'] = 'eventdates.venue=eventvenues.evid';
			$where['country'] = 'eventvenues.country="' . $this->InputSafeString($filter['country']) . '"';
		}
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'edid', true);
		
	} // fn GetDates
	
	public function Link()
	{	if ($this->id) return SITE_URL . 'event/' . $this->id . '/' . $this->details['slug'] . '/';
	} // fn Link
	
	public function HasImage($size = 'default')
	{	if ($this->id && file_exists($this->GetImageFile($size)))
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions
	
	public function ImageDimensions($size = 'default')
	{	if ($this->imagesizes[$size])
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions
	
	public function GetImageFile($size = 'default')
	{	return $this->ImageFileDirectory($size) . '/' . $this->id .'.png';
	} // end of fn GetImageFile
	
	public function ImageFileDirectory($size = 'default')
	{	return $this->imagedir . $this->InputSafeString($size);
	} // end of fn FunctionName
	
	public function GetImageSRC($size = 'default')
	{	if ($this->HasImage($size))
		{	return $this->imagelocation . $this->InputSafeString($size) . '/' . $this->id . '.png';
		}
	} // end of fn GetImageSRC

} // end of defn Event
?>