<?php
class AdminCustomer extends Customer
{	
	function __construct($id = 0)
	{	parent::__construct($id);
		$this->Get($id);
	} //  end of fn __construct
	
	function Display()
	{	echo "<div id='porder-disp'>\n",
				"<div>\n<h3>Customer</h3>\n<ul>\n",
					"<li><span class='pod-label'>Email</span><span class='pod-details'><a href='mailto:", $this->details["email"], 
								"'>", $this->details["email"], "</a></span><br class='clear' /></li>\n",
					"<li><span class='pod-label'>Registered</span><span class='pod-details'>", 
								date("d-M-y @H:i", strtotime($this->details["regdate"])), "</span><br class='clear' /></li>\n",
					"<li><span class='pod-label'>Full name</span><span class='pod-details'>", 
								$this->InputSafeString($this->details["fullname"]), "</span><br class='clear' /></li>\n",
					"<li><span class='pod-label'>Company name</span><span class='pod-details'>", 
								$this->InputSafeString($this->details["compname"]), "</span><br class='clear' /></li>\n",
					"<li><span class='pod-label'>City</span><span class='pod-details'>", 
								$this->InputSafeString($this->details["city"]), "</span><br class='clear' /></li>\n",
					"<li><span class='pod-label'>Country</span><span class='pod-details'>", 
								$this->InputSafeString($this->details["country"]), "</span><br class='clear' /></li>\n",
					"<li><span class='pod-label'>Phone</span><span class='pod-details'>", 
								$this->InputSafeString($this->details["phone"]), "</span><br class='clear' /></li>\n",
					"<li><span class='pod-label'>&nbsp;</span><span class='pod-details'><a href='customerpword.php?id=", $this->id, "'>change password</a></span><br class='clear' /></li>\n",
				"</ul>\n</div>\n",
				"<div>\n<h3>Cart Orders<span></span></h3>\n<ul>\n",
					"<li>";
		$orders = $this->CartOrders();
		$orders->ListOrders();
		echo "</li>\n",
				"</ul>\n</div>\n",
			"</div>\n";
	} // end of fn Display
	
	function CartOrders()
	{	return new AdminSCOrders("", "", 0, $this->id);
	} // end of fn Orders
	
	function AdminSavePassword($data = array())
	{	
		$fail = array();
		$success = array();
		$fields = array();
		
		if ($_POST["newpass"] !== $_POST["rtpass"])
		{	$fail[] = "password mistyped";
		} else
		{	if ($this->AcceptablePW($_POST["newpass"]))
			{	$fields[] = "pword=MD5('{$_POST["newpass"]}')";
			} else
			{	$fail[]= "password invalid (8 to 20 characters)";
			}
		}
		
		if (!$fail && ($set = implode(", ", $fields)))
		{	$sql = "UPDATE customers SET $set WHERE custid=" . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$success[] = "password changed";
				} else
				{	$fail[] = "password not changed";
				}
			}
		}
		
		
		return array("failmessage"=>implode(", ", $fail), "successmessage"=>implode(", ", $success));
		
	} // end of fn AdminSavePassword
	
	function PasswordForm()
	{	$form = new Form("customerpword.php?id=" . (int)$this->id);
		$form->AddPasswordInput("New password", "newpass", "", 20);
		$form->AddPasswordInput("Retype password", "rtpass", "", 20);
		
		$form->AddSubmitButton("", "Change password", "submit");
		echo "<h3>Change password for ", $this->InputSafeString($this->details["fullname"]), "</h3>\n";
		$form->Output();
		
	} // end of fn PasswordForm
	
} // end of defn AdminCustomer
?>