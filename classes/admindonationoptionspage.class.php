<?php
class AdminDonationOptionsPage extends CMSPage
{	protected $donationcountry;
	protected $donationproject;
	protected $donationproject2;
	protected $menuarea;

	function CMSLoggedInConstruct()
	{	parent::CMSLoggedInConstruct();
		$this->AdminDonationOptionsLoggedInConstruct();
	} // end of fn CMSLoggedInConstruct
	
	protected function AdminDonationOptionsLoggedInConstruct()
	{	$this->css['admindonationoptions'] = 'admindonationoptions.css';
		$this->AssignDonationCountry();
		$this->breadcrumbs->AddCrumb('donationoptions.php', 'Donation options');
		$this->AdminDonationOptionsConstructFunctions();
		if ($this->donationcountry->id)
		{	$this->breadcrumbs->AddCrumb('donationoption.php?id=' . $this->donationcountry->id, $this->InputSafeString($this->donationcountry->details['shortname']));
			if ($this->donationproject->id)
			{	$this->breadcrumbs->AddCrumb('donationprojects.php?id=' . $this->donationcountry->id, 'Sub-options');
				$this->breadcrumbs->AddCrumb('donationproject.php?id=' . $this->donationproject->id, $this->InputSafeString($this->donationproject->details['projectname']));
				if ($this->donationproject2->id)
				{	$this->breadcrumbs->AddCrumb('donationprojects2.php?id=' . $this->donationproject->id, 'Sub-options');
					$this->breadcrumbs->AddCrumb('donationproject2.php?id=' . $this->donationproject->id, $this->InputSafeString($this->donationproject2->details['projectname']));
				}
			}
		}
	} // end of fn AdminDonationOptionsLoggedInConstruct
	
	protected function AdminDonationOptionsConstructFunctions()
	{	
	} // end of fn AdminDonationOptionsConstructFunctions
	
	protected function AssignDonationCountry()
	{	$this->donationcountry = new AdminDonationOption($_GET['id']);
	} // end of fn AssignDonationCountry
	
	protected function AdminDonationOptionsBody()
	{	if ($menu = $this->GetPageMenu())
		{	echo '<div class="adminItemSubMenu"><ul>';
			foreach ($menu as $menuarea=>$menuitem)
			{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
			}
			echo '</ul><div class="clear"></div></div>';
		}
	} // end of fn AdminDonationOptionsBody
	
	protected function GetPageMenu()
	{	if ($this->donationcountry->id)
		{	$menu = array();
			$menu['edit'] = array('text'=>$this->InputSafeString($this->donationcountry->details['shortname']), 'link'=>'donationoption.php?id=' . $this->donationcountry->id);
			$menu['projects'] = array('text'=>'Sub-options', 'link'=>'donationprojects.php?id=' . $this->donationcountry->id);
			if ($this->donationproject->id)
			{	$menu['project'] = array('text'=>$this->InputSafeString($this->donationproject->details['projectname']), 'link'=>'donationproject.php?id=' . $this->donationproject->id);
				if (!$this->donationproject->prices)
				{	$menu['projects2'] = array('text'=>'Sub-options for ' . $this->InputSafeString($this->donationproject->details['projectname']), 'link'=>'donationprojects2.php?id=' . $this->donationproject->id);
					if ($this->donationproject2->id)
					{	$menu['project2'] = array('text'=>$this->InputSafeString($this->donationproject2->details['projectname']), 'link'=>'donationproject2.php?id=' . $this->donationproject2->id);
					}
				}
			}
		}
		return $menu;
	} // end of fn GetPageMenu
	
	function CMSBodyMain()
	{	$this->AdminDonationOptionsBody();
	} // end of fn CMSBodyMain
	
} // end of defn AdminDonationOptionsPage
?>