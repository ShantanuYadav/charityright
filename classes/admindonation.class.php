<?php
class AdminDonation extends Donation
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} //  end of fn __construct

	public function AdminTitle()
	{	ob_start();
		if ($this->details['donationref'])
		{	echo $this->details['donationref'];
		} else
		{	echo 'unpaid #', $this->id;
		}
		echo ' - ', date('d/m/y', strtotime($this->details['created']));
		return ob_get_clean();
	} //  end of fn AdminTitle
	
	public function SaveChanges($data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if ($donortitle = $this->SQLSafe($data['donortitle']))
		{	$fields['donortitle'] = 'donortitle="' . $donortitle . '"';
		} else
		{	$donor_missing = true;
			$fields['donortitle'] = 'donortitle=""';
		}
		if ($donorfirstname = $this->SQLSafe($data['donorfirstname']))
		{	$fields['donorfirstname'] = 'donorfirstname="' . $donorfirstname . '"';
		} else
		{	$donor_missing = true;
			$fields['donorfirstname'] = 'donorfirstname=""';
		}
		if ($donorsurname = $this->SQLSafe($data['donorsurname']))
		{	$fields['donorsurname'] = 'donorsurname="' . $donorsurname . '"';
		} else
		{	$donor_missing = true;
			$fields['donorsurname'] = 'donorsurname=""';
		}
		
		if ($donoradd1 = $this->SQLSafe($data['donoradd1']))
		{	$fields['donoradd1'] = 'donoradd1="' . $donoradd1 . '"';
			$address_done++;
		} else
		{	$fields['donoradd1'] = 'donoradd1=""';
		}
		if ($donoradd2 = $this->SQLSafe($data['donoradd2']))
		{	$fields['donoradd2'] = 'donoradd2="' . $donoradd2 . '"';
			$address_done++;
		} else
		{	$fields['donoradd2'] = 'donoradd2=""';
		}
		if (!$address_done)
		{	$donor_missing = true;
		}

		if ($donorpostcode = $this->SQLSafe($data['donorpostcode']))
		{	$fields['donorpostcode'] = 'donorpostcode="' . $donorpostcode . '"';
		} else
		{	$donor_missing = true;
			$fields['donorpostcode'] = 'donorpostcode=""';
		}
		
		if ($donorcity = $this->SQLSafe($data['donorcity']))
		{	$fields['donorcity'] = 'donorcity="' . $donorcity . '"';
		} else
		{	$donor_missing = true;
			$fields['donorcity'] = 'donorcity=""';
		}

		$fields['giftaid'] = 'giftaid=' . ($data['giftaid'] ? 1 : 0);
		if ($data['donorcountry'] && $this->GetCountry($data['donorcountry']))
		{	$fields['donorcountry'] = 'donorcountry="' . $data['donorcountry'] . '"';
			if ($data['giftaid'])
			{	if ($data['donorcountry'] != 'GB')
				{	$fail[] = 'Giftaid is only available for the UK';
					$fields['giftaid'] = 'giftaid=0';
				}
			}
		} else
		{	$donor_missing = true;
			$fields['donorcountry'] = 'donorcountry=""';
		}
		
		if ($data['giftaid'] && $donor_missing)
		{	$fail[] = 'All donor details must be entered for a giftaid donation';
			unset($fields['giftaid'], $fields['donortitle'], $fields['donorfirstname'], $fields['donorsurname'], $fields['donoradd1'], $fields['donoradd2'], $fields['donorpostcode'], $fields['donorcity'], $fields['donorcountry']);
		}
		
		$fields['zakat'] = 'zakat=' . ($data['zakat'] ? 1 : 0);
		$fields['donoremail'] = 'donoremail="' . $this->SQLSafe($data['donoremail']) . '"';
		$fields['donorphone'] = 'donorphone="' . $this->SQLSafe($data['donorphone']) . '"';
		
//$fail[] = 'test';
		if (!$fail && $set = implode(', ', $fields))
		{	$sql = 'UPDATE donations SET ' . $set . ' WHERE did=' . $this->id;
		//	echo $sql;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$this->Get($this->id);
					$success[] = 'Changes saved';
					$if = new InfusionSoftDonation();
					$if->AmendDonation($this);
				}
			} else $fail[] = $sql . ': ' . $this->db->Error();
		}
//	$this->VarDump($fields);
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	
	} // fn SaveChanges
	
	public function ManualConfirmDirectDebit($data = array(), $send_confirmation = true)
	{	$fail = array();
		$success = array();
		$fields = array();
		$extras = array();
		
		if ($this->IsMonthlyUnconfirmed())
		{	if ($data['ezc_ContractID'])
			{	$fields['donationconfirmed'] = 'donationconfirmed="' . $this->datefn->SQLDateTime() . '"';
				$extras['ContractID'] = $data['ezc_ContractID'];
			} else
			{	$fail[] = 'Contract ID missing';
			}
			
			if ($data['ezc_CustomerID'])
			{	$extras['CustomerID'] = $data['ezc_CustomerID'];
			} else
			{	$fail[] = 'Customer ID missing';
			}
			
			if ($data['ezc_DirectDebitRef'])
			{	$fields['donationref'] = 'donationref="' . $this->SQLSafe($this->EZCRefFromContactID($data['ezc_DirectDebitRef'])) . '"';
				$extras['DirectDebitRef'] = $data['ezc_DirectDebitRef'];
			} else
			{	$fail[] = 'DirectDebitRef missing';
			}
			
			if (($d = (int)$data['dezc_StartDate']) && ($m = (int)$data['mezc_StartDate']) && ($y = (int)$data['yezc_StartDate']))
			{	$extras['StartDate'] = $this->datefn->SQLDate($ezc_StartDateStamp = mktime(0, 0, 0, $m, $d, $y));
				$extras['StartDay'] = date('j', $ezc_StartDateStamp);
			} else
			{	$fail[] = 'Start date missing';
			}
			
			if (!$fail)
			{	$sql = 'UPDATE donations SET ' . implode(', ', $fields) . ' WHERE did=' . $this->id;
				if (($result = $this->db->Query($sql)) && $this->db->AffectedRows())
				{	foreach ($extras as $name=>$value)
					{	$extras_sql = 'INSERT INTO donation_extras SET did=' . $this->id  . ', extrafield="' . $this->SQLSafe($name) . '", extravalue="' . $this->SQLSafe($value) . '"';
						$this->db->Query($extras_sql);
					}
					$this->Get($this->id);
					$success[] = 'Direct debit recorded';
					if ($send_confirmation)
					{	$this->SendDirectDebitEmailConfirmation();
					}
					$if = new InfusionSoftDonation();
					$if->ConfirmDirectDebit($this);
				} else
				{	$fail[] = 'db update failed: ' . $this->db->Error();
				}
			}
		} else
		{	$fail[] = 'this donation is not appropriate for manual updating';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn ManualConfirmDirectDebit
	
	public function ManualDDConfirmForm($data = array())
	{	ob_start();
		$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		
		if (($d = (int)$data['dezc_StartDate']) && ($m = (int)$data['mezc_StartDate']) && ($y = (int)$data['yezc_StartDate']))
		{	$data['ezc_StartDate'] = $this->datefn->SQLDate(mktime(0, 0, 0, $m, $d, $y));
		} else
		{	$data['ezc_StartDate'] = $this->datefn->SQLDate();
		}
		
		$form->AddDateInput('Post Date', 'ezc_StartDate', $data['ezc_StartDate'], array(date('Y'), date('Y') - 1));
		$form->AddTextInput('Customer ID', 'ezc_CustomerID', $this->InputSafeString($data['ezc_CustomerID']), 'long');
		$form->AddTextInput('Contract ID', 'ezc_ContractID', $this->InputSafeString($data['ezc_ContractID']), 'long');
		$form->AddTextInput('Direct Debit Ref', 'ezc_DirectDebitRef', $this->InputSafeString($data['ezc_DirectDebitRef']), 'long');
		$form->AddSubmitButton('', 'Confirm Direct Debit Set Up', 'submit');
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm

	function AdminDonationsBodyDisplayForm()
	{	ob_start();
		if (SITE_TEST)
		{	//$this->SendOneOffDonationEmail('tim@websquare.co.uk');
			//$this->SendDirectDebitForm();
			//$this->SendDirectDebitEmailConfirmation();
			//$this->SendDirectDebitEmailConfirmationToAdmin();
		}
		$cursymbol = $this->GetCurrency($this->details['currency'], 'cursymbol');
		$donation_countries = $this->GetDonationCountries($this->details['donationtype']);
		$address = array();
		if ($this->details['donoradd1'])
		{	$address[] = $this->InputSafeString($this->details['donoradd1']);
		}
		if ($this->details['donoradd2'])
		{	$address[] = $this->InputSafeString($this->details['donoradd2']);
		}
		$address[] = $this->InputSafeString($this->details['donorcity']);
		$address[] = $this->InputSafeString($this->details['donorpostcode']);
		$address[] = $this->InputSafeString($this->GetCountry($this->details['donorcountry']));
		//if (SITE_TEST) $this->SendDirectDebitEmailConfirmation();
		//if (SITE_TEST) $this->SendDirectDebitForm();
		//if (true || SITE_TEST) $this->DirectDebitFormPDFToFile();
		if ($this->CanAdminUser('accounts admin') && $this->CanRefund())
		{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&refund=1', $_GET['refund'] ? '&confirm=1' : '', '">', $_GET['refund'] ? 'please confirm you really want to ' : '', 'refund this donation</a></p>';
		}
		echo '<form method="post" class="" target="campaigndonation.php?id=', $this->id, '"><div id="itemblock-disp">
			<div><h3>Donation</h3><ul>
				<li><span class="itemblock-label">ID</span><span class="itemblock-details">', $this->ID(), '</span><br class="clear" /></li>
				<li><span class="itemblock-label">Donation ref</span><span class="itemblock-details">', $this->details['donationref'] ? $this->details['donationref'] : 'donation not confirmed', '</span><br class="clear" /></li>';
		if ($this->details['orderid'] && ($order = new CartOrder($this->details['orderid'])) && $order->id)
		{	echo '<li><span class="itemblock-label">Cart order</span><span class="itemblock-details"><a href="cartorder.php?id=', $order->id, '">', $order->ID(), '</a></span><br class="clear" /></li>';
		}
		if ($this->extrafields['DirectDebitRef'])
		{	echo '<li><span class="itemblock-label">Direct debit ref</span><span class="itemblock-details">', $this->extrafields['DirectDebitRef'], '</span><br class="clear" /></li>';
		}
		echo '<li><span class="itemblock-label">Created</span><span class="itemblock-details">', date('j-M-Y @H:i', strtotime($this->details['created'])), '</span><br class="clear" /></li>';
		if ($this->details['cancelled'])
		{	echo '<li><span class="itemblock-label">Cancelled</span><span class="itemblock-details">', date('j-M-Y @H:i', strtotime($this->details['canceldate'])), '</span><br class="clear" /></li>';
		}
		echo '<li><span class="itemblock-label">Amount</span><span class="itemblock-details">', $cursymbol, number_format($this->details['amount'] + $this->details['adminamount'], 2), ($this->details['donationtype'] == 'monthly') ? ' monthly' : '', $this->details['giftaid'] ? ' with giftaid' : '', '</span><br class="clear" /></li>';
		if ($this->details['adminamount'])
		{	echo '<li><span class="itemblock-label">included for admin</span><span class="itemblock-details">', $cursymbol, number_format($this->details['adminamount'], 2), '</span><br class="clear" /></li>';
		}
		if ($refunded = $this->GetRefund())
		{	echo '<li><span class="itemblock-label">Refunded</span><span class="itemblock-details">', date('j-M-Y @H:i', strtotime($refunded['refunded'])), '</span><br class="clear" /></li>';
		}
		echo '<li><span class="itemblock-label">For</span><span class="itemblock-details">', $this->details['zakat'] ? 'Zakat donation for ' : '', $this->GatewayTitleInner(), '</span><br class="clear" /></li>
			<li><span class="itemblock-label">Giftaid?</span><span class="itemblock-details"><input type="checkbox" name="giftaid" value="1"', $this->details['giftaid'] ? ' checked="checked"' : '', ' /></span><br class="clear" /></li>
			<li><span class="itemblock-label">Zakat?</span><span class="itemblock-details"><input type="checkbox" name="zakat" value="1"', $this->details['zakat'] ? ' checked="checked"' : '', ' /></span><br class="clear" /></li>';
		
		if ($this->details['infusionsoftid'])
		{	$if = new Infusionsoft();
			if ($iflink = $if->GetInfusionsoftOrderLink($this->details['infusionsoftid']))
			{	echo '<li><span class="itemblock-label">&nbsp;</span><span class="itemblock-details"><a href="', $iflink, '" target="_blank">view at Infusionsoft</a></span><br class="clear" /></li>';
			}
		}
		echo '</ul></div>
			<div><h3>Donor</h3><ul>';
		echo '<li><span class="itemblock-label">Title</span><span class="itemblock-details"><input type="text" name="donortitle" value="', $this->InputSafeString($this->details['donortitle']), '" /></span><br class="clear" /></li>';
		echo '<li><span class="itemblock-label">First name</span><span class="itemblock-details"><input type="text" name="donorfirstname" value="', $this->InputSafeString($this->details['donorfirstname']), '" /></span><br class="clear" /></li>';
		echo '<li><span class="itemblock-label">Surname</span><span class="itemblock-details"><input type="text" name="donorsurname" value="', $this->InputSafeString($this->details['donorsurname']), '" /></span><br class="clear" /></li>';
		echo '<li><span class="itemblock-label">Email</span><span class="itemblock-details"><input type="text" name="donoremail" value="', $this->InputSafeString($this->details['donoremail']), '" /></span><br class="clear" /></li>';
		if ($this->CanAdminUser('crm') && $this->details['donoremail'])
		{	echo '<li><span class="itemblock-label">&nbsp;</span><span class="itemblock-details"><a href="crm_email.php?email=', urlencode($this->details['donoremail']), '" target="_blank">view "', $this->InputSafeString($this->details['donoremail']), '" in CRM</a></span><br class="clear" /></li>';
		}
			echo '<li><span class="itemblock-label">Address</span><span class="itemblock-details"><input type="text" name="donoradd1" value="', $this->InputSafeString($this->details['donoradd1']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">&nbsp;</span><span class="itemblock-details"><input type="text" name="donoradd2" value="', $this->InputSafeString($this->details['donoradd2']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Town / City</span><span class="itemblock-details"><input type="text" name="donorcity" value="', $this->InputSafeString($this->details['donorcity']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Postcode</span><span class="itemblock-details"><input type="text" name="donorpostcode" value="', $this->InputSafeString($this->details['donorpostcode']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Country</span><span class="itemblock-details"><select name="donorcountry"><option value="">------</option>';
			foreach ($this->CountryList() as $ccode=>$cname)
			{	echo '<option value="', $ccode, '"', $ccode == $this->details['donorcountry'] ? ' selected="selected"' : '', '>', $this->InputSafeString($cname), '</option>';
			}
			echo '</select></span><br class="clear" /></li>';
		
		echo '<li><span class="itemblock-label">Phone</span><span class="itemblock-details"><input type="text" name="donorphone" value="', $this->InputSafeString($this->details['donorphone']), '" /></span><br class="clear" /></li>';
		$pref = new ContactPreferences();
		echo '<li><span class="itemblock-label">Contact methods approved</span><span class="itemblock-details">', $this->details['contactpref'] ? $pref->DisplayChosenPreferences($this->details['contactpref']) : 'none', '</span></li>';
		echo '</ul></div><p><input type="submit" class="submit" value="Save Changes" /><br class="clear" /></p></form>';
		if ($this->extrafields)
		{	echo '<div><h3>Extra info</h3><ul>';
			foreach ($this->extrafields as $name=>$value)
			{	echo '<li><span class="itemblock-label">', $this->InputSafeString($name), '</span><span class="itemblock-details">', $this->InputSafeString($value), '</span><br class="clear" /></li>';
			}
			echo '</ul></div>';
		}
		echo'</div>';
	//	$this->VarDump($this->details);
	//	$this->VarDump($this->extrafields);
		return ob_get_clean();
	} // end of fn AdminDonationsBodyDisplayForm

} // end of defn AdminDonation
?>