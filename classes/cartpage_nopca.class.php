<?php
class CartPage extends Base
{	private $pagecontent = false;
	private $page = false;
	//private $session_name = 'crmain_donation';
	private $stage = '';
	private $stages = array(
			1=>array('heading'=>'Your Basket', 'content_method'=>'CartStageContent', 'submit_method'=>'CartStageSubmit', 'breadcrumbs'=>1),
			2=>array('heading'=>'Personal<br />Details', 'content_method'=>'PersonalStageContent', 'submit_method'=>'PersonalStageSubmit', 'breadcrumbs'=>1),
			3=>array('heading'=>'Confirmation', 'content_method'=>'ConfirmationStageContent', 'submit_method'=>'ConfirmationStageSubmit', 'breadcrumbs'=>1),
			4=>array('heading'=>'Direct Debit', 'content_method'=>'DirectDebitStageContent', 'submit_method'=>'DirectDebitSubmit', 'breadcrumbs'=>1, 'autosubmit'=>true),
			5=>array('heading'=>'Direct Debit<br />Confirmed', 'content_method'=>'DirectDebitConfirmStageContent', 'submit_method'=>'DirectDebitConfirmSubmit', 'breadcrumbs'=>1),
			6=>array('heading'=>'Payment', 'content_method'=>'PaymentStageContent', 'submit_method'=>'PaymentStageSubmit', 'breadcrumbs'=>1, 'autosubmit'=>true)
		);
	public $failmessage = '';
	public $successmessage = '';
	protected $cart;
	private $giftaid_unticked = false;

	public function __construct(&$page, $stage = '')
	{	parent::__construct();
		$this->pagecontent = new PageContent('cart');
		$this->PageAssign($page);
		$this->cart = new Cart();

		if ($_GET['reset_test_cart'])
		{	unset($_SESSION[$this->cart_session_name]);
		}

		if ($this->stage = (int)$_POST['donate_submit_stage'])
		{	$this->SubmitDonateForm($_POST);
		}

		if (!$this->stage && isset($this->stages[$stage]))
		{	$this->stage = $stage;
		}
		if ($this->stages[$this->stage]['autosubmit'] && !$_POST)
		{	$this->SubmitDonateForm(array());
		}
		
		$this->AdjustStagesList();

		//print_r($_GET);
		
	} //  end of fn __construct

	protected function AdjustStagesList()
	{	$this->page = &$page;
		if (!$this->cart->MonthlyMainDonations())
		{	$this->stages[4]['breadcrumbs'] = false;
			$this->stages[5]['breadcrumbs'] = false;
			if (!$this->cart->OneoffCRStarsDonations() && !$this->cart->OneoffMainDonations())
			{	$this->stages[1]['breadcrumbs'] = false;
				$this->stages[2]['breadcrumbs'] = false;
				$this->stages[3]['breadcrumbs'] = false;
				$this->stages[6]['breadcrumbs'] = false;
			}
			
		} else
		{	if (!$this->cart->OneoffCRStarsDonations() && !$this->cart->OneoffMainDonations())
			{	$this->stages[6]['breadcrumbs'] = false;
				if (!$this->cart->MonthlyMainDonations())
				{	$this->stages[1]['breadcrumbs'] = false;
					$this->stages[2]['breadcrumbs'] = false;
					$this->stages[3]['breadcrumbs'] = false;
					$this->stages[4]['breadcrumbs'] = false;
					$this->stages[5]['breadcrumbs'] = false;
				}
			}
		}
	} //  end of fn AdjustStagesList

	protected function PageAssign(&$page)
	{	$this->page = &$page;
		$this->page->css['donations.css'] = 'donations.css';
		$this->page->css['checkout.css'] = 'checkout.css';
		$this->page->js['donations.js'] = 'donations.js';
		$this->page->js['checkout.js'] = 'checkout.js';
	} //  end of fn PageAssign

	private function SubmitDonateForm($data = array())
	{	if (($method = $this->stages[$this->stage]['submit_method']) && method_exists($this, $method))
		{	$saved = $this->$method($data);
			$this->page->failmessage = $saved['failmessage'];
			//$this->page->successmessage = $saved['successmessage'];
		}
	} // end of fn SubmitDonateForm

	private function RedirectToStage($stage = 0)
	{	if (($stage = (int)$stage) && $this->stages[$stage])
		{	header('location:' . $this->pagecontent->Link() . $stage . '/');
			exit;
		}
	} // end of fn RedirectToStage

	private function CartStageSubmit($data = array(), $redirect = true)
	{	//$this->VarDump($data);
		$fail = array();
		$success = array();
		
		if ($this->cart->ReadyForCheckout())
		{	if ($redirect)
			{	$this->RedirectToStage($this->stage = 2);
			}
		} else
		{	$fail[] = 'Nothing in your cart';
		}
		return array('failmessage'=>implode('<br />', $fail), 'successmessage'=>implode('<br />', $success));
	} // end of fn CartStageSubmit

	private function PersonalStageSubmit($data = array(), $redirect = true)
	{	//$this->VarDump($data);
		//return;
		$fail = array();
		$success = array();

		if (!$this->cart->SetProperty('donor_title', $data['donor_title']))
		{	$fail[] = 'You must give your title (Mr, Mrs, Miss, etc.)';
		}
		if (!$this->cart->SetProperty('donor_firstname', $data['donor_firstname']))
		{	$fail[] = 'You must give your first name';
		}
		if (!$this->cart->SetProperty('donor_lastname', $data['donor_lastname']))
		{	$fail[] = 'You must give your last name';
		}

		$this->cart->SetProperty('donor_giftaid', 0);
		if ($data['donor_country'] && $this->GetCountry($data['donor_country']))
		{	$this->cart->SetProperty('donor_country', $data['donor_country']);
			if ($gb = ($data['donor_country'] == 'GB'))
			{	if ($data['donor_giftaid'])
				{	$this->cart->SetProperty('donor_giftaid', 1);
				} else
				{	$this->giftaid_unticked = true;
				}
			}
		} else
		{	$fail[] = 'You must give your country of residence';
		}

		if ($this->cart->SetProperty('pca_donor_address1', $data['pca_donor_address1']))
		{	$pca_address_done++;
		}
		if ($this->cart->SetProperty('pca_donor_address2', $data['pca_donor_address2']))
		{	$pca_address_done++;
		}
		if ($this->cart->SetProperty('pca_donor_address3', $data['pca_donor_address3']))
		{	$pca_address_done++;
		}
		if (!$pca_address_done && $gb)
		{	$fail[] = 'You must give your address';
		}

		if (!$this->cart->SetProperty('pca_donor_city', $data['pca_donor_city']))
		{	if ($gb)
			{	$fail[] = 'You must give your town or city';
			}
		}

		if (!$this->cart->SetProperty('pca_donor_postcode', $data['pca_donor_postcode']))
		{	if ($gb)
			{	$fail[] = 'You must give your postcode';
			}
		}

		if ($this->cart->SetProperty('donor_address1', $data['donor_address1']))
		{	$address_done++;
		}
		if ($this->cart->SetProperty('donor_address2', $data['donor_address2']))
		{	$address_done++;
		}
		if (!$address_done)
		{	if (!$gb)
			{	$fail[] = 'You must give your address';
			}
		}

		if (!$this->cart->SetProperty('donor_city', $data['donor_city']))
		{	if (!$gb)
			{	$fail[] = 'You must give your town or city';
			}
		}

		if (!$this->cart->SetProperty('donor_postcode', $data['donor_postcode']))
		{	if (!$gb)
			{	$fail[] = 'You must give your postcode';
			}
		}

		if ($data['donor_email'])
		{	if ($this->ValidEMail($data['donor_email']))
			{	$this->cart->SetProperty('donor_email', $data['donor_email']);
			} else
			{	$fail[] = 'You must give a valid email address';
				if (!$this->cart->GetProperty('donor_email'))
				{	$this->cart->SetProperty('donor_email', $data['donor_email']);
				}
			}
		} else
		{	$fail[] = 'You must give your email address';
		}

		if ($this->ValidPhoneNumber($data['donor_phone'], $gb))
		{	$this->cart->SetProperty('donor_phone', $data['donor_phone']);
		} else
		{	$fail[] = 'Your phone number is missing or invalid';
			if (!$this->cart->GetProperty('donor_phone'))
			{	$this->cart->SetProperty('donor_phone', $data['donor_phone']);
			}
		}
		
		if (is_array($data['contactpref']) && $data['contactpref'])
		{	$pref = new ContactPreferences();
			$this->cart->SetProperty('contactpref', $pref->PreferencesValueFromArray($data['contactpref']));
		} else
		{	$this->cart->SetProperty('contactpref', (int)$data['contactpref']);
		}

		if ($monthly = (count($this->cart->MonthlyMainDonations()) > 0))
		{
			if ($data['dd_accountname'] && $this->AccountNameSafe($data['dd_accountname']))
			{	if ($this->cart->GetProperty('dd_accountname') != $data['dd_accountname'])
				{	$dd_changed = true;
				}
				$this->cart->SetProperty('dd_accountname', $data['dd_accountname']);
			} else
			{	$fail[] = 'Account name missing';
				$this->cart->SetProperty('dd_accountname', $data['dd_accountname']);
			}

			if (!$data['dd_accountnumber'])
			{	$fail[] = 'Account number missing';
				$this->cart->SetProperty('dd_accountnumber', '');
			} else
			{	if ($this->ValidBankAccountNumber($data['dd_accountnumber']))
				{	if ($this->cart->GetProperty('dd_accountnumber') != $data['dd_accountnumber'])
					{	$dd_changed = true;
					}
				} else
				{	$fail[] = 'Invalid account number (must be 8 numbers)';
				}
				$this->cart->SetProperty('dd_accountnumber', $data['dd_accountnumber']);
			}

			if ($data['dd_accountsortcode'])
			{	if ($this->ValidSortCode($data['dd_accountsortcode']))
				{	if ($this->cart->GetProperty('dd_accountsortcode') != str_replace('-', '', $data['dd_accountsortcode']))
					{	$dd_changed = true;
						$this->cart->SetProperty('dd_accountsortcode', $data['dd_accountsortcode']);
					}
				} else
				{	$fail[] = 'Sort code invalid';
					$this->cart->SetProperty('dd_accountsortcode', $data['dd_accountsortcode']);
				}
			} else
			{	$fail[] = 'Sort code missing';
				$this->cart->SetProperty('dd_accountsortcode', '');
			}

			$this->cart->SetProperty('dd_confirm', $data['dd_confirm'] ? '1' : '0');
			$this->cart->SetProperty('tc_confirm', $data['tc_confirm'] ? '1' : '0');
			if (!$this->cart->GetProperty('tc_confirm'))
			{	$fail[] = 'You must agree to our terms and conditions';
			}
		}

		if ($fail)
		{	$this->giftaid_unticked = false;
		} else
		{	if (!$this->giftaid_unticked || $_POST['donor_giftaidchecked'])
			{	if ($monthly && ($dd_changed || !$this->cart->GetProperty('dd_checked')))
				{	$pca = new PCAPredictAPI();
					$pca_result = $pca->CheckBankAccount($this->cart->GetCart());
					if ($pca_result['successmessage'])
					{	$this->cart->SetProperty('dd_checked', true);
					} else
					{	$fail[] = $pca_result['failmessage'];
						$this->cart->SetProperty('dd_checked', false);
					}
				}
				if (!$fail)
				{	if ($redirect)
					{	$this->RedirectToStage($this->stage = 3);
					}
				}
			}
		}
		return array('failmessage'=>implode('<br />', $fail), 'successmessage'=>implode('<br />', $success));
	} // end of fn PersonalStageSubmit

	private function ConfirmationStageSubmit($data = array(), $redirect = true)
	{	$cartcheck = $this->CartStageSubmit($cart = $this->cart->GetCart(), false);
		if ($cartcheck['failmessage'])
		{	$this->stage = 1;
			return array('failmessage'=>'You must tell us what you want to donate', 'successmessage'=>'');
		} else
		{	$personalcheck = $this->PersonalStageSubmit($cart, false);
			if ($personalcheck['failmessage'])
			{	$this->stage = 2;
				return array('failmessage'=>'You must complete your details', 'successmessage'=>'');
			} else
			{	$cartorder = new CartOrder();
				$saved = $cartorder->CreateFromCart($cart);
				if ($saved['failmessage'])
				{	return array('failmessage'=>$saved['failmessage'], 'successmessage'=>$saved['successmessage']);
				} else
				{	if ($cartorder->id)
					{	// check for monthly
						if ($this->cart->MonthlyMainDonations())
						{	$this->RedirectToStage($this->stage = 4);
							//return array('failmessage'=>'monthly donations found', 'successmessage'=>'');
						} else
						{	if ($redirect && $cartorder->CanPayOneOffDonations())
							{	$this->RedirectToStage($this->stage = 6);
							}
						}
					}
				}
				/*$this->stage = 5;
				$saved = $this->cart->CreateOrderFromSession();
				if ($this->cart->CanBePaid())
				{	if (false || !SITE_TEST)
					{	$this->page->bodyOnLoadJS[] = '$("form#wp_button").submit()';
					}
				}
				return array('failmessage'=>$saved['failmessage'], 'successmessage'=>'confirmed ok');*/
			}
		}
		return array('failmessage'=>'', 'successmessage'=>'');
	} // end of fn ConfirmationStageSubmit

	private function PaymentStageSubmit($data = array())
	{	if (false || !SITE_TEST)
		{	$this->page->bodyOnLoadJS[] = '$("form#wp_button").submit()';
		} else
		{	$this->page->bodyOnLoadJS[] = 'console.log("test send to worldpay")';
		}
	} // end of fn PaymentStageSubmit

	public function PaymentStageDDProcess($data = array())
	{	$fail = array();
		$success = array();

		if ($_SESSION[$this->session_name]['dd_checked'])
		{
			$saved = $this->donation->CreateFromSession($_SESSION[$this->session_name]);
			if ($this->donation->CanBePaid())
			{
				if ($this->donation->details['ddsolesig'])
				{	// save account details for the donation
					$this->donation->RecordUnpaidDirectDebit($_SESSION[$this->session_name]);
					$success[] = 'Your Direct Debit form has been sent';
				} else
				{	$ez = new EazyCollectAPI();
					$dd_customercreate = $ez->AddCustomer($this->donation, $_SESSION[$this->session_name]);
					if ($dd_customercreate['return']['CustomerID'])
					{	$_SESSION[$this->session_name]['ezc_CustomerID'] = $dd_customercreate['return']['CustomerID'];
						$dd_contractcreate = $ez->AddContract($this->donation, $dd_customercreate['return']['CustomerID']);
						if ($dd_contractcreate['return']['ContractID'])
						{	$_SESSION[$this->session_name]['ezc_ContractID'] = $dd_contractcreate['return']['ContractID'];
							$_SESSION[$this->session_name]['ezc_DirectDebitRef'] = $dd_contractcreate['return']['DirectDebitRef'];
							$_SESSION[$this->session_name]['ezc_StartDate'] = $dd_contractcreate['return']['StartDate'];
							$donation_confirm = $this->donation->ConfirmDirectDebit($_SESSION[$this->session_name]);
							if ($donation_confirm['successmessage'])
							{	$success[] = 'Your Direct Debit has been created';
							} else
							{	$fail[] = $donation_confirm['failmessage'];
							}
						} else
						{	$fail[] = $dd_contractcreate['failmessage'];
						}
					} else
					{	$fail[] = $dd_customercreate['failmessage'];
					}
				}
			} else
			{	$fail[] = $saved['failmessage'];
			}
		} else
		{	$fail[] = 'bank account details have not been checked';
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn PaymentStageDDProcess

	public function Breadcrumbs()
	{	ob_start();
		if ($stage_content = $this->BreadcrumbsInner())
		{	ob_start();
			echo '<div class="container donation_breadcrumbs"><div class="container_inner">', $stage_content, '</div></div>';
			return ob_get_clean();
		}
		return '';
	} // end of fn Breadcrumbs

	public function BreadcrumbsInner()
	{	ob_start();
		if (($stages = $this->stages) && is_array($stages))
		{	foreach ($stages as $key=>$stage)
			{	if (!$stage['breadcrumbs'])
				{	unset($stages[$key]);
				}
			}
			if ($stage_count = count($stages))
			{	$width_sofar = 0;
				echo '<ul>';
				$width = floor(100 /count( $stages));
				foreach ($stages as $id=>$stage)
				{	$width_sofar += $width;
					if (++$bc_count == $stage_count)
					{	$width += (100 - $width_sofar);
					}
					$classes = array();
					if ($id == $this->stage)
					{	$classes[] = 'donation_breadcrumbs_current';
					}
					if (strstr($stage['heading'], '<br />'))
					{	$classes[] = 'donation_breadcrumbs_2lines';
					}
					echo '<li class="', implode(' ', $classes), '" style="z-index: ', 1000 - $id, '; width: ', $width, '%;">';
					if ($id < $this->stage)
					{	echo '<a href="', $this->pagecontent->Link(), $id, '/" style="z-index: ', 1000 - $id, ';">';
					} else
					{	echo '<div style="z-index: ', 1000 - $id, ';">';
					}
					echo $stage['heading'], '<span></span>', $id < $this->stage ? '</a>' : '</div>', '</li>';
				}
				echo '</ul><div class="clear"></div>';
			}
		}
		return ob_get_clean();
	} // end of fn BreadcrumbsInner

	public function MainCartContent()
	{	ob_start();
		echo '<div class="container donate_main_content"><div class="container_inner">';
		if (($method = $this->stages[$this->stage]['content_method']) && method_exists($this, $method))
		{	echo $this->$method();
		}
		if (SITE_TEST)
		{	echo '<p><a href="/cart/?reset_test_cart=1">reset whole cart</a></p>';
			$this->VarDump($_SESSION[$this->cart_session_name]);
		}
		echo '</div></div>';
		return ob_get_clean();
	} // end of fn MainCartContent

	public function DonationCurrencyChooserContainer($donation_type = '', $currency = '')
	{	ob_start();
		if ($_SESSION[$this->cart_session_name]['currency'])
		{	echo '<input type="hidden" id="donation_currency" name="donation_currency" value="', $this->InputSafeString($_SESSION[$this->cart_session_name]['currency']), '" />';
		} else
		{
			echo '<div class="form-row">
				<div class="form-col form-col-2">
					<div class="form-col-inner">
						<label for="donation_currency" class="title-label">Which currency would you like to donate?</label>
					</div>
				</div>
				<div class="form-col form-col-2">
					<div class="form-col-inner donation-currency-list">', $this->CurrencyChooserNew($donation_type, $currency), '</div>
				</div>
			</div>';
		}
		return ob_get_clean();
	} // end of fn DonationTypeContainer

	private function CartStageContent()
	{	ob_start();
		echo '<div class="cartListContainer">', $this->CartStageContentInner(),  '</div>';
		return ob_get_clean();
	} // end of fn CartStageContent

	protected function CartStageQuantity($key = 0, $quantity = 0)
	{	ob_start();
		if ($quantity = (int)$quantity)
		{	echo '<span>Quantity</span><div class="csqContainer"><a class="csqMinus', ($quantity > 1) ? '' : ' csqMinusDisabled', '" ', ($quantity > 1) ? (' onclick="AlterDonationQuantity(' . (int)$key . ',-1);"') : '', '>-</a><div class="csqNumber">', $quantity, '</div><a class="csqPlus" onclick="AlterDonationQuantity(', (int)$key, ',1);">+</a></div>';
		}
		return ob_get_clean();
	} // end of fn CartStageQuantity

	protected function CartStageContentInner()
	{	ob_start();
		if ($this->cart->ReadyForCheckout() && ($cart = $this->cart->GetCart()))
		{	$oneoff_total = 0;
			$monthly_total = 0;
			$oneoff_lines = array();
			$monthly_lines = array();
			$cursymbol = $this->GetCurrency($cart['currency'], 'cursymbol');
			$donation = new Donation();
			$countries = $donation->GetDonationCountries();
			echo '<ul class="cartList">';
			if ($cart['donations'] && is_array($cart['donations']))
			{	foreach ($cart['donations'] as $key=>$donation)
				{	ob_start();
					if ($do_monthly = ($donation['donation_type'] == 'monthly'))
					{	$monthly_total += ($subamount = $donation['donation_amount'] + $donation['donation_admin']);
					} else
					{	$oneoff_total += ($subamount = $donation['donation_amount'] + $donation['donation_admin']);
					}
					echo '<li class="cartListItemRow cartList', $do_monthly ? 'Monthly' : 'Oneoff', '"><div class="cartListItemImage"></div><div class="cartListItemDesc">';
					if ($country = $countries[$donation['donation_country']])
					{	echo '<h4>', $this->InputSafeString($country['shortname']), '</h4>';
						if ($country['projects'] && ($project = $country['projects'][$donation['donation_project']]))
						{	echo '<p>', $this->InputSafeString($project['projectname']), '</p>';
							if ($project['projects'] && ($project2 = $project['projects'][$donation['donation_project2']]))
							{	echo '<p>', $this->InputSafeString($project2['projectname']), '</p>';
							}
						}
						//$this->VarDump($country);
					} else
					{	echo '<p>donation details not found</p>';
					}
					echo '<p>', $donation['donation_zakat'] ? 'Zakat' : 'Sadaqah', '</p></div><div class="cartListItemQty', $donation['donation_quantity'] ? '' : ' cartListItemQtyNone', '">', $this->CartStageQuantity($key, $donation['donation_quantity']), '</div><div class="cartListItemAmount">', $cursymbol, number_format($subamount, 2), '</div><div class="cartListItemActions"><a onclick="RemoveDonation(\'main\',', $key, ')" class="cartListItemActionsRemove"></a></div><div></div></li>';
					if ($do_monthly)
					{	$monthly_lines[] = ob_get_clean();
					} else
					{	$oneoff_lines[] = ob_get_clean();
					}
				}
			}
			if ($cart['crsdonations'] && is_array($cart['crsdonations']))
			{	foreach ($cart['crsdonations'] as $key=>$donation)
				{	ob_start();
					$oneoff_total += ($subamount = $donation['donation_amount']);
					$campaign = new Campaign($donation['cid']);
					echo '<li class="cartListItemRow cartListCRStars"><div class="cartListItemImage">', $campaign->ImageHTML('small'), '</div><div class="cartListItemDesc"><h4>', strip_tags($campaign->FullTitle()), '</h4><p>', $donation['donation_zakat'] ? 'Zakat' : 'Sadaqah', '</p></div><div class="cartListItemQty cartListItemQtyNone"></div><div class="cartListItemAmount">', $cursymbol, number_format($subamount, 2), '</div><div class="cartListItemActions"><a onclick="RemoveDonation(\'crstars\',', $key, ')" class="cartListItemActionsRemove"></a></div><div></div></li>';
					$oneoff_lines[] = ob_get_clean();
				}
			}

			if ($oneoff_total && $oneoff_lines)
			{	$oneoff_lines = array_reverse($oneoff_lines);
				foreach ($oneoff_lines as $key=>$line)
				{	$oneoff_lines[$key] = str_replace('cartListItemRow', 'cartListItemRow cartListItemRowLast', $line);
					break;
				}
				$oneoff_lines = array_reverse($oneoff_lines);
				echo '<li class="cartListTotal"><div class="cartListTotalLabel">Total Single Donations</div><div class="cartListTotalAmount">', $cursymbol, number_format($oneoff_total, 2), '</div><div></div></li>', implode('', $oneoff_lines);
			}
			if ($monthly_total && $monthly_lines)
			{	$monthly_lines = array_reverse($monthly_lines);
				foreach ($monthly_lines as $key=>$line)
				{	$monthly_lines[$key] = str_replace('cartListItemRow', 'cartListItemRow cartListItemRowLast', $line);
					break;
				}
				$monthly_lines = array_reverse($monthly_lines);
				echo '<li class="cartListTotal"><div class="cartListTotalLabel">Total Monthly Donations</div><div class="cartListTotalAmount">', $cursymbol, number_format($monthly_total, 2), '</div><div></div></li>', implode('', $monthly_lines);
			}
			$donatepage = new PageContent('donate');
			echo '</ul><div class="cartButtons"><a href="', $this->pagecontent->Link(), '2/">Checkout</a><a href="', $donatepage->Link(), '">Make Another Donation</a><div class="clear"></div></div>';
		} else
		{	$donatepage = new PageContent('donate');
			echo '<div class="cartEmptyHeader"><h2>You have no donations in your basket</h2><div class="cartButtons"><a href="', $donatepage->Link(), '">Make A Donation</a></div></div>';
		}
		return ob_get_clean();
	} // end of fn CartStageContentInner

	protected function CurrencyChooserNew($type = '', $currency_selected = '')
	{	ob_start();
		if ($currencies = $this->GetCurrencies($type))
		{	if (!$currencies[$currency_selected])
			{	$currency_selected = false;
			}
			echo '<div class="custom-select-box"><select id="donation_currency" name="donation_currency" onchange="DonationCurrencyChangeNew();">';
			foreach ($currencies as $curcode=>$currency) {
				if (!$currency_selected)
				{	$currency_selected = $curcode;
				}
				echo '<option value="', $curcode, '"', $curcode == $currency_selected ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
			}
			echo '</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>';
		}
		return ob_get_clean();
	} // end of fn CurrencyChooserNew

	private function GetCurrencies($type = '')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies

	public function PersonalStageContent()
	{	ob_start();
		//$this->VarDump($this->CountryList());
		$pref = new ContactPreferences();
		$pp_page = new PageContent('use-of-cookies');
		$add_lines = array();
		
		if ($this->cart->GetProperty('pca_donor_address1'))
		{	$add_lines[] = $this->InputSafeString($this->cart->GetProperty('pca_donor_address1'));
		}
		if ($this->cart->GetProperty('pca_donor_address2'))
		{	$add_lines[] = $this->InputSafeString($this->cart->GetProperty('pca_donor_address2'));
		}
		if ($this->cart->GetProperty('pca_donor_address3'))
		{	$add_lines[] = $this->InputSafeString($this->cart->GetProperty('pca_donor_address3'));
		}
		if ($this->cart->GetProperty('pca_donor_city'))
		{	$add_lines[] = $this->InputSafeString($this->cart->GetProperty('pca_donor_city'));
		}
		if ($this->cart->GetProperty('pca_donor_postcode'))
		{	$add_lines[] = $this->InputSafeString($this->cart->GetProperty('pca_donor_postcode'));
		}
		if ($this->cart->GetProperty('pca_donor_country'))
		{	$add_lines[] = $this->InputSafeString($this->cart->GetProperty('pca_donor_country'));
		}
		echo '<form action="', $this->pagecontent->Link(), '2/" method="post" id="donatePersonalStageForm" class="donation-form donation-stage-2 form-rows"><input type="hidden" name="donate_submit_stage" value="2" />';
		if ($do_monthly = count($this->cart->MonthlyMainDonations()))
		{	echo '<img src="', SITE_URL, 'img/template/direct-debit-logo.png" alt="Direct Debit" class="direct-debit-logo">';
		}
		echo '	<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-title" class="title-label">Title <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-title" name="donor_title" value="', $this->InputSafeString($this->cart->GetProperty('donor_title')), '" required /></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-firstname" class="title-label">First name <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-firstname" name="donor_firstname" value="', $this->InputSafeString($this->cart->GetProperty('donor_firstname')), '" required /></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-lastname" class="title-label">Last name <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-lastname" name="donor_lastname" value="', $this->InputSafeString($this->cart->GetProperty('donor_lastname')), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-country" class="title-label">Country <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box"><select id="donor-country" name="donor_country" onchange="DonorCountryChangeNew();" required>';
		if ($do_monthly)
		{	$country = $this->cart->SetProperty('donor_country', 'GB');
			echo '<option value="GB">', $this->GetCountry('GB', 'shortname'), '</option>';
		} else
		{	if (!$country = $this->cart->GetProperty('donor_country'))
			{	$country = $this->cart->SetProperty('donor_country', 'GB');
			}
			//echo '<option value=""></option>';
			foreach ($this->CountryList() as $code=>$countryname)
			{	echo '<option value="', $code, '"', ($code == $country) ? ' selected="selected"' : "", '>', $this->InputSafeString($countryname), '</option>';
			}
		}
		//$do_pca = ($country == 'GB');
		echo '				</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>
							</div>
						</div>
					</div>
					<div class="form-row donorAddressPCA ', $do_pca ? '' : 'hidden', '">
						<link rel="stylesheet" type="text/css" href="https://services.postcodeanywhere.co.uk/css/captureplus-2.30.min.css?key=pj67-xk39-kp45-jg35" />
						<script type="text/javascript" src="https://services.postcodeanywhere.co.uk/js/captureplus-2.30.min.js?key=pj67-xk39-kp45-jg35"></script>
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="pcaAddress" class="title-label">Address<span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="pcaAddressContainer clearfix ', ($this->cart->GetProperty('pca_donor_address1') && $this->cart->GetProperty('pca_donor_city') && $this->cart->GetProperty('pca_donor_postcode')) ? 'hidden' : '', '">
									<input type="text" id="pcaAddress" name="pcaAddress" value="" placeholder="Start typing a postcode, street or address" />
								</div>
								<div class="donor-address-hidden-fields hidden">
									<input type="text" id="pca_donor_address1" name="pca_donor_address1" value="', $this->InputSafeString($this->cart->GetProperty('pca_donor_address1')), '" autocomplete="off" />
									<input type="text" id="pca_donor_address2" name="pca_donor_address2" value="', $this->InputSafeString($this->cart->GetProperty('pca_donor_address2')), '" autocomplete="off" />
									<input type="text" id="pca_donor_address3" name="pca_donor_address3" value="', $this->InputSafeString($this->cart->GetProperty('pca_donor_address3')), '" autocomplete="off" />
									<input type="text" id="pca_donor_city" name="pca_donor_city" value="', $this->InputSafeString($this->cart->GetProperty('pca_donor_city')), '" autocomplete="off" />
									<input type="text" id="pca_donor_postcode" name="pca_donor_postcode" value="', $this->InputSafeString($this->cart->GetProperty('pca_donor_postcode')), '" autocomplete="off" />
									<input type="text" id="pca_donor_country" name="pca_donor_country" value="', $this->InputSafeString($this->cart->GetProperty('pca_donor_country')), '" autocomplete="off" />
								</div>
								<div class="donor-address-box clearfix ', ($this->cart->GetProperty('pca_donor_address1') && $this->cart->GetProperty('pca_donor_city') && $this->cart->GetProperty('pca_donor_postcode')) ? '' : 'hidden', '">
									<div class="donor-address-box-content" name="pca_donor_all_fields">', implode('<br />', $add_lines), '</div>
									<div class="address-search-again"><a href="#pcaAddress" class="button">Search Address Again</a></div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row donorAddressManual', $do_pca ? ' hidden' : '', '">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-address1" class="title-label">Address<span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<input type="text" id="donor-address1" name="donor_address1" value="', $this->InputSafeString($this->cart->GetProperty('donor_address1')), '" placeholder="Address Line 1" />
								<input type="text" name="donor_address2" value="', $this->InputSafeString($this->cart->GetProperty('donor_address2')), '" placeholder="Address Line 2" />
								<input type="text" name="donor_city" value="', $this->InputSafeString($this->cart->GetProperty('donor_city')), '" placeholder="Town / City" />
								<input type="text" name="donor_postcode" value="', $this->InputSafeString($this->cart->GetProperty('donor_postcode')), '" placeholder="Postcode" />
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-email" class="title-label">Email address <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="email" id="donor-email" name="donor_email" value="', $this->InputSafeString($this->cart->GetProperty('donor_email')), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-phone" class="title-label">Phone number <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-phone" name="donor_phone" value="', $this->InputSafeString($this->cart->GetProperty('donor_phone')), '" required /></div>
						</div>
					</div>';
			echo '<div class="form-row crRegFormContactPref">
						<p class="cprefTitle">How would you like to receive updates?<br /><span>(Optional)</span></p>
						<p class="cprefHelp">We\'d love to keep you in the loop about what we\'re up to and how your donation is making a difference. But without your consent, we won\'t be able to.</p>
						<p class="cprefHelp">You can be sure your details will be kept safe, and we won’t share them with any other organisations. See our <a href="', $pp_page->Link(), '">privacy policy</a>.</p>
						<p>', $pref->FormElementsList($this->cart->GetProperty('contactpref')), '</p>
						<p class="cprefHelp">Don\'t forget, you can change your preferences at any time by emailing us at info@charityright.org.uk</p>
					</div>
					';
			if ($do_monthly)
			{	echo '<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
						</div>
					</div>
					<div class="direct-debit-container clearfix"><img src="', SITE_URL, 'img/template/direct-debit-logo.png" alt="Direct Debit" class="direct-debit-logo">
						<h3>Direct Debit details</h3>
						<p>You have chosen to set up a Variable Direct Debit. Subject to your rights under the Direct Debit Guarantee this will automatically debit payments due from your bank or building society account. If you are a joint signatory on the account, a direct debit instruction will be sent to you by email.</p>
						<div class="form-row ">
							<div class="form-col form-col-4">
								<div class="form-col-inner"><label for="dd-accountname" class="title-label">Account name <span class="required">*</span></label></div>
							</div>
							<div class="form-col form-col-2">
								<div class="form-col-inner"><input type="text" id="dd-accountname" name="dd_accountname" value="', $this->InputSafeString($this->cart->GetProperty('dd_accountname')), '" required /></div>
							</div>
						</div>
						<div class="form-row ">
							<div class="form-col form-col-4">
								<div class="form-col-inner"><label for="dd-accountnumber" class="title-label">Account number <span class="required">*</span></label></div>
							</div>
							<div class="form-col form-col-2">
								<div class="form-col-inner"><input type="text" maxlength="8" id="dd-accountnumber" name="dd_accountnumber" value="', $this->InputSafeString($this->cart->GetProperty('dd_accountnumber')), '" pattern="[0-9]{8}" title="Invalid account number (must be 8 numbers)" required /></div>
							</div>
						</div>
						<div class="form-row ">
							<div class="form-col form-col-4">
								<div class="form-col-inner"><label for="dd-accountsortcode" class="title-label">Account sort code <span class="required">*</span></label></div>
							</div>
							<div class="form-col form-col-2">
								<div class="form-col-inner"><input type="text" maxlength="6" id="dd-accountsortcode" name="dd_accountsortcode" value="', $this->InputSafeString($this->cart->GetProperty('dd_accountsortcode')), '" pattern="[0-9]{6}" title="Invalid sort code number (must be 6 numbers)"  required /></div>
							</div>
						</div>
						<div class="form-row ">
							<div class="form-col form-col-4">
								<div class="form-col-inner"><label for="dd-confirm" class="title-label">&nbsp;</label></div>
							</div>
							<div class="form-col form-col-2">
								<div class="form-col-inner">
									<div class="custom-checkbox clearfix"><input type="checkbox" name="dd_confirm" id="dd-confirm" value="1" ',$this->cart->GetProperty('dd_confirm') ? 'checked="checked"' : "", ' /><label for="dd-confirm">&nbsp;Please tick to confirm you are the sole account holder</label></div>
									<div class="form-help-text clearfix">Checking the box indicates that I am the Bank/Building Society account holder and no authority other than my own is required. If more than one person is required to authorise Debits from the account, leave this box unchecked and we will send you a paper Direct Debit Instruction for completion.</div>
									<div class="custom-checkbox clearfix">';
				$tc_page = new PageContent('terms-conditions');
				echo '<input type="checkbox" name="tc_confirm" id="tc-confirm" value="1" ', ($this->cart->GetProperty('tc_confirm')) ? 'checked="checked"' : '', ' /><label for="tc-confirm">&nbsp;I understand and agree to the <a href="', $tc_page->Link(), '">Terms and Conditions</a></label>
									</div>
								</div>
							</div>
						</div>
					</div>';
			}
			$giftaid_lines = array();
			$monthly_total = 0;
			$oneoff_total = 0;
			if ($cart = $this->cart->GetCart())
			{	if ($cart['donations'] && is_array($cart['donations']))
				{	foreach ($cart['donations'] as $key=>$donation)
					{	if ($do_monthly = ($donation['donation_type'] == 'monthly'))
						{	$monthly_total += ($subamount = $donation['donation_amount'] + $donation['donation_admin']);
						} else
						{	$oneoff_total += ($subamount = $donation['donation_amount'] + $donation['donation_admin']);
						}
					}
				}
				if ($cart['crsdonations'] && is_array($cart['crsdonations']))
				{	foreach ($cart['crsdonations'] as $key=>$donation)
					{	$oneoff_total += ($subamount = $donation['donation_amount']);
					}
				}
			}
			$cursymbol = $this->GetCurrency($this->cart->GetProperty('currency'), 'cursymbol');
			if ($oneoff_total)
			{	$giftaid_lines[] = $cursymbol . number_format($oneoff_total * 0.25, 2);
			}
			if ($monthly_total)
			{	$giftaid_lines[] = $cursymbol . number_format($monthly_total * 0.25, 2) . ' per month';
			}
			echo '<div class="form-row donor-giftaid-box ', ($country == 'GB') ? '' : ' hidden', '">
						<div class="form-col form-col-1">
							<div class="form-col-inner">
								<div class="alert-box alert-box-green">
									<h3>Reclaim Gift Aid - Add an extra <strong>', implode(' and ', $giftaid_lines), '</strong> to your donation at no extra cost to you.</h3>
									<p>&nbsp;</p><input type="hidden" name="donor_giftaidchecked" value="" />
									<div class="custom-checkbox clearfix"><input type="checkbox" name="donor_giftaid" id="donor-giftaid" value="1" ', $this->cart->GetProperty('donor_giftaid') ? 'checked="checked"' : '', '><label for="donor-giftaid">&nbsp;I want to Gift Aid my donation and any donations I make in the future or have made in the past 4 years to Charity Right.</label>
								</div>
								<div class="form-help-text clearfix">
									<strong>By ticking this box you are confirming that:</strong><br />
									"I am a UK taxpayer and understand that if I pay less Income Tax and/or Capital Gains Tax than the amount of Gift Aid claimed on all my donations in the current tax year it is my responsibility to pay any difference. I understand that other taxes such as VAT and Council Tax do not qualify. I understand the charity will reclaim 25p of tax on every &pound;1 that I give in the current tax year."
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
						</div>
					</div></div>';
		echo '<div class="cartButtons"><input type="submit" value="Next" class="button" /><div class="clear"></div></div>';

		if ($this->giftaid_unticked)
		{	echo '<div class="donGiftaidChecker"><h4>Remember as a UK tax payer, you can top up your donation by 25% at no extra cost.</h4><p><a onclick="DonorGiftaidChecker(true);">Yes, please add giftaid to my donation</a></p><p><a onclick="DonorGiftaidChecker(false);">No, continue without giftaid</a></p></div>';
		}

		echo '</form>';
		return ob_get_clean();
	} // end of fn PersonalStageContent

	public function ConfirmationStageContent()
	{	ob_start();
		$oneoff_total = 0;
		$monthly_total = 0;
		$donations_count = 0;
		$oneoff_lines = array();
		$monthly_lines = array();

		$cursymbol = $this->GetCurrency($this->cart->GetProperty('currency'), 'cursymbol');
		$donor_country = $this->cart->GetProperty('donor_country');

		echo '<form action="', $this->pagecontent->Link(), '3/" method="post" class="donation-form donation-stage-4 form-rows"><input type="hidden" name="donate_submit_stage" value="3" />';
		
		if ($this->cart->ReadyForCheckout() && ($cart = $this->cart->GetCart()))
		{	$donation = new Donation();
			$countries = $donation->GetDonationCountries();
			if ($cart['donations'] && is_array($cart['donations']))
			{	foreach ($cart['donations'] as $key=>$donation)
				{	if ($do_monthly = ($donation['donation_type'] == 'monthly'))
					{	$monthly_total += ($subamount = $donation['donation_amount'] + $donation['donation_admin']);
					} else
					{	$oneoff_total += ($subamount = $donation['donation_amount'] + $donation['donation_admin']);
					}
					$donations_count++;
					ob_start();
					echo '<li>';
					if ($country = $countries[$donation['donation_country']])
					{	echo $this->InputSafeString($country['shortname']);
						if ($country['projects'] && ($project = $country['projects'][$donation['donation_project']]))
						{	echo ', ', $this->InputSafeString($project['projectname']);
							if ($project['projects'] && ($project2 = $project['projects'][$donation['donation_project2']]))
							{	echo ', ', $this->InputSafeString($project2['projectname']);
							}
						}
					} else
					{	echo 'donation details not found';
					}
					if ($donation['donation_quantity'])
					{	echo ' &times; ', (int)$donation['donation_quantity'];
					}
					echo ', ', $donation['donation_zakat'] ? 'Zakat' : 'Sadaqah', ' : <span>', $cursymbol, number_format($subamount, 2), '</span></li>';
					if ($do_monthly)
					{	$monthly_lines[] = ob_get_clean();
					} else
					{	$oneoff_lines[] = ob_get_clean();
					}
				}
			}
			if ($cart['crsdonations'] && is_array($cart['crsdonations']))
			{	foreach ($cart['crsdonations'] as $key=>$donation)
				{	$oneoff_total += ($subamount = $donation['donation_amount']);
					$campaign = new Campaign($donation['cid']);
					$donations_count++;
					ob_start();
					echo '<li>', strip_tags($campaign->FullTitle()), ', ', $donation['donation_zakat'] ? 'Zakat' : 'Sadaqah', ' : <span>', $cursymbol, number_format($subamount, 2), '</span></li>';
					$oneoff_lines[] = ob_get_clean();
				}
			}

			if ($oneoff_total && $oneoff_lines)
			{	echo '<div class="form-row donation-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><h3>Your Single Donation';
				if (count($oneoff_lines) > 1)
				{	echo 's of ', $cursymbol, number_format($oneoff_total, 2);
				}
				echo '</h3><ul>', implode('', $oneoff_lines), '</ul></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>';
			}
			if ($monthly_total && $monthly_lines)
			{	echo '<div class="form-row donation-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><h3>Your Monthly Donation';
				if (count($monthly_lines) > 1)
				{	echo 's of ', $cursymbol, number_format($monthly_total, 2);
				}
				echo '</h3><ul>', implode('', $monthly_lines), '</ul></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>';
			}
			
		}
		if ($this->cart->GetProperty('donor_giftaid'))
		{	echo '<div class="form-row donation-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><p>Your donations include gift aid</p></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>';
		}
		
		$address = array();
		if ($this->cart->GetProperty('donor_address1') && ($donor_country != 'GB'))
		{	if ($this->cart->GetProperty('donor_address1'))
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_address1']);
			}
			if ($this->cart->GetProperty('donor_address2'))
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_address2']);
			}
			$address[] = $this->InputSafeString($this->cart->GetProperty('donor_city'));
			$address[] = $this->InputSafeString($this->cart->GetProperty('donor_postcode'));
		} else
		{	if ($pca_donor_address1 = $this->cart->GetProperty('pca_donor_address1'))
			{	$address[] = $this->InputSafeString($pca_donor_address1);
			}
			if ($pca_donor_address2 = $this->cart->GetProperty('pca_donor_address2'))
			{	$address[] = $this->InputSafeString($pca_donor_address2);
			}
			if ($pca_donor_address3 = $this->cart->GetProperty('pca_donor_address3'))
			{	$address[] = $this->InputSafeString($pca_donor_address3);
			}
			$address[] = $this->InputSafeString($this->cart->GetProperty('pca_donor_city'));
			$address[] = $this->InputSafeString($this->cart->GetProperty('pca_donor_postcode'));
		}
		$address[] = $this->InputSafeString($this->GetCountry($donor_country));

		echo	'<div class="form-row donation-your-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<h3>Your Details</h3>
							<p>', $this->InputSafeString($this->cart->GetProperty('donor_title')), ' ', $this->InputSafeString($this->cart->GetProperty('donor_firstname')), ' ', $this->InputSafeString($this->cart->GetProperty('donor_lastname')), '</p>
							<p>', implode('<br />', $address), '</p>
							<p>Email: ', $this->InputSafeString($this->cart->GetProperty('donor_email')), '</p>';
		if ($donor_phone = $this->cart->GetProperty('donor_phone'))
		{	echo '<p>Phone number: ', $this->InputSafeString($donor_phone), '</p>';
		}
		echo '<p><a href="', $this->pagecontent->Link(), '2/" class="button button-gray-outline">amend your details</a></p>
						</div>
					</div>
				</div>';
		if ($monthly_total)
		{	echo '
				<div class="form-row">
					<div class="form-col form-col-1"><div class="form-col-inner"><div class="form-row-separator clearfix"></div></div></div>
				</div>
				<div class="form-row direct-debit-account-details">
					<div class="form-col form-col-1"><img src="', SITE_URL, 'img/template/direct-debit-logo.png" alt="Direct Debit" class="direct-debit-logo">
						<div class="form-col-inner">
							<h3>Direct Debit - Account Details</h3>
							<p>Service user number: ', $this->InputSafeString(EZC_SUN), '</p>
							<p>Account name: ', $this->InputSafeString($this->cart->GetProperty('dd_accountname')), '</p>
							<p>Account number: ', $this->InputSafeString($this->cart->GetProperty('dd_accountnumber')), '</p>
							<p>Account sort code: ', $this->SortCodeDisplayString($this->cart->GetProperty('dd_accountsortcode')), '</p>
							<p>Are you the only person to sign for this account: ', $this->cart->GetProperty('dd_confirm') ? 'Yes' : 'No', '</p>
							<p><a href="', $this->pagecontent->Link(), '2/" class="button button-gray-outline">amend your direct debit details</a></p>
						</div>
					</div>
				</div>
				<div class="form-row direct-debit-guarantee">
					<div class="form-col form-col-1"><img src="', SITE_URL, 'img/template/direct-debit-logo.png" alt="Direct Debit" class="direct-debit-logo">
						<div class="form-col-inner">
							<h3>The Direct Debit Guarantee</h3>
							<p>This Guarantee is offered by all banks and building societies that accept instructions to pay Direct Debits.</p>
							<p>If there are any changes to the amount, date or frequency of your Direct Debit ', EZC_SUNNAME, ' will notify you 10 working days in advance of your account being debited or as otherwise agreed. If you request ', EZC_SUNNAME, ' to collect a payment, confirmation of the amount and date will be given to you at the time of the request.</p>
							<p>If an error is made in the payment of your Direct Debit, by ', EZC_SUNNAME, ' or your bank or building society, you are entitled to a full and immediate refund of the amount paid from your bank or building society.<ul><li>If you receive a refund you are not entitled to, you must pay it back when ', EZC_SUNNAME, ' asks you to.</p>
							<p>You can cancel a Direct Debit at any time by simply contacting your bank or building society. Written confirmation may be required. Please also notify us.</p>
						</div>
					</div>
				</div>';
		}
		$tc_page = new PageContent('terms-conditions');
		echo '<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row donation-terms-and-conditions">
					<div class="form-col form-col-1">
						<div class="form-col-inner" style="text-align: center;"><p>By making your donation you are confirming that you accept our <a href="', $tc_page->Link(), '" target="_blank">terms and conditions</a>.</p></div>
					</div>
				</div>
				<div class="cartButtons"><input type="submit" value="Make Your Donation" class="button" /><div class="clear"></div></div>
			</form>';
		return ob_get_clean();
	} // end of fn ConfirmationStageContent

	public function PaymentStageContent()
	{	ob_start();
		$cartorder = $this->cart->CartOrder();
		if ($cartorder->CanPayOneOffDonations())
		{	echo $cartorder->PaymentButton(), 
				//$cartorder->GASingleTransactionTrackingCode(), 
				'<div class="gatewayRedirect">please wait ... redirecting to Worldpay</div>';
		}
		return ob_get_clean();
	} // end of fn PaymentStageContent

	public function DirectDebitSubmit()
	{	$fail = array();
		$success = array();

		$cartorder = $this->cart->CartOrder();
		if ($cartorder->CanPayDirectDebitDonations())
		{	//$this->page->bodyOnLoadJS[] = 'alert("test direct debit process")';
			if ($cartorder->details['ddsolesig'])
			{	$ez = new EazyCollectAPI();
				$dd_customercreate = $ez->AddCustomerFromOrder($cartorder, $this->cart->GetCart());
			//	$this->VarDump($dd_customercreate);
			//	exit;
				if ($dd_customercreate['return']['CustomerID'])
				{	$this->cart->SetProperty('ezc_CustomerID', $dd_customercreate['return']['CustomerID']);
					$dd_contractcreate = $ez->AddContractFromOrder($cartorder, $dd_customercreate['return']['CustomerID']);
					if ($dd_contractcreate['return']['ContractID'])
					{	$this->cart->SetProperty('ezc_ContractID', $dd_contractcreate['return']['ContractID']);
						$this->cart->SetProperty('ezc_DirectDebitRef', $dd_contractcreate['return']['DirectDebitRef']);
						$this->cart->SetProperty('ezc_StartDate', $dd_contractcreate['return']['StartDate']);
						$donation_confirm = $cartorder->ConfirmDirectDebit($this->cart->GetCart());
						if ($donation_confirm['successmessage'])
						{	$success[] = 'Your Direct Debit has been created';
							//$this->RedirectToStage($this->stage = 5);
							$this->page->bodyOnLoadJS[] = 'window.location="' . $this->pagecontent->Link() . '5/"';
						} else
						{	$fail[] = $donation_confirm['failmessage'];
						}
					} else
					{	$fail[] = $dd_contractcreate['failmessage'];
					}
				} else
				{	$fail[] = $dd_customercreate['failmessage'];
				}
			} else
			{	$saved = $cartorder->RecordUnpaidDirectDebit($this->cart->GetCart());
				$success[] = 'Your Direct Debit form has been sent';
			}
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn DirectDebitSubmit

	public function DirectDebitStageContent()
	{	ob_start();
		$cartorder = $this->cart->CartOrder();
		
		//echo $cartorder->GAMonthlyTransactionTrackingCode();
		
		if (!$cartorder->details['ddsolesig'])
		{	$this->cart->RemoveMonthlyDonations();
			echo '<h3>Based on the answers you have provided, we are unable to set your Direct Debit up online. We will email you a form which you can print out to complete and sign. Please return this form to us as soon as possible.</h3>';
			if ($cartorder->CanPayOneOffDonations())
			{	echo '<div class="cartButtons"><a href="', $this->pagecontent->Link(), '6/">Pay Your Single Donations Now</a><div class="clear"></div></div>';
			}
		} else
		{	if ($this->page->failmessage)
			{	echo SITE_TEST ? $this->page->failmessage : '', '<h3>It has not been possible to set up your monthly direct debit.</h3>';
				if ($cartorder->CanPayOneOffDonations())
				{	echo '<div class="cartButtons"><a href="', $this->pagecontent->Link(), '6/">Pay Your Single Donations Now</a><div class="clear"></div></div>';
				}
			} else
			{	echo '<div class="gatewayRedirect">please wait ... creating your direct debit</div>';
			}
		}
		return ob_get_clean();
	} // end of fn DirectDebitStageContent

	public function DirectDebitConfirmStageContent()
	{	ob_start();
		$cartorder = $this->cart->CartOrder();
		if ($cartorder->extrafields['DirectDebitRef'])
		{	$this->cart->RemoveMonthlyDonations();
			echo '<div class="donateDDCompleted"><h2>Direct Debit - Complete</h2>
				<p>You have successfully set up a direct debit for your account.</p>
				<p><span class="ddCompleteLabel">Date</span><span class="ddCompleteContent">', date('j M Y', strtotime($cartorder->details['orderdate'])), '</span></p>
				<p><span class="ddCompleteLabel">Your reference</span><span class="ddCompleteContent">', $this->InputSafeString($cartorder->extrafields['DirectDebitRef']), '</span></p>
				<p><span class="ddCompleteLabel">Service user number</span><span class="ddCompleteContent">', $this->InputSafeString(EZC_SUN), '</span></p>
				<p><span class="ddCompleteLabel">Account holder name</span><span class="ddCompleteContent">', $this->InputSafeString($cartorder->extrafields['AccountHolder']), '</span></p>
				<p><span class="ddCompleteLabel">Account number</span><span class="ddCompleteContent">', $this->InputSafeString($cartorder->extrafields['AccountNumber']), '</span></p>
				<p><span class="ddCompleteLabel">Account sort code</span><span class="ddCompleteContent">', $this->InputSafeString($cartorder->extrafields['AccountSortCode']), '</span></p>
				<p><span class="ddCompleteLabel">First payment</span><span class="ddCompleteContent">On or after ', date('j M Y', strtotime($cartorder->extrafields['StartDate'])), '</span></p>
				<p>The company name that will appear on your bank statement against the direct debits will be "', EZC_SHORTNAME, '".</p>
				<p>Your Direct Debit Instruction will be confirmed to you by email or post within 5 working days. We will also notify you of the amount being collected at least 10 working days prior to the first collection. Any changes to the frequency or amount of your collections will be advised to you 10 working days in advance.</p>
				<h3>Instruction to your Bank or Building Society.</h3>
				<p>Please pay ', EZC_SUNNAME, ' from the account detailed in this instruction subject to the safeguards assured by the Direct Debit Guarantee. I understand that this instruction may remain with ', EZC_SUNNAME, ' and, if so, details will be passed electronically to my Bank / Building Society.</p>
				</div>';
		}
		if ($cartorder->CanPayOneOffDonations())
		{	echo '<div class="cartButtons"><a href="', $this->pagecontent->Link(), '6/">Pay Your Single Donations Now</a><div class="clear"></div></div>';
		}
	//	$this->VarDump($cartorder->details);
	//	$this->VarDump($cartorder->extrafields);
		if (!SITE_TEST)
		{	$_SESSION[$this->session_name] = array();
		}
	//	$this->VarDump($cartorder);
		return ob_get_clean();
	} // end of fn DirectDebitConfirmStageContent

	public function SortCodeDisplayString($sortcode = '')
	{	if (preg_match('|^[0-9]{6}$|', $sortcode))
		{	return implode('-', str_split($sortcode, 2));
		} else
		{	return $this->InputSafeString($sortcode);
		}
	} // end of fn SortCodeDisplayString

} // end of defn CartPage
?>