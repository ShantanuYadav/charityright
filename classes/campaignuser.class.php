<?php
class CampaignUser extends BlankItem
{	protected $imagesizes = array('thumb'=>array('w'=>30, 'h'=>30), 'medium'=>array('w'=>200, 'h'=>200), 'large'=>array('w'=>400, 'h'=>400), 'full'=>array('w'=>500, 'h'=>500), 'og'=>array('w'=>315, 'h'=>315));
	protected $imagelocation = '';
	protected $imagedir = '';

	public function __construct($id = 0, $slug = '')
	{	parent::__construct($id, 'campaignusers', 'cuid');
		if (!$this->id && $slug)
		{	$sql = 'SELECT * FROM campaignusers WHERE slug="' . $this->SQLSafe($slug) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
		$this->imagelocation = SITE_URL . 'img/campaignusers/';
		$this->imagedir = CITDOC_ROOT . '/img/campaignusers/';
	} // fn __construct

	public function GetCampaigns($filter = array())
	{	$tables = array('campaigns'=>'campaigns');
		$fields = array('campaigns.*');
		$where = array('cuid'=>'campaigns.cuid=' . $this->id);
		$orderby = array('campaigns.created DESC');
		if ($filter['live'])
		{	$where['enabled'] = 'campaigns.enabled=1';
		}
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cid');
	} // fn GetCampaigns

	public function GetFollowedCampaigns($filter = array())
	{	$tables = array('campaign_follows'=>'campaign_follows', 'campaigns'=>'campaigns');
		$fields = array('campaigns.*', 'campaign_follows.followtime');
		$where = array('campaign_link'=>'campaign_follows.cid=campaigns.cid', 'cuid'=>'campaign_follows.cuid=' . $this->id);
		$orderby = array('campaign_follows.followtime DESC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cid', true);
	} // fn GetFollowedCampaigns

	public function IsFollowing($cid = 0)
	{	$followed = $this->GetFollowedCampaigns();
		return isset($followed[$cid]);
	} // fn IsFollowing

	public function FollowCampaign($cid = 0)
	{	if (($cid = (int)$cid) && ($cuid = (int)$this->id))
		{	if (!$this->IsFollowing($cid))
			{	$sql = 'INSERT INTO campaign_follows SET cid=' . $cid . ', cuid=' . $cuid . ', followtime="' . $this->datefn->SQLDateTime() . '"';
				$this->db->Query($sql);
			}
		}
	} // fn FollowCampaign

	public function UnFollowCampaign($cid = 0)
	{	if (($cid = (int)$cid) && ($cuid = (int)$this->id))
		{	if ($this->IsFollowing($cid))
			{	$sql = 'DELETE FROM campaign_follows WHERE cid=' . $cid . ' AND cuid=' . $cuid;
				$this->db->Query($sql);
			}
		}
	} // fn FollowCampaign

	public function GetTeams($filter = array())
	{
		$tables = array('campaignmembers'=>'campaignmembers', 'campaigns_team'=>'campaigns AS campaigns_team', 'campaigns_member'=>'campaigns AS campaigns_member');
		$fields = array('campaigns_team.*');
		$where = array('campaigns_team_link'=>'campaigns_team.cid=campaignmembers.teamid', 'campaigns_member_link'=>'campaigns_member.cid=campaignmembers.memberid', 'cuid'=>'campaigns_member.cuid=' . $this->id);
		$orderby = array('campaigns_team.created DESC');
		if ($filter['live'])
		{	$where['enabled'] = 'campaigns_team.enabled=1';
		}
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cid', true);
	} // fn GetTeams

	public function EmailAlreadyUsed($email = '')
	{	$sql = 'SELECT cuid FROM campaignusers WHERE email="' . $this->SQLSafe($email) . '"';
		if ($this->id)
		{	$sql .= ' AND NOT cuid=' . $this->id;
		}
		//echo $sql;
		if ($result = $this->db->Query($sql))
		{	return $this->db->NumRows($result);
		}
	} // fn EmailAlreadyUsed

	public function Create($data = array(), $campaignimage = array(), $team = false)
	{
		$fail = array();
		$success = array();
		$user_fields = array('registered="' . ($now = $this->datefn->SQLDateTime()) . '"', 'lastupdated="' . $now . '"');

		if ($this->ValidEMail($data['email']))
		{	// check for already used email
			if ($this->EmailAlreadyUsed($data['email']))
			{	$fail[] = 'Email address is already registered';
			} else
			{	$user_fields['email'] = 'email="' . $this->SQLSafe($data['email']) . '"';
			}
		} else
		{	$fail[] = 'Email missing or invalid';
		}

		if ($firstname = $this->SQLSafe($data['firstname']))
		{	$user_fields['firstname'] = 'firstname="' . $firstname . '"';
		} else
		{	$fail[] = 'First name missing';
		}

		if ($lastname = $this->SQLSafe($data['lastname']))
		{	$user_fields['lastname'] = 'lastname="' . $lastname . '"';
			$user_fields['slug'] = 'slug="' . $this->TextToSlug($data['firstname'] . ' ' . $data['lastname']) . '"';
		} else
		{	$fail[] = 'Last name missing';
		}

		if ($this->ValidPhoneNumber($data['phone']))
		{	$user_fields['phone'] = 'phone="' . $this->SQLSafe($data['phone']) . '"';
		} else
		{	$fail[] = 'Phone number missing or invalid';
		}

		if (!$data['campname'])
		{	$fail[] = 'You must provide a campaign name';
		}

		if (!$data['description'])
		{	$fail[] = 'You must provide a description';
		}

		if ((int)$data['camptarget'] <= 0)
		{	$fail[] = 'Campaign target missing';
		}

		if (!$this->GetCurrency($data['campcurrency']))
		{	$fail[] = 'Campaign name missing';
		}

		if (!is_a($team, 'Campaign') || !$team->id)
		{	if (!$data['donation_country'])
			{	$fail[] = 'donation country missing';
			}
			if (!$data['donation_project'])
			{	$fail[] = 'donation project missing';
			}
		}

		if ($data['password'])
		{	if ($this->AcceptablePW($data['password']))
			{	$user_fields[] = 'password=MD5("' . $data['password'] . '")';
			} else
			{	$fail[]= 'Please enter a password of between 8 and 20 letters and numbers';
			}
		} else
		{	$fail[]= 'Password missing';
		}

		if ($campaignimage['size'])
		{	if ($this->ValidPhotoUpload($campaignimage))
			{	$image_to_upload = true;
			} else
			{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
			}
		} else
		{	if (!$avatarid = (int)$data['avatarid'])
			{	$fail[] = 'You must choose an image';
			}
		}
		
		$user_fields[] = 'contactpref=' . (int)$data['contactpref'];

		if (!$fail && ($set = implode(', ', $user_fields)))
		{	$sql = 'INSERT INTO campaignusers SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
				{	// create campaign
					$success[] = 'User created';
					$data['visible'] = 1;
					$data['enabled'] = 1;
					if (is_a($team, 'Campaign') && $team->id)
					{	$data['isteam'] = 0;
					} else
					{	unset($team);
						$data['isteam'] = $data['isteam'] ? '1' : '0';
					}
					$campaign = new Campaign();
					$camp_saved = $campaign->Create($data, $campaignimage, $id, $team);
					if ($campaign->id)
					{	$success[] = 'Campaign created';
						if (is_a($team, 'Campaign') && $team->CanBeJoined($id))
						{	$teamsql = 'INSERT INTO campaignmembers SET teamid=' . $team->id . ', memberid=' . $campaign->id . ', joinedteam="' . $this->datefn->SQLDateTime() . '"';
							$this->db->Query($teamsql);
						}
					} else
					{	$fail[] = $camp_saved['failmessage'];
					}
					$this->Get($id);
					$this->LogInOnCreate();
				}
			} //else $fail[] = $sql . ': ' . $this->db->Error();
		}

	//	$this->VarDump($cart);

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn Create

	public function SendUserCreateEmail($camp_row = array(), $password = '')
	{	if ($this->id)
		{	ob_start();
			echo '<p>Dear ', $this->InputSafeString($this->details['firstname']), ',</p>
<p>Thank you for signing up to raise money for Charity Right. You\'re ready to start raising money now. Let your friends know by sharing your campaign on Facebook and Twitter</p>';
			$mail = new HTMLMail();
			$mail->SetSubject('Welcome to Charity Right CR Stars');
			$mail->SendFromTemplate($this->details['email'], array('main_content'=>ob_get_clean()), 'default');
		}
	} // fn SendUserCreateEmail

	public function Save($data = array(), $image = array())
	{
		$fail = array();
		$success = array();
		$user_fields = array();

		if ($this->ValidEMail($data['email']))
		{	// check for already used email
			if ($this->EmailAlreadyUsed($data['email']))
			{	$fail[] = 'Email address is already registered';
			} else
			{	$user_fields['email'] = 'email="' . $this->SQLSafe($data['email']) . '"';
			}
		} else
		{	$fail[] = 'Email missing or invalid';
		}

		if ($firstname = $this->SQLSafe($data['firstname']))
		{	$user_fields['firstname'] = 'firstname="' . $firstname . '"';
			if ($data['firstname'] != $this->details['firstname'])
			{	$name_changed = true;
			}
		} else
		{	$fail[] = 'First name missing';
		}

		if ($lastname = $this->SQLSafe($data['lastname']))
		{	$user_fields['lastname'] = 'lastname="' . $lastname . '"';
			if ($data['firstname'])
			{	$user_fields['slug'] = 'slug="' . $this->TextToSlug($data['firstname'] . ' ' . $data['lastname']) . '"';
			}
			if ($data['lastname'] != $this->details['lastname'])
			{	$name_changed = true;
			}
		} else
		{	$fail[] = 'Last name missing';
		}

		if ($this->ValidPhoneNumber($data['phone']))
		{	$user_fields['phone'] = 'phone="' . $this->SQLSafe($data['phone']) . '"';
		} else
		{	$fail[] = 'Phone number missing or invalid';
		}
		
		$pref = new ContactPreferences(array('email', 'phone', 'text'));
		if (is_array($data['contactpref']) && $data['contactpref'])
		{	$user_fields['contactpref'] = 'contactpref=' . $pref->PreferencesValueFromArray($data['contactpref']);
		} else
		{	$user_fields['contactpref'] = 'contactpref=0';
		}

		if ($set = implode(', ', $user_fields))
		{	$sql = 'UPDATE campaignusers SET ' . $set . ' WHERE cuid=' . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	// create campaign
					$success[] = 'Your changes have been saved';
					$this->Get($this->id);
					if ($name_changed)
					{	$this->UpdateCampaignUsernames();
					}
				}
			} //else $fail[] = $sql . ': ' . $this->db->Error();

			if ($image['size'])
			{	if ($this->ValidPhotoUpload($image))
				{	$photos_created = 0;
					$photo_errors = 0;
					foreach ($this->imagesizes as $size_name=>$size)
					{	if (!file_exists($this->ImageFileDirectory($size_name)))
						{	mkdir($this->ImageFileDirectory($size_name));
						}
						if ($this->ReSizePhotoPNG($image['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($image['type'], 'png') ? 'png' : 'jpg'))
						{	$photos_created++;
						} else
						{	$photo_errors++;
						}
					}
					unset($image['tmp_name']);
					if ($photos_created)
					{	$success[] = 'Your image has been uploaded';
					}
					if ($photo_errors)
					{	$fail[] = 'There has been a problem uploading your image - Please try again later';
					}
				} else
				{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
				}
			} else
			{	if ($data['delmyimage'])
				{	foreach ($this->imagesizes as $size_name=>$size)
					{	if (@unlink($this->GetImageFile($size_name)))
						{	$img_deleted++;
						}
					}
					if ($img_deleted)
					{	$success[] = 'Your image has been removed';
					}
				}
			}
			$lastupdated_sql = 'UPDATE campaignusers SET lastupdated="' . $this->datefn->SQLDateTime() . '" WHERE cuid=' . $this->id;
			$this->db->Query($lastupdated_sql);
		}

	//	$this->VarDump($cart);

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // fn Save

	public function UpdateCampaignUsernames()
	{	if ($this->id && ($fullname = $this->details['firstname'] . ' ' . $this->details['lastname']))
		{	$sql = 'UPDATE campaigns SET campusername="' . $this->SQLSafe($fullname) . '" WHERE cuid=' . $this->id;
			if ($result = $this->db->Query($sql))
			{	return $this->db->AffectedRows();
			}
		}
	} // fn UpdateCampaignUsernames

	public function ChangePassword($data = array())
	{
		$fail = array();
		$success = array();
		$user_fields = array();

		if ($data['pword'])
		{	if ($this->AcceptablePW($data['pword']))
			{	if ($data['pword'] == $data['rtpword'])
				{	$user_fields[] = 'password=MD5("' . $data['pword'] . '")';
				} else
				{	$fail[]= 'Your password has been mistyped, please try again';
				}
			} else
			{	$fail[]= 'Invalid password, please retype';
			}
		} else
		{	$fail[]= 'Password missing';
		}

		if ($data['oldpword'])
		{	if (md5($data['oldpword']) != $this->details['password'])
			{	$fail[]= 'Your existing password does not match, please try again';
			}
		} else
		{	$fail[]= 'You must enter your existing Password';
		}

		if (!$fail && ($set = implode(', ', $user_fields)))
		{	$sql = 'UPDATE campaignusers SET ' . $set . ' WHERE cuid=' . $this->id;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	$success[] = 'Your password has been changed';
					$this->Get($this->id);
				}
			} //else $fail[] = $sql . ': ' . $this->db->Error();
		}

		return array('failmessage'=>implode('. ', $fail), 'successmessage'=>implode('. ', $success));

	} // fn ChangePassword

	protected function LogInOnCreate()
	{	$_SESSION[SITE_NAME]['camp_customer'] = $this->id;
	} // fn LogInOnCreate

	public function EditLink()
	{	return SITE_URL . 'my-cr-stars/edit/';
	} // fn EditLink

	public function Link()
	{	if ($this->id) return SITE_URL . 'cr-star/' . $this->id . '/' . $this->details['slug'] . '/';
	} // fn Link

	public function FullName()
	{	return $this->InputSafeString($this->details['firstname'] . ' ' . $this->details['lastname']);
	} // fn FullName

	public function HasImage($size = 'medium')
	{	if ($this->id && file_exists($this->GetImageFile($size)))
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions

	public function ImageDimensions($size = 'medium')
	{	if ($this->imagesizes[$size])
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions

	public function GetImageFile($size = 'medium')
	{	return $this->ImageFileDirectory($size) . '/' . $this->id .'.png';
	} // end of fn GetImageFile

	public function ImageFileDirectory($size = 'medium')
	{	return $this->imagedir . $this->InputSafeString($size);
	} // end of fn FunctionName

	public function GetImageSRC($size = 'medium')
	{	if ($this->HasImage($size))
		{	return $this->imagelocation . $this->InputSafeString($size) . '/' . $this->id . '.png';
		}
	} // end of fn GetImageSRC

	public function SetFBMeta(&$fb_meta)
	{	if ($this->id)
		{	$fb_meta['title'] = $this->InputSafeString($this->FullName());
			if ($src = $this->GetImageSRC('og'))
			{	$fb_meta['image'] = $src;
			}
			$fb_meta['url'] = $this->Link();
			//$fb_meta['description'] = $this->TextSnippet();
		}
	} // end of fn SetFBMeta

} // end of defn CampaignUser
?>