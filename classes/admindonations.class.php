<?php
class AdminDonations extends Base
{	
	public $sortOptions = array('created_desc'=>'latest date', 'created_asc'=>'earliest date', 'amount_desc'=>'largest donations', 'amount_asc'=>'lowest donations');
	
	public function GetMainDonations($filter = array())
	{	$tables = array('donations'=>'donations');
		$fields = array('donations.*');
		$where = array();
		
		if (!$filter['showunpaid'])
		{	$where['live'] = 'NOT donations.donationref=""';
		}
		
		if ($filter['startdate'])
		{	$where['startdate'] = 'donations.created>="' . $filter['startdate'] . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'donations.created<="' . $filter['enddate'] . ' 23:59:59"';
		}
		
		if ($filter['dontype'])
		{	$where['dontype'] = 'donations.donationtype="' . $filter['dontype'] . '"';
		}
		if ($filter['donorname'])
		{	$donorname = $this->SQLSafe($filter['donorname']);
			$where['donorname'] = '(donations.donorfirstname LIKE "%' . $donorname . '%" OR donations.donorsurname LIKE "%' . $donorname . '%" OR CONCAT(donations.donorfirstname, " ", donations.donorsurname) LIKE "%' . $donorname . '%")';
		}
		if ($filter['giftaid'])
		{	$where['giftaid'] = 'donations.giftaid=1';
		}
		if ($filter['zakat'])
		{	$where['zakat'] = 'donations.zakat=1';
		}
		if ($filter['admin'])
		{	$where['adminamount'] = 'donations.adminamount>0';
		}
		
		$orderby = array();
		switch ($filter['orderby'])
		{	case 'amount_asc':
			case 'amount_desc':
				break;
			case 'created_asc':
				$orderby[] = 'donations.created ASC';
				break;
			case 'created_desc':
			default:
				$orderby[] = 'donations.created DESC';
		}
		$donations = array();
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($donation_row = $this->db->FetchArray($result))
			{	$donation = new AdminDonation($donation_row);
				$donation_row['paid'] = $donation->SumPaid();
				$donations[$donation_row['did']] = $donation_row;
			}
		}
		switch ($filter['orderby'])
		{	case 'amount_asc':
				uasort($donations, array($this, 'UASortMainDonationsAmountAsc'));
				break;
			case 'amount_desc':
				uasort($donations, array($this, 'UASortMainDonationsAmountDesc'));
				break;
		}
		return $donations;
	} // end of fn GetMainDonations
	
	private function UASortMainDonationsAmountAsc($a, $b)
	{	$a_paid = $a['paid']['gbpamount'] + $a['paid']['gbpadminamount'];
		$b_paid = $b['paid']['gbpamount'] + $b['paid']['gbpadminamount'];
		if ($a_paid == $b_paid)
		{	return $a['did'] > $b['did'];
		} else
		{	if ($a_paid > 0)
			{	if ($b_paid > 0)
				{	return $a_paid > $b_paid;
				} else
				{	return false;
				}
			} else
			{	return true;
			}
		}
	} // end of fn UASortMainDonationsAmountAsc
	
	private function UASortMainDonationsAmountDesc($a, $b)
	{	$a_paid = $a['paid']['gbpamount'] + $a['paid']['gbpadminamount'];
		$b_paid = $b['paid']['gbpamount'] + $b['paid']['gbpadminamount'];
		if ($a_paid == $b_paid)
		{	return $a['did'] > $b['did'];
		} else
		{	return $a_paid < $b_paid;
		}
	} // end of fn UASortMainDonationsAmountDesc
	
	public function GetCampaignDonations($filter = array())
	{	$tables = array('campaigndonations'=>'campaigndonations');
		$fields = array('campaigndonations.*');
		$where = array();
		$orderby = array();//'campaigndonations.donated DESC');
		if ($filter['campaignid'] = (int)$filter['campaignid'])
		{	$where['campaignid'] = '(campaigndonations.campaignid=' . $filter['campaignid'] . ' OR campaigndonations.teamid=' . $filter['campaignid'] . ')';
		}
		if ($filter['giftaid'])
		{	$where['giftaid'] = 'campaigndonations.giftaid=1';
		}
		if (!$filter['showunpaid'])
		{	$where['live'] = 'NOT campaigndonations.donationref=""';
		}
		if ($filter['startdate'])
		{	$where['startdate'] = 'campaigndonations.donated>="' . $filter['startdate'] . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'campaigndonations.donated<="' . $filter['enddate'] . ' 23:59:59"';
		}
		if ($filter['zakat'])
		{	$where['zakat'] = 'campaigndonations.zakat=1';
		}
		if ($filter['offline'])
		{	$where['offline'] = 'campaigndonations.adminuser>0';
		}
		if ($filter['donorname'])
		{	$donorname = $this->SQLSafe($filter['donorname']);
			$where['donorname'] = '(campaigndonations.donorfirstname LIKE "%' . $donorname . '%" OR campaigndonations.donorsurname LIKE "%' . $donorname . '%" OR CONCAT(campaigndonations.donorfirstname, " ", campaigndonations.donorsurname) LIKE "%' . $donorname . '%")';
		}
		$orderby = array();
		switch ($filter['orderby'])
		{	case 'amount_asc':
			case 'amount_desc':
				break;
			case 'created_asc':
				$orderby[] = 'campaigndonations.donated ASC';
				break;
			case 'created_desc':
			default:
				$orderby[] = 'campaigndonations.donated DESC';
		}
		$donations = array();
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($donation_row = $this->db->FetchArray($result))
			{	$donations[$donation_row['cdid']] = $donation_row;
			}
		}
		switch ($filter['orderby'])
		{	case 'amount_asc':
				uasort($donations, array($this, 'UASortCampaignDonationsAmountAsc'));
				break;
			case 'amount_desc':
				uasort($donations, array($this, 'UASortCampaignDonationsAmountDesc'));
				break;
		}
		return $donations;
	} // end of fn GetCampaignDonations
	
	private function UASortCampaignDonationsAmountAsc($a, $b)
	{	$a_paid = ($a['donationref'] ? $a['gbpamount'] : 0);
		$b_paid = ($b['donationref'] ? $b['gbpamount'] : 0);
		if ($a_paid == $b_paid)
		{	return $a['cdid'] > $b['cdid'];
		} else
		{	if ($a_paid > 0)
			{	if ($b_paid > 0)
				{	return $a_paid > $b_paid;
				} else
				{	return false;
				}
			} else
			{	return true;
			}
		}
	} // end of fn UASortCampaignDonationsAmountAsc
	
	private function UASortCampaignDonationsAmountDesc($a, $b)
	{	$a_paid = ($a['donationref'] ? $a['gbpamount'] : 0);
		$b_paid = ($b['donationref'] ? $b['gbpamount'] : 0);
		if ($a_paid == $b_paid)
		{	return $a['cdid'] > $b['cdid'];
		} else
		{	return $a_paid < $b_paid;
		}
	} // end of fn UASortCampaignDonationsAmountDesc

	public function GetCombinedDonations($filter = array())
	{	$donations = array();
		if (!$filter['offline'] && (!$filter['dontype'] || ($filter['dontype'] != 'crstars')))
		{	if ($main = $this->GetMainDonations($filter))
			{	foreach ($main as $donation)
				{	$donation['table'] = 'donations';
					$donations[$donation['created'] . '~' . str_pad(count($donations), 10, '0', STR_PAD_LEFT)] = $donation;
				}
			}
		}
		if (!$filter['admin'] && (!$filter['dontype'] || ($filter['dontype'] == 'crstars')))
		{	if ($campaigns = $this->GetCampaignDonations($filter))
			{	foreach ($campaigns as $donation)
				{	$donation['table'] = 'campaigndonations';
					$donations[$donation['donated'] . '~' . str_pad(count($donations), 10, '0', STR_PAD_LEFT)] = $donation;
				}
			}
		}
		
		switch ($filter['orderby'])
		{	case 'amount_asc':
				uasort($donations, array($this, 'UASortCombinedDonationsAmountAsc'));
				break;
			case 'amount_desc':
				uasort($donations, array($this, 'UASortCombinedDonationsAmountDesc'));
				break;
			case 'created_asc':
				ksort($donations);
				break;
			case 'created_desc':
			default:
				krsort($donations);
		}
		return $donations;
	} // end of fn GetCombinedDonations
	
	private function UASortCombinedDonationsAmountAsc($a, $b)
	{	if (isset($a['cdid']))
		{	$a_paid = ($a['donationref'] ? $a['gbpamount'] : 0);
			$a_id = $a['cdid'];
		} else
		{	$a_paid = $a['paid']['gbpamount'] + $a['paid']['gbpadminamount'];
			$a_id = $a['did'];
		}
		if (isset($a['cdid']))
		{	$b_paid = ($b['donationref'] ? $b['gbpamount'] : 0);
			$b_id = $a['cdid'];
		} else
		{	$b_paid = $b['paid']['gbpamount'] + $b['paid']['gbpadminamount'];
			$b_id = $a['did'];
		}
		$b_paid = ($b['donationref'] ? $b['gbpamount'] : 0);
		if ($a_paid == $b_paid)
		{	return $a_id > $b_id;
		} else
		{	if ($a_paid > 0)
			{	if ($b_paid > 0)
				{	return $a_paid > $b_paid;
				} else
				{	return false;
				}
			} else
			{	return true;
			}
		}
	} // end of fn UASortCombinedDonationsAmountAsc
	
	private function UASortCombinedDonationsAmountDesc($a, $b)
	{	if (isset($a['cdid']))
		{	$a_paid = ($a['donationref'] ? $a['gbpamount'] : 0);
			$a_id = $a['cdid'];
		} else
		{	$a_paid = $a['paid']['gbpamount'] + $a['paid']['gbpadminamount'];
			$a_id = $a['did'];
		}
		if (isset($b['cdid']))
		{	$b_paid = ($b['donationref'] ? $b['gbpamount'] : 0);
			$b_id = $b['cdid'];
		} else
		{	$b_paid = $b['paid']['gbpamount'] + $b['paid']['gbpadminamount'];
			$b_id = $b['did'];
		}
		if ($a_paid == $b_paid)
		{	return $a_id > $b_id;
		} else
		{	return $a_paid < $b_paid;
		}
	} // end of fn UASortCombinedDonationsAmountDesc

	public function GetGiftaidDonations($filter = array())
	{	$donations = array();
		$filter['giftaid'] = true;
		$filter['dontype'] = 'oneoff';
		if ($main = $this->GetMainDonations($filter))
		{	foreach ($main as $donation)
			{	$donation['table'] = 'donations';
				$donations[$donation['created'] . '~' . str_pad(count($donations), 10, '0', STR_PAD_LEFT)] = $donation;
			}
		}
		if ($campaigns = $this->GetCampaignDonations($filter))
		{	foreach ($campaigns as $donation)
			{	$donation['table'] = 'campaigndonations';
				$donations[$donation['donated'] . '~' . str_pad(count($donations), 10, '0', STR_PAD_LEFT)] = $donation;
			}
		}
	
		krsort($donations);
		return $donations;
	} // end of fn GetGiftaidDonations

} // end of defn AdminDonations
?>
