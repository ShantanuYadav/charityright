<?php
class People extends Base
{	public $people = array();

	public function __construct($filter = array())
	{	parent::__construct();
		$tables = array('people'=>'people');
		$fields = array('people.*');
		$where = array();
		$orderby = array('people.listorder ASC', 'people.pname ASC', 'people.pid DESC');
		
		if ($filter['ptype'])
		{	$where['ptype'] = 'ptype="' . $this->SQLSafe($filter['ptype']) . '"';
		}
		if ($filter['liveonly'])
		{	$where['liveonly'] = 'live=1';
		}
		
		$this->people = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'pid');
	} // fn __construct
	
} // end of defn People
?>