<?php
class AdminCartOrders extends Base
{	
	public $sortOptions = array('created_desc'=>'latest date', 'created_asc'=>'earliest date', 'amount_desc'=>'largest donations', 'amount_asc'=>'lowest donations');
	
	public function GetMainOrders($filter = array())
	{	$tables = array('cartorders'=>'cartorders');
		$fields = array('cartorders.*');
		$where = array();
		$groupby = array();
		
		if ($filter['startdate'])
		{	$where['startdate'] = 'cartorders.orderdate>="' . $filter['startdate'] . ' 00:00:00"';
		}
		if ($filter['enddate'])
		{	$where['enddate'] = 'cartorders.orderdate<="' . $filter['enddate'] . ' 23:59:59"';
		}
		if ($filter['country'])
		{	$where['country'] = 'donorcountry="' . $this->SQLSafe($filter['country']) . '"';
		}
		if ($filter['howheard'])
		{	$where['howheard'] = 'donorhowheard="' . $this->SQLSafe($filter['howheard']) . '"';
		}
		
		if ($filter['donorname'])
		{	$donorname = $this->SQLSafe($filter['donorname']);
			$where['donorname'] = '(cartorders.donorfirstname LIKE "%' . $donorname . '%" OR cartorders.donorsurname LIKE "%' . $donorname . '%" OR CONCAT(cartorders.donorfirstname, " ", cartorders.donorsurname) LIKE "%' . $donorname . '%")';
		}
		if ($filter['giftaid'])
		{	$where['giftaid'] = 'donations.giftaid=1';
		}
		
		$orderby = array();
		switch ($filter['orderby'])
		{	case 'amount_asc':
			case 'amount_desc':
				break;
			case 'created_asc':
				$orderby[] = 'cartorders.orderdate ASC';
				break;
			case 'created_desc':
			default:
				$orderby[] = 'cartorders.orderdate DESC';
		}
		$orders = array();
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby, $groupby)))
		{	while ($order_row = $this->db->FetchArray($result))
			{	if ($filter['paidonly'])
				{	$cartorder = new CartOrder($order_row);
					if (!$cartorder->IsPaid())
					{	continue;
					}
				}
				$orders[$order_row['orderid']] = $order_row;
			}
		}
	/*	switch ($filter['orderby'])
		{	case 'amount_asc':
				uasort($donations, array($this, 'UASortMainDonationsAmountAsc'));
				break;
			case 'amount_desc':
				uasort($donations, array($this, 'UASortMainDonationsAmountDesc'));
				break;
		}*/
		return $orders;
	} // end of fn GetMainOrders
	
	private function UASortMainDonationsAmountAsc($a, $b)
	{	$a_paid = $a['paid']['gbpamount'] + $a['paid']['gbpadminamount'];
		$b_paid = $b['paid']['gbpamount'] + $b['paid']['gbpadminamount'];
		if ($a_paid == $b_paid)
		{	return $a['did'] > $b['did'];
		} else
		{	if ($a_paid > 0)
			{	if ($b_paid > 0)
				{	return $a_paid > $b_paid;
				} else
				{	return false;
				}
			} else
			{	return true;
			}
		}
	} // end of fn UASortMainDonationsAmountAsc
	
	private function UASortMainDonationsAmountDesc($a, $b)
	{	$a_paid = $a['paid']['gbpamount'] + $a['paid']['gbpadminamount'];
		$b_paid = $b['paid']['gbpamount'] + $b['paid']['gbpadminamount'];
		if ($a_paid == $b_paid)
		{	return $a['did'] > $b['did'];
		} else
		{	return $a_paid < $b_paid;
		}
	} // end of fn UASortMainDonationsAmountDesc

	public function GetGiftaidDonations($filter = array())
	{	$donations = array();
		$filter['giftaid'] = true;
		$filter['dontype'] = 'oneoff';
		if ($main = $this->GetMainDonations($filter))
		{	foreach ($main as $donation)
			{	$donation['table'] = 'donations';
				$donations[$donation['created'] . '~' . str_pad(count($donations), 10, '0', STR_PAD_LEFT)] = $donation;
			}
		}
		if ($campaigns = $this->GetCampaignDonations($filter))
		{	foreach ($campaigns as $donation)
			{	$donation['table'] = 'campaigndonations';
				$donations[$donation['donated'] . '~' . str_pad(count($donations), 10, '0', STR_PAD_LEFT)] = $donation;
			}
		}
	
		krsort($donations);
		return $donations;
	} // end of fn GetGiftaidDonations

} // end of defn AdminCartOrders
?>