<?php
class BookOrder extends BlankItem
{	public $items = array();

	public function __construct($id = 0)
	{	parent::__construct($id, 'bookorders', 'orderid');
	} // fn __construct
	
	public function ResetExtra()
	{	$this->items = array();
	} // fn ResetExtra
	
	public function GetExtra()
	{	$sql = 'SELECT bookorderitems.* FROM bookorderitems WHERE bookorderitems.orderid=' . $this->id . ' ORDER BY orderitem';
		$this->items = $this->db->ResultsArrayFromSQL($sql, 'orderitem');
	} // fn GetExtra
	
	public function CreateFromCart(BookingCart $cart)
	{	
		$fail = array();
		$success = array();
		$order_fields = array('orderdate="' . $this->datefn->SQLDateTime() . '"');
		$currency = '';
		
		if ($this->ValidEMail($cart->customer['email']))
		{	$order_fields['email'] = 'email="' . $this->SQLSafe($cart->customer['email']) . '"';
		} else
		{	$fail[] = 'Email missing or invalid';
		}
		
		if ($firstname = $this->SQLSafe($cart->customer['firstname']))
		{	$order_fields['firstname'] = 'firstname="' . $firstname . '"';
		} else
		{	$fail[] = 'First name missing';
		}
		
		if ($lastname = $this->SQLSafe($cart->customer['lastname']))
		{	$order_fields['lastname'] = 'lastname="' . $lastname . '"';
		} else
		{	$fail[] = 'Last name missing';
		}
		
		if ($postcode = $this->SQLSafe($cart->customer['postcode']))
		{	$order_fields['postcode'] = 'postcode="' . $postcode . '"';
		} else
		{	$fail[] = 'Postcode missing';
		}
		
		if ($address1 = $this->SQLSafe($cart->customer['address1']))
		{	$order_fields['address1'] = 'address1="' . $address1 . '"';
		} else
		{	$fail[] = 'Address missing';
		}
		
		if ($address2 = $this->SQLSafe($cart->customer['address2']))
		{	$order_fields['address2'] = 'address2="' . $address2 . '"';
		}
		
		if ($city = $this->SQLSafe($cart->customer['city']))
		{	$order_fields['city'] = 'city="' . $city . '"';
		} else
		{	$fail[] = 'City missing';
		}
		
		if ($phone = $this->SQLSafe($cart->customer['phone']))
		{	$order_fields['phone'] = 'phone="' . $phone . '"';
		} else
		{	$fail[] = 'Phone missing';
		}
	
		if ($this->GetCountry($cart->customer['country']))
		{	$order_fields['country'] = 'country="' . $cart->customer['country'] . '"';
		} else
		{	$fail[] = 'Country missing';
		}
		
		$orderitems = array();
		$currency = '';
		if (is_array($cart->tickets))
		{	foreach ($cart->tickets as $ttid=>$ticket_qty)
			{	$ticket = new TicketType($ttid);
				if ($ticket->id && ($ticket_qty = (int)$ticket_qty))
				{	if ($ticket->details['price'] > 0)
					{	if ($currency)
						{	if ($currency !== $ticket->eventdate['currency'])
							{	$fail[] = 'You cannot mix currencies in the same basket';
							}
						} else
						{	$currency = $ticket->eventdate['currency'];
						}
					}
					$orderitems[] = array('ttid=' . $ticket->id, 'quantity=' . $ticket_qty, 'price=' . round($ticket->details['price'], 2));
				} else
				{	$fail[] = 'Ticket data not valid';
				}
			}
		}
		
		if ($currency)
		{	$order_fields['currency'] = 'currency="' . $currency . '"';
		}
		
		if ($orderitems)
		{	if (!$fail && ($set = implode(', ', $order_fields)))
			{	$sql = 'INSERT INTO bookorders SET ' . $set;
				if ($result = $this->db->Query($sql))
				{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
					{	$this->Get($id);
						if ($this->id)
						{	foreach ($orderitems as $orderitem)
							{	$orderitem[] = 'orderid=' . $this->id;
								$itemsql = 'INSERT INTO bookorderitems SET ' . implode(', ', $orderitem);
								$this->db->Query($itemsql);
							}
							$this->Get($this->id);
							if (!$this->PaymentNeeded())
							{	// then mark as free and complete
								//$this->VarDump($this->items);
								//echo $this->TotalPrice();
								$sql = 'UPDATE bookorders SET payref="' . $this->FreePayRef() . '", paid="' . $this->details['orderdate'] . '" WHERE orderid=' . $this->id;
								$this->db->Query($sql);
								$this->Get($this->id);
								$cart->EmptyCartTickets();
								if ($booked = $this->CreateBookingsFromItems())
								{	$success[] = 'Your places have been booked and your tickets will be emailed to you';
								}
							}
						}
					}
				}
			}
		} else
		{	$fail[] = 'No tickets to book';
		}
		
	//	$this->VarDump($order_fields);
	//	$this->VarDump($orderitems);
		
	//	$this->VarDump($cart);
	
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // fn CreateFromCart
	
	public function CreateBookingsFromItems()
	{	$created = 0;
		if (is_array($this->items))
		{	foreach ($this->items as $item)
			{	for ($i = 0; $i < $item['quantity']; $i++)
				{	$booksql = 'INSERT INTO bookings SET ttid=' . $item['ttid'] . ', orderitem=' . $item['orderitem'];
					if ($result = $this->db->Query($booksql))
					{	$created += $this->db->AffectedRows();
					}
				}
			}
		}
		if ($created)
		{	$this->SendTickets();
		}
		return $created;
	} // fn CreateBookingsFromItems
	
	public function FreePayRef()
	{	return 'FREE~' . str_pad($this->id, 8, '0', STR_PAD_LEFT);
	} // fn FreePayRef
	
	public function PaymentNeeded()
	{	return $this->TotalPrice() > 0;
	} // fn PaymentNeeded
	
	public function TotalPrice()
	{	$price = 0;
		foreach ($this->items as $item)
		{	$ticket = new TicketType($item['ttid']);
			$price += ($item['quantity'] * $ticket->details['price']);
		}
		return $price;
	} // fn TotalPrice
	
	public function TicketCount()
	{	$count = 0;
		foreach ($this->items as $item)
		{	$count += $item['quantity'];
		}
		return $count;
	} // fn count
	
	public function GetBookings()
	{	$itemids = array();
		if (is_array($this->items))
		{	foreach ($this->items as $item)
			{	$itemids[$item['orderitem']] = $item['orderitem'];
			}
		}
		if ($itemids)
		{	$sql = 'SELECT bookings.* FROM bookings WHERE bookings.orderitem IN (' . implode(',', $itemids) . ') ORDER BY bookings.orderitem';
			return $this->db->ResultsArrayFromSQL($sql, 'bid');
		}
		return false;
	} // fn GetBookings
	
	public function SendTickets()
	{	if ($bookings = $this->GetBookings())
		{	$mail = new HTMLMail();
			$mail->SetSubject('Your Charity Right event tickets');
			$html = '';
			$plain = '';
			
			ob_start();
			$tickets = array();
			$eventdates = array();
			$venues = array();
			echo '<p>Your Charity Right event tickets</p><ul style="list-style: none; border-top: 1px solid #666666; padding: 0px; margin: 10px 0px;">
';
			foreach($bookings as $booking)
			{	if (!$tickets[$booking['ttid']])
				{	$tickets[$booking['ttid']] = new TicketType($booking['ttid']);
					if (!$eventdates[$tickets[$booking['ttid']]->details['edid']])
					{	$eventdates[$tickets[$booking['ttid']]->details['edid']] = new EventDate($tickets[$booking['ttid']]->eventdate);
					}
					if (!$venues[$eventdates[$tickets[$booking['ttid']]->details['edid']]->details['venue']])
					{	$venues[$eventdates[$tickets[$booking['ttid']]->details['edid']]->details['venue']] = new EventVenue($eventdates[$tickets[$booking['ttid']]->details['edid']]->venue);
					}
				}
				echo '
<li style="padding: 20px; border-bottom: 1px solid #666666;"><table><tbody><tr><td style="border: none; padding: 0px 20px 0px 0px; vertical-align: top;" rowspan="3">#', $booking['bid'], '</td>
<td style="border: none; padding: 0px 20px 5px; vertical-align: top;">', $this->InputSafeString($tickets[$booking['ttid']]->event['eventname']), 
'</td>
<td style="border: none; padding: 0px 20px 0px; vertical-align: top;" rowspan="3">Booked by:<br />', $this->InputSafeString($this->details['firstname'] . ' ' . $this->details['lastname']), ' <br />(', $this->details['email'], ')</td>
</tr><tr><td style="border: none; padding: 0px 20px 5px; vertical-align: top;">', $eventdates[$tickets[$booking['ttid']]->details['edid']]->DatesString(), '</td>
</tr><tr><td style="border: none; padding: 0px 20px; vertical-align: top;">at ', $this->InputSafeString($venues[$eventdates[$tickets[$booking['ttid']]->details['edid']]->details['venue']]->LocationString()), '</td>
</tr></tbody></table></li>';
			}
			echo '</ul>';
			$mail->SendFromTemplate($this->details['email'], array('main_content'=>ob_get_clean()), 'default');
			return true;
		}
	} // fn SendTickets
	
	public function PaymentButton()
	{	ob_start();
		$wp = new WorldPayRedirect();
		echo $wp->EventBookingOrderButton($this);
		return ob_get_clean();
	} // end of fn PaymentButton
	
	public function UpdateFromWorldPayPost($post = array())
	{	$fail = array();
		$success = array();
		
		$subfields = array();//'gateway="worldpay"');

		if ($this->id)
		{	if ($this->details['payref'])
			{	$fail[] = 'booking order already paid';
			} else
			{	$ppsubid = 'WP~' . $post['transId'];
				$subfields[] = 'payref="' . $ppsubid . '"';
				$subfields[] = 'paid="' . $this->datefn->SQLDateTime() . '"';
			
				$fullamount = round($post['amount'], 2);
				if ($fullamount == ($amount = $this->TotalPrice()))
				{	if ($this->details['currency'] == 'GBP')
					{	$subfields[] = 'gbpamountpaid=' . $fullamount;
					} else
					{	$subfields[] = 'gbpamountpaid=' . round($fullamount / $this->GetCurrency($this->details['currency'], 'convertrate'), 2);
					}
				} else
				{	$fail[] = 'amount/mc_gross doesn\'t match (' . $amount . ' : ' . $post['amount'] . ')';
				}
			
				if (!$fail)
				{	$subset = implode(', ', $subfields);
					$subsql = 'UPDATE bookorders SET ' . $subset . ' WHERE orderid=' . $this->id;
					if (($result = $this->db->Query($subsql)) && $this->db->AffectedRows())
					{	$success[] = 'events booking order ' . $this->id . ' updated';
						$this->Get($this->id);
						$this->CreateBookingsFromItems();
					} else $fail[] = $subsql . '---' . $this->db->Error();
				}
			}
		} else
		{	$fail[] = 'booking order not found';
		}
		//return array();
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn UpdateFromWorldPayPost
	
} // end of defn BookOrder
?>