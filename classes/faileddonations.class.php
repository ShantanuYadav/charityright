<?php
class FailedDonations extends Base
{	private $hours_back = 48;
	private $hours_since = 2;
	
	public function __construct()
	{	parent::__construct();
	} // end of fn __construct
	
	public function GetFailedDonors()
	{	$donors = array();
		$donors_raw = array();
		
		$tables = array('donations'=>'donations');
		$fields = array('donations_all'=>'donations.*');
		$where = array('donationref'=>'donations.donationref=""', 'hours_back'=>'donations.created>="' . $this->datefn->SQLDateTime(strtotime('-' . (int)($this->hours_back + $this->hours_since) . ' hours')) . '"', 'hours_since'=>'donations.created<="' . $this->datefn->SQLDateTime(strtotime('-' . (int)$this->hours_since . ' hours')) . '"');
		$orderby = array('created'=>'donations.created');
		
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	if (!$donors_raw[$row['donoremail']])
				{	$donors_raw[$row['donoremail']] = array('email'=>$row['donoremail'], 'donations'=>0, 'crdonations'=>0, 'lasttime'=>'', 'lastdonation'=>array());
				}
				if ($row['created'] > $donors_raw[$row['donoremail']]['lasttime'])
				{	$donors_raw[$row['donoremail']]['lasttime'] = $row['created'];
				//	$donors_raw[$row['donoremail']]['lastdonation'] = $row;
				}
				$donors_raw[$row['donoremail']]['donations']++;
			}
		} else echo $thid->db->Error();
		
		$tables = array('campaigndonations'=>'campaigndonations');
		$fields = array('campaigndonations_all'=>'campaigndonations.*');
		$where = array('donationref'=>'campaigndonations.donationref=""', 'hours_back'=>'campaigndonations.donated>="' . $this->datefn->SQLDateTime(strtotime('-' . (int)($this->hours_back + $this->hours_since) . ' hours')) . '"', 'hours_since'=>'campaigndonations.donated<="' . $this->datefn->SQLDateTime(strtotime('-' . (int)$this->hours_since . ' hours')) . '"');
		$orderby = array('donated'=>'campaigndonations.donated');
		
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	if (!$donors_raw[$row['donoremail']])
				{	$donors_raw[$row['donoremail']] = array('email'=>$row['donoremail'], 'donations'=>0, 'crdonations'=>0, 'lasttime'=>'');
				}
				if ($row['donated'] > $donors_raw[$row['donoremail']]['last'])
				{	$donors_raw[$row['donoremail']]['lasttime'] = $row['donated'];
				//	$donors_raw[$row['donoremail']]['lastdonation'] = $row;
				}
				$donors_raw[$row['donoremail']]['crdonations']++;
			}
		} else echo $thid->db->Error();
		
		foreach ($donors_raw as $email=>$details)
		{	if (!$this->DonationMadeAfter($email, $details['lasttime']))
			{	$donors[$email] = $details;
			}
		}
		
		$this->VarDump($donors_raw);
		$this->VarDump($donors);
		
		return $donors;
	} // end of fn GetFailedDonors
	
	private function DonationMadeAfter($email = '', $time = '')
	{	$sql = 'SELECT did FROM donations WHERE NOT donations.donationref="" AND donations.donoremail="' . $email . '" AND donations.created>"' . $time . '"';
		if ($result = $this->db->Query($sql))
		{	if ($this->db->NumRows($result))
			{	return true;
			}
		}
		$sql = 'SELECT cdid FROM campaigndonations WHERE NOT campaigndonations.donationref="" AND campaigndonations.donoremail="' . $email . '" AND campaigndonations.donated>"' . $time . '"';
		if ($result = $this->db->Query($sql))
		{	if ($this->db->NumRows($result))
			{	return true;
			}
		}
		return false;
	} // end of fn DonationMadeAfter
	
} // end of defn FailedDonations
?>