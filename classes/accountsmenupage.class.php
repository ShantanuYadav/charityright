<?php
class AccountsMenuPage extends AdminPage
{	
	public function __construct()
	{	parent::__construct('ACCOUNTS');
	} //  end of fn __construct

	public function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('accounts'))
		{	$this->AccountsLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	public function AccountsLoggedInConstruct()
	{	$this->breadcrumbs->AddCrumb('', 'Accounts');
		$this->css[] = 'accounts.css';
	} // end of fn AccountsLoggedInConstruct
	
	public function AccountsBody()
	{	
	} // end of fn AccountsBody
	
	public function AdminBodyMain()
	{	if ($this->user->CanUserAccess('accounts'))
		{	$this->AccountsBody();
		}
	} // end of fn AdminBodyMain
	
} // end of defn AccountsMenuPage
?>