<?php
class PCAPredictAPI extends Base
{	protected $apikey = 'BW98-FY13-BH78-FC29';
	protected $pcap_url = 'https://services.postcodeanywhere.co.uk/BankAccountValidation/Interactive/Validate/v2.00/json.ws';
	protected $test_endpoint = 'http://charityright.your-website-demo.co.uk/pcatest.php';

	public function __construct()
	{	parent::__construct();
	} // fn __construct

	protected function GetJSONPostResponse($data = '')
	{	if ($data && is_array($data))
		{	$post = array();
			$data['Key'] = $this->apikey;
			foreach ($data as $key=>$value)
			{	$post[] = $key . '=' . urlencode($value);
			}
			if ($this->pcap_url)
			{	$url = $this->pcap_url . '?' . implode('&', $post);
			} else 
			{	$url = $this->test_endpoint;//CITDOC_ROOT . '/pcatest.php?' . implode('&', $post);
			}
			if (SITE_TEST && $this->test_endpoint)
			{	$url = $this->test_endpoint;
			}
//echo $url;
			if (is_array($full_return = json_decode($raw = file_get_contents($url), true)))
			{	//$this->VarDump($full_return);
				return $full_return[0];
			} //else echo $raw;
			return array();
			
		}
	} // end of fn GetJSONPostResponse
	
	public function CheckBankAccount(&$data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if (!$data['dd_accountname'] || !$this->AccountNameSafe($data['dd_accountname']))
		{	$fail[] = 'Account name missing';
		}
	
		if ($data['dd_accountsortcode'])
		{	if ($this->ValidSortCode($data['dd_accountsortcode']))
			{	$fields['SortCode'] = preg_replace('|[^0-9]|', '', $data['dd_accountsortcode']);
			} else
			{	$fail[] = 'sort code invalid';
			}
		} else
		{	$fail[] = 'sort code missing';
		}
		if ($fields['AccountNumber'] = $data['dd_accountnumber'])
		{	if (!$this->ValidBankAccountNumber($fields['AccountNumber']))
			{	$fail[] = 'account number invalid';
			}
		} else
		{	$fail[] = 'account number missing';
		}
		
		if (!$fail)
		{	//$this->VarDump($data);
			if ($result = $this->GetJSONPostResponse($fields))
			{	if ($result['IsCorrect'] == 'True')
				{	//$this->VarDump($result);
					if ($result['CorrectedSortCode'])
					{	$data['dd_accountsortcode'] = $result['CorrectedSortCode'];
					}
					if ($result['CorrectedAccountNumber'])
					{	$data['dd_accountnumber'] = $result['CorrectedAccountNumber'];
					}
					$success[] = 'account details approved';
				} else
				{	$fail[] = 'Please check your account details, we have been unable to confirm your bank details';
				//	$this->VarDump($result);
				//	$this->VarDump($fields);
				}
			} else
			{	$fail[] = 'we are unable to approve your account details at this time';
			}
		}
	
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn CheckBankAccount

	public function CheckBankAccountNew(&$data = array())
	{	$fail = array();
		$success = array();
		$fields = array();
		
		if ($data['dd_accountsortcode'])
		{	if ($this->ValidSortCode($data['dd_accountsortcode']))
			{	$fields['SortCode'] = preg_replace('|[^0-9]|', '', $data['dd_accountsortcode']);
			} else
			{	$fail[] = 'sort code invalid';
			}
		} else
		{	$fail[] = 'sort code missing';
		}
		if ($fields['AccountNumber'] = $data['dd_accountnumber'])
		{	if (!$this->ValidBankAccountNumber($fields['AccountNumber']))
			{	$fail[] = 'account number invalid';
			}
		} else
		{	$fail[] = 'account number missing';
		}
		
		if (!$fail)
		{	//$this->VarDump($data);
			if ($result = $this->GetJSONPostResponse($fields))
			{	if ($result['IsCorrect'] == 'True')
				{	//$this->VarDump($result);
					if ($result['CorrectedSortCode'])
					{	$data['dd_accountsortcode'] = $result['CorrectedSortCode'];
					}
					if ($result['CorrectedAccountNumber'])
					{	$data['dd_accountnumber'] = $result['CorrectedAccountNumber'];
					}
					$success[] = 'account details approved';
				} else
				{	$fail[] = 'Please check your account details, we have been unable to confirm your bank details';
				//	$this->VarDump($result);
				//	$this->VarDump($fields);
				}
			} else
			{	$fail[] = 'we are unable to approve your account details at this time';
			}
		}
	
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn CheckBankAccount
	
} // end of defn PCAPredictAPI
?>