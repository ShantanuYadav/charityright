<?php
class Post extends BlankItem
{	public $categories = array();
	public $images = array();
	public $sections = array();
	public $videos = array();
	public $posts = array();
	protected $template_dir;
	protected $imagelocation = '';
	protected $imagedir = '';
	
	public function __construct($id = 0, $slug = '')
	{	parent::__construct($id, 'posts', 'postid');
		$this->template_dir = CITDOC_ROOT . '/templates/posts/';
		$this->imagelocation = SITE_URL . 'img/post_banners/';
		$this->imagedir = CITDOC_ROOT . '/img/post_banners/';
		if (!$this->id && $slug)
		{	$sql = 'SELECT * FROM posts WHERE slug="' . $this->SQLSafe($slug) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
	} // fn __construct
	
	public function ResetExtra()
	{	$this->categories = array();
		$this->images = array();
		$this->sections = array();
		$this->videos = array();
		$this->posts = array();
	} // end of fn ResetExtra
	
	public function GetExtra()
	{	$this->GetCategories();
		$this->GetImages();
		$this->GetSections();
		$this->GetVideos();
		$this->GetPosts();
	} // end of fn GetExtra
	
	public function GetSections()
	{	$tables = array('postsections'=>'postsections');
		$fields = array('postsections.*');
		$where = array('postid'=>'postsections.postid=' . $this->id);
		$orderby = array('postsections.psection');
		$this->sections = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'psection', true);
	} // fn GetSections
	
	public function Link()
	{	if ($this->id) return SITE_URL . 'post/' . $this->details['slug'] . '/';
	} // fn Link
	
	public function VideoLink()
	{	if ($this->id && $this->videos) return SITE_URL . 'videos/p/' . $this->details['slug'] . '/';
	} // fn VideoLink
	
	public function GetCategories()
	{	$tables = array('postsforcats'=>'postsforcats', 'postcategories'=>'postcategories');
		$fields = array('postcategories.*');
		$where = array('postcategories_link'=>'postcategories.pcatid=postsforcats.pcatid', 'postid'=>'postsforcats.postid=' . $this->id);
		$orderby = array('postcategories.catname');
		$this->categories = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'pcatid');
	} // fn GetCategories
	
	public function GetPosts()
	{	$tables = array('postposts'=>'postposts', 'posts'=>'posts');
		$fields = array('posts.*', 'postposts.listorder');
		$where = array('posts_link'=>'postposts.postid=posts.postid', 'postid'=>'postposts.parentid=' . $this->id);
		$orderby = array('postposts.listorder', 'posts.postdate DESC', 'posts.updated DESC');
		//echo $this->db->BuildSQL($tables, $fields, $where, $orderby);
		$this->posts = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'postid', true);
	} // fn GetPosts
	
	public function GetVideos()
	{	$tables = array('postvideos'=>'postvideos', 'videos'=>'videos');
		$fields = array('videos.*');
		$where = array('postvideos_link'=>'postvideos.vid=videos.vid', 'postid'=>'postvideos.postid=' . $this->id);
		$orderby = array('videos.uploaded DESC');
		$this->videos = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'vid');
	} // fn GetVideos
	
	public function HasCategory($slug = '')
	{	foreach ($this->categories as $cat_row)
		{	if ($cat_row['slug'] == $slug)
			{	return true;
			}
		}
		return false;
	} // fn CategoriesList
	
	public function CategoriesList($sep = ', ')
	{	$cats = array();
		foreach ($this->categories as $cat_row)
		{	$cat = new PostCategory($cat_row);
			if ($cat->id)
			{	ob_start();
				echo '<a href="', $cat->Link(), '">', $this->InputSafeString($cat->details['catname']), '</a>';
				$cats[] = ob_get_clean();
			}
		}
		return implode($sep, $cats);
	} // fn CategoriesList
	
	protected function AssignCategory()
	{	
	} // fn AssignCategory
	
	public function GetImages()
	{	$tables = array('postimages'=>'postimages');
		$fields = array('postimages.*');
		$where = array('postid'=>'postimages.postid=' . $this->id);
		$orderby = array('postimages.listorder', 'postimages.piid');
		$this->images = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'piid', true);
	} // fn GetImages
	
	public function MainImage()
	{	if ($this->images)
		{	foreach ($this->images as $image_row)
			{	return new PostImage($image_row);
			}
		}
	} // fn MainImage
	
	public function MainImageSource($size = 'full')
	{	if ($image = $this->MainImage())
		{	return $image->GetImageSRC($size);
		}
	} // fn MainImageSource

	public function GetQuickDonateOptions($live_only = true)
	{	$options = json_decode($this->details['quickdonate'], true);
		return ($options['live'] || !$live_only) ? $options : array();
	} // end of fn GetQuickDonateOptions
	
	public function TemplateFile()
	{	return $this->template_dir . $this->details['template'] . '.php';
	} // fn TemplateFile

	public function HasImage()
	{	if ($this->id && file_exists($this->GetImageFile()))
		{	return true;
		}
	} // end of fn ImageDimensions

	public function GetImageFile()
	{	return $this->imagedir . $this->id .'.png';
	} // end of fn GetImageFile

	public function GetImageSRC()
	{	if ($this->HasImage())
		{	return $this->imagelocation . $this->id . '.png';
		}
	} // end of fn GetImageSRC
	
} // end of defn Post
?>