<?php
class NewsImages extends NewsImageBase
{	var $images = array();

	function __construct()
	{	parent::__construct();
		$this->Get();
	} //  end of fn __construct
	
	function Get($id = 0)
	{	$this->ReSet();
		
		if ($dir = opendir($this->filedir))
		{	
			while ($file = readdir($dir))
			{	
				if (!is_dir($file))
				{	foreach ($this->suffixes as $suffix=>$suffix_details)
					{	if (strstr($file, $suffix))
						{	$this->images[] = new NewsImage($file);
							break;
						}
					}
				}
				
			}
			closedir($dir);
		}
		
	} // end of fn Get
	
	function ReSet()
	{	$this->images = array();
	} // end of fn ReSet
	
	function AdminListImages()
	{	
		echo '<table><tr class="newlink"><th colspan="5"><a href="newsimage.php">Add new image</a></th></tr><tr><th>Image</th><th>Actual size</th><th>Location</th><th>Used in</th><th>Actions</th></tr>';
			foreach ($this->images as $image)
		{	echo '<tr class="stripe', $i++ % 2, '"><td><img width="150px" src="', $image->ImageLink(), '" /></td><td>', $image->SizeString(), '</td><td>', 
					$image->ImageLink(), '</td><td>', $image->ListUses(), '</td><td>';
			//$this->VarDump($image);
			if ($image->CanDelete())
			{	echo '<a href="newsimages.php?delimage=', urlencode($image->filename), '">delete</a>';
			}
			echo '</td></tr>';
		}
		echo '</table>';
	} // end of fn AdminListImages
	
} // end of defn NewsImages
?>