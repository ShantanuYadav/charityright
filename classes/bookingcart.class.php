<?php
class BookingCart extends Base
{	public $tickets = array();
	public $customer = array();

	public function __construct($data = array())
	{	parent::__construct();
		$this->GetFromSession($data);
	} // fn __construct

	public function Reset()
	{	$this->tickets = array();
		$this->customer = array();
	} // fn Reset

	public function GetFromSession($data = array())
	{	$this->Reset();
		if (is_array($data['event_tickets']))
		{	$this->tickets = $data['event_tickets'];
		}
		if (is_array($data['event_customer']))
		{	$this->customer = $data['event_customer'];
		}
	} // fn GetFromSession

	public function CanCheckout()
	{	return is_array($this->tickets) && count($this->tickets);
	} // fn CanCheckout

	public function BookCartHeader()
	{	ob_start();
		if ($this->tickets)
		{	echo '<a href="', SITE_SUB, '/bookcart.php"><span class="bchLabel">Your cart</span><span class="bchCount">', array_sum($this->tickets), '</span></a>';
		}
		return ob_get_clean();
	} // fn BookCartHeader

	public function EmptyCartTickets()
	{	$this->tickets = array();
		unset($_SESSION['events']['event_tickets']);
		unset($_SESSION['events']['currency']);
	} // fn EmptyCartTickets

	public function SaveAddress($data = array(), &$session)
	{
		$fail = array();
		$success = array();
		$session = array();

		if ($data['email'])
		{	$session['email'] = $data['email'];
		} else
		{	$fail[] = 'email address missing (we will email your tickets)';
		}

		if ($data['firstname'])
		{	$session['firstname'] = $data['firstname'];
		} else
		{	$fail[] = 'first name missing';
		}

		if ($data['lastname'])
		{	$session['lastname'] = $data['lastname'];
		} else
		{	$fail[] = 'last name missing';
		}

		if ($data['address1'])
		{	$session['address1'] = $data['address1'];
			if ($data['address2'])
			{	$session['address2'] = $data['address2'];
			}
		} else
		{	if ($data['address2'])
			{	$session['address1'] = $data['address2'];
			} else
			{	$fail[] = 'address missing';
			}
		}

		if ($data['postcode'])
		{	$session['postcode'] = $data['postcode'];
		} else
		{	$fail[] = 'postcode missing';
		}

		if ($data['city'])
		{	$session['city'] = $data['city'];
		} else
		{	$fail[] = 'city / town missing';
		}

		if ($data['phone'])
		{	$session['phone'] = $data['phone'];
		} else
		{	$fail[] = 'phone missing';
		}

		if ($this->GetCountry($data['country']))
		{	$session['country'] = $data['country'];
		} else
		{	$fail[] = 'country missing';
		}

		if (!$fail)
		{	$success[] = 'all address fields correct';
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn SaveAddress

	public function CartHeader($container = '')
	{	ob_start();
		if ($this->tickets)
		{	$total = 0;
			$eventdates = array();
			$events = array();
			echo '<div class="cartHTable"><ul><li class="chHeaderTicket"><div class="chHeaderTicketEvent">Event</div><div class="chHeaderTicketName">Ticket</div><div class="chHeaderTicketQty">Quantity</div><div class="chHeaderTicketLinePrice">Price</div><div class="clear"></div></li>';
			foreach ($this->tickets as $ttid=>$number)
			{	$ticket = new TicketType($ttid);
				$eventdate = new EventDate($ticket->eventdate);
				$event = new Event($ticket->event);
				$cursymbol = $this->GetCurrency($ticket->eventdate['currency'], 'cursymbol');
				echo '<li><div class="chTicketImage"';
				if ($image = $event->GetImageSRC('thumb'))
				{	echo ' style="background-image: url(\'', $image, '\');"';
				}
				echo '></div><div class="chTicketDesc">', $this->InputSafeString($ticket->event['eventname']), ' at ', $this->InputSafeString($ticket->venue['venuename']), '<br />', $eventdate->DatesString(), '</div><div class="chTicketName">', $this->InputSafeString($ticket->details['ttypename']), '</div><div class="chTicketNumberContainer"><div class="chTicketNumber">', $number, '</div><div class="chTicketNumberChanger"><a onclick="CartUpdateTicketNumber(', $ticket->id, ',1);">+</a><a onclick="CartUpdateTicketNumber(', $ticket->id, ',-1);">-</a></div></div><div class="chTicketUnitPrice">';
				if ($ticket->details['price'] > 0)
				{	echo 'at ', $cursymbol, number_format($ticket->details['price'], 2);
				} else
				{	echo '&nbsp;';
				}
				echo '</div><div class="chTicketLinePrice">';
				if ($ticket->details['price'] > 0)
				{	echo $cursymbol, number_format($subtotal = $ticket->details['price'] * $number, 2);
					$total += $subtotal;
				} else
				{	echo 'FREE';
				}

				echo '</div><div class="clear"></div></li>';
			}
			if ($total > 0)
			{	echo '<li class="chTicketTotal"><div class="chTicketDesc">Total to pay</div><div class="chTicketName">&nbsp;</div><div class="chTicketNumberContainer">&nbsp;</div><div class="chTicketUnitPrice">&nbsp;</div><div class="chTicketLinePrice">', $cursymbol, number_format($total, 2), '</div><div class="clear"></div></li>';
			}
			echo '</ul></div>';
		} else
		{	echo '<div class="cartHEmpty">There are no tickets in your cart</div>';
		}
		return ob_get_clean();
	} // fn CartHeader

	public function AddressForm()
	{	ob_start();
		echo '<form action="" method="post" class="form-rows">';

			echo '<div class="form-row">';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<label for="email" class="title-label">Your email address</label>';
					echo '</div>';
				echo '</div>';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<input type="email" id="email" name="email" value="', $this->InputSafeString($this->customer['email']), '" required/>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="form-row">';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<label for="firstname" class="title-label">Your first name</label>';
					echo '</div>';
				echo '</div>';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<input type="text" id="firstname" name="firstname" value="', $this->InputSafeString($this->customer['firstname']), '" required/>';
					echo '</div>';
				echo '</div>';
			echo '</div>';


			echo '<div class="form-row">';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<label for="lastname" class="title-label">Your last name</label>';
					echo '</div>';
				echo '</div>';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<input type="text" id="lastname" name="lastname" value="', $this->InputSafeString($this->customer['lastname']), '" required/>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="form-row">';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<label for="address1" class="title-label">Address</label>';
					echo '</div>';
				echo '</div>';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<input type="text" id="address1" name="address1" value="', $this->InputSafeString($this->customer['address1']), '" required/>';
						echo '<input type="text" id="address2" name="address2" value="', $this->InputSafeString($this->customer['address2']), '" style="margin-top:15px;" />';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="form-row">';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<label for="city" class="title-label">Town / city</label>';
					echo '</div>';
				echo '</div>';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<input type="text" id="city" name="city" value="', $this->InputSafeString($this->customer['city']), '" required/>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="form-row">';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<label for="postcode" class="title-label">Postcode</label>';
					echo '</div>';
				echo '</div>';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<input type="text" id="postcode" name="postcode" value="', $this->InputSafeString($this->customer['postcode']), '" required/>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="form-row">';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<label for="" class="title-label">Country</label>';
					echo '</div>';
				echo '</div>';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';

						echo '<div class="custom-select-box">';
							echo '<select id="country" name="country">';
								echo '<option value=""></option>';
								foreach ($this->CountryList() as $country=>$ctry_name)
								{	echo '<option value="', $country, '"', $country == $this->customer['country'] ? ' selected="selected"' : '', '>', $this->InputSafeString($ctry_name), '</option>';
								}
							echo '</select>';
							echo '<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="form-row">';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<label for="phone" class="title-label">Phone number</label>';
					echo '</div>';
				echo '</div>';
				echo '<div class="form-col form-col-2">';
					echo '<div class="form-col-inner">';
						echo '<input type="text" id="phone" name="phone" value="', $this->InputSafeString($this->customer['phone']), '" required/>';
					echo '</div>';
				echo '</div>';
			echo '</div>';


			echo '<div class="form-row">';
				echo '<div class="form-col form-col-1">';
					echo '<div class="form-col-inner">';
						echo '<input type="submit" value="Submit your details" />';
					echo '</div>';
				echo '</div>';
			echo '</div>';

		echo '</form>';



		/*
		echo '<form action="" method="post">
		<p><label>Your email address</label><input type="email" name="email" value="', $this->InputSafeString($this->customer['email']), '" /></p><p><label>Your first name</label><input type="text" name="firstname" value="', $this->InputSafeString($this->customer['firstname']), '" /></p>
			<p><label>Your last name</label><input type="text" name="lastname" value="', $this->InputSafeString($this->customer['lastname']), '" /></p>
			<p><label>Address</label><input type="text" name="address1" value="', $this->InputSafeString($this->customer['address1']), '" /></p>
			<p><label>&nbsp;</label><input type="text" name="address2" value="', $this->InputSafeString($this->customer['address2']), '" /></p>
			<p><label>Town / city</label><input type="text" name="city" value="', $this->InputSafeString($this->customer['city']), '" /></p><p><label>Postcode</label><input type="text" name="postcode" value="', $this->InputSafeString($this->customer['postcode']), '" /></p><p><label>Country</label><select name="country"><option value="">-- select --</option>';
		foreach ($this->CountryList() as $country=>$ctry_name)
		{	echo '<option value="', $country, '"', $country == $this->customer['country'] ? ' selected="selected"' : '', '>', $this->InputSafeString($ctry_name), '</option>';
		}
		echo '</select></p><p><label>Phone number</label><input type="text" name="phone" value="', $this->InputSafeString($this->customer['phone']), '" /></p><p class="cartAddSubmit"><input type="submit" value="Submit your details" /></p></form>';
		*/
		return ob_get_clean();
	} // fn AddressForm

} // end of defn BookingCart
?>