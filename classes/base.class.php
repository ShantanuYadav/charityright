<?php
class Base
{	var $db;
	var $datefn;
	var $def_currency = 'GBP';
	var $lang = '';
	var $def_lang = 'en';
	var $prev_lang  = '';
	static $st_lang = '';
	static $st_prev_lang = '';
	var $druser = false;
	public $do_cart = true;
	public $cart_session_name = 'cr_cart';
	
	function __construct() // constructor
	{	$this->db = new DbConnect();
		$this->datefn = new DateXLate();
		//$this->do_cart = SITE_TEST;
		if (defined('DEFAULT_LANGUAGE'))
		{	$this->def_lang = DEFAULT_LANGUAGE;
		}
		if (!self::$st_lang)
		{	self::$st_lang = $this->AssignLanguage();
		}
		$this->lang = self::$st_lang;
		$this->prev_lang = self::$st_prev_lang;
	} // end of fn __construct
	
	public function FlagFileSet($filename = '')
	{	if ($filename)
		{	return file_exists(CITDOC_ROOT . '/flagfiles/' . $filename);
		}
	} // end of fn FlagFileSet
	
	function AssignLanguage()
	{	if ($_SESSION[SITE_NAME]['language'])
		{	return $_SESSION[SITE_NAME]['language'];
		} else
		{	if ($_COOKIE['bh_lang'])
			{	$_SESSION[SITE_NAME]['language'] = $_COOKIE['bh_lang'];
				$this->SetLanguageCookie();
				return $_SESSION[SITE_NAME]['language'];
			} else
			{	return $_SESSION[SITE_NAME]['language'] = $this->HTTPNegotiateLanguage();
			}
		}
	} // end of fn AssignLanguage
	
	function SetLanguageCookie()
	{	setcookie('bh_lang', $_SESSION[SITE_NAME]['language'], time() + ($this->datefn->secInDay * 30), SITE_SUB . '/');
	} // end of fn SetLanguageCookie
	
	function LangUsedString()
	{	$used = array();
		if ($this->langused && is_array($this->langused))
		{	foreach ($this->langused as $lang=>$true)
			{	$used[] = $lang;
			}
		}
		return implode(", ", $used);
	} // end of fn LangUsedString
	
	function DateFormat($dateformat = "", $timestamp = 0)
	{	if (!$timestamp)
		{	$timestamp = time();
		}
		return $this->TranslateDate(date($dateformat, $timestamp));
	} // end of fn DateFormat
	
	function TranslateDate($datestr = "")
	{	
		if (is_array($datestr))
		{	foreach ($datestr as $key=>$value)
			{	$datestr[$key] = $this->TranslateDate($value);
			}
		} else
		{	if ($this->lang != $this->def_lang)
			{	static $transtext = array();
				if (!$transtext)
				{	$sql = "SELECT deftext, transtext FROM datexlate WHERE lang='{$this->lang}'";
					if ($result = $this->db->Query($sql))
					{	while ($row = $this->db->FetchArray($result))
						{	$transtext[$row["deftext"]] = $row["transtext"];
						}
					}
				}
				
				if ($transtext && $datestr)
				{	foreach ($transtext as $old=>$new)
					{	$datestr = preg_replace("|\b$old\b|i", $new, $datestr);
					}
				}
			}
		}
		return $datestr;
		
	} // end of fn TranslateDate
	
	function GetTranslatedText($label, $lang = "")
	{	
		if (!$lang)
		{	$lang = $this->lang;
		}
		
		static $text_got = array();
		if (isset($text_got[$label]))
		{	return $text_got[$label];
		}
		
		$sql = "SELECT content FROM fptext WHERE fptlabel='" . $this->SQLSafe($label) . "' AND lang='$lang'";
		if ($result = $this->db->Query($sql))
		{	//if (($row = $this->db->FetchArray($result)) && ($text = $this->InputSafeString($row["content"])))
			if (($row = $this->db->FetchArray($result)) && ($text = stripslashes($row["content"])))
			{	return $text_got[$label] = $text;
			} else
			{	if (($lang == $this->def_lang))
				{	if ($lang != "en")
					{	// as last resort go for english
						$sql = "SELECT content FROM fptext WHERE fptlabel='" . $this->SQLSafe($label) . "' AND lang='en'";
						if ($result = $this->db->Query($sql))
						{	if ($row = $this->db->FetchArray($result))// && ($text = stripslashes($row["content"])))
							{	return $text_got[$label] = stripslashes($row["content"]);
							}
						}
					}
				} else
				{	return $this->GetTranslatedText($label, $this->def_lang);
				}
			}
		}
		return "";
	} // end of fn GetTranslatedText
	
	function GetParameter($field)
	{	if ($result = $this->db->Query("SELECT fieldvalue, fieldtype FROM parameters WHERE parname='$field'"))
		{	if ($row = $this->db->FetchArray($result))
			{	switch ($row["fieldtype"])
				{	case "INT": return (int) $row["fieldvalue"];
					case "BOOLEAN": return $row["fieldvalue"] ? 1 : 0;
					case "PRICE": return round($row["fieldvalue"], 2);
					default: return $row["fieldvalue"];
				}
			}
		}
		return "";
	} // end of fn GetParameter
	
	public function AcceptablePW($pw = '', $min = 8, $max = 20)
	{	return preg_match($this->AcceptablePWPattern($min, $max), $pw);
	} // end of AcceptablePW
	
	public function AcceptablePWPattern($min = 8, $max = 20)
	{	return '{^' . $this->AcceptablePWPatternInner($min, $max) . '$}i';
	} // end of AcceptablePWPattern
	
	public function AcceptablePWPatternInner($min = 8, $max = 20)
	{	return '[A-Za-z0-9]{' . $min . ',' . $max . '}';
	} // end of AcceptablePWPatternInner
	
	function CreatePassword($length = 8)
	{	$code = "";
		for ($i = 1; $i <= $length; $i++)
		{	$letter = rand(65, 108);
			if ($letter > 90)
			{	$code .= ceil(($letter - 90) / 2);
			} else
			{	if (rand(0, 1))
				{	$code .= chr($letter);
				} else
				{	$code .= chr($letter + 32);
				}
			}
		}
		return $code;
	} // end of fn CreatePassword
	
	function ConfirmCode($length = 5)
	{	$code = "";
		for ($i = 1; $i <= $length; $i++)
		{	$letter = rand(65, 108);
			if ($letter > 90)
			{	$code .= ceil(($letter - 90) / 2);
			} else
			{	if (rand(0, 1))
				{	$code .= chr($letter);
				} else
				{	$code .= chr($letter + 32);
				}
			}
		}
		return $code;
	} // end of fn ConfirmCode
	
	public function ValidEMail($email)
	{	$pattern = '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' .
'(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';
		return preg_match($pattern, $email);
	} // end of fn ValidEMail

	public function ValidSortCode($sortcode = '')
	{	$pattern = '/^[\d]{2}[\- ]?[\d]{2}[\- ]?[\d]{2}$/';
		return preg_match($pattern, $sortcode);
	} // end of fn ValidSortCode

	public function ValidBankAccountNumber($account = '')
	{	$pattern = '/^[\d]{8}$/';
		return preg_match($pattern, $account);
	} // end of fn ValidBankAccountNumber
	
	public function AccountNameSafe($name = '')
	{	return preg_replace('|[^a-z &\-./]|i', '', $name);
	} // end of fn AccountNameSafe

	public function ValidPhoneNumber($pnumber = '', $ukcheck = '')
	{	$pattern = $this->ValidPhoneNumberPattern();//'/^[\d-() +]{7,20}$/';
		if (preg_match($pattern, $pnumber))
		{	if ($ukcheck)
			{	$checknumber = preg_replace('/[^\d]/', '', $pnumber);
				if (substr($pnumber, 0, 1) == '+')
				{	return (strlen($checknumber) == 12) || (strlen($checknumber) == 13);
				} else
				{	if (substr($checknumber, 0, 1) == '0')
					{	if (substr($checknumber, 1, 1) == '0')
						{	return (strlen($checknumber) == 14) || (strlen($checknumber) == 15);
						} else
						{	return strlen($checknumber) == 11;
						}
					}
				}
			} else
			{	return true;
			}
		}
		return false;
	} // end of fn ValidPhoneNumber

	public function ValidPhoneNumberPattern()
	{	return '/^' . $this->ValidPhoneNumberPatternInner() . '$/';
	} // end of fn ValidPhoneNumberPattern

	public function ValidPhoneNumberPatternInner()
	{	return '[+]?[\d\s()-]{7,20}';
	} // end of fn ValidPhoneNumberPatternInner

	function ValidMobileNumber($pnumber)
	{	$pattern = '/^[\d ]{6,30}$/';
		return preg_match($pattern, $pnumber);
	} // end of fn ValidMobileNumber

	function ValidIntPhoneCode($pnumber)
	{	$pattern = '/^(00|\+)[\d]{2,3}$/';
		return preg_match($pattern, $pnumber);
	} // end of fn ValidIntPhoneCode
	
	function SQLSafe($string = '')
	{	return $this->db->RealEscapeString(trim($string));
	} // end of fn SQLSafe

	function BadCharacters($str)
	{	if ($str != htmlspecialchars($str, ENT_NOQUOTES))
		{	return true;
		}
		if (preg_match('{(%\d|\\\u|\\\x)}', $str))
		{	return true;
		}
		return false;
	} // end of BadCharacters
	
	function StripAllTags($data)
	{	if (is_array($data))
		{	$stripped = array();
			foreach ($data as $key=>$value)
			{	$stripped[$key] = $this->StripAllTags($value);
			}
		} else
		{	$stripped = "";
			if (!$this->BadCharacters($data)) $stripped = $data;
		}
		return $stripped;
	} // end of fn StripAllTags
	
	function ToInt($number)
	{	return (int)str_replace(array(",", " "), "", $number);
	} // end of fn ToInt
	
	function InputSafeString($string)
	{	return htmlentities(stripslashes($string), ENT_QUOTES, "utf-8", false);
	} // end of fn InputSafeString
	
	function SafeString($text)
	{	return trim(preg_replace("|[^\w\.\(\) ]|s", "", $text));
	} // end of fn SafeString	

	function Text2Para($text)
	{	$text = $this->InputSafeString($text);
		$newtext = "";
		foreach (explode("\n", $text) as $para)
		{	$newtext .= "<p>$para</p>\n";
		}
		return $newtext;
	} // end of fn Text2Para
	
	function StripJS($string)
	{   return preg_replace("|<script[^>]*?>.*?</script>|si", "", $string);
	} // end of fn StripJS
	
	function FormatYM($months, $mstring = "mth", $ystring = "yr")
	{	$str = "";
		if ($years = floor($months / 12))
		{	$str = $years . ($years == 1 ? " $ystring " : " {$ystring}s ");
		}
		if ($months = $months % 12)
		{	$str .= $months . ($months == 1 ? " $mstring" : " {$mstring}s");
		}
		return trim($str);
	} // end of fn FormatYM
	
	function FormatYMLong($months)
	{	$str = "";
		if ($years = floor($months / 12))
		{	$str = $years . ($years == 1 ? " year " : " years ");
		}
		if ($months = $months % 12)
		{	$str .= $months . ($months == 1 ? " month " : " months");
		}
		return $str;
	} // end of fn FormatYM
	
	function CSRFCheck()
	{	$host = substr(strstr($_SERVER["HTTP_REFERER"], "//"), 2);
		if ($slashpos = strpos($host, "/"))
		{	$host = substr($host, 0, $slashpos);
		}
		if ($host != $_SERVER["HTTP_HOST"])
		{	exit;
		}
	} // end of fn CSRFCheck
	
	function Ordinal($int)
	{	if ((floor($int / 10) % 10) == 1)
		{	return "th";
		} else
		{	switch ($int % 10)
			{	case 1: return "st";
				case 2: return "nd";
				case 3: return "rd";
				default: return "th";
			}
		}
	} // end of fn Ordinal
	
	function StringToTime($string)
	{	
		$min = 0;
		$hour = (int)$string;
		
		if (strstr($string, ":"))
		{	$time = explode(":", $string);
			$min = (int)$time[1];
		} else
		{	if (strstr($string, "."))
			{	$time = explode(".", $string);
				$min = (int)$time[1];
			}
		}
		
		if ($min >= 60)
		{	$hour += floor($min / 60);
			$min = $min % 60;
		}
		
		return str_pad($hour % 24, 2, "0", STR_PAD_LEFT) . ":" . str_pad($min, 2, "0", STR_PAD_LEFT);
		
	} // end of fn StringToTime
	
	function TimeToNumber($time = "")
	{	$time = str_replace(".", ":", $time);
		$time_list = explode(":", $time);
		return (int)$time_list[0] + ((int)$time_list[1] / 60);
	} // end of fn TimeToNumber
	
	function NumberToTime($number = 0)
	{	$hours = (int)$number;
		$mins = ($number - $hours) * 60;
		return str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" . str_pad(round($mins, 0), 2, "0", STR_PAD_LEFT);
	} // end of fn NumberToTime
	
	function GetScriptPlus()
	{	
		$scriptplus = $_SERVER["REQUEST_URI"];
		if ($qarray = $_GET)
		{	if ($qarray)
			{	$q = array();
				foreach ($qarray as $key=>$value)
				{	$q[] = "$key=$value";
				}
				$scriptplus = $_SERVER["SCRIPT_NAME"] . "?" . implode("&amp;", $q) . "&amp;";
			} else
			{	$scriptplus = $_SERVER["SCRIPT_NAME"] . "?";
			}
		} else
		{	$scriptplus .= "?";
		}
		return $scriptplus;
	} // end of fn GetScriptPlus
	
	function VarDump($var)
	{	echo "<pre>";
		print_r($var);
		echo "</pre>";
	} // end of fn VarDump
	
	
	function OutputDate($string = null, $format = 'd M Y')
	{
		if(is_null($string)) $string = date('Y-m-d H:i:s');
		return date($format, strtotime($string));	
	}
	
	function AddBackLinkHiddenField(&$form)
	{	if ($_POST['back_page'])
		{	$back_page = $_POST['back_page'];
		} else
		{	if (!$_GET['no_bl'] && strstr($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']))
			{	$back_page = $_SERVER['HTTP_REFERER']; 
			}
		}
		if ($back_page)
		{	$form->AddHiddenInput('back_page', $back_page);
		}
	} // end of fn AddBackLinkHiddenField
	
	function OptionsToRows($options = array(), $perrow = 4)
	{	$rows = array();
		$colcount = ceil(count($options) / $perrow);
		foreach ($options as $opt)
		{	$rows[$i++ % $colcount][] = $opt;
		}
		
		return $rows;
	} // end of fn OptionsToRows
	
	function GetAdminUser()
	{	static $adminuser;
		if (!is_a($adminuser, 'AdminUser'))
		{	$adminuser = new AdminUser((int)$_SESSION[SITE_NAME]['auserid']);
		}
		return $adminuser;
	} // end of fn GetAdminUser
	
	function CanAdminUserDelete()
	{	return $this->CanAdminUser("deletions");
	//	$adminuser = $this->GetAdminUser();
	//	return $adminuser->CanUserAccess("deletions");
	} // end of fn CanAdminUserDelete
	
	function CanSeeHistory()
	{	return true;//$this->CanAdminUser("admin");
	} // end of fn CanSeeHistory
	
	function CanAdminUser($area)
	{	$adminuser = $this->GetAdminUser();
		return $adminuser->CanUserAccess($area);
	} // end of fn CanAdminUserDelete
	
	function DisplayHistoryLink($tablename = "", $tableid = "")
	{	ob_start();
		if ($this->CanSeeHistory() && $tablename && $tableid)
		{	echo "<a class='historyOpener' onclick='ShowAdminActions(\"", $this->InputSafeString($tablename), "\", \"", $this->InputSafeString($tableid), "\");'><img src='../img/template/history_icon.png' title='history' alt='history'/></a>";
		}
		return ob_get_clean();
	} // end of fn DisplayHistoryLink
	
	function DisplayHistoryDeletedLink($tablename = "")
	{	ob_start();
		if ($this->CanSeeHistory() && $tablename)
		{	echo "<a class='historyOpener' onclick='ShowAdminActionsDeleted(\"", $this->InputSafeString($tablename), "\");'>",
				//"<img src='../img/template/history_icon.png' title='history' alt='history'/>",
				"deleted history",
				"</a>";
		}
		return ob_get_clean();
	} // end of fn DisplayHistoryDeletedLink
	
	function RecordAdminAction($parameters = array())
	{	
		if (($area = $this->SQLSafe($parameters["area"])) && ($tablename = $this->SQLSafe($parameters["tablename"])) && ($tableid = $this->SQLSafe($parameters["tableid"])) && ($auserid = (int)$_SESSION[SITE_NAME]["auserid"]))
		{	$action = $this->SQLSafe($parameters["action"]);
			$actionto = $this->SQLSafe($parameters["actionto"]);
			$actionfrom = $this->SQLSafe($parameters["actionfrom"]);
			$actiontype = $this->SQLSafe($parameters["actiontype"]);
			$linkmask = $this->SQLSafe($parameters["linkmask"]);
			$deleteparentid = $this->SQLSafe($parameters["deleteparentid"]);
			$deleteparenttable = $this->SQLSafe($parameters["deleteparenttable"]);
			$actiontime = $this->datefn->SQLdateTime();
			$sql = "INSERT INTO adminactions SET area='$area', tablename='$tablename', tableid='$tableid', auserid=$auserid, actiontime='$actiontime', action='$action', actionto='$actionto', actionfrom='$actionfrom', actiontype='$actiontype', linkmask='$linkmask', deleteparentid='$deleteparentid', deleteparenttable='$deleteparenttable'";
			if (!$this->db->Query($sql))
			{	//echo "<p>", $this->db->Error(), "</p>\n";
			}
		}
		
	} // end of fn RecordAdminAction
	
	function HTTPNegotiateLanguage()
	{	$available_languages = array($this->def_lang);
		if ($result = $this->db->Query("SELECT langcode FROM languages WHERE live=1 ORDER BY disporder"))
		{	while ($row = $this->db->FetchArray($result))
			{	if (!in_array($row["langcode"], $available_languages))
				{	$available_languages[] = $row["langcode"];
				}
			}
		}
		
		if (count($available_languages) > 1)
		{	$bestlang = $available_languages[0];
		
			preg_match_all("/([[:alpha:]]{1,8})(-([[:alpha:]|-]{1,8}))?" . "(\s*;\s*q\s*=\s*(1\.0{0,3}|0\.\d{0,3}))?\s*(,|$)/i", $_SERVER['HTTP_ACCEPT_LANGUAGE'], $hits, PREG_SET_ORDER);

			// default language (in case of no hits) is the first in the array
			$bestqval = 0;
			foreach ($hits as $arr)
			{	// read data from the array of this hit
				$langprefix = strtolower ($arr[1]);
				if (!empty($arr[3]))
				{	$langrange = strtolower ($arr[3]);
					$language = $langprefix . "-" . $langrange;
				} else
				{	$language = $langprefix;
				}
				$qvalue = 1.0;
				if (!empty($arr[5]))
				{	$qvalue = floatval($arr[5]);
				}

				// find q-maximal language 
				if (in_array($language,$available_languages) && ($qvalue > $bestqval))
				{	$bestlang = $language;
					$bestqval = $qvalue;
				} else // if no direct hit, try the prefix only but decrease q-value by 10% (as http_negotiate_language does)
				{	if (in_array($langprefix, $available_languages) && (($qvalue*0.9) > $bestqval))
					{	$bestlang = $langprefix;
						$bestqval = $qvalue*0.9;
					}
				}
			}
			return $bestlang;
		}
		return $this->def_lang;
	} // end of fn HTTPNegotiateLanguage

	function AdminEditLangList($page = "")
	{	
		if ($this->id && $this->language && $this->langused)
		{	$languages = array();
			if ($result = $this->db->Query("SELECT * FROM languages ORDER BY disporder"))
			{	while ($row = $this->db->FetchArray($result))
				{	$languages[] = $row;
				}
			}
			if (count($languages) > 1)
			{	if (!$page)
				{	$page = $_SERVER["SCRIPT_NAME"];
				}
				echo "<ul class='lang-chooser'>\n";
				foreach ($languages as $row)
				{	echo "<li>";
					if ($row["langcode"] == $this->language)
					{	echo $this->langused[$this->language] ? "editing " : "creating ", $row["langname"];
					} else
					{	echo "<a href='", $page, "?id=", $this->id, "&lang=", $row["langcode"], "'>", 
								$this->langused[$row["langcode"]] ? "edit " : "create ", $row["langname"], "</a>";
					}
					echo "</li>\n";
				}
				echo "</ul>\n";
			}
		}
	} // end of fn AdminEditLangList
	
	function CSVSafeString($text)
	{	return str_replace('"', "'", stripslashes($text));
	} // end of fn CSVSafeString
	
	public function TitleDisplay($text = '')
	{	ob_start();
		if ($text)
		{
			$words = explode(' ', $text, 2);
			if ($words[0])
			{	echo '<span>', $this->InputSafeString($words[0]), '</span>&nbsp;';
			}
			echo $this->InputSafeString($words[1]);
		}
		return ob_get_clean();
	} // end of fn PageTitleDisplay
	
	function CountryList()
	{	$countries = array();
		$toplist = array();
		$toplist_raw = array();
		$sql = 'SELECT countries.* FROM countries ORDER BY countries.shortname';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$ctry = new Country($row);
				if ($ctry->details['toplist'])
				{	$toplist_raw[str_pad($ctry->details['toplist'], 4, '0', STR_PAD_LEFT) . $ctry->details['shortname'] . $ctry->details['shortcode']] = $ctry->details;
				} else
				{	$countries[$ctry->details['shortcode']] = $ctry->details['shortname'];
				}
			}
		}
		
		if ($toplist_raw)
		{	ksort($toplist_raw);
			foreach ($toplist_raw as $country)
			{	$toplist[$country['shortcode']] = $country['shortname'];
			}
		}
		
		return array_merge($toplist, $countries);
	} // end of fn CountryList
	
	function EventVenuesList()
	{	$venues = array();
		$sql = 'SELECT eventvenues.evid, eventvenues.venuename, countries.shortname AS countryname FROM eventvenues, countries WHERE eventvenues.country=countries.shortcode ORDER BY countryname, eventvenues.venuename';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$venues[$row['evid']] = $row['venuename'] . ' [' . $row['countryname'] . ']';
			}
		}
		return $venues;
	} // end of fn CountryList
	
	function GetCountry($code = '', $field = 'shortname')
	{	$name = '';
		$sql = "SELECT $field FROM countries WHERE shortcode='$code'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	$name = $row[$field];
			}
		}
		return $name;
	} // end of fn GetCountry

	public function CartLink()
	{	if ($_SESSION['cart_plates'])
		{	return '<a href="' . SITE_URL . 'cart.php">Go to cart</a>';
		} else
		{	return '';
		}
	} // end of fn CartLink
	
	public function CartButtonContents()
	{	ob_start();
		$cartitems = 0;
		$cartprice = 0;
		if ($_SESSION['cart_disccode'])
		{	$discount = new DiscountCode();
			if (!$discount->GetFromCode($_SESSION['cart_disccode']) || !$discount->CurrentlyValid())
			{	unset($discount);
			}
		}
		if (is_array($_SESSION['cart_plates']) && count($_SESSION['cart_plates']))
		{	foreach ($_SESSION['cart_plates'] as $plate)
			{	$cartprice += $plate['cart_price'];
				$cartitems++;
			}
		}
		if (is_array($_SESSION['cart_products']) && count($_SESSION['cart_products']))
		{	foreach ($_SESSION['cart_products'] as $cart_item_id=>$product_details)
			{	$product = new Product($product_details['prodid']);
				$product_prices = array();
				for ($i = 1; $i <= $product_details['qty']; $i++)
				{	if ($product->details['autoplateadd'])
					{	++$autoplateadd_count;
					}
					if ($product->details['autoplateadd'] && $discount && $discount->details['freefixingkit'])
					{	if ($autoplateadd_count > count($_SESSION['cart_plates']))
						{	$cartprice += $product->details['price'];
						}
					} else
					{	$cartprice += $product->details['price'];
					}
				}
				$cartitems += $product_details['qty'];
			}
		}
			if ($_SESSION['cart_del'])
			{	if (!$this->deloptions && $this->customer->details['countrycode'])
				{	$this->deloptions = new DelOptions($this->customer->details['countrycode'], true);
				}
				if ($this->deloptions->deloptions && $this->deloptions->deloptions[$_SESSION['cart_del']])
				{	$cartprice += $this->deloptions->deloptions[$_SESSION['cart_del']]->details['delprice'];
				}
			}
		if ($discount)
		{	$cartprice = $discount->ApplyDiscount($cartprice);
		}
		echo '<a', $cartitems ? (' href="' . SITE_URL . 'cart.php"') : '', '><span class="white">', (int)$cartitems, ' items</span> (&pound;', number_format($cartprice, 2), ')</a>';
		return ob_get_clean();
	} // end of fn CartButtonContents
	
	public function TextToSlug($text = '')
	{	$text = preg_replace("|[^\d\w _-]|", "", $text);
		while (strstr($text, "  "))
		{	$text = str_replace("  ", " ", $text);
		}
		return strtolower(str_replace(" ", "-", $text));
	} // end of fn TextToSlug
	
	public function ImplodeExtra($array = array(), $sep = '', $lastsep = '')
	{	if (is_array($array))
		{	if ($lastsep)
			{	if (($acount = count($array)) > 1)
				{	$array[$acount - 2] = $array[$acount - 2] . $lastsep . $array[$acount - 1];
					unset($array[$acount - 1]);
				}
			}
			return implode($sep, $array);
		}
	} // end of fn TextToSlug
	
	function ReSizePhoto($uploadfile_array = array(), $file = '', $width = 0, $height = 0)
	{	$uploadfile = $uploadfile_array['tmp_name'];
		$isize = getimagesize($uploadfile);
		$ratio = $width / $isize[0];
		$h_ratio = $height / $isize[1];
		if ($h_ratio > $ratio)
		{	$ratio = $h_ratio;
		}
		
		if (stristr($uploadfile_array['type'], 'jpeg') || stristr($uploadfile_array['type'], 'jpg'))
		{	$oldimage = imagecreatefromjpeg($uploadfile);
			$imagetype = 'jpg';
		} else
		{	if (stristr($uploadfile_array['type'], 'png'))
			{	$oldimage = imagecreatefrompng($uploadfile);
				$imagetype = 'png';
			} else
			{	return false;
			}
		}
		
		if ($oldimage)
		{	$w_new = ceil($isize[0] * $ratio);
			$h_new = ceil($isize[1] * $ratio);
			
			if ($width && $height && $ratio != 1)
			{	$newimage = imagecreatetruecolor($w_new,$h_new);
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
				imagecopyresampled($newimage,$oldimage,0,0,0,0,$w_new, $h_new, $isize[0], $isize[1]);
			} else
			{	$newimage = $oldimage;
				if ($imagetype == 'png')
				{	imagealphablending($newimage, false);
					imagesavealpha($newimage, true);
				}
			}
			
			// now get middle chunk - horizontally
			if ($width && $height && ($w_new > $width || $h_new > $height))
			{	$resizeimg = imagecreatetruecolor($width, $height);
				if ($imagetype == 'png')
				{	imagealphablending($resizeimg, false);
					imagesavealpha($resizeimg, true);
				}
				$leftoffset = floor(($w_new - $width) / 2);
				imagecopyresampled($resizeimg, $newimage, 0, 0, floor(($w_new - $width) / 2), floor(($h_new - $height) / 2), $width, $height, $width, $height);
				$newimage = $resizeimg;
			}
			
			ob_start();
			imagepng($newimage, NULL, 6);
			return file_put_contents($file, ob_get_clean());
		}
	} // end of fn ReSizePhoto
	
	function CurrencyList($field = '')
	{	$currencies = array();
		if ($result = $this->db->Query('SELECT * FROM currencies ORDER BY curname'))
		{	while ($row = $this->db->FetchArray($result))
			{	if (isset($row[$field]))
				{	$currencies[$row['curcode']] = $row[$field];
				} else
				{	if ($field == 'full')
					{	$currencies[$row['curcode']] = $row;
					} else
					{	$currencies[$row['curcode']] = $row['curname'] . ' - ' . $row['cursymbol'];
					}
				}
			}
		}
		return $currencies;
	} // end of fn CurrencyList
	
	function GetCurrency($curcode = '', $field = '')
	{	$currency = array();
		if ($result = $this->db->Query('SELECT * FROM currencies WHERE curcode="' . $curcode . '"'))
		{	if ($row = $this->db->FetchArray($result))
			{	if ($field && isset($row[$field]))
				{	return $row[$field];
				} else
				{	$currency = $row;
				}
			}
		}
		return $currency;
	} // end of fn GetCurrency
	
	function GetBankHols($daysback = 0, $daysforward = 0)
	{	$hols = array();
		$tables = array('nonworkingdays'=>'nonworkingdays');
		$fields = array('nonworkingdays.nwdate');
		$where = array();
		$orderby = array('nonworkingdays.nwdate');
		
		$where['start'] = 'nonworkingdays.nwdate>="' . $this->datefn->SQLDate(strtotime('-' . (int)$daysback . ' days')) . '"';
		$where['end'] = 'nonworkingdays.nwdate<="' . $this->datefn->SQLDate(strtotime('+' . (int)$daysforward . ' days')) . '"';
		
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$hols[$row['nwdate']] = $row['nwdate'];
			}
		}
		
		return $hols;
	} // end of fn GetCurrency
	
	public function StartAndEndDatesFromGet($data = array(), &$start, &$end)
	{	
		if ($ystart = (int)$data['ystart'])
		{	if (!$mstart = (int)$data['mstart'])
			{	$mstart = 1;
			}
			if (!$dstart = (int)$data['dstart'])
			{	$dstart = 1;
			}
			$start = $this->datefn->SQLDate(mktime(0, 0, 0, $mstart, $dstart, $ystart));
		}
		
		if ($yend = (int)$data['yend'])
		{	if (!$mend = (int)$data['mend'])
			{	$mend = 12;
			}
			if (!$dend = (int)$data['dend'])
			{	$dend = date('t', mktime(0, 0, 0, $mend, 1, $yend));
			}
			$end = $this->datefn->SQLDate(mktime(0, 0, 0, $mend, $dend, $yend));
		}
		
	} // end of fn StartAndEndDatesFromGet
	
	public function GetCampaignAvatarsAvailable($include = 0)
	{	$tables = array('campaignavatars'=>'campaignavatars');
		$fields = array('campaignavatars.*');
		$where = array();
		$orderby = array('campaignavatars.listorder ASC');
		
		if ($include = (int)$include)
		{	$where['live'] = '(campaignavatars.live=1 OR campaignavatars.caid=' . $include . ')';
		} else
		{	$where['live'] = 'campaignavatars.live=1';
		}
		
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'caid');
	} // end of fn GetCampaignAvatarsAvailable
	
	public function GetCampaignSampleText($include = 0)
	{	$tables = array('campaigntext'=>'campaigntext');
		$fields = array('campaigntext.*');
		$where = array();
		$orderby = array('campaigntext.listorder ASC');
		
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'ctid');
	} // end of fn GetCampaignSampleText
	
	public function PostcodeTidy($postcode = '')
	{	$postcode = preg_replace('|[^A-Z0-9 ]|', '', trim(strtoupper($postcode)));
		$pcodebits = explode(' ', $postcode);
		if (count($pcodebits) > 1)
		{	$postcode = array_shift($pcodebits) . ' ' . implode('', $pcodebits);
		} else
		{	// need to add in space if possible
			if (preg_match('|^[A-Z]{1,2}[0-9]{2,3}|', $postcode, $matches))
			{	ob_start();
				echo substr($matches[0], 0, -1), ' ', substr($postcode, strlen($matches[0]) - 1);
				$postcode = ob_get_clean();
			}
		}
		return $postcode;
	} // end of fn PostcodeTidy

	protected function GetQDCurrencies($type = 'oneoff')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetQDCurrencies
	
	public function GetPaymentGateways($ccode = '', $getall = false)
	{	$gateways = array();
		$sql = 'SELECT paymentgateways.*, countrygateways.listorder FROM paymentgateways, countrygateways WHERE paymentgateways.pgid=countrygateways.pgid AND ccode="' . $this->SQLSafe($ccode) . '"';
		if (!$getall)
		{	$sql .= ' AND paymentgateways.suspended=0';
		}
		$sql .= ' ORDER BY countrygateways.listorder, paymentgateways.gateway';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$gateways[$row['pgid']] = $row;
			}
		}
		return $gateways;
	} // end of fn GetPaymentGateways
	
	function TimefromDateDisplay($sqldate = '')
	{	if ($sqldate)
		{	$secs = time() - strtotime($sqldate);
			if ($weeks = floor($secs / $this->datefn->secInWeek))
			{	return $weeks . ' week' . ($weeks == 1 ? '' : 's') . ' ago';
				//return str_replace('{weeks}', $weeks, ($weeks > 1 ? $this->GetTranslatedText('timeago_weeks') : $this->GetTranslatedText('timeago_week')));
			} else
			{	if ($days = floor($secs / $this->datefn->secInDay))
				{	return $days . ' day' . ($days == 1 ? '' : 's') . ' ago';
					//return str_replace('{days}', $days, ($days > 1 ? $this->GetTranslatedText('timeago_days') : $this->GetTranslatedText('timeago_day')));
				} else
				{	if ($hours = floor($secs / $this->datefn->secInHour))
					{	return $hours . ' hour' . ($hours == 1 ? '' : 's') . ' ago';
						//return str_replace('{hours}', $hours, ($hours > 1 ? $this->GetTranslatedText('timeago_hours') : $this->GetTranslatedText('timeago_hour')));
					} else
					{	if ($minutes = floor($secs / 60))
						{	return $minutes . ' minute' . ($minutes == 1 ? '' : 's') . ' ago';
							//return str_replace('{mins}', $minutes, ($minutes > 1 ? $this->GetTranslatedText('timeago_mins') : $this->GetTranslatedText('timeago_min')));
						} else
						{	return $secs . ' second' . ($secs == 1 ? '' : 's') . ' ago';
							//return str_replace('{secs}', $secs, ($secs > 1 ? $this->GetTranslatedText('timeago_secs') : $this->GetTranslatedText('timeago_sec')));
						}
					}
				}
			}
		}
		return '';
		
	} // end of fn TimefromDateDisplay
	
	public function XSSSafeArray(&$array = array())
	{	if (is_array($array) && $array)
		{	$altered = array();
			foreach ($array as $key=>$value)
			{	$altered[$this->XSSSafe($key)] = $this->XSSSafe($value);
			}
			$array = $altered;
		}
	} // end of fn XSSSafeAllGet
	
	public function XSSSafeAllGet()
	{	$this->XSSSafeArray($_GET);
	} // end of fn XSSSafeAllGet
	
	public function XSSSafeAllPost()
	{	$this->XSSSafeArray($_POST);
	} // end of fn XSSSafeAllPost
	
	public function XSSSafe($string = '')
	{	//$string = htmlentities(strip_tags($string));
		$string = htmlentities($string);
		return $string;
	} // end of fn XSSSafe
	
} // end of defn Base
?>