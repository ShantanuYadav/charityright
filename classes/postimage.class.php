<?php
class PostImage extends BlankItem
{	protected $imagesizes = array('thumb'=>array('w'=>50, 'h'=>50), 'small'=>array('w'=>150, 'h'=>150), 'medium'=>array('w'=>240, 'h'=>240), 'full'=>array('w'=>500, 'h'=>500), 'og'=>array('w'=>600, 'h'=>315));
	protected $imagelocation = '';
	protected $imagedir = '';

	function __construct($id = 0)
	{	parent::__construct($id, 'postimages', 'piid');
		$this->imagelocation = SITE_URL . 'img/posts/';
		$this->imagedir = CITDOC_ROOT . '/img/posts/';
	} //  end of fn __construct

	public function HasImage($size = 'small')
	{	if ($this->id && file_exists($this->GetImageFile($size)))
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions

	public function ImageDimensions($size = 'small')
	{	if ($this->imagesizes[$size])
		{	return array($this->imagesizes[$size]['w'], $this->imagesizes[$size]['h']);
		}
	} // end of fn ImageDimensions

	public function GetImageFile($size = 'small')
	{	return $this->ImageFileDirectory($size) . '/' . $this->id .'.png';
	} // end of fn GetImageFile

	public function ImageFileDirectory($size = 'small')
	{	return $this->imagedir . $this->InputSafeString($size);
	} // end of fn FunctionName

	public function GetImageSRC($size = 'small')
	{	if ($this->HasImage($size))
		{	return $this->imagelocation . $this->InputSafeString($size) . '/' . $this->id . '.png';
		}
	} // end of fn GetImageSRC

	public function ImageHTML($size = 'small')
	{	if ($src = $this->GetImageSRC($size))
		{	ob_start();
			echo '<img src="', $src, '" alt="', $alt = $this->InputSafeString($this->details['imagedesc']), '" title="', $alt, '"  class="image-size-'.$size.'"/>';
			return ob_get_clean();
		}
	} // end of fn ImageHTML

} // end of defn PostImage
?>