<?php
class DonationProject2 extends BlankItem
{	public $country = array();
	public $project = array();
	public $price = array();
	
	public function __construct($id = 0)
	{	parent::__construct($id, 'donation_projects2', 'dp2id');
	} // fn __construct
	
	public function ResetExtra()
	{	$this->project = array();
		$this->country = array();
		$this->prices = array();
	} // end of fn ResetExtra
	
	public function GetExtra()
	{	$this->GetDonationProject();
		$this->GetDonationCountry();
		$this->GetPrices();
	} // end of fn GetExtra
	
	public function GetFromSlug($slug = '')
	{	$this->country = array();
		$tables = array('donation_projects'=>'donation_projects');
		$fields = array('donation_projects.*');
		$where = array('slug'=>'donation_projects.slug="' . $this->InputSafeString($slug) . '"');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where)))
		{	if ($row = $this->db->FetchArray($result))
			{	$this->Get($row);
			}
		}
	} // fn GetFromSlug
	
	public function GetPrices()
	{	$tables = array('donation_prices'=>'donation_prices');
		$fields = array('donation_prices.*');
		$where = array('projectid'=>'donation_prices.projectid=' . $this->id,'projectlevel'=>'donation_prices.projectlevel=2');
		$this->prices = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where), 'currency');
	} // fn GetPrices
	
	public function GetDonationProject()
	{	$this->project = array();
		$tables = array('donation_projects'=>'donation_projects');
		$fields = array('donation_projects.*');
		$where = array('dpid'=>'donation_projects.dpid=' . $this->details['dpid']);
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where)))
		{	if ($row = $this->db->FetchArray($result))
			{	$this->project = $row;
			}
		}
	} // fn GetDonationProject
	
	public function GetDonationCountry()
	{	$this->country = array();
		if ($this->project && ($dcid = (int)$this->project['dcid']))
		{	$tables = array('donation_countries'=>'donation_countries');
			$fields = array('donation_countries.*');
			$where = array('dcid'=>'donation_countries.dcid=' . $dcid);
			if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where)))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->country = $row;
				}
			}
		}
	} // fn GetDonationCountry
	
	public function GetDonations()
	{	$donations = array();
		$tables = array('donations'=>'donations');
		$fields = array('donations.*');
		
		$where = array('country'=>'donations.donationcountry="' . $this->InputSafeString($this->country['dccode']) . '"', 'project'=>'donations.donationproject="' . $this->InputSafeString($this->project['dpcode']) . '"', 'project2'=>'donations.donationproject2="' . $this->InputSafeString($this->details['dpcode']) . '"');
		$orderby = array('donations.created ASC');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$donations[$row['did']] = $row;
			}
		}
		return $donations;
	} // fn GetDonations
	
	public function GetCampaigns()
	{	$campaigns = array();
		$tables = array('campaigns'=>'campaigns');
		$fields = array('campaigns.*');
		$where = array('country'=>'campaigns.donationcountry="' . $this->InputSafeString($this->country['dccode']) . '"', 'project'=>'campaigns.donationproject="' . $this->InputSafeString($project->details['dpcode']) . '"', 'project2'=>'campaigns.donationproject2="' . $this->InputSafeString($this->details['dpcode']) . '"');
		$orderby = array('campaigns.created ASC');
		if ($result = $this->db->Query($this->db->BuildSQL($tables, $fields, $where, $orderby)))
		{	while ($row = $this->db->FetchArray($result))
			{	$campaigns[$row['cid']] = $row;
			}
		}
		return $campaigns;
	} // fn GetCampaigns
	
} // end of defn DonationProject2
?>