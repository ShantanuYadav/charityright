<?php
class AdminVideosPage extends AdminPage
{	protected $video;

	function __construct()
	{	parent::__construct('POSTS');
	} //  end of fn __construct

	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('posts'))
		{	$this->AdminVideoLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AdminVideoLoggedInConstruct()
	{	$this->AssignVideo();
		$this->breadcrumbs->AddCrumb('videos.php', 'Videos');
		$this->VideoConstructFunctions();
		if ($this->video->id)
		{	$this->breadcrumbs->AddCrumb('video.php?id=' . $this->video->id, $this->InputSafeString($this->video->details['vtitle']));
		}
	} // end of fn AdminVideoLoggedInConstruct
	
	protected function VideoConstructFunctions()
	{	
	} // end of fn VideoConstructFunctions
	
	protected function AssignVideo()
	{	$this->video = new AdminVideo($_GET['id']);
	} // end of fn AssignVideo
	
	protected function AdminVideosBody()
	{	if ($this->video->id)
		{	if ($menu = $this->GetVideoMenu())
			{	echo '<div class="adminItemSubMenu"><ul>';
				foreach ($menu as $menuarea=>$menuitem)
				{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
				}
				echo '</ul><div class="clear"></div></div>';
			}
		}
	} // end of fn AdminVideosBody
	
	protected function GetVideoMenu()
	{	$menu = array();
		$menu['edit'] = array('text'=>'Edit', 'link'=>'video.php?id=' . $this->video->id);
		if ($this->video->GetPosts())
		{	$menu['posts'] = array('text'=>'Posts', 'link'=>'videoposts.php?id=' . $this->video->id);
		}
		return $menu;
	} // end of fn GetVideoMenu
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('posts'))
		{	$this->AdminVideosBody();
		}
	} // end of fn AdminBodyMain
	
} // end of defn AdminVideosPage
?>