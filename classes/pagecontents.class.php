<?php
class PageContents extends Base
{	var $pages = array();
	var $liveonly = true;

	function __construct($liveonly = true)
	{	parent::__construct();
		$this->liveonly = ($liveonly ? true : false);
		$this->Get();
	} //  end of fn __construct
	
	function Reset()
	{	$this->pages = array();
	} // end of fn Reset
	
	function Get()
	{	$this->Reset();
		$sql = 'SELECT * FROM pages WHERE parentid=0';
		if ($this->liveonly)
		{	$sql .= ' AND pagelive=1';
		}
		$sql .= ' ORDER BY pageorder';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->pages[] = new PageContent($row, $this->liveonly);
			}
		}
	} // end of fn Get

	function GetHeaderMenu()
	{	$menu = array();
		foreach ($this->pages as $page)
		{	if ($page->details['headermenu'])
			{	$submenu = array();
				if ($page->subpages)
				{	foreach ($page->subpages as $subpage)
					{	if ($subpage->details['headermenu'])
						{	$submenu[$subpage->details['pagename']] = array('link'=>$subpage->details['headeronly'] ? '' : $subpage->Link(), 'text'=>$this->InputSafeString($subpage->details['pagetitle']), 'class'=>$this->InputSafeString($subpage->details['menuclass']));
						}
					}
				}
				$menu[$page->details['pagename']] = array('link'=>$page->details['headeronly'] ? '' : $page->Link(), 'text'=>$this->InputSafeString($page->details['pagetitle']), 'class'=>$this->InputSafeString($page->details['menuclass']), 'submenu'=>$submenu);
			}
		}
	/*	$sql = 'SELECT * FROM pages WHERE headermenu=1 AND pagelive=1 AND parentid=0 ORDER BY pageorder';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$page = new PageContent($row, $this->liveonly);
				$menu[$page->details['pagename']] = array('link'=>$page->Link(), 'text'=>$this->InputSafeString($page->details['pagetitle']), 'class'=>$this->InputSafeString($page->details['menuclass']));
				// get any sub pages
			}
		}*/
		return $menu;
	} // end of fn GetHeaderMenu

} // end of defn AdminPageContent
?>