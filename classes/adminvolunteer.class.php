<?php
class AdminVolunteer extends Volunteer
{	
	public function __construct($id = 0)
	{	parent::__construct($id);
	} // fn __construct

	public function FullName()
	{	return $this->details['firstname'] . ' ' . $this->details['surname'];
	} // fn FullName

} // end of defn AdminVolunteer
?>