<?php
class AdminCampaign extends Campaign
{
	public function __construct($id = 0)
	{	parent::__construct($id);
	} // fn __construct
	
	public function Create($data = array(), $photo_image = array(), $cuid = 0, $team = false)
	{	$fail = array();
		$success = array();
		$camp_fields = array('created'=>'created="' . $this->datefn->SQLDateTime() . '"');
		
		if ($campname = $this->SQLSafe($data['campname']))
		{	$camp_fields['campname'] = 'campname="' . $campname . '"';
			$camp_fields['slug'] = 'slug="' . $this->TextToSlug($data['campname']) . '"';
		} else
		{	$fail[] = 'Campaign name missing';
		}

		if ($cuid = (int)$cuid)
		{	if (($campaign_user = new CampaignUser($cuid)) && $campaign_user->id)
			{	$camp_fields['cuid'] = 'cuid=' . $campaign_user->id;
				$camp_fields['campusername'] = 'campusername="' . $this->SQLSafe($campaign_user->details['firstname'] . ' ' . $campaign_user->details['lastname']) . '"';
			} else
			{	$fail[] = 'User not found';
			}
		} else
		{	$camp_fields['campusername'] = 'campusername="Charity Right"';
		}
		
		if ($description = $data['description'])
		{	$camp_fields['description'] = 'description="' . $this->SQLSafe(strip_tags($description)) . '"';
		} else
		{	$fail[] = 'You must provide a description';
		}
		
		if ($this->GetCurrency($data['currency']))
		{	$camp_fields['currency'] = 'currency="' . $data['currency'] . '"';
		} else
		{	$fail[] = 'Currency missing';
		}
		
		if ($target = (int)$data['target'])
		{	$camp_fields['target'] = 'target=' . $target;
		} else
		{	$fail[] = 'Campaign target missing';
		}
		
		if ($data['enabled'])
		{	if ($data['visible'])
			{	$camp_fields['enabled'] = 'enabled=1';
				$camp_fields['visible'] = 'visible=1';
			} else
			{	$fail[] = 'To take donations your campaign must be visible';
			}
		} else
		{	$camp_fields['enabled'] = 'enabled=0';
			$camp_fields['visible'] = 'visible=' . ($data['visible'] ? '1' : '0');
		}
		$camp_fields['isteam'] = 'isteam=1';
		$camp_fields['helpfromcr'] = 'helpfromcr=' . ($data['helpfromcr'] ? '1' : '0');

		if ($donationcountry = $this->SQLSafe($data['donation_country']))
		{	$camp_fields['donationcountry'] = 'donationcountry="' . $donationcountry . '"';
		} else
		{	$fail[] = 'donation country missing';
		}
		if ($donationproject = $this->SQLSafe($data['donation_project']))
		{	$camp_fields['donationproject'] = 'donationproject="' . $donationproject . '"';
		} else
		{	$fail[] = 'donation project missing';
		}
	
		if ($photo_image['size'])
		{	if ($this->ValidPhotoUpload($photo_image))
			{	$image_to_upload = true;
			} else
			{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
			}
		} else
		{	if ($avatarid = (int)$data['avatarid'])
			{	$camp_fields['avatarid'] = 'avatarid=' . $avatarid;
			} else
			{	$fail[] = 'You must choose an image';
			}
		}
	
		if (!$fail && $set = implode(', ', $camp_fields))
		{	$sql = 'INSERT INTO campaigns SET ' . $set;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($id = $this->db->InsertID()))
				{	$this->Get($id);
					$success[] = 'New campaign created';
					$if = new InfusionSoftCampaignOwner();
					$if->AddCampaign($this);
				}
			} else $fail[] = $sql . ': ' . $this->db->Error();
			
			if ($this->id)
			{	if ($photo_image['size'])
				{	if ($this->ValidPhotoUpload($photo_image))
					{	$photos_created = 0;
						foreach ($this->imagesizes as $size_name=>$size)
						{	if (!file_exists($this->ImageFileDirectory($size_name)))
							{	mkdir($this->ImageFileDirectory($size_name));
							}
							if ($this->ReSizePhotoPNG($photo_image['tmp_name'], $this->GetImageFile($size_name), $size['w'], $size['h'], stristr($photo_image['type'], 'png') ? 'png' : 'jpg'))
							{	$photos_created++;
							}
						}
						unset($photo_image['tmp_name']);
						if ($photos_created)
						{	$success[] = 'Your image has been uploaded';
						}
					} else
					{	$fail[] = 'Invalid image (jpeg, jpg or png only please)';
					}
				}
			}
		}
	
	//	$this->VarDump($cart);
	
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	
	} // fn Create
	
	public function Save($data = array(), $imagefile = array(), $cuid = 0)
	{	if ($this->AdminCanUpdate())
		{	$data['adminedited'] = true;
			$data['donation_country'] = $data['donationcountry'];
			$data['donation_project'] = $data['donationproject'];
			$data['isteam'] = $this->details['isteam'];
			if ($this->id)
			{	return parent::Save($data, $imagefile);
			} else
			{	$saved = $this->Create($data, $imagefile, $cuid);
				if ($this->id)
				{	$this->SendCampaignCreateEmail();
				}
				return $saved;
			}
		} else
		{	return array('failmessage'=>'Campaign cannot be amended by admin');
		}
	} // fn Save

	public function SendCampaignCreateEmail()
	{	
		if ($this->id)
		{	ob_start();
			echo '<p>Dear ', $this->InputSafeString($this->owner['firstname']), ',</p>
<p>Charity Right has set up a new campaign "', $this->InputSafeString($this->details['campname']), '" for you to raise money for Charity Right</p>
<p>Have a look at your page here: <a href="', $link = $this->Link(), '">', $link, '</a></p>
<p>I\'m here to help with any fundraising needs you may have, so please don\'t hesitate to reply to this email.<p>
<p>JazakAllah Khayr and I hope to hear from you soon.</p>';
			$mail = new HTMLMail();
			$mail->SetSubject('Your new Charity Right CR Stars campaign');
			$mail->SendFromTemplate($this->owner['email'], array('main_content'=>ob_get_clean()), 'default');
		}
		
	} // fn SendCampaignCreateEmail
	
	public function AdminCanUpdate()
	{	return true;//!$this->id || !$this->owner;
	} // fn AdminCanUpdate
	
	public function DonationsToDates($dates = array())
	{	$return = array();
		if (is_array($dates))
		{	foreach ($dates as $date)
			{	if ($date == $this->datefn->SQLDate(strtotime($date)))
				{	$return[$date] = 0;
				}
			}
		}
		if ($return)
		{	foreach ($this->GetDonations() as $donation)
			{	$donate_date = substr($donation['donated'], 0, 10);
				foreach ($return as $date=>$date_total)
				{	if ($donate_date <= $date)
					{	$return[$date] += $donation['campaignamount'];
					}
				}
			}
		}
		return $return;
	} // fn DonationsToDates

	public function AddToTeam($teamid = 0, $existing_payments = false)
	{	if (!$this->details['isteam'] && !$this->team)
		{	if (parent::AddToTeam($teamid))
			{	// reset country and project
				$resetsql = 'UPDATE campaigns SET donationcountry="", donationproject="" WHERE cid=' . $this->id;
				$this->db->Query($resetsql);
				if ($existing_payments && ($donations = $this->GetDonations(true)) && ($team = new Campaign($teamid)) && $team->id)
				{	if ($team->details['currency'] != $this->details['currency'])
					{	$new_currency = true;
						$convertrate = $this->GetCurrency($team->details['currency'], 'convertrate') / $this->GetCurrency($this->details['currency'], 'convertrate');
					}
					foreach ($donations as $donation)
					{	if (!$donation['teamid'])
						{	$fields = array('teamid'=>'teamid=' . $team->id);
							if ($team->details['currency'] != $this->details['currency'])
							{	if ($team->details['currency'] == $donation['currency'])
								{	$fields['teamamount'] = 'teamamount=' . $donation['amount'];
								} else
								{	$fields['teamamount'] = 'teamamount=' . round($donation['campaignamount'] * $convertrate, 2);
								}
							}
							$donsql = 'UPDATE campaigndonations SET ' . implode(', ', $fields) . ' WHERE cdid=' . $donation['cdid'];
							$this->db->Query($donsql);
							//$this->VarDump($donation);
						}
					}
				}
				return true;
			}
		}/* else
		{	ob_start();
			echo '';
			mail('tim@websquare.co.uk', 'conditions failed', ob_get_clean());
		}*/

	} // fn AddToTeam
	
	public function ShowDetails()
	{	ob_start();
		if (SITE_TEST)
		{	//parent::SendCampaignCreateEmail();
			//$this->SendCampaignCreateEmail();
		}
		echo '<div id="itemblock-disp">
				<div><ul>
					<li><span class="itemblock-label">Campaign type</span><span class="itemblock-details">', $this->details['isteam'] ? 'Team' : 'Solo', '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Campaign name</span><span class="itemblock-details">', $this->InputSafeString($this->details['campname']), '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Created</span><span class="itemblock-details">', date('d/m/y @H:i', strtotime($this->details['created'])), '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Target</span><span class="itemblock-details">', $this->details['currency'], ' ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->details['target'], 2), '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Raised</span><span class="itemblock-details">', $this->details['currency'], ' ', $this->GetCurrency($this->details['currency'], 'cursymbol'), number_format($this->GetDonationTotal(), 2), '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Visible</span><span class="itemblock-details">', $this->details['visible'] ? 'Yes' : 'No', '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Take donations</span><span class="itemblock-details">', $this->details['enabled'] ? 'Yes' : 'No', '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Help from CR requested</span><span class="itemblock-details">', $this->details['helpfromcr'] ? 'Yes' : 'No', '</span><br class="clear" /></li>
					<li>';
		if ($image_url = $this->GetImageSRC('medium'))
		{	echo '<span class="itemblock-label">Campaign image</span><span class="itemblock-details"><img src="', $image_url, '" /></span><br class="clear" />';
		} else
		{	if ($avatar = $this->GetAvatarSRC('medium'))
			{	echo '<span class="itemblock-label">Avatar chosen</span><span class="itemblock-details"><img src="', $avatar, '" /></span><br class="clear" />';
			} else
			{	echo '<span class="itemblock-label">Campaign image</span><span class="itemblock-details">No image uploaded or avatar chosen</span><br class="clear" />';
			}
		}
		echo '</li></ul></div></div>';
		return ob_get_clean();
	} // fn ShowDetails
	
	public function InputForm($cuid = 0)
	{	ob_start();
		if (!$data = $this->details)
		{	$data = $_POST;
			if (!$_POST)
			{	$data['htmldesc'] = true;
			}
		}

		
		if ($this->id)
		{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?id=' . $this->id);
		} else
		{	if ($cuid && ($campuser = new CampaignUser($cuid)) && $campuser->id)
			{	$form = new Form($_SERVER['SCRIPT_NAME'] . '?cuid=' . $campuser->id);
				echo '<h2>Creating a campaign for ', $campuser->FullName(), '</h2>';
			} else
			{	$form = new Form($_SERVER['SCRIPT_NAME']);
			}
		}
		$form->AddTextInput('Campaign name', 'campname', $this->InputSafeString($data['campname']), 'long');
		$form->AddCheckBox('Allow HTML Description', 'htmldesc', 1, $data['htmldesc']);
		if ($data['htmldesc'])
		{	$form->AddTextArea('Description', 'description', $this->InputSafeString($data['description']), 'tinymce', 0, 0, 20, 60);
		} else
		{	$form->AddTextArea('Description', 'description', $this->InputSafeString($data['description']), '', 0, 0, 5, 60);
		}
		$form->AddSelectWithGroups('Currency for target', 'currency', $data['currency'], '', $this->CurrencyList(), 1, 1, '');
		$form->AddTextInput('Target', 'target', (int)$data['target'], 'number');
		$form->AddCheckBox('Live (visible in front end)', 'visible', 1, $data['visible']);
		$form->AddCheckBox('Enabled (taking donations)', 'enabled', 1, $data['enabled']);
		if ($this->id)
		{	$form->AddTextInput('Team of the day (highest is used if live and enabled)', 'teamoftheday', (int)$data['teamoftheday'], 'number');
			$form->AddTextInput('Priority in lists (highest priority appears first in lists)', 'listpriority', (int)$data['listpriority'], 'number');
		}
		if ($this->CanChangeType())
		{	$form->AddCheckBox('Make a ' . ($this->details['isteam'] ? 'Solo' : 'Team') . ' campaign', 'type_change', 1, '');
		}
		if (!$this->team)
		{	ob_start();
			$donation = new Donation();
			if ($countries = $donation->GetDonationCountries('crstars', true))
			{	echo '<label for="donation_country">Which area should campaign donate to</label>
						<select id="donation_country" name="donationcountry" onchange="CRRegCountryChange();">';
				foreach ($countries as $country=>$cdetails)
				{	if (!$data['donationcountry'])
					{	$data['donationcountry'] = $country;
					}
					echo '<option value="', $this->InputSafeString($country), '"', ($country == $data['donationcountry']) ? ' selected="selected"' : "", '>', $this->InputSafeString($cdetails['shortname']), '</option>';
				}
				echo '</select><br />';
				echo '<div id="countryProjectContainer">', $this->CountryProjectsList($data['donationcountry'], $data['donationproject']), '</div>';
			}
			$form->AddRawText(ob_get_clean());
		}
		$form->AddFileUpload('Upload campaign image (ideally 1000px wide by 400px)', 'campimage');
		if ($this->id && ($src = $this->GetImageSRC('small')))
		{	$form->AddRawText('<p><label>Current image</label><img src="' . $src . '" /><br /></p>');
		}
		if (!$this->id || !$this->HasImage())
		{	if ($avatars_raw = $this->GetCampaignAvatarsAvailable())
			{	$avatars = array();
				ob_start();
				echo '<p><label>... or choose an avatar</label><select name="avatarid"><option value="0">-- choose an avatar--</option>';
				foreach ($avatars_raw as $avatar_row)
				{	$avatar = new CampaignAvatar($avatar_row);
					echo '<option value="', $avatar->id, '" style="background: url(\'', $avatar->GetImageSRC('thumb'), '\') no-repeat left center; padding-left: 65px; height: 50px; background-size: 60px;"', $avatar->id == $data['avatarid'] ? ' selected="selected"' : '', '>', $this->InputSafeString($avatar->details['imagedesc']), '</option>';
				}
				echo '</select><br /></p>';
				$form->AddRawText(ob_get_clean());
			}
		}
		$form->AddCheckBox('Help from Charity Right requested', 'helpfromcr', 1, $data['helpfromcr']);
		$form->AddSubmitButton('', $this->id ? 'Save Changes' : 'Create Campaign', 'submit');
		if ($this->id)
		{	if ($this->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->id, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this campaign</a></p>';
			}
		}
		$form->Output();
		return ob_get_clean();
	} // end of fn InputForm
	
	public function CountryProjectsList($country = '', $donation_project = '')
	{	ob_start();
		$donation = new Donation();
		$countries = $donation->GetDonationCountries('crstars', true);
		if ($projects = $countries[$country]['projects'])
		{	if (false && (count($projects) == 1))
			{	foreach ($projects as $project=>$details)
				{	echo '<input type="hidden" id="donation_project" name="donationproject" value="', $project, '" />';
				}
			} else
			{	echo '<label for="donationproject">Level 2 option</label><select id="donation_project" name="donationproject">';
				foreach ($projects as $project=>$details)
				{	echo '<option value="', $project, '"', $project == $donation_project ? ' selected="selected"' : '', '>', $this->InputSafeString($details['projectname']), '</option>';
				}
				echo '</select><br />';
			}
		}
		return ob_get_clean();
	} // end of fn CountryProjectsList
	
} // end of defn AdminCampaign
?>