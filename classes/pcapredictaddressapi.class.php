<?php
class PCAPredictAddressAPI extends PCAPredictAPI
{	protected $pcap_url = 'https://services.postcodeanywhere.co.uk/Capture/Interactive/Find/v1.00/wsdlnew.ws';
	protected $test_endpoint = '';//http://charityright.your-website-demo.co.uk/pcatest.php';
	protected $apikey = 'PJ67-XK39-KP45-JG35';

	public function __construct()
	{	parent::__construct();
	} // fn __construct
	
	public function CheckPostcode(&$data = array())
	{	$fail = array();
		$success = array();
	
		if ($data['donor_country'] != 'GB')
		{	$fail[] = 'you can only donate by direct debit if you live in the UK';
		}
	
		if ($data['donor_postcode'])
		{	$fields['Text'] = $data['donor_postcode'];
		} else
		{	$fail[] = 'post code missing';
		}
		
		if (!$fail)
		{	//$this->VarDump($data);
			if ($result = $this->GetJSONPostResponse($fields))
			{	$this->VarDump($result);
				$fail[] = 'test';
				/*if ($result['IsCorrect'] == 'True')
				{	$this->VarDump($result);
					$success[] = 'account details approved';
				} else
				{	$fail[] = 'Please check your account details, we have been unable to confirm your bank details';
				}*/
			} else
			{	$fail[] = 'we are unable to approve your address at this time';
			}
		}
	
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn CheckPostcode
	
} // end of defn PCAPredictAddressAPI
?>