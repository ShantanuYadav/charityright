<?php
class MailTemplate extends Base
{	public $id = 0;
	public $details = array();
	var $fields = array();
	var $language = "";

	function __construct($id = 0, $lang = "")
	{	parent::__construct();
		$this->AssignMailLanguage($lang);
		$this->Get($id);
	} //  end of fn __construct
	
	function AssignMailLanguage($lang = "")
	{	if ($lang)
		{	$this->language = $lang;
		} else
		{	$this->language = $this->lang;
		}
	} // end of fn AssignMailLanguage
	
	function Get($id = 0)
	{	$this->Reset();
		
		if (is_array($id))
		{	$this->id = (int)$id["mailid"];
			$this->details = $id;
			$this->AddDetailsForLang($this->language);
			$this->GetFields();
		} else
		{	if ((int)$id)
			{	$sql = "SELECT * FROM mailtemplates WHERE mailid=" . (int)$id;
			} else
			{	$sql = "SELECT * FROM mailtemplates WHERE mailname='" . $this->SQLSafe($id) . "'";
			}
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	$this->Get($row);
				}
			}
		}
		
	} // end of fn Get
	
	function GetFields()
	{	$this->fields = array();
		$sql = "SELECT mailfields.* FROM mailfields, mailtemplatefields WHERE mailfields.mfid=mailtemplatefields.mfid AND mailtemplatefields.mailid={$this->id} ORDER BY mailfields.fieldname";
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->fields[$row["fieldname"]] = $row;
			}
		}
	} // end of fn GetFields
	
	function AddDetailsForLang($lang = "")
	{	$sql = "SELECT * FROM mailtemplates_lang WHERE mailid={$this->id} AND lang='$lang'";
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	foreach ($row as $field=>$value)
				{	$this->details[$field] = $value;
				}
			} else
			{	if ($lang == $this->def_lang)
				{	// as last resort go for english
					if ($lang != "en") // only if default language not english
					{	$sql = "SELECT * FROM mailtemplates_lang WHERE mailid=$this->id AND lang='en'";
						if ($result = $this->db->Query($sql))
						{	if ($row = $this->db->FetchArray($result))
							{	foreach ($row as $field=>$value)
								{	$this->details[$field] = $value;
								}
							}
						}
					}
				} else
				{	$this->AddDetailsForDefaultLang();
				}
			}
		}
	
	} // end of fn AddDetailsForLang
	
	function AddDetailsForDefaultLang()
	{	$this->AddDetailsForLang($this->def_lang);
	} // end of fn AddDetailsForDefaultLang
	
	function Reset()
	{	$this->id = 0;
		$this->details = array();
		$this->fields = array();
	} // end of fn Reset
		
	function BuildHTMLEmailText($field_overrides = array())
	{	$text = stripslashes($this->details["htmltext"]);
		foreach ($this->fields as $fieldname=>$fielddetails)
		{	
			if (isset($field_overrides[$fieldname]))
			{	$replace = $field_overrides[$fieldname];
			} else
			{	$replace = $fielddetails["fieldvalue"];
			}
			$text = str_replace("{" . $fieldname . "}", $replace, $text);
		}
		return $text;
	} // end of fn BuildHTMLEmailText
		
	function BuildHTMLPlainText($field_overrides = array())
	{	$text = stripslashes($this->details["plaintext"]);
		foreach ($this->fields as $fieldname=>$fielddetails)
		{	
			if (isset($field_overrides[$fieldname]))
			{	$replace = $field_overrides[$fieldname];
			} else
			{	$replace = $fielddetails["fieldvalue"];
			}
			$text = str_replace("{" . $fieldname . "}", $replace, $text);
		}
		return $text;
	} // end of fn BuildHTMLPlainText

} // end of defn MailTemplate
?>