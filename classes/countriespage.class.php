<?php
class CountriesPage extends AdminPage
{	protected $country;
	protected $menuarea;
	
	function __construct()
	{	parent::__construct('COUNTRIES');
	} //  end of fn __construct
	
	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess('web content'))
		{	$this->CountriesLoggedInConstruct();
		}
	} // end of fn LoggedInConstruct
	
	protected function AssignCountry()
	{	$this->country  = new AdminCountry($_GET['ctry']);
	} // end of fn AssignCountry
	
	function CountriesLoggedInConstruct()
	{	$this->AssignCountry();
		$this->css[] = 'adminctry.css';
		$this->CountryConstructFunctions();
		$this->breadcrumbs->AddCrumb('countries.php', 'Countries');
		if ($this->country->code)
		{	$this->breadcrumbs->AddCrumb('ctryedit.php?ctry=' . $this->country->code, $this->InputSafeString($this->country->details['shortname']));
		}
	} // end of fn CountriesLoggedInConstruct
	
	protected function CountryConstructFunctions()
	{	
	} // end of fn CountryConstructFunctions
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess('web content'))
		{	$this->CountriesBodyMain();
		}
	} // end of fn AdminBodyMain
	
	function CountriesBodyMain()
	{	if ($menu = $this->GetCountryMenu())
		{	echo '<div class="adminItemSubMenu"><ul>';
			foreach ($menu as $menuarea=>$menuitem)
			{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
			}
			echo '</ul><div class="clear"></div></div>';
		}
	} // end of fn CountriesBodyMain
	
	protected function GetCountryMenu()
	{	$menu = array();
		if ($this->country->code)
		{	$menu['edit'] = array('text'=>'Edit', 'link'=>'ctryedit.php?ctry=' . $this->country->code);
			if (SITE_TEST) $menu['gateways'] = array('text'=>'Payment gateways', 'link'=>'ctrygateways.php?ctry=' . $this->country->code);
		}
		return $menu;
	} // end of fn GetCountryMenu
	
} // end of defn CountriesPage
?>