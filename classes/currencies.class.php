<?php
class Currencies extends Base
{	var $currencies = array();

	function __construct() // constructor
	{	parent::__construct();
		
		$this->GetCurrencies();
	} // end of fn __construct
	
	function GetCurrencies()
	{	
		$this->Reset();
		
		if ($result = $this->db->Query('SELECT *, IF(curorder=0, 1000, curorder) AS listorder FROM currencies ORDER BY listorder, curcode'))
		{	while ($row = $this->db->FetchArray($result))
			{	$this->currencies[$row['curcode']] = new Currency($row);
			}
		}
		
	} // end of fn GetCurrencies
	
	function Reset()
	{	$this->currencies = array();
	} // end of fn Reset
	
	function UpdateAllRates()
	{	
		foreach ($this->currencies as $currency)
		{	$currency->GoogleUpdateRate();
		}
	
	} // end of fn UpdateAllRates
	
	function AdminList()
	{	echo '<table><tr class="newlink"><th colspan="8"><a href="currency.php">Add new currency</a></th></tr><tr><th>Code</th><th>Name</th><th>Symbol</th>',
			//'<th class="num">Countries<br />used in</th>',
			'<th>Used for</th>',
			'<th>Order<br />listed</th><th class="num">Conversion<br />rate</th><th>Rate updated<br /><a href="', $_SERVER['SCRIPT_NAME'], '?updateall=1">update all now</a></th><th>Actions</th></tr>';
		foreach ($this->currencies as $currency)
		{	$used = array();
			if ($currency->details['use_oneoff'])
			{	$used[] = 'One-off';
			}
			if ($currency->details['use_monthly'])
			{	$used[] = 'Monthly';
			}
			if ($currency->details['use_campaigns'])
			{	$used[] = 'Campaigns';
			}
			echo '<tr class="stripe', $i++ % 2, '"><td>', $currency->code, '</td><td>', $currency->details['curname'], '</td><td>', $currency->details['cursymbol'], ' (' , htmlentities($currency->details['cursymbol']), ')</td>',
				//'<td class="num">', count($currency->GetCountries()), '</td>',
				'<td>', implode(', ', $used), '</td>',
				'<td class="num">', $currency->details['curorder'] ? $currency->details['curorder'] : '', '</td><td class="num">', number_format($currency->details['convertrate'], 4), '</td><td>', $currency->history ? date('d/m/y @H:i', strtotime($currency->history[0]['when'])) : '', '</td><td><a href="currency.php?code=', $currency->code, '">edit</a></td></tr>';
		}
		echo '</table>';
	} // end of fn AdminList
	
} // end of defn Currencies
?>