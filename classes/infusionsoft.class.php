<?php
include_once(CITDOC_ROOT . '/infusionsoft/xmlrpc.inc.php');

class InfusionSoft extends Base
{	protected $apikey = '87b0b8b46b200b51ac02326c7c0c3d6b';
	protected $if_target = 'https://tn172.infusionsoft.com/api/xmlrpc';
	protected $if_root = 'https://tn172.infusionsoft.com/';
	protected $client;
	protected $cats = array();
	protected $tags = array('newsletter'=>742, 
		'don_oneoff'=>804, 'don_monthly'=>806, 'don_crstars'=>808, 'don_admin'=>812, 'don_giftaid'=>848, 'don_zakat'=>846,
		'optin_post'=>816, 'optin_text'=>818, 'optin_phone'=>814,
		'donyear_2015'=>820, 'donyear_2016'=>822, 'donyear_2017'=>824, 'donyear_2018'=>826, 'donyear_2019'=>828, 'donyear_2020'=>830, 'donyear_2021'=>844, 'donyear_2022'=>832, 'donyear_2023'=>834, 'donyear_2024'=>836, 'donyear_2025'=>838, 'donyear_2026'=>840, 'donyear_2027'=>842, 
		'campowner'=>810);

	function __construct()
	{	parent::__construct();
		$this->client = new xmlrpc_client($this->if_target);
	} // fn __construct
	
	public function InfusionSoftResult($operation = '', $parameters = array())
	{	if (false || !SITE_TEST)
		{	$call = new xmlrpcmsg($operation, $parameters);
			return $this->client->send($call);
		} else
		{	//$this->VarDump($operation);
			//$this->VarDump($parameters);
		}
	} // end of fn InfusionSoftResult
	
	public function AddContact($data = array(), $create_optin = true)
	{	if ($contact = $this->UploadDataFromPost($data))
		{	if ($ifuserid = $this->GetContactIdByEmail($contact['Email']))
			{	$this->InfusionSoftResult('ContactService.update', array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode($ifuserid), php_xmlrpc_encode($contact)));
				if ($create_optin)
				{	$this->SetEmailOptIn($contact['Email']);
				}
				return $ifuserid;
			} else
			{
				if(($result = $this->InfusionSoftResult('ContactService.add', array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode($contact)))) && !$result->faultCode())
				{	if ($ifuserid = $result->value()->me['i4'])
					{	if ($create_optin)
						{	$this->SetEmailOptIn($contact['Email']);
						}
						$this->AddStoredContactIDFromEmail($contact['Email'], $ifuserid);
						return $ifuserid;
					}
				} //else $this->varDump($result);
			}
		} //else echo 'no data';
	} // end of fn AddContact
	
	public function DeleteInvoice($invid = 0)
	{	if ($invid = (int)$invid)
		{	$params = array(php_xmlrpc_encode($this->apikey), 
							php_xmlrpc_encode($invid)
						);
			//$this->VarDump($params);
			if (($result = $this->InfusionSoftResult('InvoiceService.deleteInvoice', $params)) && !$result->faultCode())
			{	return true;
			} //else $this->VarDump($result);
		}
	} // fn DeleteInvoice
	
	public function AddBlankInvoice($data = array())
	{	if (is_array($data) && $data)
		{	$params = array(php_xmlrpc_encode($this->apikey), 
							php_xmlrpc_encode($data['contactID']),
							php_xmlrpc_encode($data['name']),
							php_xmlrpc_encode(date('Ymd\TH:i:s', strtotime($data['date'])), array('auto_dates')),
							php_xmlrpc_encode(0),
							php_xmlrpc_encode(0)
						);
			//$this->VarDump($params);
			if (true && ($result = $this->InfusionSoftResult('InvoiceService.createBlankOrder', $params)) && !$result->faultCode())
			{	if ($iforderid = $result->value()->me['i4'])
				{	return $iforderid;
				}
			} //else $this->VarDump($result);
		}
	} // fn AddBlankInvoice
	
	public function AddDonationToInvoice($data = array(), $orderid = 0)
	{	if (is_array($data) && $data && ($orderid = (int)$orderid))
		{	$params = array(php_xmlrpc_encode($this->apikey), 
							php_xmlrpc_encode($orderid),
							php_xmlrpc_encode(0),
							php_xmlrpc_encode(7),
							php_xmlrpc_encode(floatval($data['gbpamount'])),
							php_xmlrpc_encode((int)$data['quantity']),
							php_xmlrpc_encode($data['description']),
							php_xmlrpc_encode($data['notes'])
						);
			if (true && ($result = $this->InfusionSoftResult('InvoiceService.addOrderItem', $params)) && !$result->faultCode())
			{	if ($ifitemid = $result->value()->me['i4'])
				{	return $ifitemid;
				}
			} //else $this->VarDump($result);
		}
	} // fn AddDonationToInvoice
	
	public function AddPaymentToInvoice($data = array(), $orderid = 0)
	{	if (is_array($data) && $data && ($orderid = (int)$orderid))
		{	$params = array(php_xmlrpc_encode($this->apikey), 
							php_xmlrpc_encode($orderid),
							php_xmlrpc_encode(floatval($data['gbpamount'])),
							php_xmlrpc_encode(date('Ymd\TH:i:s', strtotime($data['date'])), array('auto_dates')),
							php_xmlrpc_encode($data['paytype']),
							php_xmlrpc_encode($data['description']),
							php_xmlrpc_encode(false)
						);
			//$this->VarDump($params);
			if (true && ($result = $this->InfusionSoftResult('InvoiceService.addManualPayment', $params)) && !$result->faultCode())
			{	if ($ifpaidid = $result->value()->me['i4'])
				{	return $ifpaidid;
				}
				//$this->VarDump($result->value());
			} //else $this->VarDump($result);
		}
	} // fn AddPaymentToInvoice
	
	public function AddExtraToInvoice($data = array(), $orderid = 0, $diagnostics = false)
	{	if (is_array($data) && $data && ($orderid = (int)$orderid))
		{	$params = array(php_xmlrpc_encode($this->apikey), 
							php_xmlrpc_encode('Job'),
							php_xmlrpc_encode($orderid),
							php_xmlrpc_encode($data, array('auto_dates'))
						);
			if (true && ($result = $this->InfusionSoftResult('DataService.update', $params)) && !$result->faultCode())
			{	return $orderid;
			} else 
			{	if ($diagnostics)
				{	$this->VarDump($params);
					$this->VarDump($result);
				}
			}
		}
	} // fn AddExtraToInvoice
	
	public function RefundInvoice($orderid = 0)
	{	return $this->AddExtraToInvoice(array('_refunded'=>1), $orderid);
	} // end of fn RefundInvoice
	
	public function UploadDataFromPost($post = array())
	{	if ($post['email'])
		{	$data = array();
			if ($post['firstname'])
			{	$data['FirstName'] = $post['firstname'];
			}
			if ($post['lastname'])
			{	$data['LastName'] = $post['lastname'];
			}
			if (!$data)
			{	if ($post['fullname'])
				{	$fullname = explode(' ', $post['fullname'], 2);
					if ($fullname[0])
					{	$data['FirstName'] = $fullname[0];
					}
					if ($fullname[1])
					{	$data['LastName'] = $fullname[1];
					}
				}
			}
			$data['Email'] = $post['email'];
			if ($post['country'])
			{	$data['Country'] = $post['country'];
			}
			return $data;
		}
	} // end of fn UploadDataFromPost
	
	public function UpdateContact($member)
	{	if (($ifuserid = (int)$member->personal['ifuserid']) && ($contact = $this->UploadDataFromPost($member)))
		{
			if ($result = $this->InfusionSoftResult('ContactService.update', array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode($ifuserid), php_xmlrpc_encode($contact))))
			{	return !$result->faultCode();
			}
		}
	} // end of fn UpdateContact
	
	public function AddOptinTagsToContact($contactid = 0, ContactPreferences $cp_options, $contactpref = 0)
	{	if ($contactid = (int)$contactid)
		{	foreach ($cp_options->RawOptions() as $option)
			{	if ($cp_options->IsOptionsSetFromValue($contactpref, $option['cpcode']))
				{	$this->AddTagToContact($contactid, 'optin_' . $option['cpcode']);
				} else
				{	$this->RemoveTagFromContact($contactid, 'optin_' . $option['cpcode']);
				}
			}
		}
	} // end of fn AddOptinTagsToContact
	
	public function AddTagToEmail($email = 0, $tag = '')
	{	if (($email = $this->CRMEmailFromEmail($email)) && $email['ifid'] && ($contactid = (int)$email['ifid']) && ($tagid = (int)$this->tags[$tag]))
		{	return $this->AddTagIDToContact($contactid, $tagid);
		}
	} // end of fn AddTagToEmail
	
	public function AddDonationYearTagToContact($contactid = 0, $date = '')
	{	if (($contactid = (int)$contactid) && ($year = date('Y', strtotime($date))) && ($tagid = (int)$this->tags['donyear_' . $year]))
		{	return $this->AddTagIDToContact($contactid, $tagid);
		}
	} // end of fn AddDonationYearTagToContact
	
	public function AddTagToContact($contactid = 0, $tag = '')
	{	if (($contactid = (int)$contactid) && ($tagid = (int)$this->tags[$tag]))
		{	return $this->AddTagIDToContact($contactid, $tagid);
		}
	} // end of fn AddTagToContact
	
	public function AddTagIDToContact($contactid = 0, $tagid = 0)
	{	if (($contactid = (int)$contactid) && ($tagid = (int)$tagid))
		{	
			if ($result = $this->InfusionSoftResult('ContactService.addToGroup', array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode($contactid), php_xmlrpc_encode($tagid))))
			{	if ($result->faultCode())
				{	$this->LogFail($result, $contactid, $tagid);
				} else
				{	return true;
				}
			}
		}
	} // end of fn AddTagIDToContact

	public function LogFail($result, $contactid = 0, $tagid = 0)
	{	if ($f = fopen(CITDOC_ROOT . '/if_log/fail.txt', 'a'))
		{	fputs($f, date('Y-m-d H:i:s') . '|' . $tagid . '|' . $contactid . '|' . serialize($result) . "\r\n");
			fclose($f);
		}
	} // end of fn LogFail
	
	public function SetEmailOptIn($email = 0, $optin = true)
	{	if ($email)
		{	if ($result = $this->InfusionSoftResult('APIEmailService.opt' . ($optin ? 'In' : 'Out'), array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode($email), php_xmlrpc_encode('API Opt in change from Website'))))
			{	if ($result->faultCode())
				{	$this->LogFail($result, $ifuserid, 'Opt' . ($optin ? 'In' : 'Out') . ' ' . $email);
				} else
				{	return true;
				}
			}
		}
	} // end of fn SetEmailOptIn
	
	public function RemoveTagFromContact($contactid = 0, $tag = '')
	{	if (($contactid = (int)$contactid) && ($tagid = (int)$this->tags[$tag]))
		{	return $this->RemoveTagIDFromContact($contactid, $tagid);
		}
	} // end of fn RemoveTagFromContact
	
	public function RemoveTagIDFromContact($contactid = 0, $tagid = 0)
	{	if (($contactid = (int)$contactid) && ($tagid = (int)$tagid))
		{	
			if ($result = $this->InfusionSoftResult('ContactService.removeFromGroup', array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode($contactid), php_xmlrpc_encode($tagid))))
			{	return !$result->faultCode();
			}
		}
	} // end of fn RemoveTagIDFromContact
	
	public function GetAllGroups()
	{	return $this->GetDataFromTable('ContactGroup', array('Id' => '%'), array('GroupCategoryId', 'GroupDescription', 'GroupName', 'Id' ), 'Id');
	} // end of fn GetAllGroups
	
	public function GetMemberDetailsFromContactId($contactid = 0)
	{	if ($results = $this->GetDataFromTable('Contact', array('Id' => (int)$contactid), array('Id', 'FirstName', 'Nickname', 'LastName', 'Email', 'Phone1', 'City', 'Country', 'DateCreated'), 'Id'))
		{	if ($results[$contactid])
			{	return $results[$contactid];
			}
		}
	} // end of fn GetMemberDetailsFromContactId
	
	public function OutputInfusionDataField($value = '')
	{	
		// check for date
		if (preg_match('|^[\d]{8}T[\d]{2}:[\d]{2}:[\d]{2}$|', $value))
		{	return date('d-M-Y @H:i', strtotime($value . ' EST')) . ' [' . $value . ']';
		}
	
		return $value;
	} // end of fn OutputInfusionDataField
	
	public function GetAllGroupCategories()
	{	return $this->GetDataFromTable('ContactGroupCategory', array('Id' => '%'), array('CategoryDescription', 'CategoryName', 'Id' ), 'Id');
	} // end of fn GetAllGroupCategories
	
	public function GetDataFromTable($table = '', $query = array(), $fields = array(), $idfield = '')
	{	$rows = array();
		$page = 0;
		$xml_data = array('table'=>php_xmlrpc_encode($table), 'key'=>php_xmlrpc_encode($this->apikey), 'query'=>php_xmlrpc_encode($query), 'fields'=>php_xmlrpc_encode($fields), 'limit'=>php_xmlrpc_encode(1000));
		do
		{	$count = 0;
			
			if ($result = $this->InfusionSoftResult('DataService.query', array($xml_data['key'], $xml_data['table'], $xml_data['limit'], php_xmlrpc_encode($page++), $xml_data['query'], $xml_data['fields'])))
			{	
				if ($results = $result->val->me)
				{	foreach ($results as $result_list)
					{	foreach ($result_list as $group)
						{	$row = array();
							foreach ($group->me['struct'] as $field=>$data)
							{	foreach ($data->me as $value)
								{	$row[$field] = $value;
									break;
								}
							}
							if ($idfield)
							{	if ($row[$idfield])
								{	$rows[$row[$idfield]] = $row;
								}
							} else
							{	$rows[] = $row;
							}
							$count++;
						}
					}
				}
			}
		} while ($count);
		return $rows;
	} // end of fn GetDataFromTable
	
	public function DeleteContact($contactid = 0)
	{	if ($contactid  = (int)$contactid)
		{	
			if ($result = $this->InfusionSoftResult('DataService.delete', array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode('Contact'), php_xmlrpc_encode($contactid))))
			{	return !$result->faultCode();
			}
		}
	} // end of fn DeleteContact
	
	public function CreateNewTag($name = '', $catid = 0)
	{	if (!$this->GetTagByName($name))
		{	
			if ($result = $this->InfusionSoftResult('DataService.add', array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode('ContactGroup'), php_xmlrpc_encode(array('GroupName'=>$name, 'GroupCategoryId'=>(int)$catid)))))
			{	//$this->VarDump($result);
			}
		}
	} // end of fn CreateNewTag
	
	public function DeleteExistingTag($tagid = 0)
	{	
			if ($result = $this->InfusionSoftResult('DataService.delete', array(php_xmlrpc_encode($this->apikey), php_xmlrpc_encode('ContactGroup'), php_xmlrpc_encode($tagid))))
			{	$this->VarDump($result);
			}
	} // end of fn DeleteExistingTag
	
	public function GetTagByName($name = '')
	{	
		if ($tags = $this->GetDataFromTable('ContactGroup', array('GroupName' => $name), array('Id' ), 'Id'))
		{	foreach ($tags as $tag)
			{	return $tag['Id'];
			}
		}
		
	} // end of fn GetTagByName
	
	public function GetContactIdByEmail($email = '')
	{	if (($ifcontact = $this->GetStoredContactIDFromEmail($email)) && ($ifid = (int)$ifcontact['ifid']))
		{	return $ifid;
		} else
		{	if ($results = $this->GetDataFromTable('Contact', array('Email' => $email), array('Id', 'FirstName', 'LastName', 'Email'), 'Id'))
			{	foreach ($results as $contact)
				{	if ($ifid = (int)$contact['Id'])
					{	$this->AddStoredContactIDFromEmail($email, $ifid);
						return $ifid;
					}
				}
			}
		}
	} // end of fn GetContactIdByEmail
	
	public function GetInfusionsoftLinkFromEmail($email = '')
	{	if (($ifcontact = $this->GetStoredContactIDFromEmail($email)) && ($ifid = (int)$ifcontact['ifid']))
		{	return $this->if_root . 'Contact/manageContact.jsp?view=edit&ID=' . $ifid;
		}
	} // end of fn GetInfusionsoftLinkFromEmail
	
	public function GetInfusionsoftOrderLink($ifid = '')
	{	if ($ifcontact = (int)$ifid)
		{	return $this->if_root . 'Job/manageJob.jsp?view=edit&ID=' . $ifid;
		}
	} // end of fn GetInfusionsoftOrderLink
	
	public function GetStoredContactIDFromEmail($email = '')
	{	$sql = 'SELECT ifid FROM infusionsoftcontacts WHERE email="' . $this->SQLSafe($email) . '"';
		if ($result = $this->db->Query($sql))
		{	if ($row = $this->db->FetchArray($result))
			{	return $row;
			}
		}
		return false;
	} // end of fn GetStoredContactIDFromEmail
	
	public function AddStoredContactIDFromEmail($email = '', $ifid = 0)
	{	if (($ifid = (int)$ifid) && ($email = $this->SQLSafe($email)))
		{	$insert_sql = 'INSERT INTO infusionsoftcontacts SET email="' . $email . '", ifid=' . $ifid . ', timeadded="' . $this->datefn->SQLDateTime() . '"';
			if ($result = $this->db->Query($insert_sql))
			{	return $this->db->AffectedRows();
			}
		}
		return false;
	} // end of fn GetStoredContactIDFromEmail
	
	public function InfusionSoftDateFromSQL($date = '')
	{	return $this->InfusionSoftDateFromStamp(strtotime($date));
	} // end of fn InfusionSoftDateFromSQL
	
	public function InfusionSoftDateFromStamp($stamp = 0)
	{	return date('Ymd\TH:i:s');
	} // end of fn InfusionSoftDateFromStamp
	
	public function InfusionSoftDateToStamp($date = '')
	{	return strtotime($date . ' EST');
	} // end of fn InfusionSoftDateFromStamp
	
	public function InfusionSoftDateToSQL($date = '')
	{	return $this->datefn->SQLDateTime($this->InfusionSoftDateToStamp($date));
	} // end of fn InfusionSoftDateFromStamp
	
	protected function CRMEmailFromEmail($email = 0)
	{	if (is_array($email))
		{	return $email;
		} else
		{	$sql = 'SELECT * FROM infusionsoftcontacts WHERE email="' . $this->SQLSafe($email) . '"';
			if ($result = $this->db->Query($sql))
			{	if ($row = $this->db->FetchArray($result))
				{	return $row;
				}
			}
		}
	} // fn CRMEmailFromEmail
	
	public function BuildInitialList()
	{	$this->db->Query('TRUNCATE TABLE if_initial');
		$sql = 'SELECT * FROM campaigns WHERE infusionsoftid=0 AND NOT cuid=0';
		if (SITE_TEST)
		{	$sql = 'SELECT campaigns.* FROM campaigns, campaignusers WHERE campaigns.cuid=campaignusers.cuid AND infusionsoftid=0 AND campaignusers.email="fred@websquare.co.uk"';
		}
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$insertsql = 'INSERT INTO if_initial SET linetype="campaign", linedate="' . $row['created'] . '", lineid=' . $row['cid'];
				$this->db->Query($insertsql);
				$count++;
			}
		}
		$sql = 'SELECT * FROM donations WHERE infusionsoftid=0';
		if (SITE_TEST)
		{	$sql .= ' AND donoremail="fred@websquare.co.uk"';
		}
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$insertsql = 'INSERT INTO if_initial SET linetype="donation", linedate="' . $row['created'] . '", lineid=' . $row['did'];
				$this->db->Query($insertsql);
				$count++;
			}
		}
		$sql = 'SELECT * FROM campaigndonations WHERE infusionsoftid=0';
		if (SITE_TEST)
		{	$sql .= ' AND donoremail="fred@websquare.co.uk"';
		} else
		{	$sql .= ' AND NOT donoremail=""';
		}
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$insertsql = 'INSERT INTO if_initial SET linetype="campaigndonation", linedate="' . $row['donated'] . '", lineid=' . $row['cdid'];
				$this->db->Query($insertsql);
				$count++;
			}
		}
		return $count;
	} // end of fn BuildInitialList
	
	public function InitialiseContacts($limit = 10)
	{	$start = microtime(true);
		$this->BuildInitialList();
		$if_don = new InfusionSoftDonation();
		$if_cdon = new InfusionSoftCampaignDonation();
		$if_camp = new InfusionSoftCampaignOwner();
		$sql = 'SELECT * FROM if_initial ORDER BY linedate ASC';
		if ($result = $this->db->Query($sql))
		{	$total = $this->db->NumRows($result);
			while ($row = $this->db->FetchArray($result))
			{	//print_r($row);
				switch ($row['linetype'])
				{	case 'campaign':
						$if_camp->AddCampaign($row['lineid']);
						break;
					case 'donation':
						$if_don->AddDonation($row['lineid']);
						$if_don->PayDonation($row['lineid']);
						$if_don->ConfirmDirectDebit($row['lineid']);
						break;
					case 'campaigndonation':
						$if_cdon->AddDonation($row['lineid']);
						$if_cdon->PayDonation($row['lineid']);
						break;
				}
				if ($count++ >= $limit)
				{	break;
				}
			}
		}
		echo "\n", $count, ' / ', $total, ', time taken ', number_format(microtime(true) - $start, 3), 's';
	} // end of fn InitialiseContacts
	
} // end if class defn InfusionSoft
?>