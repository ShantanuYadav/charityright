<?php
class BaseFunctions extends Base
{	var $css = array();
	var $cssroot = '';
	var $js = array();
	var $failmessage = '';
	var $successmessage = '';
	var $warningmessage = '';
	var $meta = array('DESCRIPTION'=>'',
					'KEYWORDS'=>'',
					'RATING'=>'General',
					'ROBOTS'=>'index,follow',
					'REVISIT-AFTER'=>'7 days',
					'DISTRIBUTION'=>'Global');
	protected $camp_customer;
	protected $autologin_cookiename = 'crs_ali';

	function __construct() // constructor
	{	parent::__construct();
		$this->cssroot = CSS_ROOT;
	} // end of fn __construct, constructor

	function Header()
	{
	} // end of fn Header

	function FailMessage() //
	{
	} // end of fn FailMessage

	function SuccessMessage() //
	{
	} // end of fn SuccessMessage

	function MainBody() //
	{
	} // end of fn MainBody

	function BodyDefn() // display actual page
	{	echo "<body>\n";
	} // end of fn BodyDefn

	function Page() // display actual page
	{	$this->HTMLHeader();
		$this->DisplayTitle();
		$this->BodyDefn();
		$this->Header();
		$this->MainBody();
		$this->Footer();
		echo "</body>\n</html>\n";
	} // end of fn Page

	function Footer()
	{
	} // end of fn Footer

	function SetTitle($title)
	{	$this->title = $title;
	} // end of fn SetTitle

	function HTMLHeader()
	{	echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' ",
				"'https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\n<html xmlns='https://www.w3.org/1999/xhtml'>\n";
	} // end of fn HTMLHeader

	function DisplayTitle()
	{	echo "<head>\n<title>", $this->title, "</title>\n</head>\n";
	} // end of fn DisplayTitle

	function CSSInclude()
	{	if ($force_refresh = $this->FlagFileSet('forcecss'))
		{	$timestamp = substr(time(), -10, 6);
		}
		foreach ($this->css as $css_file)
		{	if (substr($css_file, 0, 7) == "http://" || substr($css_file, 0, 8) == "https://")
			{	echo "\t<link rel='stylesheet' href='", $css_file, "' integrity='sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr' crossorigin='anonymous' />\n";
			} else
			{
				echo "\t<link rel='stylesheet' href='", CSS_ROOT, $css_file;
				if ($force_refresh)
				{	echo strstr($css_file, "?") ? "&" : "?", $timestamp;
				} else
				{	if ($mtime = @filemtime(CITDOC_ROOT . '/css/' . $css_file))
					{	echo strstr($css_file, '?') ? '&' : '?', 'ver=', substr($mtime, -5);
					}
				}
				echo "' type='text/css' media='screen' />\n";
			}
		}
	} // end of fn CSSInclude

	function JSInclude()
	{	
		
		// if ($force_refresh = $this->FlagFileSet('forcejs'))
		// {	$timestamp = substr(time(), -10, 6);
		// }

		// $timestamp = substr(time(), -10, 6);

		$force_refresh = true;
		$timestamp = substr(time(), -10, 6);

		foreach ($this->js as $script)
		{	
			if($script == 'donations.js'){
				echo "\t<script type='text/javascript' src='",JS_ROOT, $script,"?ver=",substr(time(), -10, 6), "'></script>\n";
			}elseif (substr($script, 0, 7) == "http://" || substr($script, 0, 8) == "https://")
			{	echo "\t<script type='text/javascript' src='", $script, "'></script>\n";
			} else
			{	echo "\t<script type='text/javascript' src='", JS_ROOT, $script;
				if ($force_refresh)
				{	echo strstr($script, "?") ? "&" : "?", $timestamp;
				} else
				{	if ($mtime = @filemtime(CITDOC_ROOT . '/js/' . $script))
					{	echo strstr($script, '?') ? '&' : '?', 'ver=', substr($mtime, -5);
					}
				}
				echo "'></script>\n";
			}
		}
	} // end of fn JSInclude

	function SetFrontEndUsers()
	{
		if ($_SESSION[SITE_NAME]['camp_customer'])
		{	// check for logout
			if ($_GET['logout'])
			{	unset($_SESSION[SITE_NAME]['camp_customer']);
				unset($_SESSION[SITE_NAME]['camp_customer_email']);
				$this->AutoLogInDelete();
			} else
			{	// assign already logged in customer
				$this->camp_customer = new CampaignUser($_SESSION[SITE_NAME]['camp_customer']);
				if ($this->camp_customer->details['suspended'])
				{	
					unset($_SESSION[SITE_NAME]['camp_customer']);
					unset($_SESSION[SITE_NAME]['camp_customer_email']);
					$this->AutoLogInDelete();
					unset($this->camp_customer);
				}
			}
		} else
		{	// check for attempted log in
			if ($_POST['campusername'] && $_POST['camppass'])
			{	if ($camp_customer = $this->CampaignLogIn($_POST['campusername'], $_POST['camppass']))
				{	$this->camp_customer = new CampaignUser($camp_customer);
					if ($this->camp_customer->id)
					{	
						$_SESSION[SITE_NAME]['camp_customer'] = $this->camp_customer->id;
						$_SESSION[SITE_NAME]['camp_customer_email'] = $this->camp_customer->details['email'];
						if ($_POST['keeploggedin'])
						{	$this->CreateAutoLogIn($this->camp_customer->id);
						}
						if ($_POST['passurl'])
						{	header('location: ' . urldecode($_POST['passurl']));
							exit;
						}
						return true;
					}
				} else
				{	$this->failmessage = 'log in failed';
				}
			} else
			{	if (!$_SESSION[SITE_NAME]['camp_customer'] && $camp_customer = $this->AutoLogIn())
				{	$this->camp_customer = new CampaignUser($camp_customer);
					if ($this->camp_customer->id)
					{	$_SESSION[SITE_NAME]['camp_customer'] = $this->camp_customer->id;
						return true;
					}
				}
			}
		}
	} // end of fn SetFrontEndUsers

	function CampaignLogIn($username = '', $pass = '')
	{
		if ($result = $this->db->Query('SELECT * FROM campaignusers WHERE email="' . $this->SQLSafe($username) . '" AND password=MD5("' . $this->SQLSafe($pass) . '") AND suspended=0'))
		{	if ($row = $this->db->FetchArray($result))
			{	return $row;
			} else $this->failmessage = 'log in failed';
		} else $this->failmessage = 'log in failed';
		return false;
	} // end of fn CampaignLogIn

	function AutoLogInTimeToSet()
	{	return strtotime('+1 month');
	} // end of fn AutoLogInTimeToSet

	function AutoLogInDelete()
	{
		$al_bits = $this->AutoLogInCookieParts();
		$sql = 'DELETE FROM autologin WHERE userid=' . $al_bits['userid'] . ' AND cookiecode="' . $al_bits['cookiecode'] . '"';
		if ($result = $this->db->Query($sql))
		{	if ($this->db->AffectedRows())
			{	setcookie($this->autologin_cookiename, '', time() - 3600);
			}
		}

	} // end of fn AutoLogInDelete

	function AutoLogIn()
	{	if (isset($_COOKIE[$this->autologin_cookiename]))
		{
			$al_bits = $this->AutoLogInCookieParts();
			$now = time();
			$sql = 'SELECT userid FROM autologin WHERE userid=' . $al_bits['userid'] . ' AND cookiecode="' . $al_bits['cookiecode'] . '" AND expirydate>' . $now;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->NumRows($result))
				{	$this->SetAutoLogInCookie($al_bits['userid'], $al_bits['cookiecode'], $this->AutoLogInTimeToSet());
					return $al_bits['userid'];
				} else
				{	//echo $sql;
				}
			}//else echo "<p>", $this->db->Error(), "</p>\n";
		} //else echo "cookie not set";
	} // end of fn AutoLogIn

	function AutoLogInCookieParts()
	{	$al_bits = explode('_', $_COOKIE[$this->autologin_cookiename]);
		return array('userid'=>(int)$al_bits[0], 'cookiecode'=>$this->SQLSafe($al_bits[1]));
	} // end of fn AutoLogInCookieParts

	function CreateAutoLogIn($userid = 0)
	{
		if (($userid = (int)$userid) && ($code = $this->ConfirmCode(30)) && ($expiry_stamp = $this->AutoLogInTimeToSet()))
		{	$sql = 'INSERT INTO autologin SET userid=' . $userid . ', cookiecode="' . $code . '", expirydate=' . $expiry_stamp;
			if ($result = $this->db->Query($sql))
			{	if ($this->db->AffectedRows() && ($alid = $this->db->InsertID()))
				{	$this->SetAutoLogInCookie($userid, $code, $expiry_stamp);
				}
			}
		}
	} // end of fn CreateAutoLogIn

	function SetAutoLogInCookie($userid = 0, $code = '', $expiry_stamp = 0)
	{	setcookie($this->autologin_cookiename, $userid . '_' . $code, $expiry_stamp);
	} // end of fn SetAutoLogInCookie

} // end of defn BaseFunctions
?>