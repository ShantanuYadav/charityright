<?php
class CRStarsCampaignViewPage extends CRStarsPage
{	protected $campaign;
	protected $tm_perblock = 10;

	public function __construct()
	{	parent::__construct();
	} // end of fn __construct

	protected function BaseConstructFunctions()
	{	$this->campaign = new Campaign($_GET['cid'], $_GET['slug']);
		if (!$this->campaign->details['visible'])
		{	header('location: ' . SITE_URL . 'cr-stars/campaign-ended/');
			exit;
		}
		$this->canonical_link = $this->campaign->Link();
		$this->title = strip_tags($this->campaign->FullTitle());
		switch ($_GET['action'])
		{	case 'follow':
				$this->camp_customer->FollowCampaign($this->campaign->id);
				break;
			case 'unfollow':
				$this->camp_customer->UnFollowCampaign($this->campaign->id);
				break;
		}

	} // end of fn BaseConstructFunctions

	public function GetMetaDesc()
	{	return $this->campaign->TextSnippet();
	} // end of fn MetaTags

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		$this->campaign->SetFBMeta($this->fb_meta);
	} // end of fn SetFBMeta

	/* possible header buttons (from right):
	 * - view team - if this is a member of another team
	 * - join team - if member of another team or is a team campaign and can be joined
	 * - edit - if owner is viewing
	 * - follow / unfollow - if logged in and not owner
	 */
	function MainBodyContent()
	{	$raised = $this->campaign->GetDonationTotal();
		$cursymbols = array();
		$cursymbols[$this->campaign->details['currency']] = $this->GetCurrency($this->campaign->details['currency'], 'cursymbol');
		$headerbuttons = array();
		$mobilebuttons = array();
		if ($this->campaign->team)
		{	$team = new Campaign($this->campaign->team);
			if (!$cursymbols[$team->details['currency']])
			{	$cursymbols[$team->details['currency']] = $this->GetCurrency($team->details['currency'], 'cursymbol');
			}
			$mobilebuttons[count($mobilebuttons)] = '<a href="' . $team->Link() . '">view team</a>';
			$headerbuttons[] = '<li>' . $mobilebuttons[count($mobilebuttons) - 1] . '</li>';
			if ($team->CanBeJoined($this->camp_customer->id))
			{	if ($this->camp_customer->id)
				{	$mobilebuttons[count($mobilebuttons)] = '<a href="' . $team->JoinLink() . '">join team</li>';
					$headerbuttons[] = '<li>' . $mobilebuttons[count($mobilebuttons) - 1] . '</li>';
				} else
				{	$mobilebuttons[count($mobilebuttons)] = '<a href="' . $team->RegisterLink() . '">join team</a>';
					$headerbuttons[] = '<li>' . $mobilebuttons[count($mobilebuttons) - 1] . '</li>';
				}
			}
		}
		if ($this->camp_customer->id)
		{	if ($this->camp_customer->id == $this->campaign->details['cuid'])
			{	$mobilebuttons[count($mobilebuttons)] = '<a href="' . $this->campaign->EditLink() . '">edit campaign</a>';
				$headerbuttons[] = '<li>' . $mobilebuttons[count($mobilebuttons) - 1] . '</li>';
			} else
			{	if ($this->campaign->CanBeJoined($this->camp_customer->id))
				{	$mobilebuttons[count($mobilebuttons)] = '<a href="' . $this->campaign->JoinLink() . '">join team</a>';
					$headerbuttons[] = '<li>' . $mobilebuttons[count($mobilebuttons) - 1] . '</li>';
				}
				if ($this->camp_customer->IsFollowing($this->campaign->id))
				{	$mobilebuttons[count($mobilebuttons)] = '<li><a href="' . $this->campaign->Link() . 'unfollow/">stop following</a></li>';
					$headerbuttons[] = '<li>' . $mobilebuttons[count($mobilebuttons) - 1] . '</li>';
				} else
				{	$mobilebuttons[count($mobilebuttons)] = '<a href="' . $this->campaign->Link() . 'follow/">follow</a>';
					$headerbuttons[] = '<li>' . $mobilebuttons[count($mobilebuttons) - 1] . '</li>';
				}
			}
		} else
		{	if ($this->campaign->CanBeJoined())
			{	$mobilebuttons[count($mobilebuttons)] = '<a href="' . $this->campaign->RegisterLink() . '">join team</a>';
				$headerbuttons[] = '<li>' . $mobilebuttons[count($mobilebuttons) - 1] . '</li>';
			}
		}

		echo '<div class="container"><div class="container_inner campPageInner"><div class="campPageHeader"';
		if ($image = $this->campaign->GetImageSRC('large'))
		{	echo ' style="background: #aaaaaa url(\'', $image, '\') no-repeat center center; background-size: 100% auto;"';
		} else
		{	if ($avatar = $this->campaign->GetAvatarSRC('large'))
			{	echo ' style="background-image: url(\'', $avatar, '\'); background-size: 100% auto;"';
			}
		}
		echo '>';
		if ($this->campaign->owner)
		{	$owner = new CampaignUser($this->campaign->owner);
			$fullname = $owner->FullName();
			echo '<a href="', $owner->Link(), '" class="campHeaderUserImage">';
			if ($src = $owner->GetImageSRC('medium'))
			{	echo '<img src="', $src, '" alt="', $fullname, '" title="', $fullname, '" />';
			}
			echo '</a>';
		}
		if ($headerbuttons)
		{	echo '<ul class="campHeaderButtons">', implode('', $headerbuttons), '</ul>';
		}

		echo '</div><div class="campRightBar"><h1 class="page_heading">', $fulltitle = $this->campaign->FullTitle(), '</h1>', $this->campaign->DescriptionHTML(), '</div><div class="campLeftBar"><div class="campTarget"><div class="campTargetOuter"><div class="campTargetAcheived"',
			//' style="height: ', $this->campaign->details['target'] > $raised ? round(100 * (($this->campaign->details['target'] - $raised) / $this->campaign->details['target'])) : '0', '%;"',
			'></div><div class="campTargetRaised">', $cursymbols[$this->campaign->details['currency']], number_format($raised), '</div><div class="campTargetInner"><h2>Target</h2><p>', $cursymbols[$this->campaign->details['currency']], number_format($this->campaign->details['target']), '</p></div></div></div>';
		if ($raised_pc = $this->campaign->details['target'] > $raised ? floor(($raised * 100) / $this->campaign->details['target']) : 100)
		{	echo '<script>
			$(window).scroll(function()
			{	if (!$(\'.campTargetAcheived\').hasClass(\'t365team_raised_viewed\'))
				{	var hT = $(\'.campTargetAcheived\').offset().top, hH = $(\'.campTargetAcheived\').outerHeight();
					if ($(this).scrollTop() > (hT + hH - $(window).height()))
					{	$(\'.campTargetAcheived\').addClass(\'t365team_raised_viewed\');
						TeamRaisedHeight(0, ', $raised_pc, ');
					}
				}
			});

			function TeamRaisedHeight(height, last_height)
			{	$(\'.campTargetAcheived\').css(\'height\', String(height) + \'%\');
				if (height < last_height)
				{	setTimeout(function(){TeamRaisedHeight(height + 1, last_height);}, 40);
				}
			} // end of fn TeamRaisedHeight
			</script>
			';
		}

		if ($this->campaign->details['enabled'])
		{	echo '<div class="campDonateButton"><a href="', $this->campaign->DonateLink(), '">Donate<span>Click Here</span></a></div>';
		} else
		{	echo '<div class="campNoDonate">Donations no longer being taken</div>';
		}

		echo '<div class="campShareButton">';
			echo '<div class="ssk-block" style="width:100%;"><a href="" class="ssk ssk-text ssk-facebook ssk-lg" data-url="'.$this->campaign->Link().'" data-title="Support '.strip_tags($fulltitle).'">Share on Facebook</a></div>';
		echo '</div>';

		if ($donations = $this->campaign->GetDonations())
		{	echo '<div class="campDonationsList"><h2>Latest Donations</h2><ul>', $this->ListCampaignDonations($donations, 0 ,SITE_TEST ? 3 : 10), '</ul></div>';
		}
		if ($this->camp_customer->id)
		{	if ($this->camp_customer->id != $this->campaign->details['cuid'])
			{
				echo '<div class="mobile-follow-campaign">';
				if ($this->camp_customer->IsFollowing($this->campaign->id))
				{	echo '<a href="' . $this->campaign->Link() . 'unfollow/">stop following</a>';
				} else
				{	echo '<a href="' . $this->campaign->Link() . 'follow/">follow this campaign</a>';
				}
				echo '</div>';
			}
		}

		if ($team_members = $this->GetTeamMembers())
		{	echo '<div class="campTeamMembersList"><h2>Team Members</h2><div class="campTeamMembersListInner">';
			$groupcount = 0;
			$membercount = 0;
			foreach ($team_members as $team_member)
			{	if (++$membercount > $this->tm_perblock)
				{	$membercount = 0;
					echo '<div class="campTeamMemberMore" id="campTeamMemberMore_', ++$groupcount, '"><a class="button" onclick="$(\'#campTeamMemberBlock_', $groupcount, '\').removeClass(\'hidden\'); $(\'#campTeamMemberMore_', $groupcount, '\').addClass(\'hidden\');">Show more</a></div><div class="hidden" id="campTeamMemberBlock_', $groupcount, '">';
				}
				echo '<div class="campTeamMember"><div class="campMembersItemHeader"><a href="', $team_member['link'], '"><div class="campMembersItemTitle">', $team_member['title'], '</div><div class="clear"></div></a></div><div class="campMembersItemAmount">Raised ', $team_member['currency'], number_format($team_member['raised']), ' / ', $team_member['currency'], number_format($team_member['target']), '</div></div>';
			}
			for ($i = 1; $i <= $groupcount; $i++)
			{	echo '</div>';
			}
			echo '</div></div>';
		}

		if ($mobilebuttons)
		{	echo '<div class="mobile-view-team">', implode('', $mobilebuttons), '</div>';
		}
		echo '</div><div class="clear"></div>
			<div class="campaign-page-share-buttons">
				<div class="custom-share-buttons">
					Share ', strip_tags($fulltitle), ' on
					<div class="ssk-group ssk-sm" data-url="', $this->campaign->Link(), '" data-title="Support ', strip_tags($fulltitle), '">

						<a href="" class="ssk ssk-icon ssk-facebook"></a>
						<a href="" class="ssk ssk-icon ssk-twitter"></a>
					</div>
				</div>
			</div>
		</div></div>
		<div class="campaign-page-mobile-buttons-container">
			<div class="campaign-page-mobile-buttons-share">
				<div class="campaign-page-mobile-buttons-share-title">Share this campaign</div>
					<div class="campaign-page-mobile-buttons-share-buttons ssk-group ssk-sm ssk-round" data-url="', $this->campaign->Link(), '" data-title="Support ', $safetitle = $this->InputSafeString(strip_tags($fulltitle)), '"><a href="" class="ssk ssk-facebook"></a>
					<a href="" class="ssk ssk-twitter"></a>
					<a href="whatsapp://send?text=', $safetitle, ' ', $this->campaign->Link(), '" class="ssk ssk-whatsapp"></a>
					<a href="" class="ssk ssk-email"></a>
				
				</div>';
			echo'</div>';
			echo '<div class="campaign-page-mobile-buttons">';

			echo '<div class="campaign-page-mobile-button-left">';
			if ($this->campaign->details['enabled'])
			{	if ($this->camp_customer->id && $this->camp_customer->id == $this->campaign->details['cuid']) {
					echo '<a href="', $this->campaign->EditLink(), '" class="campaign-page-mobile-button-donate">Edit</a>';
				}else{
					echo '<a href="', $this->campaign->DonateLink(), '" class="campaign-page-mobile-button-donate">Donate</a>';
				}
			} else
			{
				if ($this->camp_customer->id && $this->camp_customer->id == $this->campaign->details['cuid'])
				{
					echo '<a href="', $this->campaign->EditLink(), '" class="campaign-page-mobile-button-donate">Edit</a>';
				}else{
					echo '<a href="', $this->campaign->DonateLink(), '" class="campaign-page-mobile-button-donate">Donate</a>';
				}
			}

			echo '</div>';
			echo '<div class="campaign-page-mobile-button-right">';
				echo '<a href="#campaign-page-mobile-buttons-share" class="campaign-page-mobile-button-share">Share</a>';
			echo '</div>';

			echo '<div class="clear"></div></div>';
		echo '</div>';

	} // end of fn MemberBody

	private function GetTeamMembers()
	{	$team_members = array();
		if ($this->campaign->team_members)
		{	foreach ($this->campaign->team_members as $team_member_row)
			{	if ($team_member_row['visible'])
				{	$member = new Campaign($team_member_row);
					if (!$thumbnail = $member->GetImageSRC('thumb'))
					{	$thumbnail = $member->GetAvatarSRC('thumb');
					}
					$team_members[$member->id] = array('title'=>$member->FullTitle(), 'link'=>$member->Link(), 'currency'=>$this->GetCurrency($member->details['currency'], 'cursymbol'), 'target'=>$member->details['target'], 'raised'=>$member->GetDonationTotal(), 'thumbnail'=>$thumbnail);
				}
			}
		}
		return $team_members;
	} // end of fn GetTeamMembers

} // end of class CRStarsCampaignViewPage
?>