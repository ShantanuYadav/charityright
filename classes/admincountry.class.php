<?php
class AdminCountry extends Country
{	
	function __construct($ctrycode = 0)
	{	parent::__construct($ctrycode);
	} // fn __construct
	
	function CanDelete()
	{	if ($this->code)
		{	
			//return true;
		}
		return false;
	} // end of fn CanDelete
	
	function Delete()
	{	if ($this->CanDelete())
		{	if ($result = $this->db->Query('DELETE FROM countries WHERE ccode="' . $this->code . '"'))
			{	if ($this->db->AffectedRows())
				{	$this->RecordAdminAction(array('tablename'=>'countries', 'tableid'=>$this->code, 'area'=>'countries', 'action'=>'deleted'));
					$this->Reset();
					return true;
				}
			}
		}
		return false;
	} // end of fn Delete

	function Save()
	{	$fail = array();
		$success = array();
		$fields = array();
		$admin_actions = array();
		
		if (!$this->code)
		{	$ccode = (int)$_POST['ccode'];
			if ($ccode && ($ccode == $_POST['ccode']))
			{	$ccode = str_pad($ccode, 3, 0, STR_PAD_LEFT);
				$fields[] = 'ccode="' . $ccode . '"';
				// check for duplicate
				if ($result = $this->db->Query('SELECT shortname FROM countries WHERE ccode="' . $ccode . '"'))
				{	if ($row = $this->db->FetchArray($result))
					{	$fail[] = 'code ' . $ccode . ' already used for <a href="ctryedit.php?ctry=' . $ccode . '">' . $row['shortname'] . '</a>';
					}
				}
			} else
			{	$fail[] = 'new code not numeric';
			}
		}
		
		$shortname = $this->SQLSafe($_POST['shortname']);
		if ($shortname)
		{	$fields[] = 'shortname="' . $shortname . '"';
		} else
		{	$fail[] = 'display name missing';
		}
		
		$longname = $this->SQLSafe($_POST['longname']);
		$fields[] = 'longname="'  . $longname . '"';
		
		if ($_POST['postslug'])
		{	$fields[] = 'postslug="'  . $this->TextToSlug($_POST['postslug']) . '"';
		} else
		{	$fields[] = 'postslug=""';
		}
		
		if ($shortcode = strtoupper($_POST['shortcode']))
		{	if (preg_match('|^[A-Z]{2}$|', $shortcode))
			{	// check for duplicates
				$ok = true;
				if ($result = $this->db->Query('SELECT ccode, shortname FROM countries WHERE shortcode="' . $shortcode . '" AND NOT ccode="' . $this->code . '"'))
				{	if ($row = $this->db->FetchArray($result))
					{	$fail[] = 'code ' . $shortcode . ' already used for <a href="ctryedit.php?ctry=' . $row['ccode'] . '">' . $row['shortname'] . '</a>';
						$ok = false;
					}
				}
				if ($ok)
				{	$fields[] = 'shortcode="' . $shortcode . '"';
				}
			} else
			{	$fail[] = 'short code must be 2 characters A-Z';
			}
		} else
		{	$fail[] = 'short code missing';
		}
		
		
		if ($longcode = strtoupper($_POST['longcode']))
		{	if (preg_match('|^[A-Z]{3}$|', $longcode))
			{	// check for duplicates
				$ok = true;
				if ($result = $this->db->Query('SELECT ccode, shortname FROM countries WHERE longcode="' . $longcode . '" AND NOT ccode="' . $this->code . '"'))
				{	if ($row = $this->db->FetchArray($result))
					{	$fail[] = 'long code ' . $longcode . ' already used for <a href="ctryedit.php?ctry=' . $row['ccode'] . '">' . $row['shortname'] . '</a>';
						$ok = false;
					}
				}
				if ($ok)
				{	$fields[] = 'longcode="' . $longcode . '"';
				}
			} else
			{	$fail[] = 'long code must be 3 characters A-Z';
			}
		}
		
		$toplist = (int)$_POST['toplist'];
		$fields[] = 'toplist=' . $toplist;
		if ($this->code && ($toplist != $this->details['toplist']))
		{	$admin_actions[] = array('action'=>'Top listing', 'actionfrom'=>$this->details['toplist'], 'actionto'=>$toplist);
		}
		
		if ($this->code || !$fail)
		{	$set = implode(', ', $fields);
			if ($this->code)
			{	$sql = 'UPDATE countries SET ' . $set . ' WHERE ccode="' . $this->code . '"';
			} else
			{	$sql = 'INSERT INTO countries SET ' . $set;
			}
			if ($this->db->Query($sql))
			{	if ($this->db->AffectedRows())
				{	if ($this->code)
					{	$success[] = 'Changes saved';
					} else
					{	$this->code = $ccode;
						$success[] = 'New country created';
					}
					$this->Get($this->code);
				
				} else
				{	if (!$this->code)
					{	$fail[] = 'Insert failed';
					}
				}
			} //else echo "<p>", $this->db->Error(), "</p>";
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn Save
	
	function InputForm()
	{	
		$form = new Form($_SERVER['SCRIPT_NAME'] . '?ctry=' . $this->code, 'crewcvForm');
		if (!$this->code)
		{	$form->AddTextInput('Numeric code', 'ccode', '', '', 3, 1);
		}
		if ($_POST['redirect'])
		{	$form->AddHiddenInput('redirect', $_POST['redirect']);
		} else
		{	$form->AddHiddenInput('redirect', urlencode($_SERVER['HTTP_REFERER']));
		}
		$form->AddTextInput('Short name', 'shortname', $this->InputSafeString($this->details['shortname']), 'long', 30, 1);
		$form->AddTextInput('Full name', 'longname', $this->InputSafeString($this->details['longname']), 'long', 60);
		$form->AddTextInput('Slug for posts (if blank can\'t be used for posts)', 'postslug', $this->InputSafeString($this->details['postslug']), 'long', 30, 1);
		$form->AddTextInput('Short code (2)', 'shortcode', $this->InputSafeString($this->details['shortcode']), 'short', 2);
		$form->AddTextInput('Long code (3)', 'longcode', $this->InputSafeString($this->details['longcode']), 'short', 3);
		$form->AddSelectWithGroups('Currency', 'currency', $this->details['currency'], '', $this->CurrencyList(), 1, 0);
	//	$form->AddSelect('Continent', 'continent', $this->details['continent'], '', $this->Continents(), true);
		$form->AddTextInput('List order (0 = not listed)', 'toplist', (int)$this->details['toplist'], 'number', 2);
		$form->AddSubmitButton("", $this->code ? 'Save Changes' : 'Create New Country', 'submit');
		if ($this->code && $this->CanDelete())
		{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?ctry=', $this->code, '&delete=1', 
					$_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 
					'delete this country</a></p>';
		}
		$form->Output();
	} // end of fn InputForm
	
	function Continents()
	{	$continents = array();
		if ($result = $this->db->Query('SELECT * FROM continents ORDER BY dispname'))
		{	while ($row = $this->db->FetchArray($result))
			{	$continents[$row['continent']] = $row['dispname'];
			}
		}
		return $continents;
	} // end of fn Continents
	
	public function PaymentGatewaysContainer()
	{	ob_start();
		echo '<div id="ctryPaymentGatewayContainer">', $this->PaymentGatewayTable(), '</div><script type="text/javascript"> $().ready(function(){$("body").append($(".jqmWindow")); $("#ctrygateway_modal_popup").jqm();});</script><div id="ctrygateway_modal_popup" class="jqmWindow"><a href="#" class="jqmClose">Close</a><div id="ctrygwModalInner"></div></div>';
		return ob_get_clean();
	} // end of fn PaymentGatewaysContainer
	
	public function PaymentGatewayTable()
	{	ob_start();
		if ($gateways = $this->GetPaymentGateways())
		{	echo '<form method="post" action="ctrygateways.php?ctry=', $this->code, '">';
		}
		echo '<table><tr class="newlink"><th colspan="3"><a onclick="CtryGatewayPopUp(\'', $this->code, '\');">add another gateway</a></th></tr><tr></tr';
		if ($gateways)
		{	echo '<tr><th>gateway</th><th class="num">Display Order</th><th>Actions</th></tr>';
			foreach ($gateways as $gateway)
			{	echo '<tr><td>', $this->InputSafeString($gateway['gateway']), '</td><td class="num"><input type="number" style="float: right;" class="number" name="gateway_order[', $gateway['pgid'], ']" value="', (int)$gateway['listorder'], '" /></td><td><a onclick="CtryGatewayRemove(\'', $this->code, '\', ', $gateway['pgid'], ');">remove</a></td></tr>';
			}
		} else
		{	echo '<tr><td style="text-align: center;" colspan="3"><h3>No gateways defined  - NO PAYMENTS CAN BE MADE HERE</h3></td></tr>';
		}
		echo $gateways ? '<tr><td colspan="2" style="text-align: right;"><input style="float: right;" type="submit" class="submit" value="Save Changes" /></td><td></td></tr>' : '', '</table>', $gateways ? '</form>' : '';
		return ob_get_clean();
	} // end of fn PaymentGatewayTable
	
	public function AddPaymentGateway($pgid = 0)
	{	$already = $this->GetPaymentGateways();
		if (!$already[$pgid] && ($pgid = (int)$pgid))
		{	$order = 0;
			foreach ($already as $gateway)
			{	if ($gateway['listorder'] > $order)
				{	$order = $gateway['listorder'];
				}
			}
			echo $sql = 'INSERT INTO countrygateways SET pgid=' . $pgid . ', ccode="' . $this->code . '", listorder=' . ($order + 10);
			$this->db->Query($sql);
		}
	} // end of fn AddPaymentGateway
	
	public function RemovePaymentGateway($pgid = 0)
	{	$already = $this->GetPaymentGateways();
		if ($already[$pgid] && ($pgid = (int)$pgid))
		{	$sql = 'DELETE FROM countrygateways WHERE pgid=' . $pgid . ' AND ccode="' . $this->code . '"';
			$this->db->Query($sql);
		}
	} // end of fn RemovePaymentGateway
	
	public function GetAllPaymentGateways()
	{	$gateways = array();
		$sql = 'SELECT * FROM paymentgateways WHERE live=1 ORDER BY gateway';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$gateways[$row['pgid']] = $row;
			}
		}
		return $gateways;
	} // end of fn GetAllPaymentGateways

	public function GetPaymentGateways($ccode = '', $getall = false)
	{	return parent::GetPaymentGateways($this->code, true);
	} // end of fn GetPaymentGateways
	
	public function PaymentGatewaysString($sep = ', ')
	{	$text = array();
		foreach ($this->GetPaymentGateways() as $gateway)
		{	$text[] = $this->InputSafeString($gateway['gateway']);
		}
		return implode($sep, $text);
	} // end of fn PaymentGatewaysString
	
	private function GatewaysInputTable()
	{	ob_start();
		$allgateways = $this->GetAllPaymentGateways();
		echo '<div class="clear"></div><table style="width: 500px;"><tr><th>Payment Gateways</th><th>Used</th><th>List order</th></tr>';
		foreach ($this->GetPaymentGateways() as $pgid=>$gateway)
		{	unset($allgateways[$pgid]);
			echo $this->GatewaysInputTableRow($gateway);
		}
		foreach ($allgateways as $pgid=>$gateway)
		{	echo $this->GatewaysInputTableRow($gateway);
		}
		echo '</table>';
		return ob_get_clean();
	} // end of fn FunctionName
	
	public function GatewaysInputTableRow($gateway = array())
	{	ob_start();
		echo '<tr><td>';
		if ($this->CanAdminUser('technical'))
		{	echo '<a href="gateway.php?id=', $this->InputSafeString($gateway['pgid']), '">';
		}
		echo $this->InputSafeString($gateway['gateway']), $this->CanAdminUser('technical') ? '</a>' : '', '</td><td><input type="checkbox" name="gateway_used[', $gateway['pgid'], ']" value="1"', isset($gateway['listorder']) ? ' checked="checked"' : '', ' /></td><td><input type="txet" class="number" name="gateway_order[', $gateway['pgid'], ']" value="', isset($gateway['listorder']) ? (int)$gateway['listorder'] : '', '" /></td></tr>';
		return ob_get_clean();
	} // end of fn GatewaysInputTableRow
		
} // end of defn AdminCountry
?>