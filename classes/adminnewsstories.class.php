<?php
class AdminNewsStories extends NewsStories
{	var $stories = array();

	function __construct($liveonly = false)
	{	parent::__construct($liveonly);
	} // fn __construct
	
	function AssignStory($newsid)
	{	return new AdminNewsStory($newsid);
	} // end of fn AssignStory
	
} // end if class defn AdminNewsStories
?>