<?php
class AdminDonationsPage extends AccountsMenuPage
{	protected $donation;
	protected $menuarea;

//	function __construct()
//	{	parent::__construct('ACCOUNTS');
//	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->AdminDonationsLoggedInConstruct();
	} // end of fn AccountsLoggedInConstruct
	
	protected function AdminDonationsLoggedInConstruct()
	{	$this->css['admindonations.css'] = 'admindonations.css';
		$this->AssignDonation();
		$this->breadcrumbs->AddCrumb('donations.php', 'Donations');
		$this->DonationConstructFunctions();
		if ($this->donation->id)
		{	$this->breadcrumbs->AddCrumb('donation.php?id=' . $this->donation->id, $this->donation->AdminTitle());
		}
	} // end of fn AdminDonationsLoggedInConstruct
	
	protected function DonationConstructFunctions()
	{	
	} // end of fn DonationConstructFunctions
	
	protected function AssignDonation()
	{	$this->donation = new AdminDonation($_GET['id']);
	} // end of fn AssignDonation
	
	protected function AdminDonationsBody()
	{	if ($this->donation->id)
		{	if ($menu = $this->GetDonationMenu())
			{	echo '<div class="adminItemSubMenu"><ul>';
				foreach ($menu as $menuarea=>$menuitem)
				{	echo '<li', $menuarea == $this->menuarea ? ' class="selected"' : '', '><a href="', $menuitem['link'], '">', $this->InputSafeString($menuitem['text']), '</a></li>';
				}
				echo '</ul><div class="clear"></div></div>';
			}
		}
	} // end of fn AdminDonationsBody
	
	protected function GetDonationMenu()
	{	$menu = array();
		$menu['view'] = array('text'=>'View', 'link'=>'donation.php?id=' . $this->donation->id);
		if ($this->donation->payments || $this->donation->CanBeCollectedMonthly())
		{	$menu['payments'] = array('text'=>'Payments', 'link'=>'donationpayments.php?id=' . $this->donation->id);
		}
		if ($orderid = (int)$this->donation->details['orderid'])
		{	$menu['order'] = array('text'=>'Cart Order', 'link'=>'cartorder.php?id=' . $orderid);
		} else
		{	if ($this->donation->IsMonthlyUnconfirmed())
			{	$menu['ddconfirm'] = array('text'=>'Confirm Direct Debit', 'link'=>'donation_ddconfirm.php?id=' . $this->donation->id);
			}
		}
		return $menu;
	} // end of fn GetDonationMenu
	
	function AdminBodyMain()
	{	$this->AdminDonationsBody();
	} // end of fn AdminBodyMain
	
} // end of defn AdminEventsPage
?>