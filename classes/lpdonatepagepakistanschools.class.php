<?php
class LPDonatePagePakistanSchools extends DonatePage
{	protected $googleAdsCode = '995002415';
	protected $fbPixelCode = '391990840985330';
	protected $googleTagMCode = 'PX5XJXX';
	
	public function __construct()
	{	parent::__construct($dummy, 1);
	} // end of fn __construct
	
	public function Donation()
	{	return $this->donation;
	} // end of fn Donation

	public function DonationTypeContainer($donation_type = '', $currency = '')
	{	//return '';
		ob_start();
		echo '<div class="form-group"><label for="donation_type">I would like to make a donation</label>';
		if (!$currency)
		{	$currency = $_SESSION[$this->cart_session_name]['currency'];
		}
		if (!$donation_type)
		{	$donation_type = $_SESSION[$this->SessionName()]['donation_type'];
		}
		if ($dtypes = $this->donation->GetDonationTypes())
		{	if ($currency && ($cart_cur = $this->GetCurrency($currency)))
			{	//$this->VarDump($cart_cur);
				foreach ($dtypes as $dtype=>$dtype_details)
				{	if (!$cart_cur['use_' . $dtype])
					{	unset($dtypes[$dtype]);
					}
				}
			}
			if($dtypes)
			{	
				echo '<select class="form-control" id="donation-type" name="donation_type" onchange="DonateTypeChangeNew();">';
				foreach ($dtypes as $dtype=>$ddetails)
				{	if (!$donation_type)
					{	$donation_type = $dtype;
					}
					echo '<option value="', $dtype, '" data-donation-type="', $dtype, '" ', ($dtype == $donation_type) ? 'selected="selected"' : '', '>', $this->InputSafeString($ddetails['label']), '</option>';
				}
				echo '</select>';
			}
		}
		//echo 'donation';
		echo '</div>';
		return ob_get_clean();
	} // end of fn DonationTypeContainer

	public function DonationCountriesListNew($type = '', &$country_chosen = '')
	{	ob_start();
		if ($countries = $this->donation->GetDonationCountries($type, true))
		{	echo '<div class="form-group"><label for="donation_country">Which causes would you like to support?</label>
					<select id="donation-country" class="form-control donation-country" name="donation_country" onchange="DonateCountryChangeNew();">';
			foreach ($countries as $country=>$cdetails)
			{	if (!$country_chosen)
				{	$country_chosen = $country;
				}
				echo '<option value="', $this->InputSafeString($country), '"', ($country == $country_chosen) ? ' selected="selected"' : '', '>', $this->InputSafeString($cdetails['shortname']), '</option>';
			}
			echo '</select></div>';
		}
		return ob_get_clean();
	} // end of fn DonationCountriesListNew

	public function DonationProjectsListNew($type = '', $country = '', $donation_project = '')
	{	ob_start();
		$countries = $this->donation->GetDonationCountries($type, true);
		if ($projects = $countries[$country]['projects'])
		{	echo '<div class="form-group"><label for="donation_project">Which area would you like to donate to?</label>
					<select id="donation-project" class="donation-project form-control" name="donation_project" onchange="DonateProjectChangeNew();">';
				foreach ($projects as $project=>$details)
				{	echo '<option value="', $project, '"', ($project == $donation_project) ? ' selected="selected"' : '', '>', $this->InputSafeString($details['projectname']), '</option>';
				}
				echo '</select></div>';
		}
		return ob_get_clean();
	} // end of fn DonationProjectsListNew

	public function DonationProjects2ListNew($type = '', $country = '', $donation_project = '', $donation_project2 = '')
	{	ob_start();
		$countries = $this->donation->GetDonationCountries($type, true);
		if ($projects = $countries[$country]['projects'][$donation_project]['projects'])
		{	echo '<div class="form-group"><label for="donation_project2">', $this->InputSafeString($countries[$country]['projects'][$donation_project]['formlabel']), '</label>
					<select id="donation-project2" class="donation-project2 form-control" name="donation_project2" onchange="DonateProject2Change();">';
			foreach ($projects as $project=>$details)
			{
				echo '<option value="', $project, '"', ($project == $donation_project2) ? ' selected="selected"' : '', '>', $this->InputSafeString($details['projectname']), '</option>';
			}
			echo '</select></div>';
		} //else echo 'no projects found';
		return ob_get_clean();
	} // end of fn DonationProjects2ListNew

	public function DonationCurrencyChooserContainer($donation_type = '', $currency_selected = '')
	{	return $this->CurrencyChooserNew($donation_type, $currency_selected);
	} // end of fn DonationCurrencyChooserContainer

	protected function CurrencyChooserNew($donation_type = '', $currency_selected = '')
	{	ob_start();
		echo '<div class="form-group"><label for="donation_currency">Which currency would you like to donate?</label>';
		if ($currencies = $this->GetCurrencies($donation_type))
		{	
			if ($_SESSION[$this->cart_session_name]['currency'] && $currencies[$_SESSION[$this->cart_session_name]['currency']])
			{	
				echo '<select id="donation_currency" name="donation_currency" onchange="DonationCurrencyChangeNew();" class="form-control"><option value="', $this->InputSafeString($_SESSION[$this->cart_session_name]['currency']), '" selected="selected">', $currencies[$_SESSION[$this->cart_session_name]['currency']]['cursymbol'], ' - ', $this->InputSafeString($currencies[$_SESSION[$this->cart_session_name]['currency']]['curname']), '</option></select>';
			} else
			{	if (!$currencies[$currency_selected])
				{	$currency_selected = false;
				}
				echo '<select id="donation_currency" name="donation_currency" onchange="DonationCurrencyChangeNew();" class="form-control">';
				foreach ($currencies as $curcode=>$currency)
				{	if (!$currency_selected)
					{	$currency_selected = $curcode;
					}
					echo '<option value="', $curcode, '"', $curcode == $currency_selected ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
				}
				echo '</select>';
			}
		}
		echo '</div>';
		return ob_get_clean();
	} // end of fn CurrencyChooserNew

	public function DonationAmountContainer($type = '', $amount = 0, $country = '', $project = '', $project2 = '', $quantity = 0, $was_fixed = false, $currency = 'GBP')
	{	ob_start();
		if ($countries = $this->donation->GetDonationCountries($type, true))
		{	if ($this->IsFixedPrice($countries, $type, $country, $project, $project2, $currency))
			{	if (!$quantity = (int)$quantity)
				{	$quantity = 1;
				}
				$cursymbol = $this->GetCurrency($currency, 'cursymbol');
				echo '<input type="hidden" id="donation-amount-fixed" name="donation_amountfixed" value="', number_format($amount / $quantity, 2, '.', ''), '" /><input type="hidden" id="donation-amount" name="donation_amount" value="', number_format($amount, 2, '.', ''), '" /><input type="hidden" id="donation-quantity" class="donation-quantity" name="donation_quantity" value="', $quantity, '" />';
				echo '<ul class="list-inline">
							<li class="list-inline-item">', $cursymbol, '<span class="dqcAmountPer">', number_format($amount / $quantity, 2), '</span> &times;</li>
							<li class="list-inline-item counter"><input class="form-control counter__input" type="text" value="', $quantity, '" name="donation_quantity" size="5" readonly="readonly" /><a class="counter__increment" onclick="DonateQuantityIncrement(1);" style="cursor: pointer;">+</a><a class="counter__decrement" onclick="DonateQuantityIncrement(-1);" style="cursor: pointer;">&ndash;</a></li>
							<li class="list-inline-item">= ', $cursymbol, '<span class="donation_amount_display">', number_format($amount, 2), '</span></li>
						</ul>';
			} else
			{	if ($was_fixed)
				{	$amount = 0;
				}
				echo '<input type="text" id="donation-amount" name="donation_amount" class="form-control donation-amount number inputSelectOnFocus" value="', number_format($amount, 2, '.', ''), '" onchange="DonateAmountChangeNew();" style="text-align: right;" required />';
			}
		}
		return ob_get_clean();
	} // end of fn DonationAmountContainer

	public function HeaderTrackingCode()
	{	ob_start();
		echo '
	<!-- Global site tag (gtag.js) - Google Ads: ', $this->googleAdsCode, ' -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-', $this->googleAdsCode, '"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag(\'js\', new Date());
		gtag(\'config\', \'AW-', $this->googleAdsCode, '\');
	</script>

	<!-- Event snippet for Sign-up Conversions_Bayyinahtv.com conversion page -->
	<script>
		gtag(\'event\', \'conversion\', {\'send_to\': \'AW-', $this->googleAdsCode, '/u6gdCPKIhJ0BEK-QutoD\'});
	</script>

	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,\'script\',
		\'https://connect.facebook.net/en_US/fbevents.js\');
		fbq(\'init\', \'', $this->fbPixelCode, '\');
		fbq(\'track\', \'PageView\');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=', $this->fbPixelCode, '&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
		new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
		\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,\'script\',\'dataLayer\',\'GTM-', $this->googleTagMCode, '\');
	</script>
';
		return ob_get_clean();
	} // end of fn HeaderTrackingCode

	public function BodyTopTrackingCode()
	{	ob_start();
		echo '
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-', $this->googleTagMCode, '" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
';
		return ob_get_clean();
	} // end of fn BodyTopTrackingCode
	
} // end of class LPDonatePage
?>