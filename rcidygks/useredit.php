<?php
include_once('sitedef.php');

class UserEditPage extends AdminUsersPage
{
	function AdminUsersLoggedInConstruct()
	{	parent::AdminUsersLoggedInConstruct();
		$this->menuarea = 'edit';
		if (isset($_POST['ausername']))
		{	$saved = $this->edituser->Save($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		
		if ($this->edituser->userid && ($this->edituser->userid != $this->user->userid) && $_GET['del'] && $_GET['confirm'])
		{	if ($this->edituser->Delete())
			{	header('location: userlist.php');
				exit;
			} else
			{	$this->failmessage = 'can\'t delete';
			}
		}
		if (!$this->edituser->userid)
		{	$this->breadcrumbs->AddCrumb('useredit.php', 'New User');
		}
	} // end of fn AdminUsersLoggedInConstruct
	
	function AdminUsersBody()
	{	parent::AdminUsersBody();
		$this->edituser->InputForm($this->user);
	} // end of fn AdminUsersBody
	
} // end of defn UserEditPage

$page = new UserEditPage();
$page->Page();
?>
