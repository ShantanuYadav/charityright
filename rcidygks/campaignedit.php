<?php
include_once('sitedef.php');

class CampaignPage extends AdminCampaignsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'edit';
		if (!$this->campaign->AdminCanUpdate())
		{	header('location: campaign.php?id=' . $this->campaign->id);
			exit;
		}
		$this->js['tiny_mce/jquery.tinymce.js'] = 'tiny_mce/jquery.tinymce.js';
		$this->js['crstars_tiny_mce.js'] = 'crstars_tiny_mce.js';
		$this->js['admin_crstars.js'] = 'admin_crstars.js';
		if (isset($_POST['campname']))
		{	$saved = $this->campaign->Save($_POST, $_FILES['campimage'], $_GET['cuid']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->campaign->Delete())
			{	header('location: campaigns.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		if (!$this->campaign->id)
		{	$this->breadcrumbs->AddCrumb('campaign.php', 'New campaign');
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->campaign->InputForm($_GET['cuid']);
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignPage

$page = new CampaignPage();
$page->Page();
?>