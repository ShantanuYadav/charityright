<?php
include_once('sitedef.php');

class EventsPage extends AdminEventsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminEventsBody()
	{	$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="5"><a href="event.php">Add new event</a></th></tr><tr><th></th><th>Event</th><th>Link</th><th>Coming up</th><th>Actions</th></tr>';
		if ($events = $this->GetEvents())
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($events as $event_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$event = new AdminEvent($event_row);
					echo '<tr class="stripe', $i++ % 2,  '"><td>';
					if ($image_url = $event->GetImageSRC('thumb'))
					{	echo '<img src="', $image_url, '" />';
					}
					echo '</td><td>', $this->InputSafeString($event->details['eventname']), '</td><td>', $event->Link(), '</td><td class="num">', count($event->GetDates(array('live'=>true))), '</td><td><a href="event.php?id=', $event->id, '">edit</a>';
					if ($event->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="event.php?id=', $event->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($events) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($events), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
	} // end of fn AdminEventsBody
	
	private function GetEvents()
	{	$tables = array('events'=>'events');
		$fields = array('events.*');
		$where = array();
		$orderby = array('events.eventname ASC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'eid');
	} // end of fn GetEvents
	
} // end of defn EventsPage

$page = new EventsPage();
$page->Page();
?>