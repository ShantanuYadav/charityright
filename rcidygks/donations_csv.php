<?php
include_once('sitedef.php');

class DonationsListCSV extends AdminDonationsPage
{
	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		$csv = new CSVReport();
		$csv->AddToHeaders(array('id', 'gateway ref.', 'status', 'donation date', 'for', 'donation cause', 'donation area', 'donation sub project', 'donation quantity', 'donor name', 'donor address', 'donor city/town', 'donor postcode', 'donor country', 'donor email', 'donor phone', 'donation type', 'currency', 'amount', 'admin amount included', 'paid', 'paid GBP', 
				//'admin included in paid', 
				'admin included in paid GBP', 'giftaid', 'zakat', 'utm codes'));
		
		$admindonations = new AdminDonations();
		if ($donations = $admindonations->GetMainDonations($_GET))
		{	$countries = $this->donation->GetDonationCountries();

			foreach ($donations as $donation_row)
			{	$donation = new AdminDonation($donation_row);
				$paid = $donation->SumPaid();
				$address = array();
				$utm_lines = array();
				if ($donation->details['donoradd1'])
				{	$address[] = $this->CSVSafeString($donation->details['donoradd1']);
				}
				if ($donation->details['donoradd2'])
				{	$address[] = $this->CSVSafeString($donation->details['donoradd2']);
				}
				if ($donation->extrafields)
				{	foreach($donation->extrafields as $key=>$value)
					{	if (substr($key, 0, 4) == 'utm_')
						{	$utm_lines[] = $key . '=' . $value;
						}
					}
				}
				$row = array('"' . $donation->ID() . '"', '"' . $this->CSVSafeString($donation->details['donationref']) . '"', 
						'"' . $donation->Status() . '"', 
						'"' . date('d/m/Y @H:i', strtotime($donation->details['created'])) . '"', 
						'"' . $this->CSVSafeString($donation->GatewayTitleInner()). '"', 
						'"' . $this->CSVSafeString($countries[$donation->details['donationcountry']]['shortname']). '"', 
						'"' . $this->CSVSafeString($countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projectname']). '"', 
						'"' . $this->CSVSafeString($countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projects'][$donation->details['donationproject2']]['projectname']). '"', 
						(int)$donation->details['quantity'],
						'"' . $this->CSVSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname'])) . '"', 
						'"' . implode(', ', $address) . '"', 
						'"' . $this->CSVSafeString($donation->details['donorcity']) . '"', 
						'"' . $this->CSVSafeString($donation->details['donorpostcode']) . '"', 
						'"' . $this->CSVSafeString($this->GetCountry($donation->details['donorcountry'])) . '"', 
						'"' . $this->CSVSafeString($donation->details['donoremail']) . '"', 
						'"' . $this->CSVSafeString($donation->details['donorphone']) . '"', 
						'"' . $donation->details['donationtype'] . '"', 
						'"' . $donation->details['currency'] . '"', 
						number_format($donation->details['amount'] + $donation->details['adminamount'], 2, '.', ''), 
						number_format($donation->details['adminamount'], 2, '.', ''), 
						number_format($paid['amount'], 2, '.', ''), 
						number_format($paid['gbpamount'] + $paid['gbpadminamount'], 2, '.', ''), 
						//number_format($paid['adminamount'], 2, '.', ''), 
						number_format($paid['gbpadminamount'], 2, '.', ''), 
						$donation->details['giftaid'] ? '1' : '0', 
						$donation->details['zakat'] ? '1' : '0',
						'"' . implode("\n", $utm_lines) . '"');
				$csv->AddToRows($row);
			}
		}
		$csv->Output();
	} // end of fn AdminDonationsLoggedInConstruct
	
} // end of defn DonationsListCSV

$page = new DonationsListCSV();
?>