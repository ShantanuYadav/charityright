<?php
include_once('sitedef.php');

class PageSectionsPage extends AdminPagesPage
{	protected $menuarea = 'sections';

	function AdminPagesLoggedInConstruct()
	{	parent::AdminPagesLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('pagesections.php', 'Sections');
	} // end of fn AdminPagesLoggedInConstruct

	function AdminPagesBody()
	{	parent::AdminPagesBody();
		echo $this->editpage->SectionsTable();
	} // end of fn AdminPagesBody
	
} // end of defn PageSectionsPage

$page = new PageSectionsPage();
$page->Page();
?>