<?php
include_once('sitedef.php');

class BookOrderPage extends AccountsMenuPage
{	var $order = '';

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->css[] = 'admincartorder.css';
		$this->js[] = 'admin_bookings.js';
		$this->breadcrumbs->AddCrumb('bookorders.php', 'Event booking orders');
		$this->order = new AdminBookOrder($_GET['id']);
		
/*		if ($_GET['cancel'])
		{	if ($this->order->MarkCancelled(true))
			{	$this->successmessage = 'Order has been cancelled';
			}
		}
		if ($_GET['restore'])
		{	if ($this->order->MarkCancelled(false))
			{	$this->successmessage = 'Order has been restored';
			}
		}*/
		
		$this->breadcrumbs->AddCrumb('bookorder.php?id=' . $this->order->id, 'Order No. ' . $this->order->id);
			
	} // end of fn AccountsLoggedInConstruct

	function AccountsBody()
	{	$this->order->DisplayOrder();
	} // end of fn AccountsBody
	
} // end of defn BookOrderPage

$page = new BookOrderPage();
$page->Page();
?>