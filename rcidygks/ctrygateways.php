<?php
include_once('sitedef.php');

class CountryGatewaysPage extends CountriesPage
{
	
	public function CountriesLoggedInConstruct()
	{	parent::CountriesLoggedInConstruct();
		$this->js[] = 'adminctry.js';
		$this->menuarea = 'gateways';
		$this->breadcrumbs->AddCrumb('ctrygateways.php?ctry=' . $this->country->code, 'Payment gateways');
	} // end of fn CountriesLoggedInConstruct
	
	function CountryConstructFunctions()
	{	
		if (isset($_POST['gateway_order']) && is_array($_POST['gateway_order']))
		{	$saved = $this->country->SaveGateways($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}

	} // end of fn CountryConstructFunctions
	
	function CountriesBodyMain()
	{	parent::CountriesBodyMain();
		echo $this->country->PaymentGatewaysContainer();
	} // end of fn CountriesBodyMain
	
} // end of defn CountryGatewaysPage

$page = new CountryGatewaysPage();
$page->Page();
?>