<?php
include_once('sitedef.php');

class FEMenuEditPage extends AdminFrontEndMenuPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function CMSLoggedInConstruct($page_option = '')
	{	parent::CMSLoggedInConstruct('items');
		$this->js[] = 'admin_femenus.js';
		$this->breadcrumbs->AddCrumb('femenuitems.php?id=' . $this->femenu->id, 'Menu items');
	} //  end of fn __construct

	function ConstructFunctions()
	{	parent::ConstructFunctions();
		
		if ($_POST['menuorder'])
		{	if ($saved = $this->femenu->SaveMenuItems($_POST))
			{	$this->successmessage = 'Item details saved';
			}
		}
	} // end of fn ConstructFunctions
	
	function FEMenuBodyContent()
	{	return $this->femenu->MenuItemsContainer();
	} // end of fn FEMenuBodyContent
	
} // end of defn FEMenuEditPage

$page = new FEMenuEditPage();
$page->Page();
?>
