<?php
include_once('sitedef.php');

class DonationProjectsPage extends AdminDonationOptionsPage
{	
	
	protected function AdminDonationOptionsLoggedInConstruct()
	{	parent::AdminDonationOptionsLoggedInConstruct();
		$this->menuarea = 'projects';
		$this->breadcrumbs->AddCrumb('donationprojects.php?id=' . $this->donationcountry->id, 'Sub-options');
	} // end of fn AdminDonationOptionsLoggedInConstruct

	protected function AdminDonationOptionsBody()
	{	parent::AdminDonationOptionsBody();
		echo '<table id="pagelist"><tr class="newlink"><th colspan="7"><a href="donationproject.php?dcid=', $this->donationcountry->id, '">new sub-option for "', $this->InputSafeString($this->donationcountry->details['shortname']), '"</a></th></tr><tr><th>Display name</th><th>Projects</th><th>Status</th><th class="num">Donations</th><th class="num">Campaigns</th><th class="num">List order</th><th>Actions</th></tr>';
		if ($projects = $this->donationcountry->projects)
		{	foreach($projects as $project_row)
			{	$project = new AdminDonationProject($project_row);
				$status = array();
				if ($project->details['oneoff'])
				{	$status[] = 'Used for one-off donations';
				}
				if ($project->details['monthly'])
				{	$status[] = 'Used for monthly (DD) donations';
				}
				if ($project->details['crstars'])
				{	$status[] = 'Used for CR Stars campaigns';
				}
				if (!$project->details['oneoff'] && !$project->details['monthly'] && !$project->details['crstars'])
				{	$status[] = 'Not used for anything - NOT LIVE';
				}
				if ($project->prices)
				{	$status[] = 'Fixed price option';
				}
				echo '<tr><td>', $this->InputSafeString($project->details['projectname']), '</td><td>';
				if ($pcount = count($project->projects))
				{	echo $pcount, ' (', count($project->LiveProjects()), ' live)';
				} else
				{	echo 'None';
				}
				echo '</td><td>', implode('<br />', $status), '</td><td class="num">', count($project->GetDonations()), '</td><td class="num">', count($project->GetCampaigns()), '</td><td class="num">', (int)$project->details['listorder'], '</td><td><a href="donationproject.php?id=', $project->id, '">edit</a>';
				if ($project->CanDelete())
				{	echo '&nbsp;|&nbsp;<a href="donationproject.php?id=', $project->id, '&delete=1">delete</a>';
				}
				echo '</td></tr>';
			}
		}
		echo '</table>';
	} // end of fn AdminDonationOptionsBody
	
} // end of defn DonationProjectsPage

$page = new DonationProjectsPage();
$page->Page();
?>