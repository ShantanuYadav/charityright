<?php
include_once('sitedef.php');

class PostImagesPage extends AdminPeoplePage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PeopleConstructFunctions()
	{	$this->menuarea = 'edit';
		if (isset($_POST['pname']))
		{	$saved = $this->person->Save($_POST, $_FILES['imagefile']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->person->Delete())
			{	header('location: people.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn PeopleConstructFunctions
	
	protected function AdminPeopleLoggedInConstruct()
	{	parent::AdminPeopleLoggedInConstruct();
		if (!$this->person->id)
		{	$this->breadcrumbs->AddCrumb('person.php', 'New person');
		}
	} // end of fn AdminPeopleLoggedInConstruct
	
	protected function AdminPeopleBody()
	{	parent::AdminPeopleBody();
		echo $this->person->InputForm();
	} // end of fn AdminPeopleBody
	
} // end of defn PostImagesPage

$page = new PostImagesPage();
$page->Page();
?>