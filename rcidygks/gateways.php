<?php
include_once('sitedef.php');

class AdminGatewaysListPage extends AdminGatewayPage
{	
	
	function AdminGatewayBody()
	{	
		echo '<table><tr class="newlink"><th colspan="5"><a href="gateway.php">add new payment gateway</a></th></tr><tr><th>Gateway</th><th>Button function</th><th>How many countries</th><th>Status</th><th>Actions</th></tr>';
		foreach ($this->GetGateways() as $gateway_row)
		{	$gateway = new PaymentGateway($gateway_row);
			$status = array();
			if ($gateway->details['live'])
			{	$status[] = 'Available for adding';
			} else
			{	$status[] = 'Can\'t be added';
			}
			if ($gateway->details['suspended'])
			{	$status[] = 'Suspended in front-end';
			}
			echo '<td>', $this->InputSafeString($gateway->details['gateway']), '</td><td>', $this->InputSafeString($gateway->details['buttonfunction']), '</td><td>', count($gateway->CountriesUsedIn()), '</td><td>', implode('<br />', $status), '</td><td><a href="gateway.php?id=', $gateway->id, '">edit</a>';
			if ($gateway->CanDelete())
			{	echo '&nbsp;|&nbsp;<a href="gateway.php?id=', $gateway->id, '&delete=1">delete</a>';
			}
			echo '</td></tr>';
		}
		echo '</table>';
	} // end of fn AdminGatewayBody
	
	public function GetGateways()
	{	$gateways = array();
		$sql = 'SELECT * FROM paymentgateways ORDER BY gateway';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$gateways[] = $row;
			}
		}
		return $gateways;
	} // end of fn GetGateways
	
} // end of defn AdminGatewaysListPage

$page = new AdminGatewaysListPage();
$page->Page();
?>