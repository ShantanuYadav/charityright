<?php
include_once('sitedef.php');

class EventVenuePage extends AdminEventVenuesPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function EventConstructFunctions()
	{	$this->js[] = 'tiny_mce/jquery.tinymce.js';
		$this->js[] = 'pageedit_tiny_mce.js';
		$this->menuarea = 'edit';
		if (isset($_POST['venuename']))
		{	$saved = $this->venue->Save($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->venue->Delete())
			{	header('location: events.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn EventConstructFunctions
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		if (!$this->venue->id)
		{	$this->breadcrumbs->AddCrumb('eventvenue.php', 'New venue');
		}
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function AdminEventsBody()
	{	parent::AdminEventsBody();
		echo $this->venue->InputForm();
	} // end of fn AdminEventsBody
	
} // end of defn EventVenuePage

$page = new EventVenuePage();
$page->Page();
?>