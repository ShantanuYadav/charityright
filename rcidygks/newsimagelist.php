<?php
include_once('sitedef.php');

class NewsImagePage extends CMSPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function CMSLoggedInConstruct()
	{	parent::CMSLoggedInConstruct();
		$this->css[] = 'adminnews.css';
	} // end of fn CMSLoggedInConstruct
	
	function Header(){}
	function DisplayBreadcrumbs(){}
	function AdminMenu(){}
	
	function CMSBodyMain()
	{	$newsimages = new NewsImages();
		if (is_array($newsimages) && $newsimages)
		{	foreach ($newsimages->images as $image)
			{	echo '<div class="viewimage"><p>use this url:<br /><b>', $image->ImageLink(), '</b></p><img width="150px" src="', $image->ImageLink(), '" /><p>actual size: ', $image->SizeString(), '</p></div>';
			}
		} else
		{	echo '<h3>No images available</h3>';
		}
	} // end of fn CMSBodyMain
	
} // end of defn NewsImagePage

$page = new NewsImagePage();
$page->Page();
?>