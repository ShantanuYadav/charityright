<?php
include_once('sitedef.php');

class PostImagesPage extends AdminPostsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'images';
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		if ($this->post->id)
		{	$this->breadcrumbs->AddCrumb('postimages.php', 'Images');
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->post->ImagesTable();
	} // end of fn AdminPostsBody
	
} // end of defn PostImagesPage

$page = new PostImagesPage();
$page->Page();
?>