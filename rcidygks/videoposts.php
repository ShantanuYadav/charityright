<?php
include_once('sitedef.php');

class VideoPostsPage extends AdminVideosPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function VideoConstructFunctions()
	{	
		$this->menuarea = 'posts';
	} // end of fn VideoConstructFunctions
	
	protected function AdminVideoLoggedInConstruct()
	{	parent::AdminVideoLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('videoposts.php?id=' . $this->id, 'Posts');
	} // end of fn AdminVideoLoggedInConstruct
	
	protected function AdminVideosBody()
	{	parent::AdminVideosBody();
		echo $this->video->PostsTable();
	} // end of fn AdminVideosBody
	
} // end of defn VideoPostsPage

$page = new VideoPostsPage();
$page->Page();
?>