<?php
include_once('sitedef.php');

class FEMenusPage extends CMSPage
{
	function __construct()
	{	parent::__construct('CMS');
	} //  end of fn __construct

	function CMSLoggedInConstruct($page_option = '')
	{	parent::CMSLoggedInConstruct();
		$this->css[] = 'adminpages.css';
		$this->breadcrumbs->AddCrumb('femenus.php', 'Front end menus');
	} // end of fn CMSLoggedInConstruct

	function CMSBodyMain()
	{	$this->ListMenus();
	} // end of fn CMSBodyMain
	
	function ListMenus()
	{	
		echo '<div><table><tr class="newlink"><th colspan="4"><a href="femenuedit.php">Add new menu</a></th></tr><tr><th>Name</th><th>No. of items</th><th>Assigned to</th><th>Actions</th></tr>';
		if ($menus = $this->db->ResultsArrayFromSQL('SELECT * FROM femenus ORDER BY menuname, menuid', 'menuid', true))
		{	foreach ($menus as $menu_row)
			{	$menu = new AdminFrontEndMenu($menu_row);
				echo '<tr class="stripe', $i++ % 2, '"><td>', $this->InputSafeString($menu->details['menuname']), '</td><td>', count($menu->GetAllMenuItems()), '</td><td>', $menu->details['menuassigned'] ? $menu->AssignedMenu('malabel') : '-- not assigned --', '</td><td><a href="femenuedit.php?id=', $menu->id, '">edit</a>';
				if ($menu->CanDelete())
				{	echo '&nbsp;|&nbsp;<a href="femenuedit.php?id=', $menu->id, '&delete=1">delete</a>';
				}
				echo '</td></tr>';
			}
		}
		echo '</table></div>';
	} // end of fn ListMenus
	
} // end of defn FEMenusPage

$page = new FEMenusPage();
$page->Page();
?>
