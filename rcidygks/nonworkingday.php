<?php
include_once('sitedef.php');

class NonWorkingDayPage extends AccountsMenuPage
{	private $nwd;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		$this->nwd = new NonWorkingDay($_GET['id']);
		
		if (isset($_POST['ynwdate']))
		{	$saved = $this->nwd->Save($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->nwd->Delete())
			{	header('location: nonworkingdays.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
		
		$this->breadcrumbs->AddCrumb('nonworkingdays.php', 'Non-working days');
		$this->breadcrumbs->AddCrumb('nonworkingday.php?id=' . $this->nwd->id, $this->nwd->id ? (($this->nwd->details['description'] ? $this->InputSafeString($this->nwd->details['description'] . ' - ') : '') . date('j-M-Y', strtotime($this->nwd->details['nwdate']))) : 'Adding New Day');
	} // end of fn AccountsLoggedInConstruct
	
	function AccountsBody()
	{	$this->nwd->InputForm();
	} // end of fn AccountsBody
	
} // end of defn NonWorkingDayPage

$page = new NonWorkingDayPage();
$page->Page();
?>