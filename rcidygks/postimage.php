<?php
include_once('sitedef.php');

class PostImagesPage extends AdminPostsPage
{	private $image;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'images';
		if (isset($_POST['imagedesc']))
		{	$saved = $this->image->Save($_POST, $_FILES['imagefile'], $this->post->id);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->image->Delete())
			{	header('location: postimages.php?id=' . $this->post->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('postimages.php', 'Images');
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AssignPost()
	{	$this->image = new AdminPostImage($_GET['id']);
		$this->post = new AdminPost($this->image->id ? $this->image->details['postid'] : $_GET['postid']);
	} // end of fn AssignPost
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->image->InputForm($this->post->id);
	} // end of fn AdminPostsBody
	
} // end of defn PostImagesPage

$page = new PostImagesPage();
$page->Page();
?>