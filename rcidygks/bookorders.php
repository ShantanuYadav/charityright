<?php
include_once('sitedef.php');

class CartOrdersListPage extends AccountsMenuPage
{	var $orders = '';
	var $startdate = '';
	var $enddate = '';
	var $showunpaid = 0;
	var $notdownloaded = 1;
	var $pubid = 0;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->css[] = 'admincartorder.css';
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->showunpaid = $_GET['showunpaid'];
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
			
		$this->breadcrumbs->AddCrumb('bookorders.php', 'Event booking orders');
	} // end of fn AccountsLoggedInConstruct

	function AccountsBody()
	{	echo $this->FilterForm(), $this->ListOrders();
	} // end of fn AccountsBody

	private function ListOrders()
	{	if ($orders = $this->GetOrders())
		{	$perpage = 20;
			echo '<table><tr><th class="num">Order number</th><th>Payment ref.</th><th>Order date</th><th>Ordered by</th><th>Event</th><th>Tickets</th><th class="num">Full price</th><th>Actions</th></tr>';
			if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			$tickets = array();
			$cursymbol = array();
			$can_events = $this->user->CanUserAccess('events');
		
			foreach ($orders as $order_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$order = new AdminBookOrder($order_row);
					if (!$cursymbol[$order->details['currency']])
					{	$cursymbol[$order->details['currency']] = $this->GetCurrency($order->details['currency'], 'cursymbol');
					}
					if (!$tickets[$order->details['currency']])
					{	$cursymbol[$order->details['currency']] = $this->GetCurrency($order->details['currency'], 'cursymbol');
					}
					$rowcount = count($order->items);
					$itemrows = array();
					echo '<tr class="stripe', $i++ % 2,  '"><td class="num" rowspan="', $rowcount, '">', (int)$order->id, '</td><td rowspan="', $rowcount, '">', $this->InputSafeString($order->details['payref']), '</td><td rowspan="', $rowcount, '">', date('d/m/y @H:i', strtotime($order->details['orderdate'])), '</td><td rowspan="', $rowcount, '">', $this->InputSafeString($order->details['firstname'] . ' ' . $order->details['lastname']), ' (', $order->details['email'], ')</td>';
					foreach ($order->items as $orderitem)
					{	ob_start();
						if (!$tickets[$orderitem['ttid']])
						{	$tickets[$orderitem['ttid']] = new AdminTicketType($orderitem['ttid']);
						}
						echo '<td>';
						if ($can_events)
						{	echo '<a href="eventdate.php?id=', $tickets[$orderitem['ttid']]->eventdate['edid'], '">';
						}
						echo $this->InputSafeString($tickets[$orderitem['ttid']]->event['eventname']), ', ', $this->InputSafeString($tickets[$orderitem['ttid']]->venue['venuename']), ', ', date('d/m/y', strtotime($tickets[$orderitem['ttid']]->eventdate['starttime'])), $can_events ? '</a>' : '', '</td><td>', $this->InputSafeString($tickets[$orderitem['ttid']]->details['ttypename']), ' &times; ', (int)$orderitem['quantity'];
						if ($orderitem['price'] > 0)
						{	echo ' @ ', $cursymbol[$order->details['currency']], number_format($orderitem['price'], 2);
						}
						echo '</td>';
						$itemrows[] = ob_get_clean();
					}
					//$this->VarDump($itemrows);
					echo array_shift($itemrows), '<td class="num" rowspan="', $rowcount, '">';
					if (($order_price = $order->TotalPrice()) > 0)
					{	echo $cursymbol[$order->details['currency']], number_format($order_price, 2);
					} else
					{	echo '---';
					}
					echo '</td><td rowspan="', $rowcount, '"><a href="bookorder.php?id=', $order->id, '">view</a></td></tr>';
					foreach($itemrows as $itemrow)
					{	echo '<tr>', $itemrow, '</tr>';
					}
				}
			}
			
			echo '</table>';
			if (count($orders) > $perpage)
			{	$pagelink = $_SERVER['SCRIPT_NAME'];
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
					if ($get)
					{	$pagelink .= '?' . implode('&', $get);
					}
				}
				$pag = new Pagination($_GET['page'], count($orders), $perpage, $pagelink, array(), 'page');
				echo '<div class="pagination">', $pag->Display(), '</div>';
			}			
				
		} else
		{	echo '<h4>No orders for this selection</h4>';
		}
	} // end of fn ListOrders

	private function GetOrders()
	{	$tables = array('bookorders'=>'bookorders');
		$fields = array('bookorders.*');
		$where = array();
		
		if (!$_GET['showunpaid'])
		{	$where['live'] = 'NOT bookorders.payref=""';
		}
		
		if ($this->startdate)
		{	$where['startdate'] = 'bookorders.orderdate>="' . $this->startdate . ' 00:00:00"';
		}
		if ($this->enddate)
		{	$where['enddate'] = 'bookorders.orderdate<="' . $this->enddate . ' 23:59:59"';
		}
		
		$orderby = array('bookorders.orderdate DESC');
		//echo $this->db->BuildSQL($tables, $fields, $where, $orderby);
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'orderid', true);
	} // end of fn GetOrders
	
	function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<span>show unpaid</span><input type="checkbox" name="showunpaid" value="1"', $this->showunpaid ? ' checked="checked"' : '', ' /><input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn CartOrdersListPage

$page = new CartOrdersListPage();
$page->Page();
?>