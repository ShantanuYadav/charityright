<?php
include_once('sitedef.php');

class PageSubSectionsPage extends AdminPagesPage
{	protected $menuarea = 'subsection';

	function AdminPagesLoggedInConstruct()
	{	parent::AdminPagesLoggedInConstruct();
		$this->js['tinymce'] = 'tiny_mce/jquery.tinymce.js';
		$this->js['pagesection_tiny_mce'] = 'pagesection_tiny_mce.js';
		$this->js['admin_pagesections'] = 'admin_pagesections.js';
		$this->css['jPicker'] = 'jPicker-1.1.6.css';
		$this->js['jPicker'] = 'jpicker-1.1.6.min.js';
		$this->breadcrumbs->AddCrumb('pagesections.php?id=' . $this->editpage->id, 'Sections');
		$this->breadcrumbs->AddCrumb('pagesection.php?id=' . $this->pagesection->id, '#' . $this->pagesection->id);
		$this->breadcrumbs->AddCrumb('pagesubsections.php?id=' . $this->pagesection->id, 'Subsections');
		if ($this->pagesection->id)
		{	$this->breadcrumbs->AddCrumb('pagesubsection.php?id=' . $this->pagesubsection->id, '#' . $this->pagesection->id);
		} else
		{	$this->breadcrumbs->AddCrumb('pagesubsection.php?psid=' . $this->pagesection->id, 'New subsection');
		}
	} // end of fn AdminPagesLoggedInConstruct
	
	protected function GetPageMenu()
	{	$menu = parent::GetPageMenu();
		if (!$this->pagesubsection->id)
		{	$menu['subsection'] = array('text'=>'New Subsection', 'link'=>'pagesubsection.php?psid=' . $this->pagesection->id);
		}
		return $menu;
	} // end of fn GetPageMenu
	
	function AdminPageConstructFunctions()
	{	parent::AdminPageConstructFunctions();
		
		if (isset($_POST['pstext']))
		{	$saved = $this->pagesubsection->Save($_POST, $this->editpage->id, $this->pagesection->id, $_FILES['imagefile']);
			if ($saved['failmessage'])
			{	$this->failmessage = $saved['failmessage'];
			}
			if ($saved['successmessage'])
			{	$this->successmessage = $saved['successmessage'];
			}
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->pagesubsection->Delete())
			{	header('location: pagesubsections.php?id=' . $this->pagesection->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn AdminPageConstructFunctions
	
	protected function AssignPage()
	{	$this->pagesubsection = new AdminPageSection($_GET['id']);
		if ($this->pagesubsection->id)
		{	$this->pagesection = new AdminPageSection($this->pagesubsection->details['parentsection']);
		} else
		{	$this->pagesection = new AdminPageSection($_GET['psid']);
		}
		
		$this->editpage = new AdminPageContent($this->pagesection->details['pageid']);
	} // end of fn AssignPage

	function AdminPagesBody()
	{	parent::AdminPagesBody();
		echo $this->pagesubsection->InputForm($this->editpage->id, $this->pagesection->id);
	} // end of fn AdminPagesBody
	
} // end of defn PageSubSectionsPage

$page = new PageSubSectionsPage();
$page->Page();
?>