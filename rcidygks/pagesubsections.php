<?php
include_once('sitedef.php');

class PageSubSectionsPage extends AdminPagesPage
{	protected $menuarea = 'subsections';

	function AdminPagesLoggedInConstruct()
	{	parent::AdminPagesLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('pagesections.php?id=' . $this->editpage->id, 'Sections');
		$this->breadcrumbs->AddCrumb('pagesection.php?id=' . $this->pagesection->id, '#' . $this->pagesection->id);
		$this->breadcrumbs->AddCrumb('pagesubsections.php?id=' . $this->pagesection->id, 'Subsections');
	} // end of fn AdminPagesLoggedInConstruct
	
	protected function AssignPage()
	{	$this->pagesection = new AdminPageSection($_GET['id']);
		$this->editpage = new AdminPageContent($this->pagesection->details['pageid']);
	} // end of fn AssignPage

	function AdminPagesBody()
	{	parent::AdminPagesBody();
		echo $this->pagesection->SectionsTable();
	} // end of fn AdminPagesBody
	
} // end of defn PageSubSectionsPage

$page = new PageSubSectionsPage();
$page->Page();
?>