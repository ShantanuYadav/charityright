<?php
include_once('sitedef.php');

class CampaignTextPage extends AdminCampaignTextPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'edit';
		if (isset($_POST['ctname']))
		{	$saved = $this->campaigntext->Save($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->campaigntext->Delete())
			{	header('location: campaigntextlist.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		if (!$this->campaigntext->id)
		{	$this->breadcrumbs->AddCrumb('campaigntext.php', 'New sample text');
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->campaigntext->InputForm();
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignTextPage

$page = new CampaignTextPage();
$page->Page();
?>