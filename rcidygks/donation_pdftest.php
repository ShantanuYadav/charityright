<?php
include_once('sitedef.php');

class DonationViewPage extends AdminDonationsPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		echo $this->donation->DirectDebitFormHTMLContents();
		exit;
	} // end of fn AdminDonationsLoggedInConstruct
	
} // end of defn DonationViewPage

$page = new DonationViewPage();
?>