<?php
include_once('sitedef.php');

class CartOrderViewPage extends AdminOrdersPage
{
	function AdminOrdersLoggedInConstruct()
	{	parent::AdminOrdersLoggedInConstruct();
		$this->menuarea = 'view';
		
	} // end of fn AdminOrdersLoggedInConstruct

	function AdminOrdersBody()
	{	parent::AdminOrdersBody();
		echo $this->order->AdminOrdersBodyDisplay();
	} // end of fn AdminOrdersBody
	
} // end of defn CartOrderViewPage

$page = new CartOrderViewPage();
$page->Page();
?>