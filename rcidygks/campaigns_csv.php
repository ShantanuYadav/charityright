<?php
include_once('sitedef.php');

class CampaignsCSV extends AdminCampaignsPage
{	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->startdate = $_GET['startdate'];
		$this->enddate = $_GET['enddate'];
		$csv = new CSVReport();
		$this->AddCampaignsHeaderToCSV($csv);
		$this->AddCampaignsRowsToCSV($csv, $this->GetCampaigns());
		$csv->Output();
	} // end of fn AdminCampaignsLoggedInConstruct
	
} // end of defn CampaignsCSV

$page = new CampaignsCSV();
?>