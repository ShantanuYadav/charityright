<?php
include_once('sitedef.php');

class AdminGatewaysListPage extends AdminGatewayPage
{	private $gateway;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function AdminGatewayLoggedInConstruct()
	{	parent::AdminGatewayLoggedInConstruct();
		$this->gateway = new PaymentGateway($_GET['id']);
		//$this->gateway->InitialiseCountryGateways();
		if (isset($_POST['gateway']))
		{	$saved = $this->gateway->Save($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
		
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->gateway->Delete())
			{	$this->successmessage = 'Payment gateway deleted';
			} else
			{	$this->failmessage = 'Delete failed';
			}
		}
		
		$this->breadcrumbs->AddCrumb('gateway.php?id=' . $this->gateway->id, $this->gateway->id ? $this->InputSafeString($this->gateway->details['gateway']) : 'New gateway');
	} // end of fn AdminGatewayLoggedInConstruct
	
	function AdminGatewayBody()
	{	echo $this->gateway->InputForm(), $this->gateway->CountriesList();
	} // end of fn AdminGatewayBody
	
} // end of defn AdminGatewaysListPage

$page = new AdminGatewaysListPage();
$page->Page();
?>