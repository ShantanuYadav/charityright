<?php
include_once('sitedef.php');

class VolunteersCSV extends AdminVolunteersPage
{	
	protected function AdminVolunteersLoggedInConstruct()
	{	parent::AdminVolunteersLoggedInConstruct();
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		$csv = new CSVReport();
		$csv->AddToHeaders(array('Registered', 'Time', 'Name', 'Email', 'Phone', 'Their notes'));
		if ($volunteers = $this->GetVolunteers())
		{	foreach ($volunteers as $volunteer_row)
			{	$volunteer = new AdminVolunteer($volunteer_row);
				$row = array('"' . date('d/m/Y', $stamp = strtotime($volunteer->details['regdate'])) . '"', '"' . date('H:i', $stamp) . '"', '"' . $this->InputSafeString($volunteer->FullName()) . '"', '"' . $this->InputSafeString($volunteer->details['email']) . '"', '"' . $this->InputSafeString($volunteer->details['phone']) . '"', '"' . $this->InputSafeString($volunteer->details['volnotes']) . '"');
				$csv->AddToRows($row);
			}
		}
		$csv->Output();
	} // end of fn AdminVolunteersLoggedInConstruct
	
} // end of defn VolunteersCSV

$page = new VolunteersCSV();
?>