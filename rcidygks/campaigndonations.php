<?php
include_once('sitedef.php');

class CampaignDonationsPage extends AdminCampaignsPage
{	var $startdate = '';
	var $enddate = '';
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'donations';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		$this->breadcrumbs->AddCrumb('campaigndonations.php?id=' . $this->campaign->id, 'Donations');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		if ($this->user->CanUserAccess('accounts'))
		{	echo $this->DonationsFilterForm(), $this->DonationsList($donations = $this->GetDonations());
			if ($donations)
			{	$get = array();
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
				}
				$get[] = 'startdate=' . $this->startdate;
				$get[] = 'enddate=' . $this->enddate;
				echo '<p class="adminCSVDownload"><a href="campaigndonations_csv.php?', implode('&', $get) , '">Download CSV (all pages)</a></p>';
			}
		}
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignDonationsPage

$page = new CampaignDonationsPage();
$page->Page();
?>