<?php
include_once('sitedef.php');

class PageListPage extends AdminPagesPage
{	var $pages = '';

	function AdminPagesLoggedInConstruct()
	{	parent::AdminPagesLoggedInConstruct();
		$this->pages = new AdminPageContents($this->user->CanUserAccess('administration'));
	} // end of fn AdminPagesLoggedInConstruct

	function AdminPagesBody()
	{	parent::AdminPagesBody();
		$this->pages->PageList();
	} // end of fn AdminPagesBody
	
} // end of defn PageListPage

$page = new PageListPage();
$page->Page();
?>