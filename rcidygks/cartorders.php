<?php
include_once('sitedef.php');

class CartOrdersListPage extends AdminOrdersPage
{	var $startdate = '';
	var $enddate = '';
	private $adminorders;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AdminOrdersLoggedInConstruct()
	{	parent::AdminOrdersLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->adminorders = new AdminCartOrders();
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-7 days'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
	} // end of fn AdminOrdersLoggedInConstruct

	function AdminOrdersBody()
	{	echo $this->FilterForm(), $this->ListCartOrders();
	} // end of fn AdminOrdersBody

	private function ListCartOrders()
	{	ob_start();
		$filter = $_GET;
		$filter['startdate'] = $this->startdate;
		$filter['enddate'] = $this->enddate;
		if ($orders = $this->adminorders->GetMainOrders($filter))
		{	
			$perpage = 20;
			echo '<table><tr><th>ID</th><th>Order date</th><th>By</th><th class="num">Single total</th><th class="num">Monthly total</th><th>Donations</th><th>Info</th><th>Actions</th></tr>';
			if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			$cursymbol = array();
			$totals = array('gbpamount'=>0, 'gbpadminamount'=>0);

			foreach ($orders as $order_row)
			{	$order = new AdminCartOrder($order_row);
			//	$totals['gbpamount'] += $paid['gbpamount'] + $paid['gbpadminamount'];
			//	$totals['gbpadminamount'] += $paid['gbpadminamount'];
				if (++$count > $start)
				{	if ($count <= $end)
					{	$extras = array();
						if ($order->details['giftaid'])
						{	$extras[] = 'Giftaid';
						}
					
						if (!$cursymbol[$order->details['currency']])
						{	$cursymbol[$order->details['currency']] = $this->GetCurrency($order->details['currency'], 'cursymbol');
						}
						echo '<tr class="stripe', $i++ % 2,  '"><td>', $order->ID(), '</td><td>', date('d/m/y @H:i', strtotime($order->details['orderdate'])), '</td><td>', $this->InputSafeString(trim($order->details['donortitle'] . ' ' . $order->details['donorfirstname'] . ' ' . $order->details['donorsurname'])), '</td><td class="num">', ($order->details['total_oneoff'] > 0) ? ($cursymbol[$order->details['currency']] . number_format($order->details['total_oneoff'], 2)) : '', '</td><td class="num">', ($order->details['total_monthly'] > 0) ? ($cursymbol[$order->details['currency']] . number_format($order->details['total_monthly'], 2)) : '', '</td><td class="cartListDonationList"><ul>';
						foreach ($order->MainDonations() as $donation_row)
						{	$donation = New AdminDonation($donation_row);
							echo '<li><a href="donation.php?id=', $donation->id, '">', $donation->ID(), '</a> ', $donation->details['donationtype'], ' ', $cursymbol[$order->details['currency']], number_format($donation->details['amount'] + $donation->details['adminamount'], 2), ' - ', $donation->Status(), '</li>';
						}
						foreach ($order->CRStarsDonations() as $donation_row)
						{	$donation = New AdminCampaignDonation($donation_row);
							echo '<li><a href="campaigndonation.php?id=', $donation->id, '">', $donation->ID(), '</a> CR Stars ', $cursymbol[$order->details['currency']], number_format($donation->details['amount'], 2), ' - ', $donation->Status(), '</li>';
						}
						echo '</ul></td><td>', implode('<br />', $extras), '</td><td><a href="cartorder.php?id=', $order->id, '">view</a></td></tr>';
					}
				}
			}
			$get = array();
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
			}
			$csv_get = $get;
			$csv_get[] = 'startdate=' . $this->startdate;
			$csv_get[] = 'enddate=' . $this->enddate;

			echo /*'<tr><th colspan="10">Totals (for whole selection)</th><th class="num">&pound;', number_format($totals['gbpamount'], 2), '</th>',
					//'<th></th>',
					'<th class="num">&pound;', number_format($totals['gbpadminamount'], 2), '</th><th colspan="2"></th></tr>',*/
					'</table>';//'<p class="adminCSVDownload"><a href="donations_csv.php?', implode('&', $csv_get) , '">Download CSV (for whole selection)</a></p>';
			if (count($orders) > $perpage)
			{	$pagelink = $_SERVER['SCRIPT_NAME'];
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
					if ($get)
					{	$pagelink .= '?' . implode('&', $get);
					}
				}
				$pag = new Pagination($_GET['page'], count($orders), $perpage, $pagelink, array(), 'page');
				echo '<div class="pagination">', $pag->Display(), '</div>';
			}			
				
		} else
		{	echo '<h4>No orders for this selection</h4>';
		}
		return ob_get_clean();
	} // end of fn ListCartOrders
	
	function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<span>paid only?</span><input type="checkbox" name="paidonly" value="1" ', $_GET['paidonly'] ? 'checked="checked" ' : '', '/><input type="submit" class="submit" value="Get" /><div class="clear"></div><select name="country"><option value="">-- all countries --</option>';
		foreach ($this->CountryList() as $ctry=>$ctryname)
		{	echo '<option value="', $ctry, '"', $_GET['country'] == $ctry ? ' selected="selected"' : '', '>', $this->inputSafeString($ctryname), '</option>';
		}
		echo '</select><span>How they heard</span><select name="howheard"><option value="">-- any or none --</option>';
		foreach ($this->HowHeardOptions() as $key=>$value)
		{	echo '<option value="', $key, '"', $_GET['howheard'] == $key ? ' selected="selected"' : '', '>', $this->inputSafeString($value), '</option>';
		}
		echo '</select><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm

	function HowHeardOptions()
	{	$options = array();
		$sql = 'SELECT * FROM howfoundus ORDER BY hforder, hfid';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$options[$row['hfvalue']] = $row['hfdesc'];
			}
		}
		$options['other'] = 'Other';
		return $options;
	} // end of fn HowHeardOptions
	
} // end of defn CartOrdersListPage

$page = new CartOrdersListPage();
$page->Page();
?>