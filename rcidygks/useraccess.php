<?php
include_once('sitedef.php');

class UserAccessAreasPage extends AdminUsersPage
{
	function AdminUsersLoggedInConstruct()
	{	parent::AdminUsersLoggedInConstruct();
		$this->menuarea = 'access';
		if (isset($_POST['savinguser']))
		{	$saved = $this->edituser->SaveAccessAreas($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		$this->breadcrumbs->AddCrumb('useraccess.php', 'Access areas');
	} // end of fn AdminUsersLoggedInConstruct
	
	function AdminUsersBody()
	{	parent::AdminUsersBody();
		$this->edituser->AccessAreasForm($this->user);
	} // end of fn AdminUsersBody
	
} // end of defn UserAccessAreasPage

$page = new UserAccessAreasPage();
$page->Page();
?>