<?php
include_once('sitedef.php');

class CustomersPage extends AccountsMenuPage
{	var $customers = '';

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
			
		$this->breadcrumbs->AddCrumb('customers.php', 'Customers');
		$this->customers = new AdminCustomers($_GET['email']);
	} // end of fn AccountsLoggedInConstruct

	function AccountsBody()
	{	$this->FilterForm();
		$this->customers->ListCustomers();
	} // end of fn AccountsBody
	
	function FilterForm()
	{	echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get">',
				'<span>Email (or part)</span><input type="text" name="email" value="', 
				$this->InputSafeString($_GET['email']), 
				'" /><input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
	} // end of fn FilterForm
	
} // end of defn CustomersPage

$page = new CustomersPage();
$page->Page();
?>