<?php
include_once('sitedef.php');

class WorldpayRerunPage extends AccountsMenuPage
{	private $post = array();

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->css[] = 'adminwp.css';
		
		if ($_POST['wptext'])
		{	$wpear = new AdminWorldPayEar();
			if ($this->post = $wpear->PostFromTextRaw($_POST['wptext']))
			{	$this->successmessage = 'see details of notification below';
			} else
			{	$this->failmessage = 'no posted data found in notification';
			}
		}

		$this->breadcrumbs->AddCrumb('wp_cbdisplay.php', 'Worldpay Display Notification');
		
	} // end of fn AccountsLoggedInConstruct
	
	function AccountsBody()
	{	$form = new Form($_SERVER['SCRIPT_NAME'], 'wpTextArea');
		$form->AddTextArea('Text from worldpay', 'wptext', $this->InputSafeString($_POST['wptext']), '', 0, 0, 3, 80);
		$form->AddSubmitButton('', 'Display in table', 'submit');
		$form->Output();
		
		if ($this->post)
		{	echo '<div class="clear"></div><table class="wpPostDisplay"><tr><th>Key</th><th>Value</th><th>Raw value</th></tr>';
			foreach ($this->post as $key=>$value)
			{	echo '<tr><td>', $this->InputSafeString($key), '</td><td>', $this->InputSafeString(urldecode($value)), '</td><td>', $this->InputSafeString($value), '</td></tr>';
				if (($key == 'cartId') && ($cartid = (int)$value))
				{	$cart = new AdminCartOrder($cartid);
					if (!$cart->id)
					{	unset($cart);
					}
				}
			}
			echo '</table>';
			if ($cart)
			{	
				if ($_POST['process_pmt'])
				{	$wpear = new AdminWorldPayEar();
					$wpear->ProcessPostText($_POST['wptext']);
					if ($wpear->failmessage)
					{	echo '<div class="failmessage">', $this->InputSafeString($wpear->failmessage), '</div>';
					}
					if ($wpear->successmessage)
					{	echo '<div class="successmessage">', $this->InputSafeString($wpear->successmessage), '</div>';
					}
					$cart->Get($cart->id);
				}
				echo $cart->AdminOrdersBodyDisplay();
				
				if ($cart->CanPayOneOffDonations())
				{	if (($this->post['transStatus'] == 'Y') && ($this->post['authMode'] == 'A'))
					{	echo '<h3>Successful payment, not yet processed</h3>';
						$form = new Form($_SERVER['SCRIPT_NAME']);
						$form->AddHiddenInput('wptext', $this->InputSafeString($_POST['wptext']));
						$form->AddHiddenInput('process_pmt', '1');
						$form->AddSubmitButton('', 'Process payment', 'submit');
						$form->Output();
					} else
					{	echo '<h3>Not a successful payment</h3>';
					}
				} else
				{	echo '<h3>Cart order has been paid</h3>';
				}
			}
		}
		
	} // end of fn AccountsBody
	
} // end of defn WorldpayRerunPage

$page = new WorldpayRerunPage();
$page->Page();
?>