<?php
include_once('sitedef.php');

class CampaignTextListPage extends AdminCampaignTextPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminCampaignBody()
	{	$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="4"><a href="campaigntext.php">Create new sample text</a></th></tr><tr><th>Name</th><th>List order</th><th>Text</th><th>Actions</th></tr>';
		if ($campaigntexts = $this->GetCampaignText())
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($campaigntexts as $campaigntext_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$campaigntext = new AdminCampaignText($campaigntext_row);
					echo '<tr class="stripe', $i++ % 2,  '"><td>', $this->InputSafeString($campaigntext->details['ctname']), '</td><td class="num">', (int)$campaigntext->details['listorder'], '</td><td>', nl2br($this->InputSafeString($campaigntext->details['cttext'])), '</td><td><a href="campaigntext.php?id=', $campaigntext->id, '">edit</a>';
					if ($campaigntext->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="campaigntext.php?id=', $campaigntext->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($campaigntexts) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($campaigntexts), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
	} // end of fn AdminCampaignBody
	
	private function GetCampaignText()
	{	$tables = array('campaigntext'=>'campaigntext');
		$fields = array('campaigntext.*');
		$where = array();
		$orderby = array('campaigntext.listorder ASC');
		$groupby = array();
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby, $groupby), 'ctid');
	} // end of fn GetCampaignText
	
} // end of defn CampaignTextListPage

$page = new CampaignTextListPage();
$page->Page();
?>