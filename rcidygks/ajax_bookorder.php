<?php
include_once('sitedef.php');

class BookOrderAjax extends AccountsMenuPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		
		switch ($_GET['action'])
		{	case 'resendconfirm':
				$order = new AdminBookOrder($_GET['id']);
				echo '<p><a onclick="BookingResend(', $order->id, ');">Confirm resending of Tickets</a></p>';
				break;
			case 'resend':
				$order = new AdminBookOrder($_GET['id']);
				if ($order->SendTickets())
				{	echo '<div class="successmessage" style="width: 90%;">Tickets resent</div>';
				} else
				{	echo '<div class="failmessage" style="width: 90%;">Tickets not resent</div>';
				}
				break;
		}
		//$this->order = new AdminBookOrder($_GET['id']);
			
	} // end of fn AccountsLoggedInConstruct
	
} // end of defn BookOrderAjax

$page = new BookOrderAjax();
?>