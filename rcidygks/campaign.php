<?php
include_once('sitedef.php');

class CampaignPage extends AdminCampaignsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'view';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->campaign->ShowDetails();
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignPage

$page = new CampaignPage();
$page->Page();
?>