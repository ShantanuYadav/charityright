<?php
include_once('sitedef.php');

class PageEditPage extends AdminPagesPage
{	protected $menuarea = 'edit';

	function AdminPagesLoggedInConstruct()
	{	parent::AdminPagesLoggedInConstruct();
		$this->js[] = 'tiny_mce/jquery.tinymce.js';
		$this->js[] = 'pageedit_tiny_mce.js';
		
		if (!$this->editpage->id)
		{	$this->breadcrumbs->AddCrumb('pageedit.php', 'New Page');
		}
	} // end of fn AdminPagesLoggedInConstruct

	function AdminPageConstructFunctions()
	{	parent::AdminPageConstructFunctions();
		
		if (isset($_POST['pagetitle']))
		{	$saved = $this->editpage->Save($_POST, $_FILES['imagefile']);
			if ($saved['failmessage'])
			{	$this->failmessage = $saved['failmessage'];
			}
			if ($saved['successmessage'])
			{	$this->successmessage = $saved['successmessage'];
			}
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->editpage->Delete())
			{	header('location: pagelist.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn AdminPageConstructFunctions

	function AdminPagesBody()
	{	parent::AdminPagesBody();
		$this->editpage->InputForm();
	} // end of fn AdminPagesBody
	
} // end of defn PageEditPage

$page = new PageEditPage();
$page->Page();
?>