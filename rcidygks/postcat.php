<?php
include_once('sitedef.php');

class PostCatPage extends AdminPostCatsPage
{	
	protected function PostConstructFunctions()
	{	$this->js[] = 'tiny_mce/jquery.tinymce.js';
		$this->js[] = 'pageedit_tiny_mce.js';
		$this->menuarea = 'catedit';
		$this->category = new AdminPostCategory($_GET['id']);
		if (isset($_POST['catname']))
		{	$saved = $this->category->Save($_POST, $_FILES['imagefile']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->category->Delete())
			{	header('location: postcatslist.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		if (!$this->category->id)
		{	$this->breadcrumbs->AddCrumb('postcat.php', 'New category');
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->category->InputForm();
	} // end of fn AdminPostsBody
	
} // end of defn PostCatPage

$page = new PostCatPage();
$page->Page();
?>