<?php
include_once('sitedef.php');

class VolunteersPage extends AdminVolunteersPage
{	
	protected function AdminVolunteersLoggedInConstruct()
	{	parent::AdminVolunteersLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		
		if (($this->startdate . '00:00:00') < $this->volunteer->earliest)
		{	$this->warningmessage = 'Volunteers not recorded before ' . date('j M y @H:i', strtotime($this->volunteer->earliest));
		}
		
	} // end of fn AdminVolunteersLoggedInConstruct
	
	protected function AdminVolunteersBody()
	{	echo $this->FilterForm(), $this->VolunteersList($volunteers = $this->GetVolunteers(), 20);
		if ($volunteers)
		{	$get = array();
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
			}
			$get[] = 'startdate=' . $this->startdate;
			$get[] = 'enddate=' . $this->enddate;
			echo '<p class="adminCSVDownload"><a href="volunteers_csv.php?', implode('&', $get) , '">Download CSV (for whole selection)</a></p>';
		}
	} // end of fn AdminVolunteersBody
	
	protected function VolunteersList($volunteers = array(), $perpage = 20)
	{	ob_start();
		if ($volunteers)
		{	echo '<table><tr><th>Registered</th><th>Name</th><th>Email</th><th>Phone</th><th>Their notes</th><th>Actions</th></tr>';
			if ($perpage = (int)$perpage)
			{	if ($_GET['page'] > 1)
				{	$start = ($_GET['page'] - 1) * $perpage;
				} else
				{	$start = 0;
				}
				$end = $start + $perpage;
			} else
			{	$start = 0;
			}
			
			foreach ($volunteers as $volunteer_row)
			{	$volunteer = new AdminVolunteer($volunteer_row);
				if (++$count > $start)
				{	if (!$perpage || ($count <= $end))
					{	echo '<tr class="stripe', $i++ % 2,  '"><td>', date('d/m/Y @H:i', strtotime($volunteer->details['regdate'])), '</td><td>', $this->InputSafeString($volunteer->FullName()), '</td><td><a href="mailto:', $email = $this->InputSafeString($volunteer->details['email']), '">', $email, '</a></td><td>', $this->InputSafeString($volunteer->details['phone']), '</td><td>', nl2br($this->InputSafeString($volunteer->details['volnotes'])), '</td><td><a href="volunteer.php?id=', $volunteer->id, '">view</a></td></tr>';
					}
				}
			}
			echo '</table>';
			if ($perpage && (count($volunteers) > $perpage))
			{	$pagelink = $_SERVER['SCRIPT_NAME'];
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
					if ($get)
					{	$pagelink .= '?' . implode('&', $get);
					}
				}
				$pag = new Pagination($_GET['page'], count($volunteers), $perpage, $pagelink, array(), 'page');
				echo '<div class="pagination">', $pag->Display(), '</div>';
			}
		} else
		{	echo '<h3>No volunteers for this selection</h3>';
		}
		return ob_get_clean();
	} // end of fn VolunteersList
	
	protected function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get">';
		echo '<span>Registered from</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn VolunteersPage

$page = new VolunteersPage();
$page->Page();
?>