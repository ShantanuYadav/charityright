<?php
include_once('sitedef.php');

class PostQuickDonatePage extends AdminPostsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'donateform';
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		
		if (isset($_POST['amount']))
		{	$saved = $this->post->AdminQuickDonateSave($_POST);
			if ($saved['failmessage'])
			{	$this->failmessage = $saved['failmessage'];
			}
			if ($saved['successmessage'])
			{	$this->successmessage = $saved['successmessage'];
			}
		}
		
		$this->js['admin_donationlinkbuilder'] = 'admin_donationlinkbuilder.js';
		$this->breadcrumbs->AddCrumb('postdonateform.php', 'Quick donate form');
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->post->AdminQuickDonateForm();
	} // end of fn AdminPostsBody
	
} // end of defn PostQuickDonatePage

$page = new PostQuickDonatePage();
$page->Page();
?>
