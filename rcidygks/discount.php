<?php
include_once('sitedef.php');

class DiscountEditPage extends AccountsMenuPage
{	private $discount;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->discount = new AdminDiscountCode($_GET['id']);
		
		if (isset($_POST['discpercent']))
		{	$saved = $this->discount->Save($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
		
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->discount->Delete())
			{	header('location: discounts.php');
				exit;
			} else
			{	$this->failmessage = 'Delete failed';
			}
		}
		
		$this->breadcrumbs->AddCrumb('discounts.php', 'Discounts');
		$this->breadcrumbs->AddCrumb('discount.php?id=' . $this->discount->id, $this->discount->id ? $this->InputSafeString($this->discount->details['disccode']) : 'Adding New Discount');
	} //  end of fn AccountsLoggedInConstruct
	
	function AccountsBody()
	{	$this->discount->InputForm();
	} // end of fn AccountsBody
	
} // end of defn DiscountEditPage

$page = new DiscountEditPage();
$page->Page();
?>