<?php
include_once('sitedef.php');

class DiscountsPage extends AccountsMenuPage
{	var $deloptions;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('discounts.php', 'Discounts');
	} //  end of fn AccountsLoggedInConstruct
	
	function AccountsBody()
	{	echo '<div><table><tr class="newlink"><th colspan="7"><a href="discount.php">Add new discount</a></th></tr><tr><th>Discount code</th><th>Date restrictions</th><th class="num">Discount applied</th><th>Free fixing kits</th><th class="num">Used by</th><th>Link</th><th>Actions</th></tr>';
		foreach ($this->GetDiscounts() as $discount_row)
		{	$discount = new AdminDiscountCode($discount_row);
			echo '<tr class="stripe', $i++ % 2,  '"><td>', $this->InputSafeString($discount->details['disccode']), '</td><td>', $discount->DatesDescription(), '</td><td class="num">', number_format($discount->details['discpercent'], 2), '%</td><td>', $discount->details['freefixingkit'] ? 'Yes' : '', '</td><td class="num">', $discount->GetUsedCount(true), '</td><td>', $discount->PromoLink(), '</td><td><a href="discount.php?id=', $discount->id, '">edit</a>';
			if ($discount->CanDelete())
			{	echo '&nbsp;|&nbsp;<a href="discount.php?id=', $discount->id, '&delete=1">delete</a>';
			}
			echo '</td></tr>';
		}
		echo '</table></div>';
	} // end of fn AccountsBody
	
	private function GetDiscounts()
	{	$tables = array('discountcodes'=>'discountcodes');
		$fields = array('discountcodes.*');
		$orderby = array('IF (discountcodes.startdate="0000-00-00", discountcodes.enddate, discountcodes.startdate) ASC', 'discountcodes.dcid DESC');
		
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, false, $orderby));
	} // end of fn GetDiscounts
	
} // end of defn DelOptionPage

$page = new DiscountsPage();
$page->Page();
?>