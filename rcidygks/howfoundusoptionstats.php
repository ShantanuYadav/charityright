<?php
include_once('sitedef.php');

class HowFoundUsOptionStatsPage extends AdminHowFoundUsPage
{	var $startdate = '';
	var $enddate = '';
	protected $stats;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function HowFoundUsLoggedInConstructor()
	{	parent::HowFoundUsLoggedInConstructor();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->stats = new HowHeardStats();
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		
		if ($this->startdate > $this->enddate)
		{	$this->startdate = $this->enddate;
		}
		
		if ($this->startdate < $this->stats->startdate)
		{	$this->warningmessage = 'WARNING: "How heard" only collected since ' . date('j M Y', strtotime($this->stats->startdate));
		}
		
		$this->breadcrumbs->AddCrumb('howfoundusoptionstats.php', 'stats');
		
	} // end of fn HowFoundUsLoggedInConstructorv
	
	function HowFoundUsLoggedInBody()
	{	parent::HowFoundUsLoggedInBody();
		echo $this->FilterForm();
		$filter = $_GET ? $_GET : array();
		$filter['startdate'] = $this->startdate;
		$filter['enddate'] = $this->enddate;
		if (($stats = $this->stats->OrdersPerOption($filter)) && $stats['options'])
		{	echo '<table><tr><th>How they heard</th><th class="num">Orders</th><th class="num">Proportion</th></tr>';
			foreach ($stats['options'] as $option)
			{	echo '<tr><td>', $this->InputSafeString($option['desc']), '</td><td class="num">', number_format($option['count']), '</td><td class="num">', number_format(100 * $option['proportion'], 1), '</td></tr>';
			}
			echo '<tr><th>Total</th><th class="num">', number_format($stats['totals']['count']), '</th><th class="num"></th></tr></table>';
		//	$this->VarDump($stats);
		}
	} // end of fn HowFoundUsLoggedInBody
	
	function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<span>paid only?</span><input type="checkbox" name="paidonly" value="1" ', $_GET['paidonly'] ? 'checked="checked" ' : '', '/><input type="submit" class="submit" value="Get" /><div class="clear"></div><select name="country"><option value="">-- all countries --</option>';
		foreach ($this->CountryList() as $ctry=>$ctryname)
		{	echo '<option value="', $ctry, '"', $_GET['country'] == $ctry ? ' selected="selected"' : '', '>', $this->inputSafeString($ctryname), '</option>';
		}
		echo '</select><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn HowFoundUsOptionStatsPage

$page = new HowFoundUsOptionStatsPage();
$page->Page();
?>