<?php
include_once('sitedef.php');

class GiftaidDonationsListPage extends AdminDonationsPage
{	var $startdate = '';
	var $enddate = '';

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->js['admin_giftaid.js'] = 'admin_giftaid.js';
		
		if ($_GET['delid'] && $_GET['deltype'])
		{	$this->RemoveFromGiftaid($_GET['delid'], $_GET['deltype']);
		}
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		$this->breadcrumbs->AddCrumb('giftaiddonations.php', 'Giftaid donations');
	} // end of fn AdminDonationsLoggedInConstruct

	private function RemoveFromGiftaid($delid = 0, $deltype = '')
	{	switch ($deltype)
		{	case 'main':
				$donation = new AdminDonation($delid);
				if ($donation->details['giftaid'])
				{	$delsql = 'UPDATE donations SET giftaid=0 WHERE did=' . $donation->id;
				} else
				{	$this->failmessage = 'Donation to remove not found';
				}
				break;
			case 'crstars':
				$donation = new campaignDonation($delid);
				if ($donation->details['giftaid'])
				{	$delsql = 'UPDATE campaigndonations SET giftaid=0 WHERE cdid=' . $donation->id;
				} else
				{	$this->failmessage = 'Donation to remove not found';
				}
				break;
			default:
				$this->failmessage = 'Donation type to remove not found';
		}
		if ($delsql)
		{	if ($result = $this->db->Query($delsql))
			{	if ($this->db->AffectedRows())
				{	$this->successmessage = 'Donation removed from giftaid';
				}
			}
		}
	} // end of fn RemoveFromGiftaid

	function AdminDonationsBody()
	{	echo $this->FilterForm(), $this->ListDonations();
	} // end of fn AdminDonationsBody

	private function ListDonations()
	{	$admindonations = new AdminDonations();
		$filter = $_GET;
		$filter['startdate'] = $this->startdate;
		$filter['enddate'] = $this->enddate;
		if ($donations = $admindonations->GetGiftaidDonations($filter))
		{	$perpage = 20;
			echo '<table><tr><th class="num">ID</th><th>Payment ref</th><th>Donation date</th><th>By</th><th>Payer info</th><th class="num">Paid</th><th class="num">Paid GBP</th><th>Actions</th></tr>';
			if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			$cursymbol = array();
			$countries = $this->donation->GetDonationCountries();
			$totals = array('gbpamount'=>0);
			$can_campaigns = $this->user->CanUserAccess('campaigns');

			$get = array();
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'delid') && ($key != 'deltype') && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
			}
			$csv_get = $get;
			$csv_get[] = 'startdate=' . $this->startdate;
			$csv_get[] = 'enddate=' . $this->enddate;
			$getstring = implode('&', $get);

			foreach ($donations as $donation_row)
			{	switch ($donation_row['table'])
				{	case 'donations':
						$donation = new AdminDonation($donation_row);
						$paid = $donation->SumPaid();
						$totals['gbpamount'] += $paid['gbpamount'] + $paid['gbpadminamount'];
						if (++$count > $start)
						{	if ($count <= $end)
							{	$name = array($this->InputSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname'])));
								if ($donation->details['donoradd1'])
								{	$name[] = $this->InputSafeString($donation->details['donoradd1']);
								}
								if ($donation->details['donoradd2'])
								{	$name[] = $this->InputSafeString($donation->details['donoradd2']);
								}
								if ($donation->details['donorcity'])
								{	$name[] = $this->InputSafeString($donation->details['donorcity']);
								}
								if ($donation->details['donorpostcode'])
								{	$name[] = $this->InputSafeString($donation->details['donorpostcode']);
								}
								if (!$cursymbol[$donation->details['currency']])
								{	$cursymbol[$donation->details['currency']] = $this->GetCurrency($donation->details['currency'], 'cursymbol');
								}
								$payerinfo = array();
								foreach ($donation->payments as $payment)
								{	if ($payment['payerfirstname'] || $payment['payerlastname'])
									{	$payerinfo[] = $this->InputSafeString(trim($payment['payerfirstname'] . ' ' . $payment['payerlastname']));
									}
									if ($payment['payeremail'])
									{	ob_start();
										echo ($payment['payeremail'] != $donation->details['donoremail']) ? '<span class="redInlineError">' : '', $payment['payeremail'], ($payment['payeremail'] != $donation->details['donoremail']) ? '</span>' : '';
										$payerinfo[] = $this->InputSafeString(ob_get_clean());
									}
									if ($payment['payercountry'] && ($payment['payercountry'] !== 'GB'))
									{	ob_start();
										echo '<span class="redInlineError">', $this->InputSafeString($this->GetCountry($payment['payercountry'])), '</span>';
										$payerinfo[] = ob_get_clean();
									}
									break;
								}
								echo '<tr class="stripe', $i++ % 2,  '"><td>', $donation->ID(), '</td><td>', $this->InputSafeString($donation->details['donationref']), '</td><td>', date('d/m/y @H:i', strtotime($donation->details['created'])), '</td><td>', implode('<br />', $name), '</td><td>', implode('<br />', $payerinfo), '</td><td class="num">', $cursymbol[$donation->details['currency']], number_format($paid['amount'] + $paid['adminamount']), '</td><td class="num">&pound;', number_format($paid['gbpamount'] + $paid['gbpadminamount'], 2), '</td><td><a href="donation.php?id=', $donation->id, '">view</a>&nbsp;<a style="color: #FF0000; font-size: 20px; text-decoration: none;" href="',  $_SERVER['SCRIPT_NAME'], '?', $getstring, $getstring ? '&' : '', 'deltype=main&delid=', $donation->id, '" onclick="return GARemoveDonation();">&times;</a></td></tr>';
							}
						}
						break;
					case 'campaigndonations':
						$donation = new CampaignDonation($donation_row);
						if ($donation->details['donationref'])
						{	$totals['gbpamount'] += $donation->details['gbpamount'];
						}
						if (++$count > $start)
						{	if ($count <= $end)
							{	$name = array($this->InputSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname'])));
								if ($donation->details['donoradd1'])
								{	$name[] = $this->InputSafeString($donation->details['donoradd1']);
								}
								if ($donation->details['donoradd2'])
								{	$name[] = $this->InputSafeString($donation->details['donoradd2']);
								}
								if ($donation->details['donorcity'])
								{	$name[] = $this->InputSafeString($donation->details['donorcity']);
								}
								if ($donation->details['donorpostcode'])
								{	$name[] = $this->InputSafeString($donation->details['donorpostcode']);
								}
								if (!$cursymbol[$donation->details['currency']])
								{	$cursymbol[$donation->details['currency']] = $this->GetCurrency($donation->details['currency'], 'cursymbol');
								}
								$payerinfo = array();
								if ($donation->details['payerfirstname'] || $donation->details['payerlastname'])
								{	$payerinfo[] = $this->InputSafeString(trim($donation->details['payerfirstname'] . ' ' . $donation->details['payerlastname']));
								}
								if ($donation->details['payeremail'])
								{	ob_start();
									echo ($donation->details['payeremail'] != $donation->details['donoremail']) ? '<span class="redInlineError">' : '', $donation->details['payeremail'], ($donation->details['payeremail'] != $donation->details['donoremail']) ? '</span>' : '';
									$payerinfo[] = $this->InputSafeString(ob_get_clean());
								}
								if ($donation->details['payercountry'] && ($donation->details['payercountry'] !== 'GB'))
								{	ob_start();
									echo '<span class="redInlineError">', $this->InputSafeString($this->GetCountry($donation->details['payercountry'])), '</span>';
									$payerinfo[] = ob_get_clean();
								}
								echo '<tr class="stripe', $i++ % 2,  '"><td>', $donation->ID(), '</td><td>', $this->InputSafeString($donation->details['donationref']), '</td><td>', date('d/m/y @H:i', strtotime($donation->details['donated'])), '</td>
									<td>', implode('<br />', $name), '</td><td>', implode('<br />', $payerinfo), '</td>
									<td class="num">', $cursymbol[$donation->details['currency']], number_format($donation->details['amount']), '</td>
									<td class="num">', '&pound;' . number_format($donation->details['gbpamount'], 2), '</td><td>';
								if ($can_campaigns)
								{	echo '<a href="campaigndonation.php?id=', $donation->id, '">view</a>&nbsp;';
								}
								echo '<a style="color: #FF0000; font-size: 20px; text-decoration: none;" href="',  $_SERVER['SCRIPT_NAME'], '?', $getstring, $getstring ? '&' : '', 'deltype=crstars&delid=', $donation->id, '" onclick="return GARemoveDonation();">&times;</a></td></tr>';
							}
						}
						break;
				}
			}

			echo '<tr><th colspan="6">Totals (for whole selection)</th><th class="num">&pound;', number_format($totals['gbpamount'], 2), '</th><th colspan="2"></th></tr></table><p class="adminCSVDownload"><a href="giftdonations_csv.php?', implode('&', $csv_get) , '">Download CSV (for whole selection)</a></p>';
			if (count($donations) > $perpage)
			{	$pagelink = $_SERVER['SCRIPT_NAME'];
				if ($getstring)
				{	$pagelink .= '?' . $getstring;
				}
				$pag = new Pagination($_GET['page'], count($donations), $perpage, $pagelink, array(), 'page');
				echo '<div class="pagination">', $pag->Display(), '</div>';
			}			
				
		} else
		{	echo '<h4>No donations for this selection</h4>';
		}
	} // end of fn ListDonations
	
	function FilterForm()
	{	ob_start();
		class_exists('Form');
		$don = new Donation();
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<input type="submit" class="submit" value="Get" /><div class="clear"><span>Name (partial or full)</span><input type="test" name="donorname" value="', $this->InputSafeString($_GET['donorname']), '" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn GiftaidDonationsListPage

$page = new GiftaidDonationsListPage();
$page->Page();
?>
