<?php
include_once('sitedef.php');

class DonationsListCSV extends AdminDonationsPage
{
	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		$this->GiftaidDonationsCSVOutputHMRC();
	} // end of fn AdminDonationsLoggedInConstruct
	
	function GiftaidDonationsCSVOutput()
	{	$csv = new CSVReport();
		$csv->AddToHeaders(array('id', 'gateway ref.', 'donation date', 'donor name', 'donor address', 'donor town/city', 'donor postcode', 'donor email', 'donor phone', 'currency', 'paid', 'paid GBP'));
		
		$admindonations = new AdminDonations();
		if ($donations = $admindonations->GetGiftaidDonations($_GET))
		{	
			foreach ($donations as $donation_row)
			{	switch ($donation_row['table'])
				{	case 'donations':
						$donation = new AdminDonation($donation_row);
						$paid = $donation->SumPaid();
						$address = array();
						if ($donation->details['donoradd1'])
						{	$address[] = $this->CSVSafeString($donation->details['donoradd1']);
						}
						if ($donation->details['donoradd2'])
						{	$address[] = $this->CSVSafeString($donation->details['donoradd2']);
						}
						$row = array('"' . $donation->ID() . '"', '"' . $this->CSVSafeString($donation->details['donationref']) . '"', 
								'"' . date('d/m/Y', strtotime($donation->details['created'])) . '"', 
								'"' . $this->CSVSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname'])) . '")', 
								'"' . implode(', ', $address) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorcity']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorpostcode']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donoremail']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorphone']) . '"', 
								'"' . $donation->details['currency'] . '"', 
								number_format($paid['amount'] + $paid['adminamount'], 2, '.', ''), 
								number_format($paid['gbpamount'] + $paid['gbpadminamount'], 2, '.', ''));
						$csv->AddToRows($row);
						break;
					case 'campaigndonations':
						$donation = new CampaignDonation($donation_row);
						$address = array();
						if ($donation->details['donoradd1'])
						{	$address[] = $this->CSVSafeString($donation->details['donoradd1']);
						}
						if ($donation->details['donoradd2'])
						{	$address[] = $this->CSVSafeString($donation->details['donoradd2']);
						}
						$row = array('"' . $donation->ID() . '"', '"' . $this->CSVSafeString($donation->details['donationref']) . '"', 
								'"' . date('d/m/Y', strtotime($donation->details['donated'])) . '"', 
								'"' . $this->CSVSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname'])) . '"', 
								'"' . implode(', ', $address) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorcity']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorpostcode']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donoremail']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorphone']) . '"', 
								'"' . $donation->details['currency'] . '"', 
								number_format($donation->details['amount'], 2, '.', ''), 
								number_format($donation->details['gbpamount'], 2, '.', ''));
						$csv->AddToRows($row);
						break;
				}
			}
		}
		$csv->Output();
	} // end of fn GiftaidDonationsCSVOutput
	
	function GiftaidDonationsCSVOutputHMRC()
	{	$csv = new CSVReport();
		$csv->AddToHeaders(array('Title', 'First name or initial', 'Last name', 'House name or number', 'Postcode', 'Aggregated donations', 'Sponsored event', 'Donation date', 'Amount'));
		
		$admindonations = new AdminDonations();
		if ($donations = $admindonations->GetGiftaidDonations($_GET))
		{	
			foreach ($donations as $donation_row)
			{	switch ($donation_row['table'])
				{	case 'donations':
						$donation = new AdminDonation($donation_row);
						$paid = $donation->SumPaid();
						$firstname = explode(' ', $donation->details['donorfirstname']);
						$address1 = explode(' ', $donation->details['donoradd1']);
						$housenumber = $address1[0];
						$housenumber_prefix = '';
						if (strtolower(substr($housenumber, 0, 4)) == 'flat')
						{	$housenumber_prefix = 'Flat ';
							$housenumber = $address1[1];
						}
						if ((int)$housenumber && !preg_match('|^(flat )?[1-9][0-9]*[a-zA-Z]?$|i', $housenumber))
						{	$housenumber = (int)$housenumber;
						}
						$housenumber = $housenumber_prefix . $housenumber;
						$row = array(
								'"' . $this->CSVSafeString($donation->details['donortitle']) . '"', 
								'"' . $this->CSVSafeString($firstname[0]) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorsurname']) . '"', 
								'"' . $this->CSVSafeString($housenumber) . '"', 
								'"' . $this->CSVSafeString($this->PostcodeTidy($donation->details['donorpostcode'])) . '"', 
								'""', 
								'""', 
								'"' . date('d/m/y', strtotime($donation->details['created'])) . '"', 
								number_format($paid['gbpamount'] + $paid['gbpadminamount'], 2, '.', ''));
						$csv->AddToRows($row);
						break;
					case 'campaigndonations':
						$donation = new CampaignDonation($donation_row);
						$firstname = explode(' ', $donation->details['donorfirstname']);
						$address1 = explode(' ', $donation->details['donoradd1']);
						$housenumber = $address1[0];
						$housenumber_prefix = '';
						if (strtolower(substr($housenumber, 0, 4)) == 'flat')
						{	$housenumber_prefix = 'Flat ';
							$housenumber = $address1[1];
						}
						if ((int)$housenumber && !preg_match('|^(flat )?[1-9][0-9]*[a-zA-Z]?$|i', $housenumber))
						{	$housenumber = (int)$housenumber;
						}
						$housenumber = $housenumber_prefix . $housenumber;
						$row = array(
								'"' . $this->CSVSafeString($donation->details['donortitle']) . '"', 
								'"' . $this->CSVSafeString($firstname[0]) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorsurname']) . '"', 
								'"' . $this->CSVSafeString($housenumber) . '"', 
								'"' . $this->CSVSafeString($this->PostcodeTidy($donation->details['donorpostcode'])) . '"', 
								'""', 
								'""', 
								'"' . date('d/m/y', strtotime($donation->details['donated'])) . '"', 
								number_format($donation->details['gbpamount'], 2, '.', ''));
						$csv->AddToRows($row);
						break;
				}
			}
		}
		$csv->Output();
	} // end of fn GiftaidDonationsCSVOutputHMRC
	
} // end of defn DonationsListCSV

$page = new DonationsListCSV();
?>
