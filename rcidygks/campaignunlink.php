<?php
include_once('sitedef.php');

class CampaignPage extends AdminCampaignsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'unlink';
		if (!$this->campaign->AdminCanUpdate())
		{	header('location: campaign.php?id=' . $this->campaign->id);
			exit;
		}

		if ($_GET['confirm'])
		{	if ($this->campaign->RemoveFromTeam())
			{	header('location: campaign.php?id=' . $this->campaign->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('campaignunlink.php', 'Unlink from team');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
	//	echo $this->campaign->InputForm($_GET['cuid']);
		if ($this->campaign->team && ($team = new Campaign($this->campaign->team)) && $team->id)
		{	echo '<h3>Currently linked to ', strip_tags($team->FullTitle()), '</h3><p><a class="button" href="campaignunlink.php?id=', $this->campaign->id, '&confirm=1">Remove this link (i.e. make ', strip_tags($this->campaign->FullTitle()), ' a solo campaign)</a></p>';
		} else
		{	echo '<h3>Not linked to a team</h3>';
		}
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignPage

$page = new CampaignPage();
$page->Page();
?>