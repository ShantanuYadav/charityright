<?php
include_once('sitedef.php');

class PostsPage extends AdminPostCatsPage
{	
	
	protected function AdminPostsBody()
	{	$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="8"><a href="postcat.php">Add new post category</a></th></tr><tr><th></th><th>Category name</th><th>Page title</th><th>Link</th><th class="num">No. of posts</th><th></th><th>Template</th><th>Actions</th></tr>';
		if ($cats = $this->GetCats())
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($cats as $cat_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$cat = new AdminPostCategory($cat_row);
					if (!$templates)
					{	$templates = $cat->PossibleTemplates();
					}
					
					$extras = array();
					if ($cat->GetQuickDonateOptions(true))
					{	$extras[] = 'DonateForm';
					}
					echo '<tr class="stripe', $i++ % 2,  '"><td>';
					if ($cat->HasImage())
					{	echo '<img src="', $cat->GetImageSRC(), '" width="150" />';
					}
					echo '</td><td>', $this->InputSafeString($cat->details['catname']), '</td><td>', $this->InputSafeString($cat->details['pagetitle']), '</td><td><a href="', $link = $cat->Link(), '" target="_blank">', $link, '</a></td><td class="num">', count($cat->GetPosts()), '</td><td>', implode('<br />', $extras), '</td><td>', $this->InputSafeString($templates[$cat->details['template']]), '</td><td><a href="postcat.php?id=', $cat->id, '">edit</a>';
					if ($cat->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="postcat.php?id=', $cat->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($cats) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($cats), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
	} // end of fn AdminPostsBody
	
	private function GetCats()
	{	$tables = array('postcategories'=>'postcategories');
		$fields = array('postcategories.*');
		$where = array();
		$orderby = array('postcategories.slug DESC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'pcatid');
	} // end of fn GetCats
	
} // end of defn PostsPage

$page = new PostsPage();
$page->Page();
?>
