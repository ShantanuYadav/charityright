<?php
include_once('sitedef.php');

class CampaignDonationsPage extends AdminCampaignsPage
{	private $donation;
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'donations';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->css['admincampdonation'] = 'admincampdonation.css';
		
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->donation->Delete())
			{	header('location: campaigndonations.php?id=' . $this->campaign->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
		
/*		if ($_GET['emailtest'])
		{	switch ($_GET['emailtest'] && $this->user->details['email'])
			{	case 'donor':
					//$this->donation->SendDonationEmail($this->user->details['email']);
					ob_start();
					print_r($this->user->details['email']);
					$this->successmessage = ob_get_clean();
					break;
				case 'owner':
					//$this->donation->SendOwnerDonationEmail();
					break;
			}
		}*/
		
		if ($_GET['refund'] && $_GET['confirm'] && $this->CanAdminUser('accounts admin'))
		{	if ($this->donation->WPRefund())
			{	$this->successmessage = 'Donation refunded';
			}
		}
		if (isset($_POST['donorfirstname']))
		{	$saved = $this->donation->SaveChanges($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		
		$this->breadcrumbs->AddCrumb('campaigndonations.php?id=' . $this->campaign->id, 'Donations');
		$this->breadcrumbs->AddCrumb('campaigndonation.php?id=' . $this->donation->id, $this->donation->ID());
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AssignCampaign()
	{	$this->donation = new AdminCampaignDonation($_GET['id']);
		if ($this->donation->id)
		{	$this->campaign = new AdminCampaign($this->donation->details['campaignid']);
		} else
		{	header('location: campaignsdonations.php');
		}
	} // end of fn AssignCampaign
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		if ($this->user->CanUserAccess('accounts'))
		{	
			if (SITE_TEST)
			{	//$this->donation->SendDonationEmail();
				//$this->donation->SendOwnerDonationEmail('');   //@tim removed
				//$this->VarDump($this->donation->details);
			}
			$address = array();
			if ($this->donation->details['donoradd1'])
			{	$address[] = $this->InputSafeString($this->donation->details['donoradd1']);
			}
			if ($this->donation->details['donoradd2'])
			{	$address[] = $this->InputSafeString($this->donation->details['donoradd2']);
			}
			$address[] = $this->InputSafeString($this->donation->details['donorcity']);
			$address[] = $this->InputSafeString($this->donation->details['donorpostcode']);
			$address[] = $this->InputSafeString($this->GetCountry($this->donation->details['donorcountry']));
			if ($this->donation->CanDelete())
			{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->donation->id, '&delete=1', $_GET['delete'] ? '&confirm=1' : '', '">', $_GET['delete'] ? 'please confirm you really want to ' : '', 'delete this donation</a></p>';
			} else
			{	if ($this->CanAdminUser('accounts admin') && $this->donation->CanRefund())
				{	echo '<p class="adminItemDeleteLink"><a href="', $_SERVER['SCRIPT_NAME'], '?id=', $this->donation->id, '&refund=1', $_GET['refund'] ? '&confirm=1' : '', '">', $_GET['refund'] ? 'please confirm you really want to ' : '', 'refund this donation</a></p>';
				}
			}
			echo '<form method="post" class="" target="campaigndonation.php?id=', $this->donation->id, '"><div id="itemblock-disp">
				<div><h3>Donation</h3><ul>
					<li><span class="itemblock-label">ID</span><span class="itemblock-details">', $this->donation->ID(), '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Donation ref</span><span class="itemblock-details">', $this->donation->details['donationref'] ? $this->donation->details['donationref'] : '~~ not paid ~~', '</span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">', (int)$this->donation->details['donationconfirmed'] ? 'Paid' : 'Created', '</span><span class="itemblock-details">', date('j-M-Y @H:i', strtotime((int)$this->donation->details['donationconfirmed'] ? $this->donation->details['donationconfirmed'] : $this->donation->details['donated'])), '</span><br class="clear" /></li>';
			if ($this->donation->details['cancelled'])
			{	echo '<li><span class="itemblock-label">Cancelled</span><span class="itemblock-details">', date('j-M-Y @H:i', strtotime($this->donation->details['canceldate'])), '</span><br class="clear" /></li>';
			} else
			{	if ($refunded = $this->donation->GetRefund())
				{	echo '<li><span class="itemblock-label">Refunded</span><span class="itemblock-details">', date('j-M-Y @H:i', strtotime($refunded['refunded'])), '</span><br class="clear" /></li>';
				}
			}
			echo '<li><span class="itemblock-label">Amount</span><span class="itemblock-details">', $this->GetCurrency($this->donation->details['currency'], 'cursymbol'), number_format($this->donation->details['amount'], 2), $this->donation->details['giftaid'] ? ' with giftaid' : '', '</span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">For</span><span class="itemblock-details">', $this->donation->details['zakat'] ? 'Zakat donation for<br />' : '';
			if ($this->donation->details['campaignid'] && ($campaign = new AdminCampaign($this->donation->details['campaignid'])) && $campaign->id)
			{	echo '<a href="campaign.php?id=', $campaign->id, '">', $campaign->FullTitle(), '</a>';
				if ($this->donation->details['amount'] !== $this->donation->details['campaignamount'])
				{	echo ' (', $this->GetCurrency($campaign->details['currency'], 'cursymbol'), number_format($this->donation->details['campaignamount'], 2), ')';
				}
			} else
			{	echo 'campaign missing';
			}
			if ($this->donation->details['teamid'] && ($team = new AdminCampaign($this->donation->details['teamid'])) && $team->id)
			{	echo '<br />which is raising for ... <a href="campaign.php?id=', $team->id, '">', $team->FullTitle(), '</a>';
				if ($this->donation->details['amount'] !== $this->donation->details['teamamount'])
				{	echo ' (', $this->GetCurrency($team->details['currency'], 'cursymbol'), number_format($this->donation->details['teamamount'], 2), ')';
				}
			}
			echo '</span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Giftaid?</span><span class="itemblock-details"><input type="checkbox" name="giftaid" value="1"', $this->donation->details['giftaid'] ? ' checked="checked"' : '', ' /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Zakat?</span><span class="itemblock-details"><input type="checkbox" name="zakat" value="1"', $this->donation->details['zakat'] ? ' checked="checked"' : '', ' /></span><br class="clear" /></li>';
			if ($this->donation->details['infusionsoftid'])
			{	$if = new Infusionsoft();
				if ($iflink = $if->GetInfusionsoftOrderLink($this->donation->details['infusionsoftid']))
				{	echo '<li><span class="itemblock-label">&nbsp;</span><span class="itemblock-details"><a href="', $iflink, '" target="_blank">view at Infusionsoft</a></span><br class="clear" /></li>';
				}
			}
			echo '</ul></div>
				<div><h3>Donor</h3><ul>';
			echo '<li><span class="itemblock-label">Donor anonymous</span><span class="itemblock-details"><input type="checkbox" name="donoranon" value="1"', $this->donation->details['donoranon'] ? ' checked="checked"' : '', ' /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Show details to owner</span><span class="itemblock-details"><input type="checkbox" name="donorshowto" value="1"', $this->donation->details['donorshowto'] ? ' checked="checked"' : '', ' /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Title</span><span class="itemblock-details"><input type="text" name="donortitle" value="', $this->InputSafeString($this->donation->details['donortitle']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">First name</span><span class="itemblock-details"><input type="text" name="donorfirstname" value="', $this->InputSafeString($this->donation->details['donorfirstname']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Surname</span><span class="itemblock-details"><input type="text" name="donorsurname" value="', $this->InputSafeString($this->donation->details['donorsurname']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Email</span><span class="itemblock-details"><input type="text" name="donoremail" value="', $this->InputSafeString($this->donation->details['donoremail']), '" /></span><br class="clear" /></li>';
			if ($this->CanAdminUser('crm') && $this->donation->details['donoremail'])
			{	echo '<li><span class="itemblock-label">&nbsp;</span><span class="itemblock-details"><a href="crm_email.php?email=', urlencode($this->donation->details['donoremail']), '" target="_blank">view "', $this->InputSafeString($this->donation->details['donoremail']), '" in CRM</a></span><br class="clear" /></li>';
			}
			echo '<li><span class="itemblock-label">Address</span><span class="itemblock-details"><input type="text" name="donoradd1" value="', $this->InputSafeString($this->donation->details['donoradd1']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">&nbsp;</span><span class="itemblock-details"><input type="text" name="donoradd2" value="', $this->InputSafeString($this->donation->details['donoradd2']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Town / City</span><span class="itemblock-details"><input type="text" name="donorcity" value="', $this->InputSafeString($this->donation->details['donorcity']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Postcode</span><span class="itemblock-details"><input type="text" name="donorpostcode" value="', $this->InputSafeString($this->donation->details['donorpostcode']), '" /></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Country</span><span class="itemblock-details"><select name="donorcountry"><option value="">------</option>';
			foreach ($this->donation->CountryList() as $ccode=>$cname)
			{	echo '<option value="', $ccode, '"', $ccode == $this->donation->details['donorcountry'] ? ' selected="selected"' : '', '>', $this->InputSafeString($cname), '</option>';
			}
			echo '</select></span><br class="clear" /></li>';
			echo '<li><span class="itemblock-label">Phone</span><span class="itemblock-details"><input type="text" name="donorphone" value="', $this->InputSafeString($this->donation->details['donorphone']), '" /></span><br class="clear" /></li>';
			$pref = new ContactPreferences();
			echo '<li><span class="itemblock-label">Contact methods approved</span><span class="itemblock-details">', $this->donation->details['contactpref'] ? $pref->DisplayChosenPreferences($this->donation->details['contactpref']) : 'none', '</span><br class="clear" /></li>
				<li><span class="itemblock-label">Message</span><span class="itemblock-details"><textarea name="donorcomment">', $this->InputSafeString($this->donation->details['donorcomment']), '</textarea></span><br class="clear" /></li>
				</ul></div><p><input type="submit" class="submit" value="Save Changes" /></p></form><div class="clear"></div>';
			if ($this->donation->extrafields)
			{	echo '<div><h3>Extra info</h3><ul>';
				foreach ($this->donation->extrafields as $name=>$value)
				{	echo '<li><span class="itemblock-label">', $this->InputSafeString($name), '</span><span class="itemblock-details">', $this->InputSafeString($value), '</span><br class="clear" /></li>';
				}
				echo '</ul></div>';
			}
			echo '</div>';
/*			echo '<h3>Test emails to you</h3>
					<ul>
						<li><a href="campaigndonation.php?id=', $this->donation->id, '&emailtest=donor">Email to donor</a></li>
						<li><a href="campaigndonation.php?id=', $this->donation->id, '&emailtest=owner">Email to campaign owner</a></li>
					</ul>';*/
		}
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignDonationsPage

$page = new CampaignDonationsPage();
$page->Page();
?>