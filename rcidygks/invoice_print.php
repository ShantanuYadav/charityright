<?php
include_once('sitedef.php');

class PrintableInvoice extends AccountsMenuPage
{	var $order = '';

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		
		$this->css = array();
		$this->js = array();
		
		$this->order = new AdminSCOrder($_GET['id']);
		
		$this->title = 'Invoice for order #' . $this->order->id;
		
	} // end of fn AccountsLoggedInConstruct
	
	function AdminMenu(){}
	function Header(){}
	function Footer(){}
	function DisplayBreadcrumbs(){}
	function Messages(){}
	function HistoryPopUp(){}

	function AccountsBody()
	{	echo $this->order->PrintableInvoice();
	} // end of fn AccountsBody
	
} // end of defn PrintableInvoice

$page = new PrintableInvoice();
$page->Page();
?>