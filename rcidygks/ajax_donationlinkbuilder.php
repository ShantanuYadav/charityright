<?php
include_once('sitedef.php');

class DonationLinkBuilderAjax extends AdminDonationOptionsPage
{	
	protected function AdminDonationOptionsLoggedInConstruct()
	{	parent::AdminDonationOptionsLoggedInConstruct();
		
		switch($_GET['action'])
		{	case 'type_change':
				$donation = new Donation();
				$countries = $donation->GetDonationCountries($_GET['type'], true);
				
				//check if selected is there
				$def_country = 0;
				foreach ($countries as $country)
				{	if ($country['dcid'] == $_GET['dcid'])
					{	$def_country = $country['dcid'];
						break;
					} else
					{	if (!$def_country)
						{	$def_country = $country['dcid'];
						}
					}
				}
				
				$output = array();
				ob_start();
				foreach ($countries as $country)
				{	echo '<option value="', $country['dcid'], '"', $def_country == $country['dcid'] ? ' selected="selected"' : '', '>', $this->InputSafeString($country['shortname']), '</option>';
				}
				$output['countries'] = ob_get_clean();
				
				// get currencies
				ob_start();
				$currencies = $this->GetCurrencies($_GET['type']);
				if (!$currencies[$_GET['currencies']])
				{	unset($_GET['currencies']);
				}
				foreach ($currencies as $curcode=>$currency)
				{	
					echo '<option value="', $curcode, '"', (($_GET['currencies'] && ($_GET['currencies'] == $curcode)) || !$curcount++) ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
				}
				$output['currencies'] = ob_get_clean();
				
				echo json_encode($output);
				break;
			case 'country_change':
				$country = new DonationOption($_GET['dcid']);
				if ($projects = $country->LiveProjects())
				{	if (!$projects[$_GET['dpid']])
					{	unset($_GET['dpid']);
					}
					foreach ($projects as $project)
					{	if ($_GET['dpid'])
						{	$selected = ($_GET['dpid'] == $project['dpid']);
						} else
						{	$selected = !$count++;
						}
						echo '<option value="', $project['dpid'], '"', $selected ? ' selected="selected"' : '', '>', $this->InputSafeString($project['projectname']), '</option>';
					}
				}
				break;
			case 'build':
				$builder = new DonationLinkBuilder();
				echo $builder->BuildLink($_GET);
				break;
		}
		
	} // end of fn AdminDonationOptionsLoggedInConstruct

	private function GetCurrencies($type = '')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies
	
} // end of defn DonationLinkBuilderAjax

$page = new DonationLinkBuilderAjax();
?>
