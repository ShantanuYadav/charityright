<?php
include_once('sitedef.php');

class DonationOptionPage extends AdminDonationOptionsPage
{	
	
	protected function AssignDonationCountry()
	{	$this->donationproject = new AdminDonationProject($_GET['id']);
		if ($this->donationproject->id)
		{	$this->donationcountry = new AdminDonationOption($this->donationproject->details['dcid']);
		} else
		{	$this->donationcountry = new AdminDonationOption($_GET['dcid']);
		}
	} // end of fn AssignDonationCountry
	
	protected function AdminDonationOptionsConstructFunctions()
	{	if (isset($_POST['projectname']))
		{	$saved = $this->donationproject->Save($_POST, $this->donationcountry);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->donationproject->Delete())
			{	header('location: donationprojects.php?id=' . $this->donationcountry->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
		$this->menuarea = $this->donationproject->id ? 'project' : 'projects';
	} // end of fn AdminDonationOptionsConstructFunctions
	
	protected function AdminDonationOptionsLoggedInConstruct()
	{	parent::AdminDonationOptionsLoggedInConstruct();
		if (!$this->donationproject->id)
		{	$this->breadcrumbs->AddCrumb('donationprojects.php?id=' . $this->donationcountry->id, 'Sub-options');
			$this->breadcrumbs->AddCrumb('donationproject.php?dcid=' . $this->donationcountry->id, 'New sub-option');
		}
	} // end of fn AdminDonationOptionsLoggedInConstruct
	
	protected function AdminDonationOptionsBody()
	{	parent::AdminDonationOptionsBody();
		echo $this->donationproject->InputForm($this->donationcountry);
	} // end of fn AdminDonationOptionsBody
	
} // end of defn DonationOptionPage

$page = new DonationOptionPage();
$page->Page();
?>