<?php
include_once('sitedef.php');

class PageDonateFormPage extends AdminPagesPage
{	protected $menuarea = 'donateform';

	function AdminPagesLoggedInConstruct()
	{	parent::AdminPagesLoggedInConstruct();
		$this->js['admin_donationlinkbuilder'] = 'admin_donationlinkbuilder.js';
		$this->breadcrumbs->AddCrumb('pagedonateform.php', 'Quick donate form');
	} // end of fn AdminPagesLoggedInConstruct

	function AdminPageConstructFunctions()
	{	parent::AdminPageConstructFunctions();
		
		if (isset($_POST['amount']))
		{	$saved = $this->editpage->AdminQuickDonateSave($_POST);
			if ($saved['failmessage'])
			{	$this->failmessage = $saved['failmessage'];
			}
			if ($saved['successmessage'])
			{	$this->successmessage = $saved['successmessage'];
			}
		}
	} // end of fn AdminPageConstructFunctions

	function AdminPagesBody()
	{	parent::AdminPagesBody();
		echo $this->editpage->AdminQuickDonateForm();
	} // end of fn AdminPagesBody
	
} // end of defn PageDonateFormPage

$page = new PageDonateFormPage();
$page->Page();
?>
