<?php
include_once('sitedef.php');

class EventPage extends AdminEventsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function EventConstructFunctions()
	{	$this->js[] = 'tiny_mce/jquery.tinymce.js';
		$this->js[] = 'pageedit_tiny_mce.js';
		$this->menuarea = 'edit';
		if (isset($_POST['eventname']))
		{	$saved = $this->event->Save($_POST, $_FILES['eventimage']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->event->Delete())
			{	header('location: events.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn EventConstructFunctions
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		if (!$this->event->id)
		{	$this->breadcrumbs->AddCrumb('event.php', 'New event');
		}
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function AdminEventsBody()
	{	parent::AdminEventsBody();
		echo $this->event->InputForm();
	} // end of fn AdminEventsBody
	
} // end of defn EventPage

$page = new EventPage();
$page->Page();
?>