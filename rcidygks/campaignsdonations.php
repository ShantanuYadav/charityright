<?php
include_once('sitedef.php');

class CampaignsDonationsPage extends AdminCampaignsPage
{	var $startdate = '';
	var $enddate = '';
	private $admindonations;
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->admindonations = new AdminDonations();
		$this->showunpaid = $_GET['showunpaid'];
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		$this->breadcrumbs->AddCrumb('campaignsdonations.php', 'All Donations');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	if ($this->user->CanUserAccess('accounts'))
		{	$filter = $_GET;
			$filter['startdate'] = $this->startdate;
			$filter['enddate'] = $this->enddate;
			echo $this->DonationsFilterForm(), $this->DonationsList($this->admindonations->GetCampaignDonations($filter));
			$get = array();
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
			}
			$get[] = 'startdate=' . $this->startdate;
			$get[] = 'enddate=' . $this->enddate;
			echo '<p class="adminCSVDownload"><a href="campaignsdonations_csv.php?', implode('&', $get) , '">Download CSV (for whole selection)</a></p>';
		}
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignsDonationsPage

$page = new CampaignsDonationsPage();
$page->Page();
?>
