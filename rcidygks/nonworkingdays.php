<?php
include_once('sitedef.php');

class NonWorkingDaysPage extends AccountsMenuPage
{	private $startdate = '';
	private $enddate = '';
	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		$this->StartAndEndDatesFromGet($_GET, $this->startdate, $this->enddate);
		
		if (!$this->startdate)
		{	$this->startdate = $this->datefn->SQLDate();
		}
		if (!$this->enddate)
		{	$this->enddate = $this->datefn->SQLDate(strtotime('+2 years'));
		}
		if ($this->enddate && $this->startdate && ($this->enddate < $this->startdate))
		{	$this->enddate = $this->startdate;
		}
		
		$this->breadcrumbs->AddCrumb('nonworkingdays.php', 'Non-working days');
	} // end of fn AccountsLoggedInConstruct
	
	function AccountsBody()
	{	echo $this->FilterForm(), '<table><tr class="newlink"><th colspan="3"><a href="nonworkingday.php">Add new non-working day</a></th></tr><tr><th>Date</th><th>Description</th><th>Actions</th></tr>';
		if ($days = $this->GetNonWorkingDays())
		{	foreach ($days as $day_row)
			{	$day = new NonWorkingDay($day_row);
				echo '<tr><td>', date('D j-M-Y', strtotime($day->details['nwdate'])), '</td><td>', $this->InputSafeString($day->details['description']), '</td><td><a href="nonworkingday.php?id=', $day->id, '">edit</a>';
				if ($day->CanDelete())
				echo '&nbsp;|&nbsp;<a href="nonworkingday.php?id=', $day->id, '&delete=1">delete</a></td></tr>';
			}
		}
		echo '</table>';
	} // end of fn AccountsBody
	
	function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y') - 10, +20), false, false, true);
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y') - 10, +20), false, false, true);
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
	function GetNonWorkingDays()
	{	$tables = array('nonworkingdays'=>'nonworkingdays');
		$fields = array('nonworkingdays.*');
		$where = array();
		$orderby = array('nonworkingdays.nwdate');
		
		if ($this->startdate)
		{	$where['startdate'] = 'nonworkingdays.nwdate>="' . $this->startdate . '"';
		}
		if ($this->enddate)
		{	$where['enddate'] = 'nonworkingdays.nwdate<="' . $this->enddate . '"';
		}
		
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'nwid', true);
	} // end of fn GetNonWorkingDays
	
} // end of defn NonWorkingDaysPage

$page = new NonWorkingDaysPage();
$page->Page();
?>