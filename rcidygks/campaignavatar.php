<?php
include_once('sitedef.php');

class CampaignAvatarPage extends AdminCampaignAvatarPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'edit';
		if (isset($_POST['imagedesc']))
		{	$saved = $this->campaignavatar->Save($_POST, $_FILES['imagefile']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->campaignavatar->Delete())
			{	header('location: campaignavatars.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		if (!$this->campaignavatar->id)
		{	$this->breadcrumbs->AddCrumb('campaignavatar.php', 'New avatar');
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->campaignavatar->InputForm();
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignAvatarPage

$page = new CampaignAvatarPage();
$page->Page();
?>