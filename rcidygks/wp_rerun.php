<?php
include_once('sitedef.php');

class WorldpayRerunPage extends AccountsMenuPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->css[] = 'adminwp.css';
		
		if ($_POST['wptext'])
		{	$wpear = new AdminWorldPayEar();
			$wpear->ProcessPostText($_POST['wptext']);
			$this->failmessage = $wpear->failmessage;
			$this->successmessage = $wpear->successmessage;
		}

		$this->breadcrumbs->AddCrumb('wp_rerun.php', 'Worldpay Rerun Notification');
		
	} // end of fn AccountsLoggedInConstruct
	
	function AccountsBody()
	{	$form = new Form($_SERVER['SCRIPT_NAME'], 'wpTextArea');
		$form->AddTextArea('Text from worldpay', 'wptext', $this->InputSafeString($_POST['wptext']), '', 0, 0, 3, 80);
		$form->AddSubmitButton('', 'Submit for processing', 'submit');
		$form->Output();
	} // end of fn AccountsBody
	
} // end of defn WorldpayRerunPage

$page = new WorldpayRerunPage();
$page->Page();
?>