<?php
include_once('sitedef.php');

class SitemapPage extends CMSPage
{	private $sitemapBuilder;

	function __construct()
	{	parent::__construct('CMS');
	} //  end of fn __construct

	function CMSLoggedInConstruct()
	{	parent::CMSLoggedInConstruct();
		$this->sitemapBuilder = new SitemapBuilder();
		$this->breadcrumbs->AddCrumb('sitemap.php', 'Sitemap');
		if ($this->user->CanUserAccess('web content'))
		{	if ($_GET['rebuild'])
			{	if ($this->sitemapBuilder->Build())
				{	$this->successmessage = 'Sitemap has been rebuilt';
				} else
				{	$this->failmessage = 'Sitemap rebuild failed';
				}
			}
		}
	} // end of fn CMSLoggedInConstruct

	function CMSBodyMain()
	{	if ($this->sitemapBuilder->FileExists())
		{	echo '<h3>Sitemap is ', $this->sitemapBuilder->FileAgeString(), '</h3>';
		} else
		{	echo '<h3>Sitemap file does not exist</h3>';
		}
		echo '<p><a href="sitemap.php?rebuild=1">Rebuild sitemap now</a></p><ul>';
		foreach ($this->sitemapBuilder->FileContents() as $link)
		{	echo '<li>', $link, '</li>';
		}
		echo '</ul>';
		if ($robots_line = $this->sitemapBuilder->RobotsLine())
		{	echo '<p>in robots.txt:"', $robots_line, '"</p>';
		} else
		{	echo '<h3>Sitemap not mentioned in robots.txt</h3>';
		}
	} // end of fn CMSBodyMain
	
} // end of defn SitemapPage

$page = new SitemapPage();
$page->Page();
?>