<?php
include_once('sitedef.php');

class DelOptionPage extends AccountsMenuPage
{	var $deloption;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->deloption = new AdminDelOption($_GET['id']);
		
		if ($_POST['deldesc'])
		{	$saved = $this->deloption->Save($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
		
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->deloption->Delete())
			{	header('location: deloptions.php');
				exit;
			} else
			{	$this->failmessage = 'Delete failed';
			}
		}
		
		$this->breadcrumbs->AddCrumb('deloptions.php', 'Delivery options');
		$this->breadcrumbs->AddCrumb('deloption.php?id=' . $this->deloption->id, $this->deloption->id ? $this->InputSafeString($this->deloption->details['deldesc']) : 'Adding New Option');
	} //  end of fn AccountsLoggedInConstruct
	
	function AccountsBody()
	{	$this->deloption->InputForm();
	} // end of fn AccountsBody
	
} // end of defn DelOptionPage

$page = new DelOptionPage();
$page->Page();
?>