<?php
include_once('sitedef.php');

class CampaignsPage extends AdminCampaignUsersPage
{	var $startdate = '';
	var $enddate = '';
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	if (!isset($_GET['ystart']))
			{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 year'));
			}
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		}
		
		if ($this->enddate && $this->startdate && ($this->enddate < $this->startdate))
		{	$this->enddate = $this->startdate;
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	$perpage = 20;
		echo $this->UsersFilterForm(), '<table><tr class="newlink"><th colspan="7"><a href="campaignuser.php">Create new campaign owner</a></th></tr><tr><th></th><th>Name</th><th>Email</th><th>Link</th><th>Registered</th><th>Campaigns</th><th>Actions</th></tr>';
		if ($campaignusers = $this->GetCampaignUsers())
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($campaignusers as $campaignuser_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$campaignuser = new AdminCampaignUser($campaignuser_row);
					echo '<tr class="stripe', $i++ % 2,  '"><td><a href="campaignuser.php?id=', $campaignuser->id, '"><img src="', ($src = $campaignuser->GetImageSRC('thumb')) ? $src : '/img/template/user_avatar_thumb.png', '" /></a></td><td>', $this->InputSafeString($campaignuser->details['firstname'] . ' ' . $campaignuser->details['lastname']), $campaignuser->details['suspended'] ? '<br />[SUSPENDED]' : '', '</td><td>', $this->InputSafeString($campaignuser->details['email']), '</td><td><a target="_blank" href="', $link = $campaignuser->Link(), '">', $link, '</a></td><td>', date('d/m/y @H:i', strtotime($campaignuser->details['registered'])), '</td><td><a href="campaignusercampaigns.php?id=', $campaignuser->id, '">', count($campaignuser->GetCampaigns()), '</a></td><td><a href="campaignuser.php?id=', $campaignuser->id, '">view</a>';
					if ($campaignuser->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="campaignuser.php?id=', $campaignuser->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		} else
		{	echo '<tr><td colspan="7" style="text-align: center;">No campaign owners found</td></tr>';
		}
		echo '</table>';
		if (count($campaignusers) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($campaignusers), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
	} // end of fn AdminCampaignBody
	
	private function GetCampaignUsers()
	{	$tables = array('campaignusers'=>'campaignusers');
		$fields = array('campaignusers.*');
		$where = array();
		$orderby = array('campaignusers.registered DESC');
		$groupby = array();
		
		if ($this->startdate)
		{	$where['startdate'] = 'campaignusers.registered>="' . $this->startdate . ' 00:00:00"';
		}
		if ($this->enddate)
		{	$where['enddate'] = 'campaignusers.registered<="' . $this->enddate . ' 23:59:59"';
		}
		
		if ($_GET['oname'])
		{	$where['oname'] = 'CONCAT(campaignusers.firstname, " ", campaignusers.lastname) LIKE "%' . $this->SQLSafe($_GET['oname']) . '%"';
		}
		
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby, $groupby), 'cuid');
	} // end of fn GetCampaignUsers
	
	protected function UsersFilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10), false, false, true);
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10), false, false, true);
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get">';
		echo '<span>Registered from</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<span>Owner name</span><input type="text" name="oname" value="', $this->InputSafeString($_GET['oname']), '" /><input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn UsersFilterForm
	
} // end of defn CampaignsPage

$page = new CampaignsPage();
$page->Page();
?>