<?php
include_once('sitedef.php');

class CampaignPage extends AdminCampaignsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'members';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		if (!$this->campaign->id)
		{	$this->breadcrumbs->AddCrumb('campaignmembers.php', 'Members');
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->CampaignsList($this->campaign->team_members, 30, false);
		if ($this->campaign->team_members)
		{	$get = array();
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
			}
			echo '<p class="adminCSVDownload"><a href="campaignmembers_csv.php?', implode('&', $get) , '">Download CSV (for whole selection)</a></p>';
		}
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignPage

$page = new CampaignPage();
$page->Page();
?>