<?php
include_once('sitedef.php');

class UserListPage extends AdminUsersPage
{	
	
	function AdminUsersBody()
	{	parent::AdminUsersBody();
		$this->UserList();
	} // end of fn AdminUsersBody
	
	function UserList()
	{	$sql = 'SELECT * FROM adminusers ORDER BY surname, firstname';
		echo '<table><tr class="newlink"><th colspan="4"><a href="useredit.php">create new user</a></th></tr><tr><th>Name</th><th>Log in</th><th>Access to ...</th><th>Actions</th></tr>';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$user = new AdminUser($row['auserid'], 1);
				echo '<tr class="stripe', $i++ % 2, '"><td>', $user->FullName(), '</td><td>', $user->details['ausername'], '</td><td>', $user->UserAccessList(), '</td>',
					'<td><a href="useredit.php?userid=', $row['auserid'], '">edit</a>&nbsp;|&nbsp;<a href="useraccess.php?userid=', $row['auserid'], '">access</a>';
				if ($this->user->userid != $user->userid)
				{	echo '&nbsp;|&nbsp;', $user->DeleteLink('delete');
				}
				echo '</td></tr>';
			}
		}
		echo '</table>';
	} // end of fn UserList
	
} // end of defn UserListPage

$page = new UserListPage();
$page->Page();
?>