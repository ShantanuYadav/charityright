<?php
include_once('sitedef.php');

class PostsAjax extends AdminPostsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		
		switch($_GET['action'])
		{	case 'catadd':
				if ($this->post->id)
				{	$this->post->CategoryAdd($_GET['catid']);
				}
				break;
			case 'catremove':
				if ($this->post->id)
				{	$this->post->CategoryRemove($_GET['catid']);
				}
				break;
			case 'catspopup':
				if ($this->post->id)
				{	echo $this->post->CategoriesAddList();
				}
				break;
			case 'catstable':
				if ($this->post->id)
				{	echo $this->post->CategoriesTable();
				}
				break;
			case 'vidadd':
				if ($this->post->id)
				{	$this->post->VideoAdd($_GET['vid']);
				}
				break;
			case 'vidremove':
				if ($this->post->id)
				{	$this->post->VideoRemove($_GET['vid']);
				}
				break;
			case 'vidspopup':
				if ($this->post->id)
				{	echo $this->post->VideosAddList();
				}
				break;
			case 'vidstable':
				if ($this->post->id)
				{	echo $this->post->VideosTable();
				}
			case 'postadd':
				if ($this->post->id)
				{	$this->post->PostAdd($_GET['postid']);
				}
				break;
			case 'postremove':
				if ($this->post->id)
				{	$this->post->PostRemove($_GET['postid']);
				}
				break;
			case 'postspopup':
				if ($this->post->id)
				{	echo $this->post->PostsAddList();
				}
				break;
			case 'poststable':
				if ($this->post->id)
				{	echo $this->post->PostsTable();
				}
				break;
		}
		
	} // end of fn AdminPostsLoggedInConstruct
	
} // end of defn PostsAjax

$page = new PostsAjax();
?>