<?php
include_once('sitedef.php');

class PostCatQuickDonatePage extends AdminPostCatsPage
{	
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		$this->menuarea = 'donateform';
		if (isset($_POST['amount']))
		{	$saved = $this->category->AdminQuickDonateSave($_POST);
			if ($saved['failmessage'])
			{	$this->failmessage = $saved['failmessage'];
			}
			if ($saved['successmessage'])
			{	$this->successmessage = $saved['successmessage'];
			}
		}
		
		$this->js['admin_donationlinkbuilder'] = 'admin_donationlinkbuilder.js';
		$this->breadcrumbs->AddCrumb('postcatdonateform.php', 'Quick donate form');
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->category->AdminQuickDonateForm();
	} // end of fn AdminPostsBody
	
} // end of defn PostCatQuickDonatePage

$page = new PostCatQuickDonatePage();
$page->Page();
?>
