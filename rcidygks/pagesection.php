<?php
include_once('sitedef.php');

class PageSectionsPage extends AdminPagesPage
{	protected $menuarea = 'section';

	function AdminPagesLoggedInConstruct()
	{	parent::AdminPagesLoggedInConstruct();
		$this->js['tinymce'] = 'tiny_mce/jquery.tinymce.js';
		$this->js['pagesection_tiny_mce'] = 'pagesection_tiny_mce.js';
		$this->js['admin_pagesections'] = 'admin_pagesections.js';
		$this->css['jPicker'] = 'jPicker-1.1.6.css';
		$this->js['jPicker'] = 'jpicker-1.1.6.min.js';
		$this->breadcrumbs->AddCrumb('pagesections.php?id=' . $this->editpage->id, 'Sections');
		if ($this->pagesection->id)
		{	$this->breadcrumbs->AddCrumb('pagesection.php?id=' . $this->pagesection->id, '#' . $this->pagesection->id);
		} else
		{	$this->breadcrumbs->AddCrumb('pagesection.php?pageid=' . $this->editpage->id, 'New section');
		}
	} // end of fn AdminPagesLoggedInConstruct
	
	protected function GetPageMenu()
	{	$menu = parent::GetPageMenu();
		if (!$this->pagesection->id)
		{	$menu['section'] = array('text'=>'New Section', 'link'=>'pagesection.php?pageid=' . $this->editpage->id);
		}
		return $menu;
	} // end of fn GetPageMenu
	
	function AdminPageConstructFunctions()
	{	parent::AdminPageConstructFunctions();
		
		if (isset($_POST['pstext']))
		{	$saved = $this->pagesection->Save($_POST, $this->editpage->id, 0, $_FILES['imagefile']);
			if ($saved['failmessage'])
			{	$this->failmessage = $saved['failmessage'];
			}
			if ($saved['successmessage'])
			{	$this->successmessage = $saved['successmessage'];
			}
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->pagesection->Delete())
			{	header('location: pagesections.php?id=' . $this->editpage->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn AdminPageConstructFunctions
	
	protected function AssignPage()
	{	$this->pagesection = new AdminPageSection($_GET['id']);
		if ($this->pagesection->id)
		{	$this->editpage = new AdminPageContent($this->pagesection->details['pageid']);
		} else
		{	$this->editpage = new AdminPageContent($_GET['pageid']);
		}
	} // end of fn AssignPage

	function AdminPagesBody()
	{	parent::AdminPagesBody();
		echo $this->pagesection->InputForm($this->editpage->id);
	} // end of fn AdminPagesBody
	
} // end of defn PageSectionsPage

$page = new PageSectionsPage();
$page->Page();
?>