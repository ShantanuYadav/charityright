<?php
include_once('sitedef.php');

class EmailCRMViewPage extends AdminCRMPage
{	private $startdate = '';
	private $enddate = '';
	
	protected function AdminCRMLoggedInConstruct()
	{	parent::AdminCRMLoggedInConstruct();
		$this->email = new CRMEmail($_GET['email']);
		$this->breadcrumbs->AddCrumb('crm_email.php?email=' . urlencode($this->email->Email()), $this->InputSafeString($this->email->Email()));
		if (isset($_POST['submitCPref']))
		{	$this->SaveCPRefChanges();
		}
		if (isset($_POST['submitCPrefAll']))
		{	$this->SaveCPRefChangesAll();
		}
	} // end of fn AdminCRMLoggedInConstruct
	
	protected function AdminCRMBodyMain()
	{	if ($this->email->EmailOK())
		{	if ($activity = $this->email->GetActivity())
			{	$if = new Infusionsoft();
				if ($iflink = $if->GetInfusionsoftLinkFromEmail($this->email->Email()))
				{	echo '<p><a href="', $iflink, '">Contact at Infusionsoft</a> (log in to Infusionsoft first)</p>';
				}
				echo '<form action="', $_SERVER['SCRIPT_NAME'], '?email=', urlencode($this->email->Email()), '" method="post"><table><tr><th>When</th><th>What</th><th>Contact details</th><th>Status</th><th>Contact details</th><th>Actions</th></tr>';
				foreach ($activity as $event)
				{	$rowkey++;
					$contact = array();
					$status = array();
					switch ($event['type'])
					{	case 'donation':
							$contact[] = $this->InputSafeString(trim($event['data']['donortitle'] . ' ' . $event['data']['donorfirstname'] . ' ' . $event['data']['donorsurname']));
							if ($event['data']['donoradd1'])
							{	$contact[] = $this->InputSafeString($event['data']['donoradd1']);
							}
							if ($event['data']['donoradd2'])
							{	$contact[] = $this->InputSafeString($event['data']['donoradd2']);
							}
							if ($event['data']['donorcity'])
							{	$contact[] = $this->InputSafeString($event['data']['donorcity']);
							}
							if ($event['data']['donorpostcode'])
							{	$contact[] = $this->InputSafeString($event['data']['donorpostcode']);
							}
							if ($country = $this->GetCountry($event['data']['donorcountry']))
							{	$contact[] = $this->InputSafeString($country);
							}
							if ($event['data']['donorphone'])
							{	$contact[] = 'tel: ' . $this->InputSafeString($event['data']['donorphone']);
							}
							$status[] = $event['data']['donationtype'];
							if ($event['data']['donationtype'] == 'monthly')
							{	if ($event['data']['donationref'])
								{	$status[] = 'confirmed';
								} else
								{	$status[] = 'not confirmed';
								}
								$status[] = $event['data']['currency'] . number_format($event['data']['amount'], 2) . ' per month';
							} else
							{	if ($event['data']['donationref'])
								{	$status[] = 'paid';
								} else
								{	$status[] = 'not paid';
								}
								$status[] = $event['data']['currency'] . number_format($event['data']['amount'], 2);
							}
							break;
						case 'crdonation':
							$contact[] = $this->InputSafeString(trim($event['data']['donortitle'] . ' ' . $event['data']['donorfirstname'] . ' ' . $event['data']['donorsurname']));
							if ($event['data']['donoradd1'])
							{	$contact[] = $this->InputSafeString($event['data']['donoradd1']);
							}
							if ($event['data']['donoradd2'])
							{	$contact[] = $this->InputSafeString($event['data']['donoradd2']);
							}
							if ($event['data']['donorcity'])
							{	$contact[] = $this->InputSafeString($event['data']['donorcity']);
							}
							if ($event['data']['donorpostcode'])
							{	$contact[] = $this->InputSafeString($event['data']['donorpostcode']);
							}
							if ($country = $this->GetCountry($event['data']['donorcountry']))
							{	$contact[] = $this->InputSafeString($country);
							}
							if ($event['data']['donorphone'])
							{	$contact[] = 'tel: ' . $this->InputSafeString($event['data']['donorphone']);
							}
							if ($event['data']['donationref'])
							{	$status[] = 'paid';
							} else
							{	$status[] = 'not paid';
							}
							$status[] = $event['data']['currency'] . number_format($event['data']['amount'], 2);
							break;
						case 'campaignowner':
							$contact[] = $this->InputSafeString(trim($event['data']['firstname'] . ' ' . $event['data']['lastname']));
							if ($event['data']['phone'])
							{	$contact[] = 'tel: ' . $this->InputSafeString($event['data']['phone']);
							}
							break;
					}
					$pref = new ContactPreferences($event['type'] == 'campaignowner' ? array('email', 'phone', 'text') : false);
					echo '<tr><td>', date('d/m/Y @H:i', strtotime($event['time'])), '</td><td>', $this->InputSafeString($event['typetext']), '</td><td>', implode('<br />', $status), '</td><td>', implode('<br />', $contact), '</td><td class="crmCprefTableForm"><input type="hidden" name="type[', $rowkey, ']" value="', $event['type'], '" /><input type="hidden" name="cpid[', $rowkey, ']" value="', $event['id'], '" />', $pref->AdminFormListElements($rowkey, $event['data']['contactpref']), '</td><td>';
					switch ($event['type'])
					{	case 'donation':
							echo '<a href="donation.php?id=', $event['id'], '">view</a>';
							break;
						case 'crdonation':
							echo '<a href="campaigndonation.php?id=', $event['id'], '">view</a>';
							break;
						case 'campaignowner':
							echo '<a href="campaignuser.php?id=', $event['id'], '">view</a>';
							break;
					}
					echo '</td></tr>';
				}
				echo '<tr><td colspan="4"></td><td colspan="2"><input type="submit" name="submitCPref" value="Save Contact Preferences" class="submit" /></td></tr></table></form>';
				if (($act_count = count($activity)) > 1)
				{	$pref = new ContactPreferences();
					$rowkey = 0;
					echo '<div class="crmCpredAllForm"><form action="', $_SERVER['SCRIPT_NAME'], '?email=', urlencode($this->email->Email()), '" method="post">';
					foreach ($activity as $event)
					{	$rowkey++;
						echo '<input type="hidden" name="cpAllType[', $rowkey, ']" value="', $event['type'], '" /><input type="hidden" name="cpAllID[', $rowkey, ']" value="', $event['id'], '" />';
						
					}

					echo '<h3>Amend all the above contact preferences</h3>', $pref->AdminFormBlankListElements(), '<label></label><input type="submit" class="submit" value="Save These Preferences for All" name="submitCPrefAll" /><br />';
					
					echo '</form></div>';
				}
			} else
			{	echo '<h3>No activity found</h3>';
			}
		} else
		{	echo '<h3>Invalid email</h3>';
		}
	} // end of fn AdminCRMBodyMain
	
	protected function SaveCPRefChangesAll()
	{	$pref = new ContactPreferences();
		foreach ($_POST['cpAllType'] as $key=>$type)
		{	if ($id = (int)$_POST['cpAllID'][$key])
			{	$pref = new ContactPreferences($type == 'campaignowner' ? array('email', 'phone', 'text') : false);
				$pref_value = $pref->PreferencesValueFromArray($_POST['contactpref_all']);
				switch ($type)
				{	case 'donation':
						$sql = 'UPDATE donations SET contactpref=' . $pref_value . ' WHERE did=' . $id;
						break;
					case 'crdonation':
						$sql = 'UPDATE campaigndonations SET contactpref=' . $pref_value . ' WHERE cdid=' . $id;
						break;
					case 'campaignowner':
						$sql = 'UPDATE campaignusers SET contactpref=' . $pref_value . ' WHERE cuid=' . $id;
						break;
				}
				if ($sql)
				{	if ($result = $this->db->Query($sql))
					{	if ($this->db->AffectedRows())
						{	$success++;
						}
					}
				}
			}
		}
		if ($success = (int)$success)
		{	$this->successmessage = $success . ' changes saved';
			$pref = new ContactPreferences();
			$if = new InfusionSoftCRM();
			$if->AddOptinTagsToContactFromValue($this->email, $pref->PreferencesValueFromArray($_POST['contactpref_all']));
		}
	} // end of fn SaveCPRefChangesAll
	
	protected function SaveCPRefChanges()
	{	
		if (is_array($_POST['type']) && $_POST['type'])
		{	$pref = new ContactPreferences();
			foreach ($_POST['type'] as $key=>$type)
			{	if ($id = (int)$_POST['cpid'][$key])
				{	$pref_value = $pref->PreferencesValueFromArray($_POST['contactpref'][$key]);
					switch ($type)
					{	case 'donation':
							$sql = 'UPDATE donations SET contactpref=' . $pref_value . ' WHERE did=' . $id;
							break;
						case 'crdonation':
							$sql = 'UPDATE campaigndonations SET contactpref=' . $pref_value . ' WHERE cdid=' . $id;
							break;
						case 'campaignowner':
							$sql = 'UPDATE campaignusers SET contactpref=' . $pref_value . ' WHERE cuid=' . $id;
							break;
					}
					if ($sql)
					{	if ($result = $this->db->Query($sql))
						{	if ($this->db->AffectedRows())
							{	$this->successmessage = 'changes saved';
							}
						}
					}
				}
			}
		}
	} // end of fn SaveCPRefChanges
	
} // end of defn EmailCRMViewPage

$page = new EmailCRMViewPage();
$page->Page();
?>