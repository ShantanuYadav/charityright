<?php
include_once('sitedef.php');

class CountryGatewaysAjax extends CountriesPage
{
	
	public function CountriesLoggedInConstruct()
	{	parent::CountriesLoggedInConstruct();
		switch ($_GET['action'])
		{	case 'popup':
				echo $this->GatewaysList();
				break;
			case 'table':
				echo $this->country->PaymentGatewayTable();
				break;
			case 'add':
				$this->country->AddPaymentGateway($_GET['pgid']);
				break;
			case 'remove':
				$this->country->RemovePaymentGateway($_GET['pgid']);
				break;
		}
	} // end of fn CountriesLoggedInConstruct
	
	public function GatewaysList()
	{	ob_start();
		if ($gateways = $this->PaymentGatewaysAvailable())
		{	echo '<table>';
			foreach ($gateways as $pgid=>$gateway)
			{	echo '<tr><td>', $this->InputSafeString($gateway['gateway']), '</td><td><a onclick="CtryGatewayAdd(\'', $this->country->code, '\', ', $pgid, ')">add this</a></td></tr>';
			}
			echo '</table>';
		} else
		{	echo '<h3>No more payment gateways available</h3>';
		}
		return ob_get_clean();
	} // end of fn GatewaysList
	
	private function PaymentGatewaysAvailable()
	{	$gateways = $this->country->GetAllPaymentGateways();
		foreach ($this->country->GetPaymentGateways() as $pgid=>$gateway)
		{	unset($gateways[$pgid]);
		}
		return $gateways;
	} // end of fn PaymentGatewaysAvailable
	
} // end of defn CountryGatewaysAjax

$page = new CountryGatewaysAjax();
?>