<?php
include_once('sitedef.php');

class EventDatePage extends AdminEventsPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		$this->menuarea = ($this->eventdate->id ? 'dateedit' : 'dates');
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->breadcrumbs->AddCrumb('eventdates.php?id=' . $this->event->id, 'Dates');
		if ($this->eventdate->id)
		{	ob_start();
			echo $this->InputSafeString($this->eventdate->venue['venuename']), ', ', date('d/m/y', strtotime($this->eventdate->details['starttime'])), ' - ', date('d/m/y', strtotime($this->eventdate->details['endtime']));
			$this->breadcrumbs->AddCrumb('eventdate.php?id=' . $this->eventdate->id, ob_get_clean());
		} else
		{	$this->breadcrumbs->AddCrumb('eventdate.php?eid=' . $this->event->id, 'New date');
		}
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function EventConstructFunctions()
	{	if (isset($_POST['venue']))
		{	$saved = $this->eventdate->Save($_POST, $this->event->id);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->eventdate->Delete())
			{	header('location: eventdates.php?id=' . $this->event->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn EventConstructFunctions
	
	protected function AdminEventsBody()
	{	parent::AdminEventsBody();
		echo $this->eventdate->InputForm($this->event->id);
	} // end of fn AdminEventsBody
	
	protected function AssignEvent()
	{	$this->eventdate = new AdminEventDate($_GET['id']);
		if ($this->eventdate->id)
		{	$this->event = new AdminEvent($this->eventdate->details['eid']);
		} else
		{	$this->event = new AdminEvent($_GET['eid']);
		}
	} // end of fn AssignEvent
	
} // end of defn EventDatePage

$page = new EventDatePage();
$page->Page();
?>