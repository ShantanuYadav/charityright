<?php
include_once("sitedef.php");

class LanguageEditPage extends AdminLanguagePage
{	var $editlang = "";

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function LanguageConstruct()
	{	parent::LanguageConstruct();
		$this->js[] = "adminlang.js";
		$this->editlang = new AdminLanguage($_GET["lang"]);

		if (isset($_POST["langname"]))
		{	$saved = $this->editlang->Save($_POST);
			if ($saved["failmessage"])
			{	$this->failmessage = $saved["failmessage"];
			}
			if ($saved["successmessage"])
			{	$this->successmessage = $saved["successmessage"];
			}
			if ($this->successmessage && !$this->failmessage)
			{	header("location: languages.php");
				exit;
			}
		}
		
		$this->breadcrumbs->AddCrumb("langedit.php?lang=" . $this->editlang->code, 
							$this->editlang->code ? $this->InputSafeString($this->editlang->details["langname"]) : "New language");
	
	} // end of fn VideosConstruct
	
	function LanguageContent()
	{	$this->editlang->InputForm();
	} // end of fn LanguageContent
	
} // end of defn LanguageEditPage

$page = new LanguageEditPage();
$page->Page();
?>