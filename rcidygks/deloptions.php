<?php
include_once('sitedef.php');

class DelOptionPage extends AccountsMenuPage
{	var $deloptions;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->deloptions = new AdminDelOptions();
		$this->breadcrumbs->AddCrumb('deloptions.php', 'Delivery options');
	} //  end of fn AccountsLoggedInConstruct
	
	function AccountsBody()
	{	echo '<div><table><tr class="newlink"><th colspan="6"><a href="deloption.php">Add new delivery option</a></th></tr><tr><th>Option Name</th><th>Countries used in</th><th>Default price</th><th>Live?</th><th>Fallback option</th><th>Actions</th></tr>';
		if ($this->deloptions->deloptions)
		{	foreach ($this->deloptions->deloptions as $deloption)
			{	
				echo '<tr class="stripe', $i++ % 2,  '"><td>', $this->InputSafeString($deloption->details['deldesc']), '</td><td>', $deloption->CountryList(), '</td><td>', $deloption->details['delprice'] ? ('&pound;' . number_format($deloption->details['delprice'], 2)) : '', '</td><td>', $deloption->details['live'] ? 'Yes' : '', '</td><td>', $deloption->details['fallback'] ? 'Yes' : '', '</td><td><a href="deloption.php?id=', $deloption->id, '">edit</a>';
				if ($histlink = $this->DisplayHistoryLink('deloptions', $deloption->id))
				{	echo '&nbsp;|&nbsp;', $histlink;
				}
				if ($deloption->CanDelete())
				{	echo '&nbsp;|&nbsp;<a href="deloption.php?id=', $deloption->id, '&delete=1">delete</a>';
				}
				echo '</td></tr>';
			}
		}
		echo '</table></div>';
	} // end of fn AccountsBody
	
} // end of defn DelOptionPage

$page = new DelOptionPage();
$page->Page();
?>