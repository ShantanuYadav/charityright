<?php
include_once('sitedef.php');

class CampaignUserSuspensionPage extends AdminCampaignUsersPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'susp';
		if (isset($_POST['suspflag']))
		{	$saved = $this->campaignuser->Suspend($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('campaignusercampaigns.php', 'Suspension');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->campaignuser->SuspensionForm(), $this->campaignuser->SuspensionHistoryTable();
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignUserSuspensionPage

$page = new CampaignUserSuspensionPage();
$page->Page();
?>