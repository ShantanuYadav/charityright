<?php
include_once('sitedef.php');

class IncomePerMonthPage extends AccountsMenuPage
{	private $stats;

	public function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	public function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		
		$this->stats = new IncomeAnalysis();
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ms = (int)$_GET['mstart']))
		{	$_GET['ystart'] = $ys;
			$_GET['mstart'] = $ms;
		} else
		{	$_GET['ystart'] = date('Y', strtotime('-11 months'));
			$_GET['mstart'] = date('m', strtotime('-11 months'));
		}
		
		if (($ye = (int)$_GET['yend']) && ($me = (int)$_GET['mend']))
		{	$_GET['yend'] = $ye;
			$_GET['mend'] = $me;
		} else
		{	$_GET['yend'] = date('Y');
			$_GET['mend'] = date('m');
		}
		$this->breadcrumbs->AddCrumb('incomepermonth.php', 'Income per month');
		
	} // end of fn AccountsLoggedInConstruct

	public function AccountsBody()
	{	echo $this->FilterForm(), $this->IncomeTable();
	} // end of fn AccountsBody

	private function IncomeTable()
	{	$filter = $_GET;
		if ($months = $this->stats->IncomePerMonth($filter))
		{	//$this->VarDump($months);
			//return;
			echo '<table><tr><th rowspan="2">Month</th><th colspan="3" style="text-align: center;">Main donations</th><th colspan="2" style="text-align: center;">Campaigns</th>',
				//'<th colspan="2" style="text-align: center;">Events</th>',
				'<th rowspan="2">All donations</th></tr>
					<tr><th class="num">Donations</th><th class="num">for projects</th><th class="num">for admin</th><th class="num">Donations</th><th class="num">amount</th>',
				//'<th class="num">Bookings</th><th class="num">amount</th>',
				'</tr>';

			$totals = array('donations_count'=>0, 'donations_amount'=>0, 'donations_adminamount'=>0, 'crstars_count'=>0, 'crstars_amount'=>0, 'events_count'=>0, 'events_amount'=>0);

			foreach ($months as $month_data)
			{	foreach ($month_data as $field=>$value)
				{	if (isset($totals[$field]))
					{	$totals[$field] += $value;
					}
				}
		
				if (!$cursymbol[$donation->details['currency']])
				{	$cursymbol[$donation->details['currency']] = $this->GetCurrency($donation->details['currency'], 'cursymbol');
				}
				echo '<tr class="stripe', $i++ % 2,  '"><td>', $month_data['disp'], '</td><td class="num">', (int)$month_data['donations_count'], '</td><td class="num">&pound;', number_format($month_data['donations_amount'], 2), '</td><td class="num">&pound;', number_format($month_data['donations_adminamount'], 2), '</td><td class="num">', (int)$month_data['crstars_count'], '</td><td class="num">&pound;', number_format($month_data['crstars_amount'], 2), '</td>',
					//'<td class="num">', (int)$month_data['events_count'], '</td><td class="num">&pound;', number_format($month_data['events_amount'], 2), '</td>',
					'<td class="num">&pound;', number_format($month_data['events_amount'] + $month_data['donations_amount'] + $month_data['donations_adminamount'] + $month_data['crstars_amount'], 2), '</td></tr>';
			}
			
			echo '<tr><th>Totals</th><th class="num">', (int)$totals['donations_count'], '</th><th class="num">&pound;', number_format($totals['donations_amount'], 2), '</th><th class="num">&pound;', number_format($totals['donations_adminamount'], 2), '</th><th class="num">', (int)$totals['crstars_count'], '</th><th class="num">&pound;', number_format($totals['crstars_amount'], 2), '</th>',
				//'<th class="num">', (int)$totals['events_count'], '</th><th class="num">&pound;', number_format($totals['events_amount'], 2), '</th>',
				'<th class="num">&pound;', number_format($totals['events_amount'] + $totals['donations_amount'] + $totals['donations_adminamount'] + $totals['crstars_amount'], 2), '</th></tr></table>';
			echo '<p><img src="purchasesgraph.php?mstart=', $_GET['mstart'], '&ystart=', $_GET['ystart'], '&mend=', $_GET['mend'], '&yend=', $_GET['yend'], '" /><br />todays figures are predicted for the end of the month and adjusted by length of month</p>';
		}
	} // end of fn IncomeTable
	
	function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new MonthYearSelect('From', 'start', date('Y-m-d', mktime(0, 0, 0, $_GET['mstart'], 1, $_GET['ystart'])), $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new MonthYearSelect('to', 'end', date('Y-m-d', mktime(0, 0, 0, $_GET['mend'], 1, $_GET['yend'])), $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn IncomePerMonthPage

$page = new IncomePerMonthPage();
$page->Page();
?>