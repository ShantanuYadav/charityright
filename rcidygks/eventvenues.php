<?php
include_once('sitedef.php');

class EventVenuesPage extends AdminEventVenuesPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminEventsBody()
	{	$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="5"><a href="eventvenue.php">Add new venue</a></th></tr><tr><th>Venue</th><th>Country</th><th>Link</th><th>Coming up</th><th>Actions</th></tr>';
		if ($venues = $this->GetVenues())
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($venues as $venue_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$venue = new AdminEventVenue($venue_row);
					echo '<tr class="stripe', $i++ % 2,  '"><td>', $this->InputSafeString($venue->details['venuename']), '</td><td>', $this->InputSafeString($this->GetCountry($venue->details['country'])), '</td><td>', $venue->Link(), '</td><td class="num">', count($venue->GetDates(array('live'=>true))), '</td><td><a href="eventvenue.php?id=', $venue->id, '">edit</a>';
					if ($venue->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="eventvenue.php?id=', $venue->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($venues) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($venues), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
		
	} // end of fn AdminEventsBody
	
	private function GetVenues()
	{	$tables = array('eventvenues'=>'eventvenues');
		$fields = array('eventvenues.*');
		$where = array();
		$orderby = array('eventvenues.venuename ASC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'evid');
	} // end of fn GetVenues
	
} // end of defn EventVenuesPage

$page = new EventVenuesPage();
$page->Page();
?>