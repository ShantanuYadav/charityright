<?php
include_once('sitedef.php');

class DonationDDConfirmPage extends AdminDonationsPage
{

	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		$this->menuarea = 'ddconfirm';
		
		if (isset($_POST['ezc_CustomerID']))
		{	$saved = $this->donation->ManualConfirmDirectDebit($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
		
		if (!$this->donation->IsMonthlyUnconfirmed())
		{	header('location: donation.php?id=' . $this->donation->id);
			exit;
		}
		
	} // end of fn AdminDonationsLoggedInConstruct

	function AdminDonationsBody()
	{	parent::AdminDonationsBody();
		echo $this->donation->ManualDDConfirmForm($_POST);
	} // end of fn AdminDonationsBody
	
} // end of defn DonationDDConfirmPage

$page = new DonationDDConfirmPage();
$page->Page();
?>