<?php
include_once('sitedef.php');

class DonationPaymentsPage extends AdminDonationsPage
{	private $nextPaymentDate = '';

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		$this->menuarea = 'payments';
		
		if ($_GET['takepayment'])
		{	$result = $this->donation->RequestDDPayment();
			$this->successmessage = $result['successmessage'];
			$this->failmessage = $result['failmessage'];
		}
		$this->SetNextPaymentDate();
		
		$this->breadcrumbs->AddCrumb('donationpayments.php?id=' . $this->donation->id, 'payments');
	} // end of fn AdminDonationsLoggedInConstruct

	function SetNextPaymentDate()
	{	$this->nextPaymentDate = $this->donation->NextMonthlyPaymentDate();
	} // end of fn AdminDonationsLoggedInConstruct

	function AdminDonationsBody()
	{	parent::AdminDonationsBody();
	/*	if ($this->nextPaymentDate = $this->donation->NextMonthlyPaymentDate())
		{	$today = $this->datefn->SQLDate();//strtotime('+1 month'));
			echo '<p class="nextPayment', ($overdue = $today > $this->nextPaymentDate) ? ' nextPaymentOverdue' : '', '">Next payment due ', date('j M Y', strtotime($this->nextPaymentDate));
			if ($overdue)
			{	echo ' <a href="donationpayments.php?id=', $this->donation->id, '&takepayment=1">Take payment now</a>';
			}
			echo '</p>';
		}*/
		if ($this->donation->payments)
		{	$cursymbol = $this->GetCurrency($this->donation->details['currency'], 'cursymbol');
			echo '<table><tr><th>Paid</th><th>Reference</th><th class="num">Amount</th><th class="num">GBP Amount</th><th class="num">Admin amount</th><th class="num">Admin GBP amount</th></tr>';
			foreach ($this->donation->payments as $payment)
			{	echo '<tr><td>', date('d/m/y @H:i', strtotime($payment['paydate'])), '</td><td>', $payment['paymentref'], '</td><td class="num">', $currency = $this->GetCurrency($payment['currency'], 'cursymbol'), number_format($payment['amount'], 2), '</td><td class="num">&pound;', number_format($payment['gbpamount'], 2), '</td><td class="num">', $currency, number_format($payment['adminamount'], 2), '</td><td class="num">&pound;', number_format($payment['gbpadminamount'], 2), '</td></tr>';
			}
			echo '</table>';
		}
		//$this->VarDump($this->donation->payments);
	} // end of fn AdminDonationsBody
	
} // end of defn DonationPaymentsPage

$page = new DonationPaymentsPage();
$page->Page();
?>