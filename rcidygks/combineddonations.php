<?php
include_once('sitedef.php');

class DonationsListPage extends AdminDonationsPage
{	var $startdate = '';
	var $enddate = '';
	private $admindonations;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->admindonations = new AdminDonations();
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		$this->breadcrumbs->AddCrumb('combineddonations.php', 'Combined donations');
	} // end of fn AdminDonationsLoggedInConstruct

	function AdminDonationsBody()
	{	echo $this->FilterForm(), $this->ListDonations();
	} // end of fn AdminDonationsBody

	private function ListDonations()
	{	$filter = $_GET;
		$filter['startdate'] = $this->startdate;
		$filter['enddate'] = $this->enddate;
		if ($donations = $this->admindonations->GetCombinedDonations($filter))
		{	$perpage = 20;
			//$this->VarDump($donations);
			echo '<table><tr><th class="num">ID</th><th>Payment ref</th><th>Status</th><th>Donation date</th><th>For</th><th>By</th><th>Type</th><th class="num">Amount</th><th class="num">Admin included</th><th class="num">Paid</th><th class="num">Paid GBP</th>',
					//'<th class="num">Admin included in paid</th>',
					'<th class="num">Admin included GBP</th><th>Info</th><th>Actions</th></tr>';
			if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			$cursymbol = array();
			$countries = $this->donation->GetDonationCountries();
			$totals = array('gbpamount'=>0, 'gbpadminamount'=>0);
			$campaigns = array();
			$can_campaigns = $this->user->CanUserAccess('campaigns');

			foreach ($donations as $donation_row)
			{	switch ($donation_row['table'])
				{	case 'donations':
						$donation = new AdminDonation($donation_row);
						$paid = $donation->SumPaid();
						$totals['gbpamount'] += $paid['gbpamount'] + $paid['gbpadminamount'];
						$totals['gbpadminamount'] += $paid['gbpadminamount'];
						if (++$count > $start)
						{	if ($count <= $end)
							{	$extras = array();
								if ($donation->details['zakat'])
								{	$extras[] = 'Zakat';
								}
								if ($donation->details['giftaid'])
								{	$extras[] = 'Giftaid';
								}
							
								if (!$cursymbol[$donation->details['currency']])
								{	$cursymbol[$donation->details['currency']] = $this->GetCurrency($donation->details['currency'], 'cursymbol');
								}
								echo '<tr class="stripe', $i++ % 2,  '"><td>', $donation->ID(), '</td><td>', $this->InputSafeString($donation->details['donationref']), '</td><td>', $donation->Status(), '</td><td>', date('d/m/y @H:i', strtotime($donation->details['created'])), '</td><td>', $donation->GatewayTitleInner(), '</td><td>', $this->InputSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname'])), '</td><td>', $donation->details['donationtype'], '</td><td class="num">', $cursymbol[$donation->details['currency']], number_format($donation->details['amount'] + $donation->details['adminamount'], 2), '</td><td class="num">', $donation->details['adminamount'] > 0 ? ($cursymbol[$donation->details['currency']] . number_format($donation->details['adminamount'], 2)) : '---', '</td><td class="num">', $paid['amount'] ? ($cursymbol[$donation->details['currency']] . number_format($paid['amount'] + $paid['adminamount'], 2)) : '---', '</td>',
									'<td class="num">', $paid['gbpamount'] ? ('&pound;' . number_format($paid['gbpamount'] + $paid['gbpadminamount'], 2)) : '---', '</td>',
									//'<td class="num">', $paid['adminamount'] ? ($cursymbol[$donation->details['currency']] . number_format($paid['adminamount'], 2)) : '---', '</td>',
									'<td class="num">', $paid['gbpadminamount'] ? ('&pound;' . number_format($paid['gbpadminamount'], 2)) : '---', '</td><td>', implode('<br />', $extras), '</td><td><a href="donation.php?id=', $donation->id, '">view</a></td></tr>';
							}
						}
						break;
					case 'campaigndonations':
						$donation = new CampaignDonation($donation_row);
						if ($donation->details['donationref'])
						{	$totals['gbpamount'] += $donation->details['gbpamount'];
						}
						if (++$count > $start)
						{	if ($count <= $end)
							{	$extras = array();
								if ($donation->details['zakat'])
								{	$extras[] = 'Zakat';
								}
								if ($donation->details['giftaid'])
								{	$extras[] = 'Giftaid';
								}
							
								if (!$cursymbol[$donation->details['currency']])
								{	$cursymbol[$donation->details['currency']] = $this->GetCurrency($donation->details['currency'], 'cursymbol');
								}
								if ($donation->details['teamid'] && !$campaigns[$donation->details['teamid']])
								{	$campaigns[$donation->details['teamid']] = new AdminCampaign($donation->details['teamid']);
								}
								if ($donation->details['campaignid'] && !$campaigns[$donation->details['campaignid']])
								{	$campaigns[$donation->details['campaignid']] = new AdminCampaign($donation->details['campaignid']);
								}
								echo '<tr class="stripe', $i++ % 2,  '"><td>', $donation->ID(), '</td><td>', $this->InputSafeString($donation->details['donationref']), '</td><td>', $donation->Status(), '</td><td>', date('d/m/y @H:i', strtotime($donation->details['donated'])), '</td><td><a href="campaign.php?id=', $campaigns[$donation->details['campaignid']]->id, '">', strip_tags($campaigns[$donation->details['campaignid']]->FullTitle()), '</a>';
								if ($donation->details['teamid'])
								{	echo '<br />Team: <a href="campaign.php?id=', $campaigns[$donation->details['teamid']]->id, '">', strip_tags($campaigns[$donation->details['teamid']]->FullTitle()), '</a>';
								}
								echo '</td>
									<td>', $this->InputSafeString(trim($donation->details['donortitle'] . ' ' . $donation->details['donorfirstname'] . ' ' . $donation->details['donorsurname'])), '</td>
									<td>cr stars</td>
									<td class="num">', $cursymbol[$donation->details['currency']], number_format($donation->details['amount'], 2), '</td>
									<td class="num">---</td>
									<td class="num">', $donation->details['donationref'] ? ($cursymbol[$donation->details['currency']] . number_format($donation->details['amount'], 2)) : '---', '</td><td class="num">', $donation->details['donationref'] ? ('&pound;' . number_format($donation->details['gbpamount'], 2)) : '---', '</td>',
									//'<td class="num"></td>',
									'<td class="num">---</td><td>', implode('<br />', $extras), '</td><td>';
								if ($can_campaigns)
								{	echo '<a href="campaigndonation.php?id=', $donation->id, '">view</a>';
								}
								echo '</td></tr>';
							}
						}
						break;
				}
			}
			$get = array();
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
			}
			$csv_get = $get;
			$csv_get[] = 'startdate=' . $this->startdate;
			$csv_get[] = 'enddate=' . $this->enddate;

			echo '<tr><th colspan="10">Totals (for whole selection)</th><th class="num">&pound;', number_format($totals['gbpamount'], 2), '</th>',
					//'<th></th>',
					'<th class="num">&pound;', number_format($totals['gbpadminamount'], 2), '</th><th colspan="2"></th></tr></table><p class="adminCSVDownload"><a href="combineddonations_csv.php?', implode('&', $csv_get) , '">Download CSV (for whole selection)</a></p>';
			if (count($donations) > $perpage)
			{	$pagelink = $_SERVER['SCRIPT_NAME'];
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
					if ($get)
					{	$pagelink .= '?' . implode('&', $get);
					}
				}
				$pag = new Pagination($_GET['page'], count($donations), $perpage, $pagelink, array(), 'page');
				echo '<div class="pagination">', $pag->Display(), '</div>';
			}			
				
		} else
		{	echo '<h4>No donations for this selection</h4>';
		}
	} // end of fn ListDonations
	
	function FilterForm()
	{	ob_start();
		class_exists('Form');
		$don = new Donation();
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<span>show unpaid</span><input type="checkbox" name="showunpaid" value="1"', $_GET['showunpaid'] ? ' checked="checked"' : '', ' /><input type="submit" class="submit" value="Get" /><div class="clear"></div><span>Type of donation</span><select name="dontype"><option value="">-- all --</option>';
		$dontypes = $don->GetDonationTypes();
		$dontypes['crstars'] = array('label'=>'CR Stars');
		foreach ($dontypes as $key=>$dontype)
		{	echo '<option value="', $key, '"', $key == $_GET['dontype'] ? ' selected="selected"' : '', '>', $dontype['label'], '</option>';
		}
		echo '</select><span>zakat only</span><input type="checkbox" name="zakat" value="1"', $_GET['zakat'] ? ' checked="checked"' : '', ' /><span>giftaid only</span><input type="checkbox" name="giftaid" value="1"', $_GET['giftaid'] ? ' checked="checked"' : '', ' /><span>with admin only</span><input type="checkbox" name="admin" value="1"', $_GET['admin'] ? ' checked="checked"' : '', ' /><span>offline only</span><input type="checkbox" name="offline" value="1"', $_GET['offline'] ? ' checked="checked"' : '', ' /><div class="clear"></div><span>sort by</span><select name="orderby">';
		foreach ($this->admindonations->sortOptions as $key=>$value)
		{	echo '<option value="', $key, '"', $key == $_GET['orderby'] ? ' selected="selected"' : '', '>', $value, '</option>';
		}
		echo '</select><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn DonationsListPage

$page = new DonationsListPage();
$page->Page();
?>