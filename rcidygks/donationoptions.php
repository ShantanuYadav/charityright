<?php
include_once('sitedef.php');

class DonationOptionsPage extends AdminDonationOptionsPage
{	

	protected function AdminDonationOptionsBody()
	{	parent::AdminDonationOptionsBody();
		echo '<table id="pagelist"><tr class="newlink"><th colspan="7"><a href="donationoption.php">new donation option</a></th></tr><tr><th>Display name</th><th>Projects</th><th>Status</th><th class="num">Donations</th><th class="num">Campaigns</th><th class="num">List order</th><th>Actions</th></tr>';
		if ($countries = $this->GetDonationCountries())
		{	foreach($countries as $country_row)
			{	$country = new AdminDonationOption($country_row);
				$status = array();
				if ($country->details['oneoff'])
				{	$status[] = 'Used for one-off donations';
				}
				if ($country->details['monthly'])
				{	$status[] = 'Used for monthly (DD) donations';
				}
				if ($country->details['crstars'])
				{	$status[] = 'Used for CR Stars campaigns';
				}
				if (!$country->details['oneoff'] && !$country->details['monthly'] && !$country->details['crstars'])
				{	$status[] = 'Not used for anything - NOT LIVE';
				}
				if (!$pcountlive = count($country->LiveProjects()))
				{	$status[] = 'No live projects';
				}
				if ($country->details['nozakat'])
				{	$status[] = 'Zakat not allowed';
				}
				echo '<tr><td>', $this->InputSafeString($country->details['shortname']), '</td><td>';
				if ($pcount = count($country->projects))
				{	echo $pcount, ' (', $pcountlive, ' live)';
				} else
				{	echo 'None';
				}
				echo '</td><td>', implode('<br />', $status), '</td><td class="num">', count($country->GetDonations()), '</td><td class="num">', count($country->GetCampaigns()), '</td><td class="num">', (int)$country->details['listorder'], '</td><td><a href="donationoption.php?id=', $country->id, '">edit</a>';
				if ($country->CanDelete())
				{	echo '&nbsp;|&nbsp;<a href="donationoption.php?id=', $country->id, '&delete=1">delete</a>';
				}
				echo '</td></tr>';
			}
		}
		echo '</table>';
	} // end of fn AdminDonationOptionsBody

	private function GetDonationCountries()
	{	$tables = array('donation_countries'=>'donation_countries');
		$fields = array('donation_countries.*');
		$where = array();
		$orderby = array('donation_countries.listorder', 'donation_countries.dcid');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'dcid', true);
	} // end of fn GetDonationCountries
	
} // end of defn DonationOptionsPage

$page = new DonationOptionsPage();
$page->Page();
?>