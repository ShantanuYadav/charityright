<?php
include_once('sitedef.php');

class TicketTypeBookingsCSV extends AdminEventsPage
{
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		$this->tickettype->BookingsCSVOuput();
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function AssignEvent()
	{	$this->tickettype = new AdminTicketType($_GET['id']);
		$this->eventdate = new AdminEventDate($this->tickettype->details['edid']);
		$this->event = new AdminEvent($this->eventdate->details['eid']);
	} // end of fn AssignEvent
	
} // end of defn TicketTypeBookingsCSV

$page = new TicketTypeBookingsCSV();
?>