<?php
include_once('sitedef.php');

class CampaignDonationsPage extends AdminCampaignsPage
{	private $cdonation;
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'donations';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb($cdlink = 'campaigndonations.php?id=' . $this->campaign->id, 'Donations');
		$this->breadcrumbs->AddCrumb('campaigndonation_add.php?id=' . $this->campaign->id, 'Add offline donation');
		$this->cdonation = new AdminCampaignDonation();
			
		if (isset($_POST['currency']))
		{	$saved = $this->cdonation->CreateManual($_POST, $this->campaign);
			if ($this->cdonation->id)
			{	header('location: ' . $cdlink);
				exit;
			} else
			{	$this->failmessage = $saved['failmessage'];
			}
		}
		
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->cdonation->InputForm($this->campaign);
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignDonationsPage

$page = new CampaignDonationsPage();
$page->Page();
?>