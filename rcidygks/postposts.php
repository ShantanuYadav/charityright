<?php
include_once('sitedef.php');

class PostPostsPage extends AdminPostsPage
{	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'posts';
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		if ($this->post->id)
		{	$this->breadcrumbs->AddCrumb('postposts.php', 'Featured posts');
		}
		
		if (is_array($_POST['listorder']))
		{	$saved = $this->post->SavePostsListOrder($_POST['listorder']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->post->PostsTableContainer();
	} // end of fn AdminPostsBody
	
} // end of defn PostPostsPage

$page = new PostPostsPage();
$page->Page();
?>