<?php
include_once('sitedef.php');

class EmailSearchPage extends AdminCRMPage
{	private $startdate = '';
	private $enddate = '';
	private $perpage = 30;
	
	protected function AdminCRMLoggedInConstruct()
	{	parent::AdminCRMLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		}
		
		if (isset($_GET['email']))
		{	if (!$_GET['email'])
			{	$this->failmessage = 'Email missing';
			}
		}
	} // end of fn AdminCRMLoggedInConstruct
	
	protected function AdminCRMBodyMain()
	{	echo $this->FilterForm();
		if ($_GET['email'])
		{	$filter = $_GET;
			$filter['startdate'] = $this->startdate;
			$filter['enddate'] = $this->enddate;
			$search = new CRMSearch();
			if ($results = $search->EmailSearch($filter))
			{	if ($_GET['page'] > 1)
				{	$start = ($_GET['page'] - 1) * $this->perpage;
				} else
				{	$start = 0;
				}
				$end = $start + $this->perpage;
			
				echo '<table><tr><th>Email</th><th>Dates</th><th class="num">Main donations (paid)</th><th class="num">CR Stars donations (paid)</th><th>CR Stars campaign owner</th><th>Actions</th></tr>';
				foreach ($results as $row)
				{	if (++$count > $start)
					{	if ($count <= $end)
						{	echo '<tr><td>', $row['email'], '</td><td>', date('d M y', strtotime($row['first']));
							if (substr($row['first'], 0, 10) != substr($row['last'], 0, 10))
							{	echo ' to ', date('d M y', strtotime($row['last']));
							}
							echo '</td><td class="num">', (int)$row['donations'], ' (', (int)$row['donations_paid'], ')</td><td class="num">', (int)$row['crdonations'], ' (', (int)$row['crdonations_paid'], ')</td><td>', $row['crcampaignsuser'] ? 'Yes' : '', '</td><td><a href="crm_email.php?email=', $row['email'], '">view</a></td></tr>';
						}
					}
				}
				echo '</table>';
				if (count($results) > $this->perpage)
				{	$pagelink = $_SERVER['SCRIPT_NAME'];
					if ($_GET)
					{	$get = array();
						foreach ($_GET as $key=>$value)
						{	if ($value && ($key != 'page'))
							{	$get[] = $key . '=' . $value;
							}
						}
						if ($get)
						{	$pagelink .= '?' . implode('&', $get);
						}
					}
					$pag = new Pagination($_GET['page'], count($results), $this->perpage, $pagelink, array(), 'page');
					echo '<div class="pagination">', $pag->Display(), '</div>';
				}
				//$this->VarDump($results);
			} else
			{	echo '<h3>No results found</h3>';
			}
		}
	} // end of fn AdminCRMBodyMain
	
	protected function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get">';
		echo '<span>Recorded from</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<input type="submit" class="submit" value="Get" /><div class="clear"></div><span>Email</span><input type="text" name="email" value="', $this->InputSafeString($_GET['email']), '" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn EmailSearchPage

$page = new EmailSearchPage();
$page->Page();
?>