<?php
include_once('sitedef.php');

class CampaignUserCampaignsCSV extends AdminCampaignUsersPage
{	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$csv = new CSVReport();
		$this->AddCampaignsHeaderToCSV($csv);
		$this->AddCampaignsRowsToCSV($csv, $this->campaignuser->GetCampaigns());
		$csv->Output();
	} // end of fn AdminCampaignsLoggedInConstruct
	
} // end of defn CampaignUserCampaignsCSV

$page = new CampaignUserCampaignsCSV();
?>