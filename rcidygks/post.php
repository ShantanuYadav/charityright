<?php
include_once('sitedef.php');

class PostPage extends AdminPostsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PostConstructFunctions()
	{	$this->js[] = 'tiny_mce/jquery.tinymce.js';
		$this->js[] = 'pageedit_tiny_mce.js';
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->menuarea = 'edit';
		if (isset($_POST['posttitle']))
		{	$saved = $this->post->Save($_POST, $_FILES['imagefile']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->post->Delete())
			{	header('location: posts.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		if (!$this->post->id)
		{	$this->breadcrumbs->AddCrumb('post.php', 'New post');
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->post->InputForm();
	} // end of fn AdminPostsBody
	
} // end of defn PostPage

$page = new PostPage();
$page->Page();
?>