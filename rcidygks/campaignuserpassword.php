<?php
include_once('sitedef.php');

class CampaignUserPasswordPage extends AdminCampaignUsersPage
{	
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'password';
		if (isset($_POST['pword']))
		{	$saved = $this->campaignuser->ChangePassword($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('campaignuserpassword.php', 'Change password');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->campaignuser->PasswordForm();
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignUserPasswordPage

$page = new CampaignUserPasswordPage();
$page->Page();
?>
