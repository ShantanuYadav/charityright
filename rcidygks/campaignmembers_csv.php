<?php
include_once('sitedef.php');

class CampaignMembersCSV extends AdminCampaignsPage
{	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$csv = new CSVReport();
		$this->AddCampaignsHeaderToCSV($csv);
		$this->AddCampaignsRowsToCSV($csv, $this->campaign->team_members);
		$csv->Output();
	} // end of fn AdminCampaignsLoggedInConstruct
	
} // end of defn CampaignMembersCSV

$page = new CampaignMembersCSV();
?>