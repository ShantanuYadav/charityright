<?php
include_once('sitedef.php');

class TicketTypesPage extends AdminEventsPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		$this->menuarea = 'tickettypes';
		$this->breadcrumbs->AddCrumb('eventdates.php?id=' . $this->event->id, 'Dates');
		ob_start();
		echo $this->InputSafeString($this->eventdate->venue['venuename']), ', ', date('d/m/y', strtotime($this->eventdate->details['starttime'])), ' - ', date('d/m/y', strtotime($this->eventdate->details['endtime']));
		$this->breadcrumbs->AddCrumb('eventdate.php?id=' . $this->eventdate->id, ob_get_clean());
		$this->breadcrumbs->AddCrumb('tickettypes.php?id=' . $this->eventdate->id, 'Ticket types');
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function AdminEventsBody()
	{	parent::AdminEventsBody();
		echo $this->eventdate->TicketTypesTable();
	} // end of fn AdminEventsBody
	
	protected function AssignEvent()
	{	$this->eventdate = new AdminEventDate($_GET['id']);
		$this->event = new AdminEvent($this->eventdate->details['eid']);
	} // end of fn AssignEvent
	
} // end of defn TicketTypesPage

$page = new TicketTypesPage();
$page->Page();
?>