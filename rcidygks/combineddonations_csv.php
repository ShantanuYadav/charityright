<?php
include_once('sitedef.php');

class DonationsListCSV extends AdminDonationsPage
{
	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		$csv = new CSVReport();
		$csv->AddToHeaders(array('id', 'gateway ref.', 'status', 'donation date', 'for', 'donation cause', 'donation project', 'donation sub project', 'donation quantity', 'donor title', 'donor first name', 'donor last name', 'donor address', 'donor city/town', 'donor postcode', 'donor country', 'donor email', 'donor phone', 'donation type', 'currency', 'amount', 'admin amount included', 'paid', 'paid GBP', 
				//'admin included in paid', 
				'admin included in paid GBP', 'giftaid', 'zakat', 'utm codes', 'comments (CR Stars only)'));
		
		$admindonations = new AdminDonations();
		if ($donations = $admindonations->GetCombinedDonations($_GET))
		{	$countries = $this->donation->GetDonationCountries();
			$campaigns = array();

			foreach ($donations as $donation_row)
			{	$utm_lines = array();
				switch ($donation_row['table'])
				{	case 'donations':
						$donation = new AdminDonation($donation_row);
						$paid = $donation->SumPaid();
						$address = array();
						if ($donation->details['donoradd1'])
						{	$address[] = $this->CSVSafeString($donation->details['donoradd1']);
						}
						if ($donation->details['donoradd2'])
						{	$address[] = $this->CSVSafeString($donation->details['donoradd2']);
						}
						if ($donation->extrafields)
						{	foreach($donation->extrafields as $key=>$value)
							{	if (substr($key, 0, 4) == 'utm_')
								{	$utm_lines[] = $key . '=' . $value;
								}
							}
						}
						$row = array('"' . $donation->ID() . '"', '"' . $this->CSVSafeString($donation->details['donationref']) . '"', 
								'"' . $donation->Status() . '"', 
								'"' . date('d/m/Y @H:i', strtotime($donation->details['created'])) . '"', 
								'"' . $this->CSVSafeString($donation->GatewayTitleInner()). '"', 
								'"' . $this->CSVSafeString($countries[$donation->details['donationcountry']]['shortname']). '"', 
								'"' . $this->CSVSafeString($countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projectname']). '"', 
								'"' . $this->CSVSafeString($countries[$donation->details['donationcountry']]['projects'][$donation->details['donationproject']]['projects'][$donation->details['donationproject2']]['projectname']). '"', 
								(int)$donation->details['quantity'],
								'"' . $this->CSVSafeString($donation->details['donortitle']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorfirstname']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorsurname']) . '"', 
								'"' . implode(', ', $address) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorcity']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorpostcode']) . '"', 
								'"' . $this->CSVSafeString($this->GetCountry($donation->details['donorcountry'])) . '"', 
								'"' . $this->CSVSafeString($donation->details['donoremail']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorphone']) . '"', 
								'"' . $donation->details['donationtype'] . '"', 
								'"' . $donation->details['currency'] . '"', 
								number_format($donation->details['amount'] + $donation->details['adminamount'], 2, '.', ''), 
								number_format($donation->details['adminamount'], 2, '.', ''), 
								number_format($paid['amount'], 2, '.', ''), 
								number_format($paid['gbpamount'] + $paid['gbpadminamount'], 2, '.', ''), 
								//number_format($paid['adminamount'], 2, '.', ''), 
								number_format($paid['gbpadminamount'], 2, '.', ''), 
								$donation->details['giftaid'] ? '1' : '0', 
								$donation->details['zakat'] ? '1' : '0',
								'"' . implode("\n", $utm_lines) . '",""');
						$csv->AddToRows($row);
						break;
					case 'campaigndonations':
						$donation = new CampaignDonation($donation_row);
						if ($donation->details['teamid'] && !$campaigns[$donation->details['teamid']])
						{	$campaigns[$donation->details['teamid']] = new AdminCampaign($donation->details['teamid']);
						}
						if ($donation->details['campaignid'] && !$campaigns[$donation->details['campaignid']])
						{	$campaigns[$donation->details['campaignid']] = new AdminCampaign($donation->details['campaignid']);
						}
						$address = array();
						if ($donation->details['donoradd1'])
						{	$address[] = $this->CSVSafeString($donation->details['donoradd1']);
						}
						if ($donation->details['donoradd2'])
						{	$address[] = $this->CSVSafeString($donation->details['donoradd2']);
						}
						if ($donation->extrafields)
						{	foreach($donation->extrafields as $key=>$value)
							{	if (substr($key, 0, 4) == 'utm_')
								{	$utm_lines[] = $key . '=' . $value;
								}
							}
						}
						
						$donationcountry = $donationproject = '';
						if ($campaigns[$donation->details['teamid']])
						{	$donationcountry = $campaigns[$donation->details['teamid']]->details['donationcountry'];
							$donationproject = $campaigns[$donation->details['teamid']]->details['donationproject'];
						} else
						{	if ($campaigns[$donation->details['campaignid']])
							{	$donationcountry = $campaigns[$donation->details['campaignid']]->details['donationcountry'];
								$donationproject = $campaigns[$donation->details['campaignid']]->details['donationproject'];
							}
						}
						
						$row = array('"' . $donation->ID() . '"', '"' . $this->CSVSafeString($donation->details['donationref']) . '"', 
								'"' . $donation->Status() . '"', 
								'"' . date('d/m/Y @H:i', strtotime($donation->details['donated'])) . '"', 
								'"' . strip_tags($campaigns[$donation->details['campaignid']]->FullTitle()) . ($donation->details['campaignid'] ? (', Team: ' . strip_tags($campaigns[$donation->details['campaignid']]->FullTitle())) : '') . '"', 
								'"' . $this->CSVSafeString($countries[$donationcountry]['shortname']). '"', 
								'"' . $this->CSVSafeString($countries[$donationcountry]['projects'][$donationproject]['projectname']). '"', 
								'""', 
								'0', 
								'"' . $this->CSVSafeString($donation->details['donortitle']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorfirstname']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorsurname']) . '"', 
								'"' . implode(', ', $address) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorcity']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorpostcode']) . '"', 
								'"' . $this->CSVSafeString($this->GetCountry($donation->details['donorcountry'])) . '"', 
								'"' . $this->CSVSafeString($donation->details['donoremail']) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorphone']) . '"', 
								'"cr stars"', 
								'"' . $donation->details['currency'] . '"', 
								number_format($donation->details['amount'], 2, '.', ''), 
								'0.00', 
								$donation->details['donationref'] ? number_format($donation->details['amount'], 2, '.', '') : '0', 
								$donation->details['donationref'] ? number_format($donation->details['gbpamount'], 2, '.', '') : '0', 
								//'0.00', 
								'0.00', 
								$donation->details['giftaid'] ? '1' : '0', 
								$donation->details['zakat'] ? '1' : '0',
								'"' . implode("\n", $utm_lines) . '"', 
								'"' . $this->CSVSafeString($donation->details['donorcomment']). '"');
						$csv->AddToRows($row);
						break;
				}
			}
		}
		$csv->Output();
	} // end of fn AdminDonationsLoggedInConstruct
	
} // end of defn DonationsListCSV

$page = new DonationsListCSV();
?>