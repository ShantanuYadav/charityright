<?php
include_once('sitedef.php');

class PostVideosPage extends AdminPostsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'videos';
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		if ($this->post->id)
		{	$this->breadcrumbs->AddCrumb('postvideos.php', 'Videos');
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->post->VideosTableContainer();
	} // end of fn AdminPostsBody
	
} // end of defn PostVideosPage

$page = new PostVideosPage();
$page->Page();
?>