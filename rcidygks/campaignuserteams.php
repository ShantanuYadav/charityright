<?php
include_once('sitedef.php');

class CampaignUserTeamsPage extends AdminCampaignUsersPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'teams';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('campaignuserteams.php', 'Teams joined');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->CampaignsList($campaigns = $this->campaignuser->GetTeams(), 30, false);
		if ($campaigns)
		{	$get = array();
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
			}
			echo '<p class="adminCSVDownload"><a href="campaignuserteams_csv.php?', implode('&', $get) , '">Download CSV (for whole selection)</a></p>';
		}
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignUserTeamsPage

$page = new CampaignUserTeamsPage();
$page->Page();
?>