<?php
include_once('sitedef.php');

class CampaignMovementsCSV extends AdminCampaignsPage
{	
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'movements';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		
		if ($this->user->CanUserAccess('accounts'))
		{	if ($campaigns = $this->campaign->team_members)
			{	$dates = array($_GET['startdate'], $_GET['enddate']);
				$csv = new CSVReport();
				$header = array('campaign id', 'campaign name', 'started', 'days live', 'owner', 'owner email', 'owner phone', 'currency', 'target', ($start = date('d-m-Y', strtotime($_GET['startdate']))) . ' raised', $start . ' % of target', ($end = date('d-m-Y', strtotime($_GET['enddate']))) . ' raised', $end . ' % of target');
				foreach ($header as $column)
				{	$csv->AddToHeaders($column);
				}
				foreach ($campaigns as $campaign_row)
				{	$campaign = new AdminCampaign($campaign_row);
					$raised = $campaign->DonationsToDates(array($_GET['startdate'], $_GET['enddate']));
					$row = array($campaign->id, 
							'"' . $this->CSVSafeString($campaign->details['campname']) . '"', 
							'"' . date('d/m/Y', strtotime($campaign->details['created'])) . '"', 
							$campaign->RunningForDays(), 
							'"' . ($campaign->details['cuid'] ? ($campaign->owner ? $this->CSVSafeString($campaign->owner['firstname'] . ' ' . $campaign->owner['lastname']) : 'owner not found') : 'Charity Right') . '"', 
							'"' . $this->CSVSafeString($campaign->owner['email']) . '"', 
							'"' . $this->CSVSafeString($campaign->owner['phone']) . '"', 
							'"' . $campaign->details['currency'] . '"', 
							number_format($campaign->details['target'], 2, '.', ''), 
							number_format($raised[$_GET['startdate']], 2, '.', ''), 
							number_format((100 * $raised[$_GET['startdate']]) / $campaign->details['target'], 1, '.', ''), 
							number_format($raised[$_GET['enddate']], 2, '.', ''), 
							number_format((100 * $raised[$_GET['enddate']]) / $campaign->details['target'], 1, '.', '')
						);
					$csv->AddToRows($row);
				}
				$csv->Output();
			}
		}
		
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->MovementsFilterForm();
		if ($campaigns = $this->campaign->team_members)
		{	
			echo '<table><tr><th rowspan="2">Campaign</th><th rowspan="2">Started</th><th rowspan="2">Created by</th><th rowspan="2">Target</th><th colspan="4" style="text-align: center;">Raised</th><th rowspan="2">Actions</th></tr><tr><th colspan="2" style="text-align: center;">', date('d/m/y', strtotime($this->startdate)), '</th><th colspan="2" style="text-align: center;">', date('d/m/y', strtotime($this->enddate)), '</th></tr>';
			foreach ($campaigns as $campaign_row)
			{	if (++$count > $start)
				{	if ($perpage && ($count > $end))
					{	break;
					}
					$campaign = new AdminCampaign($campaign_row);
					$raised = $campaign->DonationsToDates(array($this->startdate, $this->enddate));
					echo '<tr><td>', $this->InputSafeString($campaign->details['campname']), '</td><td>', date('d/m/y', strtotime($created_date = substr($campaign->details['created'], 0, 10))), '</td><td>', $this->InputSafeString($campaign->owner['firstname'] . ' ' . $campaign->owner['surname']), '<br /><a href="mailto:', $campaign->owner['email'], '">', $campaign->owner['email'], '</a><br />', $this->InputSafeString($campaign->owner['phone']), '</td><td class="num">', $cursymbol = $this->GetCurrency($campaign->details['currency'], 'cursymbol'), number_format($campaign->details['target'], 2), '</td><td class="num">', ($this->startdate >= $created_date) ? ($cursymbol . number_format($raised[$this->startdate], 2)) : '-----', '</td><td class="num">', ($this->startdate >= $created_date) ? (number_format((100 * $raised[$this->startdate]) / $campaign->details['target'], 1) . '%') : '-----', '</td><td class="num">', ($this->enddate >= $created_date) ? ($cursymbol . number_format($raised[$this->enddate], 2)) : '-----', '</td><td class="num">', ($this->enddate >= $created_date) ? (number_format((100 * $raised[$this->enddate]) / $campaign->details['target'], 1) . '%') : '-----', '</td><td><a href="campaign.php?id=', $campaign->id, '">campaign</a></td></tr>';
				}
			}
			echo '</table>';
		}
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignMovementsCSV

$page = new CampaignMovementsCSV();
?>
