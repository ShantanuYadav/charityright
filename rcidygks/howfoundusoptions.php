<?php
include_once('sitedef.php');

class VatRatesPage extends AdminHowFoundUsPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	private function GetAllOptions()
	{	$options = array();
		$sql = 'SELECT * FROM howfoundus ORDER BY hforder';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$options[$row['hfid']] = $row;
			}
		}
		return $options;
	} // end of fn GetAllOptions
	
	function HowFoundUsLoggedInBody()
	{	
		echo '<table><tr class="newlink"><th colspan="4"><a href="howfoundusoption.php">add new option</a></th></tr><tr><th>Recorded Value</th><th>Description in front end</th><th class="num">List order</th><th>Actions</th></tr>';
		foreach ($this->GetAllOptions() as $option_row)
		{	$option = new AdminHowFoundUsOption($option_row);
			echo '<tr><td>', $this->InputSafeString($option->details['hfvalue']), '</td><td>', $this->InputSafeString($option->details['hfdesc']), '</td><td class="num">', (int)$option->details['hforder'], '</td><td><a href="howfoundusoption.php?id=', $option->id, '">edit</a>';
			if ($option->CanDelete())
			{	echo '&nbsp;|&nbsp;<a href="howfoundusoption.php?id=', $option->id, '&delete=1">delete</a>';
			}
			echo '</td></tr>';
		}
		echo '</table>';
	} // end of fn HowFoundUsLoggedInBody
	
} // end of defn VatRatesPage

$page = new VatRatesPage();
$page->Page();
?>