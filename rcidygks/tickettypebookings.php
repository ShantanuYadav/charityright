<?php
include_once('sitedef.php');

class TicketTypeBookingsPage extends AdminEventsPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		$this->menuarea = 'ttbookings';
		$this->breadcrumbs->AddCrumb('eventdates.php?id=' . $this->event->id, 'Dates');
		ob_start();
		echo $this->InputSafeString($this->eventdate->venue['venuename']), ', ', date('d/m/y', strtotime($this->eventdate->details['starttime'])), ' - ', date('d/m/y', strtotime($this->eventdate->details['endtime']));
		$this->breadcrumbs->AddCrumb('eventdate.php?id=' . $this->eventdate->id, ob_get_clean());
		$this->breadcrumbs->AddCrumb('tickettypes.php?id=' . $this->eventdate->id, 'Ticket types');
		$this->breadcrumbs->AddCrumb('tickettype.php?id=' . $this->tickettype->id, $this->InputSafeString($this->tickettype->details['ttypename']));
		$this->breadcrumbs->AddCrumb('tickettypebookings.php?id=' . $this->tickettype->id, $this->InputSafeString($this->tickettype->details['ttypename']) . ' Bookings');
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function AdminEventsBody()
	{	parent::AdminEventsBody();
		echo $this->tickettype->BookingsTable();
	} // end of fn AdminEventsBody
	
	protected function AssignEvent()
	{	$this->tickettype = new AdminTicketType($_GET['id']);
		$this->eventdate = new AdminEventDate($this->tickettype->details['edid']);
		$this->event = new AdminEvent($this->eventdate->details['eid']);
	} // end of fn AssignEvent
	
} // end of defn TicketTypeBookingsPage

$page = new TicketTypeBookingsPage();
$page->Page();
?>