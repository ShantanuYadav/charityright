<?php
include_once('sitedef.php');

class EventDatesPage extends AdminEventsPage
{	private $startdate = '';
	private $enddate = '';
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';

		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate();
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		}

		$this->menuarea = 'dates';
		$this->breadcrumbs->AddCrumb('eventdates.php', 'Dates');
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function AdminEventsBody()
	{	parent::AdminEventsBody();
		echo $this->FilterForm(), $this->event->DatesTable(array('startdate'=>$this->startdate, 'enddate'=>$this->enddate));
	} // end of fn AdminEventsBody
	
	private function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10), false, false, true);
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10), false, false, true);
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><input type="hidden" name="id" value="', $this->event->id, '" /><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn EventDatesPage

$page = new EventDatesPage();
$page->Page();
?>