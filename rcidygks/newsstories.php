<?php
include_once('sitedef.php');

class NewsStoriesPage extends AdminNewsPage
{	var $stories;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AdminNewsLoggedInConstruct()
	{	parent::AdminNewsLoggedInConstruct();
		$this->stories = new AdminNewsStories();
	} //  end of fn AdminNewsLoggedInConstruct
	
	function AdminNewsBody()
	{	$this->ListStories();
	} // end of fn AdminNewsBody
	
	function ListStories()
	{	
		//$this->InitialiseSlugs();
		
		echo '<div><table><tr class="newlink"><th colspan="5"><a href="newsstory.php">Add new story</a></th></tr><tr><th>Date / Time</th><th>Headline</th><th>Link</th><th>Live?</th><th>Actions</th></tr>';
		if ($this->stories->stories)
		{	foreach ($this->stories->stories as $story)
			{	echo '<tr class="stripe', $i++ % 2, $story->details['live'] ? ' livenews' : '', '"><td>', date('d/m/Y @ H:i', strtotime($story->details['submitted'])), '</td><td>', $this->InputSafeString($story->details['headline']), '</td><td><a href="', $link = $story->Link(), '">', $link, '</a></td><td>', $story->details['live'] ? 'Yes' : 'No', '</td><td><a href="newsstory.php?id=', $story->id, '">edit</a>';
				if ($histlink = $this->DisplayHistoryLink('news', $story->id))
				{	echo '&nbsp;|&nbsp;', $histlink;
				}
				if ($story->CanDelete())
				{	echo '&nbsp;|&nbsp;<a href="newsstory.php?id=', $story->id, '&delete=1">delete</a>';
				}
				echo '</td></tr>';
			}
		}
		echo '</table></div>';
	} // end of fn ListStories
	
	public function InitialiseSlugs()
	{	foreach ($this->stories->stories as $story)
		{	$slug = $this->TextToSlug($story->details['headline']);
			$sql = 'UPDATE news_lang SET slug="' . $slug . '" WHERE lang="en" AND newsid=' . $story->id;
			$this->db->Query($sql);
		}
	} // end of fn InitialiseSlugs
	
} // end of defn NewsStoriesPage

$page = new NewsStoriesPage();
$page->Page();
?>