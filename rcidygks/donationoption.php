<?php
include_once('sitedef.php');

class DonationOptionPage extends AdminDonationOptionsPage
{	
	
	protected function AdminDonationOptionsConstructFunctions()
	{	$this->menuarea = 'edit';
		if (isset($_POST['shortname']))
		{	$saved = $this->donationcountry->Save($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->donationcountry->Delete())
			{	header('location: donationoptions.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn AdminDonationOptionsConstructFunctions
	
	protected function AdminDonationOptionsLoggedInConstruct()
	{	parent::AdminDonationOptionsLoggedInConstruct();
		if (!$this->donationcountry->id)
		{	$this->breadcrumbs->AddCrumb('donationoption.php', 'New option');
		}
	} // end of fn AdminDonationOptionsLoggedInConstruct
	
	protected function AdminDonationOptionsBody()
	{	parent::AdminDonationOptionsBody();
		echo $this->donationcountry->InputForm();
	} // end of fn AdminDonationOptionsBody
	
} // end of defn DonationOptionPage

$page = new DonationOptionPage();
$page->Page();
?>