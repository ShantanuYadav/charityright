<?php
include_once('sitedef.php');

class BookingsCSV extends AdminEventsPage
{
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		echo $this->eventdate->BookingsCSVOuput();
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function AssignEvent()
	{	$this->eventdate = new AdminEventDate($_GET['id']);
		$this->event = new AdminEvent($this->eventdate->details['eid']);
	} // end of fn AssignEvent
	
} // end of defn BookingsCSV

$page = new BookingsCSV();
?>