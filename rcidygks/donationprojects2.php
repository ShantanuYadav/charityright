<?php
include_once('sitedef.php');

class DonationProjectsPage extends AdminDonationOptionsPage
{	
	
	protected function AdminDonationOptionsLoggedInConstruct()
	{	parent::AdminDonationOptionsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('donationprojects2.php?id=' . $this->donationproject->id, 'Sub-options');if ($this->donationproject->id)
		{	$this->menuarea = 'projects2';
		} else
		{	header('location: donationoptions.php');
			exit;
		}
	} // end of fn AdminDonationOptionsLoggedInConstruct
	
	protected function AssignDonationCountry()
	{	$this->donationproject = new AdminDonationProject($_GET['id']);
		if ($this->donationproject->id)
		{	$this->donationcountry = new AdminDonationOption($this->donationproject->details['dcid']);
		} else
		{	$this->donationcountry = new AdminDonationOption($_GET['dcid']);
		}
	} // end of fn AssignDonationCountry

	protected function AdminDonationOptionsBody()
	{	parent::AdminDonationOptionsBody();
		echo '<table id="pagelist"><tr class="newlink"><th colspan="6"><a href="donationproject2.php?dpid=', $this->donationproject->id, '">new sub-option for "', $this->InputSafeString($this->donationproject->details['projectname']), '"</a></th></tr><tr><th>Display name</th><th>Status</th><th class="num">Donations</th><th class="num">Campaigns</th><th class="num">List order</th><th>Actions</th></tr>';
		if ($projects = $this->donationproject->projects)
		{	foreach($projects as $project_row)
			{	$project = new AdminDonationProject2($project_row);
				$status = array();
				if ($project->details['oneoff'])
				{	$status[] = 'Used for one-off donations';
				}
				if ($project->details['monthly'])
				{	$status[] = 'Used for monthly (DD) donations';
				}
				if ($project->details['crstars'])
				{	$status[] = 'Used for CR Stars campaigns';
				}
				if (!$project->details['oneoff'] && !$project->details['monthly'] && !$project->details['crstars'])
				{	$status[] = 'Not used for anything - NOT LIVE';
				}
				if ($project->prices)
				{	$status[] = 'Fixed price option';
				}
				echo '<tr><td>', $this->InputSafeString($project->details['projectname']), '</td><td>', implode('<br />', $status), '</td><td class="num">', count($project->GetDonations()), '</td><td class="num">', count($project->GetCampaigns()), '</td><td class="num">', (int)$project->details['listorder'], '</td><td><a href="donationproject2.php?id=', $project->id, '">edit</a>';
				if ($project->CanDelete())
				{	echo '&nbsp;|&nbsp;<a href="donationproject2.php?id=', $project->id, '&delete=1">delete</a>';
				}
				echo '</td></tr>';
			}
		}
		echo '</table>';
	} // end of fn AdminDonationOptionsBody
	
} // end of defn DonationProjectsPage

$page = new DonationProjectsPage();
$page->Page();
?>