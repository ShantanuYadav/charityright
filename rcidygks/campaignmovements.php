<?php
include_once('sitedef.php');

class CampaignMovementsPage extends AdminCampaignsPage
{	var $startdate = '';
	var $enddate = '';
	private $perpage = 20;
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'movements';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-2 weeks'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}

		$this->breadcrumbs->AddCrumb('campaignmovements.php?id=' . $this->campaign->id, 'Movements report');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		if ($this->user->CanUserAccess('accounts'))
		{	echo $this->MovementsFilterForm();
			if ($campaigns = $this->campaign->team_members)
			{	if ($perpage = (int)$this->perpage)
				{	if ($_GET['page'] > 1)
					{	$start = ($_GET['page'] - 1) * $perpage;
					} else
					{	$start = 0;
					}
					$end = $start + $perpage;
				} else
				{	$start = 0;
				}
				echo '<table><tr><th rowspan="2">Campaign</th><th rowspan="2">Started</th><th rowspan="2">Running for</th><th rowspan="2">Created by</th><th rowspan="2">Target</th><th colspan="4" style="text-align: center;">Raised</th><th rowspan="2">Actions</th></tr><tr><th colspan="2" style="text-align: center;">', date('d/m/y', strtotime($this->startdate)), '</th><th colspan="2" style="text-align: center;">', date('d/m/y', strtotime($this->enddate)), '</th></tr>';
				foreach ($campaigns as $campaign_row)
				{	if (++$count > $start)
					{	if ($perpage && ($count > $end))
						{	break;
						}
						$campaign = new AdminCampaign($campaign_row);
						$raised = $campaign->DonationsToDates(array($this->startdate, $this->enddate));
						echo '<tr><td>', $this->InputSafeString($campaign->details['campname']), '</td><td>', date('d/m/y', strtotime($created_date = substr($campaign->details['created'], 0, 10))), '</td><td>', $campaign->RunningForText(), '</td><td>';
						if ($campaign->details['cuid'])
						{	echo $this->InputSafeString($campaign->owner['firstname'] . ' ' . $campaign->owner['surname']), '<br /><a href="mailto:', $campaign->owner['email'], '">', $campaign->owner['email'], '</a><br />', $this->InputSafeString($campaign->owner['phone']);
						} else
						{	echo 'Charity Right';
						}
						echo '</td><td class="num">', $cursymbol = $this->GetCurrency($campaign->details['currency'], 'cursymbol'), number_format($campaign->details['target'], 2), '</td><td class="num">', ($this->startdate >= $created_date) ? ($cursymbol . number_format($raised[$this->startdate], 2)) : '-----', '</td><td class="num">', ($this->startdate >= $created_date) ? (number_format((100 * $raised[$this->startdate]) / $campaign->details['target'], 1) . '%') : '-----', '</td><td class="num">', ($this->enddate >= $created_date) ? ($cursymbol . number_format($raised[$this->enddate], 2)) : '-----', '</td><td class="num">', ($this->enddate >= $created_date) ? (number_format((100 * $raised[$this->enddate]) / $campaign->details['target'], 1) . '%') : '-----', '</td><td><a href="campaign.php?id=', $campaign->id, '">campaign</a></td></tr>';
					}
				}
				echo '</table>';
				if ($do_pagination = ($perpage && (count($campaigns) > $perpage)))
				{	$pagelink = $_SERVER['SCRIPT_NAME'];
					if ($_GET)
					{	$get = array();
						foreach ($_GET as $key=>$value)
						{	if ($value && ($key != 'page'))
							{	$get[] = $key . '=' . $value;
							}
						}
						if ($get)
						{	$pagelink .= '?' . implode('&', $get);
						}
					}
					$pag = new Pagination($_GET['page'], count($campaigns), $perpage, $pagelink, array(), 'page');
					echo '<div class="pagination">', $pag->Display(), '</div>';
				}
				$get = array();
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
				}
				$get[] = 'startdate=' . $this->startdate;
				$get[] = 'enddate=' . $this->enddate;
				echo '<p class="adminCSVDownload"><a href="campaignmovements_csv.php?', implode('&', $get) , '">Download CSV', $do_pagination ? ' (for all pages)' : '', '</a></p>';
			}
		}
	} // end of fn AdminCampaignBody
	
	protected function MovementsFilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><input type="hidden" name="id" value="', $this->campaign->id, '" />';
		$startfield->OutputField();
		echo '<span>compare to</span>';
		$endfield->OutputField();
		echo '<input type="submit" class="submit" value="Run report" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn MovementsFilterForm
	
} // end of defn CampaignMovementsPage

$page = new CampaignMovementsPage();
$page->Page();
?>