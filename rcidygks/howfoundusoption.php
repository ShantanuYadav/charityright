<?php
include_once('sitedef.php');

class HowFoundUsOptionPage extends AdminHowFoundUsPage
{	protected $option;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function HowFoundUsLoggedInConstructor()
	{	parent::HowFoundUsLoggedInConstructor();
		$this->option = new AdminHowFoundUsOption($_GET['id']);
		
		if (isset($_POST['hfvalue']))
		{	$saved = $this->option->Save($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
		
		if ($_GET['confirm'] && $_GET['delete'])
		{	if ($this->option->Delete())
			{	header('location: howfoundusoptions.php');
				exit;
			} else
			{	$this->failmessage = 'Delete failed';
			}
		}
		$this->breadcrumbs->AddCrumb('howfoundusoption.php?id=' . $this->option->id, $this->option->id ? $this->InputSafeString($this->option->details['hfvalue']) : 'new option');
		
	} // end of fn HowFoundUsLoggedInConstructorv
	
	function HowFoundUsLoggedInBody()
	{	parent::HowFoundUsLoggedInBody();
		echo $this->option->InputForm();
	} // end of fn HowFoundUsLoggedInBody
	
} // end of defn HowFoundUsOptionPage

$page = new HowFoundUsOptionPage();
$page->Page();
?>