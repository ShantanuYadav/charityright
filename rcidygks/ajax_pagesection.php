<?php
include_once('sitedef.php');

class PageSectionsAjax extends AdminPagesPage
{
	function AdminPagesLoggedInConstruct()
	{	parent::AdminPagesLoggedInConstruct();
		
		switch ($_GET['action'])
		{	case 'psclass_options':
				$return = array('values'=>array(), 'keys'=>array());
				$pagesection = new AdminPageSection();
				if (is_array($class = $pagesection->ClassFromClassName($_GET['psclass'])))
				{	foreach ($class as $key=>$value)
					{	if (substr($key, 0, 4) == 'can_')
						{	$return['values'][] = $value;
							$return['keys'][] = substr($key, 4);
						}
					}
				}
				echo json_encode($return);
				break;
		}
		
	} // end of fn AdminPagesLoggedInConstruct
	
} // end of defn PageSectionsAjax

$page = new PageSectionsAjax();
?>