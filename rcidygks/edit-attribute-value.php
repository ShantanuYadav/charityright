<?php
include_once('sitedef.php');

class PBEditAttributeValuePage extends AdminPlateBuilderPage
{	var $value;
	var $attribute;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function PBLoggedInConstruct()
	{	parent::PBLoggedInConstruct();
		$this->value = AdminCategoryFactory::createAttributeValue($_GET['id']);
				
		// Handle form update
		if(isset($_POST['edit_submit']) && isset($_POST['attribute']))
		{	
			if($this->value->getAttribute()->saveEditAttributeValueForm($_POST['attribute']))
			{
				header('location: attribute.php?id='. $this->value->attribute .'');
				exit;
			}
		}
		
		if ($this->attribute = $this->value->getAttribute())
		{	$this->breadcrumbs->AddCrumb('', $this->attribute->getCategory()->display_name);
			$this->breadcrumbs->AddCrumb('attribute.php?id=' . $this->attribute->id, $this->attribute->display_name . ' - edit');
		}
		$this->breadcrumbs->AddCrumb('edit-attribute-value.php?id=' . $this->value->id, $this->value->display_value);
	} // end of fn PBLoggedInConstruct
	
	function PlateBuilderBody()
	{	if ($this->attribute)
		{	echo '<div id="new-attribute-value">', $this->attribute->outputEditAttributeValueForm($value->id), '</div>';
		}
	} // end of fn PlateBuilderBody
	
} // end of defn PBEditAttributeValuePage

$page = new PBEditAttributeValuePage();
$page->Page();
?>