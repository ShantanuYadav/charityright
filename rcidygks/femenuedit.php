<?php
include_once('sitedef.php');

class FEMenuEditPage extends AdminFrontEndMenuPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function CMSLoggedInConstruct($page_option = '')
	{	parent::CMSLoggedInConstruct('edit');
	} //  end of fn __construct

	function ConstructFunctions()
	{	parent::ConstructFunctions();
		
		if ($_POST['menuname'])
		{	$saved = $this->femenu->Save($_POST);
			if ($saved['failmessage'])
			{	$this->failmessage = $saved['failmessage'];
			}
			if ($saved['successmessage'])
			{	$this->successmessage = $saved['successmessage'];
			}
		}
		
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->femenu->Delete())
			{	header('location: femenus.php');
				exit;
			} else
			{	$this->failmessage = 'Delete failed';
			}
		}
		
	} // end of fn ConstructFunctions
	
	function FEMenuBodyContent()
	{	ob_start();
		$this->femenu->InputForm();
		return ob_get_clean();
	} // end of fn FEMenuBodyContent
	
} // end of defn FEMenuEditPage

$page = new FEMenuEditPage();
$page->Page();
?>
