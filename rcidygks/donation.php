<?php
include_once('sitedef.php');

class DonationViewPage extends AdminDonationsPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AdminDonationsLoggedInConstruct()
	{	parent::AdminDonationsLoggedInConstruct();
		$this->css['admincampdonation'] = 'admincampdonation.css';
		$this->menuarea = 'view';
		
		if ($_GET['refund'] && $_GET['confirm'] && $this->CanAdminUser('accounts admin'))
		{	if ($this->donation->WPRefund())
			{	$this->successmessage = 'Donation refunded';
			}
		}
		if (isset($_POST['donorfirstname']))
		{	$saved = $this->donation->SaveChanges($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
	} // end of fn AdminDonationsLoggedInConstruct

	function AdminDonationsBody()
	{	parent::AdminDonationsBody();
		echo $this->donation->AdminDonationsBodyDisplayForm();
	} // end of fn AdminDonationsBody
	
} // end of defn DonationViewPage

$page = new DonationViewPage();
$page->Page();
?>