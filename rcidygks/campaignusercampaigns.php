<?php
include_once('sitedef.php');

class CampaignUserCampaignsPage extends AdminCampaignUsersPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'campaigns';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('campaignusercampaigns.php', 'Campaigns owned');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->CampaignsList($campaigns = $this->campaignuser->GetCampaigns(), 30, true, $this->campaignuser->id);
		if ($campaigns)
		{	$get = array();
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
			}
			echo '<p class="adminCSVDownload"><a href="campaignusercampaigns_csv.php?', implode('&', $get) , '">Download CSV (for whole selection)</a></p>';
		}
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignUserCampaignsPage

$page = new CampaignUserCampaignsPage();
$page->Page();
?>