<?php
include_once('sitedef.php');

class FEMenuAjax extends AdminFrontEndMenuPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function CMSLoggedInConstruct($page_option = '')
	{	parent::CMSLoggedInConstruct('items');
		switch ($_GET['action'])
		{	case 'popup':
				echo $this->PagesSelectList();
				break;
			case 'refresh':
				echo $this->femenu->MenuItemsTable();
				break;
			case 'remove':
				$this->femenu->RemoveMenuItem($_GET['miid']);
				echo $this->femenu->MenuItemsTable();
				break;
			case 'add':
				$this->femenu->AddPageAsItem($_GET['pageid']);
				//echo $this->SlidesSelectList();
				break;
		}
	} //  end of fn __construct
	
	private function PagesSelectList()
	{	if ($pages = $this->GetPagesToSelectFrom())
		{	echo '<table>';
			foreach ($pages as $page)
			{	echo $this->PagesSelectListLine($page, 0);
			}
			echo '</table>';
		}
	} // end of fn PagesSelectList
	
	public function GetPagesToSelectFrom()
	{	$pages = array();
		if ($result = $this->db->Query($sql = 'SELECT pages.*, pages_lang.pagetitle FROM pages, pages_lang WHERE pages.pageid=pages_lang.pageid ORDER BY pages_lang.pagetitle, pages.pagename'))
		{	while ($row = $this->db->FetchArray($result))
			{	$pages[] = new AdminPageContent($row);
			}
		} else echo '<p>', $sql, ': ', $this->db->Error(), '</p>';
		return $pages;
	} // end of fn GetPagesToSelectFrom
	
	private function PagesSelectListLine($page, $depth = 0)
	{	ob_start();
		echo '<td>', $this->InputSafeString($page->details['pagetitle']), ' [', $page->details['pagename'], ']', $page->details['headeronly'] ? ' ** header only **' : '', '</td><td>';
		if ($link = $page->Link())
		{	echo ' [', $link, ']';
		}
		echo '</td><td><a onclick="PageAdd(', $this->femenu->id, ',', $page->details['pageid'], ');">add this</a></td></tr>';
		return ob_get_clean();

	} // end of fn PagesSelectListLine
	
} // end of defn FEMenuAjax

$page = new FEMenuAjax();
?>
