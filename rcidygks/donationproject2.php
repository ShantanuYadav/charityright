<?php
include_once('sitedef.php');

class DonationOptionPage extends AdminDonationOptionsPage
{	
	
	protected function AssignDonationCountry()
	{	$this->donationproject2 = new AdminDonationProject2($_GET['id']);
		if ($this->donationproject2->id)
		{	$this->donationproject = new AdminDonationProject($this->donationproject2->details['dpid']);
		} else
		{	$this->donationproject = new AdminDonationProject($_GET['dpid']);
		}
		if ($this->donationproject->id)
		{	$this->donationcountry = new AdminDonationOption($this->donationproject->details['dcid']);
		} else
		{	header('location: donationoptions.php');
			exit;
		}
	} // end of fn AssignDonationCountry
	
	protected function AdminDonationOptionsConstructFunctions()
	{	$this->menuarea = 'project2';
		if (isset($_POST['projectname']))
		{	$saved = $this->donationproject2->Save($_POST, $this->donationproject);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->donationproject2->Delete())
			{	header('location: donationprojects2.php?id=' . $this->donationproject->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
		$this->menuarea = $this->donationproject2->id ? 'project2' : 'projects2';
	} // end of fn AdminDonationOptionsConstructFunctions
	
	protected function AdminDonationOptionsLoggedInConstruct()
	{	parent::AdminDonationOptionsLoggedInConstruct();
		if (!$this->donationproject2->id)
		{	$this->breadcrumbs->AddCrumb('donationprojects2.php?id=' . $this->donationproject->id, 'Sub-options');
			$this->breadcrumbs->AddCrumb('donationproject2.php?dpid=' . $this->donationproject->id, 'New sub-option');
		}
	} // end of fn AdminDonationOptionsLoggedInConstruct
	
	protected function AdminDonationOptionsBody()
	{	parent::AdminDonationOptionsBody();
		echo $this->donationproject2->InputForm($this->donationproject);
	} // end of fn AdminDonationOptionsBody
	
} // end of defn DonationOptionPage

$page = new DonationOptionPage();
$page->Page();
?>