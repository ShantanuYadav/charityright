<?php
include_once('sitedef.php');

class PostsPage extends AdminPostsPage
{	private $startdate = '';
	private $enddate = '';
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';

		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-3 months'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	$perpage = 20;
		echo $this->FilterForm(), '<table><tr class="newlink"><th colspan="11"><a href="post.php">Add new post</a></th></tr><tr><th></th><th>Title</th><th>Link</th><th>Created</th><th>Changed</th><th>Post date (for ordering)</th><th>Categories</th><th>Country</th><th>Template</th><th></th><th>Actions</th></tr>';
		if ($posts = $this->GetPosts())
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($posts as $post_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$post = new AdminPost($post_row);
					if (!$templates)
					{	$templates = $post->PossibleTemplates();
					}
					$extras = array();
					if ($post->details['live'])
					{	$extras[] = 'Live';
					} else
					{	$extras[] = 'Hidden';
					}
					if ($post->GetQuickDonateOptions(true))
					{	$extras[] = 'DonateForm';
					}
					echo '<tr class="stripe', $i++ % 2,  '"><td>';
					if ($image_url = $post->MainImageSource('thumb'))
					{	echo '<img src="', $image_url, '" />';
					}
					echo '</td><td>', $this->InputSafeString($post->details['posttitle']), '</td><td>', $post->Link(), '</td><td>', date('d/m/y @H:i', strtotime($post->details['posted'])), '</td><td>', $post->details['posted'] == $post->details['updated'] ? '' : date('d/m/y @H:i', strtotime($post->details['updated'])), '</td><td>', (int)$post->details['postdate'] ? date('d/m/y', strtotime($post->details['postdate'])) : '', '</td><td>', $post->CategoriesList('<br />'), '</td><td>', $post->details['country'] ? $this->InputSafeString($this->GetCountry($post->details['country'], 'shortname')) : '', '</td><td>', $this->InputSafeString($templates[$post->details['template']]), '</td><td>', implode('<br />', $extras), '</td><td><a href="post.php?id=', $post->id, '">edit</a>';
					if ($post->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="post.php?id=', $post->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($posts) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($posts), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
	} // end of fn AdminPostsBody
	
	private function GetPosts()
	{	$tables = array('posts'=>'posts');
		$fields = array('posts.*');
		$where = array();
		
		if ($this->startdate)
		{	$where['startdate'] = 'IF(posts.postdate="0000-00-00", LEFT(posts.posted, 10), posts.postdate) >= "' . $this->startdate . '"';
		}
		if ($this->enddate)
		{	$where['enddate'] = 'IF(posts.postdate="0000-00-00", LEFT(posts.posted, 10), posts.postdate) <= "' . $this->enddate . '"';
		}
		if ($_GET['postcat'])
		{	$tables['postsforcats'] = 'postsforcats';
			$where['postsforcats_link'] = 'postsforcats.postid=posts.postid';
			$where['pcatid'] = 'postsforcats.pcatid=' . (int)$_GET['postcat'];
		}
		if ($_GET['liveonly'])
		{	$where['liveonly'] = 'posts.live=1';
		}
		if ($_GET['country'])
		{	$where['country'] = 'posts.country="' . $this->SQLSafe($_GET['country']) . '"';
		}
		if ($_GET['text'])
		{	$where['text'] = 'CONCAT_WS(", ", posts.posttitle, posts.posttext, posts.snippet) LIKE "%' . $this->SQLSafe($_GET['text']) . '%"';
		}
		
		$orderby = array('posts.postdate DESC', 'posts.posted DESC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'postid');
	} // end of fn GetPosts
	
	private function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><input type="hidden" name="id" value="', $this->event->id, '" /><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<span>Category</span><select name="postcat"><option value="">-- all --</option>';
		foreach ($this->post->PossibleCategories() as $postcat)
		{	echo '<option value="', $postcat['pcatid'], '"', $postcat['pcatid'] == $_GET['postcat'] ? ' selected="selected"' : '', '>', $this->InputSafeString($postcat['catname']), '</option>';
		}
		echo '</select><span>Live only</span><input type="checkbox" name="liveonly" value="1" ', $_GET['liveonly'] ? 'checked="checked" ' : '', '/><input type="submit" class="submit" value="Get" /><div class="clear"></div><span>Country</span><select name="country"><option value="">-- all --</option>';
		foreach ($this->post->PossibleCountries() as $ccode=>$cname)
		{	echo '<option value="', $ccode, '"', $ccode == $_GET['country'] ? ' selected="selected"' : '', '>', $this->InputSafeString($cname), '</option>';
		}
		echo '</select><span>In text</span><input type="text" name="text" class="long" value="', $this->InputSafeString($_GET['text']), '" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn PostsPage

$page = new PostsPage();
$page->Page();
?>
