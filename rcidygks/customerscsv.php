<?php
include_once('sitedef.php');

class CustomersCSV extends AccountsMenuPage
{	var $customers = '';

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->customers = new AdminCustomers($_GET['email']);
	} // end of fn AccountsLoggedInConstruct

	function AccountsBody()
	{	$this->customers->CSVDownload();
	} // end of fn AccountsBody
	
} // end of defn CustomersCSV

$page = new CustomersCSV();
$page->AdminBodyMain();
?>