<?php
include_once("sitedef.php");

class LanguageListPage extends AdminLanguagePage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function LanguageContent()
	{	$languages = new AdminLanguages();
		$languages->ListLanguages();
	} // end of fn LanguageContent
	
} // end of defn LanguageListPage

$page = new LanguageListPage();
$page->Page();
?>