<?php
include_once('sitedef.php');

class CampaignsDonationsCSV extends AdminCampaignsPage
{	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		if ($this->user->CanUserAccess('accounts'))
		{	$csv = new CSVReport();
			$this->AddDonationsHeaderToCSV($csv);
			$admindonations = new AdminDonations();
			$this->AddDonationsRowsToCSV($csv, $admindonations->GetCampaignDonations($_GET));
			$csv->Output();
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
} // end of defn CampaignsDonationsCSV

$page = new CampaignsDonationsCSV();
?>