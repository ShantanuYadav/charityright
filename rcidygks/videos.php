<?php
include_once('sitedef.php');

class VideosPage extends AdminVideosPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminVideosBody()
	{	$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="7"><a href="video.php">Add new video</a></th></tr><tr><th></th><th>Title</th><th>Link</th><th>Youtube ID</th><th>Uploaded</th><th>Live?</th><th>Actions</th></tr>';
		if ($videos = $this->GetVideos())
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($videos as $video_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$video = new AdminVideo($video_row);
					echo '<tr class="stripe', $i++ % 2,  '"><td>';
					if ($src = $video->GetImageSRC('thumb'))
					{	echo '<img src="', $src, '" />';
					}
					echo '</td><td>', $this->InputSafeString($video->details['vtitle']), '</td><td>', $video->Link(), '</td><td>', $video->YoutubeID(), '</td><td>', date('d/m/y', strtotime($video->details['uploaded'])), '</td><td>', $video->details['live'] ? 'Live' : '', '</td><td><a href="video.php?id=', $video->id, '">edit</a>';
					if ($video->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="video.php?id=', $video->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($videos) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($videos), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
	} // end of fn AdminVideosBody
	
	private function GetVideos()
	{	$tables = array('videos'=>'videos');
		$fields = array('videos.*');
		$where = array();
		$orderby = array('videos.uploaded DESC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'vid');
	} // end of fn GetPosts
	
} // end of defn VideosPage

$page = new VideosPage();
$page->Page();
?>