<?php
include_once('sitedef.php');

class TicketTypePage extends AdminEventsPage
{
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminEventsLoggedInConstruct()
	{	parent::AdminEventsLoggedInConstruct();
		$this->menuarea = ($this->tickettype->id ? 'tickettypeedit' : 'tickettypes');
		$this->breadcrumbs->AddCrumb('eventdates.php?id=' . $this->event->id, 'Dates');
		ob_start();
		echo $this->InputSafeString($this->eventdate->venue['venuename']), ', ', date('d/m/y', strtotime($this->eventdate->details['starttime'])), ' - ', date('d/m/y', strtotime($this->eventdate->details['endtime']));
		$this->breadcrumbs->AddCrumb('eventdate.php?id=' . $this->eventdate->id, ob_get_clean());
		$this->breadcrumbs->AddCrumb('tickettypes.php?id=' . $this->eventdate->id, 'Ticket types');
		if ($this->tickettype->id)
		{	$this->breadcrumbs->AddCrumb('tickettype.php?id=' . $this->tickettype->id, $this->InputSafeString($this->tickettype->details['ttypename']));
		} else
		{	$this->breadcrumbs->AddCrumb('tickettype.php?edid=' . $this->eventdate->id, 'New ticket');
		}
	} // end of fn AdminEventsLoggedInConstruct
	
	protected function EventConstructFunctions()
	{	if (isset($_POST['ttypename']))
		{	$saved = $this->tickettype->Save($_POST, $this->eventdate->id);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->tickettype->Delete())
			{	header('location: tickettypes.php?id=' . $this->eventdate->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn EventConstructFunctions
	
	protected function AdminEventsBody()
	{	parent::AdminEventsBody();
		echo $this->tickettype->InputForm($this->eventdate->id);
	} // end of fn AdminEventsBody
	
	protected function AssignEvent()
	{	$this->tickettype = new AdminTicketType($_GET['id']);
		if ($this->tickettype->id)
		{	$this->eventdate = new AdminEventDate($this->tickettype->details['edid']);
		} else
		{	$this->eventdate = new AdminEventDate($_GET['edid']);
		}
		$this->event = new AdminEvent($this->eventdate->details['eid']);
	} // end of fn AssignEvent
	
} // end of defn TicketTypePage

$page = new TicketTypePage();
$page->Page();
?>