<?php
include_once('sitedef.php');

class CartOrderViewPage extends AdminOrdersPage
{
	function AdminOrdersLoggedInConstruct()
	{	parent::AdminOrdersLoggedInConstruct();
		$this->menuarea = 'ddconfirm';
		
		if (isset($_POST['ezc_CustomerID']))
		{	$saved = $this->order->ManualConfirmDirectDebit($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
		
		if (!$this->order->IsMonthlyUnconfirmed())
		{	header('location: cartorder.php?id=' . $this->order->id);
			exit;
		}
				
	} // end of fn AdminOrdersLoggedInConstruct

	function AdminOrdersBody()
	{	parent::AdminOrdersBody();
		echo $this->order->ManualDDConfirmForm($_POST);
	} // end of fn AdminOrdersBody
	
} // end of defn CartOrderViewPage

$page = new CartOrderViewPage();
$page->Page();
?>