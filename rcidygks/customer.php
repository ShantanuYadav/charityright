<?php
include_once('sitedef.php');

class CustomerPage extends AccountsMenuPage
{	var $customer = '';

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('customers.php', 'Customers');
		$this->customer = new AdminCustomer($_GET['id']);
		
		$this->breadcrumbs->AddCrumb('customer.php?id=' . $this->customer->id, $this->customer->details['email']);
			
	} // end of fn AccountsLoggedInConstruct

	function AccountsBody()
	{	$this->customer->Display();
	} // end of fn AccountsBody
	
} // end of defn CustomerPage

$page = new CustomerPage();
$page->Page();
?>