<?php
include_once("sitedef.php");

class PHPInfoPage extends AdminPage
{	
	function __construct()
	{	parent::__construct("ADMIN");
	} //  end of fn __construct
	
	function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if ($this->user->CanUserAccess("technical"))
		{	$this->breadcrumbs->AddCrumb("phpinfo.php", "Server Info");
		}
	} // end of fn LoggedInConstruct
	
	function AdminBodyMain()
	{	if ($this->user->CanUserAccess("technical"))
		{	//$ipctry = new IPToCountry();
			echo "<p><a href='phpinfo.php?fred=1'>plus ?fred=1</a></p>\n<p>Your IP: ", $_SERVER["REMOTE_ADDR"];//, ", in ", $ipctry->CurrentCountry(), "</p>";
			//phpinfo();
			echo "<p>apache user: ", shell_exec("id"), "</p>\n";
			echo "<p>server time: ", date("j-M-y H:i:s"), "</p>\n";
			if ($br = get_browser(null, true))
			{	echo "<p>browser info ...</p>\n";
				foreach($br as $var=>$value)
				{	echo "<p>[", $var, "]=[", $value, "]</p>\n";
				}
			}
		}
	} // end of fn AdminBodyMain
	
} // end of defn PHPInfoPage

$page = new PHPInfoPage();
$page->Page();
?>