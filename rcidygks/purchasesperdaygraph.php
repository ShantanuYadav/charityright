<?php
include_once('sitedef.php');

class PurchasesPerDayGraph extends Graph
{	protected $y_axis_prefix = '�';
	protected $dataWidth = 600;
	protected $dataHeight = 300;
	protected $y_axisGap = 35;
	
	function __construct()
	{	
		$this->titleString = 'Income for ' . date('j M y', strtotime($_GET['startdate'])) . ' to '  . date('j M y', strtotime($_GET['enddate']));
		parent::__construct();
		
	} //end of fn __construct
	
	protected function GetData()
	{	$ianal = new IncomeAnalysis();
		if ($days = $ianal->IncomePerDay($_GET))
		{	$this->legend[] = 'Main donations';
			$this->legend[] = 'CR Stars donations';
			//$this->legend[] = 'Events bookings';
			$this->legend[] = 'All income';
			$now = time();
			
			foreach ($days as $day=>$day_data)
			{	if ($details['stamp'] > $now)
				{	break;
				}
				$adj = $this->AdjustThisDay($day);
				$y = array();
				$y[] = ($day_data['donations_amount'] + $day_data['donations_adminamount']) * $adj;
				$y[] = $day_data['crstars_amount'] * $adj;
				//$y[] = $day_data['events_amount'] * $adj;
				$y[] = ($day_data['donations_amount'] + $day_data['donations_adminamount'] + $day_data['crstars_amount'] + $day_data['events_amount']) * $adj;
				
				$this->data[] = array('n'=>date('j', $day_data['stamp']), 'y'=>$y);
			}
		}
	} // end of DefineData
	
	function AdjustThisDay($date = '')
	{	if (date('Y-m-d') === $date)
		{	return (24 * 60) / ((date('G') * 60) +  date('i'));
		} else
		{	return 1;
		}
	} // end of fn AdjustThisDay
	
} // end of fn PurchasesPerDayGraph


$graph = new PurchasesPerDayGraph();
$graph->OutPut();
?>