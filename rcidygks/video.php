<?php
include_once('sitedef.php');

class VideoPage extends AdminVideosPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function VideoConstructFunctions()
	{	
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		$this->menuarea = 'edit';
		if (isset($_POST['vtitle']))
		{	$saved = $this->video->Save($_POST, $_FILES['imagefile']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->video->Delete())
			{	header('location: videos.php');
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn VideoConstructFunctions
	
	protected function AdminVideoLoggedInConstruct()
	{	parent::AdminVideoLoggedInConstruct();
		if (!$this->video->id)
		{	$this->breadcrumbs->AddCrumb('video.php', 'New video');
		}
	} // end of fn AdminVideoLoggedInConstruct
	
	protected function AdminVideosBody()
	{	parent::AdminVideosBody();
		echo $this->video->InputForm();
	} // end of fn AdminVideosBody
	
} // end of defn VideoPage

$page = new VideoPage();
$page->Page();
?>