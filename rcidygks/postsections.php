<?php
include_once('sitedef.php');

class PostSectionsPage extends AdminPostsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'sections';
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		if ($this->post->id)
		{	$this->breadcrumbs->AddCrumb('postsections.php', 'Extra sections');
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->post->SectionTable();
	} // end of fn AdminPostsBody
	
} // end of defn PostSectionsPage

$page = new PostSectionsPage();
$page->Page();
?>