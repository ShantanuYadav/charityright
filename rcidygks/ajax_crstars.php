<?php
include_once('sitedef.php');

class CampaignAjax extends AdminCampaignsPage
{	
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		
		if (method_exists($this, $method = 'Action_' . $_GET['action']))
		{	$this->$method();
		}
		
	} // end of fn AdminCampaignsLoggedInConstruct

	private function Action_countrychange()
	{	echo $this->campaign->CountryProjectsList($_GET['country'], $_GET['project']);
	} // end of fn Action_countrychange
	
} // end of defn CampaignAjax

$page = new CampaignAjax();
?>
