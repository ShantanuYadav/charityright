<?php
include_once('sitedef.php');

class CampaignPage extends AdminCampaignUsersPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'edit';
		$this->js['admin_crstars.js'] = 'admin_crstars.js';
		if (isset($_POST['firstname']))
		{	$saved = $this->campaignuser->Create($_POST);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($this->campaignuser->details['suspended'])
		{	$this->warningmessage = 'User is currently suspended';
		}
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		if (!$this->campaignuser->id)
		{	$this->breadcrumbs->AddCrumb('campaignuser.php', 'New campaign user');
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		echo $this->campaignuser->ShowDetails();
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignPage

$page = new CampaignPage();
$page->Page();
?>