<?php
include_once('sitedef.php');

class PurchasesPerMonthGraph extends Graph
{	var $ystart = 0;
	var $mstart = 0;
	var $yend = 0;
	var $mend = 0;
	var $startstamp = 0;
	var $endstamp = 0;
	var $maxlines = 2;
	protected $y_axis_suffix = '%';
	protected $dataWidth = 600;
	protected $dataHeight = 300;
	protected $y_axisGap = 10;
	
	function __construct()
	{	if (!$_GET['ystart'] || !$_GET['mstart'])
		{	$_GET['ystart'] = date('Y', strtotime('-11 months'));
			$_GET['mstart'] = date('m', strtotime('-11 months'));
		}
		if (!$_GET['yend'] || !$_GET['mend'])
		{	$_GET['yend'] = date('Y');
			$_GET['mend'] = date('m');
		}
		
		$this->startstamp = mktime(0,0,0,$_GET['mstart'], 1, $_GET['ystart']);
		$this->endstamp = mktime(0,0,0,$_GET['mend'], 1, $_GET['yend']);
		$this->titleString = 'Conversion rate ' . date('M-y', $this->startstamp) . ' to ' . date('M-y', $this->endstamp);
		parent::__construct();
		
	} //end of fn __construct
	
	protected function GetData()
	{	$ianal = new IncomeAnalysis();
		if ($months = $ianal->ConversionPerMonth($_GET))
		{	$this->legend[] = 'Main donations';
			$this->legend[] = 'CR Stars donations';
			
			foreach ($months as $month=>$month_data)
			{	$y = array();
				$y[] = $month_data['donations_rate'] * 100;
				$y[] = $month_data['crstars_rate'] * 100;
				$this->data[] = array('n'=>date('M', strtotime($month . '-01')), 'y'=>$y);
			}
		}
	} // end of DefineData
	
	function AdjustThisMonth($date = '')
	{	$daysinmonth = date('t', strtotime($date . '-01'));
		if (date('Y-m') === $date)
		{	$adjust = $daysinmonth / ((date('j') - 1) + ((date('H') + (date('i') / 60) ) / 24));
		} else
		{	$adjust = 1;
		}
		
		return array('bydays'=>(31 / $daysinmonth) * $adjust, 'raw'=>$adjust);
		
	} // end of fn AdjustThisMonth
	
} // end of fn PurchasesPerMonthGraph


$graph = new PurchasesPerMonthGraph();
$graph->OutPut();
?>