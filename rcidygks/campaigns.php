<?php
include_once('sitedef.php');

class CampaignsPage extends AdminCampaignsPage
{	var $startdate = '';
	var $enddate = '';
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 year'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	echo $this->CampaignsFilterForm(), $this->CampaignsList($this->GetCampaigns(), 20);
		$get = array();
		if ($_GET)
		{	$get = array();
			foreach ($_GET as $key=>$value)
			{	if ($value && ($key != 'page'))
				{	$get[] = $key . '=' . $value;
				}
			}
		}
		$get[] = 'startdate=' . $this->startdate;
		$get[] = 'enddate=' . $this->enddate;
		echo '<p class="adminCSVDownload"><a href="campaigns_csv.php?', implode('&', $get) , '">Download CSV (for whole selection)</a></p>';
	} // end of fn AdminCampaignBody
	
} // end of defn CampaignsPage

$page = new CampaignsPage();
$page->Page();
?>