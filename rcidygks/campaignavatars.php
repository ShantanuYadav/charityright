<?php
include_once('sitedef.php');

class CampaignAvatarsPage extends AdminCampaignAvatarPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminCampaignBody()
	{	$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="6"><a href="campaignavatar.php">Create new campaign avatar</a></th></tr><tr><th></th><th>Admin description</th><th>List order</th><th>Live?</th><th>Used by</th><th>Actions</th></tr>';
		if ($campaignavatars = $this->GetCampaignAvatars())
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($campaignavatars as $campaignavatar_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$campaignavatar = new AdminCampaignAvatar($campaignavatar_row);
					echo '<tr class="stripe', $i++ % 2,  '"><td><img src="', $campaignavatar->GetImageSRC('thumb'), '" /></td><td>', $this->InputSafeString($campaignavatar->details['imagedesc']), '</td><td class="num">', $campaignavatar->details['listorder'], '</td><td>', $campaignavatar->details['live'] ? 'Live' : '', '</td><td class="num">', $campaignavatar->UsedCount(), '</td><td><a href="campaignavatar.php?id=', $campaignavatar->id, '">edit</a>';
					if ($campaignavatar->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="campaignavatar.php?id=', $campaignavatar->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($campaignavatars) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($campaignavatars), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
	} // end of fn AdminCampaignBody
	
	private function GetCampaignAvatars()
	{	$tables = array('campaignavatars'=>'campaignavatars');
		$fields = array('campaignavatars.*');
		$where = array();
		$orderby = array('campaignavatars.listorder ASC');
		$groupby = array();
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby, $groupby), 'caid');
	} // end of fn GetCampaignText
	
} // end of defn CampaignAvatarsPage

$page = new CampaignAvatarsPage();
$page->Page();
?>