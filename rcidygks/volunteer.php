<?php
include_once('sitedef.php');

class VolunteerPage extends AdminVolunteersPage
{	
	protected function AdminVolunteersLoggedInConstruct()
	{	parent::AdminVolunteersLoggedInConstruct();
		
	} // end of fn AdminVolunteersLoggedInConstruct
	
	protected function AdminVolunteersBody()
	{	echo '<div id="itemblock-disp">
				<div><ul>
					<li><span class="itemblock-label">Name</span><span class="itemblock-details">', $this->InputSafeString($this->volunteer->FullName()), '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Email</span><span class="itemblock-details"><a href="mailto:', $email = $this->InputSafeString($this->volunteer->details['email']), '">', $email, '</a></span><br class="clear" /></li>
					<li><span class="itemblock-label">Phone</span><span class="itemblock-details">', $this->InputSafeString($this->volunteer->details['phone']), '</span><br class="clear" /></li>
					<li><span class="itemblock-label">Name</span><span class="itemblock-details">', nl2br($this->InputSafeString($this->volunteer->details['volnotes'])), '</span><br class="clear" /></li>
				</ul></div>
				</div>';
	} // end of fn AdminVolunteersBody
	
} // end of defn VolunteerPage

$page = new VolunteerPage();
$page->Page();
?>