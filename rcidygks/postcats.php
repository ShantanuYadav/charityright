<?php
include_once('sitedef.php');

class PostCatsPage extends AdminPostsPage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'cats';
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		if ($this->post->id)
		{	$this->breadcrumbs->AddCrumb('postcats.php', 'Categories');
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->post->CategoriesTableContainer();
	} // end of fn AdminPostsBody
	
} // end of defn PostCatsPage

$page = new PostCatsPage();
$page->Page();
?>