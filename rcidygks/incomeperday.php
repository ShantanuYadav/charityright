<?php
include_once('sitedef.php');

class IncomePerDayPage extends AccountsMenuPage
{	private $startdate = '';
	private $enddate = '';
	private $stats;

	public function __construct()
	{	parent::__construct();
	} //  end of fn __construct

	public function AccountsLoggedInConstruct()
	{	parent::AccountsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		$this->stats = new IncomeAnalysis();
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 month'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		$this->breadcrumbs->AddCrumb('incomeperday.php', 'Income per day');
		
	} // end of fn AccountsLoggedInConstruct

	public function AccountsBody()
	{	echo $this->FilterForm(), $this->IncomeTable();
	} // end of fn AccountsBody

	private function IncomeTable()
	{	$filter = $_GET;
		$filter['startdate'] = $this->startdate;
		$filter['enddate'] = $this->enddate;
		if ($days = $this->stats->IncomePerDay($filter))
		{	//$this->VarDump($days);
			//return;
			echo '<table><tr><th rowspan="2">Date</th><th colspan="3" style="text-align: center;">Main donations</th><th colspan="2" style="text-align: center;">Campaigns</th>',
				//'<th colspan="2" style="text-align: center;">Events</th>',
				'<th rowspan="2">All donations</th></tr>
					<tr><th class="num">Donations</th><th class="num">for projects</th><th class="num">for admin</th><th class="num">Donations</th><th class="num">amount</th>',
				//'<th class="num">Bookings</th><th class="num">amount</th>',
				'</tr>';

			$totals = array('donations_count'=>0, 'donations_amount'=>0, 'donations_adminamount'=>0, 'crstars_count'=>0, 'crstars_amount'=>0, 'events_count'=>0, 'events_amount'=>0);

			foreach ($days as $day_data)
			{	foreach ($day_data as $field=>$value)
				{	if (isset($totals[$field]))
					{	$totals[$field] += $value;
					}
				}
		
				if (!$cursymbol[$donation->details['currency']])
				{	$cursymbol[$donation->details['currency']] = $this->GetCurrency($donation->details['currency'], 'cursymbol');
				}
				echo '<tr class="stripe', $i++ % 2,  '"><td>', $day_data['disp'], '</td><td class="num">', (int)$day_data['donations_count'], '</td><td class="num">&pound;', number_format($day_data['donations_amount'], 2), '</td><td class="num">&pound;', number_format($day_data['donations_adminamount'], 2), '</td><td class="num">', (int)$day_data['crstars_count'], '</td><td class="num">&pound;', number_format($day_data['crstars_amount'], 2), '</td>',
					//'<td class="num">', (int)$day_data['events_count'], '</td><td class="num">&pound;', number_format($day_data['events_amount'], 2), '</td>',
					'<td class="num">&pound;', number_format($day_data['events_amount'] + $day_data['donations_amount'] + $day_data['donations_adminamount'] + $day_data['crstars_amount'], 2), '</td></tr>';
			}
			
			echo '<tr><th>Totals</th><th class="num">', (int)$totals['donations_count'], '</th><th class="num">&pound;', number_format($totals['donations_amount'], 2), '</th><th class="num">&pound;', number_format($totals['donations_adminamount'], 2), '</th><th class="num">', (int)$totals['crstars_count'], '</th><th class="num">&pound;', number_format($totals['crstars_amount'], 2), '</th>',
				//'<th class="num">', (int)$totals['events_count'], '</th><th class="num">&pound;', number_format($totals['events_amount'], 2), '</th>',
				'<th class="num">&pound;', number_format($totals['events_amount'] + $totals['donations_amount'] + $totals['donations_adminamount'] + $totals['crstars_amount'], 2), '</th></tr></table>';
			echo '<p><img src="purchasesperdaygraph.php?startdate=', $this->startdate, '&enddate=', $this->enddate, '" /><br />todays figures are predicted for the end of the day</p>';
		}
	} // end of fn IncomeTable
	
	function FilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><span>From</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn FilterForm
	
} // end of defn IncomePerDayPage

$page = new IncomePerDayPage();
$page->Page();
?>