<?php
include_once('sitedef.php');

class CampaignUserTeamsCSV extends AdminCampaignUsersPage
{	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$csv = new CSVReport();
		$this->AddCampaignsHeaderToCSV($csv);
		$this->AddCampaignsRowsToCSV($csv, $this->campaignuser->GetTeams());
		$csv->Output();
	} // end of fn AdminCampaignsLoggedInConstruct
	
} // end of defn CampaignUserTeamsCSV

$page = new CampaignUserTeamsCSV();
?>