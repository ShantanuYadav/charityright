<?php
include_once('sitedef.php');

class CampaignDonationsCSV extends AdminCampaignsPage
{	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		if ($this->user->CanUserAccess('accounts'))
		{	$csv = new CSVReport();
			$this->AddDonationsHeaderToCSV($csv);
			$this->AddDonationsRowsToCSV($csv, $this->GetDonations());
			$csv->Output('donations_' . $this->campaign->details['slug'] . '.csv');
		}
	} // end of fn AdminCampaignsLoggedInConstruct
	
} // end of defn CampaignDonationsCSV

$page = new CampaignDonationsCSV();
?>
