<?php
include_once('sitedef.php');

class CampaignAddToAnotherPage extends AdminCampaignsPage
{	var $startdate = '';
	var $enddate = '';
	private $getString = '';
	
	protected function CampaignConstructFunctions()
	{	$this->menuarea = 'team_add';
	} // end of fn CampaignConstructFunctions
	
	protected function AdminCampaignsLoggedInConstruct()
	{	parent::AdminCampaignsLoggedInConstruct();
		$this->css[] = 'datepicker.css';
		$this->js[] = 'datepicker.js';
		
		$get = array();
		if ($_GET)
		{	foreach ($_GET as $key=>$value)
			{	if ($value && ($key != 'page') && ($key != 'addid') && ($key != 'addid_confirm'))
				{	$get[] = $key . '=' . $value;
				}
			}
			if ($get)
			{	$this->getString = implode('&', $get);
			}
		}
	
		if ($_GET['addid'] && ($add_campaign = new Campaign($_GET['addid'])) && $add_campaign->id)
		{	$this->failmessage = '<a href="campaign_addtoteam.php?' . $this->getString . '&addid_confirm=' . $add_campaign->id . '">Click to confirm you want to add this to "' . $this->InputSafeString($add_campaign->details['campname']) . '"</a>';
		} else
		{	if ($_GET['addid_confirm'])
			{	if ($this->campaign->AddToTeam($_GET['addid_confirm']))
				{	header('location: campaign.php?id=' . $this->campaign->id);
					exit;
				} else
				{	$this->failmessage = 'failed';
				}
			}
		}
		
		if ($this->campaign->team_members || $this->campaign->team)
		{	header('location: campaign.php?id=' . $this->campaign->id);
			exit;
		}
		
		// set up dates
		if (($ys = (int)$_GET['ystart']) && ($ds = (int)$_GET['dstart']) && ($ms = (int)$_GET['mstart']))
		{	$this->startdate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->startdate = $this->datefn->SQLDate(strtotime('-1 year'));
		}
		
		if (($ys = (int)$_GET['yend']) && ($ds = (int)$_GET['dend']) && ($ms = (int)$_GET['mend']))
		{	$this->enddate = $this->datefn->SQLDate(mktime(0,0,0,$ms, $ds, $ys));
		} else
		{	$this->enddate = $this->datefn->SQLDate();
		}
		$this->breadcrumbs->AddCrumb('campaign_addtoteam.php?id=' . $this->campaign->id, 'Add to another team');
	} // end of fn AdminCampaignsLoggedInConstruct
	
	protected function AdminCampaignBody()
	{	parent::AdminCampaignBody();
		$_GET['ctype'] = 'team';
		$_GET['visible'] = '1';
		$_GET['enabled'] = '1';
		if ($this->campaign->details['cuid'])
		{	$_GET['not_cuid'] = $this->campaign->details['cuid'];
		}
		echo $this->CampaignsFilterForm(), $this->CampaignsList($this->GetCampaigns(), 20);
	} // end of fn AdminCampaignBody
	
	protected function CampaignsList($campaigns = array(), $perpage = 0, $newlink = false, $cuid = 0)
	{	ob_start();
		echo '<table><tr><th></th><th>Campaign</th><th>Owner</th><th>Created</th><th class="num">Target</th><th class="num">Raised</th><th class="num">Raised GBP</th><th>Actions</th></tr>';
		
		if (is_array($campaigns))
		{	foreach ($campaigns as $cid=>$campaign_row)
			{	if ($campaign_row['cuid'] == $this->campaign->details['cuid'])
				{	unset($campaigns[$cid]);
				}
			}
		}
		
		if ($campaigns)
		{	if ($perpage = (int)$perpage)
			{	if ($_GET['page'] > 1)
				{	$start = ($_GET['page'] - 1) * $perpage;
				} else
				{	$start = 0;
				}
				$end = $start + $perpage;
			} else
			{	$start = 0;
			}
			
			if ($campaigns[$this->campaign->id])
			{	unset($campaigns[$this->campaign->id]);
			}
			
			foreach ($campaigns as $campaign_row)
			{	$campaign = new AdminCampaign($campaign_row);
				$raised = $campaign->GetDonationTotalsAll();
				if (++$count > $start)
				{	if (!$perpage || ($count <= $end))
					{	echo '<tr class="stripe', $i++ % 2,  '"><td>';
						if ($image_url = $campaign->GetImageSRC('thumb'))
						{	echo '<img src="', $image_url, '" />';
						} else
						{	if ($avatar = $campaign->GetAvatarSRC('thumb'))
							{	echo '<img src="', $avatar, '" />';
							}
						}
						echo '</td><td><a href="campaign.php?id=', $campaign->id, '">', $this->InputSafeString($campaign->details['campname']), '</a></td><td>';
						if ($campaign->details['cuid'])
						{	if ($campaign->owner)
							{	echo '<a href="campaignuser.php?id=', $campaign->details['cuid'], '">', $this->InputSafeString($campaign->owner['firstname'] . ' ' . $campaign->owner['lastname']), '</a>';
							} else
							{	echo 'owner not found';
							}
						} else
						{	echo 'Charity Right';
						}
						echo '</td><td>', date('d/m/y H:i', strtotime($campaign->details['created'])), '</td><td class="num">', $cursymbol = $this->GetCurrency($campaign->details['currency'], 'cursymbol'), number_format($campaign->details['target']), '</td><td class="num">', $cursymbol, number_format($raised['raised'], 2), '</td><td class="num">&pound;', number_format($raised['raised_gbp'], 2), '</td><td><a href="campaign_addtoteam.php?', $this->getString, '&addid=', $campaign->id, '">add to this</a></td></tr>';
					}
				}
			}
		}
		echo '</table>';
		if ($perpage && (count($campaigns) > $perpage))
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($this->getString)
			{	$pagelink .= '?' . $this->getString;
			}
			$pag = new Pagination($_GET['page'], count($campaigns), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
		return ob_get_clean();
	} // end of fn CampaignsList
	
	protected function CampaignsFilterForm()
	{	ob_start();
		class_exists('Form');
		$startfield = new FormLineDate('', 'start', $this->startdate, $this->datefn->GetYearList(date('Y'), -10));
		$endfield = new FormLineDate('', 'end', $this->enddate, $this->datefn->GetYearList(date('Y'), -10));
		echo '<form class="maFilterForm" action="', $_SERVER['SCRIPT_NAME'], '" method="get"><input type="hidden" name="id" value="', $this->campaign->id, '" /><input type="hidden" name="ctype" value="team" />';
		echo '<span>Created from</span>';
		$startfield->OutputField();
		echo '<span>to</span>';
		$endfield->OutputField();
		echo '<div class="clear"></div><span>Campaign name</span><input type="text" name="cname" value="', $this->InputSafeString($_GET['cname']), '" /><input type="submit" class="submit" value="Get" /><div class="clear"></div><span>Visible</span><input type="checkbox" name="visible" value="1" ', $_GET['visible'] ? 'checked="checked" ' : '', '/><span>Taking donations</span><input type="checkbox" name="enabled" value="1" ', $_GET['enabled'] ? 'checked="checked" ' : '', '/><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn CampaignsFilterForm
	
} // end of defn CampaignAddToAnotherPage

$page = new CampaignAddToAnotherPage();
$page->Page();
?>