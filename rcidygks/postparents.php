<?php
include_once('sitedef.php');

class PostPostsPage extends AdminPostsPage
{	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'parents';
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		if ($this->post->id)
		{	$this->breadcrumbs->AddCrumb('postparents.php', 'Parent posts');
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->post->ParentsTable();
	} // end of fn AdminPostsBody
	
} // end of defn PostPostsPage

$page = new PostPostsPage();
$page->Page();
?>