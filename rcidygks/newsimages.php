<?php
include_once('sitedef.php');

class NewsImagesPage extends CMSPage
{
	function __construct()
	{	parent::__construct('CMS');
	} //  end of fn __construct
	
	function CMSLoggedInConstruct()
	{	parent::CMSLoggedInConstruct();
		$this->css[] = 'adminnews.css';
		$this->breadcrumbs->AddCrumb('newsimages.php', 'Images');
		
		if (($delimage = urldecode($_GET['delimage'])) && ($image = new NewsImage($delimage)))
		{	if ($image->Delete())
			{	$this->successmessage = '"' . $delimage . '" has been deleted';
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn CMSLoggedInConstruct
	
	function CMSBodyMain()
	{	$this->ListImages();
	} // end of fn CMSBodyMain
	
	function ListImages()
	{	$images = new NewsImages();
		$images->AdminListImages();
	} // end of fn ListImages
	
} // end of defn NewsImagesPage

$page = new NewsImagesPage();
$page->Page();
?>