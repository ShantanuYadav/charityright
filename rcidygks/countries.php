<?php
include_once('sitedef.php');

class CountriesListPage extends CountriesPage
{	
	function CountriesBodyMain()
	{	parent::CountriesBodyMain();
		//$this->FilterForm();
		$this->CtryList();
	} // end of fn CountriesBodyMain
	
	function ContinentList()
	{	$continents = array();
		if ($result = $this->db->Query('SELECT * FROM continents ORDER BY contorder, dispname'))
		{	while ($row = $this->db->FetchArray($result))
			{	$continents[$row['continent']] = $row['dispname'];
			}
		}
		return $continents;
	} // end of fn ContinentList
	
	function FilterForm()
	{	
		class_exists('Form');
		$contselect = new FormLineSelect('', 'continent', $_GET['continent'], '', $this->ContinentList(), true, 0, '', 
															'-- all --');
		echo '<form id="orderSelectForm" action="', $_SERVER['SCRIPT_NAME'], 
					'" method="get"><label for="continent" class="from">Continent</label>';
		$contselect->OutputField();
		echo '<input type="submit" class="submit" value="Get" /><div class="clear"></div></form>';
	} // end of fn FilterForm
	
	function CtryList()
	{	echo '<table><tr><th></th><th>Code</th><th>Cont.</th><th class="centre">List order</th><th>Use for posts</th><th>Actions</th></tr>';
		foreach ($this->Countries() as $ctry)
		{	
			echo '<tr class="stripe', $i++ % 2, '" id="tr', $ctry->code, '"><td>', $this->InputSafeString($ctry->details['shortname']), '</td><td>', $ctry->details['shortcode'], '</td><td>', $this->user->CanUserAccess('administration') ? ('<a href="continentedit.php?continent=' . $ctry->details['continent'] . '">') : '', $ctry->details['continent'], $this->user->CanUserAccess('administration') ? '</a>' : '', '</td><td class="centre">', $ctry->details['toplist'] ? $ctry->details['toplist'] : '', '</td><td>', $ctry->details['postslug'] ? $this->InputSafeString('yes: ' . $ctry->details['postslug']) : '', '</td><td><a href="ctryedit.php?ctry=', $ctry->code, '">edit</a>';
			if ($histlink = $this->DisplayHistoryLink('countries', $ctry->code))
			{	echo '&nbsp;|&nbsp;', $histlink;
			}
			if ($ctry->CanDelete($ctry->details['toplist']))
			{	echo '&nbsp;|&nbsp;<a href="ctryedit.php?ctry=', $ctry->code, '&delete=1">delete</a>';
			}
			
			echo '</td></tr>';
		}
		echo '</table>';
		if ($this->CanAdminUser('admin'))
		{	echo '<p><a href="ctryedit.php">Create new country</a></p>';
		}
	} // end of fn CtryList
	
	function Countries()
	{	$countries = array();
		$where = array();
		if ($_GET['continent'])
		{	$where[] = 'continent="' . $_GET['continent'] . '"';
		}
		
		$sql = 'SELECT countries.*, IF(toplist > 0, 0, 1) AS istoplist FROM countries';
		if ($wstr = implode(' AND ', $where))
		{	$sql .= ' WHERE ' . $wstr;
		}
		$sql .= ' ORDER BY istoplist, toplist, shortname';
		if ($result = $this->db->Query($sql))
		{	while ($row = $this->db->FetchArray($result))
			{	$countries[] = new AdminCountry($row);
			}
		}
		
		return $countries;
	} // end of fn Countries
	
} // end of defn CountriesListPage

$page = new CountriesListPage();
$page->Page();
?>