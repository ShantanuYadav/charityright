<?php
include_once('sitedef.php');

class PostImagesPage extends AdminPostsPage
{	private $section;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function PostConstructFunctions()
	{	$this->menuarea = 'sections';
		$this->js[] = 'tiny_mce/jquery.tinymce.js';
		$this->js[] = 'pageedit_tiny_mce.js';
		if (isset($_POST['psectiontext']))
		{	$saved = $this->section->Save($_POST, $this->post->id);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		if ($_GET['delete'] && $_GET['confirm'])
		{	if ($this->section->Delete())
			{	header('location: postsections.php?id=' . $this->post->id);
				exit;
			} else
			{	$this->failmessage = 'delete failed';
			}
		}
	} // end of fn PostConstructFunctions
	
	protected function AdminPostsLoggedInConstruct()
	{	parent::AdminPostsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('postsections.php', 'Extra sections');
		if ($this->section->id)
		{	$this->breadcrumbs->AddCrumb('postsection.php?id=' . $this->section->id, $this->InputSafeString($this->section->details['psection']));
		} else
		{	$this->breadcrumbs->AddCrumb('postsection.php?postid=' . $this->id . '&psection=' . ($psection = $_GET['psection'] ? $_GET['psection'] : $_POST['psection']), $psection);
		}
	} // end of fn AdminPostsLoggedInConstruct
	
	protected function AssignPost()
	{	$this->section = new AdminPostSection($_GET['id']);
		$this->post = new AdminPost($this->section->id ? $this->section->details['postid'] : $_GET['postid']);
	} // end of fn AssignPost
	
	protected function AdminPostsBody()
	{	parent::AdminPostsBody();
		echo $this->section->InputForm($this->post->id, $_GET['psection']);
	} // end of fn AdminPostsBody
	
} // end of defn PostImagesPage

$page = new PostImagesPage();
$page->Page();
?>