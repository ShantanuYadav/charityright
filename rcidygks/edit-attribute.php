<?php
include_once('sitedef.php');

class PBEditAttributePage extends AdminPlateBuilderPage
{	var $attribute;

	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	function PBLoggedInConstruct()
	{	parent::PBLoggedInConstruct();
		$this->attribute = AdminCategoryFactory::createAttribute($_GET['id']);
				
		// Handle form update
		if(isset($_POST['edit_submit']) && isset($_POST['attribute']))
		{	
			if($this->attribute->saveEditAttributeForm($_POST['attribute']))
			{
				header('location: pbcategories.php');
				exit;
			}
		}
		
		$this->breadcrumbs->AddCrumb('pbcategories.php', 'Attributes');
	//	$this->breadcrumbs->AddCrumb('', $this->attribute->getCategory()->display_name);
		$this->breadcrumbs->AddCrumb('edit-attribute.php?id=' . $this->attribute->id, $this->attribute->display_name . ' - edit');
	} // end of fn PBLoggedInConstruct
	
	function PlateBuilderBody()
	{	echo '<div id="new-attribute-value">', $this->attribute->outputEditAttributeForm($this->attribute->id), '</div>';
		//$this->VarDump($this->attribute);
	} // end of fn PlateBuilderBody
	
} // end of defn PBEditAttributePage

$page = new PBEditAttributePage();
$page->Page();
?>