<?php
include_once('sitedef.php');

class PeoplePage extends AdminPeoplePage
{	
	function __construct()
	{	parent::__construct();
	} //  end of fn __construct
	
	protected function AdminPeopleBody()
	{	$perpage = 20;
		echo '<table><tr class="newlink"><th colspan="7"><a href="person.php">Add new person</a></th></tr><tr><th></th><th>Name</th><th>Title</th><th>Type</th><th>List order</th><th>Live?</th><th>Actions</th></tr>';
		if (($people = new People()) && $people->people)
		{	if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;
		
			foreach ($people->people as $person_row)
			{	if (++$count > $start)
				{	if ($count > $end)
					{	break;
					}
					$person = new AdminPerson($person_row);
					echo '<tr class="stripe', $i++ % 2,  '"><td>';
					if ($image_url = $person->GetImageSRC('default'))
					{	echo '<img src="', $image_url, '" />';
					}
					echo '</td><td>', $this->InputSafeString($person->details['pname']), '</td><td>', $this->InputSafeString($person->details['ptitle']), '</td><td>', $this->InputSafeString($person->details['ptype']), '</td><td>', (int)$person->details['listorder'], '</td><td>', $person->details['live'] ? 'Live' : '', '</td><td><a href="person.php?id=', $person->id, '">edit</a>';
					if ($person->CanDelete())
					{	echo '&nbsp;|&nbsp;<a href="person.php?id=', $person->id, '&delete=1">delete</a>';
					}
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
		if (count($people->people) > $perpage)
		{	$pagelink = $_SERVER['SCRIPT_NAME'];
			if ($_GET)
			{	$get = array();
				foreach ($_GET as $key=>$value)
				{	if ($value && ($key != 'page'))
					{	$get[] = $key . '=' . $value;
					}
				}
				if ($get)
				{	$pagelink .= '?' . implode('&', $get);
				}
			}
			$pag = new Pagination($_GET['page'], count($people->people), $perpage, $pagelink, array(), 'page');
			echo '<div class="pagination">', $pag->Display(), '</div>';
		}
	} // end of fn AdminPeopleBody
	
	private function GetPeople()
	{	$tables = array('people'=>'people');
		$fields = array('people.*');
		$where = array();
		$orderby = array('people.listorder ASC', 'people.pname ASC', 'people.pid ASC');
		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'pid');
	} // end of fn GetPeople
	
} // end of defn PeoplePage

$page = new PeoplePage();
$page->Page();
?>