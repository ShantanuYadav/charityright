<?php
include_once('sitedef.php');

class DonationLinkBuilderPage extends AdminDonationOptionsPage
{	private $donation;
	
	protected function AdminDonationOptionsLoggedInConstruct()
	{	parent::AdminDonationOptionsLoggedInConstruct();
		$this->breadcrumbs->AddCrumb('donationlinkbuilder.php', 'Link builder');
		$this->js['admin_donationlinkbuilder'] = 'admin_donationlinkbuilder.js';
		$this->css['admin_donationlinkbuilder'] = 'admin_donationlinkbuilder.css';
		$this->bodyOnLoadJS[] = 'DLBBuildLink();';
		$this->donation = new Donation();
	} // end of fn AdminDonationOptionsLoggedInConstruct

	protected function AdminDonationOptionsBody()
	{	$countries = $this->donation->GetDonationCountries('oneoff', true);
		$types = array('oneoff'=>'one-off', 'monthly'=>'monthly (DD)');
		echo '<div class="dlbResultContainer"><form onsubmit="return false;"><p><label>Your link:</label><input type="text" value="" /><br /><script>$(\'.dlbResultContainer form input[type="text"]\').click(function() {$(this).select();});</script></form></div><div class="dlbFormContainer"><form onsubmit="return false;">
				<label>Type</label><select id="dlb_type" onchange="DLBTypeChange();">';
		foreach ($types as $key=>$text)
		{	echo '<option value="', $key, '"', !$tcount++ ? ' selected="selected"' : '', '>', $text, '</option>';
		}
		echo '</select><br />
				<label>Cause</label><select id="dlb_country" onchange="DLBChangeCountry();">';
		foreach ($countries as $country)
		{	if (!$def_country)
			{	$def_country = $country;
			}
			echo '<option value="', $country['dcid'], '"', $country['dcid'] == $def_country['dcid'] ? ' selected="selected"' : '', '>', $this->InputSafeString($country['shortname']), '</option>';
		}
		echo '</select><br />
				<label>Area</label><select id="dlb_project" onchange="DLBBuildLink();">';
		foreach ($def_country['projects'] as $project)
		{	if (!$def_project)
			{	$def_project = $project;
			}
			echo '<option value="', $project['dpid'], '"', $project['dpid'] == $def_project['dpid'] ? ' selected="selected"' : '', '>', $this->InputSafeString($project['projectname']), '</option>';
		}
		echo '</select><br />
				<label>Currency</label><select id="dlb_currency" onchange="DLBBuildLink();">';
		foreach ($this->GetCurrencies() as $curcode=>$currency)
		{	
			echo '<option value="', $curcode, '"', !$curcount++ ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
		}
		echo '</select><br />
				<label>Amount (GBP)</label><input type="text" id="dlb_amount" class="number" value="0" onkeyup="DLBBuildLink();" /><br />
				<label>Zakat</label><input type="checkbox" id="dlb_zakat" onchange="DLBBuildLink();" /><br />
				<label>Quick donate</label><input type="checkbox" id="dlb_quick" onchange="DLBBuildLink();" /><br />
				<label>Full URL (for external use)</label><input type="checkbox" id="dlb_full" onchange="DLBBuildLink();" /><br />
			</form></div>';
	} // end of fn AdminDonationOptionsBody

	private function GetCurrencies($type = 'oneoff')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetCurrencies
	
} // end of defn DonationLinkBuilderPage

$page = new DonationLinkBuilderPage();
$page->Page();
?>