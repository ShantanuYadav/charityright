<?php
require_once('init.php');

class DonatePageAjax extends LPDonatePageBangladesh
{

	public function __construct()
	{	parent::__construct($dummy, 1);

		switch ($_GET['action'])
		{	case 'projectlistnew':
				$output = array('test'=>'');
				$output['projectlist'] = $this->DonationProjectsListNew($_GET['type'], $_GET['country'], $_GET['project']);
				$was_fixed = $_GET['quantity'] > 0;
				$countries = $this->donation->GetDonationCountries($_GET['type'], true);
				$output['nozakat'] = (bool)$countries[$_GET['country']]['nozakat'];
				if ($projects = $countries[$_GET['country']]['projects'])
				{	if (!$projects[$_GET['project']])
					{	foreach ($projects as $project=>$details)
						{	$output['test'] = $_GET['project'] = $project;
							break;
						}
					}
					$output['projectlist2'] = $this->DonationProjects2ListNew($_GET['type'], $_GET['country'], $_GET['project']);
					
					if ($projects2 = $projects[$_GET['project']]['projects'])
					{	if (!$projects2[$_GET['project2']])
						{	foreach ($projects2 as $project2=>$details)
							{	$_GET['project2'] = $project2;
								if ($projects2[$_GET['project2']]['prices'])
								{	$_GET['amount'] = $projects2[$_GET['project2']]['prices'][$_GET['currency']];
									if ($_GET['quantity'])
									{	$_GET['amount'] = $_GET['amount'] * $_GET['quantity'];
									}
								}
								break;
							}
						}
					} else
					{	if ($projects[$_GET['project']]['prices'])
						{	$_GET['amount'] = $projects[$_GET['project']]['prices'][$_GET['currency']];
							if ($_GET['quantity'])
							{	$_GET['amount'] = $_GET['amount'] * $_GET['quantity'];
							}
						}
					}
					
				}
				$output['donationamount'] = $this->DonationAmountContainer($_GET['type'], $_GET['amount'], $_GET['country'], $_GET['project'], $_GET['project2'], $_GET['quantity'], $was_fixed, $_GET['currency']);
				echo json_encode($output);
				break;
			case 'projectlistnew2':
				$output = array('test'=>'');
				$countries = $this->donation->GetDonationCountries($_GET['type'], true);
				$was_fixed = $_GET['quantity'] > 0;
				$output['projectlist2'] = $this->DonationProjects2ListNew($_GET['type'], $_GET['country'], $_GET['project'], $_GET['project2']);
				if ($projects = $countries[$_GET['country']]['projects'])
				{	if ($projects2 = $projects[$_GET['project']]['projects'])
					{	if (!$projects2[$_GET['project2']])
						{	foreach ($projects2 as $project2=>$details)
							{	$_GET['project2'] = $project2;
								if ($projects2[$_GET['project2']]['prices'])
								{	$_GET['amount'] = $projects2[$_GET['project2']]['prices'][$_GET['currency']];
									if ($_GET['quantity'])
									{	$_GET['amount'] = $_GET['amount'] * $_GET['quantity'];
									}
								}
								break;
							}
						}
					} else
					{	if ($projects[$_GET['project']]['prices'])
						{	$_GET['amount'] = $projects[$_GET['project']]['prices'][$_GET['currency']];
							if ($_GET['quantity'])
							{	$_GET['amount'] = $_GET['amount'] * $_GET['quantity'];
							}
						}
					}
					
					$output['donationamount'] = $this->DonationAmountContainer($_GET['type'], $_GET['amount'], $_GET['country'], $_GET['project'], $_GET['project2'], $_GET['quantity'], $was_fixed, $_GET['currency']);
				}
				echo json_encode($output);
				break;
			case 'projectlist2change':
				$output = array('test'=>'');
				$countries = $this->donation->GetDonationCountries($_GET['type'], true);
				$was_fixed = $_GET['quantity'] > 0;
				if ($projects = $countries[$_GET['country']]['projects'])
				{	if ($projects2 = $countries[$_GET['country']]['projects'][$_GET['project']]['projects'][$_GET['project2']])
					{	if ($projects2['prices'])
						{	$_GET['amount'] = $projects2['prices'][$_GET['currency']];
							if ($_GET['quantity'])
							{	$_GET['amount'] = $_GET['amount'] * $_GET['quantity'];
							}
						}
					}
					
					$output['donationamount'] = $this->DonationAmountContainer($_GET['type'], $_GET['amount'], $_GET['country'], $_GET['project'], $_GET['project2'], $_GET['quantity'], $was_fixed, $_GET['currency']);
				}
				echo json_encode($output);
				break;
			case 'typechangenew':
				$output = array('test'=>'');
				$output['currencies'] = $this->CurrencyChooserNew($_GET['type'], $_GET['currency']);

				$countries = $this->donation->GetDonationCountries($_GET['type'], true);
				if (!$countries[$_GET['country']])
				{	$_GET['country'] = '';
				}
				$output['countrylist'] = $this->DonationCountriesListNew($_GET['type'], $_GET['country']);
				$output['projectlist'] = $this->DonationProjectsListNew($_GET['type'], $_GET['country'], $_GET['project']);
				$output['nozakat'] = (bool)$countries[$_GET['country']]['nozakat'];

				if ($projects = $countries[$_GET['country']]['projects'])
				{	if (!$projects[$_GET['project']])
					{	foreach ($projects as $project=>$details)
						{	$_GET['project'] = $project;
							break;
						}
					}
					$output['projectlist2'] = $this->DonationProjects2ListNew($_GET['type'], $_GET['country'], $_GET['project']);
					
					if ($projects2 = $projects[$_GET['project']]['projects'])
					{	if (!$projects2[$_GET['project2']])
						{	foreach ($projects2 as $project2=>$details)
							{	$_GET['project2'] = $project2;
								if ($projects2[$_GET['project2']]['prices'])
								{	$_GET['amount'] = $projects2[$_GET['project2']]['prices'][$_GET['currency']];
								}
								break;
							}
						}
					} else
					{	if ($projects[$_GET['project']]['prices'])
						{	$_GET['amount'] = $projects[$_GET['project']]['prices'][$_GET['currency']];
						}
					}
					
				}
				$output['donationamount'] = $this->DonationAmountContainer($_GET['type'], $_GET['amount'], $_GET['country'], $_GET['project'], $_GET['project2'], $_GET['quantity'], $was_fixed);
				echo json_encode($output);
				break;
			case 'currencychange':
				$output = array('test'=>'');
				if ($newcurrency = $this->GetCurrency($_GET['newcurrency']))
				{	$output['cursymbol'] = $newcurrency['cursymbol'];
					if ($amount = (int)$_GET['amount'])
					{	if ($oldcurrency = $this->GetCurrency($_GET['oldcurrency']))
						{	$output['oldcursymbol'] = $oldcurrency['cursymbol'];
							$output['amount'] = number_format(($amount * $newcurrency['convertrate']) / $oldcurrency['convertrate'], 2, '.', '');
						}
					}
					if ($adminamount = (int)$_GET['adminamount'])
					{	if ($oldcurrency = $this->GetCurrency($_GET['oldcurrency']))
						{	$output['oldcursymbol'] = $oldcurrency['cursymbol'];
							$output['adminamount'] = number_format(($adminamount * $newcurrency['convertrate']) / $oldcurrency['convertrate'], 2, '.', '');
						}
					}
					$output['presets'] = $this->PresetsList($newcurrency['curcode'], $output['amount'], $_GET['type']);
				}
				echo json_encode($output);
				break;
			case 'currencychangenew':
				$output = array('test'=>'');
				if ($newcurrency = $this->GetCurrency($_GET['newcurrency']))
				{	$output['cursymbol'] = $newcurrency['cursymbol'];
						
					$countries = $this->donation->GetDonationCountries($_GET['type'], true);
					if (($projects = $countries[$_GET['country']]['projects']) && ($project = $countries[$_GET['country']]['projects'][$_GET['project']]))
					{	if ($project['prices'])
						{	if ($project['prices'][$_GET['newcurrency']] > 0)
							{	$_GET['amount'] = $project['prices'][$_GET['newcurrency']];
								if ($_GET['quantity'])
								{	$_GET['amount'] = $_GET['amount'] * $_GET['quantity'];
								}
							}
						} else
						{	
							if ($project2 = $countries[$_GET['country']]['projects'][$_GET['project']]['projects'][$_GET['project2']])
							{	if ($project2['prices'] && ($project2['prices'][$_GET['newcurrency']] > 0))
								{	$_GET['amount'] = $project2['prices'][$_GET['newcurrency']];
									if ($_GET['quantity'])
									{	$_GET['amount'] = $_GET['amount'] * $_GET['quantity'];
									}
								}// else $output['test'] = 'no price found';
							}
						}
						$output['donationamount'] = $this->DonationAmountContainer($_GET['type'], $_GET['amount'], $_GET['country'], $_GET['project'], $_GET['project2'], $_GET['quantity'], $_GET['quantity'] > 0, $_GET['newcurrency']);
						
					}
					$output['donationtype'] = $this->DonationTypeContainer($_GET['type'], $_GET['newcurrency']);
					$output['test'] = $newcurrency;
					
				}
				echo json_encode($output);
				break;
			case 'donationformreset':
				$output = array('test'=>'');
				unset($_SESSION[$this->session_name]);
				if (is_array($dtypes = $this->donation->GetDonationTypes()))
				{	foreach ($dtypes as $donation_type=>$ddetails)
					{	$amount = 0;
						if (is_array($countries = $this->donation->GetDonationCountries($output['typevalue'] = $donation_type, true)) && $countries)
						{	foreach ($countries as $country=>$cdetails)
							{	$output['donationtype'] = $this->DonationTypeContainer($donation_type);
								$output['countrylist'] = $this->DonationCountriesListNew($donation_type, $country);
								$output['currencies'] = $this->DonationCurrencyChooserContainer($donation_type, $_GET['currency']);
								if ($cdetails['projects'] && is_array($cdetails['projects']))
								{	foreach ($cdetails['projects'] as $project=>$pdetails)
									{	$output['projectlist'] = $this->DonationProjectsListNew($donation_type, $country, $project);
										if ($pdetails['projects'] && is_array($pdetails['projects']))
										{	foreach ($pdetails['projects'] as $project2=>$p2details)
											{	$output['projectlist2'] = $this->DonationProjects2ListNew($donation_type, $country, $project);
												// check for fixed prices for project 2
												if ($p2details['prices'] && ($p2details['prices'][$_GET['currency']] > 0))
												{	$amount = $p2details['prices'][$_GET['currency']];
												}
												break;
											}
										} else
										{	// check for fixed prices for project
											if ($pdetails['prices'] && ($pdetails['prices'][$_GET['currency']] > 0))
											{	$amount = $pdetails['prices'][$_GET['currency']];
											}
										}
										break;
									}
								}
								break;
							}
						}
						$output['donationamount'] = $this->DonationAmountContainer($donation_type, $amount, $country, $project, $project2, 1, true, $_GET['currency']);
						break;
					}
				}
				echo json_encode($output);
				break;
		}

	} //  end of fn __construct

	protected function PageAssign(&$page){}

} // end of class DonatePageAjax

$page = new DonatePageAjax();
?>