<?php
require_once('init.php');

class CRStarsCampaignsPage extends CRStarsPage
{
	public function __construct()
	{	parent::__construct('cr-stars');
	} // end of fn __construct

	public function MainBodyContent()
	{	$this->XSSSafeAllGet();
		$this->XSSSafeAllPost();
		
		echo $this->CampaignListPageHeader(), '<div class="container campaignsPage"><div class="container_inner">';
		if ($campaigns = $this->GetCampaigns())
		{	$perpage = 12;
			if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;

			echo $this->MainCampaignsList($campaigns, $start, $end);
			if (count($campaigns) > $perpage)
			{	$pagelink = $_SERVER['REQUEST_URI'];
				if ($qpos = strpos($pagelink, '?'))
				{	$pagelink = substr($pagelink, 0, $qpos);
				}
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
					if ($get)
					{	$pagelink .= '?' . implode('&', $get);
					}
				}
				if (($count_campaigns = count($campaigns)) > ($perpage * 12))
				{	$count_campaigns = $perpage * 12;
				}
				if (SITE_TEST)
				{	$count_campaigns = count($campaigns);
				}
				$pag = new Pagination($_GET['page'], $count_campaigns, $perpage, $pagelink, array(), 'page');
				if (SITE_TEST)
				{	echo '<div class="pagination">', $pag->DisplayMaxPages('', 5), '</div>';
				} else
				{	echo '<div class="pagination">', $pag->Display(''), '</div>';
				}
			}

		} else
		{	echo '<h3 class="alert-box alert-box-orange no-campaigns">No campaigns yet. Why not create a campaign? <a href="'.SITE_URL.'cr-stars/">Find out more</a>.</h3>';
		}
		//$leaders = new CampaignLeaders();
		//$this->VarDump($leaders->Leaders());
		echo '</div></div>';
	} // end of fn MainBodyContent
	
	protected function HeaderLinksSearch(){}
	protected function MobileExtraLinksTop(){}

} // end of class CRStarsCampaignsPage

$page = new CRStarsCampaignsPage();
$page->Page();
?>