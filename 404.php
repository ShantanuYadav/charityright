<?php
require_once('init.php');

class NotFoundPage extends BasePage
{	//var $page;

	function __construct()
	{	parent::__construct("");
		$this->css[] = 'page.css';
	} // end of fn __construct

	function MainBodyContent()
	{	echo '<div class="container page-header"><div class="container_inner"><h1 class="page_heading">Page Not Found</h1></div></div>';
		echo '<div class="container page-section page-section-white"><div class="container_inner">';
		echo '<h3>Oops! The page you were looking for, no longer exists.</h3>';
		echo '</div></div>';

	} // end of fn MemberBody

} // end of defn NotFoundPage

$page = new NotFoundPage();
$page->Page();
?>