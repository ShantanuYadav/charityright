<?php 
require_once('../../init.php');

$_GET['param1'] = 'food-packs';
$_GET['param2'] = 'sudan';

$donationpage = new LPDonatePageSudan();
$donation = $donationpage->Donation();

//if (SITE_TEST) $donationpage->VarDump($_SESSION[$donationpage->SessionName()]);
?><!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Sponsor A Child In Sudan | Charity Right</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="/css/jqModal.css" media="screen" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqModal/1.4.2/jqModal.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>
	<script type="text/javascript" src="/js/donations.js"></script>
	<script type="text/javascript" src="js/lpconcept.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans:300,400,600|Roboto+Condensed:700|Roboto:400,700,900" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css" />   
	<script type="text/javascript">
		var donation_ajax_endpoint = "ajax_donationslp_sudan.php";
		var jsSiteRoot = "/";
		var $ = jQuery.noConflict();
	</script>
	<!-- css for slider -->
	<!--[if IE 9]>
	<link rel="stylesheet" type="text/css" href="css/ie.css" />
	<![endif]-->
	<!--[if IE]>
	<script src="js/html5shiv.min.js"></script>
	<![endif]-->
<?php echo $donationpage->HeaderTrackingCode(); ?>
</head>
<body class="page2">
<?php echo $donationpage->BodyTopTrackingCode(); ?>
<section class="nav_head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-5">
                <img class="img-fluid logo1" alt="Charity Right" src="img/logo.png" />
            </div>
            <div class="col-md-8 col-7 align-self-center">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="tel:01274 400389"><img class="img-fluid" alt="phone" src="img/mobile.png" />01274 400389</a></li>
                    <li class="list-inline-item"><a href="mailto:info@charityright.org.uk" target="_blank"><img class="img-fluid" alt="email" src="img/email.png" />info@charityright.org.uk</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="header_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Help Feed A Refugee Family,<br />
                <span>This Ramadan</span></h1>
                <p>£420 will feed a family in Sudan for an entire year.<br />
                   £35 will feed them for a month.</p>
                <a href="#donate_form" class="donate_btn roboto">Donate Now</a>
            </div>
        </div>
    </div>
</section>
<section class="donate_sec">
    <div class="container donate_bg">
        <div class="row">
           <div class="col-md-6">
               <div class="donate_text">
                   <img class="img-fluid mx-auto d-block logo_donate" alt="Donate" src="img/logo1.png" />
                   <p>We as muslims will skip lunch during ramadan. Let’s not stop there. Let’s donate the money we would have spent on lunch to the less fortunate</p>
                   <h4 class="help_text">HELP THEM</h4>
               </div>
           </div> 
           <div class="col-md-6">
               <div class="form_class">
                    <form id="donate_form">
                        <h2>What are you waiting for <br />
                            <span class="roboto">Donate for a Better future</span></h2>
                        <?php 
							echo '<div class="form-group donation_type_container">', $donationpage->DonationTypeContainer($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_currency']), '</div>
								<div class="form-group donation-country-list">', $donationpage->DonationCountriesListNew($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_country']), '</div>
								<div class="form-group donation-project-list">', $donationpage->DonationProjectsListNew($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_country'], $_SESSION[$donationpage->SessionName()]['donation_project']), '</div>
								<div class="form-group donation-project2-list">', $donationpage->DonationProjects2ListNew($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_country'], $_SESSION[$donationpage->SessionName()]['donation_project'], $_SESSION[$donationpage->SessionName()]['donation_project2']), '</div>
								<input type="hidden" name="donation_oldcurrency" value="', $_SESSION[$donationpage->SessionName()]['donation_currency'], '" />
								<div class="form-group donation-currency-list" id="donationCurrencyChooserContainer">', $donationpage->DonationCurrencyChooserContainer($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_currency']), '</div>
								';
						?>
                        <div class="amount_class">
                            <div class="row">
                                <div class="col-md-5">
                                    <p>Donation amount</p>
                                </div>
                                <div class="col-md-7 donation_amount_container">
									<?php echo $donationpage->DonationAmountContainer($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_amount'], $_SESSION[$donationpage->SessionName()]['donation_country'], $_SESSION[$donationpage->SessionName()]['donation_project'], $_SESSION[$donationpage->SessionName()]['donation_project2'], $_SESSION[$donationpage->SessionName()]['donation_quantity'], false, $_SESSION[$donationpage->SessionName()]['donation_currency']); ?>
                                </div>
                            </div>
                        </div>
                            <?php echo '<div class="form-group dpZakatContainer', $_SESSION[$donationpage->SessionName()]['donation_nozakat'] ? ' hidden' : '', '">
											<label for="donation_zakat">Is your donation Zakat?</label>
											<select class="form-control" id="donation_zakat" name="donation_zakat">
												<option value="0" ', !($_SESSION[$donationpage->SessionName()]['donation_zakat']) ? 'selected="selected"' : '', '>No</option>
												<option value="1" ', ($_SESSION[$donationpage->SessionName()]['donation_zakat']) ? 'selected="selected"' : '', '>Yes</option>
											</select>
										</div>
										<div class="form-group donation-admin">
											<label for="sel1" class="lab_height">Would you like to donate towards the administration cost of running our projects? (<span class="donation-currency-symbol">', $donationpage->GetCurrency($_SESSION[$donationpage->SessionName()]['donation_currency'], 'cursymbol'), '</span>)</label>
											<input type="text" class="form-control donation-admin" id="donation-admin" name="donation_admin" style="text-align: right;" value="', number_format($_SESSION[$donationpage->SessionName()]['donation_admin'], 2, '.', ''), '" />
										</div>
										';
                            ?>
                        <button class="btn_cart" type="button" onclick="DonationAddToCart();">Make Donation</button>
                    </form>
                </div>
           </div> 
        </div>
    </div>
</section>
<section class="feed_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="lato heading_text">In 2018, we distributed 9.2 million meals. With your help,<br> 
                    we can feed more people than ever before.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a class="feed_box feed_left" href="<?php echo SITE_URL; ?>donate/food-packs/sudan/oneoff/GBP/35/?utm_source=Hmpg&utm_medium=Box1">
                    <img class="img-fluid" alt="" src="img/family1.png" />
                    <div class="text_feed">
                        <h3 class="roboto">FEED A FAMILY</h3>
                        <p>£420 will feed a family in Sudan for an entire year.</p>
                        <h4>£35 will feed them for a month.</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feed_box feed_center" href="<?php echo SITE_URL; ?>donate/schools/wnig/oneoff/GBP/10/?utm_source=Hmpg&utm_medium=Box2">
                    <img class="img-fluid" alt="" src="img/school1.png" />
                    <div class="text_feed">
                        <h3 class="roboto">FEED A schoolchild</h3>
                        <p>£120 will feed a schoolchild for a whole year.</p>
                        <h4>£10 will feed them for a month.</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feed_box feed_right" href="<?php echo SITE_URL; ?>donate/hifzmeals/wnig/oneoff/GBP/20/?utm_source=Hmpg&utm_medium=Box3">
                    <img class="img-fluid" alt="" src="img/student1.png" />
                    <div class="text_feed">
                        <h3 class="roboto">FEED A hifz student</h3>
                        <p>£240 will feed a hifz student in school for an entire year.</p>
                        <h4>£20 will feed them for a month.</h4>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
               <img class="img-fluid logo" alt="" src="img/logo.png" /> 
            </div>
            <div class="col-md-4">
                <ul class="list-unstyled">
                    <li><span class="img_class"><img class="img-fluid" alt="location" src="img/location.png" /></span> <span class="text_class">Oakwood Court, City Road, Bradford<br>
                          West Yorkshire, United Kingdom BD8 8JY</span></li>
                    <li><a href="tel:01274 400389"><span class="img_class"><img class="img-fluid" alt="phone" src="img/phone.png" /></span><span class="text_class">01274 400389</span></a></li>
                    <li><a href="mailto:info@charityright.org.uk" target="_blank"><span class="img_class"><img class="img-fluid" alt="email" src="img/mail.png" /></span><span class="">info@charityright.org.uk</span></a></li>  
                </ul>
            </div>
            <div class="col-md-4">
                <p>Charity Right - Fighting Hunger, One Child at a Time. 
                    Registered Charity Number 1163944</p>
            </div>
        </div>
    </div>
</footer>
<!-- javascript libraries -->
<!--<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>-->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">jQuery.noConflict(); $().ready(function(){$("body").append($(".jqmWindow"));$(".cartModalPopup").jqm();});</script>
<div class="jqmWindow cartModalPopup"><a href="#" class="jqmClose cartCloseTop">&times;</a><div class="cart_modal_popup_inner"></div></div>
</body>
</html>