<?php 
require_once('../../init.php');

$_GET['param1'] = 'schools';
$_GET['param2'] = 'pakistan';

$donationpage = new LPDonatePagePakistanSchools();
$donation = $donationpage->Donation();

//if (SITE_TEST) $donationpage->VarDump($_SESSION[$donationpage->SessionName()]);
?><!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- title -->
	<title>Charity Right in Pakistan | Help School Children</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="/css/jqModal.css" media="screen" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqModal/1.4.2/jqModal.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>
	<script type="text/javascript" src="/js/donations.js"></script>
	<script type="text/javascript" src="js/lpconcept.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans:300,400,600|Roboto+Condensed:700|Roboto:400,700,900" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		var donation_ajax_endpoint = "ajax_donationslp_pak.php";
		var jsSiteRoot = "/";
		var $ = jQuery.noConflict();
	</script>
	<!-- css for slider -->
	<!--[if IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css" />
	<![endif]-->
	<!--[if IE]>
		<script src="js/html5shiv.min.js"></script>
	<![endif]-->
<?php echo $donationpage->HeaderTrackingCode(); ?>
</head>
<body>
<?php echo $donationpage->BodyTopTrackingCode(); ?>
<section class="nav_head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-5">
                <img class="img-fluid logo1" alt="Charity Right" src="img/logo.png" />
            </div>
            <div class="col-md-8 col-7 align-self-center">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="tel:01274 400389"><img class="img-fluid" alt="phone" src="img/mobile.png" />01274 400389</a></li>
                    <li class="list-inline-item"><a href="mailto:info@charityright.org.uk" target="_blank"><img class="img-fluid" alt="email" src="img/email.png" />info@charityright.org.uk</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="header_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 class="roboto_con">Feed A School Child<br />
                <span>This Ramadan</span></h1>
                <p>£120 will feed a schoolchild for a whole year.<br /> 
                   £10 will feed them for a month.</p>
                <a href="#donate_form" class="donate_btn roboto">Donate Now</a>
            </div>
            <div class="col-md-4">
                <img class="img-fluid head_img" alt="" src="img/head_img.png" />
            </div>
        </div>
    </div>
</section>
<section class="donate_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="donate_heading roboto">
                    What are you waiting for <br />
                    <span>Donate for a Better future</span>
                </p>
                <div class="form_class" id="donate_form">
                    <form>
                        <div class="row">
                            <div class="col-md-6 donation_type_container lpconcept_hide_on_empty"><?php 
                            echo $donationpage->DonationTypeContainer($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_currency']); ?></div>
                            <div class="col-md-6 donation-country-list"><?php echo $donationpage->DonationCountriesListNew($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_country']); ?></div>
                            <div class="col-md-6 donation-project-list"><?php echo $donationpage->DonationProjectsListNew($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_country'], $_SESSION[$donationpage->SessionName()]['donation_project']); ?></div>
                            <div class="col-md-6 donation-project2-list"><?php echo $donationpage->DonationProjects2ListNew($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_country'], $_SESSION[$donationpage->SessionName()]['donation_project'], $_SESSION[$donationpage->SessionName()]['donation_project2']); ?></div>
                            <?php echo '<input type="hidden" name="donation_oldcurrency" value="', $_SESSION[$donationpage->SessionName()]['donation_currency'], '" /><div class="col-md-6 donation-currency-list" id="donationCurrencyChooserContainer">', $donationpage->DonationCurrencyChooserContainer($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_currency']); ?></div>
                         </div>
                        <div class="amount_class">
                            <div class="row">
								<div class="col-md-4">
									<p>Donation amount</p>
								</div>
								<div class="col-md-8 donation_amount_container">
									<?php echo $donationpage->DonationAmountContainer($_SESSION[$donationpage->SessionName()]['donation_type'], $_SESSION[$donationpage->SessionName()]['donation_amount'], $_SESSION[$donationpage->SessionName()]['donation_country'], $_SESSION[$donationpage->SessionName()]['donation_project'], $_SESSION[$donationpage->SessionName()]['donation_project2'], $_SESSION[$donationpage->SessionName()]['donation_quantity'], false, $_SESSION[$donationpage->SessionName()]['donation_currency']); ?>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <?php echo '<div class="col-md-6 dpZakatContainer', $_SESSION[$donationpage->SessionName()]['donation_nozakat'] ? ' hidden' : '', '">
											<div class="form-group">
												<label for="donation_zakat" class="lab_height">Is your donation Zakat?</label>
												<select class="form-control" id="donation_zakat" name="donation_zakat">
													<option value="0" ', !($_SESSION[$donationpage->SessionName()]['donation_zakat']) ? 'selected="selected"' : '', '>No</option>
													<option value="1" ', ($_SESSION[$donationpage->SessionName()]['donation_zakat']) ? 'selected="selected"' : '', '>Yes</option>
												</select>
											</div>
										</div>
										<div class="col-md-6 donation-admin">
											<div class="form-group">
												<label for="sel1" class="lab_height">Would you like to donate towards the administration cost of running our projects? (<span class="donation-currency-symbol">', $donationpage->GetCurrency($_SESSION[$donationpage->SessionName()]['donation_currency'], 'cursymbol'), '</span>)</label>
												<input type="text" class="form-control donation-admin" id="donation-admin" name="donation_admin" style="text-align: right;" value="', number_format($_SESSION[$donationpage->SessionName()]['donation_admin'], 2, '.', ''), '" />
											</div>
										</div>';
                            ?>
                        </div>
                        <button class="btn_cart" type="button" onclick="DonationAddToCart();">ADD TO CART</button>
                    </form>
                 </div>
            </div>
        </div>
    </div>
</section>
<section class="banner_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="media">
                  <img src="img/logo.png" alt="" class="img-fluid" />
                  <div class="media-body">
                    <p>We as muslims will skip lunch during ramadan. Let’s not stop there. Let’s donate the money we would have spent on lunch to the less fortunate</p>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="feed_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="lato heading_text">In 2018, we distributed 9.2 million meals. With your help,<br /> 
                    we can feed more people than ever before.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a class="feed_box feed_left" href="<?php echo SITE_URL; ?>donate/food-packs/sudan/oneoff/GBP/35/?utm_source=Hmpg&utm_medium=Box1">
                    <p class="lato">£420 will feed a family in Sudan for an entire year.</p>
                    <h4 class="lato">£35 will feed them for a month.</h4>
                    <img class="img-fluid" alt="" src="img/family.png" />
                    <h3>FEED A FAMILY</h3>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feed_box feed_center" href="<?php echo SITE_URL; ?>donate/schools/wnig/oneoff/GBP/10/?utm_source=Hmpg&utm_medium=Box2">
                    <p class="lato">£120 will feed a schoolchild for a whole year.</p>
                    <h4 class="lato">£10 will feed them for a month.</h4>
                    <img class="img-fluid" alt="" src="img/school.png" />
                    <h3>FEED A schoolchild</h3>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feed_box feed_right" href="<?php echo SITE_URL; ?>donate/hifzmeals/wnig/oneoff/GBP/20/?utm_source=Hmpg&utm_medium=Box3">
                    <p class="lato">£240 will feed a hifz student in school for an entire year.</p>
                    <h4 class="lato">£20 will feed them for a month.</h4>
                    <img class="img-fluid" alt="" src="img/student.png" />
                    <h3>FEED A hifz student</h3>
                </a>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
               <img class="img-fluid logo" alt="img" src="img/logo.png" /> 
            </div>
            <div class="col-md-4">
                <ul class="list-unstyled">
                    <li><span class="img_class"><img class="img-fluid" alt="location" src="img/location.png" /></span> <span class="text_class">Oakwood Court, City Road, Bradford<br />
                          West Yorkshire, United Kingdom BD8 8JY</span></li>
                    <li><a href="tel:01274 400389"><span class="img_class"><img class="img-fluid" alt="phone" src="img/phone.png" /></span><span class="text_class">01274 400389</span></a></li>
                    <li><a href="mailto:info@charityright.org.uk" target="_blank"><span class="img_class"><img class="img-fluid" alt="email" src="img/mail.png" /></span><span class="">info@charityright.org.uk</span></a></li>  
                </ul>
            </div>
            <div class="col-md-4">
                <p>Charity Right - Fighting Hunger, One Child at a Time. 
                    Registered Charity Number 1163944</p>
            </div>
        </div>
    </div>
</footer>
<!-- javascript libraries -->
<!--<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>-->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">jQuery.noConflict(); $().ready(function(){$("body").append($(".jqmWindow"));$(".cartModalPopup").jqm();});</script>
<div class="jqmWindow cartModalPopup"><a href="#" class="jqmClose cartCloseTop">&times;</a><div class="cart_modal_popup_inner"></div></div>
</body>
</html>