<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Sponsor A Child In Sudan | Charity Right</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="/css/jqModal.css" media="screen" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqModal/1.4.2/jqModal.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>
	<script type="text/javascript" src="/js/donations.js"></script>
	<script type="text/javascript" src="js/lpconcept.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans:300,400,600|Roboto+Condensed:700|Roboto:400,700,900" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css" />   
	<script type="text/javascript">
		var donation_ajax_endpoint = "ajax_donationslp_sudan.php";
		var jsSiteRoot = "/";
		var $ = jQuery.noConflict();
	</script>
	<!-- css for slider -->
	<!--[if IE 9]>
	<link rel="stylesheet" type="text/css" href="css/ie.css" />
	<![endif]-->
	<!--[if IE]>
	<script src="js/html5shiv.min.js"></script>
	<![endif]-->


    <!-- Global site tag (gtag.js) - Google Ads: 925998688 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-925998688"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-925998688');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-62069931-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-62069931-1');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PX5XJXX');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '391990840985330');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=391990840985330&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Pinterest Tag -->
<script>
!function(e){if(!window.pintrk){window.pintrk = function () {
window.pintrk.queue.push(Array.prototype.slice.call(arguments))};var
  n=window.pintrk;n.queue=[],n.version="3.0";var
  t=document.createElement("script");t.async=!0,t.src=e;var
  r=document.getElementsByTagName("script")[0];
  r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js");
pintrk('load', '2612558765069', {em: '<user_email_address>'});
pintrk('page');
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt=""
  src="https://ct.pinterest.com/v3/?event=init&tid=2612558765069&pd[em]=<hashed_email_address>&noscript=1" />
</noscript>
<!-- end Pinterest Tag -->

<!-- Snap Pixel Code -->
<script type='text/javascript'>
(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
{a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
r.src=n;var u=t.getElementsByTagName(s)[0];
u.parentNode.insertBefore(r,u);})(window,document,
'https://sc-static.net/scevent.min.js');

snaptr('init', 'e3fc8fa5-d8f9-47b6-a384-1859c44e5594', {
'user_email': '__INSERT_USER_EMAIL__'
});

snaptr('track', 'PAGE_VIEW');

</script>
<!-- End Snap Pixel Code -->





</head>
<body class="page2"> 

    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PX5XJXX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<section class="nav_head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-5">
                <img class="img-fluid logo1" alt="Charity Right" src="img/logo.png" />
            </div>
            <div class="col-md-8 col-7 align-self-center">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="tel:01274 400389"><img class="img-fluid" alt="phone" src="img/mobile.png" />01274 400389</a></li>
                    <li class="list-inline-item"><a href="mailto:info@charityright.org.uk" target="_blank"><img class="img-fluid" alt="email" src="img/email.png" />info@charityright.org.uk</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="header_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Saves the life of refugee families in Sudan,<br />
                <span>This Ramadan</span></h1>
                <p>$540 WILL FEED A FAMILY IN SUDAN FOR AN ENTIRE YEAR.<br> $45 WILL FEED THEM FOR A MONTH.</p>
                <!-- <a href="#donate_form" class="donate_btn roboto">Donate Now</a> -->

<form action="charge.php" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_live_9J7JiUJNtubpxNklNaLfTNVd"
    data-image="http://localhost/stripe/families-in-sudan/img/logo.png"
    data-name="Families in Sudan"
    data-description="$540 WILL FEED A FAMILY IN SUDAN FOR AN ENTIRE YEAR."
    data-amount="54000">
  </script>
  <script>        
        document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
    </script>
    <button type="submit" class="donate_btn roboto">Donate Now</button>
</form>                
            </div>
        </div>
    </div>
</section>
<section class="donate_sec">
    <div class="container donate_bg">
        <div class="row">
           <div class="col-md-6">
               <div class="donate_text">
                   <img class="img-fluid mx-auto d-block logo_donate" alt="Donate" src="img/logo1.png" />
                   <p>This Ramadan helps us in saving the lives by feeding refugees who are less fortunate. Support us by donating for this noble cause so we can feed those who really need your help.</p>
                   <h4 class="help_text">HELP THEM</h4>
               </div>
           </div> 
           <div class="col-md-6">
               <div class="form_class">
                    <form id="donate_form">
                        <h2>What are you waiting for <br />
                            <span class="roboto">Donate for a Better future</span></h2>
                        
                        <div class="amount_class">
                            <div class="row">
                                <div class="col-md-5">
                                    
                                </div>
                                <div class="col-md-7 donation_amount_container">
									
                                </div>
                            </div>
                        </div>
<form action="charge.php" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_live_9J7JiUJNtubpxNklNaLfTNVd"
    data-image="http://localhost/stripe/families-in-sudan/img/logo.png"
    data-name="Families in Sudan"
    data-description="$540 WILL FEED A FAMILY IN SUDAN FOR AN ENTIRE YEAR."
    data-amount="54000">
  </script>
  <script>        
        document.getElementsByClassName("stripe-button-el")[1].style.display = 'none';
    </script>
    <button type="submit" class="btn_cart">Make Donation</button>
</form>                             
                        
                    </form>
                </div>
           </div> 
        </div>
    </div>
</section>
<section class="feed_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="lato heading_text">We have distributed 5.2 million meals in the year 2019.  Now, we want to feed more people, which is not possible without your support!  </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a class="feed_box feed_left" href="<?php echo SITE_URL; ?>donate/food-packs/sudan/oneoff/GBP/35/?utm_source=Hmpg&utm_medium=Box1">
                    <img class="img-fluid" alt="" src="img/family1.png" />
                    <div class="text_feed">
                        <h3 class="roboto">FEED A FAMILY</h3>
                        <p>Feed a family of 5 for one year by donating $540</p>
                        <p>Donate $270 to feed them for six months </p>
                        <h4>Donate $45 to feed them for a month.</h4>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feed_box feed_center" href="<?php echo SITE_URL; ?>donate/schools/wnig/oneoff/GBP/10/?utm_source=Hmpg&utm_medium=Box2">
                    <img class="img-fluid" alt="" src="img/school1.png" />
                    <div class="text_feed">
                        <h3 class="roboto">FEED A schoolchild</h3>
                        <p>Feed a school child for the entire year by donating $156</p>
                        <p>Donate $78 to feed them for six months</p>
                        <h4>Donate $13 to feed them for one month </h4>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="feed_box feed_right" href="<?php echo SITE_URL; ?>donate/hifzmeals/wnig/oneoff/GBP/20/?utm_source=Hmpg&utm_medium=Box3">
                    <img class="img-fluid" alt="" src="img/student1.png" />
                    <div class="text_feed">
                        <h3 class="roboto">FEED A hifz student</h3>
                        <p>Feed a hifz student in school for entire year by donating $300</p>
                        <p>Donate $150 to feed them for six month</p>
                        <h4>Donate $25 to feed them for one month </h4>

                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
               <img class="img-fluid logo" alt="" src="img/logo.png" /> 
            </div>
            <div class="col-md-4">
                <ul class="list-unstyled">
                    <li><span class="img_class"><img class="img-fluid" alt="location" src="img/location.png" /></span> <span class="text_class">Oakwood Court, City Road, Bradford<br>
                          West Yorkshire, United Kingdom BD8 8JY</span></li>
                    <li><a href="tel:01274 400389"><span class="img_class"><img class="img-fluid" alt="phone" src="img/phone.png" /></span><span class="text_class">01274 400389</span></a></li>
                    <li><a href="mailto:info@charityright.org.uk" target="_blank"><span class="img_class"><img class="img-fluid" alt="email" src="img/mail.png" /></span><span class="">info@charityright.org.uk</span></a></li>  
                </ul>
            </div>
            <div class="col-md-4">
                <p>Charity Right - Fighting Hunger, One Child at a Time. 
                    Registered Charity Number 1163944</p>
            </div>
        </div>
    </div>
</footer>
<!-- javascript libraries -->
<!--<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>-->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">jQuery.noConflict(); $().ready(function(){$("body").append($(".jqmWindow"));$(".cartModalPopup").jqm();});</script>
<div class="jqmWindow cartModalPopup"><a href="#" class="jqmClose cartCloseTop">&times;</a><div class="cart_modal_popup_inner"></div></div>
</body>
</html>