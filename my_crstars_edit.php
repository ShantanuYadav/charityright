<?php
require_once('init.php');

class CRStarsProfileEditPage extends MyCRStarsPage
{
	public function __construct()
	{	parent::__construct('edit');
		if (isset($_POST['firstname']))
		{	$saved = $this->camp_customer->Save($_POST, $_FILES['myimage']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}

	} // end of fn __construct

	function MemberBodyContent()
	{	ob_start();
		$pref = new ContactPreferences(array('email', 'phone', 'text'));
		$pp_page = new PageContent('use-of-cookies');
		echo '<div class="container crstarsRegister"><div class="container_inner"><h1 class="crstarsMyHeader">Editing My Profile</h1><form action="', $this->camp_customer->EditLink(), '" method="post" enctype="multipart/form-data" class="form-rows"><p><label>First name</label><input type="text" name="firstname" value="', $this->InputSafeString($this->camp_customer->details['firstname']), '" /><br class="clear" /></p><p><label>Last name</label><input type="text" name="lastname" value="', $this->InputSafeString($this->camp_customer->details['lastname']), '" /><br class="clear" /></p>
			<p><label>Email address</label><input type="text" name="email" value="', $this->InputSafeString($this->camp_customer->details['email']), '" /><br class="clear" /></p>
			<p><label>Phone number</label><input type="text" name="phone" value="', $this->InputSafeString($this->camp_customer->details['phone']), '" /><br class="clear" /></p>
			<p id="my-new-photo"><label>Your profile photo (ideally square)</label><input type="file" name="myimage" /></p>';
		if ($src = $this->camp_customer->GetImageSRC('medium'))
		{	echo '<p><label>Your current photo</label><img src="' . $src . '" /></p><p><label><input type="checkbox" name="delmyimage" value="1" /> Remove your photo</label></p>';
		}
		if (true || SITE_TEST)
		{	echo '<div class="form-row crRegFormContactPref">
					<p class="cprefTitle">How would you like to receive updates?<br /><span>(Optional)</span></p>
					<p class="cprefHelp">We\'d love to tell you about how your donation helps and keep you in the loop with what Charity Right is up to. Your details will be kept safe and never shared with other organisations. See our <a href="', $pp_page->Link(), '">privacy policy</a>.</p>
					<p>', $pref->FormElementsList($this->camp_customer->details['contactpref']), '</p>
					<p class="cprefHelp">Don\'t forget, you can change your preferences at any time by emailing us at info@charityright.org.uk or in your profile after you have created your campaign.</p>
				</div>
				';
		}
		echo '<p class="regformButtons"><button type="submit">Update My Profile</button></p></form></div></div><div class="clear"></div>';
		return ob_get_clean();
	} // end of fn MemberBodyContent

} // end of class CRStarsProfileEditPage

$page = new CRStarsProfileEditPage();
$page->Page();
?>