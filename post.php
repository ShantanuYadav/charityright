<?php
require_once('init.php');

class PostPage extends BasePage
{	private $post;

	public function __construct()
	{	parent::__construct();
		$this->css['posts'] = 'posts.css';
	} // end of fn __construct

	protected function BaseConstructFunctions()
	{	$this->post = new Post((int)$_GET['postid'], $_GET['postslug']);
		$this->title = $this->InputSafeString($this->post->details['posttitle']);
		if (file_exists(CITDOC_ROOT . '/css/posts_' . $this->post->details['template'] . '.css'))
		{	$this->css['posts_' . $this->post->details['template']] = 'posts_' . $this->post->details['template'] . '.css';
		}
		if ($this->quickdonate = $this->post->GetQuickDonateOptions())
		{	$this->js['quickdonate'] = 'quickdonate.js';
			$this->css['quickdonate'] = 'quickdonate.css';
		}
	} // end of fn BaseConstructFunctions

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		$this->fb_meta['title'] = $this->title;
		if ($this->post->images)
		{	foreach ($this->post->images as $image_row)
			{	$image = new PostImage($image_row);
				if ($src = $image->GetImageSRC('og'))
				{	$this->fb_meta['image'] = $src;
					break;
				}
			}
		}
		$this->fb_meta['url'] = $this->post->Link();
		if ($this->post->details['snippet'])
		{	$this->fb_meta['description'] = $this->InputSafeString($this->post->details['snippet']);
		}
	} // end of fn SetFBMeta

	public function GetMetaDesc()
	{	return $this->post->details['metadesc'];
	} // end of fn GetMetaDesc

	public function SetCanonicalLink()
	{	if ($this->post->id)
		{	$this->canonical_link = $this->post->Link();
		}
	} // end of fn SetCanonicalLink

	public function MainBodyContent()
	{	ob_start();
		@include_once($this->post->TemplateFile());
		echo ob_get_clean();
	} // end of fn MainBodyContent

} // end of class PostPage

$page = new PostPage();
$page->Page();
?>