<?php
require_once('init.php');
require_once 'HTTP/Request.php';
class AjaxOnlinewordpay extends Base
{
  public function __construct()
  {  
    
      parent::__construct();
     
      $alldetail    = $_POST['alldetail'];
      $owner        = $_POST['owner'];
      $cardNumber   = $_POST['cardNumber'];
      $cvv          = $_POST['cvv'];
      $expiry_day   = $_POST['expiry_day'];
      $expiry_year  = $_POST['expiry_year'];
      $sessionccard = $_POST['sessionccard'];
      $alldetail    = json_decode($alldetail);

      $orderid           = $alldetail->details->orderid;
      $orderdate         = $alldetail->details->orderdate;
      $currency          = $alldetail->details->currency;
      $total_oneoff      = floor($alldetail->details->total_oneoff) . '00';
      $total_monthly     = $alldetail->details->total_monthly;
      $giftaid           = $alldetail->details->giftaid;
      $donortitle        = $alldetail->details->donortitle;
      $donorfirstname    = $alldetail->details->donorfirstname;
      $donorsurname      = $alldetail->details->donorsurname;
      $donoradd1         = $alldetail->details->donoradd1;
      $donoradd2         = $alldetail->details->donoradd2;
      $donorcity         = $alldetail->details->donorcity;
      $donorpostcode     = $alldetail->details->donorpostcode;
      $donorcountry      = $alldetail->details->donorcountry;
      $donoremail        = $alldetail->details->donoremail;
      $donorphone        = $alldetail->details->donorphone;
      $donorhowheard     = $alldetail->details->donorhowheard;
      $contactpref       = $alldetail->details->contactpref;
      $ddsolesig         = $alldetail->details->ddsolesig;
      $donorhowheardtext = $alldetail->details->donorhowheardtext;

      // if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      // 	$ip_address = $_SERVER['HTTP_CLIENT_IP'];
      // } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      // 	$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
      // 	$ip = explode(',', $ip_address);
      // 	$ip_address = $ip[0];
      // } else {
      // 	$ip_address = $_SERVER['REMOTE_ADDR'];
      // }
      $ip_address = $_SERVER['SERVER_ADDR'];
      $acceptHeader = $_SERVER['HTTP_ACCEPT'];
      $userAgentHeader = $_SERVER['HTTP_USER_AGENT'];

      if ($donorcity == '') {
        $donorcity = 'Walthamstow';
      }
      if ($donorpostcode == '') {
        $donorpostcode = 'NR20 3DE';
      }
      if ($donorphone == '') {
        $donorphone = '07000000000';
      }


            $xml = "<?xml version=\"1.0\"?>
      <!DOCTYPE paymentService PUBLIC \"-//WorldPay//DTD WorldPay PaymentService v1//EN\" 
      \"http://dtd.wp3.rbsworldpay.com/paymentService_v1.dtd\">
      <paymentService version=\"1.4\" merchantCode=\"CHARITYRIGHTM1\">
      <submit>
      <order orderCode=\"$orderid\" >
      <description>$donortitle</description>
      <amount value=\"$total_oneoff\" currencyCode=\"$currency\" exponent=\"2\"/>
      <orderContent>
      <![CDATA[
      <center><table>
      <tr><td bgcolor=\"#ffff00\">Your Internet Order:</td><td colspan=\"2\" bgcolor=\"#ffff00\" align=\"right\">$orderid</td></tr>
      <tr><td bgcolor=\"#ffff00\">Description:</td><td>$donortitle</td><td align=\"right\">0,$total_oneoff</td></tr>
      <tr><td colspan=\"2\">Subtotal:</td><td align=\"right\">$total_oneoff</td></tr>
      <tr><td colspan=\"2\">VAT: 17.5%</td><td align=\"right\">0</td></tr>
      <tr><td colspan=\"2\">Shipping and Handling:</td><td align=\"right\">0</td></tr>
      <tr><td colspan=\"2\" bgcolor=\"#c0c0c0\">Total cost:</td><td bgcolor=\"#c0c0c0\" align=\"right\">$currency  $total_oneoff</td></tr>
      <tr><td colspan=\"3\">&nbsp;</td></tr>
      <tr><td bgcolor=\"#ffff00\" colspan=\"3\">Your billing address:</td></tr>
      <tr><td colspan=\"3\">$donorfirstname $donorsurname<br>$donoradd1<br>$donorcity<br>$donorpostcode<br>$donorcountry</td></tr>
      <tr><td colspan=\"3\">&nbsp;</td></tr>
      <tr><td bgcolor=\"#ffff00\" colspan=\"3\">Your shipping address:</td></tr>
      <tr><td colspan=\"3\">$donorfirstname $donorsurname<br>
      $donoradd1<br>$donorcity<br>$donorpostcode<br>$donorcountry</td></tr>
      <tr><td colspan=\"3\">&nbsp;</td></tr>
      <tr><td bgcolor=\"#ffff00\" colspan=\"3\">Our contact information:</td></tr>
      <tr><td colspan=\"3\">MYMERCHANT Webshops International<br>$donoradd1 <br>Merchant Town<br>$donorcity<br>$donorcountry  <br>$donoremail<br>$donorphone</td></tr>
      <tr><td colspan=\"3\">&nbsp;</td></tr>
      <tr><td bgcolor=\"#c0c0c0\" colspan=\"3\">Billing notice:</td></tr>
      <tr><td colspan=\"3\">Your payment will be handled by WorldPay<br>This name may appear on your bank statement<br>http://www.worldpay.com</td></tr>
      </table></center>
      ]]>
      </orderContent>
      <paymentDetails>
      <CARD-SSL>
      <cardNumber>$cardNumber</cardNumber>
      <expiryDate>
      <date month=\"$expiry_day\" year=\"$expiry_year\"/>
      </expiryDate>
      <cardHolderName>$owner</cardHolderName>
      <cvc>$cvv</cvc>
      <cardAddress>
      <address>
      <firstName>$donorfirstname</firstName>
      <lastName>$donorsurname</lastName>
      <street>$donoradd1</street>
      <!-- Please note that if no house or apartment number is included
      then a house name can be included using the <houseName> element.-->
      <postalCode>$donorpostcode</postalCode>
      <city>$donorcity</city>
      <countryCode>$donorcountry</countryCode>
      <telephoneNumber>$donorphone</telephoneNumber>
      </address>
      </cardAddress>
      </CARD-SSL>
      <session shopperIPAddress=\"$ip_address\" id=\"0215ui8ib1\" />
      </paymentDetails>
      <shopper>
      <shopperEmailAddress>$donoremail</shopperEmailAddress>
      <browser>
      <!-- <acceptHeader>text/html,application/xhtml+xml,application/xml;q=0.9,*/*;
      q=0.8</acceptHeader>
      <userAgentHeader>Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.1.5)
      Gecko/20091102 Firefox/3.5.5 (.NET CLR 3.5.30729)</userAgentHeader> -->
      
      <acceptHeader>$acceptHeader</acceptHeader>
      <userAgentHeader>$userAgentHeader</userAgentHeader>
      </browser>

      </shopper>
      <shippingAddress>
      <address>
      <firstName>$donorfirstname</firstName>
      <lastName>$donorsurname</lastName>
      <street>$donoradd1</street>
      <!-- We recommend that you include the whole address in this field with the exception of postalCode and CountryCode..-->
      <postalCode>$donorpostcode</postalCode>
      <countryCode>$donorcountry</countryCode>
      <telephoneNumber>$donorphone</telephoneNumber>
      </address>
      </shippingAddress>
      </order>
      </submit>
      </paymentService>";

      //$dest = 'https://7SFTSC44829VSHUY6FT9:4SpVXh9D@secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp'; //for sandbox
      // $dest = 'https://7SFTSC44829VSHUY6FT9:4SpVXh9D@secure.worldpay.com/jsp/merchant/xml/paymentService.jsp'; //for production

	  $dest = 'https://CHARITYRIGHTM1:4SpVXh9D@secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp'; //for sandbox
      // $dest = 'https://CHARITYRIGHTM1:4SpVXh9D@secure.worldpay.com/jsp/merchant/xml/paymentService.jsp'; //for production

      //$dest='https://secure-test.wp3.rbsworldpay.com/jsp/merchant/xml/paymentService.jsp'; //for sandbox
      //$dest='https://secure.wp3.rbsworldpay.com/jsp/merchant/xml/paymentService.jsp' //for production
      $req  = new HTTP_Request($dest);
      $req->setMethod(HTTP_REQUEST_METHOD_POST);
      $req->addHeader('Connection', 'keep-alive');
      $req->addRawPostData($xml);
      $req->sendRequest();
      $req->getResponseBody();
      //echo nl2br(htmlentities($req->getResponseBody()));
      $data   = $req->getResponseBody();
      $xml    = simplexml_load_string($data, NULL, LIBXML_NOCDATA);
      $avc    = json_encode($xml, JSON_PRETTY_PRINT);
      $newArr = json_decode($avc, true);
      // echo '<pre>';print_r($newArr);die;
	  // print_r($newArr);
      if ($newArr['reply']['orderStatus']['payment']['lastEvent'] == 'AUTHORISED') {

 
      $result   = $this->db->query("SELECT * FROM donations where orderid = '" . $orderid . "'");
      $data = $this->db->FetchArray($result);
      $adminamount  = $data['adminamount'];
      $curanciesval = $this->db->query("SELECT * FROM currencies where curcode = '" . $currency . "'");
      $crandcydata  = $this->db->FetchArray($curanciesval);

      $rate         = $crandcydata['convertrate'];
      $cursymbol    = $crandcydata['cursymbol'];
      $donsql       = $this->db->query("UPDATE donations SET donationref = 'WP~" . $orderid . "' , donationconfirmed='" . date('Y-m-d H:s:i') . "' , gateway= 'WorldPay' WHERE did= " . $data['did']);

      $pmtfields    = array(
      'gateway="worldpay"'
      );
      $pmtfields[]  = 'donationref="WP~' . $orderid . '"';
      $pmtfields[]  = 'paymentref="WP~' . $orderid . '"';
      $pmtfields[]  = 'currency="' . $currency . '"';
      $pmtfields[]  = 'paydate="' . date('Y-m-d H:s:i') . '"';
      $pmtfields[]  = 'amount=' . $total_oneoff;
      $pmtfields[]  = 'gbpamount=' . round($total_oneoff / $rate, 2);
      $pmtfields[]  = 'adminamount=' . $adminamount;
      $pmtfields[]  = 'gbpadminamount=' . round($adminamount / $rate, 2);
      $pmtfields[]  = 'payerfirstname="' . $donorfirstname . '"';
      $pmtfields[]  = 'payerlastname="' . $donorsurname . '"';
      $pmtfields[]  = 'payercountry="' . $donorcountry . '"';
      $pmtfields[]  = 'payeremail="' . $donoremail . '"';
      $pmtset       = implode(', ', $pmtfields);
      $pmtsql = 'INSERT INTO donationpayments SET ' . $pmtset;
      if ($pmtresult = $this->db->Query($pmtsql))
      {  

      // $this->Get($orderid);
      $Donation = new Donation();
      $Donation->extraSendOneOffDonationEmail($donoremail,$data);
      $if = new InfusionSoftDonation();
      $if->PayDonation($data);
      } else $fail[] = $pmtsql . '---' . $this->db->Error();

    unset($_SESSION[$sessionccard]);
    $response['message'] = 'success';
    $response['status']  = '1';
    echo json_encode($response);
    
} else if ($newArr['reply']['orderStatus']['payment']['lastEvent'] == 'REFUSED') {
    
    $response['message'] = 'Payment REFUSED VISA_CREDIT-SSL ';
    $response['status']  = '0';
    echo json_encode($response);
    
} else {
		  
		  //echo 'Test';
		  
		  //print_r($data);
    
    $xml    = simplexml_load_string($data, NULL, LIBXML_NOCDATA);
    $avc    = json_encode($xml, JSON_PRETTY_PRINT);
    $newArr = json_decode($avc, true);
    
    mail("charlessamuel733@gmail.com","payment failed",json_encode($newArr));
    
    $messages = $newArr['reply']['error'];
		  
		  
		  // print_r($newArr);
    
    $message2 = str_replace("XML failed validation:", " ", $messages);
    if ($message2) {
        
        $message = $message2;
        
    } else {
        $message = $newArr['reply']['orderStatus']['error'];
    }
    $response['message'] = $message;
    $response['status']  = '0';
    echo json_encode($response);
    
}

  }

}
$page = new AjaxOnlinewordpay();


//require_once('config/config.php');
//$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
//http://support.worldpay.com/support/kb/gg/sepa/content/recurringpayments.htm









?> 