<?php
include_once('init.php');

class CartDocsPage extends CartPage
{	protected $cartPageName = 'Supporting Documents';

	public function __construct() // constructor
	{	parent::__construct();
	
		if (!$this->GetCartPlates())
		{	header('location: ' . SITE_URL . 'cart_addr.php');
			exit;
		}

		$successmessage = array();
		$failmessage = array();
		
		if ($this->customer->id)
		{	if ((is_array($_FILES['platedocs']) && $_FILES['platedocs']) || (is_array($_POST) && $_POST))
			{	$saved = $this->SavePlateDocs($_POST, $_FILES['platedocs']);
				if ($saved['failmessage'])
				{	$failmessage[] = $saved['failmessage'];
				}
				if ($saved['successmessage'])
				{	$successmessage[] = $saved['successmessage'];
				}
			
				if ($this->PlateDocsDone())
				{	header('location: ' . SITE_URL . 'cart_addr.php');
					exit;
				} else
				{	$failmessage[] = 'You must provide supporting documents';
				}
			}
			
			$this->failmessage = implode(', ', $failmessage);
			$this->successmessage = implode(', ', $successmessage);
		}
	} // end of fn __construct, constructor
	
	function CartBodyHolder()
	{	ob_start();
		if (!$this->customer->id)
		{	echo $this->LoginContent();
		}
		echo parent::CartBodyHolder();
		return ob_get_clean();
	} // end of fn MemberBody
	
	protected function CartBody()
	{	ob_start();
		if ($this->customer->id)
		{	echo $this->CartSummaryHeader();
		} else
		{	echo parent::CartSummaryHeader();
		}
		return ob_get_clean();
	} // end of fn CartBody
	
	protected function CartSummaryHeader()
	{	ob_start();
		if ($cart_plates = $this->GetCartPlates())
		{	
			echo '<script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow")); $("#docUploaderJQM").jqm();});</script><div id="docUploaderJQM" class="jqmWindow"><p>Loading, please wait</p><img src="img/template/ajax-loader.gif" alt="Loading" /></div>';
			echo '<div class="cartSummaryHeader"><form id="docsform" method="post" action="', $_SERVER["SCRIPT_NAME"], '" enctype="multipart/form-data" onsubmit="$(\'#docUploaderJQM\').jqmShow(); return true;">';
			if ($cart_plates)
			{	foreach ($cart_plates as $plate_key=>$plate_vars)
				{	echo '<div class="csh_row csh_rowdocs clearfix"><div class="cshName">';
					if (is_array($plate_vars['plate_images']))
					{	foreach($plate_vars['plate_images'] as $plate_image)
						{	echo '<div class="cshNamePlate"><img src="', SITE_URL, 'resources/temp/', $plate_image, '?', time(), '" /></div>';
						}
					}
					$popup_docs = new PageContentPopup('platedocs');
					echo '</div><div class="cshDocs"><p class="cshDocsPopupLink">', $popup_docs->PopUpLink(), '</p>';
					if ($plate_vars['docs_user'])
					{	echo '<p class="clearfix"><label>Document to prove my identity, remove this</label><input type="checkbox" name="platedocs_remove[', $plate_key, '][user]" value="1" /></p>';
					} else
					{	echo '<p class="clearfix"><label>Document to prove my identity</label><input type="file" name="platedocs[', $plate_key, '][user]" /></p>';
					}
					if ($plate_vars['docs_reg'])
					{	echo '<p class="clearfix"><label>Document to prove my registration entitlement, remove this</label><input type="checkbox" name="platedocs_remove[', $plate_key, '][reg]" value="1" /></p>';
					} else
					{	echo '<p class="clearfix"><label>Document to prove my registration entitlement</label><input type="file" name="platedocs[', $plate_key, '][reg]" /></p>';
					}
					echo '<p><label>I will email or post my <strong>original</strong> evidence later</label><input type="checkbox" name="docslater[', $plate_key, ']" value="1"', $plate_vars['docslater'] ? ' checked="checked"' : '', ' /></p>',
						'<div class="clear"></div><p><label>I do not need to provide any evidence because:<br />My order does not display a Vehicle Registration Mark (VRM) or<br/>My order is for plates being delivered to outside of the UK</label><input type="checkbox" name="docsnonroad[', $plate_key, ']" value="1"', $plate_vars['docsnonroad'] ? ' checked="checked"' : '', ' /></p>',
						'</div></div>';
				}
			}
			echo '<p><label></label><input class="submit" type="submit" value="Submit" /></p></form></div>';
		}
		return ob_get_clean();
	} // end of fn CartSummaryHeader
	
	private function SavePlateDocs($post = array(), $docs = array())
	{	$fail = array();
		$success = array();
		
		if ($cart_plates = $this->GetCartPlates())
		{	//$this->VarDump($docs);
			//$this->VarDump($cart_plates);
			//$this->VarDump($_SESSION['cart_plates']);
			foreach ($cart_plates as $plate_key=>$plate_vars)
			{	if ($post['docslater'][$plate_key])
				{	$_SESSION['cart_plates'][$plate_key]['docslater'] = true;
				} else
				{	unset($_SESSION['cart_plates'][$plate_key]['docslater']);
				}
				if ($post['docsnonroad'][$plate_key])
				{	$_SESSION['cart_plates'][$plate_key]['docsnonroad'] = true;
				} else
				{	unset($_SESSION['cart_plates'][$plate_key]['docsnonroad']);
				}
				
				if ($_SESSION['cart_plates'][$plate_key]['docs_user'])
				{	// check for removal
					if ($post['platedocs_remove'][$plate_key]['user'])
					{	$userdoc = new PlateDocument($_SESSION['cart_plates'][$plate_key]['docs_user']);
						$filename = $userdoc->details['filename'];
						if ($userdoc->Delete())
						{	unset($_SESSION['cart_plates'][$plate_key]['docs_user']);
							$success[] = '"' . $this->InputSafeString($filename) . '" has been removed';
						}
					}
				} else
				{	if ($docs['size'][$plate_key]['user'])
					{	if ($docs['error'][$plate_key]['user'])
						{	$fail['docerror'] = 'Document upload error';
						} else
						{	$userdoc = new PlateDocument();
							if ($userdoc->ValidType($filetype = $userdoc->FiletypeFromName($docs['name'][$plate_key]['user'])))
							{	$created = $userdoc->Create('user', $docs['name'][$plate_key]['user'], $docs['tmp_name'][$plate_key]['user'], $filetype);

								if ($userdoc->id)
								{	$_SESSION['cart_plates'][$plate_key]['docs_user'] = $userdoc->id;
									$success[] = '"' . $this->InputSafeString($userdoc->details['filename']) . '" has been uploaded';
								} else
								{	$fail[] = '"' . $this->InputSafeString($docs['name'][$plate_key]['user']) . '" has failed to upload (' . $created['failmessage'] . ')';
								}
							} else
							{	$fail[] = '"' . $this->InputSafeString($docs['name'][$plate_key]['user']) . '" is not a valid file type (' . $userdoc->FiletypesList() . ' only)';
							}
						}
					}
				}
				
				if ($_SESSION['cart_plates'][$plate_key]['docs_reg'])
				{	// check for removal
					if ($post['platedocs_remove'][$plate_key]['reg'])
					{	$userdoc = new PlateDocument($_SESSION['cart_plates'][$plate_key]['docs_reg']);
						$filename = $userdoc->details['filename'];
						if ($userdoc->Delete())
						{	unset($_SESSION['cart_plates'][$plate_key]['docs_reg']);
							$success[] = '"' . $this->InputSafeString($filename) . '" has been removed';
						}
					}
				} else
				{	if ($docs['size'][$plate_key]['reg'])
					{	if ($docs['error'][$plate_key]['reg'])
						{	$fail['docerror'] = 'Document upload error';
						} else
						{	$userdoc = new PlateDocument();
							if ($userdoc->ValidType($filetype = $userdoc->FiletypeFromName($docs['name'][$plate_key]['reg'])))
							{	$created = $userdoc->Create('reg', $docs['name'][$plate_key]['reg'], $docs['tmp_name'][$plate_key]['reg'], $filetype);

								if ($userdoc->id)
								{	$_SESSION['cart_plates'][$plate_key]['docs_reg'] = $userdoc->id;
									$success[] = '"' . $this->InputSafeString($userdoc->details['filename']) . '" has been uploaded';
								} else
								{	$fail[] = '"' . $this->InputSafeString($docs['name'][$plate_key]['reg']) . '" has failed to upload (' . $created['failmessage'] . ')';
								}
							} else
							{	$fail[] = '"' . $this->InputSafeString($docs['name'][$plate_key]['reg']) . '" is not a valid file type (' . $userdoc->FiletypesList() . ' only)';
							}
						}
					}
				}
				
			}
		}
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
	} // end of fn SavePlateDocs

} // end of defn CartDocsPage

$page = new CartDocsPage();
$page->Page();
?>