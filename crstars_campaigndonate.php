<?php
require_once('init.php');

class CRStarsCampaignDonationPage extends CRStarsPage
{	private $campaign;
	private $donation;
	private $session_name = 'crcamp_donation';
	protected $stage = '';
	protected $stages = array(
			1=>array('heading'=>'Make a Donation', 'content_method'=>'DonationStageContent', 'submit_method'=>'DonationStageSubmit', 'linkable'=>true),
			2=>array('heading'=>'Personal Details', 'content_method'=>'PersonalStageContent', 'submit_method'=>'PersonalStageSubmit', 'linkable'=>true),
			3=>array('heading'=>'Confirmation', 'content_method'=>'ConfirmationStageContent', 'submit_method'=>'ConfirmationStageSubmit'),
			4=>array('heading'=>'Card Details', 'content_method'=>'PaymentStageContent', 'submit_method'=>'PaymentStageSubmit')
		);
	private $current_section = 'amount';
	private $giftaid_unticked = false;
	protected $campaignOwner;

	public function __construct()
	{	parent::__construct();
		$this->campaign = new Campaign($_GET['cid'], $_GET['slug']);
		if ($this->campaign->details['visible'])
		{	if (!$this->campaign->details['enabled'])
			{	header('location: ' . $this->campaign->Link());
				exit;
			}
		} else
		{	header('location: ' . SITE_URL . 'cr-stars/campaign-ended/');
			exit;
		}
		$this->campaignOwner = new CampaignUser($this->campaign->owner);
		$this->donation = new CampaignDonation();
		$this->canonical_link = $this->campaign->DonateLink();
		// $this->css['donations.css'] = 'donations.css';
		$this->css['page.css'] = 'page.css';
		$this->css['style.css'] = 'style.css';
		$this->css['style-donate.css'] = 'style-donate.css';
		$this->js['donations.js'] = 'donations.js';

		if (!$_SESSION[$this->session_name])
		{	$_SESSION[$this->session_name] = array();
			$_SESSION[$this->session_name]['donor_showto'] = true;
		}

		if ($this->stage = (int)$_POST['donate_submit_stage'])
		{	$this->SubmitDonateForm($_POST);
		} else
		{	if (!$this->stage = (int)$_GET['donate_submit_stage'])
			{	$this->stage = 1;
			}
		}

		if ($_SESSION[$this->cart_session_name]['currency'])
		{	$_SESSION[$this->session_name]['donation_currency'] = $_SESSION[$this->cart_session_name]['currency'];
		} else
		{	if (!$_SESSION[$this->session_name]['donation_currency'])
			{	$_SESSION[$this->session_name]['donation_currency'] = 'GBP';
			}
		}

	} // end of fn __construct

	private function SubmitDonateForm($data = array())
	{	if (($method = $this->stages[$this->stage]['submit_method']) && method_exists($this, $method))
		{	$saved = $this->$method($data);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
	} // end of fn SubmitDonateForm

	public function MainDonateContent()
	{	ob_start();
		// echo '<div class="container donate_main_content"><div class="container_inner">';
		if (($method = $this->stages[$this->stage]['content_method']) && method_exists($this, $method))
		{	echo $this->$method();
		}
		// if (SITE_TEST)
		// {	if ($this->do_cart)
		// 	{	echo '<p><a href="/donate/?reset_test_cart=1" target="_blank">reset whole cart (new tab)</a></p>';
		// 		$this->VarDump($_SESSION[$this->cart_session_name]);
		// 	} else
		// 	{	$this->VarDump($_SESSION[$this->session_name]);
		// 	}
		// }
		// echo '</div></div>';
		return ob_get_clean();
	} // end of fn MainDonateContent

	protected function CurrencyChooser($type = '', $currency_selected = '')
	{	ob_start();
		if ($currencies = $this->GetCurrencies('oneoff'))
		{	echo '<p><label>Your currency</label><select name="donation_currency" onchange="DonationCurrencyChange();">';
			if (!$currencies[$currency_selected])
			{	$currency_selected = false;
			}
			foreach ($currencies as $curcode=>$currency)
			{	if (!$currency_selected)
				{	$currency_selected = $curcode;
				}
				echo '<option value="', $curcode, '"', $curcode == $currency_selected ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
			}
			echo '</select><div class="clear"></div></p>';
		}
		return ob_get_clean();
	} // end of fn CurrencyChooser

	protected function CurrencyChooserNew($type = '', $currency_selected = '')
	{	ob_start();
		if ($currencies = $currencies = $this->GetCurrencies('oneoff')) {
			if (!$currencies[$currency_selected])
			{	$currency_selected = false;
			}
			echo '<div class="custom-select-box"><select id="donation_currency" name="donation_currency" onchange="DonationCurrencyChangeNew();">';
			foreach ($currencies as $curcode=>$currency) {
				if (!$currency_selected)
				{	$currency_selected = $curcode;
				}
				echo '<option value="', $curcode, '"', $curcode == $currency_selected ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
			}
			echo '</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>';
		}
		return ob_get_clean();
	} // end of fn CurrencyChooserNew

	protected function CurrencyChooserContainer($currency_selected = '')
	{	ob_start();
		if ($_SESSION[$this->cart_session_name]['currency'])
		{	echo '<input type="hidden" id="donation_currency" name="donation_currency" value="', $this->InputSafeString($_SESSION[$this->cart_session_name]['currency']), '" />';
		} else
		{	if ($currency_chooser = $this->CurrencyChooserNew('oneoff', $currency_selected))
			{	echo '<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation_currency" class="title-label">Which currency would you like to donate?</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner donation-currency-list">', $currency_chooser, '</div>
					</div>
				</div>';
			}
		}
		return ob_get_clean();
	} // end of fn CurrencyChooserContainer

	private function DonationStageContentOld()
	{	ob_start();
		echo '<form action="', $this->campaign->DonateLink(), '" method="post" class="donation-form donation-stage-1 form-rows"', $this->do_cart ? ' onsubmit="return false;"' : '', '><input type="hidden" name="donate_submit_stage" value="1" />
				<div id="donateCurrencyContainer">', $this->CurrencyChooserContainer($_SESSION[$this->session_name]['donation_currency']), '</div>
				<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation_amount" class="title-label">I want to donate (<span class="donation-currency-symbol">', $cursymbol = $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol'), '</span>)</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner"><input type="text" id="donation_amount" name="donation_amount" class="inputSelectOnFocus number" value="', $_SESSION[$this->session_name]['donation_amount'] ? $_SESSION[$this->session_name]['donation_amount'] : '0', '" required /></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation_zakat" class="title-label">Is your donation Zakat?</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner">
							<div class="custom-select-box"><select id="donation_zakat" name="donation_zakat">
								<option value="0" ', $_SESSION[$this->session_name]['donation_zakat'] ? '':'selected="selected"', '>No</option>
								<option value="1" ', $_SESSION[$this->session_name]['donation_zakat'] ? 'selected="selected"' : '', '>Yes</option>
							</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>
						</div>
					</div>
				</div>';
		if ($this->do_cart)
		{	echo
				'<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation-anon" class="title-label">Do you want to hide your name</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner">
							<div class="custom-select-box">
								<select id="donor_anon" name="donor_anon">
									<option value="0" ', !$_SESSION[$this->session_name]['donor_anon'] ? 'selected="selected"' : '', '>No</option>
									<option value="1" ', $_SESSION[$this->session_name]['donor_anon'] ? 'selected="selected"' : '', '>Yes</option>
								</select>
								<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
							</div>
						</div>
					</div>
				</div>';
		if ($this->campaign->owner)
		{	echo '<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation-showto" class="title-label">Do you want to show your details to ', $this->InputSafeString($this->campaignOwner->FullName()), '</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner">
							<div class="custom-select-box">
								<select id="donor_showto" name="donor_showto">
									<option value="0" ', !$_SESSION[$this->session_name]['donor_showto'] ? 'selected="selected"' : '', '>No</option>
									<option value="1" ', $_SESSION[$this->session_name]['donor_showto'] ? 'selected="selected"' : '', '>Yes</option>
								</select>
								<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
							</div>
						</div>
					</div>
				</div>';
		}
		echo '<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donor-phone" class="title-label">Leave a message for ', $this->campaign->owner ? $this->InputSafeString($this->campaign->owner['firstname']) : 'Charity Right', '</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner"><textarea name="donor_comment">', $this->InputSafeString($_SESSION[$this->session_name]['donor_comment']), '</textarea></div>
					</div>
				</div>
			';
		}
		echo '	<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner">', $this->do_cart ? ('<input type="submit" value="Add to Cart" onclick="CRDonationAddToCart(' . $this->campaign->id . ');" class="button" />') : '<input type="submit" value="Next" class="button" />', '</div>
					</div>
				</div>
			</form>';
		if ($this->do_cart)
		{	// modal pop-up
			echo '<script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$("#cart_modal_popup").jqm();});</script>',
	'<!-- START banners list modal popup --><div id="cart_modal_popup" class="jqmWindow cartModalPopup"><a href="#" class="jqmClose cartCloseTop">&times;</a><div class="cart_modal_popup_inner"></div></div>';
		}
		return ob_get_clean();
	} // end of fn DonationStageContent

	private function DonationStageContent()
	{
		ob_start();
		// echo 'User IP Address - '.$_SERVER['REMOTE_ADDR']; die;
		$amount = '25';
		$type = 'oneoff';
		if (isset($_GET['amount']) && $_GET['amount'] != '') {
			$amount = $_GET['amount'];
		}
		if (isset($_GET['type']) && $_GET['type'] != '') {
			$type = $_GET['type'];
		}
		if(!$image = $this->campaign->GetImageSRC('large')){
			$image = $avatar = $this->campaign->GetAvatarSRC('large');
		}
		echo '<script src="https://js.stripe.com/v2/"></script><input type="hidden" id="crdonations_form" value="true"><input type="hidden" id="stripe_publishable_key" value="',STRIPE_PUBLISHABLE_KEY,'"><div class="hero-banner donate-hero-banner" '.(($image) ? 'style="background-image:url('.$image.');"':'').'>
			<div class="container">
				<div id="loaderclass" class=""></div>
				<div class="donation-tabs">
					<div class="stv-radio-tabs-wrapper">
						<div class="main-label" style="flex: 0 0 100%;">
							<input type="radio" class="stv-radio-tab" name="donation_type" value="oneoff" id="tab1" onclick="DonationTypeClick(this);" ';
							if($type == 'oneoff'){ echo 'checked'; }
							echo '/>
							<label for="tab1">Give once</label>
						</div>	
						<!-- <div class="main-label">	
							<input type="radio" class="stv-radio-tab" name="donation_type" value="monthly" id="tab2" onclick="DonationTypeClick(this);"';
							if($type == 'monthly'){ echo 'checked'; }
							echo '/>
							<label for="tab2">Monthly</label>
						</div> -->	
					</div>
					<div class="step1">
						<!-- <div class="choose-area">
				    		<select id="donation-country" class="main-select2" name="donation_country">
								<option value="">Choose an area of our work you’d like to support</option>
								<option value="schools">School meals</option>
								<option value="food_packs">Family food packs</option>
								<option value="girls">Educate a girl</option>
								<option value="hifzmeals">Hifz</option>
								<option value="most_needed">Help where it’s most needed</option>
							</select>
				    	</div> -->
				    	<input type="hidden" value="'.$this->campaign->id.'" name="cid" id="cid">
						<input type="hidden" value="GBP" name="donation_currency" id="donation_currency">
					    <div class="choose-money">
							<p class="choose-money-p">Choose an amount you’d like to give <span class="month_content" ';
							if($type == 'oneoff'){ echo 'style="display:none;"'; }
							echo '>every month</span></p>
						</div>
						<div class="choose-money-option">
							<div class="radio-toolbar radio-first-row">
								<div class="radio-parent">
							    	<input type="radio" id="first-price" class="donation-amount" name="donation_amount" value="25" ';
							    	if($amount == '25'){ echo 'checked'; }
							    	echo '>
								    <label for="first-price">&#163;25</label>
								</div>    
								<div class="radio-parent">
							    	<input type="radio" id="second-price" class="donation-amount" name="donation_amount" value="50" ';
							    	if($amount == '50'){ echo 'checked'; }
							    	echo '>
								    <label for="second-price">&#163;50</label>
								</div>
								<div class="radio-parent">
							    	<input type="radio" id="third-price" class="donation-amount" name="donation_amount" value="75" ';
							    	if($amount == '75'){ echo 'checked'; }
							    	echo '>
								    <label for="third-price">&#163;75</label> 
								</div>    
							</div>
							<div class="radio-toolbar radio-second-row">
								<div class="radio-parent hundred-pound">
							    	<input type="radio" id="fourth-price" class="donation-amount" name="donation_amount" value="100" ';
							    	if($amount == '100'){ echo 'checked'; }
							    	echo '>
								    <label for="fourth-price">&#163;100</label>
								</div>    
								<div class="radio-parent other-amount">
							    	<input type="radio" id="fifth-price" class="donation-amount" name="donation_amount" value="other">
								    <label for="fifth-price" id="other-amount-main">Other amount</label>
								</div>
							</div>
							<div id="other_amount" style="display: none;">
								<div class="inner-other-amount">
	                            	<label class="answer-yes">Please enter amount</label>
	                            	<input type="number" name="other_donation_amount" id="other_amout_inner" class="other-amount-input">
	                            </div>	
	                    	</div>
						</div>
						<div class="tick-switch">
							<span class="same-address first-added">
								<div class="checkclaimGift">
									<input type="checkbox" class="donation_zakat" id="donation_zakat" >
									<label for="donation_zakat" style="font-weight: unset;">Is your donation Zakat?</label>
								</div>
							</span>
						</div>
						<div class="donate-btn">
							<input type="button" value="donate" onclick="DonationAddToCartNew();" class="text-uppercase text-center" name="">
						</div>
					</div>
					<div class="step2" style="display: none;">
						<div class="donation-support">
							<!-- <span class="donation-support-title">My donation will support : <span class="donation-support-country"></span></span> -->
							<span class="donation-support-title">I’m giving <span class="donation-support-amount">£<span class="donation-amount"></span> <span class="month_content" ';
							if($type == 'oneoff'){ echo 'style="display:none;"'; }
							echo '>every month</span> <a href="javascript:;" onclick="backToStep1();" class="edit-icon"><img src="/img/pen.png" class="edit-main-image"></a></span></span>
						</div>
						<div class="name-part">
							<div class="name-part-inner">
								<div class="gender-selection">
									<select class="main-select2" id="donor-title" name="donor_title">
										<option value="">Title</option>
										<option value="mr">Mr</option>
										<option value="miss">Miss</option>
										<option value="ms">Ms</option>
										<option value="mrs">Mrs</option>
										<option value="dr">Dr.</option>
										<option value="prof">Prof.</option>
									</select>
								</div>
								<div class="name-part-first">
									<input type="text" name="donor_firstname" class="form-control" id="donor-firstname" name="" placeholder="First Name" autocomplete="off">
								</div>
								<div class="name-part-first">
									<input type="text" name="donor-lastname" class="form-control" id="donor-lastname" name="" placeholder="Last Name" autocomplete="off">
								</div>
							</div>
						</div>
						<div class="email-field">
							<input type="email" class="form-control" name="donor_email" id="donor-email" placeholder="Email" autocomplete="off">
						</div>
						<div class="credit-card-part card-details">
							<div class="credit-card-inner">
								<div class="credit-card-first">
									<input type="number" class="form-control" id="cardNumber1" name="cardNumber" placeholder="Card number">
									<input type="hidden" name="stripeToken" id="stripeToken" />
								</div>
								<div class="credit-card-first">
									<span class="expiration">
									    <input type="text" name="day" id="expiry_month" placeholder="MM" maxlength="2" size="2" required="true" />
									    <span class="slash">/</span>
									    <input type="text" name="expiry_year" id="expiry_year" placeholder="YYYY" maxlength="4" size="2" required="true" />
									</span>
								</div>
								<div class="credit-card-first">
									<input type="password" class="form-control" id="cvv" name="cvv" placeholder="cvc">
								</div>
							</div>
						</div>
						<div class="debit-card-details bank-details">
							<div class="bank-aacount-field">
								<input type="text" class="form-control" maxlength="8" id="dd-accountnumber" name="dd_accountnumber" pattern="[0-9]{8}" placeholder="Bank account number">
							</div>
							<div class="bank-aacount-field">
								<input type="text" class="form-control" maxlength="6" id="dd-accountsortcode" name="dd_accountsortcode" pattern="[0-9]{6}" placeholder="Sort code Code">
							</div>
							<div class="bank-aacount-modal">
								<!-- Button trigger modal -->
								<button type="button" class="btn btn-primary bank-aacount-modal-btn main-pop-up-btn" data-toggle="modal" data-target="#bank-aacount-modal">
								  Read your direct debit guarantee
								</button>

							</div>
						</div>
						<div class="donate-btn">
							<input type="button" value="one last step" onclick="DonationAddToCartNewStep2();" class="text-uppercase text-center" name="">
						</div>
					</div>
					<div class="step3" style="display: none;">
						<div class="donation-support">
							<!-- <span class="donation-support-title">My donation will support : <span class="donation-support-country"></span></span> -->
							<span class="donation-support-title">I’m giving <span class="donation-support-amount">£<span class="donation-amount"></span> <span class="month_content" ';
							if($type == 'oneoff'){ echo 'style="display:none;"'; }
							echo '>every month</span> <a href="javascript:;" onclick="backToStep1();" class="edit-icon"><img src="/img/pen.png" class="edit-main-image"></a></span></span>
						</div>
						<div class="edit-card-details">
							<a href="javascript:;" onclick="backToStep2();" class="text-uppercase"><span class="back-icon"><img src="/img/back.png" class="back-main-image"></span>edit-card-details</a>
						</div>
						<link rel="stylesheet" type="text/css" href="https://services.postcodeanywhere.co.uk/css/captureplus-2.30.min.css?key=pj67-xk39-kp45-jg35" />
						<script type="text/javascript" src="https://services.postcodeanywhere.co.uk/js/captureplus-2.30.min.js?key=pj67-xk39-kp45-jg35"></script>
						<div class="billing-address">
							<label>Your billing address : </label>
							<input type="textarea" id="billing-address" name="billing-address" placeholder="Enter billing address">
						</div>
						<div class="email-field">
							<input type="text" class="form-control" id="billing-post-code" placeholder="Post code field" name="billing_post_code">
						</div>
						
						<div class="donor-address-hidden-fields hidden">
							<input type="text" id="pca_donor_address1" name="pca_donor_address1" value="" autocomplete="off">
							<input type="text" id="pca_donor_address2" name="pca_donor_address2" value="" autocomplete="off">
							<input type="text" id="pca_donor_address3" name="pca_donor_address3" value="" autocomplete="off">
							<input type="text" id="pca_donor_city" name="pca_donor_city" value="" autocomplete="off">
							<input type="text" id="pca_donor_postcode" name="pca_donor_postcode" value="" autocomplete="off">
							<input type="text" id="pca_donor_country" name="pca_donor_country" value="" autocomplete="off">
						</div>
						<div class="select-country" style="margin-bottom: 10px;">
							<select class="main-select2 country-selection" id="donor-country" name="donor_country" autocomplete="off"><option>Select Your Country</option>';
								foreach ($this->CountryList() as $code=>$countryname)
								{	echo '<option value="', $code, '">', $this->InputSafeString($countryname), '</option>';
								};
						echo '</select>
						</div>
						<div class="tick-switch">
							<span class="same-address first-added">
								<div class="checkclaimGift">
									<input type="checkbox" class="donor_anon" id="donor_anon" >
									<label for="donor_anon" style="font-weight: unset;">Donate anonymously?</label>
								</div>
							</span>
						</div>
						<div class="email-field">
							<input type="text" class="form-control" id="donor_comment" placeholder="Leave a message" name="donor_comment">
						</div>
						<div class="gift-aid-action" style="display: none;">
							<div class="gift-aid gift-aid-once">
								<!-- Button trigger modal -->
								<button type="button" class="btn btn-primary click-btn main-pop-up-btn" data-toggle="modal" data-target="#oneoffGiftAidModal">Claim Gift Aid to boost your donation</button>
							</div>
							<div class="gift-aid gift-aid-monthly">
								<!-- Button trigger modal -->
								<button type="button" class="btn btn-primary click-btn main-pop-up-btn" data-toggle="modal" data-target="#gift-aid-monthly">Claim Gift Aid to boost your donation</button>
								
							</div>
						</div>
						<input type="hidden" name="" id="siteurl"  value="',SITE_URL,'donate-thank-you/">
						<div class="donate-btn" style="margin-top: 10px;">
							<input type="button" value="donate" onclick="DonationAddToCartFinal();" class="text-uppercase text-center donationadd_final" name="">
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div class="description-part">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12">
						<div class="provide-text text-center">
							<h1>Donate</h1>
							<p>Donating to '.$this->campaign->FullTitle().' campaign.</p>
						</div>		
					</div>
				</div>
			</div>
		</div>
		<!-- GiftAidReminderModal -->
		<div class="modal fade donation-modal give-once-third-step-popup" id="giftAidReminderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
					<div class="modal-body">
						<div class="donGiftaidCheckernew"><h4>Remember as a UK tax payer, you can top up your donation by 25% at no extra cost.</h4><p><a href="#" onclick="DonorGiftaidChecker(true);" data-dismiss="modal" class="form-check-label">Yes, please add giftaid to my donation</a></p><p><a onclick="DonationAddToCartFinal();" data-dismiss="modal">No, continue without giftaid</a></p></div>
		            </div>
		        </div>
		    </div>
		</div>
		<!-- Modal -->
		<input type="hidden" name="donor_giftaid" id="donor_giftaid" value="0">
		<div class="modal fade donation-modal give-once-third-step-popup" id="oneoffGiftAidModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h5 class="modal-title" id="exampleModalLabel">Boost your donation with Gift Aid</h5>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true"><img src="/img/close.png"></span>
		                </button>
		            </div>
		            <div class="modal-body">
		            	<!-- <div class="gift-increased">
		            		<p>Your gift of £X will be increased to : <span class="increased-amount">£X.XX</span></p>
		            	</div> -->
		            	<div class="tick-switch">
		            		<span class="same-address">
			            		<div class="boxes checkclaimGift">
			            			<input type="checkbox" class="claimGiftAid" id="claimGiftAid" onChange="claimGiftAidBox(this);">
									<label for="claimGiftAid">Please tick if you’d like to claim Gift Aid</label>
								</div>
							</span
		            	</div>
		            	<div class="switch-permission">
		            		<p>By ticking the above box, I confirm I am a UK taxpayer and I would like Charity Right to treat this donation as a Gift Aid donation. I understand that if I pay less income tax and/or capital gains tax than the amount of Gift Aid claimed on all my donations in that year, it is my responsibility to pay any difference </p>
		            	</div>
		            	<div class="pop-up-name">
		            		<div class="name-part-inner">
								<div class="gender-selection">
									<select class="main-select3 donor_title_pop" >
										<option>Title</option>
										<option value="mr">Mr</option>
										<option value="miss">Miss</option>
										<option value="ms">Ms</option>
										<option value="mrs">Mrs</option>
										<option value="dr">Dr.</option>
										<option value="prof">Prof.</option>
									</select>
								</div>
								<div class="name-part-first">
									<input type="text" class="form-control donor_firstname_pop" placeholder="First Name" readonly="readonly">
								</div>
								<div class="name-part-first">
									<input type="text" class="form-control donor_lastname_pop" placeholder="Last Name" readonly="readonly">
								</div>
							</div>
		            	</div>
		            	<div class="pop-up-billing-address">
			            	<div class="billing-address">
								<!-- <label>Your billing address : </label> -->
								<textarea name="home_address" id="home-address" placeholder="Your home address" form="usrform"></textarea>
							</div>
						</div>
						<div class="post-code-field">
							<input type="text" id="post-code-field1" placeholder="Post code field" name="">
						</div>

						<div class="same-address">
							<div class="boxes">
								<input type="checkbox" id="copy_billing_add" onChange="copy_billing_add(this);">
								<label for="copy_billing_add">Home address the same as your billing address?</label>
							</div>
						</div>	
						<div class="pop-up-btn">
							<div class="inner-pop-up-btn">
								<button class="text-uppercase text-center" data-dismiss="modal">cancle</button>
								<button class="text-uppercase text-center" id="claimGiftAidBtn" onclick="claimGiftAid();">CLAIM GIFT AID</button>
							</div>
						</div>
		            </div>
		        </div>
		    </div>
		</div></div>

		<!-- Modal -->
		<div class="modal fade donation-modal" id="bank-aacount-modal" tabindex="-1" role="dialog" aria-labelledby="bank-aacount-modalLabel" aria-hidden="true">
		  	<div class="modal-dialog" role="document">
			    <div class="modal-content">
			    	<div class="modal-header">
			    		<h5 class="modal-title" id="bank-aacount-modalLabel">Your direct debit guarantee</h5>
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          		<span aria-hidden="true"><img src="/img/close.png"></span>
		                </button>
			      	</div>
			      	<div class="modal-body">
			      		<div class="debit-card-guarantee">
			        		<div class="debit-card-guarantee-des">
			        			<p>This guarantee is offered by all banks and building societies that accept instructions to pay direct debits.</p><p>If there are any changes to the amount, date or frequency of your direct debit, Eazy Collect (on behalf of Charity Right) will notify you 10 working days in advance of your account being debited or as otherwise agreed. If you request Eazy Collect (on behalf of Charity Right) to collect a payment, Eazy Collect will confirm the amount and date at the time of the request.</p><p>If an error is made in the payment of your direct debit, either by Eazy Collect (on behalf of Charity Right), or by your bank or building society, you are entitled to a full and immediate refund of the amount paid from your bank or building society.</p>
								<p>If you receive a refund you are not entitled to, you must pay it back when Eazy Collect (on behalf of Charity Right) asks you to.</p><p>You can cancel a direct debit at any time by simply contacting your bank or building society. Written confirmation may be required. Please also notify <a href="callto:+1111111111">Charity Right.</a></p>
			        		</div>
			        	</div>
			      	</div>
			    </div>
		 	</div>
		</div>

		<!-- Modal -->
		<div class="modal fade donation-modal step-third-monthly" id="gift-aid-monthly" tabindex="-1" role="dialog" aria-labelledby="gift-aid-monthlyLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h5 class="modal-title" id="gift-aid-monthlyLabel">Boost your donation with Gift Aid</h5>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true"><img src="/img/close.png"></span>
		                </button>
		            </div>
		            <div class="modal-body">
		            	<!-- <div class="gift-increased">
		            		<p>Every month,  your gift of £X will be increased to : <span class="increased-amount">£X.XX</span></p>
		            	</div> -->
		            	<div class="tick-switch">
		            		<span class="same-address">
		            			<div class="boxes checkclaimGift">
		            				<input type="checkbox" class="claimGiftAid" id="claimGiftAid2" onChange="claimGiftAidBox(this);">
									<label for="claimGiftAid2">Please tick if you’d like to claim Gift Aid</label>
								</div>
							</span>
		            	</div>
		            	<div class="switch-permission">
		            		<p>Yes, I am a UK taxpayer and I would like Charity Right to treat this donation and future donations associated with this subscription as Gift Aid donations. I understand that if I pay less income tax and/or capital gains tax than the amount of Gift Aid claimed on all my donations in that year, it is my responsibility to pay any difference .
							</p>
		            	</div>
		            	<div class="pop-up-name">
		            		<div class="name-part-inner">
								<div class="gender-selection">
									<select class="main-select3 donor_title_pop" >
										<option>Title</option>
										<option value="mr">Mr</option>
										<option value="miss">Miss</option>
										<option value="ms">Ms</option>
										<option value="mrs">Mrs</option>
										<option value="dr">Dr.</option>
										<option value="prof">Prof.</option>
									</select>
								</div>
								<div class="name-part-first">
									<input type="text" class="form-control donor_firstname_pop" placeholder="First Name" readonly="readonly">
								</div>
								<div class="name-part-first">
									<input type="text" class="form-control donor_lastname_pop" placeholder="Last Name" readonly="readonly">
								</div>
							</div>
		            	</div>
		            	<div class="pop-up-billing-address">
			            	<div class="billing-address">
								<!-- <label>Your billing address : </label> -->
								<textarea name="home_address_monthly" id="home-address-monthly" placeholder="Your home address" form="usrform"></textarea>
							</div>
						</div>
						<div class="post-code-field">
							<input type="text" id="post-code-field2" placeholder="Post code field" name="">
						</div>
						<div class="same-address">
							<div class="boxes">
								<input type="checkbox" id="copy_billing_add2" onChange="copy_billing_add(this);">
								<label for="copy_billing_add2">Home address the same as your billing address?</label>
							</div>
						</div>	
						<div class="pop-up-btn">
							<div class="inner-pop-up-btn">
								<button class="text-uppercase text-center" data-dismiss="modal">cancle</button>
								<button class="text-uppercase text-center" id="claimGiftAidBtn" onclick="claimGiftAid();">CLAIM GIFT AID</button>
							</div>
						</div>
		            </div>
		        </div>
		    </div>
		</div>
		<script type="text/javascript">$().ready(function(){ initAutocomplete();	});</script>
		';
		if ($this->do_cart)
		{	// modal pop-up
			echo '<script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$(".cartModalPopup").jqm();});</script>',
			'<!-- START banners list modal popup --><div class="jqmWindow cartModalPopup"><a href="#" class="jqmClose cartCloseTop">&times;</a><div class="cart_modal_popup_inner"></div></div>';
		}
		return ob_get_clean();
	}

	private function DonationStageSubmit($data = array())
	{	$fail = array();
		$success = array();

		$currencies = $this->GetCurrencies('campaigns');
		if ($currencies[$data['donation_currency']])
		{	$_SESSION[$this->session_name]['donation_currency'] = $data['donation_currency'];
		} else
		{	$fail[] = 'Donation currency missing';
		}

		if (!$_SESSION[$this->session_name]['donation_amount'] = (int)$data['donation_amount'])
		{	$fail[] = 'Donation amount missing';
		}

		$_SESSION[$this->session_name]['donation_zakat'] = ($data['donation_zakat'] ? 1 : 0);

		if (!$fail)
		{	$this->stage = 2;
		}
		return array('failmessage'=>implode('<br />', $fail), 'successmessage'=>implode('<br />', $success));
	} // end of fn DonationStageSubmit

	public function PersonalStageContent()
	{	ob_start();
		$pref = new ContactPreferences();
		$pp_page = new PageContent('use-of-cookies');
		$add_lines = array();
		if ($_SESSION[$this->session_name]['pca_donor_address1'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_address2'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_address3'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_city'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_postcode'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_country'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_country']);
		}
		echo '<form action="', $this->campaign->DonateLink(), '" method="post" id="donatePersonalStageForm" class="donation-form donation-stage-2 form-rows">
				<input type="hidden" name="donate_submit_stage" value="2" />
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-title" class="title-label">Title <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-title" name="donor_title" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_title']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-firstname" class="title-label">First name <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-firstname" name="donor_firstname" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_firstname']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-lastname" class="title-label">Last name <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-lastname" name="donor_lastname" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_lastname']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-country" class="title-label">Country <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box"><select id="donor-country" name="donor_country" onchange="DonorCountryChangeNew();" required><option value=""></option>';
		foreach ($this->CountryList() as $code=>$countryname)
		{	echo '				<option value="', $code, '"', (($code == $_SESSION[$this->session_name]['donor_country']) ? ' selected="selected"' : ""), '>', $this->InputSafeString($countryname), '</option>';
		}
		echo					'</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>
							</div>
						</div>
					</div>
					<div class="form-row donorAddressPCA ', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? '' : 'hidden', '">
						<link rel="stylesheet" type="text/css" href="https://services.postcodeanywhere.co.uk/css/captureplus-2.30.min.css?key=pj67-xk39-kp45-jg35" />
						<script type="text/javascript" src="https://services.postcodeanywhere.co.uk/js/captureplus-2.30.min.js?key=pj67-xk39-kp45-jg35"></script>
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="pcaAddress" class="title-label">Address<span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="pcaAddressContainer clearfix ', ($_SESSION[$this->session_name]['pca_donor_address1'] && $_SESSION[$this->session_name]['pca_donor_city'] && $_SESSION[$this->session_name]['pca_donor_postcode']) ? 'hidden' : '', '">
									<input type="text" id="pcaAddress" name="pcaAddress" value="" placeholder="Start typing a postcode, street or address" />
								</div>
								<div class="donor-address-hidden-fields hidden">
									<input type="text" id="pca_donor_address1" name="pca_donor_address1" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']), '" autocomplete="off" />
									<input type="text" id="pca_donor_address2" name="pca_donor_address2" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']), '" autocomplete="off" />
									<input type="text" id="pca_donor_address3" name="pca_donor_address3" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']), '" autocomplete="off" />
									<input type="text" id="pca_donor_city" name="pca_donor_city" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']), '" autocomplete="off" />
									<input type="text" id="pca_donor_postcode" name="pca_donor_postcode" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']), '" autocomplete="off" />
									<input type="text" id="pca_donor_country" name="pca_donor_country" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_country']), '" autocomplete="off" />
								</div>
								<div class="donor-address-box clearfix ', ($_SESSION[$this->session_name]['pca_donor_address1'] && $_SESSION[$this->session_name]['pca_donor_city'] && $_SESSION[$this->session_name]['pca_donor_postcode']) ? '' : 'hidden', '">
									<div class="donor-address-box-content" name="pca_donor_all_fields">', implode('<br />', $add_lines), '</div>
									<div class="address-search-again"><a href="#pcaAddress" class="button">Search Address Again</a></div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row donorAddressManual', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? ' hidden' : '', '">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-address1" class="title-label">Address<span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<input type="text" id="donor-address1" name="donor_address1" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_address1']), '" placeholder="Address Line 1" />
								<input type="text" name="donor_address2" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_address2']), '" placeholder="Address Line 2" />
								<input type="text" name="donor_city" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_city']), '" placeholder="Town / City" />
								<input type="text" name="donor_postcode" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_postcode']), '" placeholder="Postcode" />
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-email" class="title-label">Email address <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="email" id="donor-email" name="donor_email" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_email']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-phone" class="title-label">Phone number <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-phone" name="donor_phone" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_phone']), '" required/></div>
						</div>
					</div><div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donation-anon" class="title-label">Do you want to hide your name</label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box">
									<select id="donor_anon" name="donor_anon">
										<option value="0" ', !$_SESSION[$this->session_name]['donor_anon'] ? 'selected="selected"' : '', '>No</option>
										<option value="1" ', $_SESSION[$this->session_name]['donor_anon'] ? 'selected="selected"' : '', '>Yes</option>
									</select>
									<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donation-showto" class="title-label">Do you want to show your details to ', $this->InputSafeString($this->campaignOwner->FullName()), '</label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box">
									<select id="donor_showto" name="donor_showto">
										<option value="0" ', !$_SESSION[$this->session_name]['donor_showto'] ? 'selected="selected"' : '', '>No</option>
										<option value="1" ', $_SESSION[$this->session_name]['donor_showto'] ? 'selected="selected"' : '', '>Yes</option>
									</select>
									<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
					</div>';
		if (true || SITE_TEST)
		{	echo '<div class="form-row crRegFormContactPref">
					<p class="cprefTitle">How would you like to receive updates?<br /><span>(Optional)</span></p>
					<p class="cprefHelp">We\'d love to tell you about how your donation helps and keep you in the loop with what Charity Right is up to. Your details will be kept safe and never shared with other organisations. See our <a href="', $pp_page->Link(), '">privacy policy</a>.</p>
					<p>', $pref->FormElementsList($_SESSION[$this->session_name]['contactpref']), '</p>
					<p class="cprefHelp">Don\'t forget, you can change your preferences at any time by emailing us at info@charityright.org.uk.</p>
				</div>
				';
		}
		echo '<div class="form-row donor-giftaid-box ', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? '' : ' hidden', '">
						<div class="form-col form-col-1">
							<div class="form-col-inner">
								<div class="alert-box alert-box-green">
									<h3>Reclaim Gift Aid - Add an extra <strong>', $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol'), number_format(($_SESSION[$this->session_name]['donation_amount'] + $_SESSION[$this->session_name]['donation_admin']) * 0.25, 2), '</strong>', $_SESSION[$this->session_name]['donation_type'] == 'monthly' ? ' per month' : '', ' to your donation at no extra cost to you.</h3>
									<p>&nbsp;</p><input type="hidden" name="donor_giftaidchecked" value="" />
									<div class="custom-checkbox clearfix">
										<input type="checkbox" name="donor_giftaid" id="donor-giftaid" value="1" '. (($_SESSION[$this->session_name]['donor_giftaid']) ? 'checked="checked"' : ""). '><label for="donor-giftaid">&nbsp;I want to Gift Aid my donation and any donations I make in the future or have made in the past 4 years to Charity Right.</label>
									</div>
									<div class="form-help-text clearfix"><strong>By ticking this box you are confirming that:</strong><br />"I am a UK taxpayer and understand that if I pay less Income Tax and/or Capital Gains Tax than the amount of Gift Aid claimed on all my donations in that tax year it is my responsibility to pay any difference. I understand that other taxes such as VAT and Council Tax do not qualify. I understand the charity will reclaim 28p of tax on every &pound;1 that I gave up to 5 April 2008 and will reclaim 25p of tax on every &pound;1 that I give on or after 6 April 2008."</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-phone" class="title-label">Leave a message for ', $this->campaign->owner ? $this->InputSafeString($this->campaign->owner['firstname']) : 'Charity Right', '</label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><textarea name="donor_comment">', $this->InputSafeString($_SESSION[$this->session_name]['donor_comment']), '</textarea></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner">
								<div class="form-row-separator clearfix"></div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner"><input type="submit" value="Next" class="button"/></div>
						</div>
					</div>',
					$this->giftaid_unticked ? '<div class="donGiftaidChecker"><h4>Remember as a UK tax payer, you can top up your donation by 25% at no extra cost.</h4><p><a onclick="DonorGiftaidChecker(true);">Yes, please add giftaid to my donation</a></p><p><a onclick="DonorGiftaidChecker(false);">No, continue without giftaid</a></p></div>' : '',
				'</form>';
		return ob_get_clean();
	} // end of fn PersonalStageContent

	private function PersonalStageSubmit($data = array())
	{	$fail = array();
		$success = array();
		$donatecheck = $this->DonationStageSubmit($_SESSION[$this->session_name]);
		if ($donatecheck['failmessage'])
		{	$this->stage = 1;
			$fail[] = 'You must tell us what you want to donate';
		} else
		{


			if (!$_SESSION[$this->session_name]['donor_title'] = $data['donor_title'])
			{	$fail[] = 'You must give your title (Mr, Mrs, Miss, etc.)';
			}

			if (!$_SESSION[$this->session_name]['donor_firstname'] = $data['donor_firstname'])
			{	$fail[] = 'You must give your first name';
			}

			if (!$_SESSION[$this->session_name]['donor_lastname'] = $data['donor_lastname'])
			{	$fail[] = 'You must give your last name';
			}

			$_SESSION[$this->session_name]['donor_giftaid'] = 0;
			if (($_SESSION[$this->session_name]['donor_country'] = $data['donor_country']) && $this->GetCountry($data['donor_country']))
			{	if ($gb = ($data['donor_country'] == 'GB'))
				{	if ($data['donor_giftaid'])
					{	$_SESSION[$this->session_name]['donor_giftaid'] = 1;
					} else
					{	$this->giftaid_unticked = true;
					}
				}
			} else
			{	$fail[] = 'You must give your country of residence';
			}
			
			if ($_SESSION[$this->session_name]['pca_donor_address1'] = $data['pca_donor_address1'])
			{	$pca_address_done++;
			}
			if ($_SESSION[$this->session_name]['pca_donor_address2'] = $data['pca_donor_address2'])
			{	$pca_address_done++;
			}
			if ($_SESSION[$this->session_name]['pca_donor_address3'] = $data['pca_donor_address3'])
			{	$pca_address_done++;
			}
			if (!$pca_address_done && $gb)
			{	$fail[] = 'You must give your address';
			}

			if (!$_SESSION[$this->session_name]['pca_donor_city'] = $data['pca_donor_city'])
			{	if ($gb)
				{	$fail[] = 'You must give your town or city';
				}
			}

			if (!$_SESSION[$this->session_name]['pca_donor_postcode'] = $data['pca_donor_postcode'])
			{	if ($gb)
				{	$fail[] = 'You must give your postcode';
				}
			}

			if ($_SESSION[$this->session_name]['donor_address1'] = $data['donor_address1'])
			{	$address_done++;
			}
			if ($_SESSION[$this->session_name]['donor_address2'] = $data['donor_address2'])
			{	$address_done++;
			}
			if (!$address_done)
			{	if (!$gb)
				{	$fail[] = 'You must give your address';
				}
			}

			if (!$_SESSION[$this->session_name]['donor_city'] = $data['donor_city'])
			{	if (!$gb)
				{	$fail[] = 'You must give your town or city';
				}
			}

			if (!$_SESSION[$this->session_name]['donor_postcode'] = $data['donor_postcode'])
			{	if (!$gb)
				{	$fail[] = 'You must give your postcode';
				}
			}

			$_SESSION[$this->session_name]['donor_comment'] = $data['donor_comment'];
			if ($_SESSION[$this->session_name]['donor_email'] = $data['donor_email'])
			{	if (!$this->ValidEMail($data['donor_email']))
				{	$fail[] = 'You must give a valid email address';
				}
			} else
			{	$fail[] = 'You must give your email address';
			}

			if ($this->ValidPhoneNumber($data['donor_phone'], $gb))
			{	$_SESSION[$this->session_name]['donor_phone'] = $data['donor_phone'];
			} else
			{	$fail[] = 'Your phone number is missing or invalid';
				if (!$_SESSION[$this->session_name]['donor_phone'])
				{	$_SESSION[$this->session_name]['donor_phone'] = $data['donor_phone'];
				}
			}
			$_SESSION[$this->session_name]['donor_anon'] = ($data['donor_anon'] ? 1 : 0);
			$_SESSION[$this->session_name]['donor_showto'] = ($data['donor_showto'] ? 1 : 0);
			
			if (is_array($data['contactpref']) && $data['contactpref'])
			{	$pref = new ContactPreferences();
				$_SESSION[$this->session_name]['contactpref'] = $pref->PreferencesValueFromArray($data['contactpref']);
			} else
			{	$_SESSION[$this->session_name]['contactpref'] = (int)$data['contactpref'];
			}

			if ($fail)
			{	$this->giftaid_unticked = false;
			} else
			{	if (!$this->giftaid_unticked || $_POST['donor_giftaidchecked'])
				{	$this->stage = 3;
				}
			}
		}
		return array('failmessage'=>implode('<br />', $fail), 'successmessage'=>implode('<br />', $success));
	} // end of fn PersonalStageSubmit

	public function ConfirmationStageContent()
	{	ob_start();
		$cursymbol = $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol');
		$tc_page = new PageContent('terms-conditions');

		$address = array();
		
		if (isset($_SESSION[$this->session_name]['donor_address1']) && ($_SESSION[$this->session_name]['donor_country'] != 'GB'))
		{	if ($_SESSION[$this->session_name]['donor_address1'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_address1']);
			}
			if ($_SESSION[$this->session_name]['donor_address2'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_address2']);
			}
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_city']);
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_postcode']);
		} else
		{	if ($_SESSION[$this->session_name]['pca_donor_address1'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']);
			}
			if ($_SESSION[$this->session_name]['pca_donor_address2'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']);
			}
			if ($_SESSION[$this->session_name]['pca_donor_address3'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']);
			}
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']);
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']);
		}
		
		$address[] = $this->InputSafeString($this->GetCountry($_SESSION[$this->session_name]['donor_country']));

		echo '<form action="', $this->campaign->DonateLink(), '" method="post" class="donation-form donation-stage-4 form-rows"><input type="hidden" name="donate_submit_stage" value="3" />
				<div class="form-row donation-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<h3>About Your Donation</h3>
							<p>You will make ', $this->InputSafeString($dtypes[$_SESSION[$this->session_name]['donation_type']]['label']), $_SESSION[$this->session_name]['donation_zakat'] ? ' Zakat' : '', ' donation of ', $cursymbol, number_format($_SESSION[$this->session_name]['donation_amount'], 2), ' for <span class="donationConfirmCampaignTitle">', $this->campaign->FullTitle(), '</span> campaign.</p>',
							$_SESSION[$this->session_name]['donor_giftaid'] ? '<p>We will claim Gift Aid on your donation.</p>' : '',
							'<p><a href="'.$this->campaign->DonateLink().'1/" class="button button-gray-outline">amend donation details</a></p>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row donation-your-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<h3>Your Details</h3>
							<p>', $this->InputSafeString($_SESSION[$this->session_name]['donor_title']), ' ', $this->InputSafeString($_SESSION[$this->session_name]['donor_firstname']), ' ', $this->InputSafeString($_SESSION[$this->session_name]['donor_lastname']);
		if ($_SESSION[$this->session_name]['donor_anon'])
		{	echo '<br />(Your name will not be shown to others';
			if ($_SESSION[$this->session_name]['donor_showto'])
			{	echo ', except ', $this->InputSafeString($this->campaignOwner->FullName());
			}
			echo ')';
		} else
		{	if ($_SESSION[$this->session_name]['donor_showto'])
			{	echo '<br />(Your details will be shown to ', $this->InputSafeString($this->campaignOwner->FullName()), ')';
			}
		}
		echo '</p>
							<p>', implode('<br />', $address), '</p>
							<p>Email: ', $this->InputSafeString($_SESSION[$this->session_name]['donor_email']), '</p>';
		if ($_SESSION[$this->session_name]['donor_phone'])
		{	echo '		<p>Phone number: ', $this->InputSafeString($_SESSION[$this->session_name]['donor_phone']), '</p>';
		}
		echo '			<p><a href="', $this->campaign->DonateLink(), '2/" class="button button-gray-outline">amend your details</a></p>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row donation-terms-and-conditions">
					<div class="form-col form-col-1">
						<div class="form-col-inner" style="text-align: center;">
							<p>By making your donation you are confirming that you accept our <a href="', $tc_page->Link(), '" target="_blank">terms and conditions</a>.</p>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner" style="text-align: center;"><input type="submit" value="Make Your Donation" class="button" /></div>
					</div>
				</div>
			</form>';
		return ob_get_clean();
	} // end of fn ConfirmationStageContent

	private function ConfirmationStageSubmit($data = array())
	{	$donatecheck = $this->DonationStageSubmit($_SESSION[$this->session_name]);
		if ($donatecheck['failmessage'])
		{	$this->stage = 1;
			return array('failmessage'=>'You must tell us what you want to donate', 'successmessage'=>'');
		} else
		{	$personalcheck = $this->PersonalStageSubmit($_SESSION[$this->session_name]);
			if ($personalcheck['failmessage'])
			{	$this->stage = 2;
				return array('failmessage'=>'You must complete your details', 'successmessage'=>'');
			} else
			{	$this->stage = 4;
				$saved = $this->donation->Create($_SESSION[$this->session_name], $this->campaign);
				if ($this->donation->CanBePaid())
				{	if (!SITE_TEST)
					{	$this->bodyOnLoadJS[] = '$("form#wp_button").submit()';
					}
				} else
				{	return array('failmessage'=>$saved['failmessage']);
				}
			}
		}
		return array('failmessage'=>'', 'successmessage'=>'');
	} // end of fn ConfirmationStageSubmit

	public function PaymentStageContent()
	{	ob_start();
		if ($this->donation->CanBePaid())
		{	echo $this->donation->PaymentButton(), $this->donation->GATransactionTrackingCode(), '<div class="gatewayRedirect">please wait ... redirecting to Worldpay</div>';
		}
		return ob_get_clean();
	} // end of fn PaymentStageContent

	function MainBodyContent()
	{	if(!$image = $this->campaign->GetImageSRC('large')){
			$image = $avatar = $this->campaign->GetAvatarSRC('large');
		}
		//echo '<div class="container page-header" '.(($image) ? 'style="background-image:url('.$image.');"':'').'><div class="container_inner"><h1 class="page-header-title">Donate</h1><p>Donating to '.$this->campaign->FullTitle().' campaign.</p></div></div>';
		echo $this->do_cart ? '' : $this->DonationBreadcrumbs($this->campaign->DonateLink()), $this->MainDonateContent();
	} // end of fn MemberBody

} // end of class CRStarsCampaignDonationPage

$page = new CRStarsCampaignDonationPage();
$page->Page();
?>