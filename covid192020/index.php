<?php
/**
 * Stripe - Payment Gateway integration example (Stripe Checkout)
 */
// Stripe library
require '../landing-pages-commons/stripe/Stripe.php';
$params = array(
    "testmode"   => "off",
    "private_live_key" => "sk_live_tpgBJlRErfe4Tm0iVYjwWykp",
    "public_live_key"  => "pk_live_9J7JiUJNtubpxNklNaLfTNVd",
    "private_test_key" => "sk_test_4UdqBVJQbrV1KXObfvs5yMUD00yIDQjeui",
    "public_test_key"  => "pk_test_0qLdOirAp6ECXSBdDJQ7MSNh00zqhKysUB"
);
$receipt_email = "digital@biggorillaapps.com";
$track_email_id = '';
$track_email_encoded = md5(time() . rand(0, 89739));
if (isset($_GET['teston'])) {
    $params['testmode'] = 'on';
    $receipt_email = 'zeeshan@biggorillaapps.com';
}
if ($params['testmode'] == "on") {
    Stripe::setApiKey($params['private_test_key']);
    $pubkey = $params['public_test_key'];
    $prikey = $params['private_test_key'];
} else {
    Stripe::setApiKey($params['private_live_key']);
    $pubkey = $params['public_live_key'];
    $prikey = $params['private_live_key'];
}
if (isset($_POST['stripeToken'])) {
    $invoiceid = microtime();                      // Invoice ID
    $description = "Invoice #" . $invoiceid ;
    try {
        $charge = Stripe_Charge::create(
            array(
                "amount" => intval(substr($_POST['donationammount'], 0, -2) * 100),
                "currency" => "gbp",
                "source" => $_POST['stripeToken'],
                "description" => $description
            )
        );
        // Payment has succeeded, no exceptions were thrown or otherwise caught				
        $result = "success";
    } catch (Stripe_CardError $e) {
        $error = $e->getMessage();
        $result = "declined";
    } catch (Stripe_InvalidRequestError $e) {
        $result = "declined";
    } catch (Stripe_AuthenticationError $e) {
        $result = "declined";
    } catch (Stripe_ApiConnectionError $e) {
        $result = "declined";
    } catch (Stripe_Error $e) {
        $result = "declined";
    } catch (Exception $e) {
        if ($e->getMessage() == "zip_check_invalid") {
            $result = "declined";
        } else if ($e->getMessage() == "address_check_invalid") {
            $result = "declined";
        } else if ($e->getMessage() == "cvc_check_invalid") {
            $result = "declined";
        } else {
            $result = "declined";
        }
    }
    if ($result == "success") { ?>
        <div class="alert_g">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Success!</strong> Thanks for your contribution!
        </div>
        <?php
        $to = $receipt_email;
        $subject = "Donation email";
        $message = '<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<title>Charity Right</title>
<style type="text/css">
    @media only screen and (max-width: 479px) {	
	.offerBox {float:none !important; width:100% !important; padding-top:20px !important;}
	.offerHd {padding-bottom:0 !important;}
	.contentBox {padding:15px !important;}
	.offerCon {padding:0 15px !important;}
	.botContent {padding:15px !important;}
	.footer {padding:0 15px !important;}
	.footerLinks {font-size:13px !important; padding-bottom:5px !important;}
    }
</style>
</head>
<body style="background:#f6f2f2; margin:0;">
<table cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="#ffffff" style="max-width:700px;">
	<tr>
    	<td style="padding:20px;background: #000;" align="center"><img src="https://charityright.org.uk/covid192020/images/logo.png" alt="" style="vertical-align:top;"></td>
    </tr>
    <tr>
    	<td><img src="https://charityright.org.uk/covid192020/images/emailimages/banner.jpg" alt="" style="vertical-align:top; width:100%;"></td>
    </tr>
    <tr>
    	<td>
        	<table cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td class="contentBox" style="padding:30px; font-size:15px; font-family:Arial, Helvetica, sans-serif; color:#787676; line-height:22px;">
                    Hi Admin,<br><br>
                    <strong style="font-size:18px;">Hope you are doing well.</strong><br><br>
                    Name: ' . $_POST['username99'] . '<br>
                    Email: ' . $_POST['stripeEmail'] . '<br>
                    Donation For: Support against COVID19 <br>
                    Amount: £' . intval($charge->amount / 100) . '<br>
                    </td>
                </tr>
            </table>                
        </td>
    </tr>
    <tr>
    	<td><img src="https://charityright.org.uk/covid192020/images/emailimages/footer_bg.jpg" alt="" style="vertical-align:top; width:100%;"></td>
    </tr>
</table>
</body>
</html>';
        // Set Unique id for tracking using email ro time
        $track_email_id = @empty($_POST['stripeEmail']) ? time() . $_SERVER['REMOTE_ADDR'] : $_POST['stripeEmail'];
        $track_email_encoded = md5($track_email_id);
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: <webmaster@example.com>' . "\r\n";
        /*$headers .= 'Cc: myboss@example.com' . "\r\n";*/
        mail($to, $subject, $message, $headers);
        // submit to donation record api
        $page_name = $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        $page_name = $uriSegments['1'];
        $data['donor_name'] = $_POST['username99'];
        $data['donor_email'] = $_POST['stripeEmail'];
        $data['currency'] = 'gbp';
        $data['donation_amount'] = intval($charge->amount / 100);
        $data['donation_for']    = 'Support against COVID19';
        $data['donation_page']   = $page_name;
        $data['zakat']        = @$_POST['zakat'];
        $data['gift_aid']     = @$_POST['gift_aid'];
        $data['home_address'] = @$_POST['home_address'];
        $data['postcode']     = @$_POST['postcode'];
        $data['country']      = @$_POST['country'];
        $data['test_mode'] = $params['testmode'];
        // api key 
        $data['api_key'] = '3408878710';
        $curl_target = "https://" . $_SERVER['HTTP_HOST'] . "/donations-record/index.php";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curl_target);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        if ($data['test_mode'] == 'on') {
            echo '<code> <pre> ' . print_r($server_output) . ' </pre> </code>';
        }
        ?>
    <?php } else { ?>
        <div class="alert_r">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Declined!</strong><?php echo $result; ?>
        </div>
<?php
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <title>COVID19 | Charity Right</title>
    <!--[if IE]>
    <script src="js/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/stylesheet.css" type="text/css">
    <link rel="stylesheet" href="css/responsive.css" type="text/css">
    <link rel="stylesheet" href="css/slick.css" type="text/css">
    <link href="css/payment.css" rel="stylesheet" type="text/css" media="all">
    <link href="../landing-pages-commons/css/ga-modal-style.css" rel="stylesheet" type="text/css" media="all">
    <!-- jQuery Modal -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <?php include('../landing-pages-commons/tracking-codes.php'); ?>
</head>
<body>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=391990840985330&ev=PageView&noscript=1" /></noscript>
    <noscript><img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/v3/?event=init&tid=2612558765069&pd[em]=<?php echo $track_email_encoded; ?>&noscript=1" /></noscript>
    <div class="mainCon">
        <header class="flexBox spacebetween itemCenter">
            <div class="logo"><img src="images/logo.png" alt=""></div>
            <div class="headerRgt flexBox">
                <div class="phone"><img src="images/phone_icon.png" alt=""> 01274 400389</div>
                <div class="mail"><a href="mailto:info@charityright.org.uk"><img src="images/mail_icon.png" alt=""> info@charityright.org.uk</a></div>
            </div>
        </header>
        <div class="bannerCon  " style="background: url(images/banner_img.jpg) no-repeat center top;">
            <div class="container clearfix">
                <div class="banner-left">
                    <div class="bannerHd"><span>Covid-19</span><br> <span>Emergency Fund</span></div>
                    <div class="bannerText ">
                        <p>Charity Right is on the front lines, providing lifesaving food supplies to those affected by the coronavirus.</p>
                        <p>Millions of families have been left in isolation and unable to provide for their families.</p>
                        <p>£20 can help change their situation</p>
                    </div>
                </div>
                <div class="banner-right" id="donation-widget-box">
                    <div class="form-start">
                        <form method="POST">
                            <div class="input-row">
                                <label>Name</label>
                                <input type="text" class="input" placeholder="Name" id="username9" autocomplete="off" required>
                            </div>
                            <div class="input-row">
                                <label>Donation Amount ( £ ) </label>
                                <input type="text" class="input" placeholder="0.00" id="donationamount_input" autocomplete="off" required>
                            </div>

<?php /*
                            <div class="input-row">
                                <label>Donation amount</label>
                                <select id="donationamount_input">
                                    <option value="20"> £20 Can feed a family of four for a whole month </option>
                                    <option value="60"> £60 Can feed 3 families for a whole month </option>
                                    <option value="120"> £120 Can feed 6 families for a whole month </option>
                                    <option value="200"> £200 Can feed 10 families for a whole month </option>
                                    <option value="400"> £400 Can feed 20 families for a whole month </option>
                                    <option value="800"> £800 Can feed 40 families for a whole month </option>
                                    <option value="2000"> £2000 Can feed 100 families for a whole month </option>
                                </select>
                            </div>
*/ ?>

                            <div class="input-row">
                                <label>Is your donation Zakat?</label>
                                <select id="donzakat" onChange="$('#zakat').val(this.value)">
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </div>
                            <button type="button" class="btn gift-aid-open"> Make Donation</button>
                        </form>
                    </div>
                </div>
            </div>
            <form action="" method="POST">
                <script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="<?php echo $pubkey; ?>" data-amount="1000" data-name="Charity Right" data-description="Donation" data-image="https://charityright.org.uk/covid192020/images/logo.png" data-locale="auto" data-currency="gbp" data-zip-code="true">
                </script>
                <input name="username99" value="" id="username99" type="hidden">
                <input name="donationammount" value="1000" id="donationammount" type="hidden">
                <input name="zakat" value="no" id="zakat" type="hidden">
                <input type="hidden" class="input" name="gift_aid">
                <input type="hidden" class="input" name="home_address">
                <input type="hidden" class="input" name="postcode">
                <input type="hidden" class="input" name="country">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script>
                    document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                    $('#username9').on('change', function() {
                        var newname = $(this).val();
                        $("#username99").val(newname);
                    });
                    $('#donationamount_input').on('keyup change', function() {
                        var donateam = (parseFloat($(this).val()) || 0) * 100;
                        changeSelectedAmount(donateam);
                    });
                    function changeSelectedAmount(val) {
                        $("#donationammount").val(val);
                        StripeCheckout.__app.configurations.button0.amount = val;
                    }
                </script>
                <button type="submit" class="submit-stripe-form" style="width: 1px;height: 1px;"></button>
            </form>
        </div>
        <div class="zkatcalc-ribon" id="show-zakat-form">Donate Zakat at Charity Rights - Calculate Zakat Now!</div>
        <div class="grayBg pt80 pb80">
            <div class="container">
                <div class="flexBox contributeBox">
                    <div class="contributeImg"><img src="images/contribute_img.png" alt=""></div>
                    <div class="contributeDet">
                        <span>You can save lives with your small contribution during COVID19 and Ramadan</span>
                        The national lockdown affects the lives of many families who literally can't afford to deal with the Coronavirus pandemic. But, this social distancing will not stop us from helping the most vulnerable communities at their time of need. <br><br>
                        Charity Right is providing families with monthly food packs. The packs contain everything for a family of 4 to be able to eat for a month. Families that would otherwise go hungry no longer need to worry about where their next meal will come from. <br> <br>
                        Your small contribution will be turned into food for a family that really needs it.
                    </div>
                </div>
            </div>
        </div>
        <div class="pt80 pb80">
            <div class="container">
                <div class="tac mdText">
                    This Ramadan, donate to Charity Right and help us deliver vital aid to families and children affected most by Covid-19
                    <span>We can only do this with your generous support.</span>
                </div>
                <div class="flexBox mdBox">
                    <div class="mdDet">
                        <ul class="mdList">
                            <li class="cursor-pointer" data-activates="20"><span>£20</span> can feed a family for a whole month.</li>
                            <li class="cursor-pointer" data-activates="60"><span>£60</span> can feed 3 families for a whole month.</li>
                            <li class="cursor-pointer" data-activates="120"><span>£120</span> can feed 6 families for a whole month.</li>
                            <li class="cursor-pointer" data-activates="200"><span>£200</span> can feed 10 families for a whole month.</li>
                            <li class="cursor-pointer" data-activates="400"><span>£400</span> can feed 20 families for a whole month.</li>
                            <li class="cursor-pointer" data-activates="800"><span>£800</span> can feed 40 families for a whole month.</li>
                            <li class="cursor-pointer" data-activates="2000"><span>£2000</span> can feed 100 families for a whole month.</li>
                        </ul>
                        <div>
                            <button type="submit" class="btn scroll-to-widget" id="button">Make Donation</button>
                        </div>
                    </div>
                    <div class="mdImg" style="background: url(images/md_images.jpg) no-repeat center top;"></div>
                </div>
            </div>
        </div>
        <div class="botCon pt80 pb80" style="background: url(images/bot_bg.jpg) no-repeat right top;">
            <div class="container">
                <div class="flexBox itemCenter botBox">
                    <div class="botImg"><img src="images/bot_img.jpg" alt=""></div>
                    <div class="botDet">
                        Charity Right is a trusted international food programme. We have delivered over 30 million meals to needy communities over the last 6 years. We work with neglected communities around the world.<br><br>
                        Our regular food supports school children and adults in Pakistan, Bangladesh, Sudan and Turkey.
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="flexBox spacebetween">
                    <div class="footerLogo"><img src="images/logo.png" alt=""></div>
                    <div class="footerInfo">
                        <div class="infoBox address"><img src="images/address_icon.png" alt=""> Oakwood Court, City Road, Bradford<br> West Yorkshire United Kingdom BD8 8JY</div>
                        <div class="infoBox phone"><img src="images/phone_icon.png" alt="">01274 400389</div>
                        <div class="infoBox email"><a href="mailto:info@charityright.org.uk"><img src="images/mail_icon.png" alt=""> info@charityright.org.uk</a></div>
                    </div>
                    <div class="footerRgt">
                        Charity Right - Fighting Hunger, One Child at a Time.<br>
                        Registered Charity Number 1163944
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal gftaid-modal" style="text-align:left;">
            <img src="https://charityright.org.uk/img/s960_Giftaid.jpg" alt="" style="width: 100%;">
            <div class="boxes checkclaimGift">
                <input type="checkbox" class="claimGiftAid" id="claimGiftAid">
                <label for="claimGiftAid">Please tick if you’d like to claim Gift Aid</label>
            </div>
            <br>
            <hr>
            <p class="ga-text">By ticking the above box, I confirm I am a UK taxpayer and I would like Charity Right to treat this donation as a Gift Aid donation. I understand that if I pay less income tax and/or capital gains tax than the amount of Gift Aid claimed on all my donations in that year, it is my responsibility to pay any difference</p>
            <div class="ga-form">
                <div class="input-row">
                    <label>Your home address</label>
                    <input type="text" class="input" placeholder="Address" id="home_address" autocomplete="off">
                </div>
                <div class="input-row">
                    <label>Post code</label>
                    <input type="text" class="input" placeholder="" id="postcode" autocomplete="off">
                </div>
            </div>
            <button type="button" class="btn make-donation-btn"> Skip </button>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAHImaffLZPznceJFmDYimIN9Rd9tuJGtc&libraries=places'></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5/jquery.googlemap.min.js'></script>
    <script src="js/script.js"></script>
    <script>
        $(function() {
            $(".gift-aid-open").click(function() {
                var donateam = (parseFloat($('#donationamount_input').val()) || 0) * 100;
                changeSelectedAmount(donateam);
                $(".gftaid-modal").modal();
            });
            $(".make-donation-btn").click(function() {
                // $("#stripe-donation-form").get(0).submit();
                $.modal.close();
                $(".submit-stripe-form").click();
            });
            $("#claimGiftAid").change(function() {
                if ($(this).is(":checked")) {
                    $(".make-donation-btn").text('CLAIM GIFT AID');
                    $("input[name='gift_aid']").val('yes');
                } else {
                    $(".make-donation-btn").text('Skip');
                    $("input[name='gift_aid']").val('no');
                }
            });
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('home_address'), {
                types: ['geocode']
            });
            // Avoid paying for data that you don't need by restricting the set of
            // place fields that are returned to just the address components.
            autocomplete.setFields(['address_component']);
            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        });
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            var address = [];
            var postcode = country = '';
            // // Get each component of the address from the place details,
            // // and then fill-in the corresponding field on the form.
            if (place != undefined) {
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (addressType == 'street_number' || addressType == 'route' || addressType == 'locality' || addressType == 'postal_town') {
                        if (addressType == 'street_number') {
                            var val = place.address_components[i]['short_name'];
                            address.push(val);
                        }
                        if (addressType == 'route' || addressType == 'locality' || addressType == 'postal_town') {
                            var val = place.address_components[i]['long_name'];
                            address.push(val);
                        }
                    }
                    if (addressType == 'country') {
                        var country = place.address_components[i]['short_name'];
                    }
                    if (addressType == 'postal_code') {
                        var postcode = place.address_components[i]['short_name'];
                    }
                }
                if (country) {
                    $('#country').val(country);
                    $('input[name=country]').val(country);
                }
                $('#postcode').val('');
                if (postcode) {
                    $('#postcode').val(postcode);
                    $('input[name=postcode]').val(postcode);
                }
                $("input[name=home_address]").val($("input#home_address").val());
            }
        }
    </script>

    <?php include('../landing-pages-commons/zakat-calculation-form.php'); ?>
    <script>
        $(function(){
            jQuery('#currency').val('UK');
            jQuery('#currency').trigger('change');
        });
    </script>
</body>
</html>