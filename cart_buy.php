<?php
include_once('init.php');

class CartDelPage extends CartPage
{	var $order;

	public function __construct() // constructor
	{	parent::__construct();
		if ($this->customer->id)
		{
			if (!$this->PlateDocsDone())
			{	header('location: ' . SITE_URL . 'cart_docs.php');
				exit;
			}
			if (!$this->customer->details['countrycode'])
			{	header('location: ' . SITE_URL . 'cart_addr.php');
				exit;
			}
			if (!$_POST['accept'])
			{	header('location: cart_confirm.php?error=accept');
				exit;
			}
			
			$this->order = new SCOrder();
			if ($this->order->CreateFromSession($this->customer))
			{	// then redirect to paypal
				if (!SITE_TEST)
				{	
					$this->bodyOnLoadJS[] = "document.getElementById(\"paypal_button\").submit()";
				}
			}
		} else
		{	echo $this->LoginContent();
		}
	} // end of fn __construct, constructor
	
	protected function CartBody()
	{	ob_start();
		echo '<div class="cartProcessPart">', $this->PayBody();
	//	$this->VarDump($this->order);
		echo '</div>';
		return ob_get_clean();
	} // end of fn CartBody
	
	function PayBody()
	{	ob_start();
		echo "<script type='text/javascript'>\n$('.cartProcessPart').html('<div id=\"pp-redirect\"><h2 class=\"heading\">redirecting to PayPal&nbsp;&nbsp;<img src=\"", SITE_URL, "img/template/loading1.gif\" alt=\"going to paypal\" /></h2></div><div id=\"pp-cc\"><img id=\"card-image\" src=\"", SITE_URL, "img/template/pp-screen.jpg\" alt=\"pay by credit or debit card\" /><div>Please note you can still pay by credit or debit card ... just click here</div></div><br class=\"clear\" /></div>');\n</script>\n"; 
		$this->NoScript();
	/*	for ($i = 1; $i <= 51; $i++)
		{	echo '<p>', $i, ': <img src="', SITE_URL, 'img/loading/loading', $i, '.gif" alt="going to paypal" /></p>';
		}*/
		return ob_get_clean();
	} // end of fn PayBody
	
	function NoScript()
	{	echo "<div id='package-details'>\n";
		$this->PayPalButton();
		echo "</div>\n";
	} // end of fn NoScript
	
	function PayPalButton()
	{	$pp_standard = new PayPalStandard();
		echo "<noscript><p id='jsTurnedOff'>", $this->GetTranslatedText("ppjsturnedoff"), "</p></noscript>\n";
		$pp_standard->CartOrderButton($this->order);
	} // end of fn PayPalButton
	
} // end of defn CartDelPage

$page = new CartDelPage();
$page->Page();
?>