<?php
include_once('init.php');

class CartConfirmPage extends CartPage
{	protected $cartPageName = 'Confirm Your Purchase';

	public function __construct() // constructor
	{	parent::__construct();
		if (!$this->PlateDocsDone())
		{	header('location: ' . SITE_URL . 'cart_docs.php');
			exit;
		}
		if (!$this->customer->details['countrycode'])
		{	header('location: ' . SITE_URL . 'cart_addr.php');
			exit;
		}
		switch ($_GET['error'])
		{	case 'accept':
				$this->failmessage = 'You must accept our terms & conditions before proceeeding';
		}
	} // end of fn __construct, constructor
	
	function CartBodyHolder()
	{	ob_start();
		if (!$this->customer->id)
		{	echo $this->LoginContent();
		}
		echo parent::CartBodyHolder();
		return ob_get_clean();
	} // end of fn MemberBody
	
	protected function CartBody()
	{	ob_start();
		echo $this->CartSummaryHeader();
		if ($this->customer->id)
		{	echo '<div class="cartProcessPart"><h2 class="heading">Please check your order</h2>', $this->CartConfirmForm(), '</div>';
		}
		return ob_get_clean();
	} // end of fn CartBody

	public function CartConfirmForm()
	{	ob_start();
	//	$order = new SCOrder();
	//	$order->CreateFromSession($this->customer);
	//	$this->VarDump($order);
		$tc_page = new PageContent('terms-conditions');
		echo '<form id="delform" method="post" action="', SITE_URL, 'cart_buy.php"><p class="cartAccept"><input type="checkbox" value="1" name="accept" /><label>Please confirm that you accept our <a href="', $tc_page->Link(), '">terms &amp; conditions</a></label></p><p><input type="submit" class="submit" value="Place this order" /></p><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn CartConfirmForm
	
} // end of defn CartConfirmPage

$page = new CartConfirmPage();
$page->Page();
?>