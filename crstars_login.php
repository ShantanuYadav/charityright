<?php
require_once('init.php');

class CRStarsRegisterPage extends CRStarsPage
{	private $team;

	public function __construct()
	{	parent::__construct();
		$this->stages[1] = array('heading'=>'Start a Campaign');
		$this->stages[2] = array('heading'=>'Campaign Details');
		$this->stages[3] = array('heading'=>'Share Campaign');

		if ($_GET['teamid'] || $_GET['teamslug'])
		{	$this->team = new Campaign($_GET['teamid'], $_GET['teamslug']);
			if (!$this->team->CanBeJoined($this->camp_customer->id))
			{	unset($_GET['teamid'], $_GET['teamslug'], $this->team);
			}
		}

		if ($this->camp_customer->id)
		{	header('location: ' . ($this->team->id ? $this->team->JoinLink() : (SITE_SUB . '/my-cr-stars/')));
			exit;
		}
		$this->canonical_link = ($this->team->id ? $this->team->LoginLink() : (SITE_SUB . '/cr-stars/log-in/'));

	} // end of fn __construct

	function MainBodyContent()
	{
		echo '<div class="container crstarsRegisterHeader"><div class="container_inner"><h1 class="page_heading"><span class="register_heading_white">Start</span><br />Campaigning<br />and Help<br /><span class="register_heading_yellow">End</span><br />Hunger</h1></div></div>';
		echo $this->RegisterBreadcrumbs();
		echo $this->CampaignLoginForm();
	} // end of fn MemberBody

} // end of class CRStarsRegisterPage

$page = new CRStarsRegisterPage();
$page->Page();
?>