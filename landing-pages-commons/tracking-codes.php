
<?php if (!empty($result) && $result == 'success') { ?>
<script>
dataLayer = [{
    'fbq_purchase_value': '<?php echo (float) $charge->amount / 100; ?>',
    'fbq_purchase_currency': '<?php echo $charge->currency; ?>'
}];
</script>
<?php } ?>

<script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PX5XJXX');
</script>
<!-- Global site tag (gtag.js) - Google Ads: 876060108 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-876060108"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-876060108');
</script>

<script>
    
    <?php if (!empty($result) && $result == 'success') { ?>
        gtag('event', 'conversion', {
            'send_to': 'AW-925998688/rJJRCO2R530Q4LzGuQM',
            'value': '<?php echo (float) $charge->amount / 100; ?>',
            'currency': '<?php echo strtoupper($charge->currency); ?>',
            'transaction_id': '<?php echo $invoiceid; ?>'
        });
        gtag('event', 'conversion', {
            'send_to': 'AW-876060108/jTMXCL7FvM8BEMy73qED',
            'value': '<?php echo (float) $charge->amount / 100; ?>',
            'currency': '<?php echo strtoupper($charge->currency); ?>',
            'transaction_id': '<?php echo $invoiceid; ?>'
        });
    <?php } ?>
</script>

<!-- Criteo Code -->
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"> </script>
<script type="text/javascript">
    window.criteo_q = window.criteo_q || [];
    var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
    window.criteo_q.push({
        event: "setAccount",
        account: 70936
    }, {
        event: "setSiteType",
        type: deviceType
    }, {
        event: "viewList",
        item: ["1"]
    });
</script>

<?php if (!empty($result) && $result == 'success') { ?>

<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"> </script>
<script type="text/javascript">
    window.criteo_q = window.criteo_q || [];
    var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
    window.criteo_q.push({
        event: "setAccount",
        account: 70936
    }, {
        event: "setSiteType",
        type: deviceType
    }, {
        event: "setEmail",
        email: "<?php echo $track_email_id; ?>"
    }, {
        event: "trackTransaction",
        id: "",
        item: [{
            id: "1",
            price: "<?php echo (float) $charge->amount / 100; ?>",
            quantity: 1
        }]
    });
</script>
<?php } ?>
<!-- End Criteo Code -->
<!-- Pinterest Tag -->
<script>
    ! function(e) {
        if (!window.pintrk) {
            window.pintrk = function() {
                window.pintrk.queue.push(Array.prototype.slice.call(arguments))
            };
            var n = window.pintrk;
            n.queue = [], n.version = "3.0";
            var t = document.createElement("script");
            t.async = !0, t.src = e;
            var r = document.getElementsByTagName("script")[0];
            r.parentNode.insertBefore(t, r)
        }
    }("https://s.pinimg.com/ct/core.js");
    pintrk('load', '2612558765069', {
        em: '<?php echo $track_email_encoded; ?>'
    });
    pintrk('page');
</script>
<?php if (!empty($result) && $result == 'success') { ?>
    <script>
        pintrk('track', 'checkout', {
            order_id: '<?php echo $invoiceid; ?>',
            value: <?php echo (float) $charge->amount / 100; ?>,
            order_quantity: 1,
            currency: '<?php echo $charge->currency; ?>'
        });
    </script>
<?php } ?>
<!-- end Pinterest Tag -->
<!-- Snap Pixel Code -->
<script type='text/javascript'>
    (function(e, t, n) {
        if (e.snaptr) return;
        var a = e.snaptr = function() {
            a.handleRequest ? a.handleRequest.apply(a, arguments) : a.queue.push(arguments)
        };
        a.queue = [];
        var s = 'script';
        r = t.createElement(s);
        r.async = !0;
        r.src = n;
        var u = t.getElementsByTagName(s)[0];
        u.parentNode.insertBefore(r, u);
    })(window, document, 'https://sc-static.net/scevent.min.js');
    snaptr('init', 'e3fc8fa5-d8f9-47b6-a384-1859c44e5594', {
        'user_email': '<?php echo $track_email_id; ?>',
        'user_hashed_email': '<?php echo $track_email_encoded; ?>',
    });
    snaptr('track', 'PAGE_VIEW');
</script>
<!-- End Snap Pixel Code -->
<!-- Twitter universal website tag code -->
<script>
    ! function(e, t, n, s, u, a) {
        e.twq || (s = e.twq = function() {
                s.exe ? s.exe.apply(s, arguments) : s.queue.push(arguments);
            }, s.version = '1.1', s.queue = [], u = t.createElement(n), u.async = !0, u.src = '//static.ads-twitter.com/uwt.js',
            a = t.getElementsByTagName(n)[0], a.parentNode.insertBefore(u, a))
    }(window, document, 'script');
    // Insert Twitter Pixel ID and Standard Event data below
    twq('init', 'nv0yr');
    twq('track', 'PageView');
</script>
<!-- End Twitter universal website tag code -->

<?php if (!empty($result) && $result == 'success') { ?>
    <!-- Twitter universal website tag code -->
    <script>
        ! function(e, t, n, s, u, a) {
            e.twq || (s = e.twq = function() {
                    s.exe ? s.exe.apply(s, arguments) : s.queue.push(arguments);
                }, s.version = '1.1', s.queue = [], u = t.createElement(n), u.async = !0, u.src = '//static.ads-twitter.com/uwt.js',
                a = t.getElementsByTagName(n)[0], a.parentNode.insertBefore(u, a))
        }(window, document, 'script');
        // Insert Twitter Pixel ID and Standard Event data below
        twq('init', 'nv0yr');
        twq('track', 'Purchase', {
            //required parameters
            value: '<?php echo (float) $charge->amount / 100; ?>',
            currency: '<?php echo $charge->currency; ?>',
        });
    </script>
    <!-- End Twitter universal website tag code -->
<?php } ?>