
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='https://www.w3.org/1999/xhtml'>
<head prefix='og: https://ogp.me/ns#'><script src="https://cdn-eu.pagesense.io/js/biggorillaapps/c86fcb9d0f1d4e45bec7b8eda31a04f7.js"></script>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<meta name='viewport' content='width=device-width, initial-scale=1.0' />
<title>Thank you for your donation</title>
<meta name="DESCRIPTION" content="" /><meta property="og:title" content="Thank you for your donation" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://charityright.org.uk/img/template/logo_og.png" />
<meta property="og:url" content="/donate-thank-you/" />
<meta property="og:description" content="Charity Right is an international food programme providing life-changing school meals to the most vulnerable children in the world. Our vision is to see every child receiving a nutritious school meal in every place of education." />
<meta name="theme-color" content="#ec277a">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="viewport" content="width=device-width, initial-scale=1.0"><link rel="shortcut icon" href="https://charityright.org.uk/favicon.ico"><!--[if IE]><link rel="shortcut icon" href="https://charityright.org.uk/favicon.ico"><![endif]--> <link rel='stylesheet' href='/css/jquery.mmenu.all.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/jqModal.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/magnific-popup.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/global.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/social-share-kit.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/website-icons-embedded.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/slick-theme.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/slick.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/select2.min.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/bootstrap.min.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/animate.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.2/css/all.css' integrity='sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr' crossorigin='anonymous' />
<link rel='stylesheet' href='/css///code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/style.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/page_donate-thank-you.css?ver=57130' type='text/css' media='screen' />
<link rel='stylesheet' href='/css/page.css?ver=57130' type='text/css' media='screen' />
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
<script type='text/javascript' src='/js/jquery.payform.min.js?ver=57153'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAHImaffLZPznceJFmDYimIN9Rd9tuJGtc&libraries=places'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5/jquery.googlemap.min.js'></script>
<script type='text/javascript' src='/js/jquery.fitvids.js?ver=57153'></script>
<script type='text/javascript' src='/js/jquery.idTabs.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/jquery.mmenu.all.js?ver=57153'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jqModal/1.4.2/jqModal.min.js'></script>
<script type='text/javascript' src='/js/jquery.magnific-popup.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/jquery.placeholder.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/global.js?ver=57153'></script>
<script type='text/javascript' src='/js/social-share-kit.js?ver=57153'></script>
<script type='text/javascript' src='/js/paymentvarified.js?ver=57153'></script>
<script type='text/javascript' src='/js///code.jquery.com/ui/1.12.1/jquery-ui.js'></script>
<script type='text/javascript' src='/js/popper.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/bootstrap.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/select2.full.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/jquery.validate.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/slick.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/wow.min.js?ver=57153'></script>
<script type='text/javascript' src='/js/custom.js?ver=57153'></script>
<script>jsSiteRoot='/';</script>
<link rel="canonical" href="/donate-thank-you/" /><script type="text/javascript">var switchTo5x=true;</script>

<script type="text/javascript"> var _d_site = _d_site || "0041993206B9F42E67E17825"; </script>
<script src="//widget.privy.com/assets/widget.js"></script>


<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-62069931-1', 'auto');
ga('send', 'pageview');
</script>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PX5XJXX');</script>
<script type='text/javascript'>
window.__lo_site_id = 98686;

(function() {
var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
})();
</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=AW-925998688"></script>
<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		
		  gtag('config', 'AW-925998688');
		</script>
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,"script",
  "https://connect.facebook.net/en_US/fbevents.js");
  fbq("init", "391990840985330");
</script>
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true">   </script>
<script type="text/javascript">
			window.criteo_q = window.criteo_q || [];
			var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
			window.criteo_q.push(
				{ event: "setAccount", account: 70936 },
				{ event: "setSiteType", type: deviceType },
				{ event: "setEmail", email: "" },
				{ event: "trackTransaction", id: "0", item: [
				{ id: "1", price: "0", quantity: 1 } ]}
			);
			</script></head>
<body>
<mtn-widget-modal charity-id="charity-right-uk" locale="en-GB"></mtn-widget-modal>
<script src="https://www.mytennights.com/widget/script.js" defer></script><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PX5XJXX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>
		!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
		},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
		a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
		// Insert Twitter Pixel ID and Standard Event data below
		twq('init','nv0yr');
		twq('track','PageView');
		</script>

<script type='text/javascript'>
		(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
		{a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
		a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
		r.src=n;var u=t.getElementsByTagName(s)[0];
		u.parentNode.insertBefore(r,u);})(window,document,
		'https://sc-static.net/scevent.min.js');
	
		snaptr('init', 'e3fc8fa5-d8f9-47b6-a384-1859c44e5594', {
		'user_email': '',
		'user_hashed_email': 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
		});
	
		snaptr('track', 'PAGE_VIEW');
	
		</script>

<script>
		!function(e){if(!window.pintrk){window.pintrk = function () {
		window.pintrk.queue.push(Array.prototype.slice.call(arguments))};var
		  n=window.pintrk;n.queue=[],n.version="3.0";var
		  t=document.createElement("script");t.async=!0,t.src=e;var
		  r=document.getElementsByTagName("script")[0];
		  r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js");
		pintrk('load', '2612558765069', {em: '3e571ea6e78b601c1498f4510479b1a8'});
		pintrk('page');
		</script>
<noscript>
		<img height="1" width="1" style="display:none;" alt=""
		  src="https://ct.pinterest.com/v3/?event=init&tid=2612558765069&pd[em]=3e571ea6e78b601c1498f4510479b1a8&noscript=1" />
		</noscript>
<div class="donate-page"><header>
<nav class="navbar navbar-expand-lg navbar-light">
<a class="navbar-brand" href="https://charityright.org.uk/"><img src="/img/homeimages/header-logo.png"></a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNav">
<ul class="navbar-nav">
<li class="nav-item drop-main">
<a class="nav-link main-dropdown-header dropbtn" href="#">What We Do</a>
<div class="dropdown-content">
<a href="/post/uyghur-in-turkey/">Uyghur in Turkey</a>
<a href="/post/sudan/">Eritreans in Sudan</a>
<a href="/post/rohingya/">Rohingya</a>
<a href="/post/bangladesh/">Bangladesh Slums</a>
<a href="/post/pakistan/">Thar Desert Pakistan</a>
</div>
</li>
<li class="nav-item drop-main">
<a class="nav-link main-dropdown-header dropbtn" href="#">Get involved</a>
<div class="dropdown-content">
<a href="/join-our-team/">Volunteer</a>
<a href="/careers/">Careers</a>
<a href="https://tn172-b3d915.pages.infusionsoft.net/">Volunteer Experience</a>
</div>
</li>
<li class="nav-item drop-main">
<a class="nav-link main-dropdown-header dropbtn" href="/cr-stars/">Fundraise</a>
<div class="dropdown-content">
<a href="/cr-stars/log-in/">Start a Campaign</a>
<a href="/cr-stars/campaigns/">Find a Campaign</a>
<a href="/team365/">Team365</a>
</div>
</li>
<li class="nav-item">
<a class="nav-link" href="/posts/blog/">News and views</a>
</li>
<li class="nav-item drop-main">
<a class="nav-link main-dropdown-header dropbtn" href="#">About Us</a>
<div class="dropdown-content">
<a href="/why-food/">Why Food</a>
<a href="/who-we-are/">Who Are We</a>
<a href="/our-approach/">Our Approach</a>
<a href="/who-we-are/#trustees">Our People</a>
<a href="/who-we-are/#contact-us">Contact Us</a>
</div>
</li><li class="nav-item donate-btn">
<a class="nav-link donate-link" href="/donate/">Donate</a>
</li>
<li class="nav-item"><a class="nav-link" href="/cr-stars/log-in/" title="log in"><i class="icon icon-login"></i></a></li>
</ul>
</div>
</nav>
</header>
<div class="modal fade cr-search-modal" id="cr-search-modal" tabindex="-1" role="dialog" aria-labelledby="bank-aacount-modalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">

<a class="nav-link hrlSearchPopupCloser" data-dismiss="modal"><i class="icon icon-cancel"></i></a>
<div class="modal-body">
<form id="crSearchMainBar" method="get" action="/cr-stars/search/" onsubmit="return CRSearchCanSubmit('crSearchMainBar');">
<input type="text" class="search-input" placeholder="Search CR Stars Campaigns..." name="crs_search" value="" /><button type="submit" class="search-button"><i class="icon icon-search"></i></button>
</form>
</div>
</div>
</div>
</div><div class="container page-header"><div class="container_inner"><h1 class="page-header-title">Thank you</h1>
<p>Thank you for your donation!</p></div></div><div class="container pageContentContainer"><div class="container_inner"><p>We&rsquo;ve received your generous gesture and we&rsquo;d like to thank you for donating to Charity Right. Your donation will help us to feed people who struggle to eat on a daily basis.</p>
<p>You should receive an email confirmation from us soon but if you don&rsquo;t, or if you have any questions, please email <a href="/cdn-cgi/l/email-protection#99f0f7fff6d9faf1f8ebf0ede0ebf0fef1edb7f6ebfeb7ecf2"><span class="__cf_email__" data-cfemail="11787f777e517279706378656863787679653f7e63763f647a">[email&#160;protected]</span></a>.</p>
<p>Thank you,</p>
<p>The Charity Right Team</p></div></div><script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>
			pintrk('track', 'checkout', {
			order_id: '0',
			value: 0,
			order_quantity: 1,
			currency: ''
			});
			</script>
<script type='text/javascript'>
			(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
			{a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
			a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
			r.src=n;var u=t.getElementsByTagName(s)[0];
			u.parentNode.insertBefore(r,u);})(window,document,
			'https://sc-static.net/scevent.min.js');
			
			snaptr('init', 'e3fc8fa5-d8f9-47b6-a384-1859c44e5594', {
			'user_email': '',
			'user_hashed_email': 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
			});
			
			snaptr('track','PURCHASE',{'currency':'','price':0,'transaction_id':'0'});
			
			</script>

<script>
			!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
			},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
			a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
			// Insert Twitter Pixel ID and Standard Event data below
			twq('init','nv0yr');
			twq('track','Purchase', {
				//required parameters
				value: '0',
				currency: '',
			});
			</script>
<div class="post-share-buttons">
<a href="#share" class="share-button">Share Charity Right</a>
<div class="ssk-group ssk-count ssk-round" data-url="https://charityright.org.uk/" data-tile="Please make a donation to Charity Right" data-text="Nearly one billion people go to bed hungry every night. I have just made my donation. Now it is your turn.">
<a href="" class="ssk ssk-facebook"></a>
<a href="" class="ssk ssk-twitter"></a>
<a href="" class="ssk ssk-google-plus"></a>
<a href="" class="ssk ssk-pinterest"></a>
<a href="" class="ssk ssk-tumblr"></a>
<a href="" class="ssk ssk-email"></a>
</div>
</div>

<script>fbq('track', 'Purchase',{currency:"USD", value: 0.00})</script><footer>
<div class="container">
<div class="footer-logo">
<img src="/img/homeimages/footer-logo.png">
</div>
<ul class="main-ul">
<li class="footer-li">
<a class="nav-link" hr ef="#">Our Work</a>
<ul class="footer-inner-ul">
<li><a href="https://charityright.org.uk/posts/projects/">Projects</a></li>
<li><a href="https://charityright.org.uk/posts/stories/">Stories</a></li>
</ul>
</li>
<li class="footer-li">
<a class="nav-link" href="#">Get involved</a>
<ul class="footer-inner-ul">
<li><a href="https://charityright.org.uk/donate/">Give a regular gift</a></li>
<li><a href="https://charityright.org.uk/events/">Hold an event</a></li>
<li><a href="https://charityright.org.uk/join-our-team/">Become a volunteer</a></li>
<li><a href="https://charityright.org.uk/donate/">Give a one-off gift</a></li>
<li><a href="https://charityright.org.uk/posts/projects/">Leave a legacy</a></li>
<li><a href="https://charityright.org.uk/join-our-team/">Become a partner</a></li>
</ul>
</li>
<li class="footer-li">
<a class="nav-link" href="https://charityright.org.uk/cr-stars/">Fundraise</a>
</li>
<li class="footer-li">
<a class="nav-link" href="#">News and views</a>
</li>
<li class="footer-li">
<a class="nav-link" href="#">About us</a>
<ul class="footer-inner-ul">
<li><a href="https://charityright.org.uk/who-we-are/">Who We Are</a></li>
<li><a href="https://charityright.org.uk/why-food/">Why Food?</a></li>
<li><a href="https://charityright.org.uk/our-approach/">Our Approach</a></li>
<li><a href="https://charityright.org.uk/refund-policy/">Refund Policy</a></li>
<li><a href="https://charityright.org.uk/use-of-cookies/">Privacy & Cookies</a></li>
</ul>
</li>
<li class="footer-li">
<a class="nav-link" href="#">Get in touch</a>
</li>
<li class="footer-li donate-btn">
<a class="nav-link donate-link" href="/donate">Donate</a>
</li>
</ul>
<p class="charity-right">© Charity Right - Registered Charity Number: 1163944</p>
</div>
</footer></div>
<script type="text/javascript">
var $zoho=$zoho || {};
$zoho.salesiq = $zoho.salesiq || {widgetcode:"0c59fe0ac7260564ee58d19b0c505caa63977c5c6332851e8b5bd79ee94ad5c2d26184bd399c39c25760b4f3715a450b", 
	values:{},ready:function(){}};
var d=document;
s=d.createElement("script");
s.type="text/javascript";s.id="zsiqscript";
s.defer=true;
s.src="https://salesiq.zoho.com/widget";
t=d.getElementsByTagName("script")[0];
t.parentNode.insertBefore(s,t);
d.write("<div id='zsiqwidget'></div>");
</script>

<div id="fb-root"></div>
<script type="text/javascript">(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, "script", "facebook-jssdk"));</script><script type="text/javascript">
		adroll_adv_id = "TYFC5JMQS5FG5MGYQ64EYJ";
		adroll_pix_id = "2UNEFFNLRZFZVBQT2S4OBV";
	
		(function () {
			var _onload = function(){
				if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
				if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
				var scr = document.createElement("script");
				var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
				scr.setAttribute('async', 'true');
				scr.type = "text/javascript";
				scr.src = host + "/j/roundtrip.js";
				((document.getElementsByTagName('head') || [null])[0] ||
					document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
			};
			if (window.addEventListener) {window.addEventListener('load', _onload, false);}
			else {window.attachEvent('onload', _onload)}
		}());
	</script></body></html>