<div id="zakat-form-modal" class="zakat-form-modal">
    <h1 class="zakat-heading">Zakat calculator</h1>
    
    <p class="zakat-subheading">Enter all assets that have been in your possession over a lunar year in the text boxes below:</p>

    <hr>

    <form action="" method="post" class="" id="zakatForm" novalidate="novalidate">
        <p class="zakat-form-row" style="display: none;">
            <label for="currency">Select currency</label>
            <select name="currency" id="currency" class="select-field">
                <option value="UK" selected="">UK Pounds £</option>
                <option value="US">US Dollars $</option>
                <option value="EURO">Euros €</option>
            </select>
        </p>
        <div class="clearfix"></div>

        <p class="zakat-form-row">
            <label for="nisab"><a href="#" target="_parent">Nisab</a> (updated 09 April 2020):</label>
            <input type="text" class="text-field" name="nisab" id="nisab" value="£239.00" readonly>
        </p>
        <div class="clearfix"></div>
        <p class="zakat-form-row">
            <label for="gold">Value of Gold</label>
            <input type="text" class="text-field" name="gold" id="gold" value="0">
        </p>
        <div class="clearfix"></div>
        <p class="zakat-form-row">
            <label for="Silver">Value of Silver</label>
            <input type="text" class="text-field" name="silver" id="silver" value="0">
        </p>
        <div class="clearfix"></div>
        <h3>Cash</h3>
        <p class="zakat-form-row">
            <label for="cash1">In hand and in bank accounts</label>
            <input type="text" class="text-field" name="cash1" id="cash1" value="0">
        </p>
        <div class="clearfix"></div>
        <p class="zakat-form-row">
            <label for="cash2">Deposited for some future purpose <br> e.g. Hajj</label>
            <input type="text" class="text-field" name="cash3" id="cash2" value="0">
        </p>
        <div class="clearfix"></div>
        <p class="zakat-form-row">
            <label for="cash3">Given out in loans</label>
            <input type="text" class="text-field" name="cash3" id="cash3" value="0">
        </p>
        <div class="clearfix"></div>
        <p class="zakat-form-row">
            <label for="cash4">Business investments, shares, saving certificates, pensions funded by money in one’s possession</label>
            <input type="text" class="text-field" name="cash4" id="cash4" value="0">
        </p>
        <div class="clearfix"></div>
        <h3>Trade Goods</h3>
        <p class="zakat-form-row">
            <label for="stock">Value of stock</label>
            <input type="text" class="text-field" name="stock" id="stock" value="0">
        </p>
        <div class="clearfix"></div>
        <h3>Liabilities</h3>
        <p class="zakat-form-row">
            <label for="loans">Borrowed money, goods bought on credit</label>
            <input type="text" class="text-field" name="loans" id="loans" value="0">
        </p>
        <div class="clearfix"></div>
        <p class="zakat-form-row">
            <label for="wages">Wages due to employees</label>
            <input type="text" class="text-field" name="wages" id="wages" value="0">
        </p>
        <div class="clearfix"></div>
        <p class="zakat-form-row">
            <label for="tax">Taxes, rent, utility bills due immediately</label>
            <input type="text" class="text-field" name="tax" id="tax" value="0">
        </p>
        <div class="clearfix"></div>
        <p class="submitContent" style="height:80px;margin: 10px 20px;">
            <input type="submit" class="submit submitForm large" id="submitForm" name="submitForm" value="Calculate">
            <input type="Reset" class="submit gray submitForm large" id="reset" name="reset" value="Reset">
        </p>
        <div class="clearfix"></div>
        <div class="assets" style="display: none;">
            Total assets
            <span class="total" id="total"></span>
        </div>
        <div class="payable" style="display: block;">
            Zakat payable
            <span class="total total_zkaat" id="zakat">$0.00</span>
        </div>
        <div class="zdonate" style="display: none;">
            <button type="button" class="submit submitRed large" id="zakatDonate" style="float:right">Pay your Zakat now</button>
        </div>
    </form>
</div>
<style>
    #zakat-form-modal {
        display: none;
        max-width: 768px;
    }

    .clearfix{
        clear:both;
    }

    .zkatcalc-ribon{
        width: 100%;
        padding: 10px;
        font-size: 18px;
        text-align: center;
        color: red;
        background: black;
        cursor: pointer;
    }


    @media screen and (max-width: 768px) {


        .zkatcalc-ribon{
            width: 100%;
            padding: 10px;
            font-size: 20px !important;
            text-align: center;
            color: red;
            background: black;
            cursor: pointer;
        }

        #zakat-form-modal .zakat-heading {
            font-size: 26px !important;
            color: #ec277a !important;
            text-align: center !important;
            margin-bottom: 20px !important;
            padding: 0 10px !important;
            font-family: 'Poppins', sans-serif !important;
            font-weight: 600 !important;
        }

        .modal{
            padding: 10px 15px !important;
        }

        #zakat-form-modal .zakat-subheading {
            font-size: 14px !important;
            color: #444 !important;
            text-align: left !important;
            margin-bottom: 10px !important;
            padding: 0 10px !important;
            font-family: 'Poppins', sans-serif !important;
            font-weight: 300 !important;
        }

        #zakat-form-modal h3{
            font-size: 18px !important;
            color: #ec277a !important;
            text-align: left !important;
            margin-bottom: 5px !important;
            padding: 0 5px !important;
            font-family: 'Poppins', sans-serif !important;
            font-weight: 600 !important;
        }
        .zakat-form-row .text-field{
            width: 90% !important;
            max-width: 100% !important;
            margin: 5px 30px !important;
        }
        #zakatForm label{
            font-size: 14px !important;
            width: 90% !important;
            padding: 5px !important;
            font-weight: 600 !important;
            text-align: left !important;
            display: block !important;
            margin: 0px !important;
        }

        #zakatForm .submitForm,#zakatForm .submitRed{
            width: auto !important;

        }
    }

    #zakat-form-modal .zakat-heading {
        font-size: 26px;
        color: #ec277a;
        text-align: center;
        margin-bottom: 40px;
        padding: 0 50px;
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
    }

    #zakat-form-modal .zakat-subheading {
        font-size: 16px;
        color: #444;
        text-align: center;
        margin-bottom: 10px;
        padding: 0 50px;
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
    }

    #zakat-form-modal h3{
        font-size: 18px;
        color: #ec277a;
        text-align: left;
        margin-bottom: 20px;
        padding: 0 20px;
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
    }
    .assets,.payable{
        font-size: 18px;
        color: #ec277a;
        text-align: left;
        margin-bottom: 20px;
        padding: 0 20px;
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
    }

    /* #show-zakat-form {
        font-family: 'Open Sans', sans-serif;
        margin: 10px 0px;
        width: 100%;
        background-color: #f7d54f;
        color: black;
        font-size: 16px;
        font-weight: bold;
        padding: 15px;
        border: 1px solid transparent;
        cursor: pointer;
    } */

    #zakatForm .submit{
        border: 1px solid #007dc6;
        padding: 10px;
        margin: 10px;
        color: #0a0a0a;
        background-color: transparent;
        width: 320px;
        max-width: 678px;
        font-family: 'Poppins', sans-serif;
        font-size: 18px;
        line-height: 28px;
        
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-transition: all 250ms linear;
        -moz-transition: all 250ms linear;
        -o-transition: all 250ms linear;
        -ms-transition: all 250ms linear;
        transition: all 250ms linear;
        border-radius: 4px;
        margin-top: 1px;
        float: right;
    }

    #zakatForm .text-field,
    #zakatForm .select-field,
    .wpcf7 .wpcf7-form .wpcf7-textarea,
    .wpcf7 .wpcf7-form .wpcf7-text,
    .wpcf7 .wpcf7-form .wpcf7-select {
        border: 1px solid #007dc6;
        padding: 10px;
        color: #0a0a0a;
        background-color: transparent;
        width: 350px;
        max-width: 678px;
        font-family: 'Poppins', sans-serif;
        font-size: 18px;
        line-height: 28px;
        
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-transition: all 250ms linear;
        -moz-transition: all 250ms linear;
        -o-transition: all 250ms linear;
        -ms-transition: all 250ms linear;
        transition: all 250ms linear;
        border-radius: 4px;
        margin-top: 1px;
        float: right;
    }

    .zakat-form-row{
        display: block;
        margin: 5px;
        width: 100%;
        height: 60px;
    }
    #zakatForm label{
        width: 100%;
        padding: 10px 30px;
        font-weight: 600;
        text-align: left;
        display: block;
    }

    #zakatForm .select-field,
    .wpcf7 .wpcf7-select {
        height: 49px;
        padding: 0;
    }

    #zakatForm .text-field:focus,
    #zakatForm .select-field:focus,
    .wpcf7 .wpcf7-form .wpcf7-control .wpcf7-select:focus,
    .wpcf7 .wpcf7-form .wpcf7-textarea:focus,
    .wpcf7 .wpcf7-form .wpcf7-text:focus {
        border: 2px solid #007dc6 !important;
        outline: 0 !important;
        margin-top: 0;
        background-color: #f8f8f8;
    }

    .wpcf7 .wpcf7-form .wpcf7-submit {
        border: none;
        color: #fff;
        margin-top: 0;
        font-family: "DINNextLTPro-Bold", Arial;
        font-size: 16px;
        line-height: 50px;
        width: auto;
        cursor: pointer;
        -webkit-appearance: none;
        min-width: 140px;
        position: relative;
        margin-left: 23.71%;
        background-color: #18307f;
        text-align: left;
        padding: 0 40px 0 40px;
        text-transform: uppercase;
        box-shadow: 4px 4px 10px 0px rgba(0, 0, 0, 0.27);
        text-align: center;
        border-radius: 5px;
    }

    .wpcf7 .wpcf7-form .wpcf7-submit:hover {
        background-color: #007dc6;
    }

    .wpcf7 .wpcf7-form .wpcf7-validation-errors {
        width: 100%;
        max-width: 391px;
        margin-left: 0;
        margin-right: 0;
        padding-left: 10px;
        padding-right: 10px;
    }
    .submitForm{
        
    }
    .submitForm,.submitRed{
        cursor: pointer;
    }

    .submit-stripe-form{
        visibility: hidden;
    }
</style>
<script>
    var UKvalue = "239";
    var EUROvalue = "271";
    var USvalue = "277";

    var nisabUK = Math.round(UKvalue * 100) / 100;
    var nisabUS = Math.round(USvalue * 100) / 100;
    var nisabEURO = Math.round(EUROvalue * 100) / 100;

    function update() {
        var currency = jQuery('#currency').val();
        var value;


        if (currency == "UK") {
            value = nisabUK;
        }

        if (currency == "US") {
            value = nisabUS;
        }

        if (currency == "EURO") {
            value = nisabEURO;
        }

        // padding decimal places for curreny
        var value_string = value.toString()
        var decimal_location = value_string.indexOf(".")

        if (decimal_location == -1) {
            decimal_part_length = 0
            value_string += 2 > 0 ? "." : ""
        } else {
            decimal_part_length = value_string.length - decimal_location - 1
        }

        var pad_total = 2 - decimal_part_length

        if (pad_total > 0) {
            for (var counter = 1; counter <= pad_total; counter++)
                value_string += "0"
        }

        if (currency == "UK") {
            fmtvalue = "£" + value_string;
        }

        if (currency == "US") {
            fmtvalue = "$" + value_string;
        }

        if (currency == "EURO") {
            fmtvalue = "€" + value_string;
        }

        jQuery("#nisab").val(fmtvalue);
    }

    // Function for formatting zakat value
    function fmtPrice(value) {
        var currency = jQuery('#currency').val();
        var symbol;

        if (currency == "UK") {
            symbol = "£";
        }
        if (currency == "US") {
            symbol = "$";
        }

        if (currency == "EURO") {
            symbol = "€";
        }

        var result = symbol + Math.floor(value) + ".";
        var cents = 100 * (value - Math.floor(value)) + 0.47;

        result += Math.floor(cents / 10).toString();
        result += Math.floor(cents % 10).toString();

        return result;
    }


    // Function computit to calculate zakat
    function computeIt() {
        // Calculation
        var gld     = parseFloat(jQuery('#gold').val()) || 0;
        var slvr    = parseFloat(jQuery('#silver').val()) || 0;
        var cash1   = parseFloat(jQuery('#cash1').val()) || 0;
        var cash2   = parseFloat(jQuery('#cash2').val()) || 0;
        var cash3   = parseFloat(jQuery('#cash3').val()) || 0;
        var cash4   = parseFloat(jQuery('#cash4').val()) || 0;
        var stock   = parseFloat(jQuery('#stock').val()) || 0;
        var loans   = parseFloat(jQuery('#loans').val()) || 0;
        var wages   = parseFloat(jQuery('#wages').val()) || 0;
        var tax     = parseFloat(jQuery('#tax').val()) || 0;

        var total = eval(gld) + eval(slvr) + eval(cash1) + eval(cash2) + eval(cash3) + eval(cash4) + eval(stock) - eval(loans) - eval(wages) - eval(tax);

        // Display formatted values.
        var nisab;
        var currency = jQuery('#currency').val();

        if (currency == "UK") {
            nisab = nisabUK;
        }

        if (currency == "US") {
            nisab = nisabUS;
        }
        if (currency == "EURO") {
            nisab = nisabEURO;
        }

        jQuery('#total').html(fmtPrice(total));

        if (total >= nisab) {
            var x = fmtPrice(total * 0.025);
            jQuery("#zakatDonate").attr('data-zakat',(parseFloat(total * 0.025)||0));
            jQuery('.total_zkaat').html(x);
            jQuery("#donzakat").val('yes');
            setTimeout(function(){
                jQuery("#username9").focus();
            },1500);
        } else {
            jQuery("#zakatDonate").attr('data-zakat',fmtPrice(0));
            jQuery('.total_zkaat').html(fmtPrice(0));
        }
    }

    jQuery(document).ready(function($) {
        "use strict";

        update();

        jQuery("#currency").change(function() {
            update();
        });

        jQuery("#zakatForm").submit(function() {
            computeIt();
            jQuery('.payable, .assets, .zdonate').fadeIn();
            return false;
        });

        jQuery("#reset").click(function() {
            jQuery('#total').html(fmtPrice(0));
            jQuery('#zakat').html(fmtPrice(0));
            jQuery('.payable, .assets, .zdonate').fadeOut();
            return true;
        });

        jQuery("#zakatDonate").click(function(){
            var zakat = parseFloat($(this).attr('data-zakat'))||0;
            $("#donationamount_input,#quantityprice").val((zakat).toFixed(2));
            $("#donationamount_input,#quantityprice").trigger('change');
            $.modal.close();
        });
        jQuery("#show-zakat-form").click(function(){
            $('.zakat-form-modal').modal('show');
        });
    });
</script>