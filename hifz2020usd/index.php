<?php
/**
 * Stripe - Payment Gateway integration example (Stripe Checkout)
 */
// Stripe library
require '../landing-pages-commons/stripe/Stripe.php';
$params = array(
    "testmode"   => "off",
    "private_live_key" => "sk_live_tpgBJlRErfe4Tm0iVYjwWykp",
    "public_live_key"  => "pk_live_9J7JiUJNtubpxNklNaLfTNVd",
    "private_test_key" => "sk_test_4UdqBVJQbrV1KXObfvs5yMUD00yIDQjeui",
    "public_test_key"  => "pk_test_0qLdOirAp6ECXSBdDJQ7MSNh00zqhKysUB"
);
$receipt_email = "digital@biggorillaapps.com";
$track_email_id = '';
$track_email_encoded = md5(time() . rand(0, 923874));
if (isset($_GET['teston']) || $_SERVER['HTTP_HOST'] == 'crlocal.host') {
    $params['testmode'] = 'on';
    $receipt_email = 'zeeshan@biggorillaapps.com';
}
if ($params['testmode'] == "on") {
    Stripe::setApiKey($params['private_test_key']);
    $pubkey = $params['public_test_key'];
    $prikey = $params['private_test_key'];
} else {
    Stripe::setApiKey($params['private_live_key']);
    $pubkey = $params['public_live_key'];
    $prikey = $params['private_live_key'];
}
if (isset($_POST['stripeToken'])) {
    $invoiceid   = microtime();                      // Invoice ID
    $description = "Invoice #" . " AM-" . $invoiceid;
    try {
        $charge = Stripe_Charge::create(
            array(
                "amount" => intval(substr($_POST['donationammount'], 0, -2) * 100),
                "currency" => "usd",
                "source" => $_POST['stripeToken'],
                "description" => $description
            )
        );
        // Payment has succeeded, no exceptions were thrown or otherwise caught				
        $result = "success";
    } catch (Stripe_CardError $e) {
        $error = $e->getMessage();
        $result = "declined";
    } catch (Stripe_InvalidRequestError $e) {
        $result = "declined";
    } catch (Stripe_AuthenticationError $e) {
        $result = "declined";
    } catch (Stripe_ApiConnectionError $e) {
        $result = "declined";
    } catch (Stripe_Error $e) {
        $result = "declined";
    } catch (Exception $e) {
        if ($e->getMessage() == "zip_check_invalid") {
            $result = "declined";
        } else if ($e->getMessage() == "address_check_invalid") {
            $result = "declined";
        } else if ($e->getMessage() == "cvc_check_invalid") {
            $result = "declined";
        } else {
            $result = "declined";
        }
    }
    if ($result == "success") { ?>
        <div class="alert_g">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Success!</strong> Thanks for your contribution!
        </div>
        <?php
        $to = $receipt_email;
        $subject = "Donation email";
        $message = '<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<title>Charity Right</title>
<style type="text/css">
    @media only screen and (max-width: 479px) {	
	.offerBox {float:none !important; width:100% !important; padding-top:20px !important;}
	.offerHd {padding-bottom:0 !important;}
	.contentBox {padding:15px !important;}
	.offerCon {padding:0 15px !important;}
	.botContent {padding:15px !important;}
	.footer {padding:0 15px !important;}
	.footerLinks {font-size:13px !important; padding-bottom:5px !important;}
    }
</style>
</head>
<body style="background:#f6f2f2; margin:0;">
<table cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="#ffffff" style="max-width:700px;">
	<tr>
    	<td style="padding:20px;background: #000;" align="center"><img src="https://charityright.org.uk/allmeals2020/images/logo.png" alt="" style="vertical-align:top;"></td>
    </tr>
    <tr>
    	<td><img src="https://charityright.org.uk/allmeals2020/images/emailimages/banner.jpg" alt="" style="vertical-align:top; width:100%;"></td>
    </tr>
    <tr>
    	<td>
        	<table cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td class="contentBox" style="padding:30px; font-size:15px; font-family:Arial, Helvetica, sans-serif; color:#787676; line-height:22px;">
                    Hi Admin,<br><br>
                    <strong style="font-size:18px;">Hope you are doing well.</strong><br><br>
                    Name: ' . $_POST['username99'] . '<br>
                    Email: ' . $_POST['stripeEmail'] . '<br>
                    Donation For: ' . $_POST['fordonation99'] . '<br>
                    Amount: $' . intval($charge->amount / 100) . '<br>
                    </td>
                </tr>
            </table>                
        </td>
    </tr>
    <tr>
    	<td><img src="https://charityright.org.uk/allmeals2020/images/emailimages/footer_bg.jpg" alt="" style="vertical-align:top; width:100%;"></td>
    </tr>
</table>
</body>
</html>';
        // Set Unique id for tracking using email ro time
        $track_email_id = @empty($_POST['stripeEmail']) ? time() . $_SERVER['REMOTE_ADDR'] : $_POST['stripeEmail'];
        $track_email_encoded = md5($track_email_id);
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: <info@charityright.org.uk>' . "\r\n";
        /*$headers .= 'Cc: myboss@example.com' . "\r\n";*/
        mail($to, $subject, $message, $headers);
        // submit to donation record api
        $page_name = $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        $page_name = $uriSegments['1'];
        $data['donor_name'] = $_POST['username99'];
        $data['donor_email'] = $_POST['stripeEmail'];
        $data['currency'] = 'usd';
        $data['donation_amount'] = intval($charge->amount / 100);
        $data['donation_for'] = $_POST['fordonation99'];
        $data['donation_page'] = $page_name;
        $data['zakat']        = @$_POST['zakat'];
        $data['gift_aid']     = @$_POST['gift_aid'];
        $data['home_address'] = @$_POST['home_address'];
        $data['postcode']     = @$_POST['postcode'];
        $data['country']      = @$_POST['country'];
        $data['test_mode'] = $params['testmode'];
        // api key 
        $data['api_key'] = '3408878710';
        if ($params['testmode'] == 'on') {
            print_r($data);
        }
        $curl_target = "https://" . $_SERVER['HTTP_HOST'] . "/donations-record/index.php";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curl_target);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        if ($data['test_mode'] == 'on') {
            echo '<code> <pre> ' . print_r($server_output) . ' </pre> </code>';
        }
        ?>
    <?php } else { ?>
        <div class="alert_r">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Declined!</strong> <?php echo $result; ?>.
        </div>
<?php }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>charity landing page</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/response.css">
    <link href="css/payment.css" rel="stylesheet" type="text/css" media="all">
    <link href="../landing-pages-commons/css/ga-modal-style.css" rel="stylesheet" type="text/css" media="all">
    <!-- jQuery Modal -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <?php include('../landing-pages-commons/tracking-codes.php'); ?>
</head>
<body>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PX5XJXX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <noscript><img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/v3/?event=init&tid=2612558765069&pd[em]=<?php echo $track_email_encoded; ?>&noscript=1" /></noscript>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=391990840985330&ev=PageView&noscript=1" /></noscript>
    <div class="mainCon">
        <!--Start Header -->
        <header class="header clearfix">
            <div class="logo">
                <a href="#"><img src="images/ch-logo.png" alt=""></a>
            </div>
            <div class="headerRgt">
                <nav class="menu">
                    <a href="javascript:void(0)" class="showMobMenu"><i></i><i></i><i></i></a>
                    <ul class="menuLink">
                        <li><img src="images/tele.png" class="hdr-ic"> <a href="tel:01274400389">01274 400 389</a></li>
                        <li> <img src="images/mail.png" class="hdr-ic"> <a href="mailto:info@charityright.org.uk">info@charityright.org.uk</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <!--End Header -->
        <!--Start Banner -->
        <div class="bannerSlider">
            <div class="item" style="background:url(images/charity-banner.png) no-repeat center;">
                <div class="container flexBox itemCenter justifyCon">
                    <div class="bannerInfo bw">
                        <div class="bannerHd">Donate <span class="w-col"> to provide food to Hifz students </span> <br> <small>a great Sadaqah Jariyah</small> </div>
                        <div class="bannerTxt">This Ramadan help us provide regular food to Hifz students, many of whom are orphans and refugees. $25 can feed a student memorising the Quran for 1 month.</div>
                    </div>
                    <div class="form donor-form" id="donation-widget-box">
                        <form action="" method="post">
                            <div class="fbn-grp">
                                <label class="labelFor">Full Name</label>
                                <input type="text" name="username9" id="username9" class="form-control" required>
                            </div>
                            <div class="fbn-grp">
                                <label class="labelFor">Donation Amount</label>
                                <input type="text" name="quantityprice" id="quantityprice" value="25.00" class="form-control" required>
                            </div>
                            <div class="fbn-grp">
                                <label class="labelFor">Is your donation Zakat?</label>
                                <select class="form-control" id="donzakat" onChange="$('#zakat').val(this.value)">
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </div>
                            <button type="button" class="btn don-bt cursor-pointer gift-aid-open"> Make Donation</button>
                        </form>
                    </div>
                </div>
            </div>
            <form action="" method="POST">
                <script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="<?php echo $pubkey; ?>" data-amount="2500" data-name="Charity Right" data-description="Donation" data-image="https://charityright.org.uk/allmeals2020/images/logo.png" data-locale="auto" data-currency="usd" data-zip-code="true"></script>
                <input name="username99" value="" id="username99" type="hidden">
                <input name="donationammount" value="2500" id="donationammount" type="hidden">
                <input name="fordonation99" value="Hifz Student" id="fordonation99" type="hidden">
                <input name="zakat" value="no" id="zakat" type="hidden">
                <input type="hidden" class="input" name="gift_aid">
                <input type="hidden" class="input" name="home_address">
                <input type="hidden" class="input" name="postcode">
                <input type="hidden" class="input" name="country">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script>
                    document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                    $('#username9').on('change', function() {
                        var newname = $(this).val();
                        $("#username99").val(newname);
                    });
                    $('#quantityprice').on('keyup change', function() {
                        var amnt = (parseFloat($(this).val()) || 0) * 100;
                        StripeCheckout.__app.configurations.button0.amount = amnt;
                        $("#donationammount").val(amnt);
                    });
                </script>
                <button type="submit" class="submit-stripe-form cursor-pointer" style="width: 1px;height: 1px;"></button>
            </form>
        </div>
        <div class="zkatcalc-ribon" id="show-zakat-form">Donate Zakat at Charity Rights - Calculate Zakat Now!</div>
        <!--banner end -->
        <section class="c-bg pd-60">
            <div class="container flexBox itemCenter justifyCon">
                <div class="vd-sec">
                    <div class="videoBox">
                        <iframe width="560" height="380" src="https://www.youtube.com/embed/KTR6S8muTPE?t=2s" frameborder="0" allow="autoplay; fullscreen" class="videoFile"></iframe>
                    </div>
                </div>
                <div class="vd-txt">
                    <p>At Charity Right we focus on providing food for children and families in the deserts of Pakistan, slums of Bangladesh, refugee camps for Rohingya, refugee camps in Sudan and to Uyghur refugees in Turkey.</p>
                    <p>In some of these countries we also feed Hifz students. Many of these students are refugees or orphans making them highly vulnerable.</p>
                    <p>Charity Right provides them with 3 meals a day every day to ensure their bodies have what they need to grow.</p>
                    <p>We provided almost 1 million meals to 1000 children who are memorizing Qur’an By donating meals for Hifz’s students you can be part of this great Sadaqah Jariya project and reap the rewards of all that the children memorise and will inshallah teach. </p>
                </div>
            </div>
        </section>
        <!--video section end -->
        <section class="pd-60">
            <div class="container spons">
                <h1 class="rm-hd">This Ramadan feed a HIfz student</h1>
                <div class="flexBox itemCenter bx-3wrp">
                    <div class="tw-bx cent-cent cursor-pointer" data-autofill-amount="25">
                        <h1 class="euro">$25</h1>
                        <p>Feeds a Qur'an</p>
                        <p>student for one month</p>
                    </div>
                    <div class="tw-bx cent-cent cursor-pointer" data-autofill-amount="150">
                        <h1 class="euro">$150</h1>
                        <p>Feeds a Qur'an</p>
                        <p>student for six month</p>
                    </div>
                    <div class="tw-bx cent-cent cursor-pointer" data-autofill-amount="300">
                        <h1 class="euro">$300</h1>
                        <p>Feeds a Qur'an</p>
                        <p>student for one year</p>
                    </div>
                </div>
                <div class="don-wrp">
                    <a href="javascript:;" class="don-bt scroll-to-widget cursor-pointer">Make a donation</a></div>
            </div>
        </section>
        <section class="stu-sec">
            <div class="pd-60">
                <div class="container flexBox itemCenter justifyCon">
                    <div class="img-txt">
                        <p>Feeding a Hifz student is a beautiful opportunity for you to earn surplus rewards in the Akhirah. Helping someone learn the book of Allah will earn you good deeds even though you are busy with your daily chores.</p>
                        <p>Investing in learning & teaching of Qur’an is one of the best forms of of Sadaqah Jariyah (Ongoing charity) and the donor can expect to receive Allah’s blessings. If the recipient has passed on his learning then the rewards continue through the medium of his students. Such is the great reward of investing your wealth in Qur’an learning.</p>
                    </div>
                    <div class="img-sec">
                        <img src="images/student1.jpg">
                    </div>
                </div>
            </div>
        </section>
        <section class="ftr">
            <div class="container flexBox justifyCon inner-ft">
                <div class="ft-lg-wrp">
                    <img src="images/ft-logo.png">
                </div>
                <div class="ft-add">
                    <ul class="main-add">
                        <li>
                            <ul class="add-sub flexBox">
                                <li><img src="images/location.png"></li>
                                <li> Oakwood Court, City Road, Bradford West Yorkshire United Kingdom BD8 8JY</li>
                            </ul>
                        </li>
                        <li>
                            <ul class="add-sub flexBox">
                                <li><img src="images/ft-tele.png"></li>
                                <li><a href="tel:01274400389">01274 400389</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul class="add-sub flexBox">
                                <li><img src="images/ft-mail.png"></li>
                                <li><a href="mailto:info@charityright.org.uk" class="ft-mail">info@charityright.org.uk</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="ft-cont">
                    <p>Charity Right - Fighting Hunger, One Child at a Time. Registered Charity Number 1163944</p>
                </div>
            </div>
        </section>
    </div>
    <!--mainCon -->
    <div class="modal gftaid-modal" style="text-align:left;">
        <img src="https://charityright.org.uk/img/s960_Giftaid.jpg" alt="" style="width: 100%;">
        <div class="boxes checkclaimGift">
            <input type="checkbox" class="claimGiftAid" id="claimGiftAid">
            <label for="claimGiftAid">Please tick if you’d like to claim Gift Aid</label>
        </div>
        <br>
        <hr>
        <p class="ga-text">By ticking the above box, I confirm I am a UK taxpayer and I would like Charity Right to treat this donation as a Gift Aid donation. I understand that if I pay less income tax and/or capital gains tax than the amount of Gift Aid claimed on all my donations in that year, it is my responsibility to pay any difference</p>
        <div class="ga-form">
            <div class="fbn-grp">
                <label class="labelFor">Your home address</label>
                <input type="text" placeholder="Address" id="home_address" class="form-control" autocomplete="off">
            </div>
            <div class="fbn-grp">
                <label class="labelFor">Post code</label>
                <input type="text" placeholder="Post Code" id="postcode" class="form-control" autocomplete="off">
            </div>
        </div>
        <button type="button" class="btn don-bt cursor-pointer make-donation-btn"> Skip </button>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAHImaffLZPznceJFmDYimIN9Rd9tuJGtc&libraries=places'></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5/jquery.googlemap.min.js'></script>
    <script src="js/script.js"></script>
    <script>
        $(function() {
            $(".gift-aid-open").click(function() {
                var amnt = (parseFloat($('#quantityprice').val()) || 0) * 100;
                StripeCheckout.__app.configurations.button0.amount = amnt;
                $(".gftaid-modal").modal();
            });
            $(".make-donation-btn").click(function() {
                // $("#stripe-donation-form").get(0).submit();
                $.modal.close();
                $(".submit-stripe-form").click();
            });
            $("#claimGiftAid").change(function() {
                if ($(this).is(":checked")) {
                    $(".make-donation-btn").text('CLAIM GIFT AID');
                    $("input[name='gift_aid']").val('yes');
                } else {
                    $(".make-donation-btn").text('Skip');
                    $("input[name='gift_aid']").val('no');
                }
            });
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('home_address'), {
                types: ['geocode']
            });
            // Avoid paying for data that you don't need by restricting the set of
            // place fields that are returned to just the address components.
            autocomplete.setFields(['address_component']);
            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        });
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            var address = [];
            var postcode = country = '';
            // // Get each component of the address from the place details,
            // // and then fill-in the corresponding field on the form.
            if (place != undefined) {
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (addressType == 'street_number' || addressType == 'route' || addressType == 'locality' || addressType == 'postal_town') {
                        if (addressType == 'street_number') {
                            var val = place.address_components[i]['short_name'];
                            address.push(val);
                        }
                        if (addressType == 'route' || addressType == 'locality' || addressType == 'postal_town') {
                            var val = place.address_components[i]['long_name'];
                            address.push(val);
                        }
                    }
                    if (addressType == 'country') {
                        var country = place.address_components[i]['short_name'];
                    }
                    if (addressType == 'postal_code') {
                        var postcode = place.address_components[i]['short_name'];
                    }
                }
                if (country) {
                    $('#country').val(country);
                    $('input[name=country]').val(country);
                }
                $('#postcode').val('');
                if (postcode) {
                    $('#postcode').val(postcode);
                    $('input[name=postcode]').val(postcode);
                }
                $("input[name=home_address]").val($("input#home_address").val());
            }
        }
    </script>
    <?php include('../landing-pages-commons/zakat-calculation-form.php'); ?>
    <script>
        $(function(){
            jQuery('#currency').val('US');
            jQuery('#currency').trigger('change');
        });
    </script>
</body>
</html>