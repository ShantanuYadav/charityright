<?php
require_once('init.php');
require_once 'HTTP/Request.php';
require_once 'stripe-php/init.php';

if (SITE_TEST) {
  //ini_set('display_errors', 1);
  //error_reporting(E_ALL);
}

class AjaxStripepayment extends Base
{
  public function __construct()
  {

    parent::__construct();
    \Stripe\Stripe::setApiKey(STRIPE_API_KEY);

    $alldetail    = $_POST['alldetail'];
    $owner        = $_POST['owner'];
    $cardNumber   = $_POST['cardNumber'];
    $cvv          = $_POST['cvv'];
    $expiry_day   = $_POST['expiry_day'];
    $expiry_year  = $_POST['expiry_year'];
    $sessionccard = $_POST['sessionccard'];
    $alldetail    = json_decode($alldetail);
    // $donType = $_POST['donType'];

    $orderid           = $alldetail->details->orderid;
    $orderdate         = $alldetail->details->orderdate;
    $currency          = $alldetail->details->currency;
    $total_oneoff      = ($alldetail->details->total_oneoff * 100);
    $total_monthly     = $alldetail->details->total_monthly;
    $giftaid           = $alldetail->details->giftaid;
    $donortitle        = $alldetail->details->donortitle;
    $donorfirstname    = $alldetail->details->donorfirstname;
    $donorsurname      = $alldetail->details->donorsurname;
    $donoradd1         = $alldetail->details->donoradd1;
    $donoradd2         = $alldetail->details->donoradd2;
    $donorcity         = $alldetail->details->donorcity;
    $donorpostcode     = $alldetail->details->donorpostcode;
    $donorcountry      = $alldetail->details->donorcountry;
    $donoremail        = $alldetail->details->donoremail;
    $donorphone        = $alldetail->details->donorphone;
    $donorhowheard     = $alldetail->details->donorhowheard;
    $contactpref       = $alldetail->details->contactpref;
    $ddsolesig         = $alldetail->details->ddsolesig;
    $donorhowheardtext = $alldetail->details->donorhowheardtext;

    if (!empty($_POST['stripeToken'])) {
      $stripeToken = $_POST['stripeToken'];

      // if ($donType == 'oneoff') {
      // Charge a credit or a debit card 
      $stripe_desc = $donoremail . ' | Product: ' . $_SESSION[$sessionccard]['stripe_desc'];
      $success = 0;
      try {
		  
		  // Add customer to stripe 
		  $customer = \Stripe\Customer::create(array(
			'email' => $donoremail,
			'source'  => $stripeToken
		  ));
		  
        $charge = \Stripe\Charge::create(array(
          'customer' => $customer->id,
          'amount'   => $total_oneoff,
          'currency' => $currency,
          'description' => $stripe_desc,
          'metadata' => array(
            'order_id' => $orderid
          )
        ));
        $success = 1;
      } catch (\Stripe\Exception\CardException $e) {
        // Since it's a decline, \Stripe\Exception\CardException will be caught
        $error = $e->getError()->message;
      } catch (\Stripe\Exception\RateLimitException $e) {
        $error = $e->getError()->message;
        // Too many requests made to the API too quickly
      } catch (\Stripe\Exception\InvalidRequestException $e) {
        $error = $e->getError()->message;
        // Invalid parameters were supplied to Stripe's API
      } catch (\Stripe\Exception\AuthenticationException $e) {
        $error = $e->getError()->message;
        // Authentication with Stripe's API failed
        // (maybe you changed API keys recently)
      } catch (\Stripe\Exception\ApiConnectionException $e) {
        $error = $e->getError()->message;
        // Network communication with Stripe failed
      } catch (\Stripe\Exception\ApiErrorException $e) {
        $error = "System configuration error!";
        // Display a very generic error to the user, and maybe send
        // yourself an email
		  
		  @$msg = 'Message : '.$e->getError()->message;

			// use wordwrap() if lines are longer than 70 characters
			@$msg = wordwrap($msg,70);

			// send email
			@mail("zeeshan@biggorillaapps.com","CharityRight Stripe Payments ApiErrorException ",$msg);
		  
      } catch (Exception $e) {
        $error = "Cannot process your request!";
        // Something else happened, completely unrelated to Stripe
      }
      // Retrieve charge details 
      if ($success == 1) {
        $chargeJson = $charge->jsonSerialize();

        if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {

          $result   = $this->db->query("SELECT * FROM donations where orderid = '" . $orderid . "'");
          $data = $this->db->FetchArray($result);
          if (!empty($data)) {
            $adminamount  = $data['adminamount'];
            $curanciesval = $this->db->query("SELECT * FROM currencies where curcode = '" . $currency . "'");
            $crandcydata  = $this->db->FetchArray($curanciesval);

            $rate         = $crandcydata['convertrate'];
            $cursymbol    = $crandcydata['cursymbol'];
            $donsql       = $this->db->query("UPDATE donations SET donationref = '" . $chargeJson['id'] . "' , donationconfirmed='" . date('Y-m-d H:s:i') . "' , gateway= 'Stripe' WHERE did= " . $data['did']);

            $pmtfields    = array(
              'gateway="Stripe"'
            );
            $pmtfields[]  = 'donationref="' . $chargeJson['id'] . '"';
            $pmtfields[]  = 'paymentref="' . $chargeJson['id'] . '"';
            $pmtfields[]  = 'currency="' . $currency . '"';
            $pmtfields[]  = 'paydate="' . date('Y-m-d H:s:i') . '"';
            $pmtfields[]  = 'amount=' . $total_oneoff;
            $pmtfields[]  = 'gbpamount=' . round($total_oneoff / $rate, 2);
            $pmtfields[]  = 'adminamount=' . $adminamount;
            $pmtfields[]  = 'gbpadminamount=' . round($adminamount / $rate, 2);
            $pmtfields[]  = 'payerfirstname="' . $donorfirstname . '"';
            $pmtfields[]  = 'payerlastname="' . $donorsurname . '"';
            $pmtfields[]  = 'payercountry="' . $donorcountry . '"';
            $pmtfields[]  = 'payeremail="' . $donoremail . '"';
            $pmtset       = implode(', ', $pmtfields);
            $pmtsql = 'INSERT INTO donationpayments SET ' . $pmtset;
            if ($pmtresult = $this->db->Query($pmtsql)) {

              // $this->Get($orderid);
              $Donation = new Donation();
              $Donation->extraSendOneOffDonationEmail($donoremail, $data);
              $if = new InfusionSoftDonation();
              $if->PayDonation($data);
            } else $fail[] = $pmtsql . '---' . $this->db->Error();
          }

          $result_campaigndonations   = $this->db->query("SELECT * FROM campaigndonations where orderid = '" . $orderid . "'");
          $cm_data = $this->db->FetchArray($result_campaigndonations);
          // echo '<pre>';print_r($cm_data);die;
          if (!empty($cm_data)) {
            $subfields = array('gateway="Stripe"');
            $subfields[] = 'donationref="' . $chargeJson['id'] . '"';
            $subfields[] = 'donationconfirmed="' . date('Y-m-d H:s:i') . '"';
            $subfields[]  = 'payercountry="' . $donorcountry . '"';
            $subfields[]  = 'payeremail="' . $donoremail . '"';

            $name = explode(' ', $_POST['owner'], 2);
            // echo '<pre>';print_r($name);die;
            if (isset($name[0]) && $name[0] != '') {
              $subfields[]  = 'payerfirstname="' . $name[0] . '"';
            }
            if (isset($name[1]) && $name[1] != '') {
              $subfields[]  = 'payerlastname="' . $name[1] . '"';
            }
            $subset = implode(', ', $subfields);
            $subsql = 'UPDATE campaigndonations SET ' . $subset . ' WHERE cdid=' . $cm_data['cdid'];
            // echo '<pre>';print_r($subsql);die;
            if (($result = $this->db->Query($subsql)) && $this->db->AffectedRows()) {
              $CampaignDonation = new CampaignDonation();
              $CampaignDonation->Get($cm_data['cdid']);
              $CampaignDonation->SendDonationEmail();
              $CampaignDonation->SendOwnerDonationEmail();
              $if = new InfusionSoftCampaignDonation();
              $if->PayDonation($cm_data);
            } else $fail[] = $subsql . '---' . $this->db->Error();
          }


          //unset($_SESSION[$sessionccard]);
          $response['message'] = 'success';
          $response['status']  = '1';
          $thankyou_token['amount'] = @$alldetail->details->total_oneoff;
          $thankyou_token['currency'] = $currency;
          $response['thankyou_token']  = base64_encode(json_encode($thankyou_token));
          echo json_encode($response);
        }
      } else {
        if (isset($error) && $error != '') {
          $response['message'] = $error;
        } else {
          $response['message'] = 'Transaction has been failed!';
        }
        $response['status']  = '0';
        echo json_encode($response);
      }

      // }else if ($donType == 'monthly') {

      // }
    } else {
      $response['message'] = 'Error on form submission. ';
      $response['status']  = '0';
      echo json_encode($response);
    }


    //       if ($newArr['reply']['orderStatus']['payment']['lastEvent'] == 'AUTHORISED') {


    //       $result   = $this->db->query("SELECT * FROM donations where orderid = '" . $orderid . "'");
    //       $data = $this->db->FetchArray($result);
    //       $adminamount  = $data['adminamount'];
    //       $curanciesval = $this->db->query("SELECT * FROM currencies where curcode = '" . $currency . "'");
    //       $crandcydata  = $this->db->FetchArray($curanciesval);

    //       $rate         = $crandcydata['convertrate'];
    //       $cursymbol    = $crandcydata['cursymbol'];
    //       $donsql       = $this->db->query("UPDATE donations SET donationref = 'WP~" . $orderid . "' , donationconfirmed='" . date('Y-m-d H:s:i') . "' , gateway= 'WorldPay' WHERE did= " . $data['did']);

    //       $pmtfields    = array(
    //       'gateway="worldpay"'
    //       );
    //       $pmtfields[]  = 'donationref="WP~' . $orderid . '"';
    //       $pmtfields[]  = 'paymentref="WP~' . $orderid . '"';
    //       $pmtfields[]  = 'currency="' . $currency . '"';
    //       $pmtfields[]  = 'paydate="' . date('Y-m-d H:s:i') . '"';
    //       $pmtfields[]  = 'amount=' . $total_oneoff;
    //       $pmtfields[]  = 'gbpamount=' . round($total_oneoff / $rate, 2);
    //       $pmtfields[]  = 'adminamount=' . $adminamount;
    //       $pmtfields[]  = 'gbpadminamount=' . round($adminamount / $rate, 2);
    //       $pmtfields[]  = 'payerfirstname="' . $donorfirstname . '"';
    //       $pmtfields[]  = 'payerlastname="' . $donorsurname . '"';
    //       $pmtfields[]  = 'payercountry="' . $donorcountry . '"';
    //       $pmtfields[]  = 'payeremail="' . $donoremail . '"';
    //       $pmtset       = implode(', ', $pmtfields);
    //       $pmtsql = 'INSERT INTO donationpayments SET ' . $pmtset;
    //       if ($pmtresult = $this->db->Query($pmtsql))
    //       {  

    //       // $this->Get($orderid);
    //       $Donation = new Donation();
    //       $Donation->extraSendOneOffDonationEmail($donoremail,$data);
    //       $if = new InfusionSoftDonation();
    //       $if->PayDonation($data);
    //       } else $fail[] = $pmtsql . '---' . $this->db->Error();

    //     unset($_SESSION[$sessionccard]);
    //     $response['message'] = 'success';
    //     $response['status']  = '1';
    //     echo json_encode($response);

    // } else if ($newArr['reply']['orderStatus']['payment']['lastEvent'] == 'REFUSED') {

    //     $response['message'] = 'Payment REFUSED VISA_CREDIT-SSL ';
    //     $response['status']  = '0';
    //     echo json_encode($response);

    // } else {

    //     $xml    = simplexml_load_string($data, NULL, LIBXML_NOCDATA);
    //     $avc    = json_encode($xml, JSON_PRETTY_PRINT);
    //     $newArr = json_decode($avc, true);

    //     $messages = $newArr['reply']['error'];

    //     $message2 = str_replace("XML failed validation:", " ", $messages);
    //     if ($message2) {

    //         $message = $message2;

    //     } else {
    //         $message = $newArr['reply']['orderStatus']['error'];
    //     }
    //     $response['message'] = $message;
    //     $response['status']  = '0';
    //     echo json_encode($response);

    // }

  }
}
$page = new AjaxStripepayment();


//require_once('config/config.php');
//$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
//http://support.worldpay.com/support/kb/gg/sepa/content/recurringpayments.htm
