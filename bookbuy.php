<?php
require_once('init.php');

class BookingBuyPage extends EventsBasePage
{	private $cart;
	private $order;

	public function __construct()
	{	parent::__construct();
		$this->css[] = 'cart.css';
		$this->css[] = 'page.css';
		$this->cart = new BookingCart($_SESSION['events']);
		if (false && SITE_TEST)
		{	$this->order = new BookOrder(28);
		} else
		{	$this->order = new BookOrder();
		}
		if ($this->cart->CanCheckout())
		{	if (!$this->order->id)
			{	$saved = $this->order->CreateFromCart($this->cart);
			}
			if ($this->order->id)
			{	$this->successmessage = $saved['successmessage'];
				if (!$this->order->details['payref'])
				{	//$this->successmessage = 'submitting to Worldpay';
					if (!SITE_TEST)
					{	$this->bodyOnLoadJS[] = '$("form#wp_button").submit()';
					}
				}
			} else
			{	$this->failmessage = $saved['failmessage'];
			}
		} else
		{	$this->failmessage = 'There are no tickets in your cart';
		}
		$this->canonical_link = SITE_SUB . '/bookbuy.php';
	} // end of fn __construct

	function MainBodyContent()
	{	echo '<div class="container page-header"><div class="container_inner"><h1 class="page_heading">Events</h1></div></div>';
		echo '<div class="container page-section page-section-white"><div class="container_inner">';
		if ($this->order->id)
		{	if ($this->order->details['payref'])
			{	echo '<h3>'.$this->successmessage.'</h3>';
			} else
			{	echo $this->order->PaymentButton(), '<div class="gatewayRedirect">please wait ... redirecting to Worldpay</div>';
			}
		}else{
			echo '<h3>'.$this->failmessage.'</h3>';
		}
		echo '</div></div>';
	} // end of fn MemberBody

} // end of class BookingBuyPage

$page = new BookingBuyPage();
$page->Page();
?>