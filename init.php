<?php 
require_once('config/config.php'); 
//session_name('charityright');
session_start();

// autoload classes
spl_autoload_register
(	function ($className)
	{	if (file_exists($file = CITDOC_ROOT . '/classes/' . strtolower($className) . '.class.php'))
		{	include_once($file);
		}
	} // end of __autoload
);
?>