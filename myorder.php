<?php
include_once('init.php');

class MyOrderPage extends MyAccountPage
{	private $order;

	function __construct() // constructor
	{	parent::__construct('orders');
	} // end of fn __construct, constructor
	
	public function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		$this->order = new SCOrder($_GET['orderid']);
		if ($_GET['ppsuccess'])
		{	$this->successmessage = 'Your purchase has been successful, thank you for your order';
			// now destroy cart
			unset($_SESSION['cart_products']);
			unset($_SESSION['cart_plates']);
		}
		if (is_array($_FILES['platedocs']) && $_FILES['platedocs'])
		{	
			$saved = $this->order->SavePlateDocs($_FILES['platedocs']);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
		if ($this->order->details['custid'] != $this->customer->id)
		{	unset($this->order);
		}
	} // end of fn LoggedInConstruct
	
	protected function MyAccountBody()
	{	ob_start();
		if ($this->order->id)
		{	$address = array();
			if ($addname = $this->InputSafeString($this->order->details['addname']))
			{	$address[] = $addname;
			}
			if ($addstreet = $this->InputSafeString($this->order->details['addstreet']))
			{	$address[] = $addstreet;
			}
			if ($addcity = $this->InputSafeString($this->order->details['addcity']))
			{	$address[] = $addcity;
			}
			if ($addstate = $this->InputSafeString($this->order->details['addstate']))
			{	$address[] = $addstate;
			}
			if ($addcode = $this->InputSafeString($this->order->details['addcode']))
			{	$address[] = $addcode;
			}
			echo '<h2 class="heading">Order Number: ', $this->order->OrderNumber(), '</h2><div class="ma_op_details"><p class="clearfix"><span class="op_label">Order placed</span><span class="op_content">', date('j F Y', strtotime($this->order->details['orderdate'])), '</span></p><p class="clearfix"><span class="op_label">Delivery address</span><span class="op_content">', implode('<br />', $address), '</span></p>';
			$deloption = new DelOption($this->order->details['deloption']);
			if ($deloption->id)
			{	echo '<p class="clearfix"><span class="op_label">Delivery details</span><span class="op_content">', stripslashes($deloption->details['deldesc']), '</span></p>';
			}
			if ($this->order->plates)
			{	$plates_cost = 0;
				foreach ($this->order->plates as $order_plate)
				{	$plates_cost += $order_plate['price'];
				}
				echo '<p class="clearfix"><span class="op_label">', count($this->order->plates) > 1 ? (count($this->order->plates) . ' Plates') : 'Plate', '</span><span class="op_content op_price">&pound;', number_format($plates_cost, 2), '</span></p>';
			}
			$attributes = array();
			if ($this->order->items)
			{	foreach ($this->order->items as $item)
				{	$product = $this->order->ItemProductDetails($item);
					echo '<p class="clearfix"><span class="op_label"><a href="', $product->ProductLink(), '">', $this->InputSafeString($product->details['prodname']), '</a>';
					if ($item['quantity'] > 1)
					{	echo '(', number_format($item['quantity']), ' @ &pound;', number_format($item['price'], 2), ')';
					}
					if ($item['avid'])
					{	foreach ($item['avid'] as $avid)
						{	$att_value = new ProductAttributeValue($avid);
							if (!$attributes[$att_value->details['attid']])
							{	$attributes[$att_value->details['attid']] = new ProductAttribute($att_value->details['attid']);
							}
							echo '<br /><label>', $this->InputSafeString($attributes[$att_value->details['attid']]->details['attname']), '</label>: ', $this->InputSafeString($att_value->details['avname']);
						}
					}
					if ($product->details['addinfo'] && $item['addinfo'])
					{	echo '<br /><label>', $this->InputSafeString($product->details['addinfo']), '</label>: ', $this->InputSafeString($item['addinfo']);
					}
					echo'</span><span class="op_content op_price">&pound;', number_format($item['price'] * $item['quantity'], 2), '</span></p>';
				}
			}
			if ($this->order->details['delprice'])
			{	echo '<p class="clearfix"><span class="op_label">Delivery cost</span><span class="op_content op_price">&pound;', number_format($this->order->details['delprice'], 2), '</span></p>';
			}
			if ($discamount = $this->order->DiscountAmount())
			{	echo '<p class="clearfix"><span class="op_label">Discount (', $this->order->InputSafeString($this->order->details['disccode']), ')</span><span class="op_content op_price">&pound;', number_format($discamount, 2), '</span></p>';
			}
			echo '<p class="clearfix"><span class="op_label">Total Price</span><span class="op_content op_price op_total">&pound;', number_format($this->order->TotalPrice(), 2), '</span></p></div>';
			if ($this->order->plates)
			{	//check if upload form needed
				foreach ($this->order->plates as $order_plate)
				{	if (!$order_plate['docs_nonroad'] && !($order_plate['doc_user'] && $order_plate['doc_reg']))
					{	$form_needed = true;
						echo '<form method="post" action="', $_SERVER["SCRIPT_NAME"], '?orderid=', $this->order->id, '" enctype="multipart/form-data">';
						break;
					}
				}
				
				foreach ($this->order->plates as $plateid=>$order_plate)
				{	echo '<div class="ma_op_plate clearfix">';
					$plate_vars = $this->order->PlateBuilderVars($order_plate);
					$plates = array();
					$builder = new PlateBuilder();

					// Determine which plates to build
					if(isset($plate_vars['type']))
					{	if($plate_vars['type'] == 'both')
						{	$plates[] = new Plate('front');
							$plates[] = new Plate('rear');	
						} else
						{	$plates[] = new Plate($plate_vars['type']);	
						}
					}

					// Build the plates
					if(sizeof($plates))
					{	foreach($plates as $plate)
						{	$builder->process($plate, $plate_vars);
						}
					}

					$plates = $builder->getPlates();
					
					echo '<div class="cart_plate_images">';
					if(sizeof($plates))
					{	foreach($plates as $key => $plate)
						{	echo '<img src="', SITE_SUB, $this->order->PlateImageFilename($plateid, $plate->getType()), '?', time(), '" />';
						}
					}
					echo '</div><div class="cart_plate_options">';
					foreach ($plates as $current_plate)
					{	if ($plate_disp = $this->order->DisplayPlateValues($current_plate, $plateid))
						{	echo $plate_disp;
							break;
						}
					}
					echo '<div class="cart_plate_price"><span class="cartBlockPrice"><span><span>&pound;', number_format($order_plate['price'], 2), '</span></span></span></div></div><div class="cart_plate_docs">';
					if ($order_plate['docs_nonroad'])
					{	$regdoc = new PlateDocument($order_plate['docs_reg']);
						echo '<p>Plate declared for off-road use</p>';
					} else
					{
						if ($order_plate['doc_user'])
						{	$userdoc = new PlateDocument($order_plate['doc_user']);
							echo '<p class="cart_plate_docs_existing clearfix"><span>Document for customer id</span><a href="', SITE_URL, 'show_platedoc.php?id=', $userdoc->id, '" target="_blank">', $this->InputSafeString($userdoc->details['filename']), '</a></p>';
						} else
						{	echo '<p><label>Document for user id</label><input type="file" name="platedocs[', $plateid, '][user]" /></p>';
						}
						if ($order_plate['doc_reg'])
						{	$regdoc = new PlateDocument($order_plate['doc_reg']);
							echo '<p class="cart_plate_docs_existing clearfix"><span>Document for reg id</span><a href="', SITE_URL, 'show_platedoc.php?id=', $regdoc->id, '" target="_blank">', $this->InputSafeString($regdoc->details['filename']), '</a></p>';
						} else
						{	echo '<p class="cart_plate_docs_existing clearfix"><label>Document for reg id</label><input type="file" name="platedocs[', $plateid, '][reg]" /></p>';
						}
						if (!$order_plate['doc_user'] || !$order_plate['doc_reg'])
						{	$popup_docs = new PageContentPopup('platedocs');
							echo '<p class="cart_plate_docs_existing clearfix"><input type="submit" class="submit" value="upload documents" /></p><p class="mopDocsPopupLink hidden-xs">', $popup_docs->PopUpLink(), '</p>';
							
						}
					}

					echo '</div><div class="clear"></div></div>';
				}
				if ($form_needed)
				{	echo '</form>';
				}
			}
			
			if ($_GET['ppsuccess'])
			{	echo '
<!-- Google Code for Purchase Conversion Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 876438309;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "CVRTCKif42gQpcb1oQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/876438309/?label=CVRTCKif42gQpcb1oQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
';
			}
		}
		return ob_get_clean();
	} // end of fn MyAccountBody
	
} // end of class MyOrderPage

$page = new MyOrderPage();
$page->Page();
?>