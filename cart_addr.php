<?php
include_once('init.php');

class CartAddressPage extends CartPage
{	protected $cartPageName = 'Delivery Address';
	
	public function __construct() // constructor
	{	parent::__construct();
	
		if (!$this->PlateDocsDone())
		{	header('location: ' . SITE_URL . 'cart_docs.php');
			exit;
		}

		if ($this->customer->id)
		{	if (isset($_POST['addr1']))
			{	$saved = $this->customer->SaveDeliveryAddress($_POST);
				$this->failmessage = $saved['failmessage'];
				$this->successmessage = $saved['successmessage'];
				if (!$this->failmessage)
				{	header('location: ' . SITE_URL . 'cart_del.php');
					exit;
				}
			}
		}
	} // end of fn __construct, constructor
	
	function CartBodyHolder()
	{	ob_start();
		if (!$this->customer->id)
		{	echo $this->LoginContent();
		}
		echo parent::CartBodyHolder();
		return ob_get_clean();
	} // end of fn MemberBody
	
	protected function CartBody()
	{	ob_start();
		echo $this->CartSummaryHeader();
		if ($this->customer->id)
		{	echo '<div class="cartProcessPart">', $this->customer->DeliveryAddressForm(), '</div>';
		}
		return ob_get_clean();
	} // end of fn CartBody

} // end of defn CartAddressPage

$page = new CartAddressPage();
$page->Page();
?>