<?php
include_once('init.php');

class MyDetailsPage extends MyAccountPage
{	
	function __construct() // constructor
	{	parent::__construct('details');
	} // end of fn __construct, constructor
	
	public function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if (isset($_POST['addr1']))
		{	$saved = $this->customer->SaveDeliveryAddress($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
	} // end of fn LoggedInConstruct
	
	protected function MyAccountBody()
	{	ob_start();
		echo $this->customer->DeliveryAddressForm('', 'Save my details', true);
		return ob_get_clean();
	} // end of fn MyAccountBody

} // end of class MyDetailsPage

$page = new MyDetailsPage();
$page->Page();
?>