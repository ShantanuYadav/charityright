<?php
include_once('init.php');

class MyPasswordPage extends MyAccountPage
{	
	function __construct() // constructor
	{	parent::__construct();
	} // end of fn __construct, constructor
	
	public function LoggedInConstruct()
	{	parent::LoggedInConstruct();
		if (isset($_POST['password']))
		{	$saved = $this->customer->SavePassword($_POST);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
	} // end of fn LoggedInConstruct
	
	protected function MyAccountBody()
	{	ob_start();
		echo $this->customer->PasswordForm();
		return ob_get_clean();
	} // end of fn MyAccountBody
	
} // end of class MyPasswordPage

$page = new MyPasswordPage();
$page->Page();
?>