<?php
include_once('init.php');

class MyOrdersPage extends MyAccountPage
{	
	function __construct() // constructor
	{	parent::__construct('orders');
	} // end of fn __construct, constructor
	
	public function LoggedInConstruct()
	{	parent::LoggedInConstruct();
	} // end of fn LoggedInConstruct
	
	protected function MyAccountBody()
	{	ob_start();
		echo $this->customer->OrdersList();
		//echo $this->customer->OrdersTable();
		return ob_get_clean();
	} // end of fn MyAccountBody
	
} // end of class MyOrdersPage

$page = new MyOrdersPage();
$page->Page();
?>