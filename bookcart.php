<?php
require_once('init.php');

class BookingCartPage extends EventsBasePage
{	private $cart;

	public function __construct()
	{	parent::__construct('events');
		$this->css[] = 'cart.css';
		$this->css[] = 'page.css';
		$this->js[] = 'events.js';
		$this->cart = new BookingCart($_SESSION['events']);
		$this->canonical_link = SITE_SUB . '/bookcart.php';
	} // end of fn __construct

	function MainBodyContent()
	{
		echo '<div class="container page-header"><div class="container_inner"><h1 class="page_heading">Events - Cart</h1></div></div>';
		echo '<div class="container page-section page-section-white"><div class="container_inner">', $this->cart->CartHeader();
		if ($this->cart->CanCheckout())
		{	echo '<div class="cartButtonContainer"><a class="button" href="', SITE_SUB, 'bookcheckout.php">Checkout</a></div>';
		}
		//$this->VarDump($_SESSION);
		echo '</div></div>';
	//	unset($_SESSION['events']['currency']);
	} // end of fn MemberBody

} // end of class BookingCartPage

$page = new BookingCartPage();
$page->Page();
?>