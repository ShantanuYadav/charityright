<?php
require_once('init.php');

class PostCategoryPage extends BasePage
{	private $category;

	public function __construct()
	{	parent::__construct();
		$this->css['posts'] = 'posts.css';
		$this->css['page'] = 'page.css';
	} // end of fn __construct

	protected function BaseConstructFunctions()
	{	$this->category = new PostCategory((int)$_GET['catid'], $_GET['catslug']);
		$this->title = $this->InputSafeString($this->category->details['pagetitle']);
		$this->css['posts'] = 'posts.css';
		if ($this->quickdonate = $this->category->GetQuickDonateOptions())
		{	$this->js['quickdonate'] = 'quickdonate.js';
			$this->css['quickdonate'] = 'quickdonate.css';
		}
		if (file_exists(CITDOC_ROOT . '/js/' . ($jsname = 'postcats_' . $this->category->details['template'] . '.js')))
		{	$this->js[$jsname] = $jsname;
		}
		if (file_exists(CITDOC_ROOT . '/css/' . ($cssname = 'postcats_' . $this->category->details['template'] . '.css')))
		{	$this->css[$cssname] = $cssname;
		}
	} // end of fn BaseConstructFunctions

	protected function HeaderMenuIsSelected(PageContent $page)
	{	if (strstr($this->category->Link(), $page->Link()))
		{	return true;
		}
	} // end of fn HeaderMenuIsSelected

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		$this->fb_meta['title'] = $this->title;
		$this->fb_meta['url'] = $this->category->Link();
		if ($this->category->details['metadesc'])
		{	$this->fb_meta['description'] = $this->InputSafeString($this->category->details['metadesc']);
		}
	} // end of fn SetFBMeta

	public function GetMetaDesc()
	{	return $this->category->details['metadesc'];
	} // end of fn GetMetaDesc

	public function SetCanonicalLink()
	{	if ($this->category->id)
		{	$this->canonical_link = $this->category->Link();
			if ($_GET['country'])
			{	$this->canonical_link .= $_GET['country'] . '/';
			}
		}
	} // end of fn SetCanonicalLink

	public function MainBodyContent()
	{	ob_start();
		@include_once($this->category->TemplateFile());
		echo ob_get_clean();
	} // end of fn MainBodyContent

} // end of class PostCategoryPage

$page = new PostCategoryPage();
$page->Page();
?>
