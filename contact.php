<?php
include_once('init.php');
class ContactUsPage extends PagePage
{	private $form_fields = array('name'=>'R8GQ782a', 'email'=>'z3hk7E6Ct', 'phone'=>'qU99532u', 'query'=>'x2NYm3mcL');

	function __construct()
	{	parent::__construct('contact-us');
		$this->css[] = 'contactus.css';
		
		foreach ($this->form_fields as $name=>$code)
		{	$this->form_fields[$name] = $code . $_COOKIE['PHPSESSID'];
		}
		
		if (isset($_POST[$this->form_fields['name']]))
		{	$saved = $this->SendContactForm($_POST);
			$this->failmessage = $saved['failmessage'];
			if ($this->successmessage = $saved['successmessage'])
			{	unset($_POST);
			}
		}
		
	} // end of fn __construct
	
	function SendContactForm($data = array())
	{	$fields = array();
		$fail = array();
		$success = array();
		
		if ($data['name'] || $data['email'])
		{	$fail[] = 'this form has not been filled in correctly';
		}
		
		if (!$cname = $data[$this->form_fields['name']])
		{	$fail[] = 'You must give us your name';
		}
		
		if ($cemail = $data[$this->form_fields['email']])
		{	if (!$this->ValidEMail($cemail))
			{	$fail[] = 'Your email is not valid, please check your typing';
			}
		}
		
		$cphone = $data[$this->form_fields['phone']];
		
		if (!$cphone && !$cemail)
		{	$fail[] = 'You must give us some contact details';
		}
		
		if (!$fail)
		{	$mail = new CITMail();
			$body = 'Name: ' . $cname . "\n";
			if ($cemail)
			{	$body .= 'Email: ' . $cemail . "\n";
			}
			if ($cphone)
			{	$body .= 'Phone: ' . $cphone . "\n";
			}
			if ($data[$this->form_fields['query']])
			{	$body .= 'Query:' . "\n" . $data[$this->form_fields['query']] . "\n";
			}
			$mail->SendMail('Klick Plates Website Enquiry', $body, $this->GetParameter('siteemails'));
			$success[] = 'Your query has been sent, we will contact you as soon as possible';
		}
		
		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));
		
	} // end of fn SendContactForm
	
	function MainBodyContent()
	{	echo '<div class="container"><div class="container_inner"><h1 class="page_heading">', $this->page->PageTitleDisplay(), '</h1><div class="left_content">', $this->page->HTMLMainContent(), $this->ContactForm(), '</div>', $this->RightSideBar(), '<div class="clear"></div></div></div>';
	} // end of fn MemberBody
	
	protected function MiddleContent()
	{	ob_start();
		echo '<h2 class="heading">', $this->page->PageTitleDisplay(), '</h2>', $this->page->HTMLMainContent(), $this->ContactForm();
		return ob_get_clean();
	} // end of fn MiddleContent
	
	function ContactForm()
	{	ob_start();
		echo '<form id="contactus_form" method="post" action="', SITE_URL, $_SERVER["SCRIPT_NAME"], '">
				<p><label>Name:</label><input type="text" name="name" value="" class="formFilteredE6Ct" /><input type="text" name="', $this->form_fields['name'], '" value="', $this->InputSafeString($_POST[$this->form_fields['name']]), '" /><div class="clear"></div></p>
				<p><label>Email:</label><input type="email" name="email" value="" class="formFilteredE6Ct" /><input type="text" name="', $this->form_fields['email'], '" value="', $this->InputSafeString($_POST[$this->form_fields['email']]), '" /><div class="clear"></div></p>
				<p><label>Phone:</label><input type="tel" name="', $this->form_fields['phone'], '" value="', $this->InputSafeString($_POST['cphone']), '" /><div class="clear"></div></p>
				<p><label>Query:</label><textarea name="', $this->form_fields['query'], '">', $this->InputSafeString($_POST[$this->form_fields['query']]), '</textarea><div class="clear"></div></p>
				<p><label class="hidden-xs">&nbsp;</label><input type="submit" class="submit" value="Contact us" /></p></form>';
		return ob_get_clean();
	} // end of fn ForgottenForm
	
} // end of defn ContactUsPage

$page = new ContactUsPage();
$page->Page();
?>