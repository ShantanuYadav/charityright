<?php
require_once('init.php');

class CRStarsRegisterPage extends CRStarsPage
{	private $team;

	public function __construct()
	{	parent::__construct();
		$this->canonical_link = SITE_SUB . '/crstarts_register.php';

		if ($_GET['teamid'] || $_GET['teamslug'])
		{	$this->team = new Campaign($_GET['teamid'], $_GET['teamslug']);
			if (!$this->team->CanBeJoined())
			{	unset($_GET['teamid'], $this->team);
			}
		}

		if (isset($_POST['campname']))
		{	if ($this->CheckStageOne())
			{	$this->stage = 2;
				$campuser = new CampaignUser();
				$saved = $campuser->Create($_POST, $_FILES['campimage'], $this->team);
				if ($campuser->id)
				{	$_SESSION[SITE_NAME]['camp_customer'] = $campuser->id;
					$this->camp_customer = $campuser;
					$this->camp_customer->SendUserCreateEmail();
				}
				$this->successmessage = $saved['successmessage'];
				$this->failmessage = $saved['failmessage'];
			}
		} else
		{	if (isset($_POST['email']))
			{	if ($this->CheckStageOne())
				{	$this->stage = 2;
				}
			}
		}

		if ($this->camp_customer->id)
		{	header('location: ' . SITE_SUB . '/my-cr-stars/');
			exit;
		}

		$this->stages[1] = array('heading'=>'Start a Campaign');
		$this->stages[2] = array('heading'=>'Campaign Details');
		$this->stages[3] = array('heading'=>'Share Campaign');

	} // end of fn __construct

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		$this->fb_meta['title'] = 'Start a Campaign at Charity Right';
		$this->fb_meta['url'] = SITE_SUB . '/crstarts_register.php';
	} // end of fn SetFBMeta

	private function CheckStageOne()
	{	$fail = array();

		$campuser = new CampaignUser();
		if ($this->ValidEMail($_POST['email']))
		{	if ($campuser->EmailAlreadyUsed($_POST['email']))
			{	$fail[] = 'Email address is already registered';
			}
		} else
		{	$fail[] = 'Email missing or invalid';
		}

		if (!$_POST['firstname'])
		{	$fail[] = 'First name missing';
		}

		if (!$_POST['lastname'])
		{	$fail[] = 'Last name missing';
		}

		if (!$this->ValidPhoneNumber($_POST['phone']))
		{	$fail[] = 'Phone number missing or invalid';
		}

		if ($_POST['password'])
		{	if (!$this->AcceptablePW($_POST['password']))
			{	$fail[]= 'Invalid password, please retype';
			}
		} else
		{	$fail[]= 'Password missing';
		}
		
		if (is_array($_POST['contactpref']) && $_POST['contactpref'])
		{	$pref = new ContactPreferences();
			$_POST['contactpref'] = $pref->PreferencesValueFromArray($_POST['contactpref']);
		} else
		{	$_POST['contactpref'] = (int)$_POST['contactpref'];
		}

		if ($fail)
		{	$this->failmessage = implode('<br />', $fail);
		} else
		{	return true;
		}

	} // end of fn CheckStageOne

	function MainBodyContent()
	{	echo '<div class="container crstarsRegisterHeader"><div class="container_inner"><h1><span class="register_heading_white">Start</span><br />Campaigning<br />and Help<br /><span class="register_heading_yellow">End</span><br />Hunger</h1></div></div>';
		echo $this->RegisterBreadcrumbs();
		echo '<div class="container crstarsRegister"><div class="container_inner">';
		switch ($this->stage)
		{	case 2:
				echo '<h2>CREATE YOUR CAMPAIGN!</h2><h3>Fill out the details and lets get started!</h3>';
				echo $this->NewCampaignForm($_POST, $this->team);
				break;
			case 1:
				echo '<h2>Sign Up!</h2><h3>Create your CRStars Account';
				if ($this->team->id)
				{	echo ' to raise money for ', $this->team->FullTitle();
					if (!$_POST['campcurrency'])
					{	$_POST['campcurrency'] = $this->team->details['currency'];
					}
				}
				echo '.</h3>';
				//echo $this->NewCampaignForm($_POST, $this->team);
				echo $this->RegisterForm($_POST, $this->team);
				break;
		}
		echo '</div></div>';
	} // end of fn MemberBody

} // end of class CRStarsRegisterPage

$page = new CRStarsRegisterPage();
$page->Page();
?>