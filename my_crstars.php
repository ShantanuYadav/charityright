<?php 
require_once('init.php');

class CRStarsRegisterPage extends MyCRStarsPage
{
	public function __construct()
	{	parent::__construct('dashboard');
		$this->canonical_link = SITE_SUB . '/my-cr-stars/';
	} // end of fn __construct
	
	function MemberBodyContent()
	{	ob_start();
		echo '<div class="campuserMainLeft">';
		if ($campaigns = $this->camp_customer->GetCampaigns())
		{	echo '<div class="campuserMainHeader">My Campaigns</div>', $this->SideListCampaignsList($campaigns, true, true);
		}
		echo '</div>
			<div class="campuserMainRight">
				<div class="campuserMainHeader">Followed Campaigns</div>';
		if ($followed = $this->camp_customer->GetFollowedCampaigns())
		{	echo $this->SideListCampaignsList($followed);
		} else
		{	echo '<div class="campuserMainContent campuserMainHolding">You are not currently following any campaigns</div>';
		}
		echo '</div>';
	//	$this->VarDump($this->camp_customer->details);
		echo '</div><div class="clear"></div>';
		return ob_get_clean();
	} // end of fn MemberBodyContent
	
} // end of class CRStarsRegisterPage

$page = new CRStarsRegisterPage();
$page->Page();
?>