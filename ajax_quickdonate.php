<?php 
require_once('init.php');

class QuickDonateAjax extends Base
{
	public function __construct()
	{	parent::__construct();
		
		$this->XSSSafeAllGet();
		$this->XSSSafeAllPost();

		switch($_GET['action'])
		{	case 'type_change':
				$output = array();
				// get currencies
				ob_start();
				$currencies = $this->GetQDCurrencies($_GET['type']);
				if (!$currencies[$_GET['currency']])
				{	unset($_GET['currencies']);
				}
				foreach ($currencies as $curcode=>$currency)
				{	
					echo '<option value="', $curcode, '"', (($_GET['currency'] && ($_GET['currencies'] == $curcode)) || !$curcount++) ? ' selected="selected"' : '', '>', $curcode, '</option>';
				}
				$output['currencies'] = ob_get_clean();
				
				echo json_encode($output);
				break;
			case 'cur_change':
				$output = array();
				echo $this->GetCurrency($_GET['currency'], 'cursymbol');
				break;
			case 'build':
				$builder = new DonationLinkBuilder();
				echo $builder->BuildLink($_GET);
				break;
		}
		
	} // end of fn __construct

	protected function GetQDCurrencies($type = 'oneoff')
	{	$currencies = array();
		if ($type)
		{	$sql = 'SELECT * FROM currencies WHERE use_' . $this->SQLSafe($type) . ' ORDER BY curorder, curname';
			if ($result = $this->db->Query($sql))
			{	while($row = $this->db->FetchArray($result))
				{	$currencies[$row['curcode']] = $row;
				}
			}
		}
		return $currencies;
	} // end of fn GetQDCurrencies
	
} // end of class QuickDonateAjax

$ajax = new QuickDonateAjax();
?>