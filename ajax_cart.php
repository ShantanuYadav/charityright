<?php
require_once('init.php');

class CartAjax extends Base
{

	public function __construct()
	{	parent::__construct();
		
		$this->XSSSafeAllGet();
		$this->XSSSafeAllPost();
		
		switch ($_GET['action'])
		{	case 'add_donation':
				$output = array('test'=>'');
				
				$cart = new Cart();
				$added = $cart->AddMainDonationToCart($_POST);
				if ($added['success'])
				{	ob_start();
					$cartpage = new PageContent('cart');
					echo '<h3>Your donation has been added to your basket!</h3><div class="cartPopupButtons"><a class="jqmClose">Make another donation</a><a href="', $cartpage->Link(), '">View your basket</a><div class="clear"></div></div>';
					$output['success'] = ob_get_clean();
					$basket = $cart->PopUpBasket();
					$output['basket_desktop'] = $basket['desktop'];
					$output['basket_mobile'] = $basket['mobile'];
				} else
				{	if ($added['fail'])
					{	$output['error'] = implode('<br />', $added['fail']);
					}
				}
				
				echo json_encode($output);
				break;
			case 'add_donation_new':
				$output = array('test'=>'');
				
				$cart = new Cart();
				$added = $cart->AddMainDonationToCartNew($_POST);
				if ($added['success'])
				{	ob_start();
					
					$output['success'] = ob_get_clean();
					
				} else
				{	if ($added['fail'])
					{	$output['error'] = implode('<br />', $added['fail']);
					}
				}
				
				echo json_encode($output);
				break;
			case 'add_donation_new_step2':
				$output = array('test'=>'');
				
				$cart = new Cart();
				$added = $cart->AddMainDonationToCartNewStep2($_POST);
				if ($added['success'])
				{	ob_start();
					$output['success'] = ob_get_clean();
					
				} else
				{	if ($added['fail'])
					{	$output['error'] = implode('<br />', $added['fail']);
					}
				}
				
				echo json_encode($output);
				break;
			case 'add_donation_new_step3':
				$output = array('test'=>'');

				$cart = new Cart();
				
				if(!empty($_POST['crdonations_form'])){
					@$_SESSION[$this->cart_session_name]['crsdonations'][0]['donor_anon']    = $_POST['donor_anon'];
					@$_SESSION[$this->cart_session_name]['crsdonations'][0]['donor_comment'] = $_POST['donor_comment'];
				}
				
				$added = $cart->AddMainDonationToCartNewStep3($_POST);

				if ($added['success'])
				{	ob_start();
					$cart_data = $cart->GetCart();
					$cartorder = new CartOrder();
					$saved = $cartorder->CreateFromCartNew($cart_data);

					if ($saved['failmessage'])
					{	$output['error'] = implode('<br />', explode(',', $saved['failmessage']));
					} else
					{	if ($cartorder->id)
						{
							$cartorder_details = $cart->CartOrder();
							$output['success'] = 'success';
							$output['alldetail'] = $cartorder_details;
							$output['cart_session_name'] = $this->cart_session_name;
							ob_get_clean();
						}
					}
					
				} else
				{	if ($added['fail'])
					{	$output['error'] = implode('<br />', $added['fail']);
					}
				}
				
				echo json_encode($output);
				break;
			case 'makemonthpayment':
				$output = array('test'=>'');
				
				ob_start();
				$cart = new Cart();
				$cartorder = $cart->CartOrder();
		 
		 		$donate_thank_you_page = '/donate-thank-you/';

				// print_r($_SESSION[$this->cart_session_name]['dd_checked']);die;
		 
				//if (!$cartorder->details['ddsolesig'])
		 		if(empty($_SESSION[$this->cart_session_name]['dd_checked']))
				{	$cart->RemoveMonthlyDonations();
					$output_msg = '<h3>Based on the answers you have provided, we are unable to set your Direct Debit up online. We will email you a form which you can print out to complete and sign. Please return this form to us as soon as possible.</h3>';
				} else
				{	if ($this->page->failmessage)
					{	$output_msg = '<h3>It has not been possible to set up your monthly direct debit.</h3>';
						
					} else
					{	
						$output_msg = '<div class="gatewayRedirect">please wait ... creating your direct debit</div>';
					
					
						$fail = array();
						$success = array();
						$redirect_link = '';
							
						//print_r($cartorder);
						if ($cartorder->CanPayDirectDebitDonations()) {   
							if (!empty($_SESSION[$this->cart_session_name]['dd_checked'])) {
								$ez = new EazyCollectAPI(); // default = true = LiveMode

								$existing_customer_id = $ez->getCustomer($cartorder->details['donoremail']);
								if(empty($existing_customer_id)){
									$dd_customercreate = $ez->AddCustomerFromOrder($cartorder, $cart->GetCart());
									$dd_customer_ID = $dd_customercreate['return']['CustomerID'];
								}else{
									$dd_customer_ID = $existing_customer_id;
								}

								if ($dd_customer_ID) {
									$cart->SetProperty('ezc_CustomerID', $dd_customer_ID);
									$dd_contractcreate = $ez->AddContractFromOrder($cartorder, $dd_customer_ID);
									if ($dd_contractcreate['return']['ContractID']) {
										$cart->SetProperty('ezc_ContractID', $dd_contractcreate['return']['ContractID']);
										$cart->SetProperty('ezc_DirectDebitRef', $dd_contractcreate['return']['DirectDebitRef']);
										$cart->SetProperty('ezc_StartDate', $dd_contractcreate['return']['StartDate']);
										$donation_confirm = $cartorder->ConfirmDirectDebit($cart->GetCart());
										// print_r($donation_confirm);
										if ($donation_confirm['successmessage']) {
											$success[] = 'Your Direct Debit has been created';
											// $redirect_link = $this->pagecontent->Link() . '5/';
											$redirect_link = $donate_thank_you_page;
										} else {
											$fail[] = $donation_confirm['failmessage'];
										}
									} else {
										$fail[] = $dd_contractcreate['failmessage'];
									}
								} else {
									$fail[] = $dd_customercreate['failmessage'];
								}
							} else {
								$saved = $cartorder->RecordUnpaidDirectDebit($cart->GetCart());
								$success[] = 'Your Direct Debit form has been sent';
							}
						} //else $fail[] = 'cannot create dd donations';
					
					}
				}
				//unset($_SESSION[$this->cart_session_name]);
				 if(!empty($success)){
					 $output['success'] = 1;
					 $output['message'] = '<h3>'.$success[0].'</h3>';
					 $output['output_msg'] = $output_msg;

					 $thankyou_token['amount'] = @$cartorder->details['total_monthly'];
			  		 $thankyou_token['currency'] = @$cartorder->details['currency'];
			  		 $output['thankyou_token']  = base64_encode(json_encode($thankyou_token));
					 
					 $output['redirect_link'] = $redirect_link;
				 }
				 if(!empty($fail)){
					 $output['success'] = 0;
					 $output['output_msg'] = $output_msg;
					 $output['message'] = '<h3>'.$fail[0].'</h3>';
				 }

				ob_get_clean();
				echo json_encode($output);
				break;
			case 'add_crdonation':
				$output = array('test'=>'');
				
				$cart = new Cart();
				$added = $cart->AddCRDonationToCart($_POST);
				if ($added['success'])
				{	ob_start();
					//$this->VarDump($_POST);
					$cartpage = new PageContent('cart');
					echo '<h3>Your donation has been added to your basket!</h3><div class="cartPopupButtons"><a class="jqmClose">Make another donation</a><a href="', $cartpage->Link(), '">View your basket</a><div class="clear"></div></div>';
					$output['success'] = ob_get_clean();
					$basket = $cart->PopUpBasket();
					$output['basket_desktop'] = $basket['desktop'];
					$output['basket_mobile'] = $basket['mobile'];
				} else
				{	if ($added['fail'])
					{	$output['error'] = implode('<br />', $added['fail']);
					}
				}
				
				echo json_encode($output);
				break;
		}

	} //  end of fn __construct

} // end of class CartAjax

$page = new CartAjax();
?>