<?php
require_once('init.php');

class CRStarsSearchPage extends CRStarsPage
{
	public function __construct()
	{	parent::__construct('cr-stars');
	} // end of fn __construct

	protected function BaseConstructFunctions()
	{	$this->canonical_link = SITE_SUB . '/cr-stars/search/';
		$this->XSSSafeAllGet();
		if ($_GET['crs_search'])
		{	$this->canonical_link .= '?crs_search=' . $_GET['crs_search'];
		}
	} // end of fn BaseConstructFunctions

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		$this->fb_meta['url'] = $this->canonical_link;
	} // end of fn SetFBMeta

	public function MainBodyContent()
	{	echo $this->CampaignListPageHeader(), '<div class="container campaignsPage"><div class="container_inner">';
		if ($campaigns = $this->GetCampaigns())
		{	$perpage = 5;
			if ($_GET['page'] > 1)
			{	$start = ($_GET['page'] - 1) * $perpage;
			} else
			{	$start = 0;
			}
			$end = $start + $perpage;

			echo $this->MainCampaignsList($campaigns, $start, $end);
			if (count($campaigns) > $perpage)
			{	$pagelink = $_SERVER['REQUEST_URI'];
				if ($qpos = strpos($pagelink, '?'))
				{	$pagelink = substr($pagelink, 0, $qpos);
				}
			//	echo $pagelink;
				if ($_GET)
				{	$get = array();
					foreach ($_GET as $key=>$value)
					{	if ($value && ($key != 'page'))
						{	$get[] = $key . '=' . $value;
						}
					}
					if ($get)
					{	$pagelink .= '?' . implode('&', $get);
					}
				}
				//echo $pagelink;
				if (($count_campaigns = count($campaigns)) > ($perpage * 12))
				{	$count_campaigns = $perpage * 12;
				}
				$pag = new Pagination($_GET['page'], $count_campaigns, $perpage, $pagelink, array(), 'page');
				echo '<div class="pagination">', $pag->Display(''), '</div>';
			}

		} else
		{	echo '<h3 class="alert-box alert-box-orange no-campaigns">No campaigns found. Please try again or create a new campaign. <a href="'.SITE_URL.'cr-stars/">Find out more</a>.</h3>';
		}
		echo '</div></div>';
	} // end of fn MainBodyContent
	
	protected function HeaderLinksSearch(){}
	protected function MobileExtraLinksTop(){}
	
	protected function GetCampaigns()
	{	$tables = array('campaigns'=>'campaigns');
		$fields = array('campaigns.*');
		$where = array('visible'=>'campaigns.visible=1');

		if ($needle = $this->SQLSafe($_GET['crs_search']))
		{	$where['crs_search'] = 'MATCH(campaigns.campname, campaigns.campusername, campaigns.description) AGAINST("' . $needle . '" IN BOOLEAN MODE) > 0';
		}

		$campaigns = array();
		if ($result = $this->db->Query($sql = $this->db->BuildSQL($tables, $fields, $where)))
		{	while ($row = $this->db->FetchArray($result))
			{	$row['last_donation'] = '';
				$donsql = 'SELECT MAX(campaigndonations.donated) AS last_donation FROM campaigndonations WHERE campaignid=' . $row['cid'];
				if ($donresult = $this->db->Query($donsql))
				{	if ($donrow = $this->db->FetchArray($donresult))
					{	$row['last_donation'] = $donrow['last_donation'];
					}
				}
				$campaigns[$row['cid']] = $row;
			}
		} //else echo $this->db->Error();
		//if (SITE_TEST) echo $sql;
		uasort($campaigns, array($this, 'UASortPriorityList'));
		return $campaigns;
	} // end of fn GetCampaigns

} // end of class CRStarsSearchPage

$page = new CRStarsSearchPage();
$page->Page();
?>