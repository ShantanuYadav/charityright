
function AttListPopUp(prodid, lang)
{	AttListPopUpRefresh(prodid, lang);
	$("#att_modal_popup").jqmShow();
} // end of fn AttListPopUp

function AttListPopUpRefresh(prodid, lang)
{	url = 'ajax_product.php?id=' + String(prodid) + '&action=popup_att&lang=' + lang;
	document.getElementById('attModalInner').innerHTML = AP_Ajax(url);
} // end of fn AttListPopUpRefresh

function AttValueRemove(prodid, avid, lang)
{	url = 'ajax_product.php?id=' + String(prodid) + '&avid=' + String(avid) + '&action=remove_att&lang=' + lang;
	AP_Ajax(url);
	AttListRefresh(prodid, lang);
} // end of fn AttValueRemove

function AttValueAdd(prodid, avid, lang)
{	url = 'ajax_product.php?id=' + String(prodid) + '&avid=' + String(avid) + '&action=add_att&lang=' + lang;
	AP_Ajax(url);
	document.getElementById('attModalInner').innerHTML = AP_Ajax(url);
	AttListRefresh(prodid, lang);
} // end of fn AttValueAdd

function AttListRefresh(prodid, lang)
{	url = 'ajax_product.php?id=' + String(prodid) + '&action=refresh_att&lang=' + lang;
	document.getElementById('attlistContainer').innerHTML = AP_Ajax(url);
} // end of fn AttListRefresh
