function DonateTypeChangeNew()
{
	var url = 'ajax_donations.php?action=typechangenew&type=';
	var donType = GetDonateTypeNew();
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('select#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donQuantity = $('input#donation-quantity').val();

	if (donType != undefined)
	{	url += donType;
	}
	url += '&currency='
	if (currency != undefined)
	{	url += currency;
	}
	url += '&country='
	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	//var project = $('select[name="donation_project"]').val();
	if (project != undefined)
	{	url += project;
	}
	url += '&project2=';
	if (project2 != undefined)
	{	url += encodeURIComponent(project2);
	}
	url += '&type=';
	if (donType != undefined)
	{	url += encodeURIComponent(donType);
	}
	url += '&currency=';
	if (currency != undefined)
	{	url += encodeURIComponent(currency);
	}
	url += '&amount=';
	if (donAmount != undefined)
	{	url += encodeURIComponent(donAmount);
	}
	url += '&quantity=';
	if (donQuantity != undefined)
	{	url += encodeURIComponent(donQuantity);
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);

	//var response = jQuery.parseJSON(AP_Ajax(url));
	$('.donation-currency-list').html(response.currencies);

	if (response.countrylist == undefined)
	{	$('.donation-country-list').html('');
	} else
	{	$('.donation-country-list').html(response.countrylist);
	}
	if (response.projectlist == undefined)
	{	$('.donation-project-list').html('');
	} else
	{	$('.donation-project-list').html(response.projectlist);
	}
	if (response.projectlist2 == undefined)
	{	$('.donation-project2-list').html('');
	} else
	{	$('.donation-project2-list').html(response.projectlist2);
	}
	if (response.donationamount != undefined)
	{	$('.donation_amount_container').html(response.donationamount);
	}

	if (donType == 'monthly')
	{	$('.donation-admin').addClass('hidden');
	} else
	{	$('.donation-admin').removeClass('hidden');
	}
	DonationCurrencyChangeNew();
} // end of fn DonateTypeChangeNew

function GetDonateTypeNew()
{	return $('select[name=donation_type]').val();
} // end of fn GetDonateTypeNew

function DonationCurrencyChangeNew()
{
	var currency = $('#donation_currency').val();
	if (currency != undefined)
	{	
		var country = $('select#donation-country').val();
		var project = $('select#donation-project').val();
		var project2 = $('select#donation-project2').val();
		var donType = $('#donation-type').val();
		var donAmount = $('input#donation-amount').val();
		var donQuantity = $('input#donation-quantity').val();
		var url = 'ajax_donations.php?action=currencychangenew&newcurrency=' + encodeURIComponent(currency);

		url += '&country=';
		if (country != undefined)
		{	url += encodeURIComponent(country);
		}
		url += '&project=';
		if (project != undefined)
		{	url += encodeURIComponent(project);
		}
		url += '&project2=';
		if (project2 != undefined)
		{	url += encodeURIComponent(project2);
		}
		url += '&type=';
		if (donType != undefined)
		{	url += encodeURIComponent(donType);
		}
		url += '&quantity=';
		if (donQuantity != undefined)
		{	url += encodeURIComponent(donQuantity);
		}

//alert(url);
		var rawresponse = AP_Ajax(url);
//alert(rawresponse);
		var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
		$('.donation-currency-symbol').html(response.cursymbol);
		if (response.donationamount != undefined)
		{	$('.donation_amount_container').html(response.donationamount);
		}
		if (response.donationtype != undefined)
		{	$('.donation_type_container').html(response.donationtype);
		}
		$('input[name="donation_oldcurrency"]').val(currency);
	}
} // end of fn DonationCurrencyChangeNew

function DonatePresetClickNew(amount, preset_count) {
	$('input[name="donation_amount"]').val(parseFloat(amount).toFixed(2));
	$('.donation-preset').removeClass('donation-presets-selected');
	$('.donation-preset-list .form-col:nth-child(' + String(preset_count) + ') .donation-preset').addClass('donation-presets-selected');
} // end of fn DonatePresetClickNew

function DonateAmountChangeNew()
{	$('.donation-preset').removeClass('donation-presets-selected');
} // end of fn DonateAmountChangeNew

function DonateQuantityChange()
{	var quantity = parseInt($('input[name="donation_quantity"]').val());
	if (quantity < 1)
	{	quantity = 1;
	}
	$('input[name="donation_quantity"]').val(quantity);
	var amount = Number($('input[name="donation_amountfixed"]').val());
	$('input[name="donation_amount"]').val(amount * quantity);
	$('.donation_amount_display').html(number_format(amount * quantity, 2, '.', ','));
	if (quantity > 1)
	{	$('.donation_quantity_changer_minus').removeClass('donation_quantity_changer_hidden');
	} else
	{	$('.donation_quantity_changer_minus').addClass('donation_quantity_changer_hidden');
	}
} // end of fn DonateQuantityChange

function DonateQuantityIncrement(inc_amount)
{	$('input[name="donation_quantity"]').val(parseInt($('input[name="donation_quantity"]').val()) + inc_amount);
	DonateQuantityChange();
} // end of fn DonateQuantityIncrement

function _DonorCountryChangeNew() {
	$('.donorAddressPCA input').val('');
	$('.donorAddressManual').addClass('hidden');
	if ($('select[name="donor_country"]').val() == 'GB')
	{	$('.donor-giftaid-box').removeClass('hidden');
		$('.donorAddressPCA').removeClass('hidden');
		$('.donorAddressManual input').val('');
	} else
	{	if ($('.donorAddressManual').hasClass('hidden'))
		{
		}
		$('.donor-giftaid-box').addClass('hidden');
		$('.donorAddressPCA').addClass('hidden');
		$('.donorAddressManual').removeClass('hidden');
	}
} // end of fn DonorCountryChangeNew

function DonorCountryChangeNew() {
	$('.donorAddressPCA input').val('');
	//$('.donorAddressManual').addClass('hidden');
	if ($('select[name="donor_country"]').val() == 'GB')
	{	$('.donor-giftaid-box').removeClass('hidden');
		//$('.donorAddressPCA').removeClass('hidden');
		//$('.donorAddressManual input').val('');
	} else
	{	if ($('.donorAddressManual').hasClass('hidden'))
		{
		}
		$('.donor-giftaid-box').addClass('hidden');
		//$('.donorAddressPCA').addClass('hidden');
		$('.donorAddressManual').removeClass('hidden');
	}
} // end of fn DonorCountryChangeNew
function CartPersonalStageHack(){
	var $form = $('#donatePersonalStageForm');
	$form.submit(function() {
		if ($('select[name="donor_country"]').val() == 'GB' && $('.donorAddressPCA').hasClass('hidden')){
			$('input[name="pca_donor_address1"]').val($('input[name="donor_address1"]').val());
			$('input[name="pca_donor_address2"]').val($('input[name="donor_address2"]').val());
			$('input[name="pca_donor_city"]').val($('input[name="donor_city"]').val());
			$('input[name="pca_donor_postcode"]').val($('input[name="donor_postcode"]').val());
		}
	});
}
function DonorCountryChangeCampaign() {

	if ($('select[name="donor_country"]').val() == 'GB') {
		if ($('.donor-giftaid-box').hasClass('hidden')) {
			$('.donor-giftaid-box').removeClass('hidden');
		}
	} else {
		if (!$('.donor-giftaid-box').hasClass('hidden')) {
			$('.donor-giftaid-box').addClass('hidden');
		}
	}
} // end of fn DonorCountryChange

function DonateCountryChangeNew()
{
	var url = 'ajax_donations.php?action=projectlistnew&country=';
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('select#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donQuantity = $('input#donation-quantity').val();
	if (country != undefined)
	{	url += encodeURIComponent(country);
	}
	url += '&project=';
	if (project != undefined)
	{	url += encodeURIComponent(project);
	}
	url += '&project2=';
	if (project2 != undefined)
	{	url += encodeURIComponent(project2);
	}
	url += '&type=';
	if (donType != undefined)
	{	url += encodeURIComponent(donType);
	}
	url += '&currency=';
	if (currency != undefined)
	{	url += encodeURIComponent(currency);
	}
	url += '&amount=';
	if (donAmount != undefined)
	{	url += encodeURIComponent(donAmount);
	}
	url += '&quantity=';
	if (donQuantity != undefined)
	{	url += encodeURIComponent(donQuantity);
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.projectlist == undefined)
	{	$('.donation-project-list').html('');
	} else
	{	$('.donation-project-list').html(response.projectlist);
	}
	if (response.projectlist2 == undefined)
	{	$('.donation-project2-list').html('');
	} else
	{	$('.donation-project2-list').html(response.projectlist2);
	}
	if (response.donationamount != undefined)
	{	$('.donation_amount_container').html(response.donationamount);
	}
} // end of fn DonateCountryChangeNew

function DonateProjectChangeNew()
{
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('select#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donQuantity = $('input#donation-quantity').val();

	var url = 'ajax_donations.php?action=projectlistnew2&country=';

	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	if (project != undefined)
	{	url += project;
	}
	url += '&project2='
	if (project2 != undefined)
	{	url += project2;
	}
	url += '&type='
	if (donType != undefined)
	{	url += donType;
	}
	url += '&currency=';
	if (currency != undefined)
	{	url += encodeURIComponent(currency);
	}
	url += '&amount=';
	if (donAmount != undefined)
	{	url += encodeURIComponent(donAmount);
	}
	url += '&quantity=';
	if (donQuantity != undefined)
	{	url += encodeURIComponent(donQuantity);
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.projectlist2 == undefined)
	{	$('.donation-project2-list').html('');
	} else
	{	$('.donation-project2-list').html(response.projectlist2);
	}
	if (response.donationamount != undefined)
	{	$('.donation_amount_container').html(response.donationamount);
	}
} // end of fn DonateProjectChangeNew

function DonateProject2Change()
{
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('select#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donQuantity = $('input#donation-quantity').val();

	var url = 'ajax_donations.php?action=projectlist2change&country=';

	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	if (project != undefined)
	{	url += project;
	}
	url += '&project2='
	if (project2 != undefined)
	{	url += project2;
	}
	url += '&type='
	if (donType != undefined)
	{	url += donType;
	}
	url += '&currency=';
	if (currency != undefined)
	{	url += encodeURIComponent(currency);
	}
	url += '&amount=';
	if (donAmount != undefined)
	{	url += encodeURIComponent(donAmount);
	}
	url += '&quantity=';
	if (donQuantity != undefined)
	{	url += encodeURIComponent(donQuantity);
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.donationamount != undefined)
	{	$('.donation_amount_container').html(response.donationamount);
	}
} // end of fn DonateProject2Change

function DonorGiftaidCheckerNew(yes_ticked) {
	$('input[name="donor_giftaidchecked"]').val('1');
	if (yes_ticked) {
		$('input[name="donor_giftaid"]').attr('checked', 'checked');
	}
	$('form#donatePersonalStageForm').submit();
}

function DonorAddressBox(){
	var $pcaAddressContainer = $('.pcaAddressContainer');
	var $donor_address_box = $('.donor-address-box');
	var $donor_address_box_content = $('.donor-address-box-content');
	$donor_address_box_content.bind("DOMSubtreeModified",function(){
		if($donor_address_box_content.html().length > 0){
			$donor_address_box.removeClass('hidden');
			$pcaAddressContainer.addClass('hidden');
		}else{
			$pcaAddressContainer.removeClass('hidden');
			$donor_address_box.addClass('hidden');
		}
	});
	$('.address-search-again a').click(function(){
		$donor_address_box_content.html("");
		$("#pca_donor_address1").val("");
		$("#pca_donor_address2").val("");
		$("#pca_donor_address3").val("");
		$("#pca_donor_city").val("");
		$("#pca_donor_postcode").val("");
		$("#pca_donor_country").val("");
		$donor_address_box.addClass('hidden');
		$pcaAddressContainer.removeClass('hidden');
		return false;
	})
}
function DonationCombineValues(){
	var f1 = $('#dd_sort_1').val(),
		f2 = $('#dd_sort_2').val(),
		f3 = $('#dd_sort_3').val(),
		$field = $('#dd-accountsortcode');

		var field_value = f1+f2+f3;
		console.log(field_value);
		if(field_value.length == 6){
			$field.val(field_value);
		}
}
$(document).ready(function() {
	DonorAddressBox();
	CartPersonalStageHack();
});



function DonateTypeChange()
{	var url = 'ajax_donations.php?action=typechange&type=';
	var donType = GetDonateType();
	if (donType != undefined)
	{	url += donType;
	}
	url += '&currency='
	var currency = $('#donation_currency').val();
	if (currency != undefined)
	{	url += currency;
	}
	url += '&country='
	var country = $('select[name="donation_country"]').val();
	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	var project = $('select[name="donation_project"]').val();
	if (project != undefined)
	{	url += project;
	}
	url += '&amount=';

	var response = jQuery.parseJSON(AP_Ajax(url));
	$('#donationCurrenciesContainer').html(response.currencies);
	$('#donationCountriesContainer').html(response.countrylist);
	$('#donationProjectContainer').html(response.projectlist);
	if (donType == 'monthly')
	{	$('.donation_admin').addClass('hidden');
	} else
	{	$('.donation_admin').removeClass('hidden');
	}
	$('.donation_types_chooser li').removeClass('selected');
	$('.donation_types_chooser li#dtype_li_' + donType).addClass('selected');
	DonationCurrencyChange();
} // end of fn DonateTypeChange

function GetDonateType()
{	return $('.donation_types_chooser li input[type="radio"]:checked').val();
} // end of fn GetDonateType

function DonationCurrencyChange()
{	var currency = $('#donation_currency').val();
	if (currency != undefined)
	{	var url = 'ajax_donations.php?action=currencychange&newcurrency=' + currency + '&amount=' + String($('input[name="donation_amount"]').val()) + '&adminamount=' + String($('input[name="donation_admin"]').val()) + '&oldcurrency=' + String($('input[name="donation_oldcurrency"]').val()) + '&type=' + GetDonateType();
		var response = jQuery.parseJSON(AP_Ajax(url));
		//console.log(response);
		$('.donationCurrencySymbol').html(response.cursymbol);
		$('input[name="donation_amount"]').val(response.amount);
		$('input[name="donation_admin"]').val(response.adminamount);
		$('input[name="donation_oldcurrency"]').val(currency);
		$('.presets_list').html(response.presets);
	}
} // end of fn DonationCurrencyChange

function DonatePresetClick(amount, preset_count)
{	$('input[name="donation_amount"]').val(parseFloat(amount).toFixed(2));
	$('.presets_list ul li').removeClass('presets_selected');
	$('.presets_list ul li:nth-child(' + String(preset_count) + ')').addClass('presets_selected');
} // end of fn DonatePresetClick

function DonateAmountChange()
{	$('.presets_list ul li').removeClass('presets_selected');
} // end of fn DonateAmountChange

function DonorCountryChange()
{	$('.donorAddressPCA input').val('');
	$('.donorAddressManual').addClass('hidden');
	if ($('select[name="donor_country"]').val() == 'GB')
	{	$('.donor_giftaid').removeClass('hidden');
		$('.donorAddressPCA').removeClass('hidden');
		$('.donorAddressManual input').val('');
	} else
	{	if ($('.donorAddressManual').hasClass('hidden'))
		{
		}
		$('.donor_giftaid').addClass('hidden');
		$('.donorAddressPCA').addClass('hidden');
		$('.donorAddressManual').removeClass('hidden');
	}
} // end of fn DonorCountryChange

function DonationFomReset()
{	var url = 'ajax_donations.php?action=donationformreset&currency=';
	var currency = $('#donation_currency').val();
	if (currency != undefined)
	{	url += currency;
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.donationtype != undefined)
	{	$('.donation_type_container').html(response.donationtype);
		if (response.countrylist == undefined)
		{	$('.donation-country-list').html('');
		} else
		{	$('.donation-country-list').html(response.countrylist);
		}
		if (response.projectlist == undefined)
		{	$('.donation-project-list').html('');
		} else
		{	$('.donation-project-list').html(response.projectlist);
		}
		if (response.projectlist2 == undefined)
		{	$('.donation-project2-list').html('');
		} else
		{	$('.donation-project2-list').html(response.projectlist2);
		}
		if (response.donationamount != undefined)
		{	$('.donation_amount_container').html(response.donationamount);
		}
		if (response.currencies != undefined)
		{	//$('.donation-currency-list').html(response.currencies);
			$('#donationCurrencyChooserContainer').html(response.currencies);
		}
		$('input[name="donation_admin"]').val('0');
		if (response.donationtype == 'monthly')
		{	$('.donation-admin').addClass('hidden');
		} else
		{	$('.donation-admin').removeClass('hidden');
		}
	}
} // end of fn DonationFomReset

function DonateCountryChange()
{	var url = 'ajax_donations.php?action=projectlist&country=';
	var country = $('select[name="donation_country"]').val();
	var project = $('select[name="donation_project"]').val();
	var donType = $('.donation_types_chooser li input[type="radio"]:checked').val();
	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	if (project != undefined)
	{	url += project;
	}
	url += '&type='
	if (donType != undefined)
	{	url += donType;
	}
	$('#donationProjectContainer').html(AP_Ajax(url));
} // end of fn DonateCountryChange

function DonorGiftaidChecker(yes_ticked)
{	$('input[name="donor_giftaidchecked"]').val('1');
	if (yes_ticked)
	{	$('input[name="donor_giftaid"]').attr('checked', 'checked');
	}
	//alert($('input[name="donor_giftaidchecked"]').val());
	$('form#donatePersonalStageForm').submit();
} // end of fn DonorGiftaidChecker

function DonationAddToCart()
{	//alert('add to cart');
	var url = 'ajax_cart.php?action=add_donation';
	var params = new Array();
	
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donAdminAmount = $('input#donation-admin').val();
	var donQuantity = $('input#donation-quantity').val();
	var zakat = $('select#donation_zakat').val();

	if (country != undefined)
	{	params[params.length] = 'donation_country=' + encodeURIComponent(country);
	}
	if (project != undefined)
	{	params[params.length] = 'donation_project=' + encodeURIComponent(project);
	}
	if (project2 != undefined)
	{	params[params.length] = 'donation_project2=' + encodeURIComponent(project2);
	}
	if (donType != undefined)
	{	params[params.length] = 'donation_type=' + encodeURIComponent(donType);
	}
	if (currency != undefined)
	{	params[params.length] = 'donation_currency=' + encodeURIComponent(currency);
	}
	if (donAmount != undefined)
	{	params[params.length] = 'donation_amount=' + encodeURIComponent(donAmount);
	}
	if (donAdminAmount != undefined)
	{	params[params.length] = 'donation_admin=' + encodeURIComponent(donAdminAmount);
	}
	if (donQuantity != undefined)
	{	params[params.length] = 'donation_quantity=' + encodeURIComponent(donQuantity);
	}
	if (zakat != undefined)
	{	params[params.length] = 'donation_zakat=' + encodeURIComponent(zakat);
	}
	
	var action = function(rawresponse)
	{	//alert(rawresponse);
		response = jQuery.parseJSON(rawresponse);
		if ((response.test != undefined) && (response.test.length > 0))
		{	alert(response.test);
		}
		if (response.success != undefined)
		{	$(".cart_modal_popup_inner").html(response.success);
			$(".cartModalPopup").jqmShow();
		} else
		{	if ((response.error != undefined) && (response.error.length > 0))
			{	$(".cart_modal_popup_inner").html(response.error);
				$(".cartModalPopup").jqmShow();
			}
		}
		if (response.basket_desktop != undefined)
		{	$('.hrLinksCart').html(response.basket_desktop);
		}
		if (response.basket_mobile != undefined)
		{	$('.header_cartopener').html(response.basket_mobile);
		}
		DonationFomReset();
	}
	PM_Ajax_Post(url, params.join('&'), action);
} // end of fn DonationAddToCart

function CRDonationAddToCart(campaignid)
{	//alert('add to cart');
	var url = 'ajax_cart.php?action=add_crdonation&cid=' + String(campaignid);
	var params = new Array();
	
	var country = $('select#donation-country').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input[name="donation_amount"]').val();
	var zakat = $('select#donation_zakat').val();
	var donor_anon = $('select[name="donor_anon"]').val();
	var donor_showto = $('select[name="donor_showto"]').val();
	var donor_comment = $('textarea[name="donor_comment"]').val();

	if (country != undefined)
	{	params[params.length] = 'donation_country=' + encodeURIComponent(country);
	}
	if (currency != undefined)
	{	params[params.length] = 'donation_currency=' + encodeURIComponent(currency);
	}
	if (donAmount != undefined)
	{	params[params.length] = 'donation_amount=' + encodeURIComponent(donAmount);
	}
	if (zakat != undefined)
	{	params[params.length] = 'donation_zakat=' + encodeURIComponent(zakat);
	}
	if (donor_anon != undefined)
	{	params[params.length] = 'donor_anon=' + encodeURIComponent(donor_anon);
	}
	if (donor_showto != undefined)
	{	params[params.length] = 'donor_showto=' + encodeURIComponent(donor_showto);
	}
	if (donor_comment != undefined)
	{	params[params.length] = 'donor_comment=' + encodeURIComponent(donor_comment);
	}
	
	var action = function(rawresponse)
	{	//alert(rawresponse);
		response = jQuery.parseJSON(rawresponse);
		if ((response.test != undefined) && (response.test.length > 0))
		{	alert(response.test);
		}
		if (response.success != undefined)
		{	$(".cart_modal_popup_inner").html(response.success);
			$(".cartModalPopup").jqmShow();
			$('input[name="donation_amount"]').val('0');
			$('input[name="donor_comment"]').val('');
			$('#donateCurrencyContainer').addClass('hidden');
		} else
		{	if ((response.error != undefined) && (response.error.length > 0))
			{	$(".cart_modal_popup_inner").html(response.error);
				$(".cartModalPopup").jqmShow();
			}
		}
		if (response.basket_desktop != undefined)
		{	$('.hrLinksCart').html(response.basket_desktop);
		}
		if (response.basket_mobile != undefined)
		{	$('.header_cartopener').html(response.basket_mobile);
		}
		DonationFomReset();
	}
	PM_Ajax_Post(url, params.join('&'), action);
} // end of fn CRDonationAddToCart