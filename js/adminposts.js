function PostAddCatPopup(postid)
{	PostAddCatPopupRefresh(postid);
	$('#pcats_modal_popup').jqmShow();
} // end of fn PostAddCatPopup

function PostAddCatPopupRefresh(postid)
{	var url = 'ajax_posts.php?action=catspopup&id=' + String(postid);
	document.getElementById('pcatsModalInner').innerHTML = AP_Ajax(url);
} // end of fn PostAddCatPopupRefresh

function PostAddCatTableRefresh(postid)
{	var url = 'ajax_posts.php?action=catstable&id=' + String(postid);
	document.getElementById('postCatsContainer').innerHTML = AP_Ajax(url);
} // end of fn PostAddCatTableRefresh

function PostAddCatAdd(postid, catid)
{	var url = 'ajax_posts.php?action=catadd&id=' + String(postid) + '&catid=' + String(catid);
	AP_Ajax(url);
	PostAddCatPopupRefresh(postid);
	PostAddCatTableRefresh(postid)
} // end of fn PostAddCatAdd

function PostAddCatRemove(postid, catid)
{	var url = 'ajax_posts.php?action=catremove&id=' + String(postid) + '&catid=' + String(catid);
	AP_Ajax(url);
	PostAddCatTableRefresh(postid)
} // end of fn PostAddCatRemove


function PostAddVideoPopup(postid)
{	PostAddVideoPopupRefresh(postid);
	$('#pvids_modal_popup').jqmShow();
} // end of fn PostAddVideoPopup

function PostAddVideoPopupRefresh(postid)
{	var url = 'ajax_posts.php?action=vidspopup&id=' + String(postid);
	document.getElementById('pvidsModalInner').innerHTML = AP_Ajax(url);
} // end of fn PostAddVideoPopupRefresh

function PostAddVideoTableRefresh(postid)
{	var url = 'ajax_posts.php?action=vidstable&id=' + String(postid);
	document.getElementById('postVideosContainer').innerHTML = AP_Ajax(url);
} // end of fn PostAddVideoTableRefresh

function PostAddVidsAdd(postid, vid)
{	var url = 'ajax_posts.php?action=vidadd&id=' + String(postid) + '&vid=' + String(vid);
	AP_Ajax(url);
	PostAddVideoPopupRefresh(postid);
	PostAddVideoTableRefresh(postid)
} // end of fn PostAddVidsAdd

function PostVideoRemove(postid, vid)
{	var url = 'ajax_posts.php?action=vidremove&id=' + String(postid) + '&vid=' + String(vid);
	AP_Ajax(url);
	PostAddVideoTableRefresh(postid)
} // end of fn PostVideoRemove


function PostAddPostPopup(postid)
{	PostAddPostPopupRefresh(postid);
	$('#pvids_modal_popup').jqmShow();
} // end of fn PostAddPostPopup

function PostAddPostPopupRefresh(postid)
{	var url = 'ajax_posts.php?action=postspopup&id=' + String(postid);
	document.getElementById('pvidsModalInner').innerHTML = AP_Ajax(url);
} // end of fn PostAddPostPopupRefresh

function PostAddPostsTableRefresh(postid)
{	var url = 'ajax_posts.php?action=poststable&id=' + String(postid);
	document.getElementById('postVideosContainer').innerHTML = AP_Ajax(url);
} // end of fn PostAddPostsTableRefresh

function PostAddPostsAdd(postid, addid)
{	var url = 'ajax_posts.php?action=postadd&id=' + String(postid) + '&postid=' + String(addid);
	AP_Ajax(url);
	PostAddPostPopupRefresh(postid);
	PostAddPostsTableRefresh(postid)
} // end of fn PostAddVidsAdd

function PostPostRemove(postid, remid)
{	var url = 'ajax_posts.php?action=postremove&id=' + String(postid) + '&postid=' + String(remid);
	AP_Ajax(url);
	PostAddPostsTableRefresh(postid)
} // end of fn PostPostRemove

