
function DisplayRegionSelector()
{	var countryselector;
	document.getElementById('regionSelector').innerHTML = '';
	if ((countryselector = document.getElementById('country')) && (countryselector.value.length > 0))
	{	
		var url = 'ajax_regionselector.php?action=regionselector&ctry=' + countryselector.value;
		document.getElementById('regionSelector').innerHTML = AP_Ajax(url);
		DisplayCountySelector();
	}
} // end of fn DisplayRegionSelector

function DisplayCountySelector()
{	var countryselector;
	document.getElementById('countySelector').innerHTML = '';
	if ((countryselector = document.getElementById('country')) && (countryselector.value.length > 0))
	{	
		var url = 'ajax_regionselector.php?action=countyselector&ctry=' + countryselector.value;
		if ((countySelector = document.getElementById('region')) && (Number(countySelector.value) > 0))
		{	url += '&region=' + String(countySelector.value);
		}
		document.getElementById('countySelector').innerHTML = AP_Ajax(url);
	}
} // end of fn DisplayCountySelector

/* payment help functions */
function CtryPaymentHelpTableRefresh(ctry)
{	var container, url;
	if (container = document.getElementById('ctryPaymentHelpContainer'))
	{	url = 'ajax_ctrypmthelp.php?action=table&ctry=' + ctry;
		container.innerHTML = AP_Ajax(url);
	}
} // end of fn CtryPaymentHelpTableRefresh

function CtryPaymentHelpPopUpRefresh(ctry)
{	var container, url;
	if (container = document.getElementById('ctryhelpModalInner'))
	{	url = 'ajax_ctrypmthelp.php?action=popup&ctry=' + ctry;
		container.innerHTML = AP_Ajax(url);
	}
} // end of fn CtryPaymentHelpPopUpRefresh

function CtryPaymentHelpPopUp(ctry)
{	CtryPaymentHelpPopUpRefresh(ctry);
	$('#ctrypmthelp_modal_popup').jqmShow();
} // end of fn CtryPaymentHelpPopUp

function CtryPaymentHelpRemove(ctry, phid)
{	var url;
	url = 'ajax_ctrypmthelp.php?action=remove&ctry=' + ctry + '&phid=' + String(phid);
	AP_Ajax(url);
	CtryPaymentHelpTableRefresh(ctry);
} // end of fn CtryPaymentHelpRemove

function CtryPaymentHelpAdd(ctry, phid)
{	var url;
	url = 'ajax_ctrypmthelp.php?action=add&ctry=' + ctry + '&phid=' + String(phid);
	AP_Ajax(url);
	CtryPaymentHelpPopUpRefresh(ctry);
	CtryPaymentHelpTableRefresh(ctry);
} // end of fn CtryPaymentHelpAdd

/* payment gateway functions */
function CtryGatewayTableRefresh(ctry)
{	var container, url;
	if (container = document.getElementById('ctryPaymentGatewayContainer'))
	{	url = 'ajax_ctrygateways.php?action=table&ctry=' + ctry;
		container.innerHTML = AP_Ajax(url);
	}
} // end of fn CtryGatewayTableRefresh

function CtryGatewayPopUpRefresh(ctry)
{	var container, url;
	if (container = document.getElementById('ctrygwModalInner'))
	{	url = 'ajax_ctrygateways.php?action=popup&ctry=' + ctry;
		container.innerHTML = AP_Ajax(url);
	}
} // end of fn CtryGatewayPopUpRefresh

function CtryGatewayPopUp(ctry)
{	CtryGatewayPopUpRefresh(ctry);
	$('#ctrygateway_modal_popup').jqmShow();
} // end of fn CtryGatewayPopUp

function CtryGatewayRemove(ctry, pgid)
{	var url;
	url = 'ajax_ctrygateways.php?action=remove&ctry=' + ctry + '&pgid=' + String(pgid);
	AP_Ajax(url);
	CtryGatewayTableRefresh(ctry);
} // end of fn CtryGatewayRemove

function CtryGatewayAdd(ctry, pgid)
{	var url;
	url = 'ajax_ctrygateways.php?action=add&ctry=' + ctry + '&pgid=' + String(pgid);
	AP_Ajax(url);
	CtryGatewayPopUpRefresh(ctry);
	CtryGatewayTableRefresh(ctry);
} // end of fn CtryGatewayAdd