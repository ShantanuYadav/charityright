function DLBBuildLink()
{	var url = 'ajax_donationlinkbuilder.php?action=build';
	var dcid, dpid, amount, dtype, currency;
	if (dcid = $('#dlb_country option:selected').val())
	{	url += '&dcid=' + String(dcid);
	}
	if (dpid = $('#dlb_project option:selected').val())
	{	url += '&dpid=' + String(dpid);
	}
	if (currency = $('#dlb_currency option:selected').val())
	{	url += '&currency=' + String(currency);
	}
	if (amount = $('#dlb_amount').val())
	{	url += '&amount=' + String(amount);
	}
	if (dtype = $('#dlb_type option:selected').val())
	{	url += '&type=' + String(dtype);
	}
	if ($('#dlb_quick').prop('checked'))
	{	url += '&quick=1';
	}
	if ($('#dlb_zakat').prop('checked'))
	{	url += '&zakat=1';
	}
	if ($('#dlb_full').prop('checked'))
	{	url += '&full=1';
	}
	$('.dlbResultContainer form input[type="text"]').val(AP_Ajax(url));
} // end of fn DLBBuildLink

function DLBTypeChange(dont_rebuild)
{	var url, dcid, currency, new_elements;
	url = 'ajax_donationlinkbuilder.php?action=type_change&type=' + String($('#dlb_type option:selected').val());
	if (dcid = $('#dlb_country option:selected').val())
	{	url += '&dcid=' + String(dcid);
	}
	if (currency = $('#dlb_currency option:selected').val())
	{	url += '&currency=' + String(currency);
	}
	new_elements = $.parseJSON(AP_Ajax(url));
	$('#dlb_country').html(new_elements['countries']);
	$('#dlb_currency').html(new_elements['currencies']);
	DLBChangeCountry(dont_rebuild);
	if (!dont_rebuild)
	{	DLBBuildLink();
	}
} // end of fn DLBTypeChange

function DLBChangeCountry(dont_rebuild)
{	var url = 'ajax_donationlinkbuilder.php?action=country_change&dcid=' + String($('#dlb_country option:selected').val()) + '&dpid=' + String($('#dlb_project option:selected').val());
	$('#dlb_project').html(AP_Ajax(url));
	if (!dont_rebuild)
	{	DLBBuildLink();
	}
} // end of fn DLBChangeCountry

$('.dlbResultContainer form input[type="text"]').click(function() {$(this).select();});
