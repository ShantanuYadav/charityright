
function ClearField(element, value)
{
	if($(element).val() == value)
	{
		$(element).val('');
	}
} // end of fn ClearField

function RefillField(element, value)
{
	if($(element).val() == '')
	{
		$(element).val(value);
	}
} // end of fn RefillField

function number_format (number, decimals, dec_point, thousands_sep)
{	// Strip all characters but numerical ones.
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec)
		{
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3)
	{	s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec)
	{	s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
} // end of fn number_format

function AP_Ajax(url, external_url)
{	var xmlhttp;
	if (window.XMLHttpRequest) // code for IE7+, Firefox, Chrome, Opera, Safari
	{	xmlhttp=new XMLHttpRequest();
	} else // code for IE6, IE5
	{	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (external_url == undefined)
	{	url = jsSiteRoot + url;
	}
	//alert(url);
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	return xmlhttp.responseText;
} // end of fn AP_Ajax

function PM_Ajax_Post(url, params, action_function, external_url)
{	var xmlhttp;
	if (external_url == undefined)
	{	url = jsSiteRoot + url;
	}

	if (window.XMLHttpRequest) // code for IE7+, Firefox, Chrome, Opera, Safari
	{	xmlhttp = new XMLHttpRequest();
	} else // code for IE6, IE5
	{	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("POST", url, true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", params.length);
	xmlhttp.setRequestHeader("Connection", "close");

	xmlhttp.onreadystatechange = function()
	{	if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{	if ((action_function != undefined) && (action_function != ''))
			{	action_function(xmlhttp.responseText);
			}
		}
	}
	xmlhttp.send(params);

} // end of fn PM_Ajax_Post

function pageContentPopup(pagename)
{	var url = 'ajax_pagecontent.php?pagename=' + pagename;
	$('#pagecontent_modal_popup_inner').html(AP_Ajax(url));
	$("#pagecontent_modal_popup").jqmShow();
} // emd of fn pageContentPopup

function InfusionsoftContactSubmit(formtype)
{	var url = 'ajax_infusionsoftform.php';
	var params = new Array();
	params[params.length] = 'formtype=' + encodeURIComponent(formtype);
	
	var field_email = $('#inf_field_Email').val();
	if (field_email != undefined)
	{	params[params.length] = 'email=' + encodeURIComponent(field_email);
	}
	
	var field_country = $('#inf_custom_country0').val();
	if (field_country != undefined)
	{	params[params.length] = 'country=' + encodeURIComponent(field_country);
	}
	
	var field_firstname = $('#inf_field_FirstName').val();
	if (field_firstname != undefined)
	{	params[params.length] = 'firstname=' + encodeURIComponent(field_firstname);
	}
	
	var field_lastname = $('#inf_field_LastName').val();
	if (field_lastname != undefined)
	{	params[params.length] = 'lastname=' + encodeURIComponent(field_lastname);
	}
	
	var field_fullname = $('#inf_field_FullName').val();
	if (field_fullname != undefined)
	{	params[params.length] = 'fullname=' + encodeURIComponent(field_fullname);
	}

	var action_function = function(response)
		{	$('#infFormContainer').html(response);
		};
	//alert(params.join('&'));
	PM_Ajax_Post(url, params.join('&'), action_function);
} // emd of fn pageContentPopup

function sleep(miliseconds)
{	var currentTime = new Date().getTime();
	while (currentTime + miliseconds >= new Date().getTime()){}
} // emd of fn sleep

function TextCharLimit(currentElement, labelIdentifier, maxlength)
{	if ((maxlength == undefined) || ((maxlength = Number(maxlength)) <= 0))
	{	maxlength = 100;
	}
	if ($(currentElement).val().length > maxlength)
	{	$(labelIdentifier).addClass('textCharLimitOver');
	} else
	{	$(labelIdentifier).removeClass('textCharLimitOver');
	}
} // emd of fn TextCharLimit

function CRTwitterShare(share_url, share_title)
{	window.open("https://twitter.com/share?url=" + escape(share_url) + "&text=" + share_title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
	return false;
} // end of fn CRTwitterShare

$(document).ready(function($)
{	$(".kpMMmenu").mmenu(
		{	extensions: ["pageshadow", "position-right"],
			backButton: {close: true},
			navbars: [
				{position: top, content: ['prev','title','close']}],
			navbar: {add: true},
		},
		{	offCanvas: {position: "right"},
		}
	);
});
$(document).ready(function() {
	$('input, textarea').placeholder();
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		closeBtnInside: false,
		closeMarkup: '<a href="#" class="mfp-close mpf-close-custom"><i class="icon icon-cancel"></i></a>',
		fixedContentPos: false
	});
	$('.popup-image').magnificPopup({
		mainClass: 'mfp-fade',
		closeBtnInside: false,
		closeMarkup: '<a href="#" class="mfp-close mpf-close-custom"><i class="icon icon-cancel"></i></a>',
		gallery: {
			enabled: true,
			arrowMarkup: '<a href="#" class="mfp-arrow mfp-arrow-%dir%"><i class="icon icon-%dir%-open"></i></a>',
		},
		type: 'image'
	});
	$('.close-alert-container').on('click', function(){
		$('.alert-container').fadeOut();
		return false;
	});
	SocialShareKit.init();
	$('.campaign-page-mobile-button-share').on('click', function(){
		$('.campaign-page-mobile-buttons-share').slideToggle();
		return false;
	});
	if($('.campaign-page-mobile-button-share').length > 0){
		$('.container.footer_bottom').css({
			'padding-bottom' : '60px'
		});
	}
	$('.post-share-buttons a.share-button').on('click', function(){
		$(this).fadeOut( 400, function() {
			$('.post-share-buttons .ssk-group').show();
			$('.post-share-buttons .ssk-group a.ssk').each(function (i){
				var button = $(this);
				setTimeout(function(){
					button.addClass('show-button');
				}, 100*i);
			});
		});
		return false;
	});
	/*
	$(window).on('scroll', function() {
		if($( window ).width() < 768 ) {
			scrollPosition = $(this).scrollTop();
			if (scrollPosition >= 50) {
				$('.campaign-page-mobile-buttons-container').slideToggle();
				$(this).off('scroll');
			}
		}
	});
	*/
	if($('.campaign-page-mobile-buttons-container').length > 0){
		$(window).scroll(function () {
			scrollPosition = $(this).scrollTop();
			if ($( window ).width() >= 768 )
			{	scrollPosition = $(this).scrollTop();
				if (scrollPosition >= 200)
				{	$('.campaign-page-mobile-buttons-container').slideDown();
				} else
				{	$('.campaign-page-mobile-buttons-container').slideUp();
				}
			}
		});
	}
	if ($( window ).width() < 768 )
	{	$('.campaign-page-mobile-buttons-container').slideDown();
	}
	
	if($('#arrow-bottom').length > 0){
		$(window).scroll(function () {
			if ($(document).height() - $(this).scrollTop() - $(window).height() > $(window).height()) {
				$('#arrow-bottom').fadeIn();
			} else {
				$('#arrow-bottom').fadeOut();
			}
		});

		$('#arrow-bottom').click(function () {
			$('body,html').animate({
				scrollTop: $(window).scrollTop() + $(window).height()
				}, 1000);
			return false;
		});
	}
	if($('#map').length > 0){
		$("#map").googleMap({
			zoom: 14,
		});
		$("#map").addMarker({
			coords: [53.7984540, -1.7722950],
      		icon: '/img/template/map-marker.png', // Icon URL,
		});
	}
	
	$('input.inputSelectOnFocus').focus
		(	function()
			{	$(this).select();
				if (Number($(this).val()) <= 0)
				{	$(this).val('');
				}
			}
		);
	
	/*$('input.inputSelectOnFocus').focus
		(	function()
			{	$(this).one
				(	'mouseup',
					function()
					{	$(this).select();
						if (Number($(this).val()) <= 0)
						{	$(this).val('');
						}
					}
				);
			}
		);*/
		$('iframe[src*="youtube"]').parent().fitVids();
	
});

function HRLCartPopUp()
{	if ($('.hrLinksCart').hasClass('hrLinksCartOpen'))
	{	$('.hrLinksCart').removeClass('hrLinksCartOpen');;
	} else
	{	$('.hrLinksCart').addClass('hrLinksCartOpen');
	}
} // end of fn HRLCartPopUp

function ContactOptionsFormOnChange()
{	var setcount = 0, allcount = 0;
	$('.contactPrefFormElements input[type="checkbox"]').each
		(	function()
			{	if ($(this).prop('checked'))
				{	setcount++;
					//alert($(this).id);
				}
				allcount++;
			}
		);
	$('input#contactpref_all').prop('checked', setcount == allcount);
} // end of fn ContactOptionsFormChange

function ContactOptionsFormSetAll()
{	var setall = ($('input#contactpref_all').prop('checked') ? true : false);
	$('.contactPrefFormElements input[type="checkbox"]').each(function(){$(this).prop('checked', setall);});
} // end of fn ContactOptionsFormSetAll