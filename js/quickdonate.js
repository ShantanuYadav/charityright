function QDFormSubmit()
{	var url, new_url, dcid, dpid, currency, amount, dtype;
	url = 'ajax_quickdonate.php?action=build&full=1';
	if (dcid = $('#qd_country').val())
	{	url += '&dcid=' + String(dcid);
	}
	if (dpid = $('#qd_project').val())
	{	url += '&dpid=' + String(dpid);
	}
	if (currency = $('#qd_currency option:selected').val())
	{	url += '&currency=' + String(currency);
	}
	if (amount = $('#qd_amount').val())
	{	url += '&amount=' + String(amount);
	}
	if (dtype = $('#qd_type option:selected').val())
	{	url += '&type=' + String(dtype);
	}
	if ($('#qd_quick').val() > 0)
	{	url += '&quick=1';
	}
	if ($('#qd_zakat').val() > 0)
	{	url += '&zakat=1';
	}
	new_url = AP_Ajax(url);
	//alert(new_url);
	window.location = new_url;
} //  end of fn QDFormSubmit

function QDTypeChange()
{	var url, currency, new_elements;
	url = 'ajax_quickdonate.php?action=type_change&type=' + String($('#qd_type option:selected').val());
	if (currency = $('#qd_currency option:selected').val())
	{	url += '&currency=' + String(currency);
	}
	new_elements = $.parseJSON(AP_Ajax(url));
	$('#qd_currency').html(new_elements['currencies']);
	QDCurrencyChange();
} // end of fn QDTypeChange

function QDCurrencyChange()
{	var url;
	url = 'ajax_quickdonate.php?action=cur_change&currency=' + String($('#qd_currency option:selected').val());
	$('.qdFormCurSymbol').html(AP_Ajax(url));
} // end of fn QDCurrencyChange