
function PagesPopUp(menuid)
{	PagesPopUpRefresh(menuid);
	$("#rlp_modal_popup").jqmShow();
} // end of fn PagesPopUp

function PagesPopUpRefresh(menuid)
{	url = 'ajax_femenus.php?id=' + String(menuid) + '&action=popup';
	document.getElementById('rlpModalInner').innerHTML = AP_Ajax(url);
} // end of fn PagesPopUpRefresh

function PageRemove(menuid, miid)
{	url = 'ajax_femenus.php?id=' + String(menuid) + '&miid=' + String(miid) + '&action=remove';
	document.getElementById('banContainer').innerHTML = AP_Ajax(url);
} // end of fn PageRemove

function PageAdd(menuid, pageid)
{	
	url = 'ajax_femenus.php?id=' + String(menuid) + '&pageid=' + String(pageid) + '&action=add';
	//document.getElementById('rlpModalInner').innerHTML = AP_Ajax(url);
	AP_Ajax(url);
	PagesTableRefresh(menuid);
	//$("#rlp_modal_popup").jqmHide();
} // end of fn BannerAdd

function PagesTableRefresh(menuid)
{	url = 'ajax_femenus.php?id=' + String(menuid) + '&action=refresh';
	document.getElementById('banContainer').innerHTML = AP_Ajax(url);
} // end of fn PagesTableRefresh
