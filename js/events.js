function OpenTicketBook(ttid)
{	$('.jqmWindow').jqm({ overlay : 90, modal : false });
	$(".edTicketPopup").jqmShow();
	$(".edTicketPopupInner").html('');
	url = 'ajax_events.php?action=ticketget&ttid=' + String(ttid);
	$(".edTicketPopupInner").html(AP_Ajax(url));
} // end of fn OpenTicketBook

function SaveTicketBook(ttid)
{	//$(".edTicketPopupInner").html('');
	//alert($('#tpopBookForm form').find('input[type="number"]').val());
	//alert($('#tpopFormNumber').val());
	url = 'ajax_events.php?action=ticketbook&ttid=' + String(ttid) + '&number=' + String($('#tpopFormNumber').val());
	$(".edTicketPopupInner").html(AP_Ajax(url));
	CartHeaderUpdate();
} // end of fn SaveTicketBook

function CartUpdateTicketNumber(ttid, adjust)
{	$('.cartHTable').html('<div id="loadingpreview"><p>Please wait ... your plates are being generated</p><img src="/img/template/ajax-loader.gif" alt="Loading" /></div>');
	url = 'ajax_events.php?action=ticketadjust&ttid=' + String(ttid) + '&adjust=' + adjust;
	$('.cartHTable').html(AP_Ajax(url));
	CartHeaderUpdate();
} // end of fn CartUpdateTicketNumber

function CartHeaderUpdate()
{	url = 'ajax_events.php?action=cartheader';
	action_function = function(response){$('#cartBookHeader').html(response);$('#cartBookMobileHeader').html(response);};
	PM_Ajax_Post(url, '', action_function);
} // end of fn CartUpdateTicketNumber
