function RemoveDonation(dontype, donkey)
{	var url = 'ajax_checkout.php?action=removedonation&dontype=' + encodeURIComponent(dontype) + '&donkey=' + encodeURIComponent(donkey);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.cartlist != undefined)
	{	$('.cartListContainer').html(response.cartlist);
	}
	if (response.breadcrumbs != undefined)
	{	$('.donation_breadcrumbs .container_inner').html(response.breadcrumbs);
	}
	if (response.basket_desktop != undefined)
	{	$('.hrLinksCart').html(response.basket_desktop);
	}
	if (response.basket_mobile != undefined)
	{	$('.header_cartopener').html(response.basket_mobile);
	}
} // end of fn RemoveDonation

function AlterDonationQuantity(donkey, qtyamount)
{	var url = 'ajax_checkout.php?action=alterdonation&donkey=' + encodeURIComponent(donkey) + '&quantity=' + encodeURIComponent(qtyamount);
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.cartlist != undefined)
	{	$('.cartListContainer').html(response.cartlist);
	}
	if (response.breadcrumbs != undefined)
	{	$('.donation_breadcrumbs .container_inner').html(response.breadcrumbs);
	}
	if (response.basket_desktop != undefined)
	{	$('.hrLinksCart').html(response.basket_desktop);
	}
	if (response.basket_mobile != undefined)
	{	$('.header_cartopener').html(response.basket_mobile);
	}
} // end of fn AlterDonationQuantity


function HowHeardChanged()
{	var howheard = $('select#donor-howheard').val();
	if (howheard == 'other')
	{	$('.crFormHowHeardText').removeClass('hidden');
	} else
	{	$('.crFormHowHeardText').addClass('hidden');
		$('#donor-howheardtext').val('');
	}
} // end of fn HowHeardChanged