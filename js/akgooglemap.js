var gmapAPIKey = "";

function gminitialize(google_lat, google_long, map_title)
{
    var myLatlng = new google.maps.LatLng(google_lat,google_long);
    var myOptions = {
      zoom: 15,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    g_map = new google.maps.Map(document.getElementById('gmap'), myOptions);

    var marker = new google.maps.Marker({
				position: myLatlng,
				map: g_map,
				title: map_title
			 //   ,icon: 'img/template/marker.png'
			});
    
} //  end of fn gminitialize

function akGoogleMapLookUp()
{	if ((address = document.getElementById("address")))
	{	akgeocoder = new google.maps.Geocoder();
		//alert(akgeocoder);
		akgeocoder.geocode(
			{ 'address': address.value }, 
			function (results, status)
			{
				if (status == google.maps.GeocoderStatus.OK)
				{	//alert(results[0].geometry.location);
					$locbits = String(results[0].geometry.location).split(",");
					document.getElementById("googlelat").value = $locbits[0].substring(1);
					document.getElementById("googlelong").value = $locbits[1].substring(0, $locbits[1].length - 1);
					akGoogleMapRefresh();
				} else alert('no location found for...\n' + address.value);

			});

	}
} // end of fn akGoogleMapLookUp

function akGoogleMapRefresh()
{	if ((glatInput = document.getElementById("googlelat")) && (glongInput = document.getElementById("googlelong")) && (nameInput = document.getElementById("loctitle")))
	{	glat = Number(glatInput.value);
		glong = Number(glongInput.value);
		//alert(String(glat));
		gminitialize(glat, glong, nameInput.value);
		document.getElementById('gmap').style.display='block';
	} //else alert("failed");
} // end of fn akGoogleMapRefresh
