var pageCampaignID;

function CampaignAddTeamOpen(campaignid)
{	var url;
	pageCampaignID = campaignid;
	CampaignAddTeamContents(campaignid);
	$(".campAddTeamPopup").jqmShow();
	if (document.getElementById("campSearchFilter"))
	{	$(function() { addTextCallback(document.getElementById("campSearchFilter"), CampaignAddTeamRefreshList, 500); });
	}
} // end of fn CampaignAddTeamOpen

function CampaignAddTeamContents(campaignid)
{	url = 'ajax_campaign.php?action=teamaddopen&cid=' + String(campaignid);
	$(".campAddTeamPopupInner").html(AP_Ajax(url));
} // end of fn CampaignAddTeamContents

function CampaignAddLinkRefresh(campaignid)
{	url = 'ajax_campaign.php?action=teamaddlink&cid=' + String(campaignid);
	$(".campAddTeam").html(AP_Ajax(url));
} // end of fn CampaignAddLinkRefresh

function CampaignAddTeam(campaignid, teamid)
{	var url;
	url = 'ajax_campaign.php?action=teamadd&cid=' + String(campaignid) + '&teamid=' + String(teamid);
	ajax_action = function(ajax_response)
	{	var response = ajax_response.split('|~|');
		switch (response[0])
		{	case '0':
				break;
			case '1':
				$(".campAddTeamPopup").jqmHide();
				CampaignAddLinkRefresh(campaignid);
				break;
		}
	}
	PM_Ajax_Post(url, '', ajax_action);
} // end of fn CampaignAddTeam

function CampaignAddTeamRefreshList()
{	var url;
	url = 'ajax_campaign.php?action=teamaddfilter&cid=' + String(pageCampaignID);
	if ((filterfield = document.getElementById('campSearchFilter')) && (filterfield.value.length > 0))
	{	url += '&filter=' + encodeURIComponent(filterfield.value);
	}

	ajax_action = function(ajax_response)
	{	$("#campAddTeamCampaignList").html(ajax_response);
	}
	PM_Ajax_Post(url, '', ajax_action);

} // end of fn CampaignAddTeamRefreshList

function addTextCallback(textArea, callback, delay)
{
	var timer = null;
	textArea.onkeyup = function() {
		if (timer) {
			window.clearTimeout(timer);
		}
		timer = window.setTimeout( function() {
			timer = null;
			callback();
		}, delay );
	};
	textArea = null;
} // end of fn addTextCallback

function SetCampDFSection(dfsection)
{	$('.campDonateProgress li').removeClass('campDonateProgressCurrent');
	$('.campDonateProgress li.campDonateProgress_' + dfsection).addClass('campDonateProgressCurrent');
	$('.campDFSection').removeClass('campDFSectionCurrent');
	$('.campDFSection.campDFSection_' + dfsection).addClass('campDFSectionCurrent');
} // end of fn SetCampDFSection

function CampDFCountryChange(select_element)
{	if (select_element.value == 'GB')
	{	$('.campDFGiftAid').removeClass('campDFGiftAidHidden');
	} else
	{	$('.campDFGiftAid').addClass('campDFGiftAidHidden');
	}
} // end of fn CampDFCountryChange

function CRRegImageFileOnChange()
{	if ($('input[name="campimage"]').val().length > 0)
	{	$('.crstarsRegisterAvatars').addClass('crstarsRegisterAvatarsHidden');
	} else
	{	$('.crstarsRegisterAvatars').removeClass('crstarsRegisterAvatarsHidden');
	}
} // end of fn CRRegImageFileOnChange

function CRRegAvatarChooser(avatarid)
{	$('input[name="avatarid"]').val(String(avatarid));
	$('.campaign-avatars li').removeClass('crAvatarSelected');
	$('.campaign-avatars #crAvatar_' + String(avatarid)).addClass('crAvatarSelected');
	return false;
} // end of fn CRRegAvatarChooser

function CRRegCurrencyChange()
{	var cur_bits = $('select[name="campcurrency"] option:selected').html().split(' - ');
	if (cur_bits[1] == undefined)
	{	$('.campCurrencySpan').html('');
	} else
	{	$('.campCurrencySpan').html('' + cur_bits[1] + '');
	}
} // end of fn CRRegCurrencyChange

function CRRegCountryChange()
{	var project, url;
	url = 'ajax_campaign.php?action=regcountrychange&country=' + encodeURIComponent($('#donation_country option:selected').val());
	if (project = $('#donation_project').val())
	{	url += '&project=' + encodeURIComponent(project);
	} else
	{	if (project = $('#donation_project option:selected').val())
		{	url += '&project=' + encodeURIComponent(project);
		}
	}
	$('#countryProjectContainer').html(AP_Ajax(url));
} // end of fn CRRegCountryChange

function CRRegSampleChooser()
{	var txt = document.createElement('textarea');
	txt.innerHTML = crsSamples[$('.crstarsRegSampleText input[name="sample_chosen"]:checked').val()];
	$('textarea[name="description"]').val(txt.value);
} // end of fn CRRegSampleChooser

function CRSearchOpen()
{	$('.hrlSearchPopup').addClass('hrlSearchPopupOpen');
} // end of fn CRSearchOpen

function CRSearchClose()
{	$('.hrlSearchPopup').removeClass('hrlSearchPopupOpen');
} // end of fn CRSearchClose

function CRSearchCanSubmit(formid)
{	return ($('.hrlSearch #' + formid + ' input[name="crs_search"]').val().length) > 0;
} // end of fn CRSearchCanSubmit

function CRcampaignDonationsMore(campaignid, start, listcount)
{	var url;
	url = 'ajax_campaign.php?action=moredonations&cid=' + String(campaignid) + '&start=' + String(start) + '&listcount=' + String(listcount);
	$('.campDonationsList ul li.campDonationsMore').remove();
	$('.campDonationsList ul').append(AP_Ajax(url));
} // end of fn CRcampaignDonationsMore

$(function(){
	$("header").addClass('sticky keepsticky');
});