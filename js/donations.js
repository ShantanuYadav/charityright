if (donation_ajax_endpoint == undefined)
{	var donation_ajax_endpoint = 'ajax_donations.php';
}
var placeSearch, autocomplete;
function initAutocomplete() {
	// Create the autocomplete object, restricting the search predictions to
	// geographical location types.
	autocomplete = new google.maps.places.Autocomplete(document.getElementById('billing-address'), {types: ['geocode']});

	// Avoid paying for data that you don't need by restricting the set of
	// place fields that are returned to just the address components.
	autocomplete.setFields(['address_component']);

	// When the user selects an address from the drop-down, populate the
	// address fields in the form.
	autocomplete.addListener('place_changed', fillInAddress);

	
	autocomplete2 = new google.maps.places.Autocomplete(document.getElementById('home-address'), {types: ['geocode']});
	autocomplete2.setFields(['address_component']);
	autocomplete2.addListener('place_changed', fillInAddress2);

	autocomplete3 = new google.maps.places.Autocomplete(document.getElementById('home-address-monthly'), {types: ['geocode']});
	autocomplete3.setFields(['address_component']);
	autocomplete3.addListener('place_changed', fillInAddress3);
}

function fillInAddress() {
	// Get the place details from the autocomplete object.
	var place = autocomplete.getPlace();
	var address  = [];
	var postcode = country = '';

	// // Get each component of the address from the place details,
	// // and then fill-in the corresponding field on the form.
	if (place != undefined) {
		for (var i = 0; i < place.address_components.length; i++) {
			var addressType = place.address_components[i].types[0];
			if (addressType == 'street_number' || addressType == 'route' || addressType == 'locality' || addressType == 'postal_town') {
				if (addressType == 'street_number') {
					var val = place.address_components[i]['short_name'];
					address.push(val);
				}
				if (addressType == 'route' || addressType == 'locality' || addressType == 'postal_town') {
					var val = place.address_components[i]['long_name'];
					address.push(val);
				}
			}
			if (addressType == 'country') {
				var country = place.address_components[i]['short_name'];
			}
			if (addressType == 'postal_code') {
				var postcode = place.address_components[i]['short_name'];
			}
		}
		if (country) {
			$('#donor-country').val(country).trigger('change');
		}
		$('#billing-post-code').val('');
		if (postcode) {
			$('#billing-post-code').val(postcode);
		}
		// console.log(address.join(", "));
	}
}

function fillInAddress2() {
	// Get the place details from the autocomplete object.
	var place = autocomplete2.getPlace();
	var address  = [];
	var postcode = country = '';

	// // Get each component of the address from the place details,
	// // and then fill-in the corresponding field on the form.
	if (place != undefined) {
		for (var i = 0; i < place.address_components.length; i++) {
			var addressType = place.address_components[i].types[0];
			
			if (addressType == 'postal_code') {
				var postcode = place.address_components[i]['short_name'];
			}
		}
		$('#post-code-field1').val('');
		if (postcode) {
			$('#post-code-field1').val(postcode);
		}
	}
}

function fillInAddress3() {
	// Get the place details from the autocomplete object.
	var place = autocomplete3.getPlace();
	var address  = [];
	var postcode = country = '';

	// // Get each component of the address from the place details,
	// // and then fill-in the corresponding field on the form.
	if (place != undefined) {
		for (var i = 0; i < place.address_components.length; i++) {
			var addressType = place.address_components[i].types[0];
			
			if (addressType == 'postal_code') {
				var postcode = place.address_components[i]['short_name'];
			}
		}
		$('#post-code-field2').val('');
		if (postcode) {
			$('#post-code-field2').val(postcode);
		}
	}
}

function geolocate() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			var circle = new google.maps.Circle(
				{center: geolocation, radius: position.coords.accuracy});
			autocomplete.setBounds(circle.getBounds());
		});
	}
}

function DonateTypeChangeNew()
{	var url = donation_ajax_endpoint + '?action=typechangenew&type=';
	var donType = GetDonateTypeNew();
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donQuantity = $('input#donation-quantity').val();

	if (donType != undefined)
	{	url += donType;
	}
	url += '&currency='
	if (currency != undefined)
	{	url += currency;
	}
	url += '&country='
	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	//var project = $('select[name="donation_project"]').val();
	if (project != undefined)
	{	url += project;
	}
	url += '&project2=';
	if (project2 != undefined)
	{	url += encodeURIComponent(project2);
	}
	url += '&type=';
	if (donType != undefined)
	{	url += encodeURIComponent(donType);
	}
	url += '&currency=';
	if (currency != undefined)
	{	url += encodeURIComponent(currency);
	}
	url += '&amount=';
	if (donAmount != undefined)
	{	url += encodeURIComponent(donAmount);
	}
	url += '&quantity=';
	if (donQuantity != undefined)
	{	url += encodeURIComponent(donQuantity);
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);

	//var response = jQuery.parseJSON(AP_Ajax(url));
	$('.donation-currency-list').html(response.currencies);

	if (response.countrylist == undefined)
	{	$('.donation-country-list').html('');
	} else
	{	$('.donation-country-list').html(response.countrylist);
	}
	if (response.projectlist == undefined)
	{	$('.donation-project-list').html('');
	} else
	{	$('.donation-project-list').html(response.projectlist);
	}
	if (response.projectlist2 == undefined)
	{	$('.donation-project2-list').html('');
	} else
	{	$('.donation-project2-list').html(response.projectlist2);
	}
	if (response.donationamount != undefined)
	{	$('.donation_amount_container').html(response.donationamount);
	}
	if (response.nozakat != undefined)
	{	NoZakatChange(response.nozakat);
	}

	if (donType == 'monthly')
	{	$('.donation-admin').addClass('hidden');
	} else
	{	$('.donation-admin').removeClass('hidden');
	}
	DonationCurrencyChangeNew();
} // end of fn DonateTypeChangeNew

function NoZakatChange(nozakat)
{	if (nozakat)
	{	$('.dpZakatContainer').addClass('hidden');
		$('select[name="donation_zakat"]').val('0').change();
	} else
	{	$('.dpZakatContainer').removeClass('hidden');
	}
} // end of fn NoZakatChange

function GetDonateTypeNew()
{	return $('select[name=donation_type]').val();
} // end of fn GetDonateTypeNew

function DonationCurrencyChangeNew()
{
	var currency = $('#donation_currency').val();
	if (currency != undefined)
	{	
		var country = $('select#donation-country').val();
		var project = $('select#donation-project').val();
		var project2 = $('select#donation-project2').val();
		var donType = $('#donation-type').val();
		var donAmount = $('input#donation-amount').val();
		var donQuantity = $('input#donation-quantity').val();
		var url = donation_ajax_endpoint + '?action=currencychangenew&newcurrency=' + encodeURIComponent(currency);

		url += '&country=';
		if (country != undefined)
		{	url += encodeURIComponent(country);
		}
		url += '&project=';
		if (project != undefined)
		{	url += encodeURIComponent(project);
		}
		url += '&project2=';
		if (project2 != undefined)
		{	url += encodeURIComponent(project2);
		}
		url += '&type=';
		if (donType != undefined)
		{	url += encodeURIComponent(donType);
		}
		url += '&quantity=';
		if (donQuantity != undefined)
		{	url += encodeURIComponent(donQuantity);
		}

//alert(url);
		var rawresponse = AP_Ajax(url);
//alert(rawresponse);
		var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
		$('.donation-currency-symbol').html(response.cursymbol);
		if (response.donationamount != undefined)
		{	$('.donation_amount_container').html(response.donationamount);
		}
		if (response.donationtype != undefined)
		{	$('.donation_type_container').html(response.donationtype);
		}
		$('input[name="donation_oldcurrency"]').val(currency);
	}
} // end of fn DonationCurrencyChangeNew

function DonatePresetClickNew(amount, preset_count) {
	$('input[name="donation_amount"]').val(parseFloat(amount).toFixed(2));
	$('.donation-preset').removeClass('donation-presets-selected');
	$('.donation-preset-list .form-col:nth-child(' + String(preset_count) + ') .donation-preset').addClass('donation-presets-selected');
} // end of fn DonatePresetClickNew

function DonateAmountChangeNew()
{	$('.donation-preset').removeClass('donation-presets-selected');
} // end of fn DonateAmountChangeNew

function DonateQuantityChange()
{	var quantity = parseInt($('input[name="donation_quantity"]').val());
	if (quantity < 1)
	{	quantity = 1;
	}
	$('input[name="donation_quantity"]').val(quantity);
	var amount = Number($('input[name="donation_amountfixed"]').val());
	$('input[name="donation_amount"]').val(amount * quantity);
	$('.donation_amount_display').html(number_format(amount * quantity, 2, '.', ','));
	$('.dqcNumber').html(String(quantity));
	if (quantity > 1)
	{	$('.donation_quantity_changer_minus').removeClass('donation_quantity_changer_hidden');
	} else
	{	$('.donation_quantity_changer_minus').addClass('donation_quantity_changer_hidden');
	}
} // end of fn DonateQuantityChange

function DonateQuantityIncrement(inc_amount)
{	$('input[name="donation_quantity"]').val(parseInt($('input[name="donation_quantity"]').val()) + inc_amount);
	DonateQuantityChange();
} // end of fn DonateQuantityIncrement

function DonorCountryChangeNew() {
	$('.donorAddressPCA input').val('');
	$('.donorAddressManual').addClass('hidden');
	if ($('select[name="donor_country"]').val() == 'GB')
	{	$('.donor-giftaid-box').removeClass('hidden');
		$('.donorAddressPCA').removeClass('hidden');
		$('.donorAddressManual input').val('');
	} else
	{	if ($('.donorAddressManual').hasClass('hidden'))
		{
		}
		$('.donor-giftaid-box').addClass('hidden');
		$('.donorAddressPCA').addClass('hidden');
		$('.donorAddressManual').removeClass('hidden');
	}
} // end of fn DonorCountryChangeNew

function DonorCountryChangeCampaign() {

	if ($('select[name="donor_country"]').val() == 'GB') {
		if ($('.donor-giftaid-box').hasClass('hidden')) {
			$('.donor-giftaid-box').removeClass('hidden');
		}
	} else {
		if (!$('.donor-giftaid-box').hasClass('hidden')) {
			$('.donor-giftaid-box').addClass('hidden');
		}
	}
} // end of fn DonorCountryChange

function DonateCountryChangeNew()
{
	var url = donation_ajax_endpoint + '?action=projectlistnew&country=';
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donQuantity = $('input#donation-quantity').val();
	if (country != undefined)
	{	url += encodeURIComponent(country);
	}
	url += '&project=';
	if (project != undefined)
	{	url += encodeURIComponent(project);
	}
	url += '&project2=';
	if (project2 != undefined)
	{	url += encodeURIComponent(project2);
	}
	url += '&type=';
	if (donType != undefined)
	{	url += encodeURIComponent(donType);
	}
	url += '&currency=';
	if (currency != undefined)
	{	url += encodeURIComponent(currency);
	}
	url += '&amount=';
	if (donAmount != undefined)
	{	url += encodeURIComponent(donAmount);
	}
	url += '&quantity=';
	if (donQuantity != undefined)
	{	url += encodeURIComponent(donQuantity);
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.projectlist == undefined)
	{	$('.donation-project-list').html('');
	} else
	{	$('.donation-project-list').html(response.projectlist);
	}
	if (response.projectlist2 == undefined)
	{	$('.donation-project2-list').html('');
	} else
	{	$('.donation-project2-list').html(response.projectlist2);
	}
	if (response.donationamount != undefined)
	{	$('.donation_amount_container').html(response.donationamount);
	}
	if (response.nozakat != undefined)
	{	NoZakatChange(response.nozakat);
	}
} // end of fn DonateCountryChangeNew

function DonateProjectChangeNew()
{
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donQuantity = $('input#donation-quantity').val();

	var url = donation_ajax_endpoint + '?action=projectlistnew2&country=';

	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	if (project != undefined)
	{	url += project;
	}
	url += '&project2='
	if (project2 != undefined)
	{	url += project2;
	}
	url += '&type='
	if (donType != undefined)
	{	url += donType;
	}
	url += '&currency=';
	if (currency != undefined)
	{	url += encodeURIComponent(currency);
	}
	url += '&amount=';
	if (donAmount != undefined)
	{	url += encodeURIComponent(donAmount);
	}
	url += '&quantity=';
	if (donQuantity != undefined)
	{	url += encodeURIComponent(donQuantity);
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.projectlist2 == undefined)
	{	$('.donation-project2-list').html('');
	} else
	{	$('.donation-project2-list').html(response.projectlist2);
	}
	if (response.donationamount != undefined)
	{	$('.donation_amount_container').html(response.donationamount);
	}
} // end of fn DonateProjectChangeNew

function DonateProject2Change()
{
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donQuantity = $('input#donation-quantity').val();

	var url = donation_ajax_endpoint + '?action=projectlist2change&country=';

	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	if (project != undefined)
	{	url += project;
	}
	url += '&project2='
	if (project2 != undefined)
	{	url += project2;
	}
	url += '&type='
	if (donType != undefined)
	{	url += donType;
	}
	url += '&currency=';
	if (currency != undefined)
	{	url += encodeURIComponent(currency);
	}
	url += '&amount=';
	if (donAmount != undefined)
	{	url += encodeURIComponent(donAmount);
	}
	url += '&quantity=';
	if (donQuantity != undefined)
	{	url += encodeURIComponent(donQuantity);
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.donationamount != undefined)
	{	$('.donation_amount_container').html(response.donationamount);
	}
} // end of fn DonateProject2Change

function DonorGiftaidCheckerNew(yes_ticked) {
	$('input[name="donor_giftaidchecked"]').val('1');
	if (yes_ticked) {
		$('input[name="donor_giftaid"]').prop('checked', 'checked');
	}
	$('form#donatePersonalStageForm').submit();
}

function DonorAddressBox(){
	var $pcaAddressContainer = $('.pcaAddressContainer');
	var $donor_address_box = $('.donor-address-box');
	var $donor_address_box_content = $('.donor-address-box-content');
	$donor_address_box_content.bind("DOMSubtreeModified",function(){
		if($donor_address_box_content.html().length > 0){
			$donor_address_box.removeClass('hidden');
			$pcaAddressContainer.addClass('hidden');
		}else{
			$pcaAddressContainer.removeClass('hidden');
			$donor_address_box.addClass('hidden');
		}
	});
	$('.address-search-again a').click(function(){
		$donor_address_box_content.html("");
		$("#pca_donor_address1").val("");
		$("#pca_donor_address2").val("");
		$("#pca_donor_address3").val("");
		$("#pca_donor_city").val("");
		$("#pca_donor_postcode").val("");
		$("#pca_donor_country").val("");
		$donor_address_box.addClass('hidden');
		$pcaAddressContainer.removeClass('hidden');
		return false;
	})
}
function DonationCombineValues(){
	var f1 = $('#dd_sort_1').val(),
		f2 = $('#dd_sort_2').val(),
		f3 = $('#dd_sort_3').val(),
		$field = $('#dd-accountsortcode');

		var field_value = f1+f2+f3;
		console.log(field_value);
		if(field_value.length == 6){
			$field.val(field_value);
		}
}
$(document).ready(function() {
	DonorAddressBox();
});



function DonateTypeChange()
{	var url = donation_ajax_endpoint + '?action=typechange&type=';
	var donType = GetDonateType();
	if (donType != undefined)
	{	url += donType;
	}
	url += '&currency='
	var currency = $('#donation_currency').val();
	if (currency != undefined)
	{	url += currency;
	}
	url += '&country='
	var country = $('select[name="donation_country"]').val();
	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	var project = $('select[name="donation_project"]').val();
	if (project != undefined)
	{	url += project;
	}
	url += '&amount=';

	var response = jQuery.parseJSON(AP_Ajax(url));
	$('#donationCurrenciesContainer').html(response.currencies);
	$('#donationCountriesContainer').html(response.countrylist);
	$('#donationProjectContainer').html(response.projectlist);
	if (donType == 'monthly')
	{	$('.donation_admin').addClass('hidden');
	} else
	{	$('.donation_admin').removeClass('hidden');
	}
	$('.donation_types_chooser li').removeClass('selected');
	$('.donation_types_chooser li#dtype_li_' + donType).addClass('selected');
	DonationCurrencyChange();
} // end of fn DonateTypeChange

function GetDonateType()
{	return $('.donation_types_chooser li input[type="radio"]:checked').val();
} // end of fn GetDonateType

function DonationCurrencyChange()
{	var currency = $('#donation_currency').val();
	if (currency != undefined)
	{	var url = donation_ajax_endpoint + '?action=currencychange&newcurrency=' + currency + '&amount=' + String($('input[name="donation_amount"]').val()) + '&adminamount=' + String($('input[name="donation_admin"]').val()) + '&oldcurrency=' + String($('input[name="donation_oldcurrency"]').val()) + '&type=' + GetDonateType();
		var response = jQuery.parseJSON(AP_Ajax(url));
		//console.log(response);
		$('.donationCurrencySymbol').html(response.cursymbol);
		$('input[name="donation_amount"]').val(response.amount);
		$('input[name="donation_admin"]').val(response.adminamount);
		$('input[name="donation_oldcurrency"]').val(currency);
		$('.presets_list').html(response.presets);
	}
} // end of fn DonationCurrencyChange

function DonatePresetClick(amount, preset_count)
{	$('input[name="donation_amount"]').val(parseFloat(amount).toFixed(2));
	$('.presets_list ul li').removeClass('presets_selected');
	$('.presets_list ul li:nth-child(' + String(preset_count) + ')').addClass('presets_selected');
} // end of fn DonatePresetClick

function DonateAmountChange()
{	$('.presets_list ul li').removeClass('presets_selected');
} // end of fn DonateAmountChange

function DonorCountryChange()
{	$('.donorAddressPCA input').val('');
	$('.donorAddressManual').addClass('hidden');
	if ($('select[name="donor_country"]').val() == 'GB')
	{	$('.donor_giftaid').removeClass('hidden');
		$('.donorAddressPCA').removeClass('hidden');
		$('.donorAddressManual input').val('');
	} else
	{	if ($('.donorAddressManual').hasClass('hidden'))
		{
		}
		$('.donor_giftaid').addClass('hidden');
		$('.donorAddressPCA').addClass('hidden');
		$('.donorAddressManual').removeClass('hidden');
	}
} // end of fn DonorCountryChange

function DonationFomReset()
{	var url = donation_ajax_endpoint + '?action=donationformreset&currency=';
	var currency = $('#donation_currency').val();
	if (currency != undefined)
	{	url += currency;
	}
//alert(url);
	var rawresponse = AP_Ajax(url);
//alert(rawresponse);
	var response = jQuery.parseJSON(rawresponse);
//alert(response.test);
	if (response.donationtype != undefined)
	{	$('.donation_type_container').html(response.donationtype);
		if (response.countrylist == undefined)
		{	$('.donation-country-list').html('');
		} else
		{	$('.donation-country-list').html(response.countrylist);
		}
		if (response.projectlist == undefined)
		{	$('.donation-project-list').html('');
		} else
		{	$('.donation-project-list').html(response.projectlist);
		}
		if (response.projectlist2 == undefined)
		{	$('.donation-project2-list').html('');
		} else
		{	$('.donation-project2-list').html(response.projectlist2);
		}
		if (response.donationamount != undefined)
		{	$('.donation_amount_container').html(response.donationamount);
		}
		if (response.currencies != undefined)
		{	//$('.donation-currency-list').html(response.currencies);
			$('#donationCurrencyChooserContainer').html(response.currencies);
		}
		$('input[name="donation_admin"]').val('0');
		if (response.donationtype == 'monthly')
		{	$('.donation-admin').addClass('hidden');
		} else
		{	$('.donation-admin').removeClass('hidden');
		}
	}
} // end of fn DonationFomReset

function DonateCountryChange()
{	var url = donation_ajax_endpoint + '?action=projectlist&country=';
	var country = $('select[name="donation_country"]').val();
	var project = $('select[name="donation_project"]').val();
	var donType = $('.donation_types_chooser li input[type="radio"]:checked').val();
	if (country != undefined)
	{	url += country;
	}
	url += '&project='
	if (project != undefined)
	{	url += project;
	}
	url += '&type='
	if (donType != undefined)
	{	url += donType;
	}
	$('#donationProjectContainer').html(AP_Ajax(url));
} // end of fn DonateCountryChange

function DonorGiftaidChecker(yes_ticked)
{	
	
	var donation_type = $("input[type=radio][name='donation_type']:checked").val();
	if(donation_type == 'oneoff'){
		$("#oneoffGiftAidModal").modal('show');
	}
	if(donation_type == 'monthly'){
		$("#gift-aid-monthly").modal('show');
	}
	$('input[name="donor_giftaidchecked"]').val('1');
	if (yes_ticked)
	{	$('input[name="donor_giftaid"]').attr('checked', 'checked');
	}
	//alert($('input[name="donor_giftaidchecked"]').val());
	$('form#donatePersonalStageForm').submit();
} // end of fn DonorGiftaidChecker

function DonationAddToCart()
{	//alert('add to cart');
	var url = 'ajax_cart.php?action=add_donation';
	var params = new Array();
	
	var country = $('select#donation-country').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	var donType = $('#donation-type').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input#donation-amount').val();
	var donAdminAmount = $('input#donation-admin').val();
	var donQuantity = $('input#donation-quantity').val();
	var zakat = $('input#donation_zakat').prop('checked')?"1":"0";

	if (country != undefined)
	{	params[params.length] = 'donation_country=' + encodeURIComponent(country);
	}
	if (project != undefined)
	{	params[params.length] = 'donation_project=' + encodeURIComponent(project);
	}
	if (project2 != undefined)
	{	params[params.length] = 'donation_project2=' + encodeURIComponent(project2);
	}
	if (donType != undefined)
	{	params[params.length] = 'donation_type=' + encodeURIComponent(donType);
	}
	if (currency != undefined)
	{	params[params.length] = 'donation_currency=' + encodeURIComponent(currency);
	}
	if (donAmount != undefined)
	{	params[params.length] = 'donation_amount=' + encodeURIComponent(donAmount);
	}
	if (donAdminAmount != undefined)
	{	params[params.length] = 'donation_admin=' + encodeURIComponent(donAdminAmount);
	}
	if (donQuantity != undefined)
	{	params[params.length] = 'donation_quantity=' + encodeURIComponent(donQuantity);
	}
	if (zakat != undefined)
	{	params[params.length] = 'donation_zakat=' + encodeURIComponent(zakat);
	}
	
	var action = function(rawresponse)
	{	//alert(rawresponse);
		response = jQuery.parseJSON(rawresponse);
		if ((response.test != undefined) && (response.test.length > 0))
		{	console.log(response.test);
		}
		if (response.success != undefined)
		{	$(".cart_modal_popup_inner").html(response.success);
			$(".cartModalPopup").jqmShow();
			fbq('track', 'Donate');
			//fbq('track', 'AddToCart');
		} else
		{	if ((response.error != undefined) && (response.error.length > 0))
			{	$(".cart_modal_popup_inner").html(response.error);
				$(".cartModalPopup").jqmShow();
			}
		}
		if (response.basket_desktop != undefined)
		{	$('.hrLinksCart').html(response.basket_desktop);
		}
		if (response.basket_mobile != undefined)
		{	$('.header_cartopener').html(response.basket_mobile);
		}
		DonationFomReset();
	}
	PM_Ajax_Post(url, params.join('&'), action);
} // end of fn DonationAddToCart

function DonationAddToCartNew()
{	//alert('add to cart');
	var url = 'ajax_cart.php?action=add_donation_new';
	var params = new Array();
	
	var donType = $('input[name="donation_type"]:checked').val();
	var country = $('select#donation-country').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input[name="donation_amount"]:checked').val();
	if (donAmount == 'other') {
		var donAmount = $('#other_amout_inner').val();
	}
	var cid = '';
	if ($('#cid').length > 0) {
		var cid = $('#cid').val();
	}

	var donQuantity = $('input#donation-quantity').val();
	var donAdminAmount = $('input#donation-admin').val();
	var project = $('select#donation-project').val();
	var project2 = $('select#donation-project2').val();
	if($('#donation_zakat').prop("checked") == true){
		var zakat = 1;
	}else{
		var zakat = 0;
	}

	if (country != undefined && country != '')
	{	params[params.length] = 'donation_country=' + encodeURIComponent(country);
	}
	if (donType != undefined)
	{	params[params.length] = 'donation_type=' + encodeURIComponent(donType);
	}
	if (project != undefined)
	{	params[params.length] = 'donation_project=' + encodeURIComponent(project);
	}
	if (project2 != undefined)
	{	params[params.length] = 'donation_project2=' + encodeURIComponent(project2);
	}
	if (currency != undefined)
	{	params[params.length] = 'donation_currency=' + encodeURIComponent(currency);
	}
	if (donAmount != undefined)
	{	params[params.length] = 'donation_amount=' + encodeURIComponent(donAmount);
	}
	if (donAdminAmount != undefined)
	{	params[params.length] = 'donation_admin=' + encodeURIComponent(donAdminAmount);
	}
	if (donQuantity != undefined)
	{	params[params.length] = 'donation_quantity=' + encodeURIComponent(donQuantity);
	}
	if (zakat != undefined)
	{	params[params.length] = 'donation_zakat=' + encodeURIComponent(zakat);
	}
	if (cid != undefined)
	{	params[params.length] = 'cid=' + encodeURIComponent(cid);
	}
	
	var action = function(rawresponse)
	{	//alert(rawresponse);
		response = jQuery.parseJSON(rawresponse);
		if (response.success != undefined)
		{	$('.step1').hide();
			$('.step2').show();
			$('input[name="donation_type"]').attr('disabled', true);
			$('.donation-support-country').text($("#donation-country option:selected").text());
			$('.donation-amount').text(donAmount);
			if (donType == 'monthly') {
				$('.bank-details').show();
				$('.card-details').hide();
			}else{
				$('.card-details').show();
				$('.bank-details').hide();
			}
		} else
		{	if ((response.error != undefined) && (response.error.length > 0))
			{	$(".cart_modal_popup_inner").html(response.error);
				$(".cartModalPopup").jqmShow();
			}
		}
	}
	PM_Ajax_Post(url, params.join('&'), action);
} // end of fn DonationAddToCartNew

function DonationAddToCartNewStep2()
{	//alert('add to cart');
	var url = 'ajax_cart.php?action=add_donation_new_step2';
	var params = new Array();
	
	// var donAmount = $('input[name="donation_amount"]:checked').val();
	// if (donAmount == 'other') {
	// 	var donAmount = $('#other_amout_inner').val();
	// }
	var donType = $('input[name="donation_type"]:checked').val();
	var donor_title = $('select#donor-title').val();
	var donor_firstname = $('#donor-firstname').val();
	var donor_lastname = $('#donor-lastname').val();
	var donor_email = $('#donor-email').val();
	var cardNumber = $('#cardNumber1').val();
	var expiry_month = $('#expiry_month').val();
	var expiry_year = $('#expiry_year').val();
	var cvv = $('#cvv').val();
	var dd_accountnumber = $('#dd-accountnumber').val();
	var dd_accountsortcode = $('#dd-accountsortcode').val();

	if (donor_title != undefined && donor_title != '')
	{	params[params.length] = 'donor_title=' + encodeURIComponent(donor_title);
	}
	if (donType != undefined)
	{	params[params.length] = 'donation_type=' + encodeURIComponent(donType);
	}
	if (donor_firstname != undefined)
	{	params[params.length] = 'donor_firstname=' + encodeURIComponent(donor_firstname);
	}
	if (donor_lastname != undefined)
	{	params[params.length] = 'donor_lastname=' + encodeURIComponent(donor_lastname);
	}
	if (donor_email != undefined)
	{	params[params.length] = 'donor_email=' + encodeURIComponent(donor_email);
	}
	if (donType == 'oneoff') {
		if (cardNumber != undefined)
		{	params[params.length] = 'cardNumber=' + encodeURIComponent(cardNumber);
		}
		if (expiry_month != undefined)
		{	params[params.length] = 'expiry_month=' + encodeURIComponent(expiry_month);
		}
		if (expiry_year != undefined)
		{	params[params.length] = 'expiry_year=' + encodeURIComponent(expiry_year);
		}
		if (cvv != undefined)
		{	params[params.length] = 'cvv=' + encodeURIComponent(cvv);
		}
	}else if (donType == 'monthly') {
		if (dd_accountnumber != undefined)
		{	params[params.length] = 'dd_accountnumber=' + encodeURIComponent(dd_accountnumber);
		}
		if (dd_accountsortcode != undefined)
		{	params[params.length] = 'dd_accountsortcode=' + encodeURIComponent(dd_accountsortcode);
		}
	}
	
	var action = function(rawresponse)
	{	//alert(rawresponse);
		response = jQuery.parseJSON(rawresponse);
		if (response.success != undefined)
		{	$('.step2').hide();
			$('.step3').show();
			$('.donation-support-country').text($("#donation-country option:selected").text());
			// $('.donation-amount').text(donAmount);
			// $('.donor_title_pop').val(donor_title);
			$('.donor_title_pop').val(donor_title).trigger('change');
			$(".donor_title_pop").prop('disabled', true);
			$('.donor_firstname_pop').val(donor_firstname);
			$('.donor_lastname_pop').val(donor_lastname);
			if (donType == 'monthly') {
				$('.gift-aid-monthly').show();
				$('.gift-aid-once').hide();
			}else{
				$('.gift-aid-once').show();
				$('.gift-aid-monthly').hide();
			}
		} else
		{	if ((response.error != undefined) && (response.error.length > 0))
			{	$(".cart_modal_popup_inner").html(response.error);
				$(".cartModalPopup").jqmShow();
			}
		}
	}
	PM_Ajax_Post(url, params.join('&'), action);
} // end of fn DonationAddToCartNewStep2

function backToStep1()
{
	$('.step3').hide();
	$('.step2').hide();
	$('input[name="donation_type"]').attr('disabled', false);
	$('.step1').show();
}

function backToStep2()
{
	$('.step3').hide();
	$('.step2').show();
}
function copy_billing_add(checkboxElem){
	console.log(checkboxElem.checked);
	if(checkboxElem.checked) {
		if ($('#home-address').is(':visible')) {
        	$('#home-address').val($('#billing-address').val());
        	$('#post-code-field1').val($('#billing-post-code').val());
		}else{
			$('#home-address-monthly').val($('#billing-address').val());
			$('#post-code-field2').val($('#billing-post-code').val());
		}
	}
	else{
    	if ($('#home-address').is(':visible')) {
    		$('#home-address').val('');
    		$('#post-code-field1').val('');
    	}else{
    		$('#home-address-monthly').val('');
    		$('#post-code-field2').val('');
    	}
    }
}
function claimGiftAidBox(checkboxElem){
	if(!checkboxElem.checked) {
        $('#donor_giftaid').val(0);
    }
}
function DonationTypeClick(element){
	if (element.value == 'monthly') {
		$('.month_content').show();
	}else{
		$('.month_content').hide();
	}
}

function claimGiftAid(){
	if ($('#oneoffGiftAidModal').is(':visible')) {
		$('#oneoffGiftAidModal').find('.error').remove();
		$('#donor_giftaid').val(0);
		if($("#claimGiftAid").prop('checked') == false){
			$('#oneoffGiftAidModal').find('.checkclaimGift').after('<div class="error" style="color: red;">Please tick this box.</div>');
			return false;
		}
		if ($('#home-address').val() == '') {
			$('#home-address').after('<div class="error" style="color: red;">Home address cannot be blank.</div>');
			return false;
		}
		if ($('#post-code-field1').val() == '') {
			$('#post-code-field1').after('<div class="error" style="color: red;">Post code cannot be blank.</div>');
			return false;
		}
		$('#donor_giftaid').val(1);
		$('#oneoffGiftAidModal').modal('hide');
	}else{
		$('#gift-aid-monthly').find('.error').remove();
		$('#donor_giftaid').val(0);
		if($("#claimGiftAid2").prop('checked') == false){
			$('#gift-aid-monthly').find('.checkclaimGift').after('<div class="error" style="color: red;">Please tick this box.</div>');
			return false;
		}
		if ($('#home-address-monthly').val() == '') {
			$('#home-address-monthly').after('<div class="error" style="color: red;">Home address cannot be blank.</div>');
			return false;
		}
		if ($('#post-code-field2').val() == '') {
			$('#post-code-field2').after('<div class="error" style="color: red;">Post code cannot be blank.</div>');
			return false;
		}
		$('#donor_giftaid').val(1);
		$('#gift-aid-monthly').modal('hide');
	}
	DonationAddToCartFinal();
}

function stripeResponseHandler(status, response) {
	// console.log(response);
    if (response.error) {
        // Enable the submit button
        $('#payBtn').removeAttr("disabled");
        $(".cart_modal_popup_inner").html(response.error.message);
	    $(".cartModalPopup").jqmShow();
	    return false;
    } else {
        var cardNumber1 = $("#cardNumber1");
        // Get token id
        var token = response.id;
        // Insert the token into the form
        $('#stripeToken').val(token);
        return true;
        // cardNumber1.after("<input type='hidden' name='stripeToken' id='stripeToken' value='" + token + "' />");
    }
}


async function stepTwo()
{
  return new Promise((resolve) =>
  {
    let stop = false;
    $('input[type=checkbox]').one('change', (event) =>
    {
      // Will only stop the loop when the checkbox is checked
		$('#gift-aid-monthly').on('hidden.bs.modal', function (e) {
			stop = true; 
		});
		
    });
    loop();

    // Continues looping until stop is set to true (i.e. input has been done)
    function loop()
    {
      if (stop === true)
      {
        resolve();
        return;
      }

      // Keeps checking if stop is true
      setTimeout(loop, 100);
    }
  });
}

var doner_check_counter = 0;

async function DonationAddToCartFinal(){
	if($('#donor_giftaid').val() == 0 && $('#donor-country').val() == 'GB'){
		if(doner_check_counter == 0){
		   doner_check_counter++;
		   $("#giftAidReminderModal").modal();
		   await stepTwo();
		}
   }
	jQuery('body').addClass('loaderclass');
    jQuery('#loaderclass').addClass('loaderclass_custom');
    
	var url = 'ajax_cart.php?action=add_donation_new_step3';
	var params = new Array();

	// Stripe.setPublishableKey('pk_test_0qLdOirAp6ECXSBdDJQ7MSNh00zqhKysUB');
	//Stripe.setPublishableKey('pk_live_9J7JiUJNtubpxNklNaLfTNVd');
	
	if($("input#stripe_publishable_key").length){
		Stripe.setPublishableKey($("input#stripe_publishable_key").val());
	}else{
		console.error('Cannot set Stripe Key.');
	}
	
	// var donAmount = $('input[name="donation_amount"]:checked').val();
	// if (donAmount == 'other') {
	// 	var donAmount = $('#other_amout_inner').val();
	// }
    

	var donType = $('input[name="donation_type"]:checked').val();
	var donor_firstname = $('#donor-firstname').val();
	var donor_lastname = $('#donor-lastname').val();
	var cardNumber = $('#cardNumber1').val();
	var expiry_month = $('#expiry_month').val();
	var expiry_year = $('#expiry_year').val();
	var cvv = $('#cvv').val();

	var billing_address    = $('#billing-address').val();
	var billing_post_code  = $('#billing-post-code').val();
	var donor_country      = $('#donor-country').val();
	var donor_giftaid      = $('#donor_giftaid').val();
	var donor_anon 	       = $('#donor_anon').prop("checked")?"1":"0";
	var donor_comment	   = $('#donor_comment').val();
	var crdonations_form   = $("#crdonations_form").length?1:0;
	var cid = $('#cid').val();
	
	var timeout = 100;
	console.log(donType);
	if (donType == 'oneoff') {
		timeout = 2500;
		// $('.donationadd_final').attr("disabled", "disabled");

	    await Stripe.createToken({
	        number: $('#cardNumber1').val(),
	        exp_month: $('#expiry_month').val(),
	        exp_year: $('#expiry_year').val(),
	        cvc: $('#cvv').val()
	    }, stripeResponseHandler);
		var home_address = $('#home-address').val();
		var home_post_code = $('#post-code-field1').val();
	}else if (donType == 'monthly') {
		var home_address   = $('#home-address-monthly').val();
		var home_post_code = $('#post-code-field2').val();
	}


	if (donor_country != undefined && donor_country != '')
	{	params[params.length] = 'donor_country=' + encodeURIComponent(donor_country);
	}
	if (donType != undefined)
	{	params[params.length] = 'donation_type=' + encodeURIComponent(donType);
	}
	if (billing_address != undefined)
	{	params[params.length] = 'billing_address=' + encodeURIComponent(billing_address);
	}
	if (billing_post_code != undefined)
	{	params[params.length] = 'billing_post_code=' + encodeURIComponent(billing_post_code);
	}
	if (donor_giftaid != undefined)
	{	params[params.length] = 'donor_giftaid=' + encodeURIComponent(donor_giftaid);
	}
	if (donor_anon != undefined)
	{	params[params.length] = 'donor_anon=' + encodeURIComponent(donor_anon);
	}
	if (donor_comment != undefined)
	{	params[params.length] = 'donor_comment=' + encodeURIComponent(donor_comment);
	}
	if (home_address != undefined)
	{	params[params.length] = 'home_address=' + encodeURIComponent(home_address);
	}
	if (home_post_code != undefined)
	{	params[params.length] = 'home_post_code=' + encodeURIComponent(home_post_code);
	}
	if (crdonations_form != undefined)
	{	params[params.length] = 'crdonations_form=' + encodeURIComponent(crdonations_form);
	}
	
	if (cid != undefined)
	{
		url += '&cid='+cid;
	}

	
	var action = function(rawresponse)
	{	//alert(rawresponse);
		response = jQuery.parseJSON(rawresponse);
		var siteurl = $('#siteurl').val();
		if (response.success != undefined)
		{
			
            if (donType == 'oneoff') {
	            var alldetail = JSON.stringify(response.alldetail);
	            var owner = donor_firstname+' '+donor_lastname;
	            var stripeToken = $('#stripeToken').val();
	            $.ajax({
	                type: "POST",
	                url: "/../ajax_stripepayment.php",
	                data: {alldetail: alldetail,owner:owner,cardNumber:cardNumber,cvv:cvv,expiry_day:expiry_month,expiry_year:expiry_year,sessionccard:response.cart_session_name,stripeToken:stripeToken,donType:donType},
	                success: function( msg ) {
	                	// console.log(msg);
	                	// return false;
	                    jQuery('body').removeClass('loaderclass');
	                    jQuery('#loaderclass').removeClass('loaderclass_custom');
	                    var obj = JSON.parse(msg);
	                    if(obj.status == '1'){
	                       window.location.href=siteurl+"?token="+obj.thankyou_token;
	                    }else{
	                        $(".cart_modal_popup_inner").html(obj.message);
							$(".cartModalPopup").jqmShow();
							doner_check_counter=0; // reset modal window flag
	                    }
	                },
	                
	            });
            }else if (donType == 'monthly') {
            	$.ajax({
	                type: "POST",
	                url: "/../ajax_cart.php?action=makemonthpayment",
	                data: {alldetail: alldetail,sessionccard:response.cart_session_name},
	                success: function( msg ) {
	                    jQuery('body').removeClass('loaderclass');
	                    jQuery('#loaderclass').removeClass('loaderclass_custom');
	                    var obj = JSON.parse(msg);
						
						if(obj.success == 1){
						   setTimeout(function(){
						   		window.location.href = siteurl+"?token="+obj.thankyou_token;
						   },3000);
							
							$(".cart_modal_popup_inner").html(obj.message);
						}else{
							$(".cart_modal_popup_inner").html(obj.message);
							$(".cartModalPopup").jqmShow();
							$('.step3').hide();
							$('.step2').hide();
							$('input[name="donation_type"]').attr('disabled', false);
							$('.step1').show();
							doner_check_counter=0; // reset modal window flag
						}
	                },
	                
	            });
            }

			// if (donType == 'oneoff') {
			// 	var alldetail = JSON.stringify(response.alldetail);
	  //           var owner = donor_firstname+' '+donor_lastname;
	  //           $.ajax({
	  //               type: "POST",
	  //               url: "../../ajax_onlinewordpay.php",
	  //               data: {alldetail: alldetail,owner:owner,cardNumber:cardNumber,cvv:cvv,expiry_day:expiry_month,expiry_year:expiry_year,sessionccard:response.cart_session_name},
	  //               success: function( msg ) {
	  //                   jQuery('body').removeClass('loaderclass');
	  //                   jQuery('#loaderclass').removeClass('loaderclass_custom');
	  //                   var obj = JSON.parse(msg);
	  //                   if(obj.status == '1'){
	  //                      window.location.href=siteurl;
	  //                   }else{
	  //                       $(".cart_modal_popup_inner").html(obj.message);
	  //                       $(".cartModalPopup").jqmShow();
	  //                   }
	  //               },
	                
	  //           });
			// }else if (donType == 'monthly') {
			// 	$.ajax({
	  //               type: "POST",
	  //               url: "../../ajax_cart.php?action=makemonthpayment",
	  //               data: {alldetail: alldetail,sessionccard:response.cart_session_name},
	  //               success: function( msg ) {
	  //                   jQuery('body').removeClass('loaderclass');
	  //                   jQuery('#loaderclass').removeClass('loaderclass_custom');
	  //                   var obj = JSON.parse(msg);
   //                      $(".cart_modal_popup_inner").html(obj.message);
   //                      $(".cartModalPopup").jqmShow();
	  //               },
	                
	  //           });
			// }
		} else
		{	
			if ((response.error != undefined) && (response.error.length > 0))
			{	$(".cart_modal_popup_inner").html(response.error);
				$(".cartModalPopup").jqmShow();
				jQuery('body').removeClass('loaderclass');
				jQuery('#loaderclass').removeClass('loaderclass_custom');
			}
		}
	}
	setTimeout(function(){
		PM_Ajax_Post(url, params.join('&'), action);
	}, timeout);
		

}

function CRDonationAddToCart(campaignid)
{	//alert('add to cart');
	var url = 'ajax_cart.php?action=add_crdonation&cid=' + String(campaignid);
	var params = new Array();
	
	var country = $('select#donation-country').val();
	var currency = $('#donation_currency').val();
	var donAmount = $('input[name="donation_amount"]').val();
	var zakat = $('select#donation_zakat').val();
	var donor_anon = $('select[name="donor_anon"]').val();
	var donor_showto = $('select[name="donor_showto"]').val();
	var donor_comment = $('textarea[name="donor_comment"]').val();

	if (country != undefined)
	{	params[params.length] = 'donation_country=' + encodeURIComponent(country);
	}
	if (currency != undefined)
	{	params[params.length] = 'donation_currency=' + encodeURIComponent(currency);
	}
	if (donAmount != undefined)
	{	params[params.length] = 'donation_amount=' + encodeURIComponent(donAmount);
	}
	if (zakat != undefined)
	{	params[params.length] = 'donation_zakat=' + encodeURIComponent(zakat);
	}
	if (donor_anon != undefined)
	{	params[params.length] = 'donor_anon=' + encodeURIComponent(donor_anon);
	}
	if (donor_showto != undefined)
	{	params[params.length] = 'donor_showto=' + encodeURIComponent(donor_showto);
	}
	if (donor_comment != undefined)
	{	params[params.length] = 'donor_comment=' + encodeURIComponent(donor_comment);
	}
	
	var action = function(rawresponse)
	{	//alert(rawresponse);
		response = jQuery.parseJSON(rawresponse);
		if ((response.test != undefined) && (response.test.length > 0))
		{	console.log(response.test);
		}
		if (response.success != undefined)
		{	$(".cart_modal_popup_inner").html(response.success);
			$(".cartModalPopup").jqmShow();
			$('input[name="donation_amount"]').val('0');
			$('input[name="donor_comment"]').val('');
			$('#donateCurrencyContainer').addClass('hidden');
			fbq('track', 'Donate');
			//fbq('track', 'AddToCart');
		} else
		{	if ((response.error != undefined) && (response.error.length > 0))
			{	$(".cart_modal_popup_inner").html(response.error);
				$(".cartModalPopup").jqmShow();
			}
		}
		if (response.basket_desktop != undefined)
		{	$('.hrLinksCart').html(response.basket_desktop);
		}
		if (response.basket_mobile != undefined)
		{	$('.header_cartopener').html(response.basket_mobile);
		}
		DonationFomReset();
	}
	PM_Ajax_Post(url, params.join('&'), action);
} // end of fn CRDonationAddToCart