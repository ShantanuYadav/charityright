
var datePickerDivID = "datepicker";
var iFrameDivID = "datepickeriframe";

var dayArrayShort = new Array('Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa');
var dayArrayMed = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
var dayArrayLong = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
var monthArrayShort = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
var monthArrayMed = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
var monthArrayLong = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

//<a onclick="displayDatePicker('StartDate', 2001);"><img /></a>
//where there are select boxes for dStartDate, mStartDate and yStartdate
function displayDatePicker(dateFieldName, defYear)
{	displayBelowThisObject = document.getElementsByName ("y" + dateFieldName).item(0);

	var x = displayBelowThisObject.offsetLeft;
	var y = displayBelowThisObject.offsetTop + displayBelowThisObject.offsetHeight ;

	// deal with elements inside tables and such
	var parent = displayBelowThisObject;
	while (parent.offsetParent)
	{	parent = parent.offsetParent;
		x += parent.offsetLeft;
		y += parent.offsetTop ;
	}
 
	drawDatePicker(dateFieldName, x, y, defYear);
} // end of fn displayDatePicker

/** Draw the datepicker object (which is just a table with calendar elements) at the
specified x and y coordinates, using the targetDateField object as the input tag
that will ultimately be populated with a date.
This function will normally be called by the displayDatePicker function. */
function drawDatePicker(fieldName, x, y, defPickerYear)
{	
	var ytargetDateField = document.getElementsByName ("y" + fieldName).item(0);
	var dtargetDateField = document.getElementsByName ("d" + fieldName).item(0);
	var mtargetDateField = document.getElementsByName ("m" + fieldName).item(0);
 
	var dt = new Date(ytargetDateField.value, mtargetDateField.value, dtargetDateField.value);

	// the datepicker table will be drawn inside of a <div> with an ID defined by the
	// global datePickerDivID variable. If such a div doesn't yet exist on the HTML
	// document we're working with, add one.
	if (!document.getElementById(datePickerDivID))
	{	var newNode = document.createElement("div");
		newNode.setAttribute("id", datePickerDivID);
		newNode.setAttribute("class", "dpDiv");
		newNode.setAttribute("style", "visibility: hidden;");
		document.body.appendChild(newNode);
	}

	// move the datepicker div to the proper x,y coordinate and toggle the visiblity
	var pickerDiv = document.getElementById(datePickerDivID);
	pickerDiv.style.position = "absolute";
	pickerDiv.style.left = x + "px";
	pickerDiv.style.top = y + "px";
	pickerDiv.style.visibility = (pickerDiv.style.visibility == "visible" ? "hidden" : "visible");
	pickerDiv.style.display = (pickerDiv.style.display == "block" ? "none" : "block");
	pickerDiv.style.zIndex = 10000;

	// draw the datepicker table
	if (ytargetDateField.value > 0)
	{	year = ytargetDateField.value;
	} else
	{	year = defPickerYear;
	}
	refreshDatePicker(fieldName, year, mtargetDateField.value - 1, dtargetDateField.value);
} // end of fn drawDatePicker

/**This is the function that actually draws the datepicker calendar.*/
function refreshDatePicker(dateFieldName, year, month, day)
{	// if no arguments are passed, use today; otherwise, month and year required (if a day is passed, it will be highlighted later)
	var thisDay = new Date();
	
	if (year > 0)
	{	if (month >= 0)
		{	thisDay = new Date(year, month, 1);
		} else
		{	day = thisDay.getDate();
			month = thisDay.getMonth();
			thisDay = new Date(year, month, 1);
		}
	} else
	{	day = thisDay.getDate();
		thisDay.setDate(1);
	}

	// the calendar will be drawn as a table
	// you can customize the table elements with a global CSS style sheet,
	// or by hardcoding style and formatting elements below
	var crlf = "\r\n";
	var TABLE = "<table cols=7 class='dpTable'>" + crlf;
	var xTABLE = "</table>" + crlf;
	var TR = "<tr class='dpTR'>";
	var TR_title = "<tr class='dpTitleTR'>";
	var TR_days = "<tr class='dpDayTR'>";
	var TR_todaybutton = "<tr class='dpTodayButtonTR'>";
	var xTR = "</tr>" + crlf;
	var TD = "<td class='dpTD' onMouseOut='this.className=\"dpTD\";' onMouseOver=' this.className=\"dpTDHover\";' ";    // leave this tag open, because we'll be adding an onClick event
	var TD_title = "<td colspan=3 class='dpTitleTD'>";
	var TD_buttons = "<td class='dpButtonTD'>";
	var TD_buttons2 = "<td class='dpButtonTD' colspan='2'>";
	var TD_todaybutton = "<td colspan=7 class='dpTodayButtonTD'>";
	var TD_days = "<td class='dpDayTD'>";
	var TD_selected = "<td class='dpDayHighlightTD' onMouseOut='this.className=\"dpDayHighlightTD\";' onMouseOver='this.className=\"dpTDHover\";' ";    // leave this tag open, because we'll be adding an onClick event
	var xTD = "</td>" + crlf;
	var DIV_title = "<div class='dpTitleText'>";
	var DIV_selected = "<div class='dpDayHighlight'>";
	var xDIV = "</div>";
 
	// start generating the code for the calendar table
	var html = TABLE;

	// this is the title bar, which displays the month and the buttons to
	// go back to a previous month or forward to the next month
	html += TR_title;
	html += TD_buttons2 + getYearButtonCode(dateFieldName, thisDay, -1, "&lt;&lt;") + "&nbsp;" + getButtonCode(dateFieldName, thisDay, -1, "&lt;") + xTD;
	html += TD_title + DIV_title + monthArrayMed[thisDay.getMonth()] + " " + thisDay.getFullYear() + xDIV + xTD;
	html += TD_buttons2 + getButtonCode(dateFieldName, thisDay, 1, "&gt;") + "&nbsp;" + getYearButtonCode(dateFieldName, thisDay, 1, "&gt;&gt;") + xTD;
	html += xTR;

	// this is the row that indicates which day of the week we're on
	html += TR_days;
	for(i = 0; i < dayArrayShort.length; i++)
	{	html += TD_days + dayArrayShort[i] + xTD;
	}
	html += xTR;
 
	// now we'll start populating the table with days of the month
	html += TR;

	// first, the leading blanks
	for (i = 0; i < thisDay.getDay(); i++)
	{	html += TD + "&nbsp;" + xTD;
	}

	// now, the days of the month
	do
	{	dayNum = thisDay.getDate();
		TD_onclick = " onclick=\"updateDateField('" + dateFieldName + "', '" + thisDay.getFullYear() + "', '" + (thisDay.getMonth() + 1) + "', '" + thisDay.getDate() + "');\">";

		if (dayNum == day)
		{	html += TD_selected + TD_onclick + DIV_selected + dayNum + xDIV + xTD;
		} else
		{	html += TD + TD_onclick + dayNum + xTD;
		}

		// if this is a Saturday, start a new row
		if (thisDay.getDay() == 6)
		{	html += xTR + TR;
		}

		// increment the day
		thisDay.setDate(thisDay.getDate() + 1);
	} while (thisDay.getDate() > 1)

	// fill in any trailing blanks
	if (thisDay.getDay() > 0)
	{	for (i = 6; i > thisDay.getDay(); i--)
		html += TD + "&nbsp;" + xTD;
	}
	html += xTR;

	// add a button to allow the user to easily return to today, or close the calendar
	var today = new Date();
	var todayString = "Today is " + dayArrayMed[today.getDay()] + ", " + monthArrayMed[today.getMonth()] + " " + today.getDate();
	html += TR_todaybutton + TD_todaybutton;
	html += "<button class='dpTodayButton' onClick='refreshDatePicker(\"" + dateFieldName + "\");'>this month</button>&nbsp;";
	html += "<button class='dpTodayButton' onClick='updateDateField(\"" + dateFieldName + "\");'>close</button>";
	html += xTD + xTR;

	// and finally, close the table
	html += xTABLE;

	document.getElementById(datePickerDivID).innerHTML = html;
} // end of fn refreshDatePicker

//Convenience function for writing the code for the buttons that bring us back or forward a month.
function getButtonCode(dateFieldName, dateVal, adjust, label)
{	var newMonth = (adjMonth = dateVal.getMonth() + adjust) % 12;
	var newYear = dateVal.getFullYear() + parseInt(adjMonth / 12);
	if (newMonth < 0)
	{	newMonth += 12;
		newYear += -1;
	}

	return "<button class='dpButton' onClick='refreshDatePicker(\"" + dateFieldName + "\", " + newYear + ", " + newMonth + ");'>" + label + "</button>";
} // end of fn getButtonCode

//Convenience function for writing the code for the buttons that bring us back or forward a month.
function getYearButtonCode(dateFieldName, dateVal, adjust, label)
{	return "<button class='dpButton' onClick='refreshDatePicker(\"" + dateFieldName + "\", " + (dateVal.getFullYear() + adjust) + ", " + dateVal.getMonth() + ");'>" + label + "</button>";
} // end of fn getYearButtonCode

function updateDateField(dateFieldName, yString, mString, dString)
{
	var ytargetDateField = document.getElementsByName ("y" + dateFieldName).item(0);
	var mtargetDateField = document.getElementsByName ("m" + dateFieldName).item(0);
	var dtargetDateField = document.getElementsByName ("d" + dateFieldName).item(0);
	if (yString)
	{	ytargetDateField.value = yString;
		mtargetDateField.value = mString;
		dtargetDateField.value = dString;
	}

	var pickerDiv = document.getElementById(datePickerDivID);
	pickerDiv.style.visibility = "hidden";
	pickerDiv.style.display = "none";

	dtargetDateField.focus();

} // end of fn updateDateField
