$( document ).ready(function() {

	$('.main-select2').select2({
		dropdownParent: '.donation-tabs'
	});
	$('.main-select3').select2({
		dropdownParent: '.donation-modal'
	});
	$("#donor-country").on("change", function () { 
		var country = $(this).val();
		if (country == 'GB') {
			$('.gift-aid-action').show();
		}else{
			$('.gift-aid-action').hide();
		}
	});
	// $('#donor-country').on("select2:select", function(e) { 
	// 	var country = $(this).val();
	// 	if (country == 'GB') {
	// 		$('.gift-aid-action').show();
	// 	}else{
	// 		$('.gift-aid-action').hide();
	// 	}
 //    });

	var stopOpening = false;
	selectinput = function() {
	    // stopOpening = true;
	    $(".selectinput").on("keyup", function() {
	        var amount = this.value;
	        $('.custom_amount').val(amount);
		});
	}

	$('.main-select4').select2({
		dropdownParent: '.middle-part',
		templateResult: function (item) {
			// console.log(item);
			if (item.text == 'Other') {
				var $result = $('<span class="label">'+item.text+'</span>');
			}else{
				var $result = $('<span class="label">'+item.text+'</span>');
			}
			return $result;
		},
		templateSelection: function (item) {
			if (item.text == 'Other') {
				var $result = $('<span  class="label">'+item.text+' &#163;</span><input type="number" class="selectinput" placeholder="Enter Amount" onclick="selectinput()">');
				stopOpening = true;
			}else{
				var $result = $('<span  class="label">'+item.text+'</span>');
				stopOpening = false;
			}
			return $result;
		}
	}).on({
	    "select2:unselecting": function(e) {
	        console.log("unselecting");
	        if (e.params.args.originalEvent.target.className === 'input') {
	            e.preventDefault();
	        }
	    },
	    "select2:opening": function(e) {
	        if(stopOpening == true){
	            stopOpening = false;
	            e.preventDefault();
	            // e.stopImmediatePropagation();
	            e.stopPropagation();
	        }
	    }
	});
	
	new WOW().init();
	$("#other-amount-main").click(function(){
		$("#other_amount").toggle();
	});
	$(".donation-amount").click(function(){
		if($('input:radio[name=donation_amount]:checked').val() != "other"){
			$('#other_amount').hide();
		}
	});
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1){  
		    $('header').addClass("sticky");
		  }
		  else{
		    $('header:not(.keepsticky)').removeClass("sticky");
		  }
		});
	$('.hero-tabs').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		dots: false,
		arrows: false,
		prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left'></i></button>",
        nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right'></i></button>"
	});
	$('.turkey-part').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		speed: 1000,
		autoplaySpeed: 4000,
		dots: false,
		arrows: true,
		prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left'></i></button>",
        nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right'></i></button>",
        responsive: [
				    {
				      breakpoint: 1024,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1,
				        infinite: true
				      }
				    },
				    {
				      breakpoint: 991,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1,
				        dots: true,
						arrows: false
				      }
				    },
				    {
				      breakpoint: 767,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1,
				        dots: true,
						arrows: false
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1,
				        dots: true,
						arrows: false
				      }
				    }
  					]
	});
	$('.news-views-articles').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		dots: false,
		arrows: true,
		prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left'></i></button>",
        nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right'></i></button>",
       	 responsive: [
				    {
				      breakpoint: 1024,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1,
				        infinite: true
				      }
				    },
				    {
				      breakpoint: 992,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1,
				        dots: true,
						arrows: false
				      }
				    },
				    {
				      breakpoint: 767,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1,
				        dots: true,
						arrows: false
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1,
				        dots: true,
						arrows: false
				      }
				    }
  					]
	});

});
