$(function() {
    
   
    var owner = $('#owner');
    var cardNumber = $('#cardNumber');
    var cardNumberField = $('#card-number-field');
    var CVV = $("#cvv");
    var cvvs = $("#cvv");
    var mastercard = $("#mastercard");
    var confirmButton = $('#confirm-purchase');
    var visa = $("#visa");
    var amex = $("#amex");
    var alldetail = $("#alldetail");
    var expiry_day = $("#expiry_day");
    var expiry_year = $("#expiry_year");
    var sessionccard = $("#sessionccard");
    var captcha = $('textarea[id="g-recaptcha-response"]').val();
    var siteurl = $('#siteurl').val();

    // Use the payform library to format and validate
    // the payment fields.

    // cardNumber.payform('formatCardNumber');
    CVV.payform('formatCardCVC');


    // cardNumber.keyup(function() {

    //     amex.removeClass('transparent');
    //     visa.removeClass('transparent');
    //     mastercard.removeClass('transparent');

    //     if ($.payform.validateCardNumber(cardNumber.val()) == false) {
    //         cardNumberField.addClass('has-error');
    //     } else {
    //         cardNumberField.removeClass('has-error');
    //         cardNumberField.addClass('has-success');
    //     }

    //     if ($.payform.parseCardType(cardNumber.val()) == 'visa') {
    //         mastercard.addClass('transparent');
    //         amex.addClass('transparent');
    //     } else if ($.payform.parseCardType(cardNumber.val()) == 'amex') {
    //         mastercard.addClass('transparent');
    //         visa.addClass('transparent');
    //     } else if ($.payform.parseCardType(cardNumber.val()) == 'mastercard') {
    //         amex.addClass('transparent');
    //         visa.addClass('transparent');
    //     }
    // });

    function stripeResponseHandler(status, response) {
        // console.log(response);
        if (response.error) {
            // Enable the submit button
            $('#error-msg').html(response.error.message);
            // $(".cart_modal_popup_inner").html(response.error.message);
            // $(".cartModalPopup").jqmShow();
            return false;
        } else {
            var cardNumber1 = $("#cardNumber1");
            // Get token id
            var token = response.id;
            // Insert the token into the form
            $('#stripeToken').val(token);
            return true;
            // cardNumber1.after("<input type='hidden' name='stripeToken' id='stripeToken' value='" + token + "' />");
        }
    }

    confirmButton.click(function(e) {

        e.preventDefault();
        // Stripe.setPublishableKey('pk_test_0qLdOirAp6ECXSBdDJQ7MSNh00zqhKysUB');
        Stripe.setPublishableKey('pk_live_9J7JiUJNtubpxNklNaLfTNVd');
        Stripe.createToken({
            number: $('#cardNumber').val(),
            exp_month: $('#expiry_day').val(),
            exp_year: $('#expiry_year').val(),
            cvc: $('#cvv').val()
        }, stripeResponseHandler);

        var isCardValid = $.payform.validateCardNumber(cardNumber.val());
        var isCvvValid = $.payform.validateCardCVC(CVV.val());
        $('#owner-error').html('');
        $('#donor-error').html('');
        $('#cvv-error').html('');
        if(owner.val().length < 5){
            //alert("Wrong owner name");
            $('#owner-error').html('CardHolder name is invalid');
        } else if (!isCardValid) {
            $('#donor-error').html('Card number is invalid');
            //alert("Wrong card number");
        } else if (!isCvvValid) {
            $('#cvv-error').html('CVV is invalid');
            //alert("Wrong CVV");
        } else {
            console.log('Hii');
            jQuery('body').addClass('loaderclass');
            jQuery('#loaderclass').addClass('loaderclass_custom');
            setTimeout(function(){
                var stripeToken = $('#stripeToken').val();
                $.ajax({
                    type: "POST",
                    // url: "../../ajax_onlinewordpay.php",
                    url: "../../ajax_stripepayment.php",
                    data: {alldetail: alldetail.val(),owner:owner.val(),cardNumber:cardNumber.val(),cvv:cvvs.val(),expiry_day:expiry_day.val(),expiry_year:expiry_year.val(),sessionccard:sessionccard.val(),stripeToken:stripeToken},
                    success: function( msg ) {
                        jQuery('body').removeClass('loaderclass');
                        jQuery('#loaderclass').removeClass('loaderclass_custom');
                        var obj = JSON.parse(msg);
                        if(obj.status == '1'){
                           window.location.href=siteurl;
                        }else{
                            $('#error-msg').html(obj.message);
                        }
                    },
                    
                });
            }, 3000);
            // Everything is correct. Add your form submission code here.
            //alert("Everything is correct");
        }
    });
});
