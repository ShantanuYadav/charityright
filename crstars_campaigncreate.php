<?php 
require_once('init.php');

class CRStarsRegisterPage extends MyCRStarsPage
{	private $campaign;
	private $team;

	public function __construct()
	{	parent::__construct('create');
	} // end of fn __construct

	protected function BaseConstructFunctions()
	{	$this->campaign = new Campaign();
		$this->mypagename = 'create';
		
		if ($_GET['teamid'] || $_GET['teamslug'])
		{	$this->team = new Campaign($_GET['teamid'], $_GET['teamslug']);
			if (!$this->team->CanBeJoined($this->camp_customer->id))
			{	unset($_GET['teamid'], $_GET['teamslug'], $this->team);
			}
		}
		
		if (isset($_POST['campname']))
		{	$data = $_POST;
			$data['visible'] = 1;
			$data['enabled'] = 1;
			$saved = $this->campaign->Create($data, $_FILES['campimage'], $this->camp_customer->id, $this->team);
			if ($this->campaign->id)
			{	$this->campaign->SendCampaignCreateEmail();
			}
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}
		
		if ($this->campaign->id)
		{	header('location: ' . $this->campaign->Link());
			exit;
		}
	
	} // end of fn BaseConstructFunctions
	
	protected function SetFBMeta()
	{	parent::SetFBMeta();
		$this->fb_meta['title'] = 'Start a Campaign at Charity Right';
		$this->fb_meta['url'] = SITE_SUB . '/crstarts_register.php';
		$this->fb_meta['image'] = CIT_FULLLINK . 'img/template/logo_og.png';
	} // end of fn SetFBMeta
	
	function MemberBodyContent()
	{	ob_start();
		echo '<div class="container crstarsRegister"><div class="container_inner">';
		echo '<h1 class="crstarsMyHeader">Create a ';
		if ($this->team->id)
		{	echo 'Campaign to Raise Money for ' . $this->team->FullTitle();
		} else
		{	echo 'New Campaign';
		}
		echo '</h1>', $this->NewCampaignForm($_POST, $this->team), '</div></div>';
		return ob_get_clean();
	} // end of fn MemberBodyContent
	
} // end of class CRStarsRegisterPage

$page = new CRStarsRegisterPage();
$page->Page();
?>