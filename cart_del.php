<?php
include_once('init.php');

class CartDelPage extends CartPage
{	protected $deloptions;
	protected $cartPageName = 'Delivery Options';

	public function __construct() // constructor
	{	parent::__construct();
		if (!$this->PlateDocsDone())
		{	header('location: ' . SITE_URL . 'cart_docs.php');
			exit;
		}
		if (!$this->customer->details['countrycode'])
		{	header('location: ' . SITE_URL . 'cart_addr.php');
			exit;
		}
		$this->deloptions = new DelOptions($this->customer->details['countrycode'], true);
		
		if ($_POST['deloption'] && $this->deloptions->deloptions[$_POST['deloption']])
		{	$this->SaveDelOption($_POST['deloption']);
		}
		
	} // end of fn __construct, constructor
	
	public function SaveDelOption($deloption = 0)
	{	if ($_SESSION['cart_del'] = (int)$deloption)
		{	header('location: ' . SITE_URL . 'cart_confirm.php');
			exit;
		}
	} // end of fn SaveDelOption
	
	function CartBodyHolder()
	{	ob_start();
		if (!$this->customer->id)
		{	echo $this->LoginContent();
		}
		echo parent::CartBodyHolder();
		return ob_get_clean();
	} // end of fn MemberBody
	
	protected function CartBody()
	{	ob_start();
		echo $this->CartSummaryHeader();
		if ($this->customer->id)
		{	echo '<div class="cartProcessPart"><h2 class="heading">Choose your delivery method</h2>', $this->DeliveryOptionsForm(), '</div>';
		}
		return ob_get_clean();
	} // end of fn CartBody

	public function DeliveryOptionsForm()
	{	ob_start();
		echo '<form id="delform" method="post" action="', $_SERVER["SCRIPT_NAME"], '">';
		foreach ($this->deloptions->deloptions as $deloption)
		{	
			echo '<p><label>', stripslashes($deloption->details['deldesc']), '<br /><span>&pound;', number_format($deloption->details['delprice'], 2), '</span></label><input type="radio" name="deloption" value="', $deloption->id, '" ', (($deloption->id == $_SESSION['cart_del']) || (!$_SESSION['cart_del'] && ($deloption->details['deldefault'] || (!$deloption->details['deldefault'] && !$delcount++)))) ? 'checked="checked" ' : '', '/></p><div class="clear"></div>';
		}
		echo '<p><input type="submit" class="submit" value="Pick delivery option" /></p><div class="clear"></div></form>';
		return ob_get_clean();
	} // end of fn DeliveryOptionsForm
	
} // end of defn CartDelPage

$page = new CartDelPage();
$page->Page();
?>