<?php 
require_once('init.php');

class PreviousEventsListPage extends EventsPage
{	
	public function __construct()
	{	parent::__construct('previous-events');
	} // end of fn __construct

	protected function GetPreviousLink()
	{	if (($events_page = new PageContent('events')) && $events_page->id)
		{	return array('label'=>'Events Coming up', 'link'=>$events_page->Link());
		}
	} // end of fn GetPreviousLink

	protected function GetEventDates()
	{	$tables = array('eventdates'=>'eventdates');
		$fields = array('eventdates.*');
		$where = array('eventdates.endtime<="' . $this->datefn->SQLDateTime() . '"', 'eventdates.live=1');
		$orderby = array('endtime'=>'eventdates.endtime DESC');
		return array_slice($this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables ,$fields ,$where ,$orderby), 'edid', true), 0, 11, true);
	} // end of fn GetEventDates

} // end of class PreviousEventsListPage

$page = new PreviousEventsListPage();
$page->Page();
?>