<?php
require_once('init.php');

class EventDatePage extends EventsBasePage
{	protected $eventdate;
	protected $event;
	protected $venue;

	public function __construct()
	{	parent::__construct('events');
		$this->js['events'] = 'events.js';
	} // end of fn __construct

	protected function BaseConstructFunctions()
	{	$this->eventdate = new EventDate($_GET['edid']);
		$this->event = new Event($this->eventdate->event);
		$this->venue = new EventVenue($this->eventdate->venue);
		$this->canonical_link = $this->eventdate->Link();
		$this->title = $this->InputSafeString($this->event->details['eventname']) . ' - ' . date('M Y', strtotime($this->eventdate->details['starttime'])) . ' at ' . $this->InputSafeString($this->venue->details['venuename']);
	} // end of fn BaseConstructFunctions

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		$this->fb_meta['title'] = $this->title;
		if ($image = $this->event->GetImageSRC('og'))
		{	$this->fb_meta['image'] = $image;
		}
		$this->fb_meta['url'] = $this->eventdate->Link();
		if ($this->event->details['metadesc'])
		{	$this->fb_meta['description'] = $this->InputSafeString($this->event->details['metadesc']);
		}
	} // end of fn SetFBMeta

	public function GetMetaDesc()
	{	return $this->event->details['metadesc'];
	} // end of fn GetMetaDesc

	function MainBodyContent()
	{
		echo '<div class="container event-page-header-container"><div class="container_inner"><h1 class="page_heading">'.$this->page->PageTitleDisplay().'</h1><h2 class="page_sub_heading">Join our fight against hunger by attending one of our regular events.</h2></div></div>';
		echo '<div class="container eventDetailsBody"><div class="container_inner"><div class="edbRight"><h1>', $this->InputSafeString($this->event->details['eventname']), '</h1><h2>', $this->eventdate->DatesString(), ' at ', $this->venue->LocationString(), '</h2>', stripslashes($this->event->details['description']);
		if ($tickets = $this->eventdate->GetTicketTypes(array('live'=>true)))
		{	echo '<div class="edlTickets"><ul><h3>Choose your ticket</h3>';
			foreach ($tickets as $ticket_row)
			{	$ticket = new TicketType($ticket_row);
				echo '<li><a onclick="OpenTicketBook(', $ticket->id, ');">', $this->InputSafeString($ticket->details['ttypename']), '</a></li>';
			}
			echo '</ul></div><div class="clear"></div><script type="text/javascript">$().ready(function(){$("body").append($(".jqmWindow"));$(".edTicketPopup").jqm();});</script><div class="jqmWindow edTicketPopup"><a href="#" class="jqmClose">Close</a><div class="edTicketPopupInner"></div></div>';
		}
		echo '</div><div class="edbLeft"';
		if ($image = $this->event->GetImageSRC('full'))
		{	echo ' style="background-image: url(\'', $image, '\');"';
		}
		echo '></div><div class="clear"></div></div></div>';
		//$this->VarDump($this->eventdate);
		//unset($_SESSION['events']['currency']);
		if (SITE_TEST)
		{	$this->VarDump($_SESSION['events']);
		}

	} // end of fn MemberBody

} // end of class EventDatePage

$page = new EventDatePage();
$page->Page();
?>