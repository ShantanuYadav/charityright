<?php
require_once('init.php');
class CRStarsStartPage extends CRStarsPage {
	public function __construct()
	{   parent::__construct();

	//	if ($this->camp_customer->id)
	//	{	header('location: ' . ($this->team->id ? $this->team->JoinLink() : (SITE_SUB . '/my-cr-stars/')));
	//		exit;
	//	}
		$this->canonical_link = SITE_SUB . '/cr-stars/campaign-ended/';
		$this->stages[1] = array('heading'=>'Start a Campaign');
		$this->stages[2] = array('heading'=>'Campaign Details');
		$this->stages[3] = array('heading'=>'Share Campaign');
	} // end of fn __construct

	public function MainBodyContent()
	{
		echo '<div class="container crstarsRegisterHeader crstarsStartHeader"><div class="container_inner"><h1><span class="register_heading_white">Start</span><br />Campaigning<br />and Help<br /><span class="register_heading_yellow">End</span><br />Hunger</h1></div></div>';
		echo $this->RegisterBreadcrumbs();
		echo '<div class="container crstarsStartContent"><div class="container_inner">';
		echo '<h2>This campaign has ended but you can have a real impact on the lives of people living in poverty. Make a difference that will last them a lifetime. Become a Charity Right Star!</h2>';
		if ($this->camp_customer->id)
		{	echo '<p><a href="'. SITE_SUB .'/my-cr-stars/" class="button-join">Go to My CR Stars</a></p>';
		} else
		{	echo '<p><a href="'. SITE_SUB .'/cr-stars/register/" class="button-join">Join</a><br>Already have an account? <a href="'. SITE_SUB. '/cr-stars/log-in/">Sign in</a>.</p>';

			echo '<p>&nbsp;<br class="hidden-xs" />&nbsp;</p>';
			echo '<p>A small group of driven and passionate individuals can change the world. We’re looking for proactive, dedicated and humble people to become our CR Stars.</p>';
			echo '<p>CR Stars is our elite volunteer programme. It’s for those who want to fundraise to make a real difference in the lives of hungry people on an ongoing basis throughout the year, without the hassle of collecting and delivering money or setting up multiple fundraising pages.</p>';
			echo '</div></div>';
			echo '<div class="container crstarsStartContent crstarsStartContentHalfs"><div class="container_inner">';
			echo '<div class="crstarsStartContentHalf crstarsStartContentHalfBg"><div class="crstarsStartContentHalfInner">';
			echo '&nbsp;';
			echo '</div></div>';
			echo '<div class="crstarsStartContentHalf"><div class="crstarsStartContentHalfInner">';
			echo '<h3>Because we use our own our fundraising platform 100% of the money you raise will come to Charity Right. That means a bigger impact for every penny that you raise and more hungry mouths fed with every donation.</h3>';
			echo '</div></div>';
			echo '</div></div>';

			echo '<div class="container crstarsStartContent"><div class="container_inner">';
			echo '<p>You’ll rise through the ranks of our CR Star statuses. The more you fundraise, the more stars will appear on your profile, and there are some great rewards for those who fundraise well. <a href="">Sign up today</a>.</p>';
			echo '<p><a href="'. SITE_SUB .'/cr-stars/register/" class="button-join">Join</a></p>';
		}
		echo '<ul class="crstarsStartSocial">';
		echo '<li><a href="https://www.facebook.com/CharityRightUK/" target="_blank"><i class="icon-facebook"></i></a></li>';
		echo '<li><a href="https://twitter.com/charityrightuk" target="_blank"><i class="icon-twitter"></i></a></li>';
		echo '<li><a href="https://www.instagram.com/charityrightuk/" target="_blank"><i class="icon-instagram"></i></a></li>';
		echo '</ul>';
		echo '</div></div>';
	} // end of fn MainBodyContent

} // end of class CRStarsStartPage

$page = new CRStarsStartPage();
$page->Page();
?>