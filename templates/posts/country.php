<?php
$style = '';
if ($img_src = $this->post->GetImageSRC())
{	$style = 'style="background-image: url(' . $img_src . ');"';
}

echo '<div class="container postDetailsHeader', $img_src ? ' postDetailsHeaderHasImage' : '', '" ', $style, '><div class="container_inner"><h1>', $posttitle = $this->InputSafeString($this->post->details['posttitle']), '</h1><p><a  href="', SITE_URL, 'donate/" class="button button-white-outline">Donate To a Project in ', $posttitle, '</a></p></div></div>';
echo $this->MainBodyQuickDonateForm(), '<div class="container postCategoryContainer"><div class="container_inner"><div class="postCategoryLeft">', stripslashes($this->post->details['posttext']), '<div class="post-share-buttons">
	<a href="#share" class="share-button">Share Story</a>
	<div class="ssk-group ssk-count ssk-round">
			<a href="" class="ssk ssk-facebook"></a>
			<a href="" class="ssk ssk-twitter"></a>
			<!--<a href="" class="ssk ssk-google-plus"></a>
			<a href="" class="ssk ssk-pinterest"></a>-->
			<a href="" class="ssk ssk-instagram"></a>
			<a href="" class="ssk ssk-linkedin"></a>
			<!--<a href="" class="ssk ssk-tumblr"></a>-->
			<a href="" class="ssk ssk-email"></a>
		</div>
</div></div>
';
$country_category = new PostCategory(0, 'where-we-work');
//$this->VarDump($country_category->details);
echo '<div class="postCategoryRight">';
if ($this->post->sections['country_our_stats'])
{	echo '<div class="postRightPinkWell">', stripslashes($this->post->sections['country_our_stats']['psectiontext']), '</div>';
}
if ($this->post->sections['country_work'])
{	echo '<div class="postRightPanel">', stripslashes($this->post->sections['country_work']['psectiontext']), '</div>';
}
if ($this->post->sections['country_about'])
{	echo '<div class="postRightPanel">', stripslashes($this->post->sections['country_about']['psectiontext']), '</div>';
}
if ($countries = $country_category->GetPosts())
{	echo '<ul class="postCountryFilter">';
	foreach ($countries as $country_row)
	{	$country = new Post($country_row);
		echo '<li', $country->id == $this->post->id ? ' class="selected"' : '', '><a href="', $country->Link(), '">', $this->InputSafeString($country->details['posttitle']), '<span>view</span></a></li>';
	}
	echo '</ul>';
}
if ($this->post->videos)
{	echo '<div class="postRightPanelVideos"><h3>Videos</h3>';
	foreach ($this->post->videos as $video_row)
	{	if ($vidcount++)
		{	echo '<p class="videsoMoreLink"><a href="', $this->post->VideoLink(), '">more videos</a></p>';
			break;
		} else
		{	$video = new Video($video_row);
			if ($src = $video->GetImageSRC()){
			}else{
				$src = SITE_URL.'img/template/post-placeholder-image-og.png';
			}
			echo '<div class="postMainVideo">
					<a href="', $video->Link($this->post), '" class="videoImgLink"><img src="', $src, '" alt="', $this->InputSafeString($video->details['vtitle']), '"></a>
					<h4><a href="', $video->Link($this->post), '">', $this->InputSafeString($video->details['vtitle']), '</a></h4>';
			if ($video->details['description'])
			{	echo '<p>', $this->InputSafeString($video->details['description']), '</p>';
			}
			echo '</div>';
		}
	}
	echo '</div>';
}
echo '</div></div><div class="clear"></div>
</div></div>';
?>