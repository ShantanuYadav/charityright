<?php
echo $this->MainBodyQuickDonateForm(), '<div class="container postDetailsContainer"><div class="container_inner"><div class="postDetailsRight"><h1>', $this->InputSafeString($this->post->details['posttitle']), '</h1>', stripslashes($this->post->details['posttext']), 
'<div class="post-share-buttons">
	<a href="#share" class="share-button">Share Story</a>
	<div class="ssk-group ssk-count ssk-round">
			<a href="" class="ssk ssk-facebook"></a>
			<a href="" class="ssk ssk-twitter"></a>
			<!--<a href="" class="ssk ssk-google-plus"></a>
			<a href="" class="ssk ssk-pinterest"></a>-->
			<a href="" class="ssk ssk-instagram"></a>
			<a href="" class="ssk ssk-linkedin"></a>
			<!--<a href="" class="ssk ssk-tumblr"></a>-->
			<a href="" class="ssk ssk-email"></a>
		</div>
</div>
</div><div class="postDetailsLeft">';
if ($this->post->images)
{	foreach ($this->post->images as $image_row)
	{	$image = new PostImage($image_row);
		$image_size = $imagecount++ ? 'medium' : 'full';
		echo '<a href="', $image->GetImageSRC('full'), '" class="popup-image popup-image-size-', $image_size.'">', $image->ImageHTML($image_size), '</a>';
	}
	echo '<div class="clear"></div>';
}
if ($this->post->videos)
{	echo '<div class="postLeftVideos"><h3>Videos</h3>';
	foreach ($this->post->videos as $video_row)
	{	if ($vidcount++)
		{	echo '<p class="videsoMoreLink"><a href="', $this->post->VideoLink(), '">more videos</a></p>';
			break;
		} else
		{	$video = new Video($video_row);
			if ($src = $video->GetImageSRC()){
			}else{
				$src = SITE_URL.'img/template/post-placeholder-image-og.png';
			}
			echo '<div class="postMainVideo"><a href="', $video->Link($this->post), '" class="videoImgLink"><img src="', $src, '" alt="', $this->InputSafeString($video->details['vtitle']), '"></a><h4><a href="', $video->Link($this->post), '">', $this->InputSafeString($video->details['vtitle']), '</a></h4>';
			if ($video->details['description'])
			{	echo '<p>', $this->InputSafeString($video->details['description']), '</p>';
			}
			echo '</div>';
		}
	}
	echo '</div>';
}
echo '</div><div class="clear"></div>
</div></div>';
?>
