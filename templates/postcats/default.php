<?php
echo '<div class="container postCategoryHeader"';
if ($this->category->HasImage())
{	echo ' style="background-image: url(\'', $this->category->GetImageSRC(), '\')"';
}
echo '><div class="container_inner"><h1>', $this->InputSafeString($this->category->details['catname']), '</h1></div></div>', $this->MainBodyQuickDonateForm();
if ($this->category->details['pagecontent'])
{	echo '<div class="container pageContentContainer"><div class="container_inner">', stripslashes($this->category->details['pagecontent']), '</div></div>';
}
echo '<div class="container postCategoryContainer"><div class="container_inner">';
if ($countries = $this->category->GetCountries())
{	$catname = $this->inputSafeString(strtolower($this->category->details['catname']));
	echo '<div class="postCategoryRight"><div class="postCountryOpener" onclick="$(\'.postCategoryRight\').toggleClass(\'postCategoryRightOpen\');">Showing ', $catname, ' for ', $countries[$_GET['country']] ? $this->InputSafeString($countries[$_GET['country']]['shortname']) : 'all countries', ' <i class="icon-down-open"></i><i class="icon-up-open"></i></div><ul class="postCountryFilter">';
	echo '<li', !$_GET['country'] ? ' class="selected"' : '', '><a href="', $this->category->Link(), '">All ', $catname, '<span>view ', $catname, '</span></a></li>';
	foreach ($countries as $country)
	{	echo '<li', $country['postslug'] == $_GET['country'] ? ' class="selected"' : '', '><a href="', $this->category->Link(), $country['postslug'], '/">', $this->InputSafeString($country['shortname']), '<span>view ', $catname, '</span></a></li>';
	}
	echo '</ul></div>';
}
echo '<div class="postCategoryLeft">';
if ($posts = $this->category->GetPosts($_GET))
{	$perpage = 12;
	if ($_GET['page'] > 1)
	{	$start = ($_GET['page'] - 1) * $perpage;
	} else
	{	$start = 0;
	}
	$end = $start + $perpage;

	echo '<ul class="postsList">';
	foreach ($posts as $post_row)
	{	if (++$count > $start)
		{	if ($count <= $end)
			{
				$post = new Post($post_row);

				echo '<li>';
				echo '<div class="postListLeft">';
				echo '<a class="postListLeftInner" href="', $post->Link(), '">';
				if ($image = $post->MainImage())
				{	echo $image->ImageHTML('medium');
					echo $image->ImageHTML('og');
				}else {
					echo '<img src="'.SITE_URL.'img/template/post-placeholder-image-medium.png" alt="'.$this->InputSafeString($post->details['posttitle']).'" class="placeholder-image image-size-medium">';
					echo '<img src="'.SITE_URL.'img/template/post-placeholder-image-og.png" alt="'.$this->InputSafeString($post->details['posttitle']).'" class="placeholder-image image-size-og">';
				}
				echo '</a>';
				echo '</div>';
				echo '<div class="postListRight">';
				echo '<h2><a href="', $link = $post->Link(), '">', $this->InputSafeString($post->details['posttitle']), '</a></h2>';
				echo '<p>', $this->InputSafeString($post->details['snippet']), '</p>';
				echo '<a class="postListReadMore" href="', $link, '">Read more</a>';
				echo '</div>';
				echo '<div class="clear"></div></li>';
			} else
			{	break;
			}
		}
	}
	echo '</ul>';
	if (count($posts) > $perpage)
	{	$pagelink = $this->category->Link();
		if ($_GET['country'])
		{	$pagelink .= $this->InputSafeString($_GET['country']) . '/';
		}
		$pag = new FriendlyURLPagination($_GET['page'], count($posts), $perpage, $pagelink);
		echo '<div class="pagination">', $pag->Display(''), '</div>';
	}
} else
{	echo '<h3 class="postCategoryNone">No posts available.</h3>';
}
echo '</div><div class="clear"></div></div></div>';
?>
