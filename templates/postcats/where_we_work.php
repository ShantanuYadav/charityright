<?php
echo $this->MainBodyQuickDonateForm(), '<div class="container postHeaderContainer"><div class="container_inner"><h1>', $this->InputSafeString($this->category->details['catname']), '</h1>';
if ($this->category->details['pagecontent'])
{	echo stripslashes($this->category->details['pagecontent']);
}
echo '</div></div><div class="container postCategoryContainer"><div class="container_inner"><div class="postCategoryRight postCategoryRightOpen">';
$countrynames = array();
if ($posts = $this->category->GetPosts($_GET))
{	echo '<ul class="postCountryFilter">';
	foreach ($posts as $post_row)
	{	$country = new Post($post_row);
		$countrynames[] = $this->InputSafeString($country->details['posttitle']);
		echo '<li><a href="', $country->Link(), '">', $this->InputSafeString($country->details['posttitle']), '<span>view</span></a></li>';
	}
	echo '</ul>';
}
echo '<p>Currently we work in ', $this->ImplodeExtra($countrynames, ', ', ' and '), '. Children from these areas don\'t often have the opportunity to escape poverty, but by providing regular meals at school, they are more likely to stay in education, which means they have a chance at a prosperous future.</p></div><div class="postCategoryLeft"><img src="', SITE_URL, 'img/template/wherewework_map.png" alt="where we work" title="where we work" /></div><div class="clear"></div></div></div>';
?>
