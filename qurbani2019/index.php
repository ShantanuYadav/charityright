<?php 
require_once('../init.php');

//$donationpage = new LPDonatePageSudan();
//$donation = $donationpage->Donation();

$donatelink = SITE_URL . 'donate/';
$base = new Base();
//$videolink = 'http://e14aaeb709f7cde1ae68-a1d0a134a31b545b257b15f8a8ba5726.r70.cf3.rackcdn.com/projects/31432/1427815464209-bf74131a7528d0ea5ce8c0710f530bb5/1280x720.mp4';
$videolink = 'video/Charity.mp4';

//if (SITE_TEST) $donationpage->VarDump($_SESSION[$donationpage->SessionName()]);
?><!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- <link rel="icon" type="image/ico" href="img/favicon.ico" sizes="16x16"> -->

	<title>Charity Right | Qurbani 2019</title>

	<!-- Bootstrap CSS --> 
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,400i,600,700,700i,800" rel="stylesheet">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqModal/1.4.2/jqModal.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>

	<link href="css/style.css?<?php echo substr(@filemtime('css/style.css'), -6); ?>" rel="stylesheet">
	<script type="text/javascript">
		//var donation_ajax_endpoint = "ajax_donationslp_sudan.php";
		var jsSiteRoot = "/";
		var $ = jQuery.noConflict();
	</script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-62069931-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-62069931-2');
	</script>
	
	<script>
		!function(f,b,e,v,n,t,s)
		{	if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)
		} (window, document,"script", "https://connect.facebook.net/en_US/fbevents.js");
		fbq("init", "391990840985330");
		fbq("track", "PageView");
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=391990840985330&ev=PageView&noscript=1"	/></noscript>

</head>

<body>

	<!-- start header -->
	<header>
		<div class="container-fluid">
			<nav class="navbar navbar-expand-lg">
				<a class="navbar-brand" href="#"><img src="img/logo.png" class="img-fluid logo"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					<span class="navbar-toggler-icon"></span>
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto ml-5">
<?php
$fmenu = new FrontEndMenu();
$fmenu->GetMenuByName('qurbani2019header');
if ($menuitems = $fmenu->MenuItems())
{	foreach ($menuitems as $menuitem)
	{	$page = new PageContent($menuitem['item']['pageid']);
		echo '<li class="nav-item"><a class="nav-link" href="', $page->Link(), '">', $base->InputSafeString($menuitem['item']['itemtitle'] ? $menuitem['item']['itemtitle'] : $page->details['pagetitle']), '</a></li>';
	}
}
?>
					</ul>
					<ul class="navbar-nav d-flex justify-content-end">
						<li class="nav-item active">
							<a href="<?php echo $donatelink; ?>" class="btn btn_donate">DONATE NOW!</a>
						</li>
<?php
if ($instagram_link = $base->GetParameter('instagram_link'))
{	echo '<li class="nav-item"><a href="', $instagram_link, '"><img src="img/insta.png" class="img-fluid" /></a></li>';
}
if ($facebook_link = $base->GetParameter('facebook_link'))
{	echo '<li class="nav-item"><a href="', $facebook_link, '"><img src="img/fb.png" class="img-fluid" /></a></li>';
}
if ($twitter_link = $base->GetParameter('twitter_link'))
{	echo '<li class="nav-item"><a href="', $twitter_link, '"><img src="img/twt.png" class="img-fluid" /></a></li>';
}
if ($youtube_link = $base->GetParameter('youtube_link'))
{	echo '<li class="nav-item"><a href="', $youtube_link, '"><img src="img/youtube.png" class="img-fluid" /></a></li>';
}
?>
					</ul>
				</div>
			</nav>
		</div>
	</header>
	<!-- end header -->

	<!-- start banner -->
	<section class="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h1 class="letter">2019</h1>
					<img src="img/Compound-Shape2.png" class="img-fluid qurbani_img" />
					<p>Quality meat for vulnerable families<br>in hard to reach areas</p>
				</div>

				<div class="col-md-4">
					<a href="<?php echo $donatelink; ?>"><img src="img/now.png" class="img-fluid now" /></a>
				</div>
			</div>
		</div>
	</section>
	<!-- end banner -->
<?php /*?>
<!-- start qurbani -->
<section class="qurbani">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="pink_box">
                    <div class="row">
                        <div class="col-lg-2 offset-lg-1 col-md-12">
                            <h4>Qurbani<span>2019</span></h4>
                        </div>

                        <div class="col-lg-2 col-md-4">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">£</span>
                              </div>
                              <input type="text" class="form-control" placeholder="Amount" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-4">
                          <select class="custom-select" id="inputGroupSelect02">
                            <option selected>Donation type</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                        </div>

                        <div class="col-lg-2 col-md-4">
                          <select class="custom-select" id="inputGroupSelect02">
                            <option selected>Country</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                        </div>

                        <div class="col-lg-3 col-md-12">
                            <button class="btn btn_donate">DONATE NOW!</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end qurbani -->
<?php /**/?>

	<!-- start food -->
	<section class="food">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="title">When it comes to distributing food <br>- we’re experts.</h2>
					<p>We hand-pick quality animals that meet the conditions of sacrifice,<br>and when we’re done we’ll send you a text.</p>

 <?php /*               <div class="row">
                    <div class="col-md-4">
                      <div class="img_box">
                        <a href="#">
                          <img src="img/d1.png" class="img-fluid mx-auto d-block w-100">
                           <div class="img_txt">
                            Where we distribute
                           </div>
                        </a>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="img_box">
                        <a href="#">
                          <img src="img/d2.png" class="img-fluid mx-auto d-block w-100">
                           <div class="img_txt">
                            Qurbani cost
                           </div>
                        </a>
                      </div>                        
                    </div>


                    <div class="col-md-4">
                     <div class="img_box">
                        <a href="#">
                          <img src="img/d3.png" class="img-fluid mx-auto d-block w-100">
                           <div class="img_txt">
                           Why choose us
                           </div>
                        </a>
                      </div>
                    </div>
                </div>/**/?>
					<h2 class="title" style="color: #e53b82;">3 Reasons to Donate to Charity Right</h2>
					<div class="row">
						<div class="col-md-4">
							<div class="pink_circle"><div class="pink_circle_inner"><p>1</p><p>We hand-pick the best quality livestock for your donation</p></div></div>
						</div>

						<div class="col-md-4">
							<div class="pink_circle"><div class="pink_circle_inner"><p>2</p><p>We oversee sacrifices to ensure they're performed according to Quran &amp; Sunnah</p></div></div>
						</div>

						<div class="col-md-4">
							<div class="pink_circle"><div class="pink_circle_inner"><p>3</p><p>We'll send a text to let you know your sacrifice has been performed</p></div></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end food -->

 <?php ?>
<!-- start donate -->
<section class="donate">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title">Donate your Qurbani</h2>

                <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th class="table_txt">One sheep or goat is equal to one Qurbani.<br>
                                One cow is equal to seven Qurbanis<br>
                                (One Qurbani is 1/7th of the share) </th>
                          <th><img src="img/Cow.png" class="img-fluid mx-auto d-block"></th>
                          <th><img src="img/Sheep_-Goat.png" class="img-fluid mx-auto d-block"></th>
                          <th><img src="img/1_7th-Cow.png" class="img-fluid mx-auto d-block"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">Sudan</th>
                          <td>£400</td>
                          <td>£110</td>
                          <td>£58</td>
                        </tr>
                        <tr>
                          <th scope="row">Pakistan</th>
                          <td>£450</td>
                          <td>£150</td>
                          <td>£65</td>
                        </tr>
                        <tr>
                          <th scope="row">Bangladesh Dhaka Slums</th>
                          <td>£630</td>
                          <td>£120</td>
                          <td>£90</td>
                        </tr>
                        <tr>
                          <th scope="row">Rohingya</th>
                          <td>£660</td>
                          <td>£130</td>
                          <td>£95</td>
                        </tr>
                      </tbody>
                    </table>
                </div>

                <div style="text-align: center;"><a href="<?php echo $donatelink; ?>" class="btn btn_now">Donate Now!</a></div>
            </div>
        </div>
    </div>
</section>
<!-- end donate -->

	<img src="img/bg2.png" class="img-fluid mx-auto d-block">

	<!-- start help -->
	<section class="help">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="title">In 2018, we distributed over<br />9.2 million meals in 6 countries.</h2>
					<div style="text-align: center;"><a href="https://charityright.org.uk/posts/where-we-work/" class="btn btn_now">See who we help</a></div>
				</div>
			</div>
		</div>
	</section>
	<!-- end help -->

	<!-- start video -->
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 pad0">
					<div class="wrapper">
						<video class="video" controls="" style="height: auto;"><source src="<?php echo $videolink; ?>" type="video/mp4" /></video>
						<div class="playpause"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end video -->
	<div style="clear: both;"></div>

	<!-- start reward -->
	<section class="reward">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<h3><span>The Prophet (saw) said: </span>“It is the Sunnah of your <br />father Ibrahim (as). For every hair of the Qurbani<br /> you receive a reward from Allah (swt).”</h3>
					<div class="pink_box pinkbox_infFormContainer" id="infFormContainer">
					<?php
					$inf_form = new InfusionSoftContactForm();
					echo $inf_form->Q19MinimalSectionForm();
					?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end reward -->

	<!-- start footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<img src="img/f_logo.png" class="img-fluid f_logo" />
					<p>We work in neglected communities around the world, feeding forgotten and underserved individuals.</p>
					<p>Our meals support schoolchildren and adults in refugee camps, slums, jungles, and remote villages across Asia, Africa, and Europe too.</p>
				</div>
				<div class="col-md-4 offset-md-2">
					<h4>Contact Us</h4>
					<ul class="list-unstyled">
						<li class="list-unstyled-item"><img src="img/p1.png" /><a href="tel:01274400389">01274 400389</a></li>
						<li class="list-unstyled-item"><img src="img/p2.png" /><a href="mailto:info@charityright.org.uk">info@charityright.org.uk</a></li>
						<li class="list-unstyled-item"><img src="img/p3.png" /><a href="/">charityright.org.uk</a></li>
						<li class="list-unstyled-item"><img src="img/p4.png" />Oakwood Court<br />City Road<br />Bradford BD8 8JY</li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!-- end footer -->

	<div class="copy">
		<div class="container">
			<div class="row">
				<div class="col-md-9">© Charity Right - Fighting Hunger, One Child at a Time. Registered Charity Number 1163944</div>
				<div class="col-md-3 text-right">
					<ul class="list-inline">
<?php

if ($instagram_link = $base->GetParameter('instagram_link'))
{	echo '<li class="list-inline-item"><a href="', $instagram_link, '"><img src="img/f_insta.png" /></a></li>';
}
if ($facebook_link = $base->GetParameter('facebook_link'))
{	echo '<li class="list-inline-item"><a href="', $facebook_link, '"><img src="img/f_fb.png" /></a></li>';
}
if ($twitter_link = $base->GetParameter('twitter_link'))
{	echo '<li class="list-inline-item"><a href="', $twitter_link, '"><img src="img/f_twt.png" /></a></li>';
}
if ($youtube_link = $base->GetParameter('youtube_link'))
{	echo '<li class="list-inline-item"><a href="', $youtube_link, '"><img src="img/f_y.png" /></a></li>';
}
?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php /*?>
	<!--<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>--><?php /**/?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
	<script type="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.bundle.min.js"></script>
	<script src="js/owl.carousel.min.js" type="text/javascript"></script>

	<script type="text/javascript">
	$('.video').parent().click(function () {
	if($(this).children(".video").get(0).paused){     
	$(this).children(".video").get(0).play();  
	$(this).children(".playpause").fadeOut();
	}
	else{  
	$(this).children(".video").get(0).pause();
	$(this).children(".playpause").fadeIn();
	}
	});
	</script>
</body>

</html>