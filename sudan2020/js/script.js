$(document).ready(function () {
  $("#quantityprice").change(function(){
    var amount = (parseFloat($(this).val())||0) * 100;
    StripeCheckout.__app.configurations.button0.amount = amount; // convert to cents
    $("#donationammount").val(amount);
  });
});
