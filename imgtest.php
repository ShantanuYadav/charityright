<?php
$image = imagecreatetruecolor(200, 200);
$white = imagecolorallocate($image, 255, 255, 255);
imagefill($image, 0, 0, $white);
$red = imagecolorallocate($image, 140, 140, 0);
$green = imagecolorallocate($image, 0, 180, 0);
$values = array(0, 0, 0, 100, 100, 0);
imagefilledpolygon($image, $values, sizeof($values) / 2, $red);
$values = array(1, 1, 1, 100, 100, 1);
imagefilledpolygon($image, $values, sizeof($values) / 2, $green);

header('Content-type: image/png');
imagepng($image);
imagedestroy($image);
?>