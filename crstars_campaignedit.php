<?php
require_once('init.php');

class CRStarsCampaignEditPage extends MyCRStarsPage
{	private $campaign;

	public function __construct()
	{	parent::__construct();
		$this->campaign = new Campaign($_GET['cid']);

		if ($this->campaign->details['cuid'] != $this->camp_customer->id)
		{	header('location: ' . $this->campaign->Link());
			exit;
		}
		
		if ($this->campaign->details['htmldesc'])
		{	$this->js[] = 'tiny_mce/jquery.tinymce.js';
			$this->js[] = 'crstars_tiny_mce.js';
		}

		if (isset($_POST['campname']))
		{	$saved = $this->campaign->Save($_POST, $_FILES['campimage']);
			$this->successmessage = $saved['successmessage'];
			$this->failmessage = $saved['failmessage'];
		}

	} // end of fn __construct

	function MemberBodyContent()
	{	ob_start();


		echo '<div class="container crstarsRegister"><div class="container_inner">';
		echo '<h1 class="crstarsMyHeader">Editing: <a href="', $this->campaign->Link(), '">', $this->InputSafeString($this->campaign->details['campname']), '</a>';
		if ($this->campaign->team && ($team = new Campaign($this->campaign->team)) && $team->id)
		{	echo '<br />Raising money for <a href="', $team->Link(), '">', $team->FullTitle(), '</a>.';
		}
		echo '</h1>';


		echo '<form class="newCampaignForm" action="', $this->campaign->EditLink(), '" method="post" enctype="multipart/form-data">';

			echo '<div class="form-row">';
				echo '<div class="form-row-inner">';
					echo '<div class="form-row-half">';
						echo '<label for="campname">Campaign Name</label>';
					echo '</div>';
					echo '<div class="form-row-half">';
						echo '<input type="text" id="campname" name="campname" value="'. $this->InputSafeString($this->campaign->details['campname']). '" required />';
					echo '</div>';
				echo '<div class="clear"></div></div>';
			echo '</div>';

			if ($this->campaign->CanChangeCurrency()) {
				echo '<div class="form-row">';
					echo '<div class="form-row-inner">';
						echo '<div class="form-row-half">';
							echo '<label for="campcurrency">Currency</label>';
						echo '</div>';
						echo '<div class="form-row-half ">';
							echo '<div class="form-select-container">';
							echo '<select id="campcurrency" name="campcurrency" onchange="CRRegCurrencyChange();" required>';
							foreach ($this->GetCurrencies('campaigns') as $curcode=>$currency)
							{	echo '<option value="', $curcode, '"', $curcode == $this->campaign->details['currency'] ? ' selected="selected"' : '', '>', $this->InputSafeString($currency['curname']), ' - ', $currency['cursymbol'], '</option>';
							}
							echo '</select>';
							echo '</div>';
						echo '</div>';
					echo '<div class="clear"></div></div>';
				echo '</div>';
			}

			echo '<div class="form-row">';
				echo '<div class="form-row-inner">';
					echo '<div class="form-row-half">';
						echo '<label for="target">How much would you like to raise?</label>';
					echo '</div>';
					echo '<div class="form-row-half">';
						echo '<div class="campCurrencySpanContainer">';
						echo '<span class="campCurrencySpan">';
							echo $this->GetCurrency($this->campaign->details['currency'], 'cursymbol');
						echo '</span>';
						echo '<input type="number" id="target" name="target" class="number" value="', (int)$this->campaign->details['target'], '" required />';
						echo '</div>';
					echo '</div>';
				echo '<div class="clear"></div></div>';
			echo '</div>';

			echo '<div class="form-row">';
				echo '<div class="form-row-inner">';
					echo '<div class="form-row-full">';
						echo '<label for="description">Campaign Summary</label>';
						echo '<p>Write your own summary</p>';
					echo '<textarea id="description" name="description" placeholder="Campaign Summary"', $this->campaign->details['htmldesc'] ? ' class="tinymce"' : '', ' required>', $this->InputSafeString($this->campaign->details['description']), '</textarea>';
					echo '</div>';
			if (!$this->campaign->team)
			{	$donation = new Donation();
				if ($countries = $donation->GetDonationCountries('oneoff', true))
				{	echo '<div class="clear"></div><div class="form-row-half">
								<label for="">Which causes would you like to support?</label>
							</div>
							<div class="form-row-half"><div class="form-select-container"><select id="donation_country" name="donation_country" onchange="CRRegCountryChange();">';
					$donation_country = $this->campaign->details['donationcountry'];
					foreach ($countries as $country=>$cdetails)
					{	if (!$donation_country)
						{	$donation_country = $country;
						}
						echo '<option value="', $this->InputSafeString($country), '"', ($country == $donation_country) ? ' selected="selected"' : "", '>', $this->InputSafeString($cdetails['shortname']), '</option>';
					}
					echo '</select></div></div>';
					echo '<div id="countryProjectContainer">', $this->CountryProjectsList($donation_country, $this->campaign->details['donationproject']), '</div>';
				}
			}
			echo '<div class="clear"></div></div>';
			echo '</div>';

			echo '<div class="form-row">';
				echo '<div class="form-row-inner">';
					echo '<div class="form-row-half">';
						echo '<label for="">Campaign Picture</label>';
						echo '<p>Upload your own picture or choose one from our bank of images</p>';
					echo '</div>';
					echo '<div class="form-row-half">';
						echo '<ul class="radio-list">';

						echo '<li><input type="radio" name="campaign_picture_choosen" id="campaign_picture_choosen_1" value="campaign_picture_choosen_1" onclick="$(\'.campaign_picture_choosen_2_container\').hide();$(\'.campaign_picture_choosen_1_container\').slideDown();" '.(($this->campaign->GetImageSRC('small'))?'checked="checked"':"").'><label for="campaign_picture_choosen_1">Upload my own picture</label><div class="clear"></div></li>';

						echo '<li><input type="radio" name="campaign_picture_choosen" id="campaign_picture_choosen_2" value="campaign_picture_choosen_2" onclick="$(\'.campaign_picture_choosen_1_container\').hide();$(\'.campaign_picture_choosen_2_container\').slideDown();$(\'#campimage\').val(\'\');" '.((!$this->campaign->GetImageSRC('small'))?'checked="checked"':"").'><label for="campaign_picture_choosen_2">Choose an image</label><div class="clear"></div></li>';
						echo '</ul>';
					echo '</div>';
					echo '<div class="clear"></div>';
					echo '<div class="form-row-full campaign_picture_choosen_1_container" '.((!$this->campaign->GetImageSRC('small'))?'style="overflow: hidden; display: none;""':"").'>';
						if ($src = $this->campaign->GetImageSRC('small')){
							echo '<div class=""><label>Your current image</label><br><img src="' . $src . '" /></div>';
						}
						echo '<div class="input-box-container">';
						echo '<input type="file" id="campimage" name="campimage" onchange="" />';
						echo '</div>';
					echo '</div>';
					echo '<div class="clear"></div>';
					if ($avatars = $this->GetCampaignAvatarsAvailable()){
					echo '<div class="form-row-full campaign_picture_choosen_2_container" '.(($this->campaign->GetImageSRC('small'))?'style="overflow: hidden; display: none;""':"").'>';
						echo '<div class="input-box-container">';
						echo '<ul class="campaign-avatars">';
						foreach ($avatars as $avatar_row) {
							$avatar = new CampaignAvatar($avatar_row);
							echo '<li id="crAvatar_', $avatar->id, '" class="', $this->campaign->details['avatarid'] == $avatar->id ? 'crAvatarSelected' : '', '">
							<a href="#crAvatar_', $avatar->id, '" onclick="CRRegAvatarChooser(', $avatar->id, ');return false;">
							<img src="', $avatar->GetImageSRC('small'), '" alt="', $alt = $this->InputSafeString($avatar->details['imagedesc']), '" title="', $alt, '" />
							</a>
							</li>';
						}
						echo '</ul>';

						echo '<input type="hidden" name="avatarid" value="', (int)$this->campaign->details['avatarid'], '" />';
						echo '<div class="clear"></div>';
						echo '</div>';
					echo '</div>';
					}
				echo '<div class="clear"></div></div>';
			echo '</div>';

			if ($this->campaign->CanChangeType()) {
			echo '<div class="form-row">';
				echo '<div class="form-row-inner">';
					echo '<div class="form-row-half">';
						echo '<label for="">Type Of Campaign</label>';
						echo '<p>Will you be campaigning on your own or within a team?</p>';
					echo '</div>';
					echo '<div class="form-row-half">';
						echo '<ul class="radio-list">';
							echo '<li><input type="radio" name="isteam" id="isteam_0" value="0" '. (($this->campaign->details['isteam']) ? "" : ' checked="checked"'). '><label for="isteam_0">Individual Campaign</label><div class="clear"></div></li>';
							echo '<li><input type="radio" name="isteam" id="isteam_1" value="1" '. (($this->campaign->details['isteam']) ? ' checked="checked"' : ""). '><label for="isteam_1">Team Campaign</label><div class="clear"></div></li>';
						echo '</ul>';
					echo '</div>';
				echo '<div class="clear"></div></div>';
			echo '</div>';
			}

			echo '<div class="form-row">';
				echo '<div class="form-row-inner">';
					echo '<div class="form-row-half">';
						echo '<label for="">Campaign Settings</label>';
						echo '<p>Enable campaign and donations</p>';
					echo '</div>';
					echo '<div class="form-row-half">';
						echo '<ul class="checkbox-list">';
							echo '<li><input type="checkbox" name="visible" id="visible" value="1" '. (($this->campaign->details['visible']) ? 'checked="checked"' : ""). '><label for="visible">Campaign is visible</label><div class="clear"></div></li>';
							echo '<li><input type="checkbox" name="enabled" id="enabled" value="1" '. (($this->campaign->details['enabled']) ? ' checked="checked"' : ""). '><label for="enabled">Donations are enabled</label><div class="clear"></div></li>';
						echo '</ul>';
					echo '</div>';
				echo '<div class="clear"></div></div>';
			echo '</div>';

			echo '<div class="form-row crRegFormContactPref"><div class="form-row-inner"><div class="form-row-full"><div class="custom-checkbox clearfix"><input type="checkbox" name="helpfromcr" id="helpfromcr" value="1" ', $this->campaign->details['helpfromcr'] ? 'checked="checked" ' : '', '/><label for="helpfromcr">I\'d like Charity Right to help me with my fundraising</label></div></div></div></div>';

			echo '<div class="form-row form-row-no-border">';
				echo '<div class="form-row-full regformButtons" style="text-align:center;">';

					echo '<button type="submit">Save Changes</button>';

				echo '<div class="clear"></div></div>';
			echo '</div>';
		echo '</form>';
		echo '</div></div>';
		return ob_get_clean();
	} // end of fn MemberBodyContent

} // end of class CRStarsCampaignEditPage

$page = new CRStarsCampaignEditPage();
$page->Page();
?>