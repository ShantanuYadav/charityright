<?php 
require_once('init.php');

class CRStarsAjax extends CRStarsPage
{
	public function __construct()
	{	parent::__construct();
		
		if ($_GET['cid'])
		{	$this->campaign = new Campaign($_GET['cid']);
		}
		
		if (method_exists($this, $method = 'Action_' . $_GET['action']))
		{	$this->$method();
		}
		
	} // end of fn __construct

	private function Action_moredonations()
	{	if ($donations = $this->campaign->GetDonations())
		{	echo $this->ListCampaignDonations($donations, $_GET['start'], $_GET['listcount']);
		}
	} // end of fn Action_moredonations

	private function Action_regcountrychange()
	{	echo $this->CountryProjectsList($_GET['country'], $_GET['project']);
	} // end of fn Action_regcountrychange

	private function Action_teamaddlink()
	{	echo $this->campaign->AddTeamContent();
	} // end of fn Action_teamaddlink

	private function Action_teamadd()
	{	echo $this->campaign->AddToTeam($_GET['teamid']) ? '1' : '0', '|~|';
	} // end of fn Action_teamadd

	private function Action_teamaddopen()
	{	if (!$this->campaign->details['isteam'])
		{	if ($this->campaign->team)
			{	echo 'Already raising for ', $this->InputSafeString($this->campaign->team['campname']);
			} else
			{	echo '<form onsubmit="return false;">Search campaigns to join<input type="text" id="campSearchFilter" /></form><div id="campAddTeamCampaignList">', $this->CampaignsAvailableList(), '</div>';
			}
		}
	} // end of fn Action_teamaddopen

	private function Action_teamaddfilter()
	{	if (!$this->campaign->details['isteam'])
		{	if (!$this->campaign->team)
			{	echo $this->CampaignsAvailableList();
			}
		}
	} // end of fn Action_teamaddfilter

	private function CampaignsAvailableList()
	{	ob_start();
		if ($campaigns = $this->CampaignsAvailable())
		{	echo '<ul>';
			foreach ($campaigns as $campaign_row)
			{	$campaign = new Campaign($campaign_row);
				echo '<li><h3>', $campaign->FullTitle(), '</h3><a onclick="CampaignAddTeam(', $this->campaign->id, ',', $campaign->id, ');">Join this campaign</a></li>';
			}
			echo '</ul>';
		}
		return ob_get_clean();
	} // end of fn CampaignsAvailableList

	private function CampaignsAvailable()
	{	$tables = array('campaigns'=>'campaigns');
		$fields = array('campaigns.*');
		$where = array('isteam'=>'campaigns.isteam=1', 'visible'=>'campaigns.visible=1', 'enabled'=>'campaigns.enabled=1');
		$orderby = array('campaigns.created DESC');
		
		if ($_GET['filter'])
		{	$tables['campaigns'] = 'campaigns LEFT JOIN campaignusers ON campaignusers.cuid=campaigns.cuid';
			$where['campname'] = '(campaigns.campname LIKE "%' . $this->SQLSafe($_GET['filter']) . '%" OR (campaigns.cuid > 0 AND (campaignusers.firstname LIKE "%' . $this->SQLSafe($_GET['filter']) . '%" OR campaignusers.lastname LIKE "%' . $this->SQLSafe($_GET['filter']) . '%")) OR (campaigns.cuid = 0 AND ("charity right" LIKE "%' . $this->SQLSafe($_GET['filter']) . '%")))';
		}
		$sql = $this->db->BuildSQL($tables, $fields, $where, $orderby);
		return $this->db->ResultsArrayFromSQL($sql, 'cid');
	} // end of fn CampaignsAvailable
	
} // end of class CRStarsAjax

$page = new CRStarsAjax();
?>
