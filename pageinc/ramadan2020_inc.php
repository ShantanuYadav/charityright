<?php

$this->css['hover-min'] = '../assets/css/hover-min.css';
$this->css['fontawesome'] = '../assets/fontawesome/css/all.css';
$this->css['bootstrap'] = '../assets/css/bootstrap.min.css';
$this->css['style'] = '../assets/css/style.css';
$this->css['margin-helpers'] = '../assets/css/margin-helper-classes.css';




// Alls meals - charityright.org.uk/meals
// Hifz - charityright.org.uk/sudan-hifz
// Corona - charityright.org.uk/covid19
// Sudan - charityright.org.uk/mealsforsudan

?>

<!-- Start: Header -->
<div class="container" style="margin:0; overflow:hidden; min-width: auto !important;
	max-width: 100% !important;">
    <!-- Start: page wrapper -->
    <div class="container-fluid">
        <div class="row" style="background-color: black;">
            <div class=" col-12 px-0 mx-0 text-center">
                <img src="/assets/img/header.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
        <div class="container-fluid py-3">
                    <div class="container" style="width:100%; margin-left: auto; margin-right:auto;">
                    <div class="row justify-content-md-center">
                        <div class="col col-lg-10">
                            <div class="row">
                                <div class="col-sm-6 py-2">
                                    <a href="/mealsforsudan" class="">
                                        <img src="/assets/img/blogs/feed_famileis_sudan.jpg" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <div class="col-sm-6 py-2">
                                    <a href="/sudan-hifz" class="">
                                        <img src="/assets/img/blogs/feed_hifz_students_in_sudan.jpg" alt="" class="img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
    <?php


/* 
<div class="container-fluid m-0 p-0" onclick="openMyTenNights()">
    <div class="tn-banner">
        <div class="container">
            <div class="row py-5">
                <div class="col-12 justify-content-center text-center">
                    <p class="tn-banner-txt-1">Don't miss giving on <span class="tn-banner-txt-2">Laylatul
                            Qadr</span></p>
                    <p class="tn-banner-txt-3">Schedule your Donations now <a href="javascript:;" class="tn-banner-btn" onclick="openMyTenNights()">START
                            NOW</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

*/

    echo '<!-- Start: Donation Suggestion -->
			<div class="container-fluid tennightsmargin" style="">
            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <div class="ir-counter-card">
                        <div class="card shadow p-0 m-0" style="border-radius: 0;border: 0px;">
                            <div class="card-body p-0 m-0">
                                <div class="c-no container-fluid p-0 m-0">
                                    <div class="row p-0 m-0" id="counter">
                                        <div class="col-sm-3 counter-Txt text-center hvr-grow-shadow"
                                            style="background-color: #00A99D;">
											<a href="https://charityright.org.uk/donate">
											<img src="/assets/img/ds1.jpg" alt=""
                                                class="img-fluid"></a>
												</div>
                                                <div class="col-md-4 counter-Txt" style="background-color: #E53B82;">
                                                <a href="https://charityright.org.uk/donate?other_amout_inner=20&region=most_needed"
                                                    class="cr-d-btn-1-l hvr-grow-shadow">DONATE £20</a><br>
                                                <p class="cr-d-btn-desc-1-l">FOR AN EMERGENCY</p><br>
                                                <p class="cr-d-btn-desc-1-l" style="margin-top:-15px">FAMILY FOOD PACK</p>
                                                <br>
                                                <p class="cr-d-btn-detail-1-l">Feed a family of four for one month</p>
                                            </div>
                                            <div class="col-md-5 counter-Txt" style="background-color: #FFFF00;">
                                                <a href="https://charityright.org.uk/donate?other_amout_inner=100&region=most_needed"
                                                    class="cr-d-btn-1-r hvr-grow-shadow">DONATE £100</a><br>
                                                <p class="cr-d-btn-desc-1-r">TO PROVIDE</p><br>
                                                <p class="cr-d-btn-desc-1-r" style="margin-top:-15px">250 MEALS</p>
                                                <br>
                                                <p class="cr-d-btn-detail-1-r"> Feed adults and children around the world </p>
                                            </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End: Donation Suggestion -->
        </div>
    <!-- End: Header -->';


    echo '<!-- Start: Intro Text -->
        <div class="cotainer" style="overflow: hidden; width:80%; margin-left: auto; margin-right:auto;">
            <div class="row cr-intro">
                <div class="col-12 text-center">
                    <p class="cr-txt-1 mt-35">Although the people we regularly support are fearful of the coronavirus,<br> the
                        biggest threat
                        to
                        their lives right now is starvation.</p>
                    <p class="cr-txt-2">Because of government closures, we can no longer provide nutritious meals to
                        children <br> in most of
                        the schools we normally work in. </p>
                    <p class="cr-txt-2">And due to national lockdowns, parents and their children have no opportunities
                        to work <br> or even
                        beg for money that will help to feed them.</p>
                    <p class="cr-txt-3">This is why your help is urgently needed this Ramadan.</p>
                    <p><img src="/assets/img/line.png" alt="" style="width: 30%;"></p>
                    <p class="cr-txt-1">Some things are important right now.<br>
                        And then some things are literally life and death. </p>
                    <p class="cr-txt-4">Please watch our video.</p>
					
					
                </div>
            </div>
        </div>
    <!-- End: Intro Text  -->';

    /*   <p><mtn-widget-button>Donate now</mtn-widget-button><p>   */

    /*
echo '<!-- Start: Video Section -->
    <section class="container-fluid" style="background-color: #000000;">
        <div class="container">
            <div class="row py-4">
                <div class="col-12 text-center">
                    <div class="embed-responsive mx-auto"
                        style="max-height: 360px; max-width: 640px;">
                        <iframe width="640px" height="360px" class="vid" id="yt" src="https://www.youtube.com/embed/P_ICQ4lHoTM" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

            </div>
        </div>
    <!-- End: Video Section -->';
	
	*/

    echo '<!-- Start: Video Section -->
                    <div class="container" style="background: #000;">
            <div class="row py-4">
                <div class="col-12 text-center">
                    <div class="embed-responsive mx-auto" style="max-height: 360px; max-width: 640px;">
                        <iframe width="640px" height="360px" class="vid" id="yt" src="https://www.youtube.com/embed/P_ICQ4lHoTM" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

            </div>
        </div>
    <!-- End: Video Section -->';



    echo '<!-- Start: Whats Happeing in pakistan -->
        <div class="container cr-pakistan" style="width:80%; text-align: center; margin-left: auto; margin-right:auto;">
            <div class="row">
                <div class="col-12 text-center">
                    <p><img src="/assets/img/pakistan.png" alt="" class="img-fluid"></p>
                    <p class="cr-txt-1">Since a national lockdown began in late March incomes have dried up for many
                        Pakistanis who rely
                        on low-paid work to survive.</p>
                    <p class="cr-txt-2">Although the number of coronavirus cases moved past 5,300 in mid-April, the most
                        immediate
                        concern for people is not COVID-19 but a fear of going hungry.</p>
                    <p class="cr-txt-2">As a result, we have already distributed emergency food packs to over 1,000
                        families who live in
                        the Tharparkar desert. And currently we are identifying the most vulnerable families who live in
                        cities and doing all we can to provide them with potentially life-saving food packs. </p>

                </div>
            </div>
            <div class="row p-0 m-0 text-left">
            <div class="col-md-1"></div>
            <div class="col-md-5" style="background-color: #E53B82;">
                <a href="https://charityright.org.uk/donate?other_amout_inner=20&region=most_needed" class="cr-d-btn-1-l hvr-grow-shadow">DONATE
                    £20</a><br>
                <p class="cr-d-btn-desc-2-l">FOR AN EMERGENCY</p><br>
                <p class="cr-d-btn-desc-2-l" style="margin-top:-20px">FAMILY FOOD PACK</p><br>
                <p class="cr-d-btn-detail-2-l">Feed a family of four for one month</p>
            </div>
            <div class="col-md-5" style="background-color: #FFFF00;">
                <a href="https://charityright.org.uk/donate?other_amout_inner=100&region=most_needed" class="cr-d-btn-1-r hvr-grow-shadow">DONATE
                    £100</a><br>
                <p class="cr-d-btn-desc-2-r">TO PROVIDE</p><br>
                <p class="cr-d-btn-desc-2-r" style="margin-top:-20px">250 MEALS</p><br>
                <p class="cr-d-btn-detail-2-r">Feed adults and children around the world</p>
            </div>
            <div class="col-md-1"></div>
            </div>
        </div>
    <!-- End: Whats HAppening in pakistan -->';

    // echo '<!-- Start: Prayer Picture Section -->
    //     <div class="container-fluid" style=>
    //         <div class="row" style="background-color: black;">
    //             <div class=" col-12 px-0 mx-0 text-center">
    //                 <img src="/assets/img/prayer.png" alt="" class="img-fluid">
    //             </div>
    //         </div>
    //     </div>
    // <!-- End: Prayer Picture Section -->';

    echo '<!-- Start: Whats Happening in bangladesh -->
        <div class="container cr-bangladesh" style="overflow: hidden; width:80%; margin-left: auto; margin-right:auto;">
            <div class="row">
                <div class="col-12 text-center">
                    <p><img src="/assets/img/bangladesh.png" alt="" class="img-fluid"></p>
                    <p class="cr-txt-1">The Bangladeshi government has imposed a complete lockdown to <br> prevent the
                        coronavirus from
                        spreading. But the move is bringing <br> hardship to tens of thousands of people, including many
                        daily wage <br> workers who live in the slums of Dhaka and Rohingya people living in <br>
                        refugee camps in
                        the city of Cox’s Bazar.</p>

                    <p class="cr-txt-4" style="font-weight: bold; margin-top: 50px;">We are taking action in the
                        following ways:</p>

                    <p><img src="/assets/img/dhaka.png" alt="" class="img-fluid"></p>
                    <p class="cr-txt-2">Our teams are distributing food to families with young children and families who
                        send <br> their
                        children to a Charity Right school. This means we are continuing to support <br> children who
                        have
                        received our free meals in schools over the past 6 years.</p>
                    <p><img src="/assets/img/coxbazar.png" alt="" class="img-fluid"></p>
                    <p class="cr-txt-2">Our teams are continuing to source food locally so they can provide meals to
                        Rohingya <br> children in
                        nine schools located in the refugee camps. As normal, we’re also providing <br> our food packs
                        to
                        Rohingya families who have no means of sourcing their next meal.</p>

                </div>
            </div>
            <div class="row p-0 m-0 text-left">
            <div class="col-md-1"></div>
            <div class="col-md-5" style="background-color: #E53B82;">
                <a href="https://charityright.org.uk/donate?other_amout_inner=20&region=most_needed" class="cr-d-btn-1-l hvr-grow-shadow">DONATE
                    £20</a><br>
                <p class="cr-d-btn-desc-2-l">FOR AN EMERGENCY</p><br>
                <p class="cr-d-btn-desc-2-l" style="margin-top:-20px">FAMILY FOOD PACK</p><br>
                <p class="cr-d-btn-detail-2-l">Feed a family of four for one month</p>
            </div>
            <div class="col-md-5" style="background-color: #FFFF00;">
                <a href="https://charityright.org.uk/donate?other_amout_inner=100&region=most_needed" class="cr-d-btn-1-r hvr-grow-shadow">DONATE
                    £100</a><br>
                <p class="cr-d-btn-desc-2-r">TO PROVIDE</p><br>
                <p class="cr-d-btn-desc-2-r" style="margin-top:-20px">250 MEALS</p><br>
                <p class="cr-d-btn-detail-2-r">Feed adults and children around the world</p>
            </div>
            <div class="col-md-1"></div>
            </div>
        </div>
    <!-- End:  WHats Happening in bangladesh -->';


    echo '<!-- Start: Family Food Packages -->
    <div class="container-fluid" style="background-color: #EDE1D1; ">
        <div class="container py-5" style="overflow: hidden; width:80%; margin-left: auto; margin-right:auto;">
            <div class="row">
                <div class="col-12 text-center">
                    <p><img src="/assets/img/family-food-pack.png" alt="" class="img-fluid"></p>
                    <p class="cr-txt-1">Our food packs are designed to provide a family of four
                        with all <br> the
                        essentials
                        they need to feed
                        themselves for a month.</p>


                </div>
            </div>

            <div class="row text-center">
                <div class="col-md-6 text-center my-auto">
                    <div class="">
                        <img src="/assets/img/box.png" class="img-fluid" />
                    </div>
                </div>
                <div class="col-md-6 text-center my-auto">
                    <div class="">
                        <img src="/assets/img/chart.png" class="img-fluid" />
                    </div>
                </div>
            </div>

        </div>
        </div>
    <!-- End: Family Food Packages -->';


    echo '<!-- Start: Support Section -->
    <div class="container-fluid" style="background-color: #E73B84;" style="margin-bottom:400px">
        <div class="container" style="width:80%; margin-left: auto; margin-right:auto;">
            <div class="row cr-support-mb">
                <div class="col-12 text-center mt-5">
                    <p><img src="/assets/img/support.png" alt="" class="img-fluid"></p>
                    <p class="cr-txt-1" style="color: #ffffff;">Charity Right is an experienced and trusted charity that
                        has delivered <br> over
                        30
                        million meals to
                        vulnerable children and adults over the past 6 <br> years.</p>
                    <p class="cr-txt-2" style="color: #ffffff;">We have lots of experience working in difficult
                        conditions, which includes
                        supporting <br> people
                        in
                        refugee camps, remote villages and inner city slums.</p>
                    <p class="cr-txt-2" style="color: #ffffff;">We also have dedicated teams in all the areas we work
                        who understand how the
                        <br> coronavirus is
                        impacting on the people we regularly help and the support they need <br> right now.</p>
                    <p class="cr-txt-1" style="color: #ffffff;">So please take action today. <br>
                        Just a few clicks or taps of your phone will save lives. </p>

                </div>
            </div>

            <div class="row justify-content-md-center">
                <div class="col col-lg-10">
                    <div class="ir-counter-card">
                        <div class="card" style="border-radius: 0;border: 0px;">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row" id="counter">

                                    <div class="col-sm-6 " style="background-color: #E53B82;">
                                    <a href="https://charityright.org.uk/donate?other_amout_inner=20&region=most_needed&region=most_needed"
                                        class="cr-d-btn-1-l hvr-grow-shadow">DONATE
                                        £20</a><br>
                                    <p class="cr-d-btn-desc-1-l">FOR AN EMERGENCY</p><br>
                                    <p class="cr-d-btn-desc-1-l" style="margin-top:-15px">FAMILY FOOD PACK</p><br>
                                    <p class="cr-d-btn-detail-1-l">Feed a family of four for one month</p>
                                </div>
                                <div class="col-sm-6" style="background-color: #FFFF00;">
                                    <a href="https://charityright.org.uk/donate?other_amout_inner=100&region=most_needed"
                                        class="cr-d-btn-1-r hvr-grow-shadow">DONATE
                                        £100</a><br>
                                    <p class="cr-d-btn-desc-1-r">TO PROVIDE</p><br>
                                    <p class="cr-d-btn-desc-1-r" style="margin-top:-15px">250 MEALS</p><br>
                                    <p class="cr-d-btn-detail-1-r"> Feed adults and children around the world</p>
                                </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div>
    <!-- End: Support Section -->';

    echo '<!-- Start: Header -->
        <div class="container-fluid">
            <div class="row">
                <div class="cr-seperator">
                    
                </div>
            </div>
            </div>
            
            
            </div><!-- End: page wrapper -->';




    $this->js['jquery'] = '../assets/js/jquery-3.4.1.min.js';
    $this->js['popper'] = '../assets/js/popper-2.0.0.min.js';
    $this->js['bootstrap'] = '../assets/js/bootstrap-4.4.1.min.js';
    $this->js['script'] = '../assets/js/script.js';
    ?>