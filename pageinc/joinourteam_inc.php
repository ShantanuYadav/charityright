<?php
//unset($_SESSION['jotFormPrefix']);
if (!$_SESSION['jotFormPrefix'])
{	$_SESSION['jotFormPrefix'] = 'F' . $this->ConfirmCode(4);
}

$volunteer = new Volunteer();

if (isset($_POST[$_SESSION['jotFormPrefix'] . 'fname']))
{	$fail = array();
	$saved = $volunteer->Create($_POST);
	$this->failmessage = $saved['failmessage'];
	$this->successmessage = $saved['successmessage'];
}
?>
<div class="container page-section page-section-half page-section-custom-1">
	<div class="container_inner">
		<div class="page-section-half-content page-section-half-content-1">
			<div class="page-section-half-content-left">
				<h3>Get Involved!</h3>
				<!--<p>Complete the Contact Form<br>or get in touch via <strong><?php echo $this->InputSafeString($this->GetParameter('compphone'));?></strong><br>or email <a href="mailto:<?php echo $compemail = $this->GetParameter('compemail');?>"><strong><?php echo $compemail;?></strong></a></p>-->
				<p>Fill in the contact form<br>or call us on <strong><?php echo $this->InputSafeString($this->GetParameter('compphone'));?></strong></p>
			</div>
		</div>
		<div class="page-section-half-content page-section-half-content-2">
			<div class="page-section-half-content-right">
				<div class="contact-form">
					<form method="post" class="joinourteamForm">
						<div class="form-row">
							<div class="form-row-half">
								<input name="<?php echo $_SESSION['jotFormPrefix'];?>fname" id="<?php echo $_SESSION['jotFormPrefix'];?>fname" type="text" value="<?php echo $this->InputSafeString($_POST[$_SESSION['jotFormPrefix'] . 'fname']);?>" placeholder="First Name" required />
							</div>
							<div class="form-row-half">
								<input name="<?php echo $_SESSION['jotFormPrefix'];?>sname" id="<?php echo $_SESSION['jotFormPrefix'];?>sname" type="text" value="<?php echo $this->InputSafeString($_POST[$_SESSION['jotFormPrefix'] . 'sname']);?>" placeholder="Second Name" required />
							</div>
							<div class="clear"></div>
						</div>
						<div class="form-row">
							<div class="form-row-half">
								<input name="<?php echo $_SESSION['jotFormPrefix'];?>email" id="<?php echo $_SESSION['jotFormPrefix'];?>email" type="email" value="<?php echo $this->InputSafeString($_POST[$_SESSION['jotFormPrefix'] . 'email']);?>" placeholder="Email" required />
							</div>
							<div class="form-row-half">
								<input name="<?php echo $_SESSION['jotFormPrefix'];?>phone" id="<?php echo $_SESSION['jotFormPrefix'];?>phone" type="text" value="<?php echo $this->InputSafeString($_POST[$_SESSION['jotFormPrefix'] . 'phone']);?>" placeholder="Phone" required />
							</div>
							<div class="clear"></div>
						</div>
						<div class="form-row">
							<textarea name="<?php echo $_SESSION['jotFormPrefix'];?>notes" id="<?php echo $_SESSION['jotFormPrefix'];?>notes" placeholder="Notes" required><?php echo $this->InputSafeString($_POST[$_SESSION['jotFormPrefix'] . 'notes']) ? $this->InputSafeString($_POST[$_SESSION['jotFormPrefix'] . 'notes']) : '';?></textarea>
							<div class="clear"></div>
						</div>
						<div class="form-row form-row-submit">
							<input type="submit" value="Submit" class="button" />
							<div class="clear"></div>
						</div>
					</form>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>