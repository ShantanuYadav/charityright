<?php
echo '<div class="crstCampList">';
$campaigns = array();

$tables = array('campaigns'=>'campaigns');
$fields = array('campaigns.*');
$where = array('campaigns.visible=1');
$orderby = array('campaigns.created DESC');

$campaigns = $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'cid', true);

if ($campaigns)
{	
	echo '<ul class="campaignsList">';
	foreach ($campaigns as $campaign_row)
	{	$campaign = new Campaign($campaign_row);
		echo '<li><h2><a href="', $campaign->Link(), '">', $campaign->FullTitle(), '</a></h2></li>';
	}
	echo '</ul>';
	
} else
{	echo '<h3>no campaigns yet</h3>';
}
echo '</div><div class="crstCampDBoard">';
if ($_SESSION[SITE_NAME]['camp_customer'])
{	echo '<ul><li><a href="', SITE_SUB, '/cr-stars/log-out/">log out</a></li><li><a href="', SITE_SUB, '/my-cr-stars/">My Dashboard</a></li></ul>';
} else
{	echo '<ul><li><a href="', SITE_SUB, '/cr-stars/register/">Register a campaign</a></li><li><a href="', SITE_SUB, '/cr-stars/log-in/">Log in</a></li></ul>';
}
echo '</div>';
?>