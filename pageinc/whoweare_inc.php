<?php
if (($people = new People(array('ptype'=>'trustee', 'liveonly'=>true))) && $people->people)
{	echo '<div id="trustees" class="container page-section page-section-full-width page-section-custom-4"><div class="container_inner"><h2>Our Trustees.</h2><div class="trustees">';
	foreach ($people->people as $p_row)
	{	$person = new Person($p_row);
		echo '<div class="trustee"><div class="trustee-image"><img src="', $person->GetImageSRC('default'), '" alt="', $this->InputSafeString($person->details['pname']), '"></div><h3 class="trustee-name">', $this->InputSafeString($person->details['pname']), '</h3><p class="trustee-title">', $this->InputSafeString($person->details['ptitle']), '</p></div>';
	}
	echo '<div class="clear"></div></div></div></div>';
}
if (($partners = new Partners()) && $partners->partners)
{	
	echo '<div id="partners" class="container page-section page-section-full-width page-section-no-padding page-section-custom-5"><div class="container_inner"><h2>Our Partners.</h2><div class="partners">';
	foreach ($partners->partners as $partner)
	{	echo '<div class="partner">';
		if ($partner['link'])
		{	echo '<a href="', $partner['link'], '" target="_blank">';
		}
		echo '<img src="', SITE_URL, 'img/partners/', $partner['imagename'], '" alt="', $this->InputSafeString($partner['pname']), '">', $partner['link'] ? '</a>' : '', '</div>';
	}
	echo '<div class="clear"></div></div></div></div>';
}
?>
<div id="contact-us" class="container page-section page-section-half page-section-custom-6">

	<div class="container_inner">
		<div class="page-section-half-background">
			<div id="map" class="custom-google-map"></div>
		</div>
		<div class="page-section-half-content">
			<div class="page-section-half-content-right">
				<h2>Contact Us.</h2>
				<div class="address">
					<div class="address-1">
						<?php
						if($compaddress = $this->GetParameter('compaddress'))
						{	echo '<h3>Address</h3><p>';
							if($comptitle = $this->GetParameter('comptitle'))
							{	echo $this->InputSafeString($comptitle), '<br>';
							}
							echo nl2br($this->InputSafeString($compaddress)), '</p>';
						}?>
						<!--<a href="https://goo.gl/maps/y3QTm2pXWJJ2" class="button" target="_blank">Open Map</a>-->
					</div>
					<div class="address-2">
						<?php
						if($compphone = $this->GetParameter('compphone'))
						{	echo '<h3>Telephone</h3><p>', $this->InputSafeString($compphone), '</p>';
						}
						if($compemail = $this->GetParameter('compemail'))
						{	echo '<h3>Email</h3><p><a href="mailto:', $this->InputSafeString($compemail), '">', $this->InputSafeString($compemail), '</a></p>';
						}?>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>