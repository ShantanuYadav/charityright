<?php
//echo 'TEST123';
function GetEventDates()
{	$base = new Base();
	$tables = array('eventdates'=>'eventdates');
	$fields = array('eventdates.*');
	$where = array('eventdates.endtime>"' . $base->datefn->SQLDateTime() . '"', 'eventdates.live=1');
	$orderby = array('endtime'=>'eventdates.endtime ASC');
	return $base->db->ResultsArrayFromSQL($base->db->BuildSQL($tables ,$fields ,$where ,$orderby), 'edid', true);
} // end of fn GetEventDates

if ($eventdates = GetEventDates(array('live'=>1)))
{	$events = array();
	$venues = array();
	//print_r($events);
	echo '<div class="eventdates_listing"><ul>';
	foreach ($eventdates as $event_row)
	{	$eventdate = new EventDate($event_row);
		if (!$events[$eventdate->details['eid']])
		{	$events[$eventdate->details['eid']] = new Event($eventdate->details['eid']);
		}
		if (!$venues[$eventdate->details['venue']])
		{	$venues[$eventdate->details['venue']] = new EventVenue($eventdate->details['venue']);
		}
		echo '<li><div class="el_event">', $eventdate->InputSafeString($events[$eventdate->details['eid']]->details['eventname']), '</div><div class="el_eventdate">', $eventdate->DatesString(), ' at ', $venues[$eventdate->details['venue']]->LocationString(), '</div><div class="el_eventdatelink"><a href="', $eventdate->Link(), '">more info</a></div></li>';
	}
	echo '</ul></div>';
} else
{	echo '<h4>No upcoming events</h4>';
}
?>