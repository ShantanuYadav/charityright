<?php


if(SITE_TEST){
  print_r($_SESSION[$this->cart_session_name]);
}

$cartorder = new CartOrder($_SESSION[$this->cart_session_name]['orderid']);

//echo $cartorder->GASingleTransactionTrackingCode();
echo $cartorder->PintrestTagPurchaseCode();
echo $cartorder->SnapPixelCode();
echo $cartorder->TwitterPurchaseCode();
// echo $cartorder->AdRollPurchaseValueCode();

?><div class="post-share-buttons">
    <a href="#share" class="share-button">Share Charity Right</a>
    <div class="ssk-group ssk-count ssk-round" data-url="<?php echo SITE_URL;?>" data-tile="Please make a donation to Charity Right" data-text="Nearly one billion people go to bed hungry every night. I have just made my donation. Now it is your turn.">
        <a href="" class="ssk ssk-facebook"></a>
        <a href="" class="ssk ssk-twitter"></a>
        <a href="" class="ssk ssk-google-plus"></a>
        <a href="" class="ssk ssk-pinterest"></a>
        <a href="" class="ssk ssk-tumblr"></a>
        <a href="" class="ssk ssk-email"></a>
    </div>
</div>

<!-- fb conversion code -->
<?php  echo $cartorder->FBPixelTrackPurchaseCode();  ?>

<!-- Modal -->
<div class="modal fade" id="myTennNights" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
		<div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
		  <p><img src="https://charityright.org.uk/img/mytennightslogo.svg" alt=""></p>
		  <p class="tn-banner-txt-1-pu mt-5" style="line-height:40px;">Don't miss giving on</p>
		  <p class="tn-banner-txt-2-pu mb-5" style="line-height:50px;">Laylatul Qadr</p>
		  <p><img src="https://charityright.org.uk/img/pu-border.png" alt=""></p>
		  <p class="tn-banner-txt-3-pu mt-5" style="line-height:30px;">Schedule your donations in</p>
		  <p class="tn-banner-txt-3-pu mb-5" style="line-height:30px;">just a few minutes with us</p>
		  <p><a href="javascript:;" class="tn-banner-btn-pu" onclick="openMyTenNights()">START NOW</a></p>
		  <p class="mt-5"><img src="https://charityright.org.uk/img/cr-pink-logo.png" alt=""></p>
      </div>
      <div class="modal-footer" style="border:none;">
      </div>
    </div>
  </div>
</div>
<script>
	   $(window).on('load',function(){
       // $('#myTennNights').modal('show');
    });
</script>