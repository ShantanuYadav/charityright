<?php
$this->css['team365'] = 'team365.css';

$yt_code = $this->GetParameter('team365_youtube');
$this->do_infform_footer = false;
//$team365 = new Campaign(SITE_TEST ? 50 : 1046);
//$cursymbol = $this->GetCurrency($team365->details['currency'], 'cursymbol');
//$raised = $team365->GetDonationTotal();
//$target = $team365->details['target'];


$team3652013 = new Campaign(2013);
$cursymbol = $this->GetCurrency($team3652013->details['currency'], 'cursymbol');
$raised = $team3652013->GetDonationTotal();
$target = $team3652013->details['target'];

$teamdaysql = 'SELECT campaigns.* FROM campaigns WHERE campaigns.enabled=1 AND campaigns.visible=1 ORDER BY campaigns.teamoftheday DESC, campaigns.created DESC LIMIT 1';
if ($teamdayresult = $this->db->Query($teamdaysql))
{	if ($teamdayrow = $this->db->FetchArray($teamdayresult))
	{	$teamday = new Campaign($teamdayrow);
	}
}

$sections = array();
foreach ($this->page->PageSections() as $section_row)
{	$section = new PageSection($section_row);
	if ($img_url = $section->GetImageSRC())
	{	$section_row['img_url'] = $img_url;
	}
	$sections[$section_row['listorder']] = $section_row;
}
$this->page->RemovePageSections();

// echo '<div class="container t365header"><div class="container_inner">', stripslashes($this->page->details['headertext']), '<p><a class="t365button_joinus" href="', $this->camp_customer->id ? $team365->JoinLink() : $team365->RegisterLink(), '">Join us</a><a class="t365button_donatenow" href="', $team365->DonateLink(), '">Donate now</a></p></div></div>';

echo '<div class="container t365header"><div class="container_inner">', stripslashes($this->page->details['headertext']), '<p><a class="t365button_joinus" href="https://',$_SERVER['HTTP_HOST'],'/cr-star-campaign/2013/our-team-365/register/">Join us</a><a class="t365button_donatenow" href="https://',$_SERVER['HTTP_HOST'],'/cr-star-campaign/2013/team-365/donate/">Donate now</a></p></div></div>';

echo '<div class="container t365section2"><div class="container_inner">
	<div class="t365section2_left">', stripslashes($this->page->details['pagetext']), '</div>
	<div class="t365section2_right hidden-xs"">
		<iframe class="visible-lg" width="560" height="315" src="https://www.youtube.com/embed/', $yt_code, '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		<iframe class="visible-sm" width="507" height="285" src="https://www.youtube.com/embed/', $yt_code, '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
	<div class="t365section2_right_mobile visible-xs">
		<div class="t365section2_right_mobile_embed">
			<iframe width="200" height="113" src="https://www.youtube.com/embed/', $yt_code, '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>
	<div class="clear"></div>
</div></div>

<div class="container t365section3"><div class="container_inner">', stripslashes($sections[30]['pstext']), '
	<div class="t365section3_steps">
		<div class="t365section3_steps_1"></div>
		<div class="t365section3_steps_divider"></div>
		<div class="t365section3_steps_2"></div>
		<div class="t365section3_steps_divider"></div>
		<div class="t365section3_steps_3"></div>
		<div class="clear"></div>
	</div>
</div></div>

<div class="container t365section4"';
if ($sections[40]['img_url'])
{	echo ' style="background-image: url(\'', $sections[40]['img_url'], '\')";';
}
echo '><div class="container_inner">', stripslashes($sections[40]['pstext']), '</div></div>

<div class="container t365section_teams">
	<div class="container_inner container_inner_half">
		<div class="container_inner_half_left">
			<div class="container_inner_half_content">', stripslashes($sections[45]['pstext']), '
				<div class="t365team_raised">
					<div class="t365team_raised_fill"></div>
					<div class="t365team_raised_inner"><p>', $cursymbol, number_format($raised), '</p><p class="t365team_raised_target">target: ', $cursymbol, number_format($target), '</p></div>
				</div>';
				// echo '<p class="t365team_team_buttons"><a class="t365button_donatenow" href="', $team365->DonateLink(), '">Donate now</a></p>';
				echo '<p class="t365team_team_buttons"><a class="t365button_donatenow" href="https://',$_SERVER['HTTP_HOST'],'/cr-star-campaign/2013/team-365/donate/">Donate now</a></p>';
			echo '</div>
		</div>
		<div class="container_inner_half_right">
			<div class="container_inner_half_content">';
if ($teamday->id)
{	$teamcursymbol = $this->GetCurrency($teamday->details['currency'], 'cursymbol');
	echo stripslashes($sections[50]['pstext']), 
		'<p><a href="', $teamday->Link(), '">', $teamday->FullTitle(), '</a></p>
		<p class="teamdayraised">', $teamcursymbol, number_format($teamday->GetDonationTotal()), ' / ', $teamcursymbol, number_format($teamday->details['target']), '</p>';
}
echo '		</div>
			<div class="t365_teamday_bg"';
if ($sections[50]['img_url'])
{	echo ' style="background-image: url(\'', $sections[50]['img_url'], '\')";';
}
echo '></div>
		</div>
	</div>
</div>

<div class="container t365section6">
	<div class="container_inner_half">
		<div class="container_inner_half_left"';
		if ($sections[60]['img_url'])
		{	echo ' style="background-image: url(\'', $sections[60]['img_url'], '\')";';
		}
		echo '>
			<div class="container_inner_half_content"></div>
		</div>
		<div class="container_inner_half_right">
			<div class="container_inner_half_content">', stripslashes($sections[60]['pstext']), '</div>
		</div>
	</div>
</div>

<div class="container t365section7"><div class="container_inner"';
if ($sections[70]['img_url'])
{	echo ' style="background-image: url(\'', $sections[70]['img_url'], '\')";';
}
echo '><p><ul class="t365section7social">
<li><a href="http://www.facebook.com/charityrightuk" target="_blank"><i class="icon-facebook"></i></a></li>
<li><a href="http://www.twitter.com/charityrightuk" target="_blank"><i class="icon-twitter"></i></a></li>
<li><a href="https://instagram.com/charityrightuk/" target="_blank"><i class="icon-instagram"></i></a></li>
<li><a href="https://www.youtube.com/c/CharityRightUK-official" target="_blank"><i class="icon-youtube"></i></a></li>
</ul></p></div></div>

';
echo "<script>
$(function(){
	$('header').addClass('sticky keepsticky');
});
</script>";
if ($raised_pc = ($target > $raised ? floor(($raised* 100) / $target) : 100))
{	echo '<script>
	$(window).scroll(function()
	{	if (!$(\'.t365team_raised\').hasClass(\'t365team_raised_viewed\'))
		{	var hT = $(\'.t365team_raised\').offset().top, hH = $(\'.t365team_raised\').outerHeight();
			if ($(this).scrollTop() > (hT + hH - $(window).height()))
			{	$(\'.t365team_raised\').addClass(\'t365team_raised_viewed\');
				TeamRaisedHeight(0, ', $raised_pc, ');
			}
		}
	});

	function TeamRaisedHeight(height, last_height)
	{	$(\'.t365team_raised_fill\').css(\'height\', String(height) + \'%\');
		if (height < last_height)
		{	setTimeout(function(){TeamRaisedHeight(height + 1, last_height);}, 40);
		}
	} // end of fn TeamRaisedHeight
	</script>
	';
}

unset($this->page->details['pagetext'], $this->page->details['headertext']);
?>