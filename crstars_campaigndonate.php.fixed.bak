<?php
require_once('init.php');

class CRStarsCampaignDonationPage extends CRStarsPage
{	private $campaign;
	private $donation;
	private $session_name = 'crcamp_donation';
	protected $stage = '';
	protected $stages = array(
			1=>array('heading'=>'Make a Donation', 'content_method'=>'DonationStageContent', 'submit_method'=>'DonationStageSubmit', 'linkable'=>true),
			2=>array('heading'=>'Personal Details', 'content_method'=>'PersonalStageContent', 'submit_method'=>'PersonalStageSubmit', 'linkable'=>true),
			3=>array('heading'=>'Confirmation', 'content_method'=>'ConfirmationStageContent', 'submit_method'=>'ConfirmationStageSubmit'),
			4=>array('heading'=>'Card Details', 'content_method'=>'PaymentStageContent', 'submit_method'=>'PaymentStageSubmit')
		);
	private $current_section = 'amount';
	private $giftaid_unticked = false;

	public function __construct()
	{	parent::__construct();
		$this->campaign = new Campaign($_GET['cid'], $_GET['slug']);
		if ($this->campaign->details['visible'])
		{	if (!$this->campaign->details['enabled'])
			{	header('location: ' . $this->campaign->Link());
				exit;
			}
		} else
		{	header('location: ' . SITE_URL . 'cr-stars/campaign-ended/');
			exit;
		}
		$this->donation = new CampaignDonation();
		$this->canonical_link = $this->campaign->DonateLink();
		$this->css['donations.css'] = 'donations.css';
		$this->js['donations.js'] = 'donations.js';
		$this->css['page.css'] = 'page.css';

		if (!$_SESSION[$this->session_name])
		{	$_SESSION[$this->session_name] = array();
		}

		if ($this->stage = (int)$_POST['donate_submit_stage'])
		{	$this->SubmitDonateForm($_POST);
		} else
		{	if (!$this->stage = (int)$_GET['donate_submit_stage'])
			{	$this->stage = 1;
			}
		}

		if (!$_SESSION[$this->session_name]['donation_currency'])
		{	$_SESSION[$this->session_name]['donation_currency'] = 'GBP';
		}

	} // end of fn __construct

	private function SubmitDonateForm($data = array())
	{	if (($method = $this->stages[$this->stage]['submit_method']) && method_exists($this, $method))
		{	$saved = $this->$method($data);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}
	} // end of fn SubmitDonateForm

	public function MainDonateContent()
	{	ob_start();
		echo '<div class="container donate_main_content"><div class="container_inner">';
		if (($method = $this->stages[$this->stage]['content_method']) && method_exists($this, $method))
		{	echo $this->$method();
		}
		if (SITE_TEST)
		{	echo '<p><a href="/donate/?reset_test=1">reset</a></p>';
			$this->VarDump($_SESSION[$this->session_name]);
		}
		echo '</div></div>';
		return ob_get_clean();
	} // end of fn MainDonateContent

	protected function CurrencyChooser($type = '', $currency_selected = '')
	{	ob_start();
		if ($currencies = $this->GetCurrencies('oneoff'))
		{	echo '<p><label>Your currency</label><select name="donation_currency" onchange="DonationCurrencyChange();">';
			if (!$currencies[$currency_selected])
			{	$currency_selected = false;
			}
			foreach ($currencies as $curcode=>$currency)
			{	if (!$currency_selected)
				{	$currency_selected = $curcode;
				}
				echo '<option value="', $curcode, '"', $curcode == $currency_selected ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
			}
			echo '</select><div class="clear"></div></p>';
		}
		return ob_get_clean();
	} // end of fn CurrencyChooser

	protected function CurrencyChooserNew($type = '', $currency_selected = '')
	{	ob_start();
		if ($currencies = $currencies = $this->GetCurrencies('oneoff')) {
			if (!$currencies[$currency_selected])
			{	$currency_selected = false;
			}
			echo '<div class="custom-select-box"><select id="donation_currency" name="donation_currency" onchange="DonationCurrencyChangeNew();">';
			foreach ($currencies as $curcode=>$currency) {
				if (!$currency_selected)
				{	$currency_selected = $curcode;
				}
				echo '<option value="', $curcode, '"', $curcode == $currency_selected ? ' selected="selected"' : '', '>', $currency['cursymbol'], ' - ', $this->InputSafeString($currency['curname']), '</option>';
			}
			echo '</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>';
		}
		return ob_get_clean();
	} // end of fn CurrencyChooser

	private function DonationStageContent()
	{	ob_start();
		echo '<form action="', $this->campaign->DonateLink(), '" method="post" class="donation-form donation-stage-1 form-rows"><input type="hidden" name="donate_submit_stage" value="1" />
				<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation_currency" class="title-label">Which currency would you like to donate?</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner donation-currency-list">', $this->CurrencyChooserNew($_SESSION[$this->session_name]['donation_type'], $_SESSION[$this->session_name]['donation_currency']), '</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation_amount" class="title-label">I want to donate (<span class="donation-currency-symbol">', $cursymbol = $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol'), '</span>)</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner"><input type="text" name="donation_amount" value="', $_SESSION[$this->session_name]['donation_amount'], '" required /></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-2">
						<div class="form-col-inner"><label for="donation-zakat" class="title-label">Is your donation Zakat? (100% donation policy)</label></div>
					</div>
					<div class="form-col form-col-2">
						<div class="form-col-inner">
							<div class="custom-select-box"><select id="donation_zakat" name="donation_zakat">
								<option value="0" ', $_SESSION[$this->session_name]['donation_zakat'] ? '':'selected="selected"', '>No</option>
								<option value="1" ', $_SESSION[$this->session_name]['donation_zakat'] ? 'selected="selected"' : '', '>Yes</option>
							</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><input type="submit" value="Next" class="button" /></div>
					</div>
				</div>
			</form>';
		return ob_get_clean();
	} // end of fn DonationStageContent

	private function DonationStageSubmit($data = array())
	{	$fail = array();
		$success = array();

		$currencies = $this->GetCurrencies('campaigns');
		if ($currencies[$data['donation_currency']])
		{	$_SESSION[$this->session_name]['donation_currency'] = $data['donation_currency'];
		} else
		{	$fail[] = 'Donation currency missing';
		}

		if (!$_SESSION[$this->session_name]['donation_amount'] = (int)$data['donation_amount'])
		{	$fail[] = 'Donation amount missing';
		}

		$_SESSION[$this->session_name]['donation_zakat'] = ($data['donation_zakat'] ? 1 : 0);

		if (!$fail)
		{	$this->stage = 2;
		}
		return array('failmessage'=>implode('<br />', $fail), 'successmessage'=>implode('<br />', $success));
	} // end of fn DonationStageSubmit

/*	private function GetCurrencies()
	{	$currencies = array();
		$sql = 'SELECT * FROM currencies WHERE use_oneoff ORDER BY curorder, curname';
		if ($result = $this->db->Query($sql))
		{	while($row = $this->db->FetchArray($result))
			{	$currencies[$row['curcode']] = $row;
			}
		}
		return $currencies;
	} // end of fn GetCurrencies*/

	public function PersonalStageContent()
	{	ob_start();
		$add_lines = array();
		if ($_SESSION[$this->session_name]['pca_donor_address1'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_address2'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_address3'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_city'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_postcode'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']);
		}
		if ($_SESSION[$this->session_name]['pca_donor_country'])
		{	$add_lines[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_country']);
		}
		echo '<form action="', $this->campaign->DonateLink(), '" method="post" id="donatePersonalStageForm" class="donation-form donation-stage-2 form-rows">
				<input type="hidden" name="donate_submit_stage" value="2" />
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-title" class="title-label">Title <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-title" name="donor_title" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_title']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-firstname" class="title-label">First name <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-firstname" name="donor_firstname" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_firstname']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-lastname" class="title-label">Last name <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-lastname" name="donor_lastname" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_lastname']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-country" class="title-label">Country <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box"><select id="donor-country" name="donor_country" onchange="DonorCountryChangeNew();" required><option value=""></option>';
		foreach ($this->CountryList() as $code=>$countryname)
		{	echo '				<option value="', $code, '"', (($code == $_SESSION[$this->session_name]['donor_country']) ? ' selected="selected"' : ""), '>', $this->InputSafeString($countryname), '</option>';
		}
		echo					'</select><span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span></div>
							</div>
						</div>
					</div>
					<div class="form-row donorAddressPCA ', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? '' : 'hidden', '">
						<link rel="stylesheet" type="text/css" href="https://services.postcodeanywhere.co.uk/css/captureplus-2.30.min.css?key=pj67-xk39-kp45-jg35" />
						<script type="text/javascript" src="https://services.postcodeanywhere.co.uk/js/captureplus-2.30.min.js?key=pj67-xk39-kp45-jg35"></script>
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="pcaAddress" class="title-label">Address<span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="pcaAddressContainer clearfix ', ($_SESSION[$this->session_name]['pca_donor_address1'] && $_SESSION[$this->session_name]['pca_donor_city'] && $_SESSION[$this->session_name]['pca_donor_postcode']) ? 'hidden' : '', '">
									<input type="text" id="pcaAddress" name="pcaAddress" value="" placeholder="Start typing a postcode, street or address" />
								</div>
								<div class="donor-address-hidden-fields hidden">
									<input type="text" id="pca_donor_address1" name="pca_donor_address1" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']), '" autocomplete="off" />
									<input type="text" id="pca_donor_address2" name="pca_donor_address2" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']), '" autocomplete="off" />
									<input type="text" id="pca_donor_address3" name="pca_donor_address3" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']), '" autocomplete="off" />
									<input type="text" id="pca_donor_city" name="pca_donor_city" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']), '" autocomplete="off" />
									<input type="text" id="pca_donor_postcode" name="pca_donor_postcode" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']), '" autocomplete="off" />
									<input type="text" id="pca_donor_country" name="pca_donor_country" value="', $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_country']), '" autocomplete="off" />
								</div>
								<div class="donor-address-box clearfix ', ($_SESSION[$this->session_name]['pca_donor_address1'] && $_SESSION[$this->session_name]['pca_donor_city'] && $_SESSION[$this->session_name]['pca_donor_postcode']) ? '' : 'hidden', '">
									<div class="donor-address-box-content" name="pca_donor_all_fields">', implode('<br />', $add_lines), '</div>
									<div class="address-search-again"><a href="#pcaAddress" class="button">Search Address Again</a></div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row donorAddressManual', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? ' hidden' : '', '">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-address1" class="title-label">Address<span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<input type="text" id="donor-address1" name="donor_address1" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_address1']), '" placeholder="Address Line 1" />
								<input type="text" name="donor_address2" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_address2']), '" placeholder="Address Line 2" />
								<input type="text" name="donor_city" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_city']), '" placeholder="Town / City" />
								<input type="text" name="donor_postcode" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_postcode']), '" placeholder="Postcode" />
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-email" class="title-label">Email address <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="email" id="donor-email" name="donor_email" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_email']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-phone" class="title-label">Phone number <span class="required">*</span></label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><input type="text" id="donor-phone" name="donor_phone" value="', $this->InputSafeString($_SESSION[$this->session_name]['donor_phone']), '" required/></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donation-anon" class="title-label">Do you want to hide your name</label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner">
								<div class="custom-select-box">
									<select id="donor_anon" name="donor_anon">
										<option value="0" ', !$_SESSION[$this->session_name]['donor_anon'] ? 'selected="selected"' : '', '>No</option>
										<option value="1" ', $_SESSION[$this->session_name]['donor_anon'] ? 'selected="selected"' : '', '>Yes</option>
									</select>
									<span class="custom-select-box-arrow"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row donor-giftaid-box ', ($_SESSION[$this->session_name]['donor_country'] == 'GB') ? '' : ' hidden', '">
						<div class="form-col form-col-1">
							<div class="form-col-inner">
								<div class="alert-box alert-box-green">
									<h3>Reclaim Gift Aid - Add an extra <strong>', $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol'), number_format(($_SESSION[$this->session_name]['donation_amount'] + $_SESSION[$this->session_name]['donation_admin']) * 0.25, 2), '</strong>', $_SESSION[$this->session_name]['donation_type'] == 'monthly' ? ' per month' : '', ' to your donation at no extra cost to you.</h3>
									<p>&nbsp;</p><input type="hidden" name="donor_giftaidchecked" value="" />
									<div class="custom-checkbox clearfix">
										<input type="checkbox" name="donor_giftaid" id="donor-giftaid" value="1" '. (($_SESSION[$this->session_name]['donor_giftaid']) ? 'checked="checked"' : ""). '><label for="donor-giftaid">&nbsp;I want to Gift Aid my donation and any donations I make in the future or have made in the past 4 years to Charity Right.</label>
									</div>
									<div class="form-help-text clearfix"><strong>By ticking this box you are confirming that:</strong><br />"I am a UK taxpayer and understand that if I pay less Income Tax and/or Capital Gains Tax than the amount of Gift Aid claimed on all my donations in that tax year it is my responsibility to pay any difference. I understand that other taxes such as VAT and Council Tax do not qualify. I understand the charity will reclaim 28p of tax on every &pound;1 that I gave up to 5 April 2008 and will reclaim 25p of tax on every &pound;1 that I give on or after 6 April 2008."</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-4">
							<div class="form-col-inner"><label for="donor-phone" class="title-label">Leave a message for ', $this->campaign->owner ? $this->InputSafeString($this->campaign->owner['firstname']) : 'Charity Right', '</label></div>
						</div>
						<div class="form-col form-col-2">
							<div class="form-col-inner"><textarea name="donor_comment">', $this->InputSafeString($_SESSION[$this->session_name]['donor_comment']), '</textarea></div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner">
								<div class="form-row-separator clearfix"></div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-col form-col-1">
							<div class="form-col-inner"><input type="submit" value="Next" class="button"/></div>
						</div>
					</div>',
					$this->giftaid_unticked ? '<div class="donGiftaidChecker"><h4>Remember as a UK tax payer, you can top up your donation by 25% at no extra cost.</h4><p><a onclick="DonorGiftaidChecker(true);">Yes, please add giftaid to my donation</a></p><p><a onclick="DonorGiftaidChecker(false);">No, continue without giftaid</a></p></div>' : '',
				'</form>';
		return ob_get_clean();
	} // end of fn PersonalStageContent

	private function PersonalStageSubmit($data = array())
	{	$fail = array();
		$success = array();
		$donatecheck = $this->DonationStageSubmit($_SESSION[$this->session_name]);
		if ($donatecheck['failmessage'])
		{	$this->stage = 1;
			$fail[] = 'You must tell us what you want to donate';
		} else
		{
			if (!$_SESSION[$this->session_name]['donor_title'] = $data['donor_title'])
			{	$fail[] = 'You must give your title (Mr, Mrs, Miss, etc.)';
			}

			if (!$_SESSION[$this->session_name]['donor_firstname'] = $data['donor_firstname'])
			{	$fail[] = 'You must give your first name';
			}

			if (!$_SESSION[$this->session_name]['donor_lastname'] = $data['donor_lastname'])
			{	$fail[] = 'You must give your last name';
			}

			$_SESSION[$this->session_name]['donor_giftaid'] = 0;
			if (($_SESSION[$this->session_name]['donor_country'] = $data['donor_country']) && $this->GetCountry($data['donor_country']))
			{	if ($gb = ($data['donor_country'] == 'GB'))
				{	if ($data['donor_giftaid'])
					{	$_SESSION[$this->session_name]['donor_giftaid'] = 1;
					} else
					{	$this->giftaid_unticked = true;
					}
				}
			} else
			{	$fail[] = 'You must give your country of residence';
			}

			if ($_SESSION[$this->session_name]['pca_donor_address1'] = $data['pca_donor_address1'])
			{	$pca_address_done++;
			}
			if ($_SESSION[$this->session_name]['pca_donor_address2'] = $data['pca_donor_address2'])
			{	$pca_address_done++;
			}
			if ($_SESSION[$this->session_name]['pca_donor_address3'] = $data['pca_donor_address3'])
			{	$pca_address_done++;
			}
			if (!$pca_address_done && $gb)
			{	$fail[] = 'You must give your address';
			}

			if (!$_SESSION[$this->session_name]['pca_donor_city'] = $data['pca_donor_city'])
			{	if ($gb)
				{	$fail[] = 'You must give your town or city';
				}
			}

			if (!$_SESSION[$this->session_name]['pca_donor_postcode'] = $data['pca_donor_postcode'])
			{	if ($gb)
				{	$fail[] = 'You must give your postcode';
				}
			}

			if ($_SESSION[$this->session_name]['donor_address1'] = $data['donor_address1'])
			{	$address_done++;
			}
			if ($_SESSION[$this->session_name]['donor_address2'] = $data['donor_address2'])
			{	$address_done++;
			}
			if (!$address_done)
			{	if (!$gb)
				{	$fail[] = 'You must give your address';
				}
			}

			if (!$_SESSION[$this->session_name]['donor_city'] = $data['donor_city'])
			{	if (!$gb)
				{	$fail[] = 'You must give your town or city';
				}
			}

			if (!$_SESSION[$this->session_name]['donor_postcode'] = $data['donor_postcode'])
			{	if (!$gb)
				{	$fail[] = 'You must give your postcode';
				}
			}

			$_SESSION[$this->session_name]['donor_comment'] = $data['donor_comment'];
			if ($_SESSION[$this->session_name]['donor_email'] = $data['donor_email'])
			{	if (!$this->ValidEMail($data['donor_email']))
				{	$fail[] = 'You must give a valid email address';
				}
			} else
			{	$fail[] = 'You must give your email address';
			}

			if ($this->ValidPhoneNumber($data['donor_phone'], $gb))
			{	$_SESSION[$this->session_name]['donor_phone'] = $data['donor_phone'];
			} else
			{	$fail[] = 'Your phone number is missing or invalid';
				if (!$_SESSION[$this->session_name]['donor_phone'])
				{	$_SESSION[$this->session_name]['donor_phone'] = $data['donor_phone'];
				}
			}
			$_SESSION[$this->session_name]['donor_anon'] = ($data['donor_anon'] ? 1 : 0);

			if ($fail)
			{	$this->giftaid_unticked = false;
			} else
			{	if (!$this->giftaid_unticked || $_POST['donor_giftaidchecked'])
				{	$this->stage = 3;
				}
			}
		}
		return array('failmessage'=>implode('<br />', $fail), 'successmessage'=>implode('<br />', $success));
	} // end of fn PersonalStageSubmit

	public function ConfirmationStageContent()
	{	ob_start();
		$cursymbol = $this->GetCurrency($_SESSION[$this->session_name]['donation_currency'], 'cursymbol');
		$tc_page = new PageContent('terms-conditions');

		$address = array();
		if (isset($_SESSION[$this->session_name]['donor_address1']) && ($_SESSION[$this->session_name]['donor_country'] != 'GB'))
		{	if ($_SESSION[$this->session_name]['donor_address1'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_address1']);
			}
			if ($_SESSION[$this->session_name]['donor_address2'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_address2']);
			}
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_city']);
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['donor_postcode']);
		} else
		{	if ($_SESSION[$this->session_name]['pca_donor_address1'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address1']);
			}
			if ($_SESSION[$this->session_name]['pca_donor_address2'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address2']);
			}
			if ($_SESSION[$this->session_name]['pca_donor_address3'])
			{	$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_address3']);
			}
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_city']);
			$address[] = $this->InputSafeString($_SESSION[$this->session_name]['pca_donor_postcode']);
		}
		$address[] = $this->InputSafeString($this->GetCountry($_SESSION[$this->session_name]['donor_country']));

		echo '<form action="', $this->campaign->DonateLink(), '" method="post" class="donation-form donation-stage-4 form-rows"><input type="hidden" name="donate_submit_stage" value="3" />
				<div class="form-row donation-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<h3>About Your Donation</h3>
							<p>You will make ', $this->InputSafeString($dtypes[$_SESSION[$this->session_name]['donation_type']]['label']), $_SESSION[$this->session_name]['donation_zakat'] ? ' Zakat' : '', ' donation of ', $cursymbol, number_format($_SESSION[$this->session_name]['donation_amount'], 2), ' for <span class="donationConfirmCampaignTitle">', $this->campaign->FullTitle(), '</span> campaign.</p>',
							$_SESSION[$this->session_name]['donor_giftaid'] ? '<p>We will claim Gift Aid on your donation.</p>' : '',
							'<p><a href="'.$this->campaign->DonateLink().'1/" class="button button-gray-outline">amend donation details</a></p>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row donation-your-details">
					<div class="form-col form-col-1">
						<div class="form-col-inner">
							<h3>Your Details</h3>
							<p>', $this->InputSafeString($_SESSION[$this->session_name]['donor_title']), ' ', $this->InputSafeString($_SESSION[$this->session_name]['donor_firstname']), ' ', $this->InputSafeString($_SESSION[$this->session_name]['donor_lastname']), $_SESSION[$this->session_name]['donor_anon'] ? '<br />(Your name will not be shown to others)' : '', '</p>
							<p>', implode('<br />', $address), '</p>
							<p>Email: ', $this->InputSafeString($_SESSION[$this->session_name]['donor_email']), '</p>';
		if ($_SESSION[$this->session_name]['donor_phone'])
		{	echo '		<p>Phone number: ', $this->InputSafeString($_SESSION[$this->session_name]['donor_phone']), '</p>';
		}
		echo '			<p><a href="', $this->campaign->DonateLink(), '2/" class="button button-gray-outline">amend your details</a></p>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner"><div class="form-row-separator clearfix"></div></div>
					</div>
				</div>
				<div class="form-row donation-terms-and-conditions">
					<div class="form-col form-col-1">
						<div class="form-col-inner" style="text-align: center;">
							<p>By making your donation you are confirming that you accept our <a href="', $tc_page->Link(), '" target="_blank">terms and conditions</a>.</p>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col form-col-1">
						<div class="form-col-inner" style="text-align: center;"><input type="submit" value="Make Your Donation" class="button" /></div>
					</div>
				</div>
			</form>';
		return ob_get_clean();
	} // end of fn ConfirmationStageContent

	private function ConfirmationStageSubmit($data = array())
	{	$donatecheck = $this->DonationStageSubmit($_SESSION[$this->session_name]);
		if ($donatecheck['failmessage'])
		{	$this->stage = 1;
			return array('failmessage'=>'You must tell us what you want to donate', 'successmessage'=>'');
		} else
		{	$personalcheck = $this->PersonalStageSubmit($_SESSION[$this->session_name]);
			if ($personalcheck['failmessage'])
			{	$this->stage = 2;
				return array('failmessage'=>'You must complete your details', 'successmessage'=>'');
			} else
			{	$this->stage = 4;
				$saved = $this->donation->Create($_SESSION[$this->session_name], $this->campaign);
				if ($this->donation->CanBePaid())
				{	if (!SITE_TEST)
					{	$this->bodyOnLoadJS[] = '$("form#wp_button").submit()';
					}
				} else
				{	return array('failmessage'=>$saved['failmessage']);
				}
			}
		}
		return array('failmessage'=>'', 'successmessage'=>'');
	} // end of fn ConfirmationStageSubmit

	public function PaymentStageContent()
	{	ob_start();
		if ($this->donation->CanBePaid())
		{	echo $this->donation->PaymentButton(), '<div class="gatewayRedirect">please wait ... redirecting to Worldpay</div>';
		}
		return ob_get_clean();
	} // end of fn PaymentStageContent

	function MainBodyContent()
	{	if(!$image = $this->campaign->GetImageSRC('large')){
			$image = $avatar = $this->campaign->GetAvatarSRC('large');
		}
		echo '<div class="container page-header" '.(($image) ? 'style="background-image:url('.$image.');"':'').'><div class="container_inner"><h1 class="page-header-title">Donate</h1><p>Donating to '.$this->campaign->FullTitle().' campaign.</p></div></div>';
		echo $this->DonationBreadcrumbs($this->campaign->DonateLink()), $this->MainDonateContent();
	} // end of fn MemberBody

} // end of class CRStarsCampaignDonationPage

$page = new CRStarsCampaignDonationPage();
$page->Page();
?>