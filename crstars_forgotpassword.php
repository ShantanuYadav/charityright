<?php
require_once('init.php');

class CRStarsRegisterPage extends CRStarsPage
{	private $emailsent = false;

	public function __construct()
	{	parent::__construct();
		$this->stages[1] = array('heading'=>'Start a Campaign');
		$this->stages[2] = array('heading'=>'Campaign Details');
		$this->stages[3] = array('heading'=>'Share Campaign');
		$this->canonical_link = SITE_SUB . '/crstarts_forgotpassword.php';

		if ($this->camp_customer->id)
		{	header('location: ' . SITE_SUB . '/my-cr-stars/');
			exit;
		}
		if (isset($_POST['femail']))
		{	$saved = $this->SendNewPassword($_POST['femail']);
			$this->failmessage = $saved['failmessage'];
			$this->successmessage = $saved['successmessage'];
		}

	} // end of fn __construct

	function MainBodyContent()
	{	if ($this->successmessage)
		{	echo $this->CampaignLoginForm();
		} else
		{
			echo '<div class="container crstarsRegisterHeader"><div class="container_inner"><h1 class="page_heading"><span class="register_heading_white">Start</span><br />Campaigning<br />and Help<br /><span class="register_heading_yellow">End</span><br />Hunger</h1></div></div>';
			echo $this->RegisterBreadcrumbs();
			echo '<div class="container crstarsRegister"><div class="container_inner"><h2>Forgotten your password?</h2>
				<h3>Enter your email address and we will send you a new password to use.</h3>
				<form action="" method="post">
				<p><input type="text" name="femail" value="', $this->InputSafeString($_POST['femail']), '" placeholder="Email" /></p>
				<p class="regformButtons"><button type="submit">Get new password</button></p>
			</form>
			</div></div>';
		}
	} // end of fn MemberBody

	function SendNewPassword($email = '')
	{	$fields = array();
		$fail = array();
		$success = array();

		if ($this->ValidEMail($email))
		{	//check for email registered
			if ($result = $this->db->Query('SELECT * FROM campaignusers WHERE email="' . $email . '"'))
			{	if ($this->db->NumRows($result))
				{	// then do the email and send success message
					$newpword = $this->CreatePassword();
					$sql = 'UPDATE campaignusers SET password=MD5("' . $newpword . '") WHERE email="' . $email . '"';
					if ($result = $this->db->Query($sql))
					{	if ($this->db->AffectedRows())
						{	// send email
							$mail = new HTMLMail();
							$body = '<p>Your new password is <strong>' . $newpword . '</strong></p>
<p>You can now log in to Charity Right CR Stars with your email address and this password.</p>';
							$mail->SetSubject('Your New Password from Charity Right');
							$mail->SendFromTemplate($email, array('main_content'=>$body), 'default');
							$success[] = 'Your new password has been emailed to you';
							$this->forgotSent = true;
						}// else $fail[] = 'query failed?';
					}// else $fail[] = $this->db->Error();

				} else
				{	$fail[] = 'Your email address has not been found, please check your typing';
				}
			}// else $fail[] = $this->db->Error();
		} else
		{	$fail[] = 'Invalid email address given';
		}

		return array('failmessage'=>implode(', ', $fail), 'successmessage'=>implode(', ', $success));

	} // end of fn SendNewPassword

} // end of class CRStarsRegisterPage

$page = new CRStarsRegisterPage();
$page->Page();
?>