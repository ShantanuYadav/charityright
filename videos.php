<?php
require_once('init.php');

class VideosPage extends BasePage
{	private $post;
	private $video;

	public function __construct()
	{	parent::__construct('videos');
	} // end of fn __construct

	protected function BaseConstructFunctions()
	{	$this->video = new Video((int)$_GET['vid'], $_GET['vslug']);
		$this->post = new Post((int)$_GET['postid'], $_GET['postslug']);
		$this->css['videos'] = 'videos.css';
		if ($this->video->id)
		{	$this->title = $this->InputSafeString($this->video->details['vtitle'] . ' - at Charity Right');
		}
	} // end of fn BaseConstructFunctions

	protected function SetFBMeta()
	{	parent::SetFBMeta();
		if ($this->video->id)
		{	$this->fb_meta['url'] = $this->canonical_link;
			$this->fb_meta['title'] = $this->title;
			if ($src = $this->video->GetImageSRC('og'))
			{	$this->fb_meta['image'] = $src;
			}
		}
	} // end of fn SetFBMeta

	public function GetMetaDesc()
	{	return ($this->video->id && $this->video->details['metadesc']) ? $this->video->details['metadesc'] : parent::GetMetaDesc();
	} // end of fn GetMetaDesc

	public function SetCanonicalLink()
	{	parent::SetCanonicalLink();
		if ($this->video->id)
		{	$this->canonical_link = $this->video->Link();
		}
	} // end of fn SetCanonicalLink

	public function MainBodyContent()
	{	echo $this->MainBodyContentDefault();
	} // end of fn MainBodyContent

	public function MainBodyContentDefault()
	{	ob_start();
		echo '<div class="container videosHeaderContainer"><div class="container_inner"><h1 class="page_heading">';
		if ($this->video->id)
		{	echo $this->InputSafeString($this->video->details['vtitle']);
		} else
		{	echo 'Videos';
			if ($this->post->id)
			{	echo ' for ', $this->InputSafeString($this->post->details['posttitle']);
			}
		}
		echo '</h1>';
		echo '</div></div>';
		echo '<div class="container videosContainer"><div class="container_inner">';
		if ($this->video->id)
		{	echo '<div class="videoContainer">', $this->video->EmbedCode(900, array('autoplay')), '</div><p class="videoDesc">', $this->InputSafeString($this->video->details['description']), '</p>';
		}
		if ($videos = $this->GetVideos())
		{	if ($this->post->id && $this->video->id)
			{	echo '<h3>More videos for ', $this->InputSafeString($this->post->details['posttitle']), '</h3>';
			}
			echo '<ul>';
			foreach ($videos as $video_row)
			{	$video = new Video($video_row);
				echo '<li';
				if ($src = $video->GetImageSRC())
				{	echo ' style="background: url(\'', $src, '\') no-repeat top center;"';
				} else
				{	if ($src = $video->YoutubeImageURL())
					{	echo ' style="background: #FFF url(\'', $src, '\') no-repeat top center; background-size: 100%;"';
					}
				}
				echo '><a class="videoImgLink', $src ? '' : ' videoImgLinkNoImage', '" href="', $link = $video->Link($this->post), '" /></a><h2><a href="', $link, '" />', $this->InputSafeString($video->details['vtitle']), '</a></h2></li>';
			}
			echo '</ul><div class="clear"></div>';
			//$this->VarDump($videos);
		}
		if ($this->post->id)
		{	echo '<p class="allVideosLink"><a href="', $this->page->Link(), '">View all videos</a></p>';
		}
		echo '</div></div>';
		return ob_get_clean();
	} // end of fn MainBodyContentDefault

	public function GetVideos()
	{	$tables = array('videos'=>'videos');
		$fields = array('videos.*');
		$where = array('live'=>'videos.live=1');
		$orderby = array('videos.uploaded DESC');

		if ($this->post->id)
		{	$tables['postvideos'] = 'postvideos';
			$where['postvideos_link'] = 'postvideos.vid=videos.vid';
			$where['postid'] = 'postvideos.postid=' . $this->post->id;
		}

		if ($this->video->id)
		{	$where['video->id'] = 'NOT videos.vid=' . $this->video->id;
		}

		return $this->db->ResultsArrayFromSQL($this->db->BuildSQL($tables, $fields, $where, $orderby), 'vid', true);

	} // end of fn GetVideos

} // end of class VideosPage

$page = new VideosPage();
$page->Page();
?>