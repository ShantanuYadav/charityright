<?php 
require_once('init.php');

class CRStarsCampaignThankYouPage extends CRStarsCampaignViewPage
{
	public function __construct()
	{	parent::__construct();
		$this->canonical_link = $this->campaign->ThankYouLink();
		$this->successmessage = 'Thank you for your donation';
	} // end of fn __construct
	
} // end of class CRStarsCampaignThankYouPage

$page = new CRStarsCampaignThankYouPage();
$page->Page();
?>